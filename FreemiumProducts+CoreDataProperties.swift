//
//  FreemiumProducts+CoreDataProperties.swift
//  
//
//  Created by a k azad on 19/12/19.
//
//

import Foundation
import CoreData


extension FreemiumProducts {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FreemiumProducts> {
        return NSFetchRequest<FreemiumProducts>(entityName: "FreemiumProducts")
    }

    @NSManaged public var categoryId: Int64
    @NSManaged public var categoryName: String?
    @NSManaged public var company: String?
    @NSManaged public var image: String?
    @NSManaged public var inventoryId: Int64
    @NSManaged public var name: String?
    @NSManaged public var productId: Int64
    @NSManaged public var sellingPrice: Double
    @NSManaged public var sku: String?
    @NSManaged public var unit: String?
    @NSManaged public var varient: String?

}
