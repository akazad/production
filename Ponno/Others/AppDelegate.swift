//
//  AppDelegate.swift
//  Ponno
//
//  Created by a k azad on 18/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SDWebImage
import Firebase
import CoreData

@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private var presenter = VersionCheckPresenter(service: LoginService())
    
    var latestVersion : String?
    var forceUpdate : Bool?
    var recommendedUpdate : Bool?
    
    var lastUpdateTime: String?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 24.0 
        self.setRootViewController()
        //self.setUpLanguage(isBangla : true)
        //LanguageManager.setLanguageAs(language: .Bangla)
        //setUpInitialLanguage()
        //print("Documents Directory: ", FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last ?? "Not Found!")

        FirebaseApp.configure()
        self.setUpLanguage()
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
           setApplicationVersion(versionNumber: version)
        }
        
        self.attachPresenter()
        
        return true
    }
    
    func setUpLanguage(){
        if getLanguage(){
            LanguageManager.setLanguageAs(language: .Bangla)
        }
        else{
            LanguageManager.setLanguageAs(language: .English)
        }
    }
    
    func version() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return "\(version) build \(build)"
    }
    
    func setRootViewController(){
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        if isUserLoggedIn(){
            if let settings = getViewSettings(){
                if settings.dashboard == "FreemiumDashboard"{
                    let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                    let viewController: FreemiumHomeViewController = storyboard.instantiateViewController(withIdentifier: "FreemiumHomeViewController") as! FreemiumHomeViewController
                    let navigationController = UINavigationController(rootViewController: viewController)
                    navigationController.isNavigationBarHidden = false
                    self.window?.rootViewController = navigationController
                }else{
                    let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                    let viewController: HomeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    let navigationController = UINavigationController(rootViewController: viewController)
                    navigationController.isNavigationBarHidden = false
                    self.window?.rootViewController = navigationController
                }
            }
        }else{
            let storyboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
            let loginViewController: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            //self.window?.rootViewController = loginViewController
            let navigationController = UINavigationController(rootViewController: loginViewController)
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window!.rootViewController = navigationController
        }
        self.window?.makeKeyAndVisible()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        self.setRootViewController()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.attachPresenter()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        //resetAllRecords(in : "FreemiumProducts")
    }
    
    
    
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "FreemiumProduct")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
//    lazy var managedObjectContext: NSManagedObjectContext = {
//        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
//        let coordinator = self.persistentStoreCoordinator
//        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
//        managedObjectContext.persistentStoreCoordinator = coordinator
//        return managedObjectContext
//    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }


}

extension AppDelegate : VersionCheckDelegate{
    func versionCheck(data: VersionCheckDataMapper) {
        
        self.latestVersion = data.latestVersion
        self.forceUpdate = data.forceUpdate
        self.recommendedUpdate = data.recommendedUpdate
        
        if let message = data.message {
            if data.forceUpdate != false {
                forceAlertController(message: message)
            }else if data.recommendedUpdate != false {
                recommendedAlertController(message: message)
            }
        }
        
        
    }
    
    func onFailed(message: String) {
        
    }
    
    func showLoading() {
        
    }
    
    func hideLoading() {
        
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getVersionCheckDataFromServer()
    }
}

extension AppDelegate {
    func recommendedAlertController (message : String) -> Void {
        let alertController = UIAlertController(title: message, message: "", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            if let url = URL(string: "https://apps.apple.com/app/id1479221352"),
                UIApplication.shared.canOpenURL(url)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func forceAlertController (message : String) -> Void {
        let alertController = UIAlertController(title: message, message: "", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            if let url = URL(string: "https://apps.apple.com/app/id1479221352"),
                UIApplication.shared.canOpenURL(url)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
//        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
//            (action:UIAlertAction!) in
//        }
        alertController.addAction(OKAction)
//        alertController.addAction(cancelAction)
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
}

