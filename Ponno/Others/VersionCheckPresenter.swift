//
//  VersionCheckPresenter.swift
//  Ponno
//
//  Created by a k azad on 11/7/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol VersionCheckDelegate : NSObjectProtocol {
    func versionCheck (data: VersionCheckDataMapper)
    func onFailed(message : String)
    func showLoading()
    func hideLoading()
}
class VersionCheckPresenter: NSObject {
    
    private let service : LoginService
    weak private var viewDelegate : VersionCheckDelegate?
    
    init(service : LoginService) {
        self.service = service
    }
    
    func attachView(viewDelegate : VersionCheckDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVersionCheckDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getVersionCheckData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.versionCheck(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}



