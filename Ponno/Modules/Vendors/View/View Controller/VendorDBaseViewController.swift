//
//  VendorDBaseViewController.swift
//  Ponno
//
//  Created by a k azad on 29/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import FloatingPanel
import SDWebImage

class VendorDBaseViewController: UIViewController,FloatingPanelControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = VendorDetailsViewPresenter(service: VendorsService())
    
    var fpc: FloatingPanelController!
    var bottomShetVC : VendorDViewController!
    
    
    var vendorId : Int?
    var vendorDetails : VendorDetails?{
        didSet{
            self.refreshTableView()
        }
    }
    
    var currentPayable : String?
    
    enum Sections : Int{
        case VendorInfo
        case VendorSummary
    }
    
    var payableData : PayableLogList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.setBarBtn()
        self.attachPresenter()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addBottomSheet()
        self.initialSetup()
        self.setUpInitialLanguage()
    }
    
    func addBottomSheet(){
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = UIColor.lightGreen.withAlphaComponent(0.5)
        fpc.surfaceView.layer.cornerRadius = 9.0
        fpc.surfaceView.clipsToBounds = true
        
        fpc.surfaceView.shadowHidden = false
        bottomShetVC = storyboard?.instantiateViewController(withIdentifier: "VendorDViewController") as? VendorDViewController
        bottomShetVC.vendorId = self.vendorId
        
        
        fpc.set(contentViewController: bottomShetVC)
        fpc.track(scrollView: bottomShetVC.tableView)
        fpc.addPanel(toParent: self)
    }
    
    func removeBottomSheet(){
        fpc.removePanelFromParent(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeBottomSheet()
    }
    
    func initialSetup(){
        self.title = LanguageManager.VendorProfile
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarBtn(){
        let popUpBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        popUpBtn.setImage(UIImage(named: "popupmenudark"), for: UIControl.State.normal)
        popUpBtn.addTarget(self, action: #selector(onBarPopUpBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        popUpBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        popUpBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        popUpBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn3 = UIBarButtonItem(customView: popUpBtn)
        
        navigationItem.setRightBarButtonItems([barBtn3], animated: true)
    }
    
    @objc func onBarPopUpBtnTapped(sender: UIButton){
        self.popOverToDBasePopoverViewController(sender: sender)
    }
    
    
    func popOverToDBasePopoverViewController(sender: UIButton){
        let storyboard : UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: "DBaseViewPopoverViewController") as? DBaseViewPopoverViewController{
            controller.preferredContentSize = CGSize(width: 170, height: 120)
            controller.popedOverFrom = PopedOverFrom.vendor.rawValue
            controller.vendorId = self.vendorId
            controller.navigate = self
            controller.navigateTo = self
            showPopup(controller, sourceView: sender)
        }
        
    }
    
}

//MARK: TableViewDelegate And DataSource
extension VendorDBaseViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(VendorDetailsCell.nib, forCellReuseIdentifier: VendorDetailsCell.identifier)
        self.tableView.register(VendorDetailsInfoCell.nib, forCellReuseIdentifier: VendorDetailsInfoCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.VendorInfo.rawValue:
            return 1
        case Sections.VendorSummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case Sections.VendorInfo.rawValue:
            let cell: VendorDetailsCell = tableView.dequeueReusableCell(withIdentifier: VendorDetailsCell.identifier, for: indexPath) as! VendorDetailsCell
            cell.selectionStyle = .none
            
            guard let vendorDetail = self.vendorDetails else{
                return cell
            }
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.vendorImage + vendorDetail.image), placeholderImage: UIImage(named: "placeholder"))
            cell.nameLabel.text = vendorDetail.name
            cell.phoneLabel.text = LanguageManager.Phone + " : " + vendorDetail.phone
            let address = vendorDetail.address
            if address == "" {
                cell.addressLabel.text = LanguageManager.Address + " : " + "---"
            }else{
                cell.addressLabel.text = LanguageManager.Address + " : " + vendorDetail.address
            }
            
            self.currentPayable = "\(vendorDetail.totalPayable)"
            cell.duePaidBtn.addTarget(self, action: #selector(onDuePaidBtnPressed), for: .touchUpInside)
            
            return cell
            
        case Sections.VendorSummary.rawValue :
            let cell: VendorDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: VendorDetailsInfoCell.identifier, for: indexPath) as! VendorDetailsInfoCell
            cell.selectionStyle = .none
            
            guard let vendorDetail = self.vendorDetails else{
                return cell
            }
            cell.totalPurchaseLabel.text = "\(vendorDetail.totalPurchase)"
            
            let totalPayable = vendorDetail.totalPayable
            if totalPayable == "" {
                cell.totalPayableLabel.text = "0.00" + " " + Constants.currencySymbol
            }else{
                cell.totalPayableLabel.text = "\(totalPayable)" + " " + Constants.currencySymbol
            }
            
            let currentPayable = vendorDetail.currentPayable
            if currentPayable.isEmpty {
                cell.currentPayableLabel.text = "0.00" + " " + Constants.currencySymbol
            }else{
                cell.currentPayableLabel.text = "\(currentPayable)" + " " + Constants.currencySymbol
            }
            
            let totalPaid = vendorDetail.totalPaid
            if totalPaid != ""{
                cell.totalPaidLabel.text = "\(totalPaid)" + " " + Constants.currencySymbol
            }else{
                cell.totalPaidLabel.text = "0.00" + " " + Constants.currencySymbol
            }
            
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.VendorInfo.rawValue:
            return UITableView.automaticDimension
        case Sections.VendorSummary.rawValue:
            return 140.0
        default:
            return 0
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onDuePaidBtnPressed(sender: UIButton){
        self.navigateToVendorPaidVC()
    }
    
    func navigateToVendorPaidVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorPaidViewController") as! VendorPaidViewController
        viewController.vendorDetails = vendorDetails
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}


extension VendorDBaseViewController : VendorsDetailsViewDelegate{
    
    func setVendorsDetailsData(data: VendorsDetailsDataMapper) {
        guard let details = data.vendor else {
            return
        }
        self.vendorDetails = details
        //self.tableView.isHidden = false
    }
    
    func setVendorsPayableHistoryData(data: VendorPayableHistoryDataMapper) {
        
    }
    
    func setVendorsPurchaseHistoryData(data: VendorPurchaseHistoryDataMapper) {
        
    }
    
    func setVendorsPurchaseReturnHistoryData(data: VendorPurchaseReturnHistoryDataMapper){
        
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        
        guard let id = self.vendorId else{
            return
        }
        self.presenter.getVendorsDataFromServer(id: id)
    }
    
}

extension VendorDBaseViewController : NavigateToAdjustAmount{
    func navigateTo(id: Int) {
        navigateToPayableAdjustViewController(id: id)
    }
    
    func navigateToPayableAdjustViewController(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PayableAdjustListViewController") as! PayableAdjustListViewController
        viewController.vendorId = id
        viewController.vendor = self.vendorDetails
        viewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension VendorDBaseViewController : NavigateToWallet{
    func navigateToWallet(id: Int) {
        navigateToVendorWalletViewController(id: id)
    }

    func navigateToVendorWalletViewController(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorWalletViewController") as! VendorWalletViewController
        viewController.vendorId = id
        viewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
