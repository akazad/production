//
//  VendorSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 18/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class VendorSearchViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = VendorSearchPresenter(service: VendorsService())
    
    var vendorList : [VendorsList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var searchText = ""
    var currentPage : Int = 1
    var lastPage : Int = 0
    
    var isLoading : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSearchBar()
        self.configureTableView()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.setNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    func navigateToUpdateVendorVC(vendors: VendorsList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewVendorViewController") as! AddNewVendorViewController
        viewController.vendors = vendors
        viewController.vendorState = VendorState.Update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.presenter.getVendorSearchDataFromServer(page: self.currentPage, searchText: "")
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Search Delegate
extension VendorSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.VendorSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.vendorList = []
        self.isLoading = true
        self.isSearchActive = true
        self.presenter.getVendorSearchDataFromServer(page: self.currentPage, searchText: searchText)
    }
}

//MARK: TableView Delegate And DataSoruce
extension VendorSearchViewController : UITableViewDelegate, UITableViewDataSource{
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(VendorsListCell.nib, forCellReuseIdentifier: VendorsListCell.identifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.vendorList.count > 0  {
            return self.vendorList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : VendorsListCell = tableView.dequeueReusableCell(withIdentifier: VendorsListCell.identifier, for: indexPath) as! VendorsListCell
        cell.selectionStyle = .none
        
        if self.vendorList.count > 0 {
            let vendors = self.vendorList[indexPath.row]
            
            cell.vendorList = vendors
            
            if isLoading == false && indexPath.row == self.vendorList.count - 1 && self.currentPage < self.lastPage{
                self.isLoading = true
                self.currentPage += 1
                if isSearchActive{
                    self.presenter.getVendorSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.vendorList.count > 0 {
            let listItem = self.vendorList[indexPath.row]
            let id = listItem.id
            self.navigateToVendorDetailsVC(vendorId: id)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            if self.vendorList.count > 0{
                let vendor = self.vendorList[indexPath.row]
                self.navigateToUpdateVendorVC(vendors: vendor)
                
            }
        }
        
        edit.backgroundColor = UIColor.orange
        
        return [edit]
        
    }
    
    fileprivate func navigateToVendorDetailsVC(vendorId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorDBaseViewController") as! VendorDBaseViewController
        viewController.vendorId = vendorId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    fileprivate func refreshTableView(){
        self.tableView.reloadData()
    }
    
}

//Mark: Api Delegate
extension VendorSearchViewController : VendorsSearchViewDelegate{
    func setVendorsSerachData(data: VendorsDataMapper) {
        guard let list = data.vendors, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.vendorList += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPage = pagination.lastPageNo
    }
    
    func onFailed(failure : String) {
        self.searchAlert(title: LanguageManager.NoInformationFound, message: failure)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getVendorSearchDataFromServer(page: self.currentPage, searchText: "")
    }
}
