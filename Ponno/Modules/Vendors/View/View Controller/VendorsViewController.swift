//
//  VendorsViewController.swift
//  Ponno
//
//  Created by a k azad on 27/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

class VendorsViewController: BaseViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionVIew: UICollectionView!
    @IBOutlet weak var tableView: UITableView!

    private var presenter = VendorsPresenter(service: VendorsService())
    
    var vendorList : [VendorsList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    private var vendorSummary : [VendorsSummary]?{
        didSet{
            self.collectionVIew.reloadData()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var filteredList : [VendorsList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var currentPage : Int = 1
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.setNavigationBarTitle()
        self.configureTableView()
        self.configureCollectionView()
        //self.setUpSearchBar()
        self.addFloaty()
        self.setBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.resetData()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.VendorBook
       self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0))]
       self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
//    self.navigateToVendorAddViewController()
    func setBarButton(){
        
        let search: UIButton = UIButton (type: UIButton.ButtonType.custom)
        search.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        search.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        search.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        search.widthAnchor.constraint(equalToConstant: 24).isActive = true
        search.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: search)
                
        let addBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        addBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        addBtn.addTarget(self, action: #selector(onAdd(sender:)), for: UIControl.Event.touchUpInside)
        addBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        addBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        addBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: addBtn)
        
       navigationItem.setRightBarButtonItems([barButton2,barButton1], animated: true)
        
    }
    
    @objc func onSearch(sender: UIButton){
        self.navigateToVendorsSearchVC()
    }
    
    func navigateToVendorsSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorSearchViewController") as! VendorSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @objc func onAdd(sender: UIButton){
        self.navigateToVendorAddViewController()
    }
    
    @IBAction func addDealerCompany(_ sender: UIBarButtonItem) {
        self.navigateToVendorAddViewController()
    }
    
    func resetData(){
        self.vendorList = []
    }
    
    func navigateToUpdateVendorVC(vendors: VendorsList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewVendorViewController") as! AddNewVendorViewController
        viewController.vendors = vendors
        viewController.vendorState = VendorState.Update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToVendorAddViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewVendorViewController") as! AddNewVendorViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        
//        let extraItem = FloatyItem()
//        extraItem.icon = UIImage(named: "next")
//        extraItem.buttonColor = UIColor.lightGreen
//        extraItem.title = "নতুন ডিলার/কোম্পানি"
//        extraItem.handler = { item in
//            self.navigateToVendorAddViewController()
//        }
        
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(VendorsViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.currentPage = 1
        self.vendorList = []
        self.presenter.getVendorsDataFromServer(page : self.currentPage)
        refreshControl.endRefreshing()
    }
    
}

//Mark: Floaty Delegate
extension VendorsViewController: FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}

//MARK: SearchBar Delegate
//extension VendorsViewController: UISearchBarDelegate{
//
//
//    fileprivate func setUpSearchBar(){
//        self.searchBar.delegate = self
//    }
//
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        self.view.endEditing(true)
//        //self.navigateToVendorsSearchVC()
//        isSearchActive = false
//    }
//
////    func navigateToVendorsSearchVC(){
////        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
////        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorSearchViewController") as! VendorSearchViewController
////        self.navigationController?.pushViewController(viewController, animated: true)
////
////    }
//}

//MARK: CollectionView Delegate And DataSource
extension VendorsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionVIew.delegate = self
        self.collectionVIew.dataSource = self
        self.collectionVIew.register(VendorsSummaryCell.nib, forCellWithReuseIdentifier: VendorsSummaryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let summaryList = self.vendorSummary, summaryList.count > 0 else{
            return 0
        }
        return summaryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: VendorsSummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: VendorsSummaryCell.identifier, for: indexPath) as! VendorsSummaryCell
        guard let summaryItem = self.vendorSummary, summaryItem.count > 0 else {
            return cell
        }
        let summaryList = summaryItem[indexPath.row]
        
//        for (key, _) in summaryItem.enumerated() {
//            if key == 0 {
//                summaryItem[0].title = "Total Vendor Number"
//            }else if key == 1 {
//                summaryItem[1].title = "Current Payable"
//            }else if key == 2 {
//                summaryItem[2].title = "Total Payable Withdraw"
//            }
//        }
        
        cell.titleLabel.text = summaryList.title
        cell.valueLabel.text = summaryList.value
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionVIew.frame.width, height: 60.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.vendorSummary, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionVIew)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//MARK: TableView Delegate And DataSoruce
extension VendorsViewController : UITableViewDelegate, UITableViewDataSource{
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(VendorsListCell.nib, forCellReuseIdentifier: VendorsListCell.identifier)
        self.tableView.estimatedRowHeight = 105
        self.tableView.separatorStyle = .none
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.vendorList.count > 0 {
            return self.vendorList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : VendorsListCell = tableView.dequeueReusableCell(withIdentifier: VendorsListCell.identifier, for: indexPath) as! VendorsListCell
        cell.selectionStyle = .none
        
        if self.vendorList.count > 0 {
            let vendors = self.vendorList[indexPath.row]
            
            cell.vendorList = vendors
            
            cell.popUpBtn.tag = vendors.id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnPressed), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.vendorList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getVendorsDataFromServer(page: self.currentPage)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.vendorList.count > 0 {
            let listItem = self.vendorList[indexPath.row]
            let id = listItem.id
            self.navigateToVendorDBaseViewController(vendorId: id)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            
            if self.vendorList.count > 0{
                let vendor = self.vendorList[indexPath.row]
                self.navigateToUpdateVendorVC(vendors: vendor)
                
            }
        }
        
        edit.backgroundColor = UIColor.orange
        
        return [edit]
        
    }
    
    fileprivate func navigateToVendorDetailsVC(vendorId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorDetailsBaseViewController") as! VendorDetailsBaseViewController
        viewController.vendorId = vendorId 
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    fileprivate func navigateToVendorDBaseViewController(vendorId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorDBaseViewController") as! VendorDBaseViewController
        viewController.vendorId = vendorId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    fileprivate func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopUpBtnPressed(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.vendorList.count > 0{
            let vendorId = sender.tag
            for vendor in self.vendorList{
                if vendorId == vendor.id{
                    let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToUpdateVendorVC(vendors: vendor)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(editAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }

}

//MARK: API Delegate
extension VendorsViewController : VendorsViewDelegate{
    func setVendorsData(data: VendorsDataMapper) {
        guard let summaryItem = data.summary, summaryItem.count > 0 else{
            return
        }
        self.vendorSummary = summaryItem
        self.collectionVIew.reloadData()
        self.setUpAutoScroll()
        
        guard let list = data.vendors, list.count > 0 else{
            return
        }
        self.isLoading = false
        self.vendorList += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
        
    }
    
    func onFailed() {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.currentPage = 1
        self.vendorList = []
        self.presenter.getVendorsDataFromServer(page : self.currentPage)
    }
}
