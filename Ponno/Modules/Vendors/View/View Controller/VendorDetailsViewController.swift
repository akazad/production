//
//  VendorDetailsViewController.swift
//  Ponno
//
//  Created by a k azad on 15/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SVProgressHUD
import Floaty

class VendorDetailsViewController: UIViewController{
    
    @IBOutlet weak var emptyMessage: UILabel!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet var panView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = VendorDetailsViewPresenter(service: VendorsService())
    var vendorId : Int?
    
    var vendorPayables : [VendorPayables] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var vendorPurchases : [VendorPurchases] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var vendorReturns : [VendorReturns] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var selectedSegment : Int?
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    //////PanView
    var lastY: CGFloat = 0
    var pan: UIPanGestureRecognizer!
    
    var bottomSheetDelegate: BottomSheetDelegate?
    var parentView: UIView!
    
    var initalFrame: CGRect!
    var topY: CGFloat = 80 //change this in viewWillAppear for top position
    var middleY: CGFloat = 400 //change this in viewWillAppear to decide if animate to top or bottom
    var bottomY: CGFloat = 600 //no need to change this
    let bottomOffset: CGFloat = 80 //sheet height on bottom position
    var lastLevel: SheetLevel = .bottom //choose inital position of the sheet
    
    var disableTableScroll = false
    
    //hack panOffset To prevent jump when goes from top to down
    var panOffset: CGFloat = 0
    var applyPanOffset = false
    
    //tableview variables
    var listItems: [Any] = []
    var headerItems: [Any] = []
    
    var modelStatus : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.attachPresenter()
        pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        pan.delegate = self
        self.panView.addGestureRecognizer(pan)
        
        self.tableView.panGestureRecognizer.addTarget(self, action: #selector(handlePan(_:)))
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap(_:)))
        tap.delegate = self
        tableView.addGestureRecognizer(tap)
        self.configureTableView()
    }
    
    func navigateToVendorAddViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewVendorViewController") as! AddNewVendorViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.attachPresenter()
        self.initalFrame = UIScreen.main.bounds
        self.topY = round(initalFrame.height * 0.05)
        self.middleY = initalFrame.height * 0.6
        self.bottomY = initalFrame.height - bottomOffset
        self.lastY = self.middleY
        
        bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.middleY))
    }
    
    
    @IBAction func segmentedControllAction(_ sender: UISegmentedControl) {
        guard let id = self.vendorId else{
            return
        }
        if segmentedControl.selectedSegmentIndex == 0{
            self.isLoading = true
            self.currentPage = 1
            self.vendorPayables = []
            self.presenter.getVendorsPayableHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
        }
        else if segmentedControl.selectedSegmentIndex == 1{
            self.isLoading = true
            self.currentPage = 1
            self.vendorPurchases = []
            self.presenter.getVendorsPurchaseHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
        }
        else if segmentedControl.selectedSegmentIndex == 2{
            self.isLoading = true
            self.currentPage = 1
            self.vendorReturns = []
            self.presenter.getVendorsPurchaseReturnHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
        }
        else{
            return
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == tableView else {return}
        
        if (self.parentView.frame.minY > topY){
            self.tableView.contentOffset.y = 0
        }
    }
    
    
    //this stops unintended tableview scrolling while animating to top
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard scrollView == tableView else {return}
        
        if disableTableScroll{
            targetContentOffset.pointee = scrollView.contentOffset
            disableTableScroll = false
        }
    }
    
    
    @objc func handleTap(_ recognizer: UITapGestureRecognizer){
        let p = recognizer.location(in: self.tableView)
        let index = tableView.indexPathForRow(at: p)
        
        if segmentedControl.selectedSegmentIndex == 1{
            if self.vendorPurchases.count > 0 {
                guard let purchaseIndex = index else{
                    return
                }
                let purchaseItem = self.vendorPurchases[purchaseIndex.row]
                self.navigateToPurchaseDetailsVC(selectedId: purchaseItem.id)
            }
        }
        else if segmentedControl.selectedSegmentIndex == 2{
            if self.vendorReturns.count > 0 {
                guard let purchaseReturnIndex = index else{
                    return
                }
                let vendorReturnItem = self.vendorReturns[purchaseReturnIndex.row]
                self.navigateToPurchaseReturnDetailsVC(selectedId: vendorReturnItem.id)
            }
        }
      
        tableView.selectRow(at: index, animated: false, scrollPosition: .none)
    }
    
    func navigateToPurchaseDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseDetailsViewController") as! PurchaseDetailsViewController
        viewController.selectedId = selectedId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchaseReturnDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseReturnDetailsViewController") as! PurchaseReturnDetailsViewController
        viewController.selectedId = selectedId 
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    @objc func handlePan(_ recognizer: UIPanGestureRecognizer){
        
        let dy = recognizer.translation(in: self.parentView).y
        switch recognizer.state {
        case .began:
            applyPanOffset = (self.tableView.contentOffset.y > 0)
        case .changed:
            if self.tableView.contentOffset.y > 0{
                panOffset = dy
                return
            }
            
            if self.tableView.contentOffset.y <= 0{
                if !applyPanOffset{panOffset = 0}
                let maxY = max(topY, lastY + dy - panOffset)
                let y = min(bottomY, maxY)
                //                self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: y)
                bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: y))
            }
            
            if self.parentView.frame.minY > topY{
                self.tableView.contentOffset.y = 0
            }
        case .failed, .ended, .cancelled:
            panOffset = 0
            
            if (self.tableView.contentOffset.y > 0){
                return
            }
            
            self.panView.isUserInteractionEnabled = false
            
            self.disableTableScroll = self.lastLevel != .top
            
            self.lastY = self.parentView.frame.minY
            self.lastLevel = self.nextLevel(recognizer: recognizer)
            
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.9, options: .curveEaseOut, animations: {
                
                switch self.lastLevel{
                case .top:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.topY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.topY))
                    self.tableView.contentInset.bottom = 50
                case .middle:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.middleY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.middleY))
                case .bottom:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.bottomY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.bottomY))
                }
                
            }) { (_) in
                self.panView.isUserInteractionEnabled = true
                self.lastY = self.parentView.frame.minY
            }
        default:
            break
        }
    }
    
    func nextLevel(recognizer: UIPanGestureRecognizer) -> SheetLevel{
        let y = self.lastY
        let velY = recognizer.velocity(in: self.view).y
        if velY < -200{
            return y > middleY ? .middle : .top
        }else if velY > 200{
            return y < (middleY + 1) ? .middle : .bottom
        }else{
            if y > middleY {
                return (y - middleY) < (bottomY - y) ? .middle : .bottom
            }else{
                return (y - topY) < (middleY - y) ? .top : .middle
            }
        }
    }
    
}

//Mark: TableView Delegate And Data Source
extension VendorDetailsViewController: UITableViewDelegate, UITableViewDataSource{ 
    
    func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = .clear
        self.tableView.register(VendorPayableHistoryCell.nib, forCellReuseIdentifier: VendorPayableHistoryCell.identifier)
        self.tableView.register(VendorPurchaseHistoryCell.nib, forCellReuseIdentifier: VendorPurchaseHistoryCell.identifier)
        self.tableView.register(VendorReturnsCell.nib, forCellReuseIdentifier: VendorReturnsCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.emptyMessage.isHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl.selectedSegmentIndex == 0{
            if self.vendorPayables.count > 0 {
                self.emptyMessage.isHidden = true
                return self.vendorPayables.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        if segmentedControl.selectedSegmentIndex == 1{
            if self.vendorPurchases.count > 0 {
                self.emptyMessage.isHidden = true
                return self.vendorPurchases.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        if segmentedControl.selectedSegmentIndex == 2{
            if self.vendorReturns.count > 0 {
                self.emptyMessage.isHidden = true
                return self.vendorReturns.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if segmentedControl.selectedSegmentIndex == 0{
            let cell : VendorPayableHistoryCell = tableView.dequeueReusableCell(withIdentifier: VendorPayableHistoryCell.identifier, for: indexPath) as! VendorPayableHistoryCell
            cell.selectionStyle = .none
            if self.vendorPayables.count > 0 {
                let item = self.vendorPayables[indexPath.row]
                cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_yy_h_mm_a.rawValue)
                cell.dateLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                
                cell.amountLabel.text = "দেনা: " + "\(item.amount)" + " " + Constants.currencySymbol
                cell.buyerLabel.text = "ক্রেতা: " + "\(item.id)"
                if item.type == 0 {
                    cell.payableLabel.text = "দেনা"
                    cell.payableLabel.textColor = UIColor(red: CGFloat(220/255.0), green: CGFloat(53/255.0), blue: CGFloat(69/255.0), alpha: CGFloat(1.0))
                    cell.typeView.backgroundColor = UIColor(red: CGFloat(255/255.0), green: CGFloat(225/255.0), blue: CGFloat(228/255.0), alpha: CGFloat(1.0))
                }
                else if item.type == 1 {
                    cell.payableLabel.text = "পূরণ"
                    cell.payableLabel.textColor = UIColor(red: CGFloat(91/255.0), green: CGFloat(160/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1.0))
                    cell.typeView.backgroundColor = UIColor(red: CGFloat(241/255.0), green: CGFloat(255/255.0), blue: CGFloat(222/255.0), alpha: CGFloat(1.0))
                }
                guard let id = self.vendorId else{
                    return cell
                }
                if isLoading == false && indexPath.row == self.vendorPayables.count - 1 && self.currentPage < self.lastPageNo {
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getVendorsPayableHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
                }
                return cell
            }

        }
        else if segmentedControl.selectedSegmentIndex == 1{
            let cell : VendorPurchaseHistoryCell = tableView.dequeueReusableCell(withIdentifier: VendorPurchaseHistoryCell.identifier, for: indexPath) as! VendorPurchaseHistoryCell
            cell.selectionStyle = .none
            if self.vendorPurchases.count > 0 {
                let item = self.vendorPurchases[indexPath.row]
                cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.dateLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.invoiceLabel.text = item.invoice
                cell.totalLabel.text = "সর্বমোটঃ " + "\(item.total)" + " " + Constants.currencySymbol
                cell.paidLabel.text = "পরিশোধঃ " + "\(item.paid)" + " " + Constants.currencySymbol
                cell.payableLabel.text = "দেনাঃ " + "\(item.payable)" + " " + Constants.currencySymbol
                
                guard let id = self.vendorId else{
                    return cell
                }
                if isLoading == false && indexPath.row == self.vendorPurchases.count - 1 && self.currentPage < self.lastPageNo{
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getVendorsPurchaseHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
                }
            }
            return cell
        }
        else if segmentedControl.selectedSegmentIndex == 2{
            let cell : VendorReturnsCell = tableView.dequeueReusableCell(withIdentifier: VendorReturnsCell.identifier, for: indexPath) as! VendorReturnsCell
            cell.selectionStyle = .none
            if self.vendorReturns.count > 0 {
                let item = self.vendorReturns[indexPath.row]
                cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.dateLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.sellerLabel.text = "ক্রেতাঃ " + item.name
                cell.invoiceLabel.text = item.invoice
                cell.totalLabel.text = "সর্বমোটঃ " + "\(item.total)" + " " + Constants.currencySymbol
                
                guard let id = self.vendorId else{
                    return cell
                }
                if isLoading == false && indexPath.row == self.vendorReturns.count - 1 && self.currentPage < self.lastPageNo{
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getVendorsPurchaseReturnHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if segmentedControl.selectedSegmentIndex == 0 {
            return 85.0
        }
        else if segmentedControl.selectedSegmentIndex == 1 {
            return 88.0
        }
        else if segmentedControl.selectedSegmentIndex == 2 {
            return 75.0
        }
        else{
            return 0
        }
    }
}

extension VendorDetailsViewController: UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

//MARK: Api Delegate
extension VendorDetailsViewController : VendorsDetailsViewDelegate{ 
    func setVendorsPayableHistoryData(data: VendorPayableHistoryDataMapper) {
        guard let vendorPayable = data.payables, vendorPayable.count > 0 else{
            return
        }
        self.isLoading = false
        self.segmentedControl.selectedSegmentIndex = 0
        self.vendorPayables += vendorPayable
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setVendorsPurchaseHistoryData(data: VendorPurchaseHistoryDataMapper) {
        guard let vendorPurchase = data.purchases, vendorPurchase.count > 0 else {
            return
        }
        self.isLoading = false
        self.vendorPurchases += vendorPurchase
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setVendorsPurchaseReturnHistoryData(data: VendorPurchaseReturnHistoryDataMapper) {
        guard let vendorReturn = data.returns, vendorReturn.count > 0 else {
            return
        }
        self.isLoading = false
        self.vendorReturns += vendorReturn
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setVendorsDetailsData(data: VendorsDetailsDataMapper) {
   
    }
    
    func onFailed(data: String) {
        self.showAlert(title: "দুঃখিত", message: "কোন তথ্য পাওয়া যায়নি")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        guard let id = self.vendorId else{
            return
        }
        self.presenter.getVendorsPayableHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
    }
}
