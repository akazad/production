//
//  VendorAddViewController.swift
//  Ponno
//
//  Created by a k azad on 5/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class VendorAddViewController: UIViewController {

    @IBOutlet weak var vendorNameLabel: UILabel!
    @IBOutlet weak var vendorNameTextField: UITextField!
    @IBOutlet weak var vendorPhoneLabel: UILabel!
    @IBOutlet weak var vendorPhoneTextField: UITextField!
    @IBOutlet weak var vendorAddressLabel: UILabel!
    @IBOutlet weak var vendorAddressTextField: UITextField!
    @IBOutlet weak var vendorSubmitButtonLabel: UIButton!
    @IBOutlet weak var vendorReloadButtonLabel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func vendorSubmitButton(_ sender: UIButton) {
    }
    

}
