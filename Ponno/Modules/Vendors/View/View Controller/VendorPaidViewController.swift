//
//  VendorPaidViewController.swift
//  Ponno
//
//  Created by a k azad on 28/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class VendorPaidViewController: UIViewController {
    
    @IBOutlet weak var dealerLbl: UILabel!
    @IBOutlet weak var dealerTextField: UITextField!{
        didSet{
            dealerTextField.placeholder = LanguageManager.SelectOne
        }
    }
    @IBOutlet weak var currentDueLbl: UILabel!
    @IBOutlet weak var currentDueTextField: UITextField!{
        didSet{
            currentDueTextField.placeholder = LanguageManager.CurrentPayable
        }
    }
    @IBOutlet weak var duePaidLbl: UILabel!
    @IBOutlet weak var duePaidTextField: UITextField!{
        didSet{
            duePaidTextField.placeholder = LanguageManager.DuePaid
        }
    }
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var detailsTextField: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = PayablePaidPresenter(service: PayableService())
    
    var vendorId : Int?
    var type : Int = 1
    var payableId : Int?
    var vendorDetails : VendorDetails?
    var currentPayable : Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetUp(){
        self.dealerLbl.text = LanguageManager.Vendor
        self.currentDueLbl.text = LanguageManager.CurrentPayable
        self.currentDueTextField.isEnabled = true
        self.duePaidLbl.text = LanguageManager.PaidAmount
        self.detailsLbl.text = LanguageManager.Description
        self.title = LanguageManager.PayableWithdraw
        self.dealerTextField.underlined()
        self.currentDueTextField.underlined()
        self.duePaidTextField.underlined()
//        if payableState == PayableState.list.rawValue{
//            guard let list = payableList else{
//                return
//            }
//            self.dealerTextField.text = list.name
//            self.currentDueTextField.text = list.currentPayable
//        }else{
//            guard let data = payableData else {
//                return
//            }
//
//        }
        guard let data = vendorDetails else{
            return
        }
        //self.payableId = data.id
//        self.vendorId = Int(data.vendorId)
        self.vendorId = data.id
        self.dealerTextField.text = data.name
        self.dealerTextField.isEnabled = false
        self.currentDueTextField.text = "\(data.currentPayable)"
        self.currentDueTextField.isEnabled = false
        self.duePaidTextField.text = ""
        self.currentPayable = Double(data.currentPayable)
        
    }
    
    @IBAction func submitButton(_ sender: UIButton) {
        if isValidated(){
            
            guard let id = self.vendorId, let amount = duePaidTextField.text, let description = detailsTextField.text else {
                return
            }
            let payablePaidData : [String: Any] = ["vendor_id" : id, "type" : self.type, "amount" : amount, "description" : description]
            
            
            
//            if let id = self.payableId{
//                payablePaidData["id"] = id
//            }
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: payablePaidData, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            self.presenter.postPayablePaidDataToServer(data: payablePaidData)
        }
    }
    
    //    func toolbarSetUp(){
    //
    //        //PayableAmountToolBar
    //        let toolBar = UIToolbar()
    //        toolBar.barStyle = UIBarStyle.default
    //        toolBar.isTranslucent = true
    //        toolBar.tintColor = UIColor.black
    //        toolBar.sizeToFit()
    //
    //        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
    //
    //        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
    //
    //        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDonePaidAmount(sender:)))
    //
    //        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
    //        toolBar.isUserInteractionEnabled = true
    //
    //        self.dealerTextField.inputView = pickerView
    //        self.dealerTextField.inputAccessoryView = toolBar
    //    }
    //
    //    @objc func onPressingDonePaidAmount(sender: UIBarButtonItem){
    //        self.duePaidTextField.resignFirstResponder()
    //        self.detailsTextField.becomeFirstResponder()
    //    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
//        if (self.duePaidTextField.text?.isEmpty)!{
//            self.displayMessage(userMessage: "Please enter an amount")
//            return false
//        }else {
//            return true
//        }
        
        guard let paidAmountText = self.duePaidTextField.text, let paidAmount = Double(paidAmountText), let currentPayable = self.currentPayable else{
            self.displayMessage(userMessage: LanguageManager.PaidAmountIsRequired)
            return false
        }
        
        if currentPayable < paidAmount {
            self.displayMessage(userMessage: LanguageManager.PaidAmountIsGreaterThanCurrentPayable)
            return false
        }
        return true
    } 
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                //self.popToSpecificViewController()
                self.navigateToPayableViewController()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is VendorsViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func navigateToPayableViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PayableViewController") as! PayableViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: API Delegate
extension VendorPaidViewController: PayablePaidViewDelegate{
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) { 
        self.displayMessage(userMessage: data)
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
    
}
