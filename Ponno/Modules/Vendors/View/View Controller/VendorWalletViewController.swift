//
//  VendorWalletViewController.swift
//  Ponno
//
//  Created by a k azad on 28/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class VendorWalletViewController: UIViewController {
    
    @IBOutlet weak var cashInitialInputTextField: UITextField!{
        didSet{
            cashInitialInputTextField.placeholder = LanguageManager.CashInToVendorWallet
        }
    }
    @IBOutlet weak var cashInitialInputHeight: NSLayoutConstraint!
    @IBOutlet weak var cashInitialSubmitBtn: UIButton!
    @IBOutlet weak var cashInitialSubmitBtnHeight: NSLayoutConstraint!
    
    @IBOutlet weak var cashLbl: UILabel!
    @IBOutlet weak var cashLblHeight: NSLayoutConstraint!
    @IBOutlet weak var cashInBtn: UIButton!{
        didSet{
            cashInBtn.setTitle("+ " + LanguageManager.CashIn, for: .normal)
        }
    }
    @IBOutlet weak var cashInBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var cashOutBtn: UIButton!{
        didSet{
            cashOutBtn.setTitle("- " + LanguageManager.CashOut, for: .normal)
        }
    }
    @IBOutlet weak var cashOutBtnHeight: NSLayoutConstraint!
    
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    
    @IBOutlet weak var noInfoMessageLbl: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var presenter = VendorWalletPresenter(service: VendorsService())
    
    var vendorWallet : VendorWallet?
    var walletTransaction : [VendorWalletTransactions] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var vendorId: Int?
    var walltetAmount : Double?
    
    var datePicker = UIDatePicker()
    var currentPage : Int = 1
    var lastPageNo : Int = 0
    var isLoading : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.attributeSetUp()
        self.setUpInitialLanguage()
        self.showStartDatePicker()
        self.showEndDatePicker()
        self.configureTableView()
        self.cashInititalIsHidden(hidden: true)
        self.cashIsHidden(is: true)
        self.hideDateSearch(is: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        noInfoMessageLbl.text = LanguageManager.NoInformationFound
        self.attachPresenter()
    }
    
    func attributeSetUp(){
        
        self.cashInBtn.addTarget(self, action: #selector(onCashInBtnTapped), for: .touchUpInside)
        self.cashOutBtn.addTarget(self, action: #selector(onCashOutBtnTapped), for: .touchUpInside)
        
        self.searchBtn.addTarget(self, action: #selector(onSearchBtnTapped), for: .touchUpInside)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func initialSetup(){
        
        self.title = LanguageManager.VendorWallet
        
        if self.vendorWallet == nil{
            self.cashInititalIsHidden(hidden: false)
            self.cashIsHidden(is: true)
            self.hideDateSearch(is: true)
            
            self.cashInitialSubmitBtn.addTarget(self, action: #selector(onInitialSubmitBtnPressed), for: .touchUpInside)
            
        }else{
            self.cashInititalIsHidden(hidden: true)
            self.cashIsHidden(is: false)
            self.hideDateSearch(is: false)
            
            if let cash = self.vendorWallet  {
                let amount = cash.amount
                if amount.isEmpty{
                    self.cashLbl.text = "0.00" + Constants.currencySymbol
                }else{
                    self.cashLbl.text = LanguageManager.Cash + " : " +  amount + " " + Constants.currencySymbol
                    self.walltetAmount = amount.toDouble()
                }
            }
        }
        
    }
    
    @objc func onInitialSubmitBtnPressed(sender: UIButton){
        if isValidated(){
            if let cashInText = self.cashInitialInputTextField.text, let cashIn = Double(cashInText) {
                if let id = self.vendorId{
                    let param : [String : Any] = ["amount": cashIn, "vendor_id": id]
                    self.presenter.postVendorWalletCashInDataToServer(param: param)
                }
                
            }
            
        }
        
    }
    
    func isValidated()->Bool{
        if (self.cashInitialInputTextField.text?.isEmpty)! {
            self.showAlert(title: LanguageManager.CashIsRequired, message: "")
            return false
        }
        return true
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.cashInitialSubmitBtn.isEnabled = isEnabled
    }
    
    @objc func onCashInBtnTapped(sender: UIButton){
        let type = 1
        let title = LanguageManager.CashIn
        
        self.alertWithTextField(title: title, type: type)
    }
    
    @objc func onCashOutBtnTapped(sender: UIButton){
        let type = 0
        let title = LanguageManager.CashOut
        
        self.alertWithTextField(title: title, type: type)
    }
    
    func cashIsHidden(is hidden: Bool){
        self.cashLbl.isHidden = hidden
        self.cashInBtn.isHidden = hidden
        self.cashOutBtn.isHidden = hidden
    }
    
    func cashInititalIsHidden(hidden: Bool){
        self.cashInitialInputTextField.isHidden = hidden
        self.cashInitialSubmitBtn.isHidden = hidden
    }
    
    func hideDateSearch(is hidden: Bool){
        self.datePickerTextField.isHidden = hidden
        self.toDatePickerTextField.isHidden = hidden
        self.searchBtn.isHidden = hidden
    }
    
}

//Mark : TableView Delegate and Data Source
extension VendorWalletViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(WalletCell.nib, forCellReuseIdentifier: WalletCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.tableView.estimatedRowHeight = 60.0
        self.automaticallyAdjustsScrollViewInsets = false
        self.noInfoMessageLbl.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.walletTransaction.count > 0{
            self.noInfoMessageLbl.isHidden = true
            return self.walletTransaction.count
        }else{
            self.noInfoMessageLbl.isHidden = false
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : WalletCell = tableView.dequeueReusableCell(withIdentifier: WalletCell.identifier, for: indexPath) as! WalletCell
        cell.selectionStyle = .none
        
        if self.walletTransaction.count > 0 {
            let item = self.walletTransaction[indexPath.row]
            
            cell.vendorWalletTransaction = item
            
            cell.popUpMenuBtn.tag = item.id
            cell.popUpMenuBtn.addTarget(self, action: #selector(onPopUpMenuPressed), for: .touchUpInside)
            
            let description = item.description
            if description.isEmpty{
                cell.descriptionImageView.isHidden = true
            }else{
                cell.descriptionImageView.addTapGestureRecognizer{
                    self.showAlert(title: LanguageManager.Description, message: description)
                }
            }
            
            if isLoading == false && indexPath.row == self.walletTransaction
                .count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                if let id = self.vendorId{
                    self.presenter.getVendorWalletDataFromServer(page: self.currentPage, id: id)
                }
            }
            
        }
        return cell
    }
    
    @objc func onDescriptionImageViewTapped(sender: UIImageView){
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopUpMenuPressed(sender: UIButton){
        
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        let id = sender.tag
        if self.walletTransaction.count > 0 {
            for item in self.walletTransaction{
                let purchaseId = item.purchaseId
                if id == item.id{
                    if purchaseId == nil{
                        let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                            self.navigateToCashTransactionUpdateVC(item: item)
                        }
                        
                        let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                            self.transactionDeleteAlert(id: id)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(updateAction)
                        myActionSheet.addAction(deleteAction)
                        myActionSheet.addAction(cancelAction)
                        
                    }else{
                        let invoiceAction = UIAlertAction(title: LanguageManager.PurchaseInvoice + " " + item.purchaseInvoice, style: UIAlertAction.Style.default)  { (action) in
                            if let id = purchaseId{
                                self.navigateToPurchaseDetailsVC(selectedId : id)
                            }
                            
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(invoiceAction)
                        myActionSheet.addAction(cancelAction)
                    }
                }
            }
            
            popOverForIpad(action: myActionSheet)
            
            self.present(myActionSheet, animated: true, completion: nil)
        }
        
    }
    
    
    func navigateToCashTransactionUpdateVC(item : VendorWalletTransactions){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorWalletTransactionUpdateVC") as! VendorWalletTransactionUpdateVC
        viewController.walletTransactions = item
        viewController.walletAmount = self.walltetAmount
        //viewController.customerWalletVCInstance = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchaseDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseDetailsViewController") as! PurchaseDetailsViewController
        viewController.selectedId = selectedId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//DatePicker
extension VendorWalletViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isSearchValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSearchBtnTapped(sender: UIButton){
        if isSearchValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            self.walletTransaction = []
            if let id = self.vendorId{
                self.presenter.getVendorWalletSearchDataFromServer(page: self.currentPage, id: id, startDt: startDate, endDt: endDate)
            }
            
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

//Mark: Api Delegate
extension VendorWalletViewController : VendorWalletViewDelegate{
    func setWalletData(data: VendorWalletDataMapper) {
        guard let wallet = data.vendorWallet else {
            self.initialSetup()
            return
        }
        
        self.vendorWallet = wallet
        
        self.initialSetup()
        
        guard let transactions = data.transactions, transactions.count > 0 else {
            self.datePickerTextField.text = ""
            self.toDatePickerTextField.text = ""
            self.dateSearchFailedMessage(userMessage: LanguageManager.NoInformationFound)
            //self.showAlert(title: LanguageManager.NoInformationFound, message: "")
            return
        }
        self.isLoading = false
        self.walletTransaction += transactions
        self.noInfoMessageLbl.isHidden = true
        self.tableView.isHidden = false
        
        guard let pagination = data.pagination else{
            return
        }
        
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onCashAdd(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onCashInOut(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onTransactionDelete(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onCashAddFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func onDateSearchFailed(message: String) {
        self.datePickerTextField.text = ""
        self.toDatePickerTextField.text = ""
        self.dateSearchFailedMessage(userMessage: LanguageManager.NoInformationFound)
    }
    
    func onFailed(data: String) {
        //self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.noInfoMessageLbl.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.noInfoMessageLbl.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.currentPage = 1
        self.isLoading = true
        self.walletTransaction = []
        if let id = self.vendorId{
            self.presenter.getVendorWalletDataFromServer(page: self.currentPage, id: id)
        }
        
    }
}

extension VendorWalletViewController{
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.walletTransaction = []
            if let id = self.vendorId{
                self.presenter.getVendorWalletDataFromServer(page: self.currentPage, id: id)
            }
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func dateSearchFailedMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: userMessage, message: "", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.walletTransaction = []
            if let id = self.vendorId{
                self.presenter.getVendorWalletDataFromServer(page: self.currentPage, id: id)
            }
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func transactionDeleteAlert(id: Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: LanguageManager.AreYouSure, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            let param : [String : Any] = ["id": id]
            self.presenter.postVendorWalletTransactionDeleteDataToServer(param: param)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alertWithTextField(title: String, type: Int){
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.CashIsRequired, message: "")
                return
            }
            
            if type == 0{
                if let data = self.vendorWallet{
                    if let cashOutAmount = text.toDouble(), let amount = data.amount.toDouble(){
                        if cashOutAmount > amount{
                            self.showAlert(title: Constants.currencySymbol + " \(amount)", message: LanguageManager.YouCanNotCashOutMoreThan)
                        }
                    }
                }
            }
            
            let textField1 = alertController.textFields![1]
            let description = textField1.text ?? ""
            
            if let id = self.vendorId{
                let param : [String : Any] = ["amount": textField.text!, "type": type, "description": description, "vendor_id": id]
                self.presenter.postVendorWalletCashInOutDataToServer(param: param)
            }
             
        }
        
        confirmAction.isEnabled = false
        
        alertController.addTextField { textField0 in
            textField0.placeholder = LanguageManager.Cash
            textField0.textAlignment = .center
            textField0.keyboardType = .decimalPad
            
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField0, queue: OperationQueue.main, using:
                {_ in
                    let textCount = textField0.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0
                    let textIsNotEmpty = textCount > 0
                    
                    confirmAction.isEnabled = textIsNotEmpty
                
            })
            
        }
        
        alertController.addTextField { textField1 in
            textField1.placeholder = LanguageManager.Description
            textField1.textAlignment = .center
            textField1.keyboardType = .default
            
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}
