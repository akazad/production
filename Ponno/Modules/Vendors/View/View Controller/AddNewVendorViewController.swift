//
//  AddNewVendorViewController.swift
//  Ponno
//
//  Created by a k azad on 7/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit
import Floaty
import SDWebImage

enum VendorState : Int {
    case Add
    case Update
    case PayableUpdate
}

class AddNewVendorViewController: UIViewController {
    
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var vendorNameLabel: UILabel!
    @IBOutlet weak var vendorNameTextField: UITextField!{
        didSet{
            vendorNameTextField.placeholder = LanguageManager.VendorName
        }
    }
    @IBOutlet weak var vendorPhoneLabel: UILabel!
    @IBOutlet weak var vendorPhoneTextField: UITextField!{
        didSet{
            vendorPhoneTextField.placeholder = LanguageManager.PhoneNumber
        }
    }
    @IBOutlet weak var vendorAddressLabel: UILabel!
    @IBOutlet weak var vendorAddressTextField: UITextField!{
        didSet{
            vendorAddressTextField.placeholder = LanguageManager.Address
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = AddNewVendorPresenter(service: VendorsService())
    
    var vendors : VendorsList?
    var vendorDetails : VendorDetails?
    var vendorState = VendorState.Add.rawValue
    var vendorId : Int?
    var payableVendor : PayableList?
    
    let imagePicker = UIImagePickerController()
    var selectedImage : UIImage?
    var message : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setVendorTitles()
        self.configureTextFields()
        self.initialSetUp()
        self.setImageView()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }

    func initialSetUp(){
        if vendorState == VendorState.Update.rawValue {
            guard let vendor = vendors else {
                return
            }
            self.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.vendorImage + vendor.image), placeholderImage: UIImage(named: "placeholder"))
            self.vendorNameTextField.text = vendor.name
            self.vendorPhoneTextField.text = vendor.phone
            self.vendorAddressTextField.text = vendor.address
            self.vendorId = vendor.id
        }else if vendorState == VendorState.PayableUpdate.rawValue{
            guard let payble = self.payableVendor else{
                return
            }
            self.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.vendorImage + payble.image), placeholderImage: UIImage(named: "placeholder"))
            self.vendorNameTextField.text = payble.name
            self.vendorPhoneTextField.text = payble.phone
            self.vendorAddressTextField.text = payble.address
            self.vendorId = payble.id
        }
        
        imagePicker.delegate = self
        self.imageIcon.addTapGestureRecognizer(action: {self.onTapOnImageView()})
    }
    
    @IBAction func vendorSubmitButton(_ sender: UIButton) {
        if self.isValidated(){
            
            guard let nameText = self.vendorNameTextField.text, let phoneText = self.vendorPhoneTextField.text else{
                return
            }
            
            let addressText = self.vendorAddressTextField.text ?? ""
            
            var params: [String: String] = ["name": nameText, "phone": phoneText, "address": addressText]
            
            if self.vendorState == VendorState.Add.rawValue{
                self.presenter.postAddNewVendorDataToServer(vendorData: params)
            }else if self.vendorState == VendorState.Update.rawValue{
                if let id = self.vendorId{
                    params["id"] = "\(id)"
                }
                self.presenter.updateVendorData(params: params)
            }else{
                if let id = self.vendorId{
                    params["id"] = "\(id)"
                }
                self.presenter.updateVendorData(params: params)
            }
        }
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func setVendorTitles(){
        
        if self.vendorState == VendorState.Add.rawValue{
            self.title = LanguageManager.VendorInfo
        }else{
            self.title = LanguageManager.VendorInfoUpdate
        }
        self.vendorNameLabel.text = LanguageManager.VendorName
        self.vendorAddressLabel.text = LanguageManager.Address
        self.vendorPhoneLabel.text = LanguageManager.PhoneNumber
        self.toolbarSetUp()
    }
    
    func isValidated()->Bool{
        if (self.vendorNameTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.VendorNameIsRequired)
            return false
        }else if (self.vendorPhoneTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
        }
        
        guard let phone = vendorPhoneTextField.text else {
            return false
        }
        if (!validatePhoneNumber(value: phone)){
            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
            return false
        }
//        if(self.vendorAddressTextField.text?.isEmpty)!{
//            self.displayMessage(userMessage: "Please enter an address")
//            return false
//        }
        return true
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)

    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                //self.popToSpecificViewController()
                self.navigateToVendorsViewController()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is VendorsViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func navigateToVendorsViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorsViewController") as! VendorsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToVendorAddViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewVendorViewController") as! AddNewVendorViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func toolbarSetUp(){
        
        //vendorPhoneToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDonePhone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.vendorPhoneTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDonePhone(sender: UIBarButtonItem){
        self.vendorPhoneTextField.resignFirstResponder()
        self.vendorAddressTextField.becomeFirstResponder()
    }
    
}

//Mark: TextField Delegate
extension AddNewVendorViewController : UITextFieldDelegate{
    
    fileprivate func configureTextFields(){
        self.vendorNameTextField.delegate = self
//        self.vendorPhoneTextField.delegate = self
        self.vendorAddressTextField.delegate = self
        self.vendorNameTextField.underlined()
        self.vendorPhoneTextField.underlined()
        self.vendorAddressTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.vendorNameTextField{
            self.vendorPhoneTextField.becomeFirstResponder()
        }
        else{
            self.view.endEditing(true)
        }
        return false
    }
}

//Mark: API Delegate
extension AddNewVendorViewController: AddNewVendorViewDelegate{
    func updateVendorData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.message = message
        guard let id = self.vendorId, let image = self.selectedImage else{
            self.successMessage(userMessage: message)
            return
        }
        self.presenter.uploadImage(vendorId : id, image: image)
    }
    
    func setAddNewVendorData(vendorData: VendorAddDataMapper) {
        guard let message = vendorData.message, let vendorId = vendorData.vendor?.id else {
            return
        }
        self.message = message
        guard let image = self.selectedImage else{
            self.successMessage(userMessage: message)
            return
        }
        self.presenter.uploadImage(vendorId: vendorId, image: image)   
    }
    
    func onImageUpload(data: AddDataMapper) {
        self.successMessage(userMessage: self.message ?? "")
    }
    
    func onFailed(data: String) {
        self.displayMessage(userMessage: data)
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

//extension UITextField {
//
//    func underlined(){
//        let border = CALayer()
//        let width = CGFloat(1.0)
//        border.borderColor = UIColor.lightGray.cgColor
//        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
//        border.borderWidth = width
//        self.layer.addSublayer(border)
//        self.layer.masksToBounds = true
//    }
//}

//Mark: Image Picker
extension AddNewVendorViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    private func setImageView(){
        guard let imageUrl = self.vendors?.image else{
            self.setImageInImageView(imageUrl: "")
            return
        }
        
        self.setImageInImageView(imageUrl: ImageUrl.sharedImageInstance.vendorImage + imageUrl)
    }
    
    func setImageInImageView(imageUrl : String){
        self.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageIcon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
    }
    
    func onTapOnImageView(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageIcon.contentMode = .scaleAspectFill
            imageIcon.image = pickedImage
            self.selectedImage = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

