//
//  PayableAdjustViewController.swift
//  Ponno
//
//  Created by a k azad on 27/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PayableAdjustViewController: UIViewController {

    @IBOutlet weak var vendorNameLbl: UILabel!
    @IBOutlet weak var vendorNameTextField: UITextField!
    @IBOutlet weak var currentPayableLbl: UILabel!
    @IBOutlet weak var currentPayableTextField: UITextField!
    @IBOutlet weak var payableAdjustAmountLbl: UILabel!
    @IBOutlet weak var payableAdjustAmountTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptonTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = PayableAdjustPresenter(service: VendorsService())
    
    var type = Type.add
    var payableAdjust : PayableAdjustList?
    var vendorId: Int?
    var vendor: VendorDetails?
    var payableAdjustId : Int?
    var payableAdjustAmount : Double?
    var currentPayable: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.configureTextFields()
        self.toolbarSetup()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.setUpInitialLanguage()
        self.vendorNameLbl.text = LanguageManager.VendorName
        self.currentPayableLbl.text = LanguageManager.CurrentPayable
        self.payableAdjustAmountLbl.text = LanguageManager.PayableAdjustAmount
        self.descriptionLbl.text = LanguageManager.Description
        self.vendorNameTextField.isEnabled = false
        self.currentPayableTextField.isEnabled = false
        if let data = self.vendor{
            self.vendorNameTextField.text = data.name
            self.vendorId = data.id
            //self.currentPayableTextField.text = data.currentPayable
        }
        
        if let cPayable = self.currentPayable{
            self.currentPayableTextField.text = "\(cPayable)"
        }
        
        if type == Type.add{
            self.title = LanguageManager.NewPayableAdjust

        }else{
            self.title = LanguageManager.PayableAdjustUpdate
            if let data = payableAdjust{
                self.payableAdjustAmountTextField.text = data.amount
                if let adjustAmount = data.amount?.toDouble(){
                    self.payableAdjustAmount = adjustAmount
                }
                self.descriptonTextField.text = data.description
                self.payableAdjustId = data.id
            }
        }
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        self.payableAdjustAmountTextField.addTarget(self, action: #selector(onPayableAdjustChange), for: .editingChanged)
    }
    
    func toolbarSetup(){
        let payableAdjustTextFieldToolBar = UIToolbar()
        payableAdjustTextFieldToolBar.barStyle = UIBarStyle.default
        payableAdjustTextFieldToolBar.isTranslucent = true
        payableAdjustTextFieldToolBar.tintColor = UIColor.black
        payableAdjustTextFieldToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnPayableAdjustAmount))
        
        payableAdjustTextFieldToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        payableAdjustTextFieldToolBar.isUserInteractionEnabled = true
        
        self.payableAdjustAmountTextField.inputAccessoryView = payableAdjustTextFieldToolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnPayableAdjustAmount(sender: UIBarButtonItem){
        self.descriptonTextField.becomeFirstResponder()
    }
    
    @objc func onPayableAdjustChange(sender: UITextField){
        if let currentPayable = self.currentPayable, let payableAdjust = self.payableAdjustAmount{
            let adjustedAmount = self.payableAdjustAmountTextField.text?.toDouble()
            if let givenAdjustedAmount = adjustedAmount{
                if givenAdjustedAmount > payableAdjust + currentPayable{
                    let total = currentPayable + payableAdjust
                    self.payableAdjustAmountTextField.text = total.toString()
                    self.showAlert(title: Constants.currencySymbol + " \(total)" , message: LanguageManager.YouCanNotAdjustMoreThan)
                }
            }
        }
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title:  LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            guard let amount = payableAdjustAmountTextField.text, let adjustAmount = amount.toDouble() else{
                return
            }
            let description = self.descriptonTextField.text ?? ""
            if type == Type.add{
                guard let id = self.vendorId else{
                    return
                }
                let param: [String : Any] = ["amount": adjustAmount, "description": description, "vendor": id]
                self.presenter.postPayableAdjustStoreDataToServer(param: param)
            }else{
                guard let id = self.payableAdjustId else{
                    return
                }
                
                let param : [String: Any] = ["id": id, "amount": adjustAmount, "description": description]
                self.presenter.postPayableAdjustUpdateDataToServer(param: param)
            }
            
        }
    }
    
    func isValidated()->Bool{
        if self.payableAdjustAmountTextField.text == "" {
            showAlert(title: LanguageManager.PayableAdjustAmountIsRequired, message: "")
            return false
        }
        
        return true
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
}

extension PayableAdjustViewController: UITextFieldDelegate{
    func configureTextFields(){
        self.vendorNameTextField.delegate = self
        self.currentPayableTextField.delegate = self
        self.payableAdjustAmountTextField.delegate = self
        self.descriptonTextField.delegate = self
        self.vendorNameTextField.underlined()
        self.currentPayableTextField.underlined()
        self.payableAdjustAmountTextField.underlined()
        self.descriptonTextField.underlined()
    }
}

extension PayableAdjustViewController : PayableAdjustViewDelegate{
    func onPayableAdjustSuccessfullyUpdate(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onPayableAdjustSuccessfullyAdd(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(failure: String) {
        self.showAlert(title: failure, message: "")
    }
    
    func showLoading() {
        submitBtnControlWith(isEnabled : false)
        self.showLoader()
    }
    
    func hideLoading() {
        submitBtnControlWith(isEnabled : true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
    
}

