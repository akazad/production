//
//  VendorDetailsBaseViewController.swift
//  Ponno
//
//  Created by a k azad on 15/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class VendorDetailsBaseViewController: UIViewController , BottomSheetDelegate {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = VendorDetailsViewPresenter(service: VendorsService())
    
    var vendorId : Int?
    var vendorDetails : VendorDetails?{
        didSet{
            self.refreshTableView()
        }
    }
    
    var currentPayable : String?
    
    enum Sections : Int{
        case VendorInfo
        case VendorSummary
    }
    
    var payableData : PayableLogList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        container.layer.cornerRadius = 15
        container.layer.masksToBounds = true
        self.configureTableView()
        self.setBarButton()
    }
    
    func setBarButton(){
        
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "edit"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onEdit(sender:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        button.widthAnchor.constraint(equalToConstant: 32).isActive = true
        button.heightAnchor.constraint(equalToConstant: 32).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func onEdit(sender: UIBarButtonItem){
        self.navigateToVendorPaidVC()
    }
    
//    func navigateToUpdateVendorVC(){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "PayablePaidViewController") as! PayablePaidViewController
//        viewController.payableData = payableData
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
    
    func navigateToVendorPaidVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorPaidViewController") as! VendorPaidViewController
        viewController.vendorDetails = vendorDetails
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? VendorDetailsViewController{
            viewController.bottomSheetDelegate = self
            viewController.vendorId = self.vendorId
            viewController.parentView = container
        }
    }
    
    func updateBottomSheet(frame: CGRect) {
        container.frame = frame
    }
   
}

//MARK: TableViewDelegate And DataSource
extension VendorDetailsBaseViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(VendorDetailsCell.nib, forCellReuseIdentifier: VendorDetailsCell.identifier)
        self.tableView.register(VendorDetailsInfoCell.nib, forCellReuseIdentifier: VendorDetailsInfoCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.VendorInfo.rawValue:
            return 1
        case Sections.VendorSummary.rawValue:
            return 1
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case Sections.VendorInfo.rawValue:
            let cell: VendorDetailsCell = tableView.dequeueReusableCell(withIdentifier: VendorDetailsCell.identifier, for: indexPath) as! VendorDetailsCell
            guard let vendorDetail = self.vendorDetails else{
                return cell
            }
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.vendorImage + vendorDetail.image), placeholderImage: UIImage(named: "placeholder"))
            cell.nameLabel.text = vendorDetail.name
            cell.phoneLabel.text = "ফোনঃ " + vendorDetail.phone
            cell.addressLabel.text = "ঠিকানাঃ " +  vendorDetail.address
            self.currentPayable = "\(vendorDetail.totalPayable)"

            return cell
            
        case Sections.VendorSummary.rawValue :
            let cell: VendorDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: VendorDetailsInfoCell.identifier, for: indexPath) as! VendorDetailsInfoCell
            
            guard let vendorDetail = self.vendorDetails else{
                return cell
            }
            cell.totalPurchaseLabel.text = "\(vendorDetail.totalPurchase)"
            cell.totalPayableLabel.text = "\(vendorDetail.totalPayable)" + " " + Constants.currencySymbol
            cell.currentPayableLabel.text = "\(vendorDetail.currentPayable)" + " " + Constants.currencySymbol
            cell.totalPaidLabel.text = "\(vendorDetail.totalPaid)" + " " + Constants.currencySymbol
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.VendorInfo.rawValue:
            return UITableView.automaticDimension
        case Sections.VendorSummary.rawValue:
            return 140.0
        default:
            return 0
        }
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
}


extension VendorDetailsBaseViewController : VendorsDetailsViewDelegate{
    
    func setVendorsDetailsData(data: VendorsDetailsDataMapper) {
        guard let details = data.vendor else {
            return
        }
        self.vendorDetails = details
        //self.tableView.isHidden = false
    }
    
    func setVendorsPayableHistoryData(data: VendorPayableHistoryDataMapper) {
        
    }
    
    func setVendorsPurchaseHistoryData(data: VendorPurchaseHistoryDataMapper) {
        
    }
    
    func setVendorsPurchaseReturnHistoryData(data: VendorPurchaseReturnHistoryDataMapper){
    
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        
        guard let id = self.vendorId else{
            return
        }
        self.presenter.getVendorsDataFromServer(id: id)
    }
    
}
