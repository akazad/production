//
//  VendorDViewController.swift
//  Ponno
//
//  Created by a k azad on 29/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

struct Model {
    var name : String
    var id : Int
    
    init(name: String, id: Int){
        self.name = name
        self.id = id
    }
}

class VendorDViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!{
        didSet{
            segmentedControl.setTitle(LanguageManager.PayableHistory, forSegmentAt: 0)
            segmentedControl.setTitle(LanguageManager.PurchaseHistory, forSegmentAt: 1)
            segmentedControl.setTitle(LanguageManager.ReturnHistory, forSegmentAt: 2)
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessage: UILabel!{
        didSet{
            emptyMessage.text = LanguageManager.NoInformationFound
        }
    }
    @IBOutlet weak var modelChangeTextFiled: UITextField!
    @IBOutlet weak var modelChangeTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var dropDownImage: UIImageView!
    
    private var presenter = VendorDetailsViewPresenter(service: VendorsService())
    
    var vendorId : Int?
    
    var vendorPayables : [VendorPayables] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var vendorPurchases : [VendorPurchases] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var vendorReturns : [VendorReturns] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var selectedSegment : Int?
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    var userName : String?
        
    var modelStatus : Bool = true
    var modelPicker = UIPickerView()
    
    var vendorModels : [String] = [LanguageManager.Inventory, LanguageManager.RawMaterial]
    
//    var vendorModels : [Model]?{
//        didSet{
//           let inventory = Model(name: "Inventory", id: 0)
//           let rawMaterial = Model(name: "Raw Material", id: 1)
//            vendorModels?.append(inventory)
//            vendorModels?.append(rawMaterial)
////            let models = [LanguageManager.Inventory, LanguageManager.RawMaterial]
////            let ids = [0,1]
////            for i in 0..<1{
////                vendorModels?.append(Model(name: models[i], id: ids[i]))
////            }
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.initialSetUp()
        self.setUpPickerView()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetUp(){
        self.segmentedControl.addTarget(self, action: #selector(onSegmentChange), for: .valueChanged)
        
        if let shop = getShopData(){
            guard let inventorySystem = shop.inventorySystem else {
                return
            }
            if inventorySystem != 2 {
                self.modelChangeTextFiled.isHidden = true
                self.modelChangeTextFieldHeight.constant = 0.01
                self.dropDownImage.isHidden = true
            }
            
        }
        
        self.modelChangeTextFiled.text = LanguageManager.Inventory
        self.modelChangeTextFiled.underlined()
        
        if let user = getUserData(){
            guard let userName = user.name else{
                return
            }
            self.userName = userName
        }
    }
    
    @objc func onSegmentChange(sender: UISegmentedControl){
        if let id = self.vendorId {
            if segmentedControl.selectedSegmentIndex == 0{
                self.isLoading = true
                self.currentPage = 1
                self.vendorPayables = []
                self.presenter.getVendorsPayableHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
            }
            else if segmentedControl.selectedSegmentIndex == 1{
                self.isLoading = true
                self.currentPage = 1
                self.vendorPurchases = []
                self.presenter.getVendorsPurchaseHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
            }
            else if segmentedControl.selectedSegmentIndex == 2{
                self.isLoading = true
                self.currentPage = 1
                self.vendorReturns = []
                self.presenter.getVendorsPurchaseReturnHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
            }
            else{
                return
            }
        }
        
    }
    
    
    func setUpPickerView(){
        modelPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        self.modelChangeTextFiled.inputView = modelPicker
        self.modelChangeTextFiled.inputAccessoryView = toolBar
        
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        if let id = self.vendorId {
            if segmentedControl.selectedSegmentIndex == 0{
                self.isLoading = true
                self.currentPage = 1
                self.vendorPayables = []
                self.presenter.getVendorsPayableHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
            }
            else if segmentedControl.selectedSegmentIndex == 1{
                self.isLoading = true
                self.currentPage = 1
                self.vendorPurchases = []
                self.presenter.getVendorsPurchaseHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
            }
            else if segmentedControl.selectedSegmentIndex == 2{
                self.isLoading = true
                self.currentPage = 1
                self.vendorReturns = []
                self.presenter.getVendorsPurchaseReturnHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
            }
            else{
                return
            }
        }
        self.view.endEditing(true)
    }
    
    @objc func onPressingCancel(sender: UITextField){
        self.view.endEditing(true)
    }

}

//Mark: PickerViewDelegate
extension VendorDViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        if let models = self.vendorModels, models.count > 0{
//            let modelName = models[row].name
//            self.modelChangeTextFiled.text = modelName
//            let id = models[row].id
//            if id == 0 {
//                self.modelStatus = true
//            }else{
//                self.modelStatus = false
//            }
//            return modelName
//        }
        
        if self.vendorModels.count > 0 {
            let list = self.vendorModels[row]
            self.modelChangeTextFiled.text = list
            if list == LanguageManager.Inventory{
                self.modelStatus = true
            }else{
                self.modelStatus = false
            }
            return list
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.vendorModels.count > 0 {
            let modelName = self.vendorModels[row]
            self.modelChangeTextFiled.text = modelName
            if modelName == LanguageManager.Inventory {
                self.modelStatus = true
            }else{
                self.modelStatus = false
            }
        }
    }
    
}

//Mark: TableView Delegate And Data Source
extension VendorDViewController: UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = .clear
        self.tableView.register(VendorPayableHistoryCell.nib, forCellReuseIdentifier: VendorPayableHistoryCell.identifier)
        self.tableView.register(VendorPurchaseHistoryCell.nib, forCellReuseIdentifier: VendorPurchaseHistoryCell.identifier)
        self.tableView.register(VendorReturnsCell.nib, forCellReuseIdentifier: VendorReturnsCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.emptyMessage.isHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl.selectedSegmentIndex == 0{
            if self.vendorPayables.count > 0 {
                self.emptyMessage.isHidden = true
                return self.vendorPayables.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        if segmentedControl.selectedSegmentIndex == 1{
            if self.vendorPurchases.count > 0 {
                self.emptyMessage.isHidden = true
                return self.vendorPurchases.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        if segmentedControl.selectedSegmentIndex == 2{
            if self.vendorReturns.count > 0 {
                self.emptyMessage.isHidden = true
                return self.vendorReturns.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if segmentedControl.selectedSegmentIndex == 0{
            let cell : VendorPayableHistoryCell = tableView.dequeueReusableCell(withIdentifier: VendorPayableHistoryCell.identifier, for: indexPath) as! VendorPayableHistoryCell
            cell.selectionStyle = .none
            if self.vendorPayables.count > 0 {
                let item = self.vendorPayables[indexPath.row]
                cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_yy_h_mm_a.rawValue)
                cell.dateLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                
                cell.amountLabel.text = LanguageManager.Payable + " : " + "\(item.amount)" + " " + Constants.currencySymbol
                
                guard let name = self.userName else{
                    return cell
                }
                
                cell.buyerLabel.text = LanguageManager.PurchaseBy + " : " + name
                
                let description = item.description
                if description == "" {
                    cell.imageIcon.isHidden = true
                }else{
                    cell.imageIcon.isHidden = false
                }
                
                cell.imageIcon.addTapGestureRecognizer {
                    self.showAlert(title: LanguageManager.Description, message: description)
                }
                
                if item.type == 0 {
                    cell.payableLabel.text = LanguageManager.Payable
                    cell.payableLabel.textColor = UIColor(red: CGFloat(220/255.0), green: CGFloat(53/255.0), blue: CGFloat(69/255.0), alpha: CGFloat(1.0))
                    
                }
                else if item.type == 1 {
                    cell.payableLabel.text = LanguageManager.Withdraw
                    cell.payableLabel.textColor = UIColor(red: CGFloat(91/255.0), green: CGFloat(160/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1.0))
                    
                }
                guard let id = self.vendorId else{
                    return cell
                }
                if isLoading == false && indexPath.row == self.vendorPayables.count - 1 && self.currentPage < self.lastPageNo {
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getVendorsPayableHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
                }
                return cell
            }
            
        }
        else if segmentedControl.selectedSegmentIndex == 1{
            let cell : VendorPurchaseHistoryCell = tableView.dequeueReusableCell(withIdentifier: VendorPurchaseHistoryCell.identifier, for: indexPath) as! VendorPurchaseHistoryCell
            cell.selectionStyle = .none
            if self.vendorPurchases.count > 0 {
                let item = self.vendorPurchases[indexPath.row]
                cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.dateLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.invoiceLabel.text = item.invoice
                cell.totalLabel.text = LanguageManager.TotalAmount + " : " + "\(item.total)" + " " + Constants.currencySymbol
                cell.paidLabel.text = LanguageManager.Paid + " : " + "\(item.paid)" + " " + Constants.currencySymbol
                cell.payableLabel.text = LanguageManager.Payable + " : " + "\(item.payable)" + " " + Constants.currencySymbol
                
                guard let id = self.vendorId else{
                    return cell
                }
                if isLoading == false && indexPath.row == self.vendorPurchases.count - 1 && self.currentPage < self.lastPageNo{
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getVendorsPurchaseHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
                }
            }
            return cell
        }
        else if segmentedControl.selectedSegmentIndex == 2{
            let cell : VendorReturnsCell = tableView.dequeueReusableCell(withIdentifier: VendorReturnsCell.identifier, for: indexPath) as! VendorReturnsCell
            cell.selectionStyle = .none
            if self.vendorReturns.count > 0 {
                let item = self.vendorReturns[indexPath.row]
                cell.colorView.backgroundColor = UIColor(red: CGFloat(232/255.0), green: CGFloat(94/255.0), blue: CGFloat(88/255.0), alpha: CGFloat(0.6))
                cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.dateLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.sellerLabel.text = LanguageManager.ReturnedBy + " : " + item.name
                cell.invoiceLabel.text = item.invoice
                cell.totalLabel.text = LanguageManager.Total + " : " + "\(item.total)" + " " + Constants.currencySymbol
                
                guard let id = self.vendorId else{
                    return cell
                }
                if isLoading == false && indexPath.row == self.vendorReturns.count - 1 && self.currentPage < self.lastPageNo{
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getVendorsPurchaseReturnHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if segmentedControl.selectedSegmentIndex == 1 {
            if self.vendorPurchases.count > 0{
                let item = self.vendorPurchases[indexPath.row]
                let id = item.id
                self.navigateToPurchaseDetailsVC(selectedId: id)
            }
        }else if segmentedControl.selectedSegmentIndex == 2 {
            if self.vendorReturns.count > 0 {
                let item = self.vendorReturns[indexPath.row]
                let id = item.id
                self.navigateToPurchaseReturnDetailsVC(selectedId: id)
            }
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if segmentedControl.selectedSegmentIndex == 0 {
            return 92.0
        }
        else if segmentedControl.selectedSegmentIndex == 1 {
            return 114.0
        }
        else if segmentedControl.selectedSegmentIndex == 2 {
            return 85.0
        }
        else{
            return 0
        }
    }
    
    func navigateToPurchaseDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseDetailsViewController") as! PurchaseDetailsViewController
        viewController.selectedId = selectedId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchaseReturnDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseReturnDetailsViewController") as! PurchaseReturnDetailsViewController
        viewController.selectedId = selectedId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//MARK: Api Delegate
extension VendorDViewController : VendorsDetailsViewDelegate{
    func setVendorsPayableHistoryData(data: VendorPayableHistoryDataMapper) {
        guard let vendorPayable = data.payables, vendorPayable.count > 0 else{
            return
        }
        self.isLoading = false
        self.segmentedControl.selectedSegmentIndex = 0
        self.vendorPayables += vendorPayable
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setVendorsPurchaseHistoryData(data: VendorPurchaseHistoryDataMapper) {
        guard let vendorPurchase = data.purchases, vendorPurchase.count > 0 else {
            return
        }
        self.isLoading = false
        self.vendorPurchases += vendorPurchase
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setVendorsPurchaseReturnHistoryData(data: VendorPurchaseReturnHistoryDataMapper) {
        guard let vendorReturn = data.returns, vendorReturn.count > 0 else {
            return
        }
        self.isLoading = false
        self.vendorReturns += vendorReturn
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setVendorsDetailsData(data: VendorsDetailsDataMapper) {
        
    }
    
    func onFailed(data: String) {
        //self.showAlert(title: "দুঃখিত", message: "কোন তথ্য পাওয়া যায়নি")
    }
    
    func showLoading() {
        //self.tableView.isHidden = true
        self.emptyMessage.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        //self.tableView.isHidden = false
        self.emptyMessage.isHidden = false
        self.hideLoader()
    }
    
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        guard let id = self.vendorId else{
            return
        }
        self.presenter.getVendorsPayableHistoryFromServer(page: self.currentPage, id: id, model: self.modelStatus)
    }
}
