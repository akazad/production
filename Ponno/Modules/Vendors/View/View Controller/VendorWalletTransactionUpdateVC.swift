//
//  VendorWalletTransactionUpdateVC.swift
//  Ponno
//
//  Created by a k azad on 28/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

import UIKit

class VendorWalletTransactionUpdateVC: UIViewController {

    @IBOutlet weak var previousCashLbl: UILabel!
    @IBOutlet weak var previousCashTextField: UITextField!
    @IBOutlet weak var newCashLbl: UILabel!
    @IBOutlet weak var newCashTextField: UITextField!
    @IBOutlet weak var descripTionLbl: UILabel!
    @IBOutlet weak var descripTionTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = CashTransactionsUpdatePresenter(service: CashDrawerService())
       
    var walletTransactions : VendorWalletTransactions?
       
    var transactionId : Int?
    var walletAmount : Double?
        
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        
        self.title = LanguageManager.TransactionUpdate
        
        guard let transaction = self.walletTransactions else{
            return
        }
        self.previousCashLbl.text = LanguageManager.PreviousCashAmount
        self.previousCashTextField.text = transaction.amount
        self.newCashLbl.text = LanguageManager.NewCashAmount
        self.newCashTextField.placeholder = LanguageManager.EnterCashAmount
        self.descripTionLbl.text = LanguageManager.Description
        self.descripTionTextField.placeholder = LanguageManager.Description
        
        self.transactionId = transaction.id
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        //self.newCashTextField.addTarget(self, action: #selector(onNewCashChange), for: .editingChanged)
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            guard let amount = self.newCashTextField.text, let id = self.transactionId else{
                return
            }
            
            let description = self.descripTionTextField.text ?? ""
            
            let param : [String : Any] = ["id" : id, "amount" : amount, "description": description]
            self.presenter.postCustomerWalletTransactionUpdateDataToServer(param: param)
        }
    }
    
//    @objc func onNewCashChange(sender: UITextField){
//        if let newCashAmount = self.newCashTextField.text?.toDouble(){
//            if let previousAmount = self.previousCashTextField.text?.toDouble(), let walletAmount = self.walletAmount{
//                if previousAmount + walletAmount < newCashAmount{
//                    let total = previousAmount + walletAmount
//                    self.showAlert(title: " \(total)", message: LanguageManager.YouCanNotUpdateMoreThan)
//                    self.newCashTextField.text = "\(total)"
//                }
//            }
//        }
//    }
    
    func isValidated()->Bool{
        if (self.newCashTextField.text?.isEmpty)! {
            self.showAlert(title: LanguageManager.CashIsRequired, message: "")
            return false
        }
        if self.checkNewCashAmount(textField: self.newCashTextField) == false{
            return false
        }
        return true
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func checkNewCashAmount(textField: UITextField) -> Bool{
        guard let transaction = self.walletTransactions else{
            return false
        }
        if transaction.type == 0{
            if let newCashAmount = textField.text?.toDouble(){
                if let previousAmount = self.previousCashTextField.text?.toDouble(), let walletAmount = self.walletAmount{
                    if previousAmount + walletAmount < newCashAmount{
                        let total = previousAmount + walletAmount
                        self.showAlert(title: " \(total)", message: LanguageManager.YouCanNotUpdateMoreThan)
                        self.newCashTextField.text = "\(total)"
                        return false
                    }
                }
            }
        }else{
            if let newCashAmount = textField.text?.toDouble(){
                if let previousAmount = self.previousCashTextField.text?.toDouble(), let walletAmount = self.walletAmount{
                    var total = 0.0
                    var minAmount = 0.0
                    minAmount = previousAmount - walletAmount
                    if minAmount > 0{
                        total = minAmount
                    }else{
                        total = 0
                    }
                    if total > newCashAmount{
                        self.showAlert(title: " \(total)", message: LanguageManager.YouCanNotUpdateLessThan)
                        self.newCashTextField.text = "\(total)"
                        return false
                    }
                }
            }
        }
        return true
    }

}

//Mark: Api Delegate
extension VendorWalletTransactionUpdateVC : CashTransactionsUpdateViewDelegate{
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

extension VendorWalletTransactionUpdateVC {
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
