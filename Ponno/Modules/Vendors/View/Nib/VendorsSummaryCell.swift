//
//  VendorsSummaryCell.swift
//  Ponno
//
//  Created by a k azad on 15/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class VendorsSummaryCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: VendorsSummaryCell.self)
    }
}
