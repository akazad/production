//
//  VendorDetailsInfoCell.swift
//  Ponno
//
//  Created by a k azad on 20/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class VendorDetailsInfoCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var totalPurchaseLabel: UILabel!
    @IBOutlet weak var totalPurchaseTitleLbl: UILabel!{
        didSet{
            totalPurchaseTitleLbl.text = LanguageManager.TotalPurchase
        }
    }
    @IBOutlet weak var totalPayableLabel: UILabel!
    @IBOutlet weak var totalPayableTitleLbl: UILabel!{
        didSet{
            totalPayableTitleLbl.text = LanguageManager.TotalPayable
        }
    }
    @IBOutlet weak var currentPayableLabel: UILabel!
    @IBOutlet weak var currentPayableTitleLbl: UILabel!{
        didSet{
            currentPayableTitleLbl.text = LanguageManager.CurrentPayable
        }
    }
    @IBOutlet weak var totalPaidLabel: UILabel!
    @IBOutlet weak var totalPaidTitleLbl: UILabel!{
        didSet{
            totalPaidTitleLbl.text = LanguageManager.TotalPayable
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: VendorDetailsInfoCell.self)
    }
}
