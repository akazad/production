//
//  VendorsListCell.swift
//  Ponno
//
//  Created by a k azad on 29/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class VendorsListCell: UITableViewCell {
    
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet var imageIcon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var popUpBtn: UIButton!
    
    var vendorList : VendorsList?{
        didSet{
            if let vendors = vendorList{
                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray 
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.vendorImage + vendors.image), placeholderImage: UIImage(named: "placeholder"))
                nameLabel.text = LanguageManager.VendorName + " : " + vendors.name
                
                let phone = vendors.phone
                if phone == "" {
                    phoneLabel.text = LanguageManager.Phone + " : " + "---"
                }else{
                    phoneLabel.text = LanguageManager.Phone + " : " +  vendors.phone
                }
                
                let address = vendors.address
                if address == "" {
                     addressLabel.text = LanguageManager.Address + " : " +  "---"
                }else{
                    addressLabel.text = LanguageManager.Address + " : " +  vendors.address
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: VendorsListCell.self)
    }
    
}
