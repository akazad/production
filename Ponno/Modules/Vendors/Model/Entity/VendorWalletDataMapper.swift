//
//  VendorWalletDataMapper.swift
//  Ponno
//
//  Created by a k azad on 28/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class VendorWalletDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var vendorWallet : VendorWallet? 
    var transactions : [VendorWalletTransactions]?
    var pagination : Pagination?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        vendorWallet <- map["vendor_wallet"]
        transactions <- map["transactions"]
        pagination <- map["pagination"]
    }

}
