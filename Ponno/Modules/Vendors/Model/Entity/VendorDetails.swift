//
//  VendorDetails.swift
//  Ponno
//
//  Created by a k azad on 17/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class VendorDetails : Mappable {
    var id : Int = 0
    var name : String = ""
    var phone : String = ""
    var address : String = ""
    var image : String = ""
    var totalPurchase : Int = 0
    var currentPayable : String = ""
    var totalPayable : String = ""
    var totalPaid : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        address <- map["address"]
        image <- map["image"]
        totalPurchase <- map["total_purchase"]
        currentPayable <- map["current_payable"]
        totalPayable <- map["total_payable"]
        totalPaid <- map["total_paid"]
    }
    
}

