//
//  VendorPayables.swift
//  Ponno
//
//  Created by a k azad on 17/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class VendorPayables : Mappable {
    var id : Int = 0
    var type : Int = 0
    var createdAt : String = ""
    var amount : String = ""
    var description : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        type <- map["type"]
        amount <- map["amount"]
        createdAt <- map["created_at"]
        description <- map["description"]
    }
    
}

