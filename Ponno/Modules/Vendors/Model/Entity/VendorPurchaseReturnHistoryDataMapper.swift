//
//  VendorPurchaseReturnHistoryDataMapper.swift
//  Ponno
//
//  Created by a k azad on 17/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class VendorPurchaseReturnHistoryDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var returns : [VendorReturns]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        returns <- map["returns"]
        pagination <- map["pagination"]
    }
    
}
