//
//  VendorsDataMapper.swift
//  Ponno
//
//  Created by a k azad on 27/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class VendorsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [VendorsSummary]?
    var vendors : [VendorsList]?
    var pagination : Pagination?
    
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        vendors <- map["vendors"]
        pagination <- map["pagination"]
    }
    
}

