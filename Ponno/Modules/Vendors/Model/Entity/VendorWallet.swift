//
//  VendorWallet.swift
//  Ponno
//
//  Created by a k azad on 28/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class VendorWallet : Mappable {
    var id : Int = 0
    var amount : String = ""
    var customerId : Int = 0
    var pharmacyId : Int = 0
    var createdAt : String = ""
    var updatedAt : String = ""
    var deletedAt : String = ""

    required init(map: Map) {

    }

    func mapping(map: Map) {
        id <- map["id"]
        amount <- map["amount"]
        customerId <- map["vendor_id"]
        pharmacyId <- map["pharmacy_id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
    }

}
