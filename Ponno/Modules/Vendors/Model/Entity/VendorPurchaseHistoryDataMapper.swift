//
//  VendorPurchaseHistoryDataMapper.swift
//  Ponno
//
//  Created by a k azad on 17/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class VendorPurchaseHistoryDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var purchases : [VendorPurchases]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        purchases <- map["purchases"]
        pagination <- map["pagination"]
    }
    
}

