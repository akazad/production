//
//  PayableAdjustList.swift
//  Ponno
//
//  Created by a k azad on 27/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class PayableAdjustList : Mappable {
    var id : Int?
    var amount : String?
    var description : String = ""
    var vendorId : Int?
    var addedBy : String?
    var pharmacyId : Int?
    var createdAt : String?
    var updatedAt : String?
    var deletedAt : String?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        id <- map["id"]
        amount <- map["amount"]
        description <- map["description"]
        vendorId <- map["vendor_id"]
        addedBy <- map["added_by"]
        pharmacyId <- map["pharmacy_id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
    }

}
