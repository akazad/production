//
//  VendorAdd.swift
//  Ponno
//
//  Created by a k azad on 7/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class VendorAdd : Mappable {
    var name : String = ""
    var phone : String = ""
    var address : String = ""
    var pharmacyId : Int = 0
    var addedBy : Int = 0
    var updatedAt : String = ""
    var createdAt : String = ""
    var id : Int = 0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        name <- map["name"]
        phone <- map["phone"]
        address <- map["address"]
        pharmacyId <- map["pharmacy_id"]
        addedBy <- map["added_by"]
        updatedAt <- map["updated_at"]
        createdAt <- map["created_at"]
        id <- map["id"]
    }
    
}
