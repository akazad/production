//
//  VendorAddDataMapper.swift
//  Ponno
//
//  Created by a k azad on 7/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class VendorAddDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var vendor : VendorAdd? 
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        vendor <- map["vendor"]
    }
    
}

