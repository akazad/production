//
//  VendorsViewDataMapper.swift
//  Ponno
//
//  Created by a k azad on 15/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class VendorsDetailsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var vendor : VendorDetails?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        vendor <- map["vendor"]
    }
    
}
