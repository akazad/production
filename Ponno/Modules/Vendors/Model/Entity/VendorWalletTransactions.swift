//
//  VendorWalletTransactions.swift
//  Ponno
//
//  Created by a k azad on 30/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

//class VendorWalletTransactions : Mappable {
//var id : Int = 0
//var type : Int = 0
//var amount : String = ""
//var description : String = ""
//var purchaseId: Int?
//var purchaseInvoice: String = ""

import Foundation
import ObjectMapper

class VendorWalletTransactions : Mappable {
    var id : Int = 0
    var type : Int = 0
    var amount : String = ""
    var description : String = ""
    var purchaseId: Int?
    var purchaseInvoice: String = ""
    var createdAt : String = ""

    required init(map: Map) {

    }

    func mapping(map: Map) {

        id <- map["id"]
        type <- map["type"]
        amount <- map["amount"]
        description <- map["description"]
        purchaseId <- map["purchase_id"]
        purchaseInvoice <- map["purchase_invoice"]
        createdAt <- map["created_at"]
    }

}

