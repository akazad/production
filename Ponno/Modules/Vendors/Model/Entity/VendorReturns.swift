//
//  VendorReturns.swift
//  Ponno
//
//  Created by a k azad on 17/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class VendorReturns : Mappable {
    var id : Int = 0
    var invoice : String = ""
    var total : String = ""
    var createdAt : String = ""
    var name : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        invoice <- map["invoice"]
        total <- map["total"]
        name <- map["name"]
        createdAt <- map["created_at"]
    }
    
}
