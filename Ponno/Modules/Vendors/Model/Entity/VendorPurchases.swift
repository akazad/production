//
//  VendorPurchases.swift
//  Ponno
//
//  Created by a k azad on 17/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class VendorPurchases : Mappable {
    var id : Int = 0
    var invoice : String = ""
    var total : String = ""
    var payable : String = ""
    var paid : String = ""
    var createdAt : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        invoice <- map["invoice"]
        total <- map["total"]
        payable <- map["payable"]
        paid <- map["paid"]
        createdAt <- map["created_at"]
    }
    
}

