//
//  VendorAdjustListDataMapper.swift
//  Ponno
//
//  Created by a k azad on 27/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class PayableAdjustListDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var vendorCurrentPayable: String?
    var payableAdjustList : [PayableAdjustList]? 
    var pagination : Pagination?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        vendorCurrentPayable <- map["vendor_current_payable"]
        payableAdjustList <- map["payable_adjust_list"]
        pagination <- map["pagination"]
    }

}
