//
//  VendorsList.swift
//  Ponno
//
//  Created by a k azad on 27/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class VendorsList : Mappable {
    var id : Int = 0
    var name : String = ""
    var address : String = ""
    var phone : String = ""
    var image : String = ""
    var currentPayable : String = ""
    var hasWallet: Bool = false
    var amount : String = ""
    
    required init(map: Map) {
        
    }
    
    init(id: Int, name: String, address: String, phone: String) {
        self.id = id
        self.name = name
        self.phone = phone
        self.address = address
    }
    
    init(id: Int, name: String, phone: String) {
        self.id = id
        self.name = name
        self.phone = phone
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        address <- map["address"]
        phone <- map["phone"]
        image <- map["image"]
        currentPayable <- map["current_payable"]
        hasWallet <- map["has_wallet"]
        amount <- map["amount"]
    }
    
}
