//
//  VendorsService.swift
//  Ponno
//
//  Created by a k azad on 27/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

public class VendorsService : NSObject{
    
    func getVendorsData(page: Int, success: @escaping (VendorsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Vendors + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
       let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<VendorsDataMapper>) in
                if let vendorsResponse = response.result.value{
                    if let vendorRes = vendorsResponse.vendors, vendorRes.count > 0 {
                        success(vendorsResponse)
                    }else if let failureMessage = vendorsResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getAllVendorsData(getAll: Bool, success: @escaping (VendorsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Vendors + RestURL.sharedInstance.getAllData(text: getAll)
        
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<VendorsDataMapper>) in
                if let vendorsResponse = response.result.value{
                    if let vendorRes = vendorsResponse.vendors, vendorRes.count > 0 {
                        success(vendorsResponse)
                    }else if let failureMessage = vendorsResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getVendorSearchData(page: Int, searchString: String, success: @escaping (VendorsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let vendorsUrl = RestURL.sharedInstance.Vendors  + RestURL.sharedInstance.getSearchText(text: searchString) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = vendorsUrl.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<VendorsDataMapper>) in
                if let vendorsResponse = response.result.value{
                    if let vendorRes = vendorsResponse.vendors, vendorRes.count > 0 {
                        success(vendorsResponse)
                    }
                    else if let vendorsFailure = vendorsResponse.message{
                        failure(vendorsFailure)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getVendorsDetailsData(id: Int, success: @escaping (VendorsDetailsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.VendorDetails + RestURL.sharedInstance.getVendorDetailsUrl(id: id)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<VendorsDetailsDataMapper>) in
                if let vendorDetailsResponse = response.result.value{
                    if vendorDetailsResponse.success == true{
                        success(vendorDetailsResponse)
                    }else if let failureMessage = vendorDetailsResponse.message{
                        failure(failureMessage)
                    }
                    
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getVendorsPayableHistoryData(page: Int, id: Int, model: Bool, success: @escaping (VendorPayableHistoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        var url : String = ""
        if model == true{
            url = RestURL.sharedInstance.VendorPayableHistory + RestURL.sharedInstance.getCommonId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        }else{
            url = RestURL.sharedInstance.VendorRawMaterialPayableHistory + RestURL.sharedInstance.getCommonId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        }
        
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<VendorPayableHistoryDataMapper>) in
                if let vendorPayableResponse = response.result.value{
                    if let payableResponse = vendorPayableResponse.payables, payableResponse.count > 0 {
                        success(vendorPayableResponse)
                    }else if let failureMessage = vendorPayableResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getVendorsPurchaseHistoryData(page: Int, id: Int, model: Bool, success: @escaping (VendorPurchaseHistoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        var url : String = ""
        if model == true{
            url = RestURL.sharedInstance.VendorPurchaseHistory + RestURL.sharedInstance.getCommonId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        }else{
            url = RestURL.sharedInstance.VendorRawMaterialPurchaseHistory + RestURL.sharedInstance.getCommonId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        }
        
        print(url)
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<VendorPurchaseHistoryDataMapper>) in
                if let vendorPurchaseResponse = response.result.value{
                    if let purchaseResponse = vendorPurchaseResponse.purchases, purchaseResponse.count > 0 {
                        success(vendorPurchaseResponse)
                    }else if let failureMessage = vendorPurchaseResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getVendorsReturnHistoryData(page: Int, id: Int, model: Bool, success: @escaping (VendorPurchaseReturnHistoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        var url : String = ""
        if model == true{
            url = RestURL.sharedInstance.VendorReturnHistory + RestURL.sharedInstance.getCommonId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        }else{
            url = RestURL.sharedInstance.VendorRawMaterialReturnHistory + RestURL.sharedInstance.getCommonId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        }
        
        print(url)
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<VendorPurchaseReturnHistoryDataMapper>) in
                if let vendorReturnResponse = response.result.value{
                    if let returnResponse = vendorReturnResponse.returns, returnResponse.count > 0 {
                        success(vendorReturnResponse)
                    }else if let failureMessage = vendorReturnResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getVendorPayableAdjustData(page: Int, id: Int, success: @escaping (PayableAdjustListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PayableAdjust + RestURL.sharedInstance.getDueAdjustId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PayableAdjustListDataMapper>) in
                if let response = response.result.value{
                    if response.success == true{
                        success(response)
                    }else if let failureMessage = response.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getPayableAdjustDateSearchData(page: Int, id : Int, startDate: String, endDate : String, success: @escaping (PayableAdjustListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PayableAdjust + RestURL.sharedInstance.getDueAdjustId(id: id) + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getDateRangePage(text: page)
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<PayableAdjustListDataMapper>) in
                if let adjustResponse = response.result.value{
                    if let response = adjustResponse.payableAdjustList, response.count > 0 {
                        success(adjustResponse)
                    }else if let failureMessage = adjustResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func postPayableAdjustDeleteData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PayableAdjustDelete
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let response = response.result.value {
                if response.success == true{
                    success(response)
                }else if let failureMessage = response.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postPayableAdjustUpdateData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PayableAdjustUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let response = response.result.value {
                if response.success == true{
                    success(response)
                }else if let failureMessage = response.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postPayableAdjustStoreData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PayableAdjustStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let response = response.result.value {
                if response.success == true{
                    success(response)
                }else if let failureMessage = response.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func getVendorWalletData(page: Int, id: Int, success: @escaping (VendorWalletDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.VendorWallet + RestURL.sharedInstance.getDueAdjustId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<VendorWalletDataMapper>) in
                if let response = response.result.value{
                    if response.success == true{
                        success(response)
                    }else if let failureMessage = response.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getVendorWalletSearchData(page: Int, id: Int, startDate: String, endDate: String, success: @escaping (VendorWalletDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let walletURL = RestURL.sharedInstance.VendorWallet + RestURL.sharedInstance.getDueAdjustId(id: id) + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = walletURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<VendorWalletDataMapper>) in
                if let walletResponse = response.result.value{
                    if walletResponse.success == true{
                        success(walletResponse)
                    }else if let failureMessage = walletResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }

    //Mark: PostRequest
    func postAddNewVendorData(vendorData: [String: Any] , success: @escaping (VendorAddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.VendorAdd
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = vendorData
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<VendorAddDataMapper>) in
            if let vendorAddResponse = response.result.value {
                if vendorAddResponse.success == true{
                    success(vendorAddResponse) 
                }else if let failureMessage = vendorAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postUpdateVendorData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.VendorUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            
            if let vendorUpdateResponse = response.result.value {
                if vendorUpdateResponse.success == true{
                    success(vendorUpdateResponse)
                }else if let failureMessage = vendorUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postVendorWalletStoreData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.VendorWalletStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let response = response.result.value {
                if response.success == true{
                    success(response)
                }else if let failureMessage = response.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }

    func postVendorWalletUpdateData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.VendorWalletUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let response = response.result.value {
                if response.success == true{
                    success(response)
                }else if let failureMessage = response.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postVendorWalletDeleteData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.VendorWalletDelete
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let response = response.result.value {
                if response.success == true{
                    success(response)
                }else if let failureMessage = response.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postVendorWalletCashInOut(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.VendorWalletCashInOut
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let cashInOutResponse = response.result.value {
                if cashInOutResponse.success == true{
                    success(cashInOutResponse)
                }else if let failureMessage = cashInOutResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    //Mark: ImageUpload
    func postUserImage(vendorId : Int,image : UIImage? ,  succeed: @escaping((AddDataMapper)->Void), failure : @escaping((String) -> Void)){
        
        let imageUploadUrl = RestURL.sharedInstance.VendorImageUpload
        let params = ["vendor_id" : "\(vendorId)"]
        
        let service = PostImageService()
        
        service.postImage(params: params, image: image, imageUrl: imageUploadUrl, succeed: { response in
            if let success = response.success, success == true{
                print(response.message ?? "")
                succeed(response)
            }
            else{
                failure(response.message ?? "")
            }
        }, failure: {message in
            failure(message)
        })
    }
    
}

