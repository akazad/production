//
//  VendorWalletPresenter.swift
//  Ponno
//
//  Created by a k azad on 28/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol VendorWalletViewDelegate : NSObjectProtocol {
    func setWalletData(data: VendorWalletDataMapper)
    func onCashAdd(data: AddDataMapper)
    func onCashInOut(data: AddDataMapper)
    func onTransactionDelete(data: AddDataMapper)
    func onCashAddFailed(data: String)
    func onDateSearchFailed(message: String)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class VendorWalletPresenter : NSObject {
    
    private var service : VendorsService
    weak private var viewDelegate : VendorWalletViewDelegate?
    
    init(service : VendorsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : VendorWalletViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorWalletDataFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getVendorWalletData(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setWalletData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postVendorWalletCashInDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postVendorWalletStoreData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCashAdd(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCashAddFailed(data: message)
        })
    }
    
    func postVendorWalletCashInOutDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postVendorWalletCashInOut(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCashInOut(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postVendorWalletTransactionDeleteDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postVendorWalletDeleteData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onTransactionDelete(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getVendorWalletSearchDataFromServer(page: Int, id: Int, startDt: String, endDt: String){
        self.viewDelegate?.showLoading()
        self.service.getVendorWalletSearchData(page: page, id: id, startDate: startDt, endDate: endDt, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setWalletData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDateSearchFailed(message: message)
        })
    }
    
}

