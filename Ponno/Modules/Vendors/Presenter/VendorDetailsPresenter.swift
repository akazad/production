//
//  VendorDetailsPresenter.swift
//  Ponno
//
//  Created by a k azad on 15/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol VendorsDetailsViewDelegate : NSObjectProtocol {
    func setVendorsDetailsData(data: VendorsDetailsDataMapper)
    func setVendorsPayableHistoryData(data: VendorPayableHistoryDataMapper)
    func setVendorsPurchaseHistoryData(data: VendorPurchaseHistoryDataMapper)
    func setVendorsPurchaseReturnHistoryData(data: VendorPurchaseReturnHistoryDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class VendorDetailsViewPresenter : NSObject {
    
    private var service : VendorsService
    weak private var viewDelegate : VendorsDetailsViewDelegate?
    
    init(service : VendorsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : VendorsDetailsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getVendorsDetailsData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsDetailsData(data: data)
        }, failure: { (message) in
                self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
            })
    }
    func getVendorsPayableHistoryFromServer(page: Int, id: Int, model: Bool){
        self.viewDelegate?.showLoading()
        self.service.getVendorsPayableHistoryData(page: page, id: id, model: model, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsPayableHistoryData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    func getVendorsPurchaseHistoryFromServer(page: Int, id: Int, model: Bool){
        self.viewDelegate?.showLoading()
        self.service.getVendorsPurchaseHistoryData(page: page, id: id, model: model,  success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsPurchaseHistoryData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    func getVendorsPurchaseReturnHistoryFromServer(page: Int, id: Int, model: Bool){
        self.viewDelegate?.showLoading()
        self.service.getVendorsReturnHistoryData(page: page, id: id, model: model, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsPurchaseReturnHistoryData(data: data) 
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}

