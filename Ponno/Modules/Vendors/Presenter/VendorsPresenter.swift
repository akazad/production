//
//  VendorsPresenter.swift
//  Ponno
//
//  Created by a k azad on 27/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol VendorsViewDelegate : NSObjectProtocol {
    func setVendorsData(data: VendorsDataMapper)
    func onFailed()
    func showLoading()
    func hideLoading()
}

class VendorsPresenter : NSObject {
    
    private var service : VendorsService
    weak private var viewDelegate : VendorsViewDelegate?
    
    init(service : VendorsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : VendorsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        service.getVendorsData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
}
