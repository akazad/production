//
//  PayableAdjustPresenter.swift
//  Ponno
//
//  Created by a k azad on 27/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol PayableAdjustViewDelegate : NSObjectProtocol {
    func onPayableAdjustSuccessfullyUpdate(data : AddDataMapper)
    func onPayableAdjustSuccessfullyAdd(data: AddDataMapper)
    func onFailed(failure: String)
    func showLoading()
    func hideLoading()
}

class PayableAdjustPresenter: NSObject {
    
    private let service : VendorsService
    weak private var viewDelegate: PayableAdjustViewDelegate?
    
    init(service : VendorsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PayableAdjustViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postPayableAdjustStoreDataToServer(param:[String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postPayableAdjustStoreData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onPayableAdjustSuccessfullyAdd(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
    func postPayableAdjustUpdateDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postPayableAdjustUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onPayableAdjustSuccessfullyUpdate(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
}

