//
//  VendorSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 18/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
protocol VendorsSearchViewDelegate : NSObjectProtocol {
    func setVendorsSerachData(data: VendorsDataMapper)
    func onFailed(failure: String)
    func showLoading()
    func hideLoading()
}

class VendorSearchPresenter : NSObject {
    
    private var service : VendorsService
    weak private var viewDelegate : VendorsSearchViewDelegate?
    
    init(service : VendorsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : VendorsSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorSearchDataFromServer(page: Int, searchText: String){
        self.viewDelegate?.showLoading()
        self.service.getVendorSearchData(page: page, searchString: searchText, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsSerachData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
}
