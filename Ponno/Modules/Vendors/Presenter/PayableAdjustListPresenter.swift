//
//  PayableAdjustListPresenter.swift
//  Ponno
//
//  Created by a k azad on 27/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
protocol PayableAdjustListViewDelegate : NSObjectProtocol {
    func setPayableAdjustData(data : PayableAdjustListDataMapper)
    func onPayableAdjustDelete(data: AddDataMapper)
    func onDateSearchFailed(message: String)
    func onFailed(failure: String)
    func showLoading()
    func hideLoading()
}

class PayableAdjustListPresenter: NSObject {
    
    private let service : VendorsService
    weak private var viewDelegate: PayableAdjustListViewDelegate?
    
    init(service : VendorsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PayableAdjustListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getPayableAdjustListDataFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getVendorPayableAdjustData(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPayableAdjustData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
    func getPayableAdjustDateSearchDataFromServer(startDt: String, endDt: String, page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getPayableAdjustDateSearchData(page: page, id: id, startDate: startDt, endDate: endDt, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPayableAdjustData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDateSearchFailed(message: message)
        })
    }
    
    func postPayableAdjustDeleteDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postPayableAdjustDeleteData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onPayableAdjustDelete(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
}
