//
//  AddNewVendorPresenter.swift
//  Ponno
//
//  Created by a k azad on 7/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol AddNewVendorViewDelegate : NSObjectProtocol {
    func setAddNewVendorData(vendorData : VendorAddDataMapper)
    func onImageUpload(data : AddDataMapper)
    func updateVendorData(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}
class AddNewVendorPresenter: NSObject {
    
    private let service : VendorsService
    weak private var viewDelegate : AddNewVendorViewDelegate?
    
    init(service : VendorsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AddNewVendorViewDelegate){
        self.viewDelegate = viewDelegate
    }

    func postAddNewVendorDataToServer(vendorData : [String: String]){
        self.viewDelegate?.showLoading()
        self.service.postAddNewVendorData(vendorData: vendorData, success: { (data) in
            self.viewDelegate?.setAddNewVendorData(vendorData: data) 
            self.viewDelegate?.hideLoading()
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func updateVendorData(params: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postUpdateVendorData(param: params, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateVendorData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func uploadImage(vendorId : Int,image : UIImage?){
        self.viewDelegate?.showLoading()
        self.service.postUserImage(vendorId: vendorId, image: image, succeed: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onImageUpload(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}


