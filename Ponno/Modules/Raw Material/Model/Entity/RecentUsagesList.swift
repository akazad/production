//
//  RecentUsagesList.swift
//  Ponno
//
//  Created by a k azad on 17/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class RecentUsagesList : Mappable {
    var unit : String = ""
    var materialQuantity : String = ""
    var currentQuantity : String = ""
    var productName : String = ""
    var createdAt : String = ""
    
     required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        unit <- map["unit"]
        materialQuantity <- map["material_quantity"]
        currentQuantity <- map["current_quantity"]
        productName <- map["product_name"]
        createdAt <- map["created_at"]
    }
    
}

