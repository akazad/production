//
//  RawMaterialRecentStockDataMapper.swift
//  Ponno
//
//  Created by a k azad on 17/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class RawMaterialRecentStockDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var recentStocksList : [RawMaterialRecentStocksList]? 
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        recentStocksList <- map["recent_stocks_list"]
        pagination <- map["pagination"]
    }
    
}

