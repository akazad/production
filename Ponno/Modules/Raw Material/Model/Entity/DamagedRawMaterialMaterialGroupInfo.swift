//
//  DamagedMaterialGroupInfo.swift
//  Ponno
//
//  Created by a k azad on 23/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DamagedRawMaterialGroupInfo : Mappable {
    var damagedMaterialId : Int = -1
    var damagedMaterialName : String = ""
    var unit : String = ""
    var damagedLot : Int = 0
    var totalLoss : Int = 0
    var totalQuantity : Int = 0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        damagedMaterialId <- map["damaged_material_id"]
        damagedMaterialName <- map["damaged_material_name"]
        unit <- map["unit"]
        damagedLot <- map["damaged_lot"]
        totalLoss <- map["total_loss"]
        totalQuantity <- map["total_quantity"]
    }
    
}

