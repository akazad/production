//
//  DamagedPerRawMaterial.swift
//  Ponno
//
//  Created by a k azad on 23/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DamagedPerRawMaterial : Mappable {
    var id : Int = 0
    var materialId : Int = 0
    var materialInventoryId : Int = 0
    var materialInventoryHistoryId : Int = 0
    var quantity : String = ""
    var buyingPrice : String = ""
    var description : String = ""
    var pharmacyId : Int = 0
    var addedBy : Int = 0
    var createdAt : String = ""
    var updatedAt : String = ""
    var deletedAt : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        materialId <- map["material_id"]
        materialInventoryId <- map["material_inventory_id"]
        materialInventoryHistoryId <- map["material_inventory_history_id"]
        quantity <- map["quantity"]
        buyingPrice <- map["buying_price"]
        description <- map["description"]
        pharmacyId <- map["pharmacy_id"]
        addedBy <- map["added_by"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
    }
    
}
