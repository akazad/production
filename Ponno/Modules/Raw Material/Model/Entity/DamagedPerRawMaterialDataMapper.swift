//
//  DamagedPerRawMaterialDataMapper.swift
//  Ponno
//
//  Created by a k azad on 23/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DamagedPerRawMaterialDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var materialName : String = ""
    var damagedPerRawMaterial : [DamagedPerRawMaterial]? 
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        materialName <- map["material_name"]
        damagedPerRawMaterial <- map["damaged_material_list"]
        pagination <- map["pagination"]
    }
    
}

