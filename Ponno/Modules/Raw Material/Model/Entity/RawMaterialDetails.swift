//
//  RawMaterialDetails.swift
//  Ponno
//
//  Created by a k azad on 17/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class RawMaterialDetails : Mappable {
    var materialInventoryId : Int = 0
    var deletable : Int = 0
    var quantity : String = ""
    var currentStockAmount : String = ""
    var unitId : Int = 0
    var unit : String = ""
    var buyingPrice : String = ""
    var stockAlert : Int = 0
    var materialId : Int = 0
    var name : String = ""
    var company : String = ""
    var variant : String = ""
    var sku : String = ""
    var image : String = ""
    var categoryId : Int = 0
    var categoryName : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        materialInventoryId <- map["material_inventory_id"]
        deletable <- map["deletable"]
        quantity <- map["quantity"]
        currentStockAmount <- map["current_stock_amount"]
        unitId <- map["unit_id"]
        unit <- map["unit"]
        buyingPrice <- map["buying_price"]
        stockAlert <- map["stock_alert"]
        materialId <- map["material_id"]
        name <- map["name"]
        company <- map["company"]
        variant <- map["variant"]
        sku <- map["sku"]
        image <- map["image"]
        categoryId <- map["category_id"]
        categoryName <- map["category_name"]
    }
    
}

