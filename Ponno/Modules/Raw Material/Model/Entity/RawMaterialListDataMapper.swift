//
//  RawMaterialListDataMapper.swift
//  Ponno
//
//  Created by a k azad on 16/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class RawMaterialListDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var materialList : [MaterialList]? 
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        materialList <- map["material_list"]
        pagination <- map["pagination"]
    }
    
}

