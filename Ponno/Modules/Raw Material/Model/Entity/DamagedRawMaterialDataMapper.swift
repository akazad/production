//
//  DamagedRawMaterialDataMapper.swift
//  Ponno
//
//  Created by a k azad on 23/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DamagedRawMaterialDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var damagedRawMaterialGroupInfo : [DamagedRawMaterialGroupInfo]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        damagedRawMaterialGroupInfo <- map["damaged_material_group_info"]
        pagination <- map["pagination"]
    }
    
}

