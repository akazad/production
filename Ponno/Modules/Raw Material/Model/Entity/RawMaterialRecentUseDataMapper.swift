//
//  RawMaterialRecentUseDataMapper.swift
//  Ponno
//
//  Created by a k azad on 17/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class RawMaterialRecentUseDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var recentUsagesList : [RecentUsagesList]? 
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        recentUsagesList <- map["recent_usages_list"]
        pagination <- map["pagination"]
    }
    
}
