//
//  RawMaterialDetailsDataMapper.swift
//  Ponno
//
//  Created by a k azad on 17/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class RawMaterialDetailsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var rawMaterialDetails : RawMaterialDetails?
    var vendors : [String]?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        rawMaterialDetails <- map["material_inventory_list"]
        vendors <- map["vendors"]
    }
    
}
