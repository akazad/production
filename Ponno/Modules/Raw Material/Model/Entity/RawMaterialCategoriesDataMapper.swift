//
//  RawMaterialCategoriesDataMapper.swift
//  Ponno
//
//  Created by a k azad on 18/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class RawMaterialCategoriesDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var rawMaterialCategories : [RawMaterialCategories]? 
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        rawMaterialCategories <- map["categories"]
    }
    
}
