//
//  RawMaterialAddDataMapper.swift
//  Ponno
//
//  Created by a k azad on 18/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class RawMaterialAddDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var rawMaterialId : Int?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        rawMaterialId <- map["material_id"]
    }
    
}
