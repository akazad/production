//
//  RawMaterialCategories.swift
//  Ponno
//
//  Created by a k azad on 18/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class RawMaterialCategories : Mappable {
    var id : Int = 0
    var name : String = ""
    var gen : Int = 0
    var parent : Int = 0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        gen <- map["gen"]
        parent <- map["parent"]
    }
    
}

