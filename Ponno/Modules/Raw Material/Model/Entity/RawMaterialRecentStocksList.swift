//
//  RawMaterialRecentStocksList.swift
//  Ponno
//
//  Created by a k azad on 17/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class RawMaterialRecentStocksList : Mappable {
    var materialInventoryHistoryId : Int = 0
    var materialInventoryId : Int = 0
    var materialId : Int = 0
    var unit : String = ""
    var quantity : String = ""
    var currentQuantity : String = ""
    var buyingPrice : String = ""
    var expireDate : String = ""
    var vendorId : Int = 0
    var vendorName : String = ""
    var createdAt : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        materialInventoryHistoryId <- map["material_inventory_history_id"]
        materialInventoryId <- map["material_inventory_id"]
        materialId <- map["material_id"]
        unit <- map["unit"]
        quantity <- map["quantity"]
        currentQuantity <- map["current_quantity"]
        buyingPrice <- map["buying_price"]
        expireDate <- map["expire_date"]
        vendorId <- map["vendor_id"]
        vendorName <- map["vendor_name"]
        createdAt <- map["created_at"]
    }
    
}
