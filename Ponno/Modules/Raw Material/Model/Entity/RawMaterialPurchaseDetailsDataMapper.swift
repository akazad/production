//
//  RawMaterialPurchaseDetailsDataMapper.swift
//  Ponno
//
//  Created by a k azad on 1/10/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class RawMaterialPurchaseDetailsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var rawMaterialPurchaseDetails : RawMaterialPurchaseDetails?
    var rawMaterialPurchaseMaterials : [RawMaterialPurchaseMaterials]?
    var rawMaterialPayableTransactions : [PayableTransactions]?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        rawMaterialPurchaseDetails <- map["purchase_details"]
        rawMaterialPurchaseMaterials <- map["purchase_materials"]
        rawMaterialPayableTransactions <- map["payable_transactions"]
    }
    
}
