//
//  RawMaterialService.swift
//  Ponno
//
//  Created by a k azad on 16/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper

public class RawMaterialService : NSObject{
    
    func getRawMaterialListData(page: Int, success: @escaping (RawMaterialListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.RawMaterial + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(URL, headers: headers)
            .responseObject { (response: DataResponse<RawMaterialListDataMapper>) in
                if let rawMaterialResponse = response.result.value{
                    if let response = rawMaterialResponse.materialList, response.count > 0 {
                        success(rawMaterialResponse)
                    }
                    else if let failureMessage = rawMaterialResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getAllProductData(getAll: Bool, success: @escaping (ProductListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Product + RestURL.sharedInstance.getAllData(text: getAll)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ProductListDataMapper>) in
                if let productResponse = response.result.value{
                    if let response = productResponse.productList, response.count > 0 {
                        success(productResponse)
                    }
                    else if let failureMessage = productResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getRawMaterialSearchData(page: Int, searchString: String, success: @escaping (RawMaterialListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.RawMaterial + RestURL.sharedInstance.getSearchText(text: searchString) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = URL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<RawMaterialListDataMapper>) in
                if let rawMaterialResponse = response.result.value{
                    if rawMaterialResponse.success == true{
                        if let response = rawMaterialResponse.materialList, response.count > 0 {
                            success(rawMaterialResponse)
                        }
                        else if let failureMessage = rawMaterialResponse.message{
                            failure(failureMessage)
                        }
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
        
    }
 
    func getRawMaterialViewData(id: Int, success: @escaping (RawMaterialDetailsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let viewURL = RestURL.sharedInstance.RawMaterialView + RestURL.sharedInstance.getCommonId(id: id)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(viewURL, headers: headers)
            .responseObject { (response: DataResponse<RawMaterialDetailsDataMapper>) in
                if let rawMaterialViewResponse = response.result.value{
                    if rawMaterialViewResponse.success == true{
                        success(rawMaterialViewResponse)
                    }else if let failureMessage = rawMaterialViewResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getRawMaterialRecentStock(page: Int, id: Int, success: @escaping (RawMaterialRecentStockDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialRecentStock + RestURL.sharedInstance.getCommonId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(url)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<RawMaterialRecentStockDataMapper>) in
                if let rawMaterialRecentStockResponse = response.result.value{
                    if let recentStock = rawMaterialRecentStockResponse.recentStocksList, recentStock.count > 0 {
                        success(rawMaterialRecentStockResponse)
                    }else if let failureMessage = rawMaterialRecentStockResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getRawMaterialRecentUse(page: Int, id: Int, success: @escaping (RawMaterialRecentUseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialRecentUse + RestURL.sharedInstance.getCommonId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(url)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<RawMaterialRecentUseDataMapper>) in
                if let rawMaterialRecentUseResponse = response.result.value{
                    if let recentUsages = rawMaterialRecentUseResponse.recentUsagesList, recentUsages.count > 0 {
                        success(rawMaterialRecentUseResponse)
                    }else if let failureMessage = rawMaterialRecentUseResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getAllRawMaterialCategories(getAll: Bool, success: @escaping (RawMaterialCategoriesDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialCategories + RestURL.sharedInstance.getAllData(text: getAll)
        print(url)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<RawMaterialCategoriesDataMapper>) in
                if let rawMaterialCategoryResponse = response.result.value{
                    if let rawMaterialCategory = rawMaterialCategoryResponse.rawMaterialCategories, rawMaterialCategory.count > 0 {
                        success(rawMaterialCategoryResponse)
                    }else if let failureMessage = rawMaterialCategoryResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getRawMaterialCategoryListData(page : Int, success: @escaping (RawMaterialCategoriesDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialCategories
        print(url)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<RawMaterialCategoriesDataMapper>) in
                if let rawMaterialCategoriesListResponse = response.result.value{
                    if let rawMaterialCategory = rawMaterialCategoriesListResponse.rawMaterialCategories, rawMaterialCategory.count > 0 {
                        success(rawMaterialCategoriesListResponse)
                    }else if let failureMessage = rawMaterialCategoriesListResponse.message {
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getRawMaterialPurchaseListData(page : Int, success: @escaping (PurchaseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialPurchase
        print(url)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<PurchaseDataMapper>) in
                if let rawMaterialPurchaseListResponse = response.result.value{
                    if let rawMaterialPurchase = rawMaterialPurchaseListResponse.purchaseBook, rawMaterialPurchase.count > 0 {
                        success(rawMaterialPurchaseListResponse)
                    }else if let failureMessage = rawMaterialPurchaseListResponse.message {
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getRawMaterialPurchaseSearchData(page: Int, startDate: String, endDate: String, success: @escaping (PurchaseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialPurchase + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getDateRangePage(text: page)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PurchaseDataMapper>) in
                if let purchaseResponse = response.result.value{
                    if let purchase = purchaseResponse.purchaseBook, purchase.count > 0{
                        if purchaseResponse.success == true{
                            success(purchaseResponse)
                        }else if let failureMessage = purchaseResponse.message{
                            failure(failureMessage)
                        }
                    }else{
                        failure(CommonMessages.ApiFailure)
                    }
                }
        }
    }
    
    func getPerDayPurchaseRawMaterialData(page: Int, date: String, success: @escaping (PerDayPurchaseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialPerDayPurchase + RestURL.sharedInstance.getPerDayPurchaseUrl(date: date) + RestURL.sharedInstance.getPageNumber(page: page)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PerDayPurchaseDataMapper>) in
                if let purchasePerDayResponse = response.result.value{
                    if let perDayResponse = purchasePerDayResponse.purchaseList, perDayResponse.count > 0{
                        success(purchasePerDayResponse)
                    }
                    else if let failureResponse = purchasePerDayResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getPerDayPurchaseRawMaterialSearchData(page: Int, date: String, searchText: String, success: @escaping (PerDayPurchaseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialPerDayPurchase + RestURL.sharedInstance.getPerDayPurchaseUrl(date: date) + RestURL.sharedInstance.getSearchText(text: searchText) + RestURL.sharedInstance.getPageNumber(page: page)
        
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PerDayPurchaseDataMapper>) in
                if let purchasePerDayResponse = response.result.value{
                    if let perDayResponse = purchasePerDayResponse.purchaseList, perDayResponse.count > 0{
                        success(purchasePerDayResponse)
                    }
                    else if let failureResponse = purchasePerDayResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getRawMaterialPurchaseDetailsData(id: Int, success: @escaping (RawMaterialPurchaseDetailsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialPurchaseDetails + RestURL.sharedInstance.getCommonId(id: id)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<RawMaterialPurchaseDetailsDataMapper>) in
                if let rawMaterialPurchaseDetailsResponse = response.result.value {
                    if rawMaterialPurchaseDetailsResponse.success == true{
                        success(rawMaterialPurchaseDetailsResponse)
                    }
                    else if let failureMessage = rawMaterialPurchaseDetailsResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getDamageRawMaterialData(success: @escaping (DamagedRawMaterialDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.DamagedRawMaterial
        print(URL)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(URL, headers: headers)
            .responseObject { (response: DataResponse<DamagedRawMaterialDataMapper>) in
                if let damagedRawMaterialResponse = response.result.value{
                    if let damageRawMaterial = damagedRawMaterialResponse.damagedRawMaterialGroupInfo, damageRawMaterial.count > 0 {
                        success(damagedRawMaterialResponse)
                    }else if let failureMessage =  damagedRawMaterialResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getDamagedRawMaterialSearchData(page: Int, searchString: String, success: @escaping (DamagedRawMaterialDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.DamagedRawMaterial + RestURL.sharedInstance.getSearchText(text: searchString) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = URL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<DamagedRawMaterialDataMapper>) in
                if let damagedRawMaterialResponse = response.result.value{
                    if damagedRawMaterialResponse.success == true{
                        if let response = damagedRawMaterialResponse.damagedRawMaterialGroupInfo, response.count > 0 {
                            success(damagedRawMaterialResponse)
                        }
                        else if let failureMessage = damagedRawMaterialResponse.message{
                            failure(failureMessage)
                        }
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
        
    }
    
    func getDamagePerRawMaterialData(page: Int, id: Int, success: @escaping (DamagedPerRawMaterialDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.DamagedPerRawMaterial + RestURL.sharedInstance.getCommonId(id: id)
        print(URL)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(URL, headers: headers)
            .responseObject { (response: DataResponse<DamagedPerRawMaterialDataMapper>) in
                if let damagedPerRawMaterialResponse = response.result.value{
                    if let damagePerRawMaterial = damagedPerRawMaterialResponse.damagedPerRawMaterial, damagePerRawMaterial.count > 0 {
                        success(damagedPerRawMaterialResponse)
                    }else if let failureMessage =  damagedPerRawMaterialResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    //Mark: PurchaseCancel
    func PostPerDayPurchaseCancelData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.RawMaterialPerDayPurchaseCancel
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(URL, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let cancelPurchaseResponse = response.result.value {
                if cancelPurchaseResponse.success == true{
                    success(cancelPurchaseResponse)
                }else if let failureResponse = cancelPurchaseResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    //RawMaterialDelete
    
    func PostRawMaterialDeleteData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialDelete
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["inventory_id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let deleteRawMaterialResponse = response.result.value {
                if deleteRawMaterialResponse.success == true{
                    success(deleteRawMaterialResponse)
                }else if let failureResponse = deleteRawMaterialResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func PostRawMaterialRecentStockDeleteData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialRecentStockDelete
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["material_inventory_history_id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let deleteRecentStockResponse = response.result.value {
                if deleteRecentStockResponse.success == true{
                    success(deleteRecentStockResponse)
                }else if let failureResponse = deleteRecentStockResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postRawMaterialUpdateData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let rawMaterialUpdateResponse = response.result.value {
                if rawMaterialUpdateResponse.success == true{
                    success(rawMaterialUpdateResponse)
                }else if let failureMessage = rawMaterialUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postNewRawMaterialDataToServer(params: [String: Any] , success: @escaping (RawMaterialAddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<RawMaterialAddDataMapper>) in
            if let rawMaterialStoreResponse = response.result.value {
                if rawMaterialStoreResponse.success == true{
                    success(rawMaterialStoreResponse)
                }else if let failureMessage = rawMaterialStoreResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    //NewRawMaterialCategoryAdd
    func postRawMaterialCategoryAddData(params: [String: Any] , success: @escaping (CategoryAddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialCategoryStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<CategoryAddDataMapper>) in
            if let rawMaterialategoryAddResponse = response.result.value {
                if rawMaterialategoryAddResponse.success == true{
                    success(rawMaterialategoryAddResponse)
                }else if let failureMessage = rawMaterialategoryAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postRawMaterialRecentStockReturnData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialRecentStockReturn
        print(url)
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let rawMaterialRecentStockReturnResponse = response.result.value {
                if rawMaterialRecentStockReturnResponse.success == true{
                    success(rawMaterialRecentStockReturnResponse)
                }else if let failureMessage = rawMaterialRecentStockReturnResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postProductionData(params: [String: Any] , success: @escaping (ProductAddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductionStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<ProductAddDataMapper>) in
            if let productionResponse = response.result.value {
                if productionResponse.success == true{
                    success(productionResponse)
                }else if let failureMessage = productionResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postPurchaseReturnRawMaterialData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialPurchaseReturn
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let purchaseReturnResponse = response.result.value {
                if purchaseReturnResponse.success == true{
                    success(purchaseReturnResponse)
                }else if let failureMessage = purchaseReturnResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postRawMaterialPurchaseData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RawMaterialPurchaseStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let rawMaterialPurchaseStoreResponse = response.result.value {
                if rawMaterialPurchaseStoreResponse.success == true{
                    success(rawMaterialPurchaseStoreResponse)
                }else if let failureMessage = rawMaterialPurchaseStoreResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postDamagedRawMaterialData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DamagedRawMaterialStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let damagedRawMaterialStoreResponse = response.result.value {
                if damagedRawMaterialStoreResponse.success == true{
                    success(damagedRawMaterialStoreResponse)
                }else if let failureMessage = damagedRawMaterialStoreResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postDamagedPerRawMaterialUpdateData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DamagedRawMaterialUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let damagedPerRawMaterialUpdateResponse = response.result.value {
                if damagedPerRawMaterialUpdateResponse.success == true{
                    success(damagedPerRawMaterialUpdateResponse)
                }else if let failureMessage = damagedPerRawMaterialUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func deleteDamagedPerRawMaterialData(id: Int, success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DamagedRawMaterialDelete
        print(url +  "\(id)")
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let damageRawMaterialDeleteResponse = response.result.value {
                if damageRawMaterialDeleteResponse.success == true{
                    success(damageRawMaterialDeleteResponse)
                }else if let failureMessage = damageRawMaterialDeleteResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    //Mark: ImageUpload
    func postRawMaterialImage(rawMaterialId : Int,image : UIImage? ,  succeed: @escaping((AddDataMapper)->Void), failure : @escaping((String) -> Void)){
        
        let imageUploadUrl = RestURL.sharedInstance.RawMaterialUploadImage
        let params = ["material_id" : "\(rawMaterialId)"]
        
        let service = PostImageService()
        
        service.postImage(params: params, image: image, imageUrl: imageUploadUrl, succeed: { response in
            if let success = response.success, success == true{
                print(response.message ?? "")
                succeed(response)
            }
            else{
                failure(response.message ?? "")
            }
        }, failure: {message in
            failure(message)
        })
    }
    
}
