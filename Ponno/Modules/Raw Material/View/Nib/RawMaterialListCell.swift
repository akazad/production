//
//  RawMaterialListCell.swift
//  Ponno
//
//  Created by a k azad on 16/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class RawMaterialListCell: UITableViewCell {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var buyingPriceLbl: UILabel!
    @IBOutlet weak var variantLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var selectBtn: UIButton!
    
    var rawMaterial : MaterialList?{
        didSet{
            if let item = rawMaterial{

                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.rawMaterialImage + item.image), placeholderImage: UIImage(named: "placeholder"))
                
                nameLabel.text = String(describing: item.name)
                variantLabel.text = LanguageManager.Varient + ": " + String(describing: item.variant)
                
                let buyingPrice = item.buyingPrice
                if buyingPrice.isEmpty{
                    buyingPriceLbl.text = LanguageManager.BuyingPrice + " : " + "0.00" + Constants.currencySymbol
                }else{
                    buyingPriceLbl.text = LanguageManager.BuyingPrice + " : " + item.buyingPrice + Constants.currencySymbol
                }
                
                let quantity = item.quantity
                if quantity.isEmpty{
                    quantityLabel.text = LanguageManager.Quantity +  " : " + String(describing: 0.00) + " " + item.unit
                }else{
                    quantityLabel.text = LanguageManager.Quantity +  " : " + String(describing: item.quantity) + " " + item.unit
                    
                    if let quantity = Double(item.quantity){
                        let stockAlert = Double(item.stockAlert)
                        if quantity <= stockAlert{
                            quantityLabel.textColor = UIColor.red
                        }
                    }
                }
                
                categoryLabel.text = LanguageManager.Category + " : " + String(describing: item.categoryName)
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: RawMaterialListCell.self)
    }
    
}
