//
//  RawMaterialDetailsInfoCell.swift
//  Ponno
//
//  Created by a k azad on 17/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class RawMaterialDetailsInfoCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var alphabetView: UIView!
    @IBOutlet weak var rawMaterialName: UILabel!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var variantLabel: UILabel!
    @IBOutlet weak var sku: UILabel!
    @IBOutlet weak var buyingPrice: UILabel!
    @IBOutlet weak var cautionLbl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: RawMaterialDetailsInfoCell.self)
    }
    
}
