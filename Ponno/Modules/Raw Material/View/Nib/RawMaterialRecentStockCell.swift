//
//  RawMaterialRecentStockCell.swift
//  Ponno
//
//  Created by a k azad on 17/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class RawMaterialRecentStockCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var currentQuantityLabel: UILabel!
    @IBOutlet weak var stockAmountLabel: UILabel!
    @IBOutlet weak var buyingPriceLabel: UILabel!
    @IBOutlet weak var dealerNameLabel: UILabel!
    @IBOutlet weak var expireDateLbl: UILabel!
    @IBOutlet weak var popUpBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: RawMaterialRecentStockCell.self)
    }
    
    
}
