//
//  RawMaterialDetailsSummaryCell.swift
//  Ponno
//
//  Created by a k azad on 17/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class RawMaterialDetailsSummaryCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var currentStockLbl: UILabel!
    @IBOutlet weak var currentStockTitleLbl: UILabel!
    @IBOutlet weak var currentStockAmountLbl: UILabel!
    @IBOutlet weak var currentStockAmountTitleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: RawMaterialDetailsSummaryCell.self)
    }
    
}
