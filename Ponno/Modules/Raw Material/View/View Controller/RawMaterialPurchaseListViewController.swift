//
//  RawMaterialPurchaseListViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class RawMaterialPurchaseListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    @IBOutlet weak var dateSearchHeight: NSLayoutConstraint!
    
    var selectedDate : String?
    
    private var presenter = PurchaseRawMaterialListPresenter(service: PurchaseService())
    
    private var purchaseBookList : [PurchaseBook] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var purchaseSummery: [PurchaseSummery] = []{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    //Mark: PaginationPropertise
    public var isLoading : Bool = false
    public var currentPage : Int = 1
    var lastPageNumber: Int = 0
    var searchPage : Int = 1
    var searchEnable: Bool = false
    
    var isSearchActive = false {
        didSet{
            self.refreshTableView()
        }
    }
    
    var datePicker = UIDatePicker()
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNavigationBarTitle()
        self.configureTableView()
        self.configureCollectionView()
        self.initialSetUp()
        self.showStartDatePicker()
        self.showEndDatePicker()
        self.setBarButton()
        
    }
    
    func initialSetUp(){
        self.generatingDate()
        self.searchBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        self.dateSearchHeight.constant = 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setBarButton(){
        
        //        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        //        button.setImage(UIImage(named: "dateSearch"), for: UIControl.State.normal)
        //        button.addTarget(self, action: #selector(onPressed(sender:)), for: UIControl.Event.touchUpInside)
        //        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        //        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        //        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        //        let barButton = UIBarButtonItem(customView: button)
        //
        //        self.navigationItem.rightBarButtonItem = barButton
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "dateSearch"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearchBtnPressed(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let popUpBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        popUpBtn.setImage(UIImage(named: "popupmenudark"), for: UIControl.State.normal)
        popUpBtn.addTarget(self, action: #selector(onBarPopUpBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        popUpBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        popUpBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        popUpBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn2 = UIBarButtonItem(customView: popUpBtn)
        
        let purchaseBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        purchaseBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        purchaseBtn.addTarget(self, action: #selector(onRawMaterialpurchase(sender:)), for: UIControl.Event.touchUpInside)
        purchaseBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        purchaseBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        purchaseBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton3 = UIBarButtonItem(customView: purchaseBtn)
        
        navigationItem.setRightBarButtonItems([barBtn2,barButton3,barButton1], animated: true)
    }
    
    @objc func onSearchBtnPressed(sender: UIBarButtonItem){
        if searchEnable == false{
            self.dateSearchHeight.constant = 30.0
            self.searchEnable = !searchEnable
        }else{
            self.dateSearchHeight.constant = 0.0
            searchEnable = !searchEnable
        }
        
    }
    
    @objc func onRawMaterialpurchase(sender: UIBarButtonItem){
        self.navigateToPurchaseableRawMaterialListViewController()
    }
    
    @objc func onBarPopUpBtnTapped(sender : UIBarButtonItem){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        let returnAction = UIAlertAction(title: LanguageManager.RawMaterialReturn, style: UIAlertAction.Style.default) { (action) in
            self.navigateToPurchaseReturnRawMaterialListViewController()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
            
            self.view.endEditing(true)
        }
        
        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
        
        myActionSheet.addAction(returnAction)
        myActionSheet.addAction(cancelAction)
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.RawMaterialPurchaseBook
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func navigateToPurchaseReturnRawMaterialListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseReturnRawMaterialListViewController") as! PurchaseReturnRawMaterialListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchaseableRawMaterialListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseableRawMaterialListViewController") as! PurchaseableRawMaterialListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(PurchaseViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.purchaseBookList = []
        self.currentPage = 1
        self.isSearchActive = false
        self.datePickerTextField.text = ""
        self.toDatePickerTextField.text = ""
        self.presenter.getRawMaterialPurchaseDataFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }
    
    func generatingDate(){
        
        let today = Date()
        print(today)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        //self.datePickerTextField.text = dateFormatter.string(from: today)
        //self.toDatePickerTextField.text = dateFormatter.string(from: today)
        
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.currentPage = 1
            self.presenter.getRawMaterialPurchaseDataFromServer(page: self.currentPage)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}

extension RawMaterialPurchaseListViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            if let startDate = self.datePickerTextField.text, let endDate = self.toDatePickerTextField.text {
                self.searchPage = 1
                self.purchaseBookList = []
                self.isSearchActive = true
                self.presenter.getRawMaterialPurchaseSearchDataFromServer(page: self.searchPage, startDate: startDate, endDate: endDate)
            }
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

//MARK: CollectionView Delegate And Data Source
extension RawMaterialPurchaseListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(PurchaseSummeryCell.nib, forCellWithReuseIdentifier: PurchaseSummeryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard self.purchaseSummery.count > 0 else{
            return 0
        }
        return self.purchaseSummery.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PurchaseSummeryCell = collectionView.dequeueReusableCell(withReuseIdentifier: PurchaseSummeryCell.identifier, for: indexPath) as! PurchaseSummeryCell
        guard self.purchaseSummery.count > 0 else {
            return cell
        }
        let summeryItem = self.purchaseSummery[indexPath.row]
        
//        for (key, _) in self.purchaseSummery.enumerated() {
//            if key == 0 {
//                self.purchaseSummery[0].title = "Total Purchase"
//            }else if key == 1 {
//                self.purchaseSummery[1].title = "Total Purchase Amount"
//            }else if key == 2 {
//                self.purchaseSummery[2].title = "Current Payable"
//            }
//        }
        
        cell.summeryName.text = summeryItem.title
        cell.summeryNumber.text = summeryItem.value
        return cell
    }
    
    func refreshCollectionView(){
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70.0)
    }
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard self.purchaseSummery.count > 0 else{
            return
        }
        let listCount = self.purchaseSummery.count
        
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}


//MARK: TableView Delegate and Data Source
extension RawMaterialPurchaseListViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PurchaseListCell.nib, forCellReuseIdentifier: PurchaseListCell.identifier)
        self.tableView.estimatedRowHeight = 96.0
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.separatorColor = UIColor.clear
        self.tableView.addSubview(refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.purchaseBookList.count > 0 {
            return self.purchaseBookList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PurchaseListCell = tableView.dequeueReusableCell(withIdentifier: PurchaseListCell.identifier, for: indexPath) as! PurchaseListCell
        
        cell.selectionStyle = .none
        
        if self.purchaseBookList.count > 0 {
            let purchaseItem = self.purchaseBookList[indexPath.row]
            
            cell.dateLabel.text = purchaseItem.date.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
            cell.dayPurchase.text = LanguageManager.TotalPurchase + " : " + String(describing: purchaseItem.dayPurchase)
            
            let dayPayable = purchaseItem.dayPayable
            if dayPayable.isEmpty{
                cell.dayPurchaseAmountLabel.text = LanguageManager.TotalPayable + " : " + "\(0.0 )" + " "  + Constants.currencySymbol
            }else{
                cell.dayPurchaseAmountLabel.text = LanguageManager.TotalPayable + ": " + "\(dayPayable)" + " "  + Constants.currencySymbol
            }
            
            
            guard let payable = Double(purchaseItem.dayPayable) else{
                return cell
            }
            
            if payable > 0.0 {
                cell.dayPurchaseAmountLabel.textColor = UIColor.lightRed
            }else{
                cell.dayPurchaseAmountLabel.textColor = UIColor(red: 0, green: 100, blue: 0)
            }
            
            cell.dayPayableLabel.text = String(describing: purchaseItem.dayPurchaseAmount) + " "  + Constants.currencySymbol
            if isLoading == false && indexPath.row == self.purchaseBookList.count - 1{
                self.isLoading = true
                if isSearchActive == false{
                    if self.currentPage < self.lastPageNumber {
                        self.currentPage += 1
                        self.presenter.getRawMaterialPurchaseDataFromServer(page: self.currentPage)
                    }
                }else{
                    if self.searchPage < self.lastPageNumber {
                        self.searchPage += 1
                        guard let startDate = self.datePickerTextField.text else{
                            return cell
                        }
                        guard let endDate = self.toDatePickerTextField.text else{
                            return cell
                        }
                        self.presenter.getRawMaterialPurchaseSearchDataFromServer(page: self.searchPage, startDate: startDate, endDate: endDate)
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.purchaseBookList.count > 0 {
            let purchaseItem = self.purchaseBookList[indexPath.row]
            self.navigateToRawMaterialPerDayPurchaseListVC(selectedDate: purchaseItem.date)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToRawMaterialPerDayPurchaseListVC(selectedDate : String){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RawMaterialPerDayPurchaseListViewController") as! RawMaterialPerDayPurchaseListViewController
        viewController.selectedDate = selectedDate
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

// MARK: API Delegate
extension RawMaterialPurchaseListViewController : PurchaseRawMaterialListViewDelegate{
    func setPurchaseList(data: PurchaseDataMapper) {
        
        guard let list = data.purchaseBook, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.purchaseBookList += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNumber = pagination.lastPageNo
        
        guard let summeryItem = data.summary else {
            return
        }
        self.purchaseSummery = summeryItem
        self.setUpAutoScroll()
    }
    
    func onFailed(message: String) {
        showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.purchaseBookList = []
        self.presenter.getRawMaterialPurchaseDataFromServer(page: self.currentPage)
    }
    
}

