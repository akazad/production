//
//  AddRawMaterialCategoryViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class AddRawMaterialCategoryViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var presenter = AddRawMaterialCategoryPresenter(service: RawMaterialService())
    
    var rawMaterialCategories : [RawMaterialCategories] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var list : [RawMaterialCategories] = []
    
    var categoryName : String?
    var categoryParentId: Int = 0
    var categoryId : Int?
    //var gen : Int = 1
    var parentId : Int?
    var currentPage : Int = 1
    var categoryNameString : String = ""
    
    var nameString : [Int : String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationBarSetup()
        self.title = LanguageManager.SelectCategory
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.configureTableView()
        self.setUpInitialLanguage()
        self.categoryNameString = ""
        self.attachPresenter()
    }
    
    func navigationBarSetup(){
        let categoryAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        categoryAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        categoryAddBtn.addTarget(self, action: #selector(onCategoryAdd(sender:)), for: UIControl.Event.touchUpInside)
        categoryAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        categoryAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        categoryAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: categoryAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2], animated: true)
    }
    
    @objc func onCategoryAdd(sender: UIBarButtonItem){
        self.categoryParentId = 0
        alertWithTextField()
    }
    
}


extension AddRawMaterialCategoryViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductCategoryCell.nib, forCellReuseIdentifier: ProductCategoryCell.identifier)
        self.tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rawMaterialCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductCategoryCell = tableView.dequeueReusableCell(withIdentifier: ProductCategoryCell.identifier, for: indexPath) as! ProductCategoryCell
        
        cell.selectionStyle = .none
        
        let item = self.rawMaterialCategories[indexPath.row]
        
        cell.categoryItemLbl.text = item.name
        
        cell.subCategoryAddBtn.isHidden = true
        
        cell.subCategoryAddBtn.tag = item.id
        cell.subCategoryAddBtn.addTarget(self, action: #selector(onSubcategoryAdd), for: .touchUpInside)
        
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard self.rawMaterialCategories.count > 0 else {
            return
        }
        let item = self.rawMaterialCategories[indexPath.row]
        let id = item.id
        self.categoryId = id
        self.parentId = item.parent
        
        self.categoryNameString += item.name
        
        let gen = item.gen
        let name = item.name
        self.nameString [gen] = name
        
        self.navigateToAddRawMaterialBaseInfoViewController()
        
        //self.didSelect(categories: self.list, categoryId: id)
        
    }
    
    
//    func didSelect(categories: [RawMaterialCategories], categoryId: Int){
//
//        var flag = 0
//
//        for item in categories{
//            if item.parent == categoryId{
//                flag = 1
//                break
//            }
//        }
//
//        if flag == 0{
//            self.navigateToAddRawMaterialBaseInfoViewController()
//        }
//        else {
//            self.navigateToRawMaterialSubCategoryVC(id : categoryId)
//        }
//    }
    
    @objc func onSubcategoryAdd(sender: UIButton){
        let id = sender.tag
        for item in self.rawMaterialCategories {
            if id == item.id {
                self.categoryParentId = id
                self.nameString [item.gen] = item.name
            }
        }
        self.alertWithTextField()
    }
    
    func navigateToRawMaterialSubCategoryVC(id : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddRawMaterialSubCategoryViewController") as! AddRawMaterialSubCategoryViewController
        viewController.rawMaterialCategories = self.list
        viewController.id = id
        viewController.parentId = self.parentId
        viewController.nameString = self.nameString
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToAddRawMaterialBaseInfoViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddRawMaterialBaseInfoViewController") as! AddRawMaterialBaseInfoViewController
        viewController.nameString = self.nameString
        if let categoryId = self.categoryId {
            viewController.rawMaterialCategoryId = categoryId
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}


extension AddRawMaterialCategoryViewController : AddRawMaterialCategoryViewDelegate{
    func setRawMaterialCategoriesList(categoryData: RawMaterialCategoriesDataMapper) {
        guard let categories = categoryData.rawMaterialCategories, categories.count > 0 else{
            return
        }
        
        self.list = categories
        self.rawMaterialCategories = categories
        self.rawMaterialCategories = self.rawMaterialCategories.filter({$0.parent == 0})
    }
    
    
    func setCategoryAddData(data: CategoryAddDataMapper) {
        guard let categoryData = data.category else {
            return
        }
        self.categoryParentId = categoryData.parent
        self.categoryId = categoryData.id
        self.nameString [categoryData.gen] = categoryData.name
        
        self.refreshTableView()
        
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
        
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getRawMaterialCategoryListFromServer(page: self.currentPage)
    }
}

extension AddRawMaterialCategoryViewController{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewCategoryAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.CategoryName
            textField.textAlignment = .center
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showMessage(userMessage: LanguageManager.CategoryIsRequired)
                return
            }
            
            self.categoryName = textField.text?.lowercased()
            
            for category in self.rawMaterialCategories{
                let existedCategory = category.name.lowercased()
                if self.categoryName == existedCategory{
                    self.showAlert(title: LanguageManager.CategoryAlreadyExists, message: "")
                    return
                }
            }
            
            let param : [String : Any] = ["name": textField.text!, "parent": self.categoryParentId]
            
            self.presenter.postRawMaterialCategoryAddDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func showMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.alertWithTextField()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigateToAddRawMaterialBaseInfoViewController()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
