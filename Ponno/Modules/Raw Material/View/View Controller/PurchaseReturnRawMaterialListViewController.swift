//
//  PurchaseReturnRawMaterialListViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

protocol PurchaseReturnRawMaterialDelegate : NSObjectProtocol{
    func setRawMaterialData(quanity : Double, refundPrice : Double)
}

class PurchaseReturnRawMaterialListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    
    fileprivate var presenter = RawMaterialListPresenter(service: RawMaterialService())
    
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var rawMaterialList : [MaterialList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var searchText = ""
    var timer : Timer?
    
    var currentPage : Int = 1
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    
    var rawMaterialId : Int?
    
    var crossImage = UIImage(named: "error_red.png")
    
    var selectedRawMaterialName : String?
    var selectedRawMaterialId : [Int] = []
    var selectedQuantities : [Double] = []
    var selectedRefundsPrices : [Double] = []
    
    var selectedRawMaterial : MaterialList?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpSearchBar()
        self.setUpViews()
        self.configureTableVIew()
        self.attachPresenter()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
        self.setUpInitialLanguage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext(sender:)), for: .touchUpInside)
        self.title = LanguageManager.SelectRawMaterial
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func onNext(sender : UIButton){
        if self.selectedRawMaterialId.count <= 0 {
            self.showAlert(title: LanguageManager.SelectARawMaterialToReturn, message: "")
        }else{
            self.navigateToPurchaseReturnRawMaterialAccessoryInfoVC()
        }
    }
    
    func navigateToPurchaseReturnRawMaterialAccessoryInfoVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseReturnRawMaterialAccessoryInfoViewController") as! PurchaseReturnRawMaterialAccessoryInfoViewController
        
        viewController.selectedRawMaterialId = self.selectedRawMaterialId
        viewController.selectedQuantities = self.selectedQuantities
        viewController.selectedRefundPrices = self.selectedRefundsPrices
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(RawMaterialListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.rawMaterialList = []
        self.currentPage = 1
        self.presenter.getRawMaterialListDataFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }
}
//MARK: SearchBar Delegate
extension PurchaseReturnRawMaterialListViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.RawMaterialSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.rawMaterialList = []
        self.isLoading = true
        self.isSearchActive = true
        
        self.presenter.getRawMaterialSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
    }
}

//MARK: TableView Delegate and Data Source
extension PurchaseReturnRawMaterialListViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableVIew(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(RawMaterialListCell.nib, forCellReuseIdentifier: RawMaterialListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 135
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.rawMaterialList.count > 0  {
            return self.rawMaterialList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : RawMaterialListCell = tableView.dequeueReusableCell(withIdentifier: RawMaterialListCell.identifier, for: indexPath) as! RawMaterialListCell
        cell.selectionStyle = .none
        if self.rawMaterialList.count > 0 {
            let rawMaterial = self.rawMaterialList[indexPath.row]
            
            cell.rawMaterial = rawMaterial
            
            if self.selectedRawMaterialId.contains(rawMaterial.materialId){
                cell.selectBtn.isHidden = false
            }
            else{
                cell.selectBtn.isHidden = true
            }
            
//            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
//            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.rawMaterialImage + rawMaterial.image), placeholderImage: UIImage(named: "Product-1"))
//
//            cell.nameLabel.text = String(describing: rawMaterial.name)
//            cell.variantLabel.text = "Variant" + ": " + String(describing: rawMaterial.variant)
//
//            let buyingPrice = rawMaterial.buyingPrice
//            if buyingPrice.isEmpty{
//                cell.buyingPriceLbl.text = "Buying Price" + " : " + "0.00" + Constants.currencySymbol
//            }else{
//                cell.buyingPriceLbl.text = "Buying Price" + " : " + rawMaterial.buyingPrice + Constants.currencySymbol
//            }
//
//            let quantity = rawMaterial.quantity
//            if quantity.isEmpty{
//                cell.quantityLabel.text =  "Quantity" +  ": " + String(describing: 0.00) + " " + rawMaterial.unit
//            }else{
//                cell.quantityLabel.text =  "Quantity" +  ": " + String(describing: rawMaterial.quantity) + " " + rawMaterial.unit
//
//                if let quantity = Double(rawMaterial.quantity){
//                    let stockAlert = Double(rawMaterial.stockAlert)
//                    if quantity <= stockAlert{
//                        cell.quantityLabel.textColor = UIColor.red
//                    }
//                }
//            }
//
//            cell.categoryLabel.text = "Category" + ": " + String(describing: rawMaterial.categoryName)
            
            self.selectedRawMaterialName = rawMaterial.name
            cell.selectBtn.tag = rawMaterial.materialId
            cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.rawMaterialList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getRawMaterialListDataFromServer(page: self.currentPage)
            }
        }
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onRemove(sender : UIButton){
        let rawMaterialId = sender.tag
        
        if self.selectedRawMaterialId.contains(rawMaterialId){
            removeDataofRawMaterial(id: rawMaterialId)
        }
    }
    
    func removeDataofRawMaterial(id : Int){
        var productAtIndex = -1
        for index in 0..<self.selectedRawMaterialId.count{
            if self.selectedRawMaterialId[index] == id{
                productAtIndex = index
            }
        }
        
        self.selectedQuantities.remove(at: productAtIndex)
        self.selectedRefundsPrices.remove(at: productAtIndex)
        self.selectedRawMaterialId.remove(at: productAtIndex)
        
        self.refreshTableView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.rawMaterialList.count > 0 {
            let rawMaterial = self.rawMaterialList[indexPath.row]
            
            if self.selectedRawMaterialId.contains(rawMaterial.materialId){
                self.showAlert(title: LanguageManager.TheRawMaterialIsAlreadySelectedDeselectItThenUpdate, message: "")
            }else{
                self.selectedRawMaterial = rawMaterial
                
                let quantity = rawMaterial.quantity
                if quantity.isEmpty == false && quantity != "0.00"{
                    self.navigateToPurchaseReturnRawMaterialBaseInfoVC(rawMaterial: rawMaterial)
                }else{
                    self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
                }
            }
        }
    }
    
    func navigateToPurchaseReturnRawMaterialBaseInfoVC(rawMaterial : MaterialList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseReturnRawMaterialBaseInfoViewController") as! PurchaseReturnRawMaterialBaseInfoViewController
        viewController.rawMaterialList = rawMaterial
        viewController.delegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

//Mark: Api Delegate
extension PurchaseReturnRawMaterialListViewController : RawMaterialListViewDelegate{
    func setRawMaterialData(data: RawMaterialListDataMapper) {
        
        if let list = data.materialList, list.count > 0 {
            self.isLoading = false
            self.rawMaterialList += list
        }
        
        
        if let pagination = data.pagination {
            self.lastPageNo = pagination.lastPageNo
        }
        
    }
    
    func setRawMaterialSearchData(data: RawMaterialListDataMapper) {
        guard let list = data.materialList else{
            return
        }
        self.isLoading = false
        self.rawMaterialList += list
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.currentPage = 1
        self.rawMaterialList = []
        self.presenter.getRawMaterialListDataFromServer(page: self.currentPage)
    }
    
}

//ProductDelegate
extension PurchaseReturnRawMaterialListViewController : PurchaseReturnRawMaterialDelegate{
    func setRawMaterialData(quanity: Double, refundPrice: Double) {
        guard let selectedRawMaterial = self.selectedRawMaterial else{
            return
        }
        self.selectedRawMaterialId.append(selectedRawMaterial.materialId)
        self.selectedRefundsPrices.append(refundPrice)
        self.selectedQuantities.append(quanity)
        self.refreshTableView()
    }
    
}
