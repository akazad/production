//
//  DamageableRawMaterialBaseInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 24/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DamageableRawMaterialBaseInfoViewController: UIViewController {

    var rawMaterial : MaterialList?
    var delegate : damageableRawMaterialDelegate?
    
    @IBOutlet weak var quantityLbl: UILabel!{
        didSet{
            quantityLbl.text = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var quantityTextField: UITextField!{
        didSet{
            quantityTextField.placeholder = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var buyingPriceLbl: UILabel!{
        didSet{
            buyingPriceLbl.text = LanguageManager.BuyingPrice
        }
    }
    @IBOutlet weak var buyingPriceTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!{
        didSet{
            descriptionLbl.text = LanguageManager.Description
        }
    }
    @IBOutlet weak var descriptionTextField: UITextField!{
        didSet{
            descriptionTextField.placeholder = LanguageManager.Description
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    
    var datePicker = UIDatePicker()
    var descriptionText : String = ""
    var quantity : Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpViews()
        self.toolBarSetUp()
        self.initialSetUp()
        self.configureTextFiled()
    }
    
    
    func initialSetUp(){
        
        guard let rawMaterial = self.rawMaterial else {
            return
        }
        self.buyingPriceTextField.text = rawMaterial.buyingPrice
        self.quantity = Double(rawMaterial.quantity)
        //self.buyingPriceTextField.textColor = UIColor(red: 190, green: 186, blue: 167)
        self.buyingPriceTextField.isEnabled = false
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext), for: .touchUpInside)
    }
    
    func toolBarSetUp(){
        //QuantityToolBar
        let quantityToolBar = UIToolbar()
        quantityToolBar.barStyle = UIBarStyle.default
        quantityToolBar.isTranslucent = true
        quantityToolBar.tintColor = UIColor.black
        quantityToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
        
        quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        quantityToolBar.isUserInteractionEnabled = true
        
        self.quantityTextField.inputAccessoryView = quantityToolBar
        
//        //BuyingPriceToolBar
//        let buyingPriceToolBar = UIToolbar()
//        buyingPriceToolBar.barStyle = UIBarStyle.default
//        buyingPriceToolBar.isTranslucent = true
//        buyingPriceToolBar.tintColor = UIColor.black
//        buyingPriceToolBar.sizeToFit()
//        
//        let buyingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnBuyingPrice(sender:)))
//        
//        buyingPriceToolBar.setItems([cancelButton, spaceButton, buyingPriceDoneButton], animated: false)
//        buyingPriceToolBar.isUserInteractionEnabled = true
//        
//        self.buyingPriceTextField.inputAccessoryView = buyingPriceToolBar
        
    }
    
    @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
        self.quantityTextField.resignFirstResponder()
        self.descriptionTextField.becomeFirstResponder()
    }
    
//    @objc func onPressingDoneOnBuyingPrice(sender: UIBarButtonItem){
//        self.buyingPriceTextField.resignFirstResponder()
//
//    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onNext(sender : UIButton){
        if self.isValidated(){
            guard let quantity = self.quantityTextField.text, let quantityDouble = Double(quantity), let buyingprice = self.buyingPriceTextField.text, let buyingPriceDouble = Double(buyingprice) else{
                return
            }
            self.descriptionText = self.descriptionTextField.text ?? ""
            self.delegate?.setDamagedMaterialData(quantity: quantityDouble, buyingPrice: buyingPriceDouble, description: self.descriptionText)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func isValidated()->Bool{
        if self.quantityTextField.text == ""{
            self.showAlert(title: LanguageManager.QuantityIsRequired, message: "")
            return false
        }else if let quantityCheck = self.quantity, let quantityText = self.quantityTextField.text, let quantity = Double(quantityText)  {
            if quantity > quantityCheck {
                self.showAlert(title: LanguageManager.QuantityCantBeGreaterThan + " \(quantityCheck)", message: "")
                return false
            }
        }
        else if self.buyingPriceTextField.text == ""{
            self.showAlert(title: LanguageManager.BuyingPriceIsRequired, message: "")
            return false
        }
        return true
    }
    

}

//Mark: TextField Delegate
extension DamageableRawMaterialBaseInfoViewController : UITextFieldDelegate {
    func configureTextFiled(){
        self.quantityTextField.delegate = self
        self.buyingPriceTextField.delegate = self
        self.descriptionTextField.delegate = self
        self.quantityTextField.underlined()
        self.buyingPriceTextField.underlined()
        self.descriptionTextField.underlined()
    }
}
