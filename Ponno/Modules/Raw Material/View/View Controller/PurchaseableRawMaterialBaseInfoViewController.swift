//
//  PurchaseableRawMaterialBaseInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PurchaseableRawMaterialBaseInfoViewController: UIViewController {
    
    var rawMaterial : MaterialList?
    var delegate : purchaseableNewRawMaterialDelegate?

    @IBOutlet weak var quantityLbl: UILabel!{
        didSet{
            quantityLbl.text = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var quantityTextField: UITextField!{
        didSet{
            quantityTextField.placeholder = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var buyingPriceLbl: UILabel!{
        didSet{
            buyingPriceLbl.text = LanguageManager.BuyingPrice
        }
    }
    @IBOutlet weak var buyingPriceTextField: UITextField!
    @IBOutlet weak var expireDateLbl: UILabel!{
        didSet{
            expireDateLbl.text = LanguageManager.ExpireDate
        }
    }
    @IBOutlet weak var expireDateTextField: UITextField!{
        didSet{
            expireDateTextField.placeholder = "dd/mm/yyyy"
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    
    var datePicker = UIDatePicker()
    var expireDate : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpInitialLanguage()
        self.setUpViews()
        self.toolBarSetUp()
        self.initialSetUp()
        self.configureTextFiled()
        self.showExpireDatePicker()
    }
    
    
    func initialSetUp(){
        
        
        guard let rawMaterial = self.rawMaterial else {
            return
        }
        self.buyingPriceTextField.text = rawMaterial.buyingPrice
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext), for: .touchUpInside)
    }
    
    func toolBarSetUp(){
        //QuantityToolBar
        let quantityToolBar = UIToolbar()
        quantityToolBar.barStyle = UIBarStyle.default
        quantityToolBar.isTranslucent = true
        quantityToolBar.tintColor = UIColor.black
        quantityToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
        
        quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        quantityToolBar.isUserInteractionEnabled = true
        
        self.quantityTextField.inputAccessoryView = quantityToolBar
        
        //BuyingPriceToolBar
        let buyingPriceToolBar = UIToolbar()
        buyingPriceToolBar.barStyle = UIBarStyle.default
        buyingPriceToolBar.isTranslucent = true
        buyingPriceToolBar.tintColor = UIColor.black
        buyingPriceToolBar.sizeToFit()
        
        let buyingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnBuyingPrice(sender:)))
        
        buyingPriceToolBar.setItems([cancelButton, spaceButton, buyingPriceDoneButton], animated: false)
        buyingPriceToolBar.isUserInteractionEnabled = true
        
        self.buyingPriceTextField.inputAccessoryView = buyingPriceToolBar
        
//        //ExpireDateToolBar
//        let expireDateToolBar = UIToolbar()
//        expireDateToolBar.barStyle = UIBarStyle.default
//        expireDateToolBar.isTranslucent = true
//        expireDateToolBar.tintColor = UIColor.black
//        expireDateToolBar.sizeToFit()
//
//        let expireDateDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnExpireDate(sender:)))
//
//        expireDateToolBar.setItems([cancelButton, spaceButton, expireDateDoneButton], animated: false)
//        expireDateToolBar.isUserInteractionEnabled = true
//
//        self.expireDateTextField.inputAccessoryView = expireDateToolBar
        
    }
    
    @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
        self.quantityTextField.resignFirstResponder()
        self.buyingPriceTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnBuyingPrice(sender: UIBarButtonItem){
        self.buyingPriceTextField.resignFirstResponder()
        self.expireDateTextField.becomeFirstResponder()
    }
    
//    @objc func onPressingDoneOnExpireDate(sender: UIBarButtonItem){
//        self.expireDateTextField.resignFirstResponder()
//        self.nextBtn.becomeFirstResponder()
//    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onNext(sender : UIButton){
        if self.isValidated(){
            guard let quantity = self.quantityTextField.text, let quantityDouble = Double(quantity), let buyingprice = self.buyingPriceTextField.text, let buyingPriceDouble = Double(buyingprice) else{
                return
            }
            self.delegate?.setRawMaterialData(quantity: quantityDouble, buyingPrice: buyingPriceDouble, expireDate: self.expireDate)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func isValidated()->Bool{
        if self.quantityTextField.text == ""{
            self.showAlert(title: LanguageManager.QuantityIsRequired, message: "")
            return false
        }else if self.buyingPriceTextField.text == ""{
            self.showAlert(title: LanguageManager.BuyingPriceIsRequired, message: "")
            return false
        }
        return true
    }


}

extension PurchaseableRawMaterialBaseInfoViewController {
    func showExpireDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        expireDateTextField.inputAccessoryView = toolbar
        expireDateTextField.inputView = datePicker
        
    }
    
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        expireDateTextField.text = formatter.string(from: datePicker.date)
        self.expireDate = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}

//Mark: TextField Delegate
extension PurchaseableRawMaterialBaseInfoViewController : UITextFieldDelegate {
    func configureTextFiled(){
        self.quantityTextField.delegate = self
        self.buyingPriceTextField.delegate = self
        self.expireDateTextField.delegate = self
        self.quantityTextField.underlined()
        self.buyingPriceTextField.underlined()
        self.expireDateTextField.underlined()
    }
}
