//
//  AddRawMaterialSubCategoryViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class AddRawMaterialSubCategoryViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = AddRawMaterialCategoryPresenter(service: RawMaterialService())
    
    var id : Int = -1
    var filteredCategory : [RawMaterialCategories] = []
    var rawMaterialCategories : [RawMaterialCategories] = []
    var categoryNameString : String = ""
    var currentPage : Int = 1
    var parentId : Int?
    var categoryId : Int?
    var nameString : [Int : String] = [:]
    var categoryName : String?
        
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationBarSetup()
        self.configureTableView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.filteredCategory = self.rawMaterialCategories.filter({$0.parent == self.id})
        self.refreshTableView()
    }
    
    func navigationBarSetup(){
        let productAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        productAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        productAddBtn.addTarget(self, action: #selector(onSubCategoryAdd(sender:)), for: UIControl.Event.touchUpInside)
        productAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        productAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        productAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: productAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2], animated: true)
    }
    
    @objc func onSubCategoryAdd(sender: UIBarButtonItem){
        //self.categoryParentId = 0
        alertWithTextField()
    }
    

}

//Mark: TableView Delegate
extension AddRawMaterialSubCategoryViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductCategoryCell.nib, forCellReuseIdentifier: ProductCategoryCell.identifier)
        self.tableView.separatorStyle = .none
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductCategoryCell = tableView.dequeueReusableCell(withIdentifier: ProductCategoryCell.identifier, for: indexPath) as! ProductCategoryCell
        
        cell.selectionStyle = .none
        
        let item = self.filteredCategory[indexPath.row]
        
        cell.categoryItemLbl.text = item.name
        
        cell.subCategoryAddBtn.tag = item.id
        cell.subCategoryAddBtn.addTarget(self, action: #selector(onSubcategoryAdd), for: .touchUpInside)
        
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
    @objc func onSubcategoryAdd(sender: UIButton){
        self.id = sender.tag
        for item in self.rawMaterialCategories {
            if id == item.id {
                self.parentId = item.parent
                self.nameString [item.gen] = item.name
            }
        }
        self.alertWithTextField()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = self.filteredCategory[indexPath.row]
        
        let categoryName = item.name
        self.categoryId = item.id
        self.categoryNameString += " / " + "\(categoryName)"
        
        let gen = item.gen
        let name = item.name
        
        
        for (key , _) in self.nameString {
            if key == gen {
                self.nameString.removeValue(forKey: key)
                self.nameString [gen] = name
            }else{
                self.nameString [gen] = name
            }
        }
        
        self.parentId = item.parent
        
        self.didSelect(categories: self.rawMaterialCategories, categoryId: self.categoryId ?? -1)
    }
    
    func didSelect(categories: [RawMaterialCategories], categoryId: Int){
        var flag = 0
        
        for item in categories{
            if item.parent == categoryId{
                flag = 1
                break
            }
        }
        
        if flag == 0{
            self.navigateToAddRawMaterialBaseInfoViewController()
        }
        else {
            self.navigateToRawMaterialSubCategoryVC(id : categoryId)
        }
    }
    
    func navigateToRawMaterialSubCategoryVC(id : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddRawMaterialSubCategoryViewController") as! AddRawMaterialSubCategoryViewController
        viewController.rawMaterialCategories = self.rawMaterialCategories
        viewController.nameString = self.nameString
        viewController.id = id
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddRawMaterialBaseInfoViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddRawMaterialBaseInfoViewController") as! AddRawMaterialBaseInfoViewController
        viewController.nameString = self.nameString
        if let categoryId = self.categoryId {
           viewController.rawMaterialCategoryId = categoryId
        }
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

//Mark: Api Delegate
extension AddRawMaterialSubCategoryViewController : AddRawMaterialCategoryViewDelegate{
    func setRawMaterialCategoriesList(categoryData: RawMaterialCategoriesDataMapper) {
        guard let categories = categoryData.rawMaterialCategories, categories.count > 0 else{
            return
        }
        
        let subCategoryItem = categories
        
        let subCategories = subCategoryItem.filter({$0.parent == id})
        self.rawMaterialCategories = subCategories
    }
    
    func setCategoryAddData(data: CategoryAddDataMapper) {
        guard let categoryData = data.category, let message = data.message else {
            return
        }
        
        self.id = categoryData.parent
        self.categoryId = categoryData.id
        self.nameString [categoryData.gen] = categoryData.name
        self.refreshTableView()
        
        self.successMessage(userMessage: message)
        
        
    }
    
    func onFailed(data: String) {
        self.showAlert(title: "No Category Found", message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

extension AddRawMaterialSubCategoryViewController{
    func alertWithTextField(){
        let alertController = UIAlertController(title: "New Sub Category Add", message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = "Sub Category Name"
            textField.textAlignment = .center
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showMessage(userMessage: "Sub Category name is empty")
                return
            }
            
            let categoryName = textField.text?.lowercased()
            
            for category in self.rawMaterialCategories{
                let existedCategory = category.name.lowercased()
                if categoryName == existedCategory{
                    self.showAlert(title: "Sub Category is already exist", message: "")
                    return
                }
            }
            
            let param : [String : Any] = ["name": textField.text!, "parent": self.id]
            
            self.presenter.postRawMaterialCategoryAddDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: "Warning", message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.alertWithTextField()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: "Successful", message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigateToAddRawMaterialBaseInfoViewController()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
