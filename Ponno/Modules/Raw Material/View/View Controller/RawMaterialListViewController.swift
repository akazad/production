//
//  RawMaterialListViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

class RawMaterialListViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var presenter = RawMaterialListPresenter(service: RawMaterialService())
    
    var timer : Timer?
    var rawMaterialList : [MaterialList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var rawMaterialListSummery : [Summary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var searchText = ""
    
    var currentPage : Int = 1
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    
    var rawMaterialId : Int?
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.setNavigationBarTitle()
        self.configureTableVIew()
        self.configureCollectionView()
        self.addFloaty()
        self.setBarButton()
        
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.RawMaterial
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0))]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setBarButton(){
        
        let popUpBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        popUpBtn.setImage(UIImage(named: "popupmenudark"), for: UIControl.State.normal)
        popUpBtn.addTarget(self, action: #selector(onBarPopUpBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        popUpBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        popUpBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        popUpBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn2 = UIBarButtonItem(customView: popUpBtn)

        let purchaseBookBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        purchaseBookBtn.setImage(UIImage(named: "purchaseBarBook"), for: UIControl.State.normal)
        purchaseBookBtn.addTarget(self, action: #selector(onBarPurchaseBookBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        purchaseBookBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        purchaseBookBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        purchaseBookBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn3 = UIBarButtonItem(customView: purchaseBookBtn)
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn1 = UIBarButtonItem(customView: searchBtn)
        
        navigationItem.setRightBarButtonItems([barBtn2, barBtn3, barBtn1], animated: true)
    }
    
    @objc func onBarPopUpBtnTapped(sender : UIBarButtonItem){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        let productionAction = UIAlertAction(title: LanguageManager.Production, style: UIAlertAction.Style.default) { (action) in
            self.navigateToProductionRawMaterialListViewController()
        }
        
        let newRawMaterialAction = UIAlertAction(title: LanguageManager.NewRawMaterial, style: UIAlertAction.Style.default) { (action) in
            self.navigateToAddNewRawMaterialViewController()
        }
        
        let damagedRawMaterialAction = UIAlertAction(title: LanguageManager.DamagedRawMaterial, style: UIAlertAction.Style.default) { (action) in
            self.navigateToDamagedRawMaterialViewController()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
            
            self.view.endEditing(true)
        }
        
        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
        
        myActionSheet.addAction(productionAction)
        myActionSheet.addAction(newRawMaterialAction)
        myActionSheet.addAction(damagedRawMaterialAction)
        myActionSheet.addAction(cancelAction)
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    @objc func onSearch(sender: UIBarButtonItem){
        self.navigateToRawMaterialSearchVC()
    }
    
    @objc func onBarPurchaseBookBtnTapped(sender: UIBarButtonItem){
        self.navigateToRawMaterialPurchaseListViewController()
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(RawMaterialListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.rawMaterialList = []
        self.currentPage = 1
        self.presenter.getRawMaterialListDataFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }
    
}

//Mark: Floaty Delegate
extension RawMaterialListViewController: FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}

//MARK: CollectionView Delegate And Data Source
extension RawMaterialListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(ProductListSummeryCell.nib, forCellWithReuseIdentifier: ProductListSummeryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let item = self.rawMaterialListSummery, item.count > 0  else {
            return 0
        }
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProductListSummeryCell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductListSummeryCell.identifier, for: indexPath) as! ProductListSummeryCell
        guard let item = self.rawMaterialListSummery, item.count > 0 else{
            return cell
        }
        
//        for (key, _) in item.enumerated() {
//            if key == 0 {
//                item[0].title = "Total Stock"
//            }else if key == 1 {
//                item[1].title = "Total Stock Amount"
//            }else if key == 2 {
//                item[2].title = "Total Category"
//            }
//        }
        
        cell.summeryTitle.text = item[indexPath.row % item.count].title
        cell.summeryValue.text = item[indexPath.row % item.count].value
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70.0)
    }
    
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.rawMaterialListSummery, list.count > 0 else{
            return
        }
        let listCount = list.count
        
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//MARK: TableView Delegate and Data Source
extension RawMaterialListViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableVIew(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(RawMaterialListCell.nib, forCellReuseIdentifier: RawMaterialListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 135
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.rawMaterialList.count > 0  {
            return self.rawMaterialList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : RawMaterialListCell = tableView.dequeueReusableCell(withIdentifier: RawMaterialListCell.identifier, for: indexPath) as! RawMaterialListCell
        cell.selectionStyle = .none
        if self.rawMaterialList.count > 0 {
            let rawMaterial = self.rawMaterialList[indexPath.row]
            
            cell.rawMaterial = rawMaterial
//
//            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
//            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.rawMaterialImage + rawMaterial.image), placeholderImage: UIImage(named: "placeholder"))
//
//            cell.nameLabel.text = String(describing: rawMaterial.name)
//            cell.variantLabel.text = "Variant" + ": " + String(describing: rawMaterial.variant)
//
//            let buyingPrice = rawMaterial.buyingPrice
//            if buyingPrice.isEmpty{
//                cell.buyingPriceLbl.text = "Buying Price" + " : " + "0.00" + Constants.currencySymbol
//            }else{
//                cell.buyingPriceLbl.text = "Buying Price" + " : " + rawMaterial.buyingPrice + Constants.currencySymbol
//            }
//
//            let quantity = rawMaterial.quantity
//            if quantity.isEmpty{
//                cell.quantityLabel.text =  "Quantity" +  ": " + String(describing: 0.00) + " " + rawMaterial.unit
//            }else{
//                cell.quantityLabel.text =  "Quantity" +  ": " + String(describing: rawMaterial.quantity) + " " + rawMaterial.unit
//
//                if let quantity = Double(rawMaterial.quantity){
//                    let stockAlert = Double(rawMaterial.stockAlert)
//                    if quantity <= stockAlert{
//                        cell.quantityLabel.textColor = UIColor.red
//                    }
//                }
//            }
//
//            cell.categoryLabel.text = "Category" + ": " + String(describing: rawMaterial.categoryName)
            
            cell.selectBtn.isHidden = true
            if isLoading == false && indexPath.row == self.rawMaterialList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getRawMaterialListDataFromServer(page: self.currentPage)
            }
        }
        return cell
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.rawMaterialList.count > 0 {
            let listItem = self.rawMaterialList[indexPath.row]
            self.navigateToRawMaterialDetailsBaseVC(rawMaterial: listItem)
        }
    }
    
    func navigateToRawMaterialDetailsBaseVC(rawMaterial : MaterialList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RawMaterialDetailsBaseViewController") as! RawMaterialDetailsBaseViewController
        viewController.materialInventoryId = rawMaterial.materialInventoryId
        viewController.rawMaterialName = rawMaterial.name
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

//Mark: Api Delegate
extension RawMaterialListViewController : RawMaterialListViewDelegate{
    func setRawMaterialData(data: RawMaterialListDataMapper) {
        if let summeryList = data.summary {
            self.rawMaterialListSummery = summeryList
            self.setUpAutoScroll()
        }
        
        if let list = data.materialList, list.count > 0 {
            self.isLoading = false
            self.rawMaterialList += list
        }
        
        
        if let pagination = data.pagination {
            self.lastPageNo = pagination.lastPageNo
        }
        
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.currentPage = 1
        self.rawMaterialList = []
        self.presenter.getRawMaterialListDataFromServer(page: self.currentPage)
    }
    
}

extension RawMaterialListViewController{
    func navigateToRawMaterialSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RawMaterialSearchViewController") as! RawMaterialSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewRawMaterialViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddRawMaterialCategoryViewController") as! AddRawMaterialCategoryViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToProductionRawMaterialListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductionRawMaterialListViewController") as! ProductionRawMaterialListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToRawMaterialPurchaseListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RawMaterialPurchaseListViewController") as! RawMaterialPurchaseListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToDamagedRawMaterialViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamagedRawMaterialListViewController") as! DamagedRawMaterialListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
