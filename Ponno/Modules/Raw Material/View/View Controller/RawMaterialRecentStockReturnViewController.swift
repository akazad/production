//
//  RawMaterialRecentStockReturnViewController.swift
//  Ponno
//
//  Created by a k azad on 18/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class RawMaterialRecentStockReturnViewController: UIViewController {
    
    @IBOutlet weak var rawMaterialNameLbl: UILabel!{
        didSet{
            rawMaterialNameLbl.text = LanguageManager.RawMaterialName
        }
    }
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var quantityLbl: UILabel!{
        didSet{
            quantityLbl.text = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var quantityTextField: UITextField!{
        didSet{
            quantityTextField.placeholder = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var cashBackLbl: UILabel!{
        didSet{
            cashBackLbl.text = LanguageManager.CashBack
        }
    }
    @IBOutlet weak var cashBackTextField: UITextField!{
        didSet{
            cashBackTextField.placeholder = LanguageManager.CashBack
        }
    }
    @IBOutlet weak var totalLbl: UILabel!{
        didSet{
            totalLbl.text = LanguageManager.Total
        }
    }
    @IBOutlet weak var totalTextField: UITextField!{
        didSet{
            totalTextField.text = LanguageManager.Total
        }
    }
    @IBOutlet weak var vendorLbl: UILabel!{
        didSet{
            vendorLbl.text = LanguageManager.Vendor
        }
    }
    @IBOutlet weak var vendorsTextField: UITextField!{
        didSet{
            vendorsTextField.placeholder = LanguageManager.Vendor
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = RawMaterialRecentStockReturnPresenter(service: RawMaterialService())
    
    var recentStock : RawMaterialRecentStocksList?
    
    var vendorPicker = UIPickerView()
    var vendorsList : [VendorsList] = []{
        didSet{
            self.vendorPicker.reloadAllComponents()
        }
    }
    var vendorId : Int?
    var vendorName : String?
    var materialId: Int?
    var inventoryId: Int?
    var rawMaterialName: String?
    
    
    let screenHeight = UIScreen.main.bounds.height
    let scrollViewContentHeight = 780 as CGFloat

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetUp()
        self.setUpPickerView()
        self.configureTextFields()
        self.setUpToolBar()
        self.attachPresenter()
        
    }
    
    func initialSetUp(){
        
        self.title = LanguageManager.RawMaterialInfo
        
        self.nameTextField.isEnabled = false
        self.totalTextField.isEnabled = false
        if let recentPurchase = self.recentStock, let name = self.vendorName  {
            self.nameTextField.text = self.rawMaterialName
            self.quantityTextField.text = recentPurchase.currentQuantity
            self.inventoryId = recentPurchase.materialInventoryHistoryId
            self.materialId = recentPurchase.materialId
            self.vendorsTextField.text = name
            self.vendorId = recentPurchase.vendorId
            
            self.cashBackTextField.text = recentPurchase.buyingPrice
            
            let buyingPrice = Double(recentPurchase.buyingPrice) ?? 0.0
            let quantity = Double(recentPurchase.quantity) ?? 0.0
            
            let totalAmouont = buyingPrice * quantity
            
            self.totalTextField.text = "\(totalAmouont)"
            
            self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
            self.quantityTextField.addTarget(self, action: #selector(quantityTextFieldDidChange), for: .editingChanged)
            self.cashBackTextField.addTarget(self, action: #selector(cashBackTextFieldDidChange), for: .editingChanged)
        }
        
    }
    
    @objc func quantityTextFieldDidChange(sender: UITextField){
        if self.quantityTextField.text != "" {
            self.refreshView()
        }
    }
    
    @objc func cashBackTextFieldDidChange(sender: UITextField){
        if self.cashBackTextField.text != "" {
            self.refreshView()
        }
    }
    
    func setUpToolBar(){
        
        //quantityToolBaar
        let quantityToolBar = UIToolbar()
        quantityToolBar.barStyle = UIBarStyle.default
        quantityToolBar.isTranslucent = true
        quantityToolBar.tintColor = UIColor.black
        quantityToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
        
        quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        quantityToolBar.isUserInteractionEnabled = true
        
        self.quantityTextField.inputAccessoryView = quantityToolBar
        
        let cashBackToolBar = UIToolbar()
        cashBackToolBar.barStyle = UIBarStyle.default
        cashBackToolBar.isTranslucent = true
        cashBackToolBar.tintColor = UIColor.black
        cashBackToolBar.sizeToFit()
        
        let cashBackDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCashBack(sender:)))
        
        cashBackToolBar.setItems([cancelButton, spaceButton, cashBackDoneButton], animated: false)
        cashBackToolBar.isUserInteractionEnabled = true
        
        self.cashBackTextField.inputAccessoryView = cashBackToolBar
    }
    
    @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
        self.cashBackTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnCashBack(sender: UIBarButtonItem){
        self.vendorsTextField.becomeFirstResponder()
    }
    
    //Mark: PickerView
    func setUpPickerView(){
        vendorPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        self.vendorsTextField.inputView = vendorPicker
        self.vendorsTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.vendorsTextField.resignFirstResponder()
        self.submitBtn.becomeFirstResponder()
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let quantityText = self.quantityTextField.text, let quantity = Double(quantityText), let totalPrice = self.totalTextField.text, let inventoryId = self.inventoryId, let materialId = self.materialId, let vendorId = self.vendorId, let cashBack = self.cashBackTextField.text else{
                return
            }
            
            guard let recentPurchase = self.recentStock else {
                return
            }
            let currentQuantity = recentPurchase.currentQuantity
            let quan = Double(currentQuantity)
            
            if let cquan = quan {
                if cquan <= 0.0 {
                    self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
                }else{
                    let param : [String: Any] = ["material_inventory_history_id": inventoryId, "material_id": materialId,  "quantity": quantity, "buying_price": cashBack, "total": totalPrice, "vendor": vendorId]
                    
                    if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
                        print(json)
                    }
                    self.presenter.postRawMaterialRecentStockReturnDataToServer(param: param)
                }
            }
        }
    }
    
    func isValidated()->Bool{
        
        if let quantityText = self.quantityTextField.text, let quantity = Double(quantityText), let quantityToCheck = recentStock?.quantity, quantity > Double(quantityToCheck)!{
            self.displayMessage(userMessage: LanguageManager.YouCantRetrunMoreThan + " \(quantityToCheck) ")
            return false
        }
        else if let buyingPriceText = self.cashBackTextField.text, let buyingPrice = Double(buyingPriceText), buyingPrice <= 0 {
            self.displayMessage(userMessage: LanguageManager.PricecannotBe0OrLess)
            return false
        }
        else if (self.vendorsTextField.text?.isEmpty)! {
            self.displayMessage(userMessage: LanguageManager.SelectAVendorToReturnProduct)
            return false
        }else{
            return true
        }
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.popToSpecificViewController()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is RawMaterialDetailsBaseViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }

}

extension RawMaterialRecentStockReturnViewController : UITextFieldDelegate{
    func configureTextFields(){
        self.nameTextField.delegate = self
        self.quantityTextField.delegate = self
        self.cashBackTextField.delegate = self
        self.totalTextField.delegate = self
        self.vendorsTextField.delegate = self
        self.nameTextField.underlined()
        self.quantityTextField.underlined()
        self.totalTextField.underlined()
        self.cashBackTextField.underlined()
        self.vendorsTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            self.quantityTextField.becomeFirstResponder()
        }else if textField == vendorsTextField{
            self.submitBtn.becomeFirstResponder()
        }else{
            self.view.endEditing(true)
        }
        return false
    }
}

//Mark: PickerViewDelegate
extension RawMaterialRecentStockReturnViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        guard self.vendorsList.count > 0 else {
            return 0
        }
        return self.vendorsList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard self.vendorsList.count > 0 else {
            return ""
        }
        let vendors = self.vendorsList[row]
        self.vendorsTextField.text = vendors.name
        self.vendorId = vendors.id
        return vendors.name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard self.vendorsList.count > 0 else {
            return
        }
        let vendors = self.vendorsList[row]
        self.vendorsTextField.text = vendors.name
        self.vendorId = vendors.id
    }
}

//Mark: Api Delegate
extension RawMaterialRecentStockReturnViewController: RawMaterialRecentStockReturnViewDelegate{
    func setVendorsList(data: VendorsDataMapper) {
        guard let list = data.vendors, list.count > 0 else {
            return
        }
        self.vendorsList = list
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.displayMessage(userMessage: data)
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getVendorsListDataFromServer(getAll: true)
    }
}

extension RawMaterialRecentStockReturnViewController{
    func refreshView(){
        guard let quantityText = self.quantityTextField.text, let quantity = Double(quantityText) else {
            return
        }
        guard let buyingPriceText = self.cashBackTextField.text, let buyingPrice = Double(buyingPriceText) else {
            return
        }
        
        let totalPrice = quantity * buyingPrice
        self.totalTextField.text = "\(totalPrice)"
    }
}
