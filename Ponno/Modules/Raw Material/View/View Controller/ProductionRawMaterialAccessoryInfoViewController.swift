//
//  ProductionRawMaterialAccessoryInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductionRawMaterialAccessoryInfoViewController: UIViewController {

    @IBOutlet weak var productLbl: UILabel!{
        didSet{
            productLbl.text = LanguageManager.Product
        }
    }
    @IBOutlet weak var productTextField: UITextField!{
        didSet{
            productTextField.placeholder = LanguageManager.SelectProduct

        }
    }
    @IBOutlet weak var quanitiyLbl: UILabel!{
        didSet{
            quanitiyLbl.text = LanguageManager.Quantity

        }
    }
    @IBOutlet weak var quantityTextField: UITextField!{
        didSet{
            quantityTextField.placeholder = LanguageManager.Quantity

        }
    }
    @IBOutlet weak var productionCostLbl: UILabel!{
        didSet{
            productionCostLbl.text = LanguageManager.ProductionCost

        }
    }
    @IBOutlet weak var productionCostTextField: UITextField!{
        didSet{
            productionCostTextField.placeholder = LanguageManager.ProductionCost

        }
    }
    @IBOutlet weak var sellingPriceLbl: UILabel!{
        didSet{
            sellingPriceLbl.text = LanguageManager.SellingPrice

        }
    }
    @IBOutlet weak var sellingPriceTextField: UITextField!{
        didSet{
            sellingPriceTextField.placeholder = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = ProductionRawMaterialAccessoryInfoPresenter(service: RawMaterialService())
    
    var productPicker = UIPickerView()
    var productList : [ProductList] = []{
        didSet{
            //self.refreshTableView()
        }
    }
    var productId: Int?
    
    var selectedRawMaterialId : [Int] = []
    var selectedQuantities : [Double] = []
    var selectedBuyingPrices : [Double] = []
    
    var productionCost : Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetup()
        self.toolBarSetUp()
        self.attachPresenter()
        
    }
    
    func initialSetup(){
        
        self.productionCostTextField.isEnabled = false
        
        self.quantityTextField.addTarget(self, action: #selector(onQuantityTextFieldEdit), for: .editingChanged)
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
    }
    
    
    
    func toolBarSetUp(){
        //QuantityToolBar
        let quantityToolBar = UIToolbar()
        quantityToolBar.barStyle = UIBarStyle.default
        quantityToolBar.isTranslucent = true
        quantityToolBar.tintColor = UIColor.black
        quantityToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
        
        quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        quantityToolBar.isUserInteractionEnabled = true
        
        self.quantityTextField.inputAccessoryView = quantityToolBar
        
        //SellingPriceToolBar
        let sellingPriceToolBar = UIToolbar()
        sellingPriceToolBar.barStyle = UIBarStyle.default
        sellingPriceToolBar.isTranslucent = true
        sellingPriceToolBar.tintColor = UIColor.black
        sellingPriceToolBar.sizeToFit()
        
        let sellingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnSellingPrice(sender:)))
        
        sellingPriceToolBar.setItems([cancelButton, spaceButton, sellingPriceDoneButton], animated: false)
        sellingPriceToolBar.isUserInteractionEnabled = true
        
        self.sellingPriceTextField.inputAccessoryView = sellingPriceToolBar
        
        //ProductToolBar
        
        productPicker.delegate = self
        
        let productToolBar = UIToolbar()
        productToolBar.barStyle = UIBarStyle.default
        productToolBar.isTranslucent = true
        productToolBar.tintColor = UIColor.black
        productToolBar.sizeToFit()
        
        let productDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnProduct(sender:)))
        
        productToolBar.setItems([cancelButton, spaceButton, productDoneButton], animated: false)
        productToolBar.isUserInteractionEnabled = true
        
        self.productTextField.inputAccessoryView = productToolBar
        self.productTextField.inputView = productPicker
    }
    
    @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
        self.quantityTextField.resignFirstResponder()
        self.refreshView()
        self.sellingPriceTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnSellingPrice(sender: UIBarButtonItem){
        self.submitBtn.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }

    @objc func onPressingDoneOnProduct(sender: UIBarButtonItem){
        self.productTextField.resignFirstResponder()
        self.quantityTextField.becomeFirstResponder()
    }
    
    @objc func onQuantityTextFieldEdit(sender: UITextField){
        self.refreshView()
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            if let productId = self.productId, let quantity = self.quantityTextField.text, let unitPrice = self.productionCostTextField.text, let sellingPrice = self.sellingPriceTextField.text{
                let param : [String: Any] = ["product_id": productId, "quantity": quantity, "unit_price": unitPrice, "selling_price": sellingPrice, "material": self.selectedRawMaterialId, "quantities": self.selectedQuantities, "buying_prices": self.selectedBuyingPrices]
                self.presenter.postProductionDataToServer(param: param)
            }
        }
    }
    
    func isValidated()->Bool{
        if self.productTextField.text == ""{
            self.showAlert(title: LanguageManager.ProductIsRequired, message: "")
            return false
        }else if self.quantityTextField.text == ""{
            self.showAlert(title: LanguageManager.QuantityIsRequired, message: "")
            return false
        }
        else if self.sellingPriceTextField.text == ""{
            self.showAlert(title: LanguageManager.SellingPriceIsRequired, message: "")
            return false
        }
        return true
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigateToProductVC()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func navigateToProductVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: PickerViewDelegate
extension ProductionRawMaterialAccessoryInfoViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        guard self.productList.count > 0 else {
            return 0
        }
        return self.productList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard self.productList.count > 0 else {
            return ""
        }
        let item = self.productList[row]
        self.productTextField.text = item.name
        self.productId = item.productId
        return item.name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard self.productList.count > 0 else {
            return
        }
        let item = self.productList[row]
        self.productTextField.text = item.name
        self.productId = item.productId
    }
    
}

//Api Delegate
extension ProductionRawMaterialAccessoryInfoViewController : ProductionRawMaterialAccessoryInfoViewDelegate{
    func setProduct(data: ProductListDataMapper) {
        guard let productList = data.productList else{
            return
        }
        self.productList = productList
    }
    
    func onProductionStore(data: ProductAddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        //self.showAlert(title: "Failed", message: data)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getProductDataFromServer(getAll: true)
    }
    
}

extension ProductionRawMaterialAccessoryInfoViewController{
    func refreshView(){
        guard self.selectedBuyingPrices.count > 0 else { return }
        var totalPrice = 0.0
        var temp : Int = 0
        for price in self.selectedBuyingPrices {
            if self.selectedQuantities.count > 0{
                let tempTotal = price * self.selectedQuantities[temp]
                totalPrice += tempTotal
                temp += 1
            }
        }
        
        if let quantityText = self.quantityTextField.text, let quantity = Double(quantityText){
            self.productionCostTextField.text = "\(totalPrice / quantity)"
        }
        
    }
}
