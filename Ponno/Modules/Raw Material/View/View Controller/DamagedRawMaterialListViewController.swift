//
//  DamagedRawMaterialListViewController.swift
//  Ponno
//
//  Created by a k azad on 23/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class DamagedRawMaterialListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = DamagedRawMaterialListPresenter(service: RawMaterialService())
    
    private var damagedRawMaterialList : [DamagedRawMaterialGroupInfo] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var damagesSummary : [Summary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var damagedMaterialName: String = ""
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.addFloaty()
        self.configureTableView()
        self.setNavigationBarTitle()
        self.configureCollectionView()
        self.setBarBtn()
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.DamagedProducts
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        //self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setBarBtn(){
        let damagedMaterialAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        damagedMaterialAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        damagedMaterialAddBtn.addTarget(self, action: #selector(ondamagedMaterialAddBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        damagedMaterialAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        damagedMaterialAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        damagedMaterialAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn3 = UIBarButtonItem(customView: damagedMaterialAddBtn)
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearchBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn1 = UIBarButtonItem(customView: searchBtn)
        
        navigationItem.setRightBarButtonItems([barBtn3, barBtn1], animated: true)
    }
    
    @objc func ondamagedMaterialAddBtnTapped(sender: UIBarButtonItem){
        self.navigateToDamageableRawMaterialListViewController()
    }
    
    @objc func onSearchBtnTapped(sender: UIBarButtonItem){
        self.navigateToDamageableRawMaterialSearchViewController()
    }
    
    
    
    func navigateToDamageableRawMaterialSearchViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamagedRawMaterialSearchViewController") as! DamagedRawMaterialSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToDamageableRawMaterialListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamageableRawMaterialListViewController") as! DamageableRawMaterialListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //FloatingButton
    func addFloaty(){
        
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ExpenseViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.damagedRawMaterialList = []
        self.currentPage = 1
        self.presenter.getDamagedRawMaterialListDataFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }
    
    
}

extension DamagedRawMaterialListViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC(){
        self.navigateToAddNewExpenseViewController()
    }
    
}

//MARK: CollectionView Delegate And Data Source
extension DamagedRawMaterialListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(ExpenseSummaryCell.nib, forCellWithReuseIdentifier: ExpenseSummaryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let expenseSummaryItem = self.damagesSummary, expenseSummaryItem.count > 0 else{
            return 0
        }
        return expenseSummaryItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ExpenseSummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: ExpenseSummaryCell.identifier, for: indexPath) as! ExpenseSummaryCell
        guard let expenseSummaryItem = self.damagesSummary, expenseSummaryItem.count > 0 else {
            return cell
        }
        let list = expenseSummaryItem[indexPath.row]
        
//        for (key, _) in expenseSummaryItem.enumerated() {
//            if key == 0 {
//                expenseSummaryItem[0].title = "Total Damaged Material"
//            }else if key == 1 {
//                expenseSummaryItem[1].title = "Total Damaged Quantity"
//            }else if key == 2 {
//                expenseSummaryItem[2].title = "Total Loss"
//            }
//        }
        
        cell.summaryTitle.text = list.title
        cell.summeryValue.text = list.value
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70.0)
    }
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.damagesSummary, list.count > 0 else{
            return
        }
        let listCount = list.count
        
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//Mark TableView Delegate and Data Source
extension DamagedRawMaterialListViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DamagedProductListCell.nib, forCellReuseIdentifier: DamagedProductListCell.identifier)
        self.tableView.rowHeight = 80
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.damagedRawMaterialList.count > 0 {
            return self.damagedRawMaterialList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DamagedProductListCell = tableView.dequeueReusableCell(withIdentifier: DamagedProductListCell.identifier, for: indexPath) as! DamagedProductListCell
        cell.selectionStyle = .none
        if self.damagedRawMaterialList.count > 0 {
            let data = self.damagedRawMaterialList[indexPath.row]
            
            cell.damagedProduct.text = data.damagedMaterialName
            cell.numberOfDamages.text = LanguageManager.Damaged + " : " + String(describing: data.damagedLot) + " " + LanguageManager.Times
            cell.totalDamages.text = LanguageManager.TotalDamages + " : " + "\(data.totalQuantity)" + data.unit
            cell.totalLoss.text = LanguageManager.TotalDamageAmount + " : " + String(describing: data.totalLoss) + " " + Constants.currencySymbol
            
            cell.selectBtn.isHidden = true 
            
            if isLoading == false && indexPath.row == self.damagedRawMaterialList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getDamagedRawMaterialListDataFromServer(page: self.currentPage)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.damagedRawMaterialList.count > 0 {
            let damagedItem = self.damagedRawMaterialList[indexPath.row]
            self.damagedMaterialName = damagedItem.damagedMaterialName
            self.navigateToDamagePerRawMaterialVC(id: damagedItem.damagedMaterialId)
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToDamagePerRawMaterialVC(id : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamagedPerRawMaterialViewController") as! DamagedPerRawMaterialViewController
        viewController.damagedRawMaterialId = id
        viewController.damagedMaterialName = self.damagedMaterialName
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Api Delegate
extension DamagedRawMaterialListViewController : DamagedRawMaterialListViewDelegate{
    func setDamagedProductData(data: DamagedRawMaterialDataMapper) {
        guard let damagesSummaryItem = data.summary, damagesSummaryItem.count > 0 else {
            return
        }
        self.damagesSummary = damagesSummaryItem
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        guard let list = data.damagedRawMaterialGroupInfo, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.damagedRawMaterialList += list
        
        
        guard let pagination = data.pagination else{
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.currentPage = 1
        self.damagedRawMaterialList = []
        self.presenter.getDamagedRawMaterialListDataFromServer(page: self.currentPage)
    }
}

