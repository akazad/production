//
//  DamagedRawMaterialSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 25/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DamagedRawMaterialSearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = DamagedRawMaterialListPresenter(service: RawMaterialService())
    
    private var damagedRawMaterialList : [DamagedRawMaterialGroupInfo] = []{
        didSet{
            self.refreshTableView()
        }
    }

    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var damagedMaterialName: String = ""
    
    var searchText = ""
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpSearchBar()
        self.configureTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.presenter.getDamagedRawMaterialSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(DamagedRawMaterialSearchViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.damagedRawMaterialList = []
        self.currentPage = 1
        self.presenter.getDamagedRawMaterialListDataFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }

}

//Mark: Search Delegate
extension DamagedRawMaterialSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.DamagedRawMaterialSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.damagedRawMaterialList = []
        self.isLoading = true
        self.isSearchActive = true
        self.presenter.getDamagedRawMaterialSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
    }
}

//Mark TableView Delegate and Data Source
extension DamagedRawMaterialSearchViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DamagedProductListCell.nib, forCellReuseIdentifier: DamagedProductListCell.identifier)
        self.tableView.rowHeight = 80
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.damagedRawMaterialList.count > 0 {
            return self.damagedRawMaterialList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DamagedProductListCell = tableView.dequeueReusableCell(withIdentifier: DamagedProductListCell.identifier, for: indexPath) as! DamagedProductListCell
        cell.selectionStyle = .none
        if self.damagedRawMaterialList.count > 0 {
            let data = self.damagedRawMaterialList[indexPath.row]
            
            cell.damagedProduct.text = data.damagedMaterialName
            cell.numberOfDamages.text = LanguageManager.Damaged + " : " + String(describing: data.damagedLot) + " " + LanguageManager.Times
            cell.totalDamages.text = LanguageManager.TotalDamages + " : " + "\(data.totalQuantity)" + data.unit
            cell.totalLoss.text = LanguageManager.TotalDamageAmount + " : " + String(describing: data.totalLoss) + " " + Constants.currencySymbol
            
            cell.selectBtn.isHidden = true
            
            if isLoading == false && indexPath.row == self.damagedRawMaterialList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getDamagedRawMaterialListDataFromServer(page: self.currentPage)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.damagedRawMaterialList.count > 0 {
            let damagedItem = self.damagedRawMaterialList[indexPath.row]
            self.damagedMaterialName = damagedItem.damagedMaterialName
            self.navigateToDamagePerRawMaterialVC(id: damagedItem.damagedMaterialId)
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToDamagePerRawMaterialVC(id : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamagedPerRawMaterialViewController") as! DamagedPerRawMaterialViewController
        viewController.damagedRawMaterialId = id
        viewController.damagedMaterialName = self.damagedMaterialName
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Api Delegate
extension DamagedRawMaterialSearchViewController : DamagedRawMaterialListViewDelegate{
    func setDamagedProductData(data: DamagedRawMaterialDataMapper) {
        guard let list = data.damagedRawMaterialGroupInfo, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.damagedRawMaterialList += list
        
        
        guard let pagination = data.pagination else{
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.currentPage = 1
        self.damagedRawMaterialList = []
        self.presenter.getDamagedRawMaterialSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
    }
}
