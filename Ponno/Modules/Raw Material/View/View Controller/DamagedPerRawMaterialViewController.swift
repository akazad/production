//
//  DamagedPerRawMaterialViewController.swift
//  Ponno
//
//  Created by a k azad on 23/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class DamagedPerRawMaterialViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = DamagedPerDayRawMaterialPresenter(service: RawMaterialService())
    
    var damagedMaterialName : String?
    
    var damagedRawMaterialId : Int?
    
    var perDamagedRawMaterialList :[DamagedPerRawMaterial] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var summary : [Summary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    //var expenseData : PerExpenseDataList?
    
    let manager = CollectionViewScrollManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.configureCollectionView()
        self.addFloaty()
        self.initialSetUp()
    }
    
    func initialSetUp(){
        if let name = self.damagedMaterialName {
            self.title = name
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    //FloatingButton
    func addFloaty(){
        
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        viewController.expenseState = ExpenseState.Add.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(DamagedPerRawMaterialViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.perDamagedRawMaterialList = []
        currentPage = 1
        if let id = self.damagedRawMaterialId {
            self.presenter.getDamagedPerRawMaterialDataFromServer(page: currentPage, id: id)
        }
        refreshControl.endRefreshing()
    }
    
}

//MARK: CollectionView Delegate And DataSource
extension DamagedPerRawMaterialViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(ExpensePerCategorySummaryCell.nib, forCellWithReuseIdentifier: ExpensePerCategorySummaryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let summary = self.summary, summary.count > 0 else{
            return 0
        }
        return summary.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ExpensePerCategorySummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: ExpensePerCategorySummaryCell.identifier, for: indexPath) as! ExpensePerCategorySummaryCell
        guard let perDamagedRawMaterial = self.summary, perDamagedRawMaterial.count > 0 else {
            return cell
        }
        let list = perDamagedRawMaterial[indexPath.row]
        
//        for (key, _) in perDamagedRawMaterial.enumerated() {
//            if key == 0 {
//                perDamagedRawMaterial[0].title = "Damage Ratio"
//            }else if key == 1 {
//                perDamagedRawMaterial[1].title = "Total Quantity"
//            }else if key == 2 {
//                perDamagedRawMaterial[2].title = "Total Loss"
//            }
//        }
        
        cell.amount.text = list.value
        cell.title.text = list.title
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70.0)
    }
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.summary, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//MARK: TableView Delegate And DataSource
extension DamagedPerRawMaterialViewController: UITableViewDelegate, UITableViewDataSource {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ExpenseSalleryCell.nib, forCellReuseIdentifier: ExpenseSalleryCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.perDamagedRawMaterialList.count > 0 {
            return self.perDamagedRawMaterialList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ExpenseSalleryCell = tableView.dequeueReusableCell(withIdentifier: ExpenseSalleryCell.identifier, for: indexPath) as! ExpenseSalleryCell
        if self.perDamagedRawMaterialList.count > 0 {
            let data = self.perDamagedRawMaterialList[indexPath.row]
//            if data.name == ""{
                cell.nameView.isHidden = true
                cell.nameViewWidth.constant = 0.0
//            }else{
//                cell.nameView.isHidden = false
//                cell.nameViewWidth.constant = 160.0
//            }
            //cell.nameLabel.text = data.name
            cell.createdAtLabel.text = data.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
            cell.createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
            cell.amountLabel.text = LanguageManager.DamagedQuantity + " : " + "\(data.quantity)"
            
            let desc = data.description
            if desc == "" {
                cell.infoImage.isHidden = true
            }else{
                cell.infoImage.addTapGestureRecognizer{
                    self.showAlert(title: LanguageManager.Description, message: desc)
                }
            }
            
            cell.popUpBtn.tag = data.id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.perDamagedRawMaterialList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                if let id = self.damagedRawMaterialId {
                    self.presenter.getDamagedPerRawMaterialDataFromServer(page: self.currentPage, id: id)
                }
            }
            cell.selectionStyle = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.perDamagedRawMaterialList.count > 0{
            let damagedMaterialId = sender.tag
            for damagedMaterial in self.perDamagedRawMaterialList{
                if damagedMaterialId == damagedMaterial.id{
                    let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToDamagedPerRawMaterialUpdateViewController(data: damagedMaterial)
                    }
                    
                    let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: damagedMaterialId)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(editAction)
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToDamagedPerRawMaterialUpdateViewController(data: DamagedPerRawMaterial){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamagedPerRawMaterialUpdateViewController") as! DamagedPerRawMaterialUpdateViewController
        viewController.damagedMaterialId = data.id
        viewController.damagedRawMaterial = data
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Floaty Delegate
extension DamagedPerRawMaterialViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC(){
        self.navigateToAddNewExpenseViewController()
    }
}

//Api Delegate
extension DamagedPerRawMaterialViewController : DamagedPerDayRawMaterialViewDelegate{
    func setDamagedPerRawMaterialData(data: DamagedPerRawMaterialDataMapper) {
        guard let perDamagedSummary = data.summary else {
            return
        }
        self.summary = perDamagedSummary
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        if let list = data.damagedPerRawMaterial, list.count > 0  {
            self.isLoading = false
            //self.expenseCategory = data.expenseCategory
            self.perDamagedRawMaterialList += list
        }
        
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onDeleteDamagedData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.getDamagedPerRawMaterialData()
    }
    
    func getDamagedPerRawMaterialData(){
        if let id = self.damagedRawMaterialId {
            self.isLoading = true
            self.currentPage = 1
            self.perDamagedRawMaterialList = []
            self.presenter.getDamagedPerRawMaterialDataFromServer(page: currentPage, id: id)
        }
    }
   
}


extension DamagedPerRawMaterialViewController{
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.perDamagedRawMaterialList = []
            self.refreshTableView()
            self.getDamagedPerRawMaterialData()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.presenter.deleteDamagedPerRawMaterialDataFromServer(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
