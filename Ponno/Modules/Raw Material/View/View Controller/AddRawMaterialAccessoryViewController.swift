//
//  AddRawMaterialAccessoryViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class AddRawMaterialAccessoryViewController: UIViewController {
    
    @IBOutlet weak var expireDateLbl: UILabel!{
        didSet{
            expireDateLbl.text = LanguageManager.ExpireDate
        }
    }
    @IBOutlet weak var expireDateTextField: UITextField!
    @IBOutlet weak var quantityAlertLbl: UILabel!{
        didSet{
            quantityAlertLbl.text = LanguageManager.QuantityAlertWhenSmallerThan
        }
    }
    @IBOutlet weak var quantityAlertTextField: UITextField!
    @IBOutlet weak var unitLbl: UILabel!{
        didSet{
            unitLbl.text = LanguageManager.Unit
        }
    }
    @IBOutlet weak var quantityLbl: UILabel!{
        didSet{
            quantityLbl.text = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var buyingPriceLbl: UILabel!{
        didSet{
            buyingPriceLbl.text = LanguageManager.BuyingPrice
        }
    }
    @IBOutlet weak var buyingPriceTextField: UITextField!
    @IBOutlet weak var vendorLbl: UILabel!{
        didSet{
            vendorLbl.text = LanguageManager.Vendor
        }
    }
    @IBOutlet weak var dealerOrCompanyTextField: UITextField!{
        didSet{
            dealerOrCompanyTextField.placeholder = LanguageManager.SelectOne
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var newVendorAddBtn: UIButton!{
        didSet{
            newVendorAddBtn.setTitle(LanguageManager.NewVendorAdd, for: .normal)
        }
    }
    
    private var presenter = AddRawMaterialAccessoryInfoPresenter(service: RawMaterialService())
    
    var vendorId : Int?
    var vendorsList : [VendorsList] = []
    var isLoading : Bool = false
    var currentPage : Int = 1
    
    var checked : Bool = false
    
    var message : String?
    
    var vendorListPicker = UIPickerView()
    var datePicker = UIDatePicker()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpPickerView()
        self.toolBarSetUp()
        self.configureTextField()
        self.initialSetup()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.amountTextField.isEnabled = true
        RawMaterialObject.expireDate = ""
        RawMaterialObject.buyingPrice = nil
        self.amountTextField.text = "\(0)"
        self.quantityAlertTextField.text = "\(0)"
        RawMaterialObject.vendorId = 0
        self.showStartDatePicker()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpViews()
    }
    
    func setUpViews(){
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtn(sender:)), for: .touchUpInside)
        self.navigationController?.navigationBar.topItem?.title = " "
        self.newVendorAddBtn.addTarget(self, action: #selector(onVendorAddBtnPressed), for: .touchUpInside)
        self.expireDateTextField.addTarget(self, action: #selector(onExpireDateChange), for: .editingDidEnd)
    }
    
    @objc func onVendorAddBtnPressed(sender: UIButton){
        self.alertWithTextField()
    }
    
    @objc func onExpireDateChange(sender: UITextField){
        RawMaterialObject.expireDate = self.expireDateTextField.text
    }
    
    @objc func onSubmitBtn(sender : UIButton){
        
        guard let amount = self.amountTextField.text, let buyingPrice =  self.buyingPriceTextField.text, let quantityAlert = self.quantityAlertTextField.text else{
            return
        }
        RawMaterialObject.stockAlert = quantityAlert
        RawMaterialObject.quantity = amount
        RawMaterialObject.buyingPrice = buyingPrice
        RawMaterialObject.vendorId = self.vendorId
        
        if self.isValidated(){
            guard let name = RawMaterialObject.rawMaterialName, let category = RawMaterialObject.rawMaterialCategoryId, let companyName = RawMaterialObject.companyName, let varient = RawMaterialObject.varient, let unit = RawMaterialObject.unitId, let sku = RawMaterialObject.sku,  let buyingPricee = RawMaterialObject.buyingPrice, let quantity = RawMaterialObject.quantity else{
                return
            }
            let vendorId = RawMaterialObject.vendorId ?? nil
            guard let expireDate = RawMaterialObject.expireDate, let stockAlert = RawMaterialObject.stockAlert else{
                return
            }
            
            
            let params : [String : Any] = ["material_search": "", "name" :name, "category": category,
                                           "company": companyName,
                                           "variant": varient,
                                           "unit": unit,
                                           "sku": sku,
                                           "buying_price": buyingPricee,
                                           "vendor": vendorId,
                                           "quantity": quantity,
                                           "stock_alert": stockAlert,
                                           "expire_date": expireDate, "parent_category": 0]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            self.presenter.postNewRawMaterialDataToServer(param: params)
        }
        
    }
    
    func isValidated()->Bool{
        if let quantityText = self.amountTextField.text, let quantity = Double(quantityText), quantity > 0 {
            if self.buyingPriceTextField.text == ""{
                showAlert(title: LanguageManager.BuyingPriceIsRequired, message: "")
                return false
            }
        }
        return true
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigateToRawMaterialListVC()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func navigateToRawMaterialListVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RawMaterialListViewController") as! RawMaterialListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //PickerView
    func setUpPickerView(){
        vendorListPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        self.dealerOrCompanyTextField.inputView = vendorListPicker
        self.dealerOrCompanyTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.dealerOrCompanyTextField.resignFirstResponder()
        self.submitBtn.becomeFirstResponder()
    }
    
    func toolBarSetUp(){
        //amount toolBar
        let amountToolBar = UIToolbar()
        amountToolBar.barStyle = UIBarStyle.default
        amountToolBar.isTranslucent = true
        amountToolBar.tintColor = UIColor.black
        amountToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let amountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnAmount(sender:)))
        
        amountToolBar.setItems([cancelButton,spaceButton, amountDoneButton], animated: false)
        amountToolBar.isUserInteractionEnabled = true
        
        self.amountTextField.inputAccessoryView = amountToolBar
        
        //buyingPrice toolBar
        let buyingPriceToolBar = UIToolbar()
        buyingPriceToolBar.barStyle = UIBarStyle.default
        buyingPriceToolBar.isTranslucent = true
        buyingPriceToolBar.tintColor = UIColor.black
        buyingPriceToolBar.sizeToFit()
        
        let buyingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnBuyingPrice(sender:)))
        
        buyingPriceToolBar.setItems([cancelButton,spaceButton, buyingPriceDoneButton], animated: false)
        buyingPriceToolBar.isUserInteractionEnabled = true
        
        self.buyingPriceTextField.inputAccessoryView = buyingPriceToolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnAmount(sender: UIBarButtonItem){
        self.buyingPriceTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnBuyingPrice(sender: UIBarButtonItem){
        self.dealerOrCompanyTextField.becomeFirstResponder()
    }

}

//Mark: PickerViewDelegate
extension AddRawMaterialAccessoryViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        guard self.vendorsList.count > 0 else{
            return 0
        }
        return vendorsList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.vendorsList.count > 0 {
            let list = self.vendorsList[row]
            self.dealerOrCompanyTextField.text = list.name
            self.vendorId = list.id
            return list.name
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if self.vendorsList.count > 0 {
            let list = self.vendorsList[row]
            self.dealerOrCompanyTextField.text = list.name
            self.vendorId = list.id
            
        }
    }
    
}

//Mark: TextField Delegate
extension AddRawMaterialAccessoryViewController : UITextFieldDelegate{
    func configureTextField(){
        self.amountTextField.delegate = self
        self.buyingPriceTextField.delegate = self
        self.quantityAlertTextField.delegate = self
        self.expireDateTextField.delegate = self
        self.amountTextField.underlined()
        self.buyingPriceTextField.underlined()
        self.quantityAlertTextField.underlined()
        self.expireDateTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.quantityAlertTextField{
            self.amountTextField.becomeFirstResponder()
        }else if textField == self.amountTextField{
            self.buyingPriceTextField.becomeFirstResponder()
        }else if textField == self.buyingPriceTextField{
            self.dealerOrCompanyTextField.becomeFirstResponder()
        }
        return false
    }
}

//Mark: Api Delegate
extension AddRawMaterialAccessoryViewController : AddRawMaterialAccessoryInfoViewDelegate{
    func postNewRawMaterialData(data: RawMaterialAddDataMapper) {
        guard let message = data.message, let rawMaterialId = data.rawMaterialId else {
            return
        }
        self.message = message
        self.successMessage(userMessage: message)
        guard let image = RawMaterialObject.rawMaterialImage else{
            self.successMessage(userMessage: message)
            return
        }
        
        self.presenter.uploadImage(id: rawMaterialId, image: image)
    }
    
    func setVendorListData(data: VendorsDataMapper) {
        guard let list = data.vendors, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.vendorsList += list
    }
    
    func postNewVendorData(data: VendorAddDataMapper) {
        guard let vendor = data.vendor else {
            return
        }
        self.vendorId = vendor.id
        self.dealerOrCompanyTextField.text = vendor.name
        self.dealerOrCompanyTextField.resignFirstResponder()
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onImageUpload(data: AddDataMapper) {
        self.successMessage(userMessage: self.message ?? "")
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.presenter.getVendorsListFromServer(page: self.currentPage)
    }
}


extension AddRawMaterialAccessoryViewController{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewVendorAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.VendorName
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.MobileNumber
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                //self.showAlert(title: "Name Can't be empty", message: "")//
                self.showMessage(userMessage: LanguageManager.VendorNameIsRequired)
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                self.showMessage(userMessage: LanguageManager.VendorMobileNumberIsRequired)
                return
            }
            
//            if(!self.validatePhoneNumber(value: phone)){
//                self.showMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
//            }
            
            let param : [String : Any] = ["name": textField.text!, "phone": textField2.text!, "address": ""]
            
            self.presenter.postNewVendorDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.alertWithTextField()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension AddRawMaterialAccessoryViewController {
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        expireDateTextField.inputAccessoryView = toolbar
        expireDateTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        expireDateTextField.text = formatter.string(from: datePicker.date)
        self.quantityAlertTextField.becomeFirstResponder()
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
