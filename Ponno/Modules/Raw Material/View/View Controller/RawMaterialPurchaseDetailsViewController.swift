//
//  RawMaterialPurchaseDetailsViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class RawMaterialPurchaseDetailsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessage: UILabel!{
        didSet{
            emptyMessage.text = LanguageManager.NoInformationFound
        }
    }
    
    private var presenter = RawMaterialPurchaseDetailsPresenter(service: RawMaterialService())
    var selectedId : Int?
    var invoicePayable: Double?
    var currentPayable : Double?
    var vendor : String?
    
    private var rawMaterialPurchaseDetails : RawMaterialPurchaseDetails?
    private var rawMaterialPurchaseMaterial : [RawMaterialPurchaseMaterials]?{
        didSet{
            self.refreshTableView()
        }
    }
    private var rawMaterialPayableTransactions : [PayableTransactions] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    enum Sections : Int{
        case PurchaseInfo
        case Product
        case PurchaseTotal
        case PayableTransactionsHeader
        case PayableTransactions
        case PurchasePayableHistory
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.configureTableView()
        self.setNavigationBarTitle()
        self.attachPresenter()
    }
    func setNavigationBarTitle(){
        self.title = LanguageManager.MaterialPurchaseInvoice
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
    }
}


//MARK: TableView Delegate And DataSource
extension RawMaterialPurchaseDetailsViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PurchaseDetailsCell.nib, forCellReuseIdentifier: PurchaseDetailsCell.identifier)
        self.tableView.register(PurchaseProductsCell.nib, forCellReuseIdentifier: PurchaseProductsCell.identifier)
        self.tableView.register(SaleDetailsTotalCell.nib, forCellReuseIdentifier: SaleDetailsTotalCell.identifier)
        self.tableView.register(SaleDetailsDueTransactionsHeaderCell.nib, forCellReuseIdentifier: SaleDetailsDueTransactionsHeaderCell.identifier)
        self.tableView.register(SaleDetailsDueTransactionsCell.nib, forCellReuseIdentifier: SaleDetailsDueTransactionsCell.identifier)
        self.tableView.register(SaleDetailsDueHistoryCell.nib, forCellReuseIdentifier: SaleDetailsDueHistoryCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.emptyMessage.isHidden = true
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.PurchaseInfo.rawValue:
            if self.rawMaterialPurchaseDetails == nil{
                return 0
            }else{
                return 1
            }
        case Sections.Product.rawValue:
            guard let list = self.rawMaterialPurchaseMaterial, list.count > 0 else {
                return 0
            }
            return list.count
        case Sections.PurchaseTotal.rawValue:
            return 1
        case Sections.PayableTransactionsHeader.rawValue:
            return 1
        case Sections.PayableTransactions.rawValue:
            if self.rawMaterialPayableTransactions.count < 0{
                return 0
            }else{
                return self.rawMaterialPayableTransactions.count
            }
        case Sections.PurchasePayableHistory.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {        
        switch indexPath.section {
        case Sections.PurchaseInfo.rawValue:
            
            let cell : PurchaseDetailsCell = tableView.dequeueReusableCell(withIdentifier: PurchaseDetailsCell.identifier, for: indexPath) as! PurchaseDetailsCell
            cell.selectionStyle = .none
            
            if let data = self.rawMaterialPurchaseDetails {
                cell.invoiceLabel.text = LanguageManager.Invoice + "#" + " : " + data.invoice
                cell.totalAmountLabel.text = LanguageManager.TotalPrice + " : " + "\(data.total)" + " " + Constants.currencySymbol
                cell.discountLbl.text = LanguageManager.Discount + " : " + data.discountAmount + " " + Constants.currencySymbol
                
                let total = Double(data.total) ?? 0.0
                let discount = Double(data.discountAmount) ?? 0.0
                
                let payableAmount = total - discount
                
                cell.payableAmountLbl.text = LanguageManager.PayableAmount + " : " + "\(payableAmount)"
                cell.dateLabel.text = LanguageManager.Date + " : " + convertDateFormater(data.createdAt)
                cell.timeLabel.text = LanguageManager.Time + " : " + convertDateFormaterToTime(data.createdAt)
                cell.paidLabel.text = LanguageManager.Paid + " : " + "\(data.paid)" + " " + Constants.currencySymbol
                cell.payableLabel.text = LanguageManager.Payable + " : " + "\(data.payable)" + " " + Constants.currencySymbol
                cell.nameLabel.text = LanguageManager.Vendor + " : " + data.vendorName
                cell.PhoneLabel.text = LanguageManager.MobileNumber + " : " + data.vendorPhone
            }
            
            return cell
            
        case Sections.Product.rawValue:
            let cell: PurchaseProductsCell = tableView.dequeueReusableCell(withIdentifier: PurchaseProductsCell.identifier, for: indexPath) as! PurchaseProductsCell
            cell.selectionStyle = .none
            
            if let list = self.rawMaterialPurchaseMaterial, list.count > 0 {
                let item = list[indexPath.row]
                cell.nameLabel.text = item.name
                cell.quantityLabel.text = LanguageManager.Quantity + " : " + "\(item.quantity)"
                cell.buyingPriceLabel.text = LanguageManager.PricePerUnit + " : " + "\(item.buyingPrice)"
                cell.totalLabel.text = "\(item.total)" + Constants.currencySymbol
            }
            return cell
            
        case Sections.PurchaseTotal.rawValue:
            let cell: SaleDetailsTotalCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsTotalCell.identifier, for: indexPath) as! SaleDetailsTotalCell
            
            cell.selectionStyle = .none
            
            if let total = self.rawMaterialPurchaseDetails?.total {
                cell.totalLbl.text = LanguageManager.Total + " : " + total + Constants.currencySymbol
            }
            return cell
            
        case Sections.PayableTransactionsHeader.rawValue:
            let cell: SaleDetailsDueTransactionsHeaderCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsDueTransactionsHeaderCell.identifier, for: indexPath) as! SaleDetailsDueTransactionsHeaderCell
            cell.selectionStyle = .none
            
            if self.rawMaterialPayableTransactions.count > 0{
                cell.dateLbl.text = LanguageManager.Date
                cell.paidAmountLbl.text = LanguageManager.PaidAmount
            }else{
                cell.isHidden = true
            }
            
            return cell
            
        case Sections.PayableTransactions.rawValue:
            let cell : SaleDetailsDueTransactionsCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsDueTransactionsCell.identifier, for: indexPath) as! SaleDetailsDueTransactionsCell
            cell.selectionStyle = .none
            if self.rawMaterialPayableTransactions.count >= 0 {
                let list = self.rawMaterialPayableTransactions[indexPath.row]
                
                let vendor = self.vendor ?? ""
                
                if !vendor.isEmpty{
                    cell.dateDetailsLbl.text = list.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_dd_yyyy.rawValue)
                    cell.paidAmount.text = list.paid + Constants.currencySymbol
                }
            }
            
            return cell
        case Sections.PurchasePayableHistory.rawValue:
            let cell: SaleDetailsDueHistoryCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsDueHistoryCell.identifier, for: indexPath) as! SaleDetailsDueHistoryCell
            
            cell.selectionStyle = .none
            
            if let invoicePayable = self.invoicePayable {
                
                let vendor = self.vendor ?? ""
                
                if !vendor.isEmpty , let currentPayable = self.currentPayable{
                    
                    cell.invoiceLbl.text = LanguageManager.InvoicePayable + " : "
                    cell.invoice.text = "\(invoicePayable)" + Constants.currencySymbol
                    
                    cell.latestDueLbl.text = LanguageManager.LatestPayable + " : "
                    cell.latestDue.text = "\(currentPayable)" + Constants.currencySymbol
                    
                    var othersPayable : Double = 0.0
                    
                    if (self.rawMaterialPayableTransactions.count != 0) {
                        var duePaid : Double = 0.00
                        for item in self.rawMaterialPayableTransactions{
                            let paid = Double(item.paid) ?? 0.00
                            duePaid += paid
                        }
                        cell.duePaidLbl.text = LanguageManager.PayablePaid + " : "
                        cell.duePaid.text = "\(duePaid)" + Constants.currencySymbol
                        
                        let remainigDue : Double = invoicePayable - duePaid
                        
                        cell.remainingDueLbl.text = LanguageManager.RemainingPayable + " : "
                        cell.remainingDue.text = "\(remainigDue)" + Constants.currencySymbol
                        
                        
                        if currentPayable > remainigDue{
                            othersPayable = currentPayable - remainigDue
                        }else{
                            othersPayable = remainigDue - currentPayable
                        }
                        
                        cell.othersDueLbl.text = LanguageManager.OthersPayable + " : "
                        cell.othersDue.text = "\(othersPayable)" + Constants.currencySymbol
                    }else{
                        
                        let remainigPayable : Double = invoicePayable
                        
                        cell.remainingDueLbl.text = LanguageManager.RemainingPayable + " : "
                        cell.remainingDue.text = "\(remainigPayable)" + Constants.currencySymbol
                        
                        
                        if currentPayable > remainigPayable{
                            othersPayable = currentPayable - remainigPayable
                        }else{
                            othersPayable = remainigPayable - currentPayable
                        }
                        
                        cell.othersDueLbl.text = LanguageManager.OthersPayable + " : "
                        cell.othersDue.text = "\(othersPayable)" + Constants.currencySymbol
                        cell.duePaidLbl.isHidden = true
                        cell.duePaid.isHidden = true
                        cell.remainingDueLbl.isHidden = true
                        cell.remainingDue.isHidden = true
                    }
                }
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.PurchaseInfo.rawValue:
            return 365.0
        case Sections.Product.rawValue:
            if let list = self.rawMaterialPurchaseMaterial, list.count > 0  {
                return 90
            }
            return 0.01
        case Sections.PurchaseTotal.rawValue:
            return 44.0
        case Sections.PayableTransactionsHeader.rawValue:
            return 24.0
        case Sections.PayableTransactions.rawValue:
            return 32.0
        case Sections.PurchasePayableHistory.rawValue:
            return 190.0
        default:
            return 0.01
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func convertDateFormater(_ date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd MMM, yy"
        return  dateFormatter.string(from: date!)
    }
    
    func convertDateFormaterToTime(_ date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "HH:mm"
        return  dateFormatter.string(from: date!)
    }
    
}

//Mark: Api Delegate
extension RawMaterialPurchaseDetailsViewController : RawMaterialPurchaseDetailsViewDelegate{
    
    func setPurchaseDetailsData(data: RawMaterialPurchaseDetailsDataMapper) {
        guard let rawMaterialPurchaseDetail = data.rawMaterialPurchaseDetails, let purchaseRawMaterialItem = data.rawMaterialPurchaseMaterials, purchaseRawMaterialItem.count > 0 else {
            self.emptyMessage.isHidden = false
            return
        }
        self.emptyMessage.isHidden = true
        self.rawMaterialPurchaseDetails = rawMaterialPurchaseDetail
        self.invoicePayable = Double(rawMaterialPurchaseDetail.payable)
        self.currentPayable = Double(rawMaterialPurchaseDetail.currentPayable)
        self.rawMaterialPurchaseMaterial = purchaseRawMaterialItem
        self.vendor = rawMaterialPurchaseDetail.vendorName
        self.rawMaterialPayableTransactions = data.rawMaterialPayableTransactions ?? []
        self.refreshTableView()
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.emptyMessage.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.emptyMessage.isHidden = false
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        if let id = self.selectedId {
            self.presenter.getRawMaterialPurchaseDetailsDataFromServer(id: id)
        }
    }
    
}
