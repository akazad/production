//
//  RawMaterialBaseViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import FloatingPanel
import SDWebImage

class RawMaterialDetailsBaseViewController: UIViewController, FloatingPanelControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var presenter = RawMaterialDetailsPresenter(service: RawMaterialService())
    
    var fpc: FloatingPanelController!
    var bottomShetVC : RawMaterialDetailsViewController!
    
    var rawMaterialDetails : RawMaterialDetails?
    var materialInventoryId : Int?
    //var rawMaterialId : Int?
    //var hasSerial : Int?
    var rawMaterialName: String?
//    var vendor: Vendors?
    
    enum Sections : Int {
        case rawMaterialInfo
        case rawMaterialSummary
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addBottomSheet()
        self.setUpInitialLanguage()
        self.configureTableView()
        self.initialSetup()
        self.attachPresenter()
    }
    
    func addBottomSheet(){
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = UIColor.lightGreen.withAlphaComponent(0.5)
        fpc.surfaceView.layer.cornerRadius = 9.0
        fpc.surfaceView.clipsToBounds = true
        
        fpc.surfaceView.shadowHidden = false
        bottomShetVC = storyboard?.instantiateViewController(withIdentifier: "RawMaterialDetailsViewController") as? RawMaterialDetailsViewController
        
        bottomShetVC.materialInventoryId = self.materialInventoryId
        bottomShetVC.rawMaterialName = self.rawMaterialName
        fpc.set(contentViewController: bottomShetVC)
        fpc.track(scrollView: bottomShetVC.tableView)
        fpc.addPanel(toParent: self)
    }
    
    func removeBottomSheet(){
        fpc.removePanelFromParent(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeBottomSheet()
    }
    
    func initialSetup(){
        self.title = LanguageManager.RawMaterialInfo
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(RawMaterialDetailsBaseViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        if let id = self.materialInventoryId {
            self.presenter.getRawMaterialDetailsDataFromServer(id: id)
        }
        refreshControl.endRefreshing()
    }
    
}

//MARK: TableView Delegate and DataSource
extension RawMaterialDetailsBaseViewController : UITableViewDataSource, UITableViewDelegate {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(RawMaterialDetailsInfoCell.nib, forCellReuseIdentifier: RawMaterialDetailsInfoCell.identifier)
        self.tableView.register(RawMaterialDetailsSummaryCell.nib, forCellReuseIdentifier: RawMaterialDetailsSummaryCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.white
        self.tableView.separatorStyle = .singleLine
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(self.refreshControl)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.rawMaterialInfo.rawValue:
            return 1
        case Sections.rawMaterialSummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case Sections.rawMaterialInfo.rawValue:
            let cell : RawMaterialDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: RawMaterialDetailsInfoCell.identifier, for: indexPath) as! RawMaterialDetailsInfoCell
            cell.selectionStyle = .none
            if let rawMaterialDetails = self.rawMaterialDetails {
                cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.rawMaterialImage + rawMaterialDetails.image), placeholderImage: UIImage(named: "Product-1"))
                
                cell.rawMaterialName.text = rawMaterialDetails.name.capitalized
                
                let company = rawMaterialDetails.company
                if company.isEmpty{
                    cell.company.text = LanguageManager.Company + " : ---"
                }else{
                    cell.company.text = LanguageManager.Company + " : " + company
                }
                
                cell.categoryName.text = LanguageManager.Category + " : " + rawMaterialDetails.categoryName
                
                let variant = rawMaterialDetails.variant
                if variant.isEmpty{
                    cell.variantLabel.text = LanguageManager.Varient + " : ---"
                }else{
                    cell.variantLabel.text = LanguageManager.Varient + " : " + variant
                }
                
                let sku = rawMaterialDetails.sku
                if sku.isEmpty{
                    cell.sku.text = LanguageManager.MaterialSku + " : ---"
                }else{
                    cell.sku.text = LanguageManager.MaterialSku + " : " + sku
                }
                
                let purchasePrice = rawMaterialDetails.buyingPrice
                if purchasePrice.isEmpty{
                    cell.buyingPrice.text = LanguageManager.PurchasePrice + " : " + "0.00" + " " + Constants.currencySymbol
                }else{
                    cell.buyingPrice.text = LanguageManager.PurchasePrice + " : " + "\(rawMaterialDetails.buyingPrice)" + " " + Constants.currencySymbol
                }
                
                let quantityAlert = rawMaterialDetails.stockAlert
                if quantityAlert == 0{
                    cell.cautionLbl.text = LanguageManager.QuantityAlert + " : " + "---"
                }else{
                    cell.cautionLbl.text = LanguageManager.QuantityAlert + " : " + "\(rawMaterialDetails.stockAlert)"
                }
                
                cell.editBtn.tag = rawMaterialDetails.materialInventoryId
                cell.editBtn.addTarget(self, action: #selector(onEdit(sender:)), for: .touchUpInside)
                
                let deletable = rawMaterialDetails.deletable
                if deletable == 1 {
                    cell.deleteBtn.isHidden = false
                    cell.deleteBtn.tag = rawMaterialDetails.materialInventoryId
                    cell.deleteBtn.addTarget(self, action: #selector(onDelete(sender:)), for: .touchUpInside)
                }else{
                    cell.deleteBtn.isHidden = true
                }
                
            }
            
            return cell
        case Sections.rawMaterialSummary.rawValue:
            let cell : RawMaterialDetailsSummaryCell = tableView.dequeueReusableCell(withIdentifier: RawMaterialDetailsSummaryCell.identifier, for: indexPath) as! RawMaterialDetailsSummaryCell
            cell.selectionStyle = .none
            if let details = self.rawMaterialDetails {
                let quantity = details.quantity
                if quantity == "" {
                    cell.currentStockLbl.text = "0 " + " " + details.unit
                }else{
                    cell.currentStockLbl.text = quantity + " " + details.unit
                }
                cell.currentStockTitleLbl.text = LanguageManager.CurrentStock
                
                let currentStockAmount = details.currentStockAmount
                if currentStockAmount.isEmpty {
                    cell.currentStockAmountLbl.text = "0.00" + " " + Constants.currencySymbol
                }else{
                    cell.currentStockAmountLbl.text = currentStockAmount + " " + details.unit
                }
                cell.currentStockAmountTitleLbl.text = LanguageManager.CurrentStockAmount
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.rawMaterialInfo.rawValue:
            return UITableView.automaticDimension
        case Sections.rawMaterialSummary.rawValue:
            return 70.0
        default:
            return 0
        }
    }
    
    @objc func onDelete(sender: UIButton){
        let id = sender.tag
        if let inventoryId = self.materialInventoryId,  id == inventoryId {
            self.confirmationMessage(userMessage: LanguageManager.Warning, deleteId: inventoryId)
        }
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: userMessage, message: "Are you sure!", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: LanguageManager.YesDelete, style: .default){
            (action:UIAlertAction!) in
            self.presenter.postRawMaterialDeleteDataToServer(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: LanguageManager.No, style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is ProductListViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    @objc func onEdit(sender: UIBarButtonItem){
        self.navigateToRawMaterialUpdateVC()
    }
    
    func navigateToRawMaterialUpdateVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RawMaterialUpdateViewController") as! RawMaterialUpdateViewController
        viewController.rawMaterialDetails = rawMaterialDetails
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToRawMaterialListVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RawMaterialListViewController") as! RawMaterialListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Api Delegate
extension RawMaterialDetailsBaseViewController : RawMaterialDetailsBaseViewDelegate{
    func setRawMaterialDetailsData(data: RawMaterialDetailsDataMapper) {
        guard let item = data.rawMaterialDetails else {
            return
        }
        self.rawMaterialDetails = item
        self.refreshTableView()
    }
    
    func deleteRawMaterialData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        
        self.showDeleteAlert(title: message, message: "")
        
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        if let id = self.materialInventoryId{
            self.presenter.getRawMaterialDetailsDataFromServer(id: id)
        }
        
    }
}

extension RawMaterialDetailsBaseViewController{
    func showDeleteAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigateToRawMaterialListVC()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
