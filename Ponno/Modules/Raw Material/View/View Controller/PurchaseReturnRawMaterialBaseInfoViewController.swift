//
//  PurchaseReturnRawMaterialBaseInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PurchaseReturnRawMaterialBaseInfoViewController: UIViewController {

    @IBOutlet weak var quantityLbl: UILabel!{
        didSet{
            quantityLbl.text = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var quantityTextField: UITextField!{
        didSet{
            quantityTextField.placeholder = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var returnPriceLbl: UILabel!{
        didSet{
            returnPriceLbl.text = LanguageManager.ReturnPrice
        }
    }
    @IBOutlet weak var returnPriceTextField: UITextField!{
        didSet{
            returnPriceTextField.placeholder = LanguageManager.ReturnPrice
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    
    var delegate : PurchaseReturnRawMaterialDelegate?
    var rawMaterialList : MaterialList?
    var quantity : Double?
    var buyingPrice : Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpViews()
        self.toolBarSetUp()
        self.initialSetup()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext), for: .touchUpInside)
        self.quantityTextField.underlined()
        self.returnPriceTextField.underlined()
        
    }
    
    func initialSetup(){
        self.title = LanguageManager.RawMaterialReturn
        guard let item = self.rawMaterialList else{
            return
        }
        self.returnPriceTextField.text = item.buyingPrice
        self.quantity = Double(item.quantity)
        self.buyingPrice = Double(item.buyingPrice)
    }
    
    func toolBarSetUp(){
        //QuantityToolBar
        let quantityToolBar = UIToolbar()
        quantityToolBar.barStyle = UIBarStyle.default
        quantityToolBar.isTranslucent = true
        quantityToolBar.tintColor = UIColor.black
        quantityToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
        
        quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        quantityToolBar.isUserInteractionEnabled = true
        
        self.quantityTextField.inputAccessoryView = quantityToolBar
        
        //RefundPriceToolBar
        let refundPriceToolBar = UIToolbar()
        refundPriceToolBar.barStyle = UIBarStyle.default
        refundPriceToolBar.isTranslucent = true
        refundPriceToolBar.tintColor = UIColor.black
        refundPriceToolBar.sizeToFit()
        
        let refundPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnRefundPrice(sender:)))
        
        refundPriceToolBar.setItems([cancelButton, spaceButton, refundPriceDoneButton], animated: false)
        refundPriceToolBar.isUserInteractionEnabled = true
        
        self.returnPriceTextField.inputAccessoryView = refundPriceToolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
        self.quantityTextField.resignFirstResponder()
        self.returnPriceTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnRefundPrice(sender: UIBarButtonItem){
        self.returnPriceTextField.resignFirstResponder()
    }
    
    @objc func onNext(sender : UIButton){
        if self.isValidated(){
            guard let quantity = self.quantityTextField.text, let quantityDouble = Double(quantity), let refundPrice = self.returnPriceTextField.text, let refundPriceDouble = Double(refundPrice)  else{
                return
            }
            self.delegate?.setRawMaterialData(quanity: quantityDouble, refundPrice: refundPriceDouble)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func isValidated()->Bool{
        if (self.quantityTextField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.QuantityIsRequired)
            return false
        }
        else if (self.returnPriceTextField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.ReturnPriceIsRequired)
            return false
        }
        guard let buyingPriceText = self.returnPriceTextField.text, let buyingPrice = Double(buyingPriceText), let quantityText = self.quantityTextField.text, let quantity = Double(quantityText), let buyingPriceCheck = self.buyingPrice, let quantityCheck = self.quantity else {
            return false
        }
        if quantity > quantityCheck {
            self.showAlert(title: LanguageManager.QuantityCantBeGreaterThan + " \(quantityCheck)", message: "")
            return false
        }else if buyingPrice > buyingPriceCheck {
            self.showAlert(title: LanguageManager.CashbackPriceCantBeGreaterThan + " \(buyingPriceCheck)", message: "")
            return false
        }
        
        return true
    }

}
