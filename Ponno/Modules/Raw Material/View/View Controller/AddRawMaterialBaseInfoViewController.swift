//
//  AddRawMaterialBaseInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class AddRawMaterialBaseInfoViewController: UIViewController {
    
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var rawMaterialNameLbl: UILabel!{
        didSet{
            rawMaterialNameLbl.text = LanguageManager.RawMaterialName
        }
    }
    @IBOutlet weak var rawMaterialNameTextField: UITextField!{
        didSet{
            rawMaterialNameTextField.placeholder = LanguageManager.RawMaterialName
        }
    }
    @IBOutlet weak var categoryLbl: UILabel!{
        didSet{
            categoryLbl.text = LanguageManager.Category
        }
    }
    @IBOutlet weak var rawMaterialCategoryTextField: UITextField!{
        didSet{
            rawMaterialCategoryTextField.placeholder = LanguageManager.SelectCategory
        }
    }
    @IBOutlet weak var companyLbl: UILabel!{
        didSet{
            companyLbl.text = LanguageManager.Company
        }
    }
    @IBOutlet weak var companyNameTextField: UITextField!{
        didSet{
            companyNameTextField.placeholder = LanguageManager.Company
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var varientLbl: UILabel!{
        didSet{
            varientLbl.text = LanguageManager.Varient
        }
    }
    @IBOutlet weak var varientTextField: UITextField!{
        didSet{
            varientTextField.placeholder = LanguageManager.Varient
        }
    }
    @IBOutlet weak var unitLbl: UILabel!{
        didSet{
            unitLbl.text = LanguageManager.Unit
        }
    }
    @IBOutlet weak var unitTextField: UITextField!{
        didSet{
            unitTextField.text = LanguageManager.Unit
        }
    }
    @IBOutlet weak var rawMaterialCodeLbl: UILabel!{
        didSet{
            rawMaterialCodeLbl.text = LanguageManager.RawMaterialCode
        }
    }
    @IBOutlet weak var rawMaterialCodeTextField: UITextField!{
        didSet{
            rawMaterialCodeTextField.placeholder = LanguageManager.RawMaterialCode
        }
    }
    
    var categoryName : String?
    var nameString : [Int : String] = [:]
    var rawMaterialCategoryId: Int?
    
    private var presenter = AddRawMaterialBaseInfoPresenter(service: RawMaterialService())
    
    var categoryParentId: Int = 0
    var rawMaterial : RawMaterialDetails?

    var name : String = ""
    
    var currentPage : Int = 1
    var isLoading : Bool = false
    var getAll: Bool = true
    
    let imagePicker = UIImagePickerController()
    var selectedImage : UIImage?
    
    var unitId : Int?
    var productUnit : [ProductUnit] = []{
        didSet{
            self.unitPicker.reloadAllComponents()
        }
    }
    var unitPicker = UIPickerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.appendCategoryName()
        self.setUpViews()
        self.setUpPickerView()
        self.configureTextFields()
        self.setImageView()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated);
        if self.isMovingFromParent
        {
            self.categoryName = ""
            self.rawMaterialCategoryId = nil
        }
        if self.isBeingDismissed
        {
            //Dismissed
        }
    }
    
    func setUpViews(){
        self.title = LanguageManager.NewRawMaterial
        
        RawMaterialObject.companyName = ""
        
        self.nextBtn.addTarget(self, action: #selector(onNextBtn(sender:)), for: .touchUpInside)
        //self.navigationController?.navigationBar.topItem?.title = " "
        
        imagePicker.delegate = self
        self.imageIcon.addTapGestureRecognizer(action: {self.onTapOnImageView()})
       
        self.rawMaterialCategoryTextField.isEnabled = false
        
        self.unitTextField.text = "pc"
        self.unitId = 1
        
        
    }
    
    func appendCategoryName(){
        
        let sortedDictionary = self.nameString.sorted{$0.key < $1.key}
        
        
        for (_, value) in sortedDictionary{
            
            self.name +=  " / " + "\(value)"
        }
        self.rawMaterialCategoryTextField.text = self.name
    }
    
    @objc func onNextBtn(sender : UIButton){
        if self.isValidated(){
            RawMaterialObject.rawMaterialImage = self.selectedImage
            RawMaterialObject.rawMaterialName = self.rawMaterialNameTextField.text
            RawMaterialObject.rawMaterialCategoryId = self.rawMaterialCategoryId
            RawMaterialObject.companyName = self.companyNameTextField.text
            RawMaterialObject.varient = self.varientTextField.text
            RawMaterialObject.unitId = self.unitId
            RawMaterialObject.sku = self.rawMaterialCodeTextField.text
            self.navigateToAddRawMaterialAccessoryInfoViewController()
        }
    }
    
    func isValidated()->Bool{
        if self.rawMaterialNameTextField.text == ""{
            showAlert(title: LanguageManager.RawMaterialNameIsRequired, message: "")
            return false
        }else if self.rawMaterialCategoryTextField.text == ""{
            showAlert(title: LanguageManager.CategoryIsRequired, message: "")
            return false
        }else if self.unitTextField.text == ""{
            showAlert(title: LanguageManager.RawMaterialUnitRequired, message: "")
            return false
        }
        return true
    }
    
    func navigateToAddRawMaterialAccessoryInfoViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddRawMaterialAccessoryViewController") as! AddRawMaterialAccessoryViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //Mark: PickerView
    func setUpPickerView(){
        unitPicker.delegate = self
        
        let unitToolBar = UIToolbar()
        unitToolBar.barStyle = UIBarStyle.default
        unitToolBar.isTranslucent = true
        unitToolBar.tintColor = UIColor.black
        unitToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButtonOnUnit = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnUnit(sender:)))
        
        unitToolBar.setItems([cancelButton,spaceButton, doneButtonOnUnit], animated: false)
        unitToolBar.isUserInteractionEnabled = true
        
        self.unitTextField.inputView = unitPicker
        self.unitTextField.inputAccessoryView = unitToolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnUnit(sender : UIBarButtonItem){
        self.rawMaterialCodeTextField.becomeFirstResponder()
    }
    
}

//Mark: PickerViewDelegate
extension AddRawMaterialBaseInfoViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        guard self.productUnit.count > 0 else {
            return 0
        }
        return self.productUnit.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard self.productUnit.count > 0 else {
            return ""
        }
        let unitItem = self.productUnit[row]
        self.unitTextField.text = unitItem.name
        return unitItem.name
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.productUnit.count > 0 {
            let unitItem = self.productUnit[row]
            self.unitTextField.text = unitItem.name
            self.unitId = unitItem.id
        }
    }
    
}

//Mark: TextField Delegate
extension AddRawMaterialBaseInfoViewController: UITextFieldDelegate{
    func configureTextFields(){
        self.rawMaterialNameTextField.delegate = self
        self.rawMaterialCategoryTextField.delegate = self
        self.companyNameTextField.delegate = self
        self.varientTextField.delegate = self
        self.unitTextField.delegate = self
        self.rawMaterialCodeTextField.delegate = self
        
        self.rawMaterialNameTextField.underlined()
        self.rawMaterialCategoryTextField.underlined()
        self.companyNameTextField.underlined()
        self.varientTextField.underlined()
        self.unitTextField.underlined()
        self.rawMaterialCodeTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.rawMaterialNameTextField{
            self.rawMaterialCategoryTextField.becomeFirstResponder()
        }else if textField == self.rawMaterialCategoryTextField{
            self.companyNameTextField.becomeFirstResponder()
        }else if textField == self.companyNameTextField{
            self.varientTextField.becomeFirstResponder()
        }else if textField == self.varientTextField{
            self.unitTextField.becomeFirstResponder()
        }else if textField == self.unitTextField{
            self.rawMaterialCodeTextField.becomeFirstResponder()
        }else if textField == self.rawMaterialCodeTextField{
            self.rawMaterialCodeTextField.resignFirstResponder()
            self.nextBtn.becomeFirstResponder()
        }
        return false
    }
}

//Mark: Api Delegate
extension AddRawMaterialBaseInfoViewController : AddRawMaterialBaseInfoViewDelegate{
    func setProductUnit(unit: ProductUnitDataMapper) {
        guard let item = unit.productUnit, item.count > 0 else {
            return
        }
        self.productUnit = item
    }
    
    func onFailed(data: String) {
        
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getProductUnitDataFromServer()
    }
    
}

//Mark: ImagePicker
extension AddRawMaterialBaseInfoViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    private func setImageView(){
        guard let imageUrl = self.rawMaterial?.image else{
            self.setImageInImageView(imageUrl: "")
            return
        }
        
        self.setImageInImageView(imageUrl: ImageUrl.sharedImageInstance.productImage + imageUrl)
    }
    
    func setImageInImageView(imageUrl : String){
        self.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageIcon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
    }
    
    func onTapOnImageView(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageIcon.contentMode = .scaleAspectFill
            imageIcon.image = pickedImage
            self.selectedImage = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


