//
//  DamageableRawMaterialListViewController.swift
//  Ponno
//
//  Created by a k azad on 23/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

protocol damageableRawMaterialDelegate {
    func setDamagedMaterialData(quantity : Double, buyingPrice : Double, description : String)
}

class DamageableRawMaterialListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    
    private var presenter = DamagedableRawMaterialListPresenter(service: RawMaterialService())
    
    private var rawMaterialList : [MaterialList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var searchText = ""
    var timer : Timer?
    
    var damagedMaterialId : Int?
    
    var crossImage = UIImage(named: "error_red.png")
    
    var selectedMaterialId : [Int] = []
    var selectedQuantities : [Double] = []
    var selectedBuyingPrices : [Double] = []
    var selectedDescription : [String] = []
    
    var selectedRawMaterial : MaterialList?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.attachPresenter()
        self.configureTableVIew()
        self.setUpViews()
        self.setUpSearchBar()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
        self.setUpInitialLanguage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onSubmitBtnTapped(sender:)), for: .touchUpInside)
        self.title = LanguageManager.SelectRawMaterial
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func onSubmitBtnTapped(sender : UIButton){
        if self.selectedMaterialId.count <= 0 {
            self.showAlert(title: LanguageManager.SelectRawMaterialToDamage, message: "")
        }else{
            self.submitData()
        }
    }
    
    func submitData(){
        if self.selectedMaterialId.count > 0 , self.selectedQuantities.count > 0 , self.selectedBuyingPrices.count > 0 {
            let param : [String : Any] = ["materials": self.selectedMaterialId, "damaged_quantities": self.selectedQuantities, "buying_prices": self.selectedBuyingPrices, "descriptions": self.selectedDescription]
            
            self.presenter.postDamagedRawMaterialDataToServer(param: param)
        }
    }
    
//    func navigateToDaRawMaterialAccessoryInfoVC(){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseableRawMaterialAccessoryInfoViewController") as! PurchaseableRawMaterialAccessoryInfoViewController
//
//        viewController.selectedRawMaterialId = self.selectedRawMaterialId
//        viewController.selectedQuantites = self.selectedQuantities
//        viewController.selectedBuyingPrices = self.selectedBuyingPrices
//        viewController.selectedExpireDate = self.selectedExpireDate
//
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(PurchaseableRawMaterialListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.rawMaterialList = []
        self.currentPage = 1
        self.presenter.getRawMaterialSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
        
        refreshControl.endRefreshing()
    }

}


//MARK: SearchBar Delegate
extension DamageableRawMaterialListViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.RawMaterialSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.rawMaterialList = []
        self.isLoading = true
        self.isSearchActive = true
        
        self.presenter.getRawMaterialSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
    }
}

//MARK: TableView Delegate and Data Source
extension DamageableRawMaterialListViewController : UITableViewDelegate, UITableViewDataSource {
    
    private func configureTableVIew(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(RawMaterialListCell.nib, forCellReuseIdentifier: RawMaterialListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 135
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.rawMaterialList.count > 0  {
            return self.rawMaterialList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : RawMaterialListCell = tableView.dequeueReusableCell(withIdentifier: RawMaterialListCell.identifier, for: indexPath) as! RawMaterialListCell
        cell.selectionStyle = .none
        if self.rawMaterialList.count > 0 {
            let rawMaterial = self.rawMaterialList[indexPath.row]
            
            cell.rawMaterial = rawMaterial
            
            if self.selectedMaterialId.contains(rawMaterial.materialId){
                cell.selectBtn.isHidden = false
            }
            else{
                cell.selectBtn.isHidden = true
            }
            
//            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
//            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.rawMaterialImage + rawMaterial.image), placeholderImage: UIImage(named: "Product-1"))
//            
//            cell.nameLabel.text = String(describing: rawMaterial.name)
//            cell.variantLabel.text = "Variant" + ": " + String(describing: rawMaterial.variant)
//            
//            let buyingPrice = rawMaterial.buyingPrice
//            if buyingPrice.isEmpty{
//                cell.buyingPriceLbl.text = "Buying Price" + " : " + "0.00" + Constants.currencySymbol
//            }else{
//                cell.buyingPriceLbl.text = "Buying Price" + " : " + rawMaterial.buyingPrice + Constants.currencySymbol
//            }
//            
//            let quantity = rawMaterial.quantity
//            if quantity.isEmpty{
//                cell.quantityLabel.text =  "Quantity" +  ": " + String(describing: 0.00) + " " + rawMaterial.unit
//            }else{
//                cell.quantityLabel.text =  "Quantity" +  ": " + String(describing: rawMaterial.quantity) + " " + rawMaterial.unit
//                
//                if let quantity = Double(rawMaterial.quantity){
//                    let stockAlert = Double(rawMaterial.stockAlert) ?? 0.00
//                    if quantity <= stockAlert{
//                        cell.quantityLabel.textColor = UIColor.red
//                    }
//                }
//            }
//            
//            cell.categoryLabel.text = "Category" + ": " + String(describing: rawMaterial.categoryName)
            
            //self.selectedRawMaterialName = rawMaterial.name
            cell.selectBtn.tag = rawMaterial.materialId
            cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.rawMaterialList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getRawMaterialSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
            }
        }
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    @objc func onRemove(sender : UIButton){
        let damagedMaterialId = sender.tag

        if self.selectedMaterialId.contains(damagedMaterialId){
            removeDataofRawMaterial(id: damagedMaterialId)
        }
    }

    func removeDataofRawMaterial(id : Int){
        var productAtIndex = -1
        for index in 0..<self.selectedMaterialId.count{
            if self.selectedMaterialId[index] == id{
                productAtIndex = index
            }
        }

        self.selectedQuantities.remove(at: productAtIndex)
        self.selectedBuyingPrices.remove(at: productAtIndex)
        self.selectedMaterialId.remove(at: productAtIndex)

        if selectedDescription.indices.contains(productAtIndex){
            self.selectedDescription.remove(at: productAtIndex)
        }

        self.refreshTableView()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.rawMaterialList.count > 0 {
            let rawMaterial = self.rawMaterialList[indexPath.row]
            
            if self.selectedMaterialId.contains(rawMaterial.materialId){
                self.showAlert(title: LanguageManager.TheRawMaterialIsAlreadySelectedDeselectItThenUpdate, message: "")
            }else{
                self.selectedRawMaterial = rawMaterial
                
                let quantity = rawMaterial.quantity
                if quantity.isEmpty == false && quantity != "0.00"{
                    self.navigateToDamageableRawMaterialBaseInfoVC(rawMaterial: rawMaterial)
                }else{
                    self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
                }
            }
        }
    }
    
    func navigateToDamageableRawMaterialBaseInfoVC(rawMaterial : MaterialList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamageableRawMaterialBaseInfoViewController") as! DamageableRawMaterialBaseInfoViewController
        viewController.rawMaterial = rawMaterial
        viewController.delegate = self 
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
}


//Mark: Api Delegate
extension DamageableRawMaterialListViewController : DamagedableRawMaterialListViewDelegate{
    func setRawMaterialData(data: RawMaterialListDataMapper) {
        
        if let list = data.materialList, list.count > 0 {
            self.isLoading = false
            self.rawMaterialList += list
        }
        
        
        if let pagination = data.pagination {
            self.lastPageNo = pagination.lastPageNo
        }
        
    }
    
    func onDamagedRawMaterialStore(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func setRawMaterialSearchData(data: RawMaterialListDataMapper) {
        guard let list = data.materialList else{
            return
        }
        self.isLoading = false
        self.rawMaterialList += list
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.currentPage = 1
        self.rawMaterialList = []
        self.presenter.getRawMaterialSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
    }
    
}

//Mark: Api Delegate
extension DamageableRawMaterialListViewController : damageableRawMaterialDelegate{
    func setDamagedMaterialData(quantity: Double, buyingPrice: Double, description: String) {
        guard let selectedRawMaterial = self.selectedRawMaterial else{
            return
        }
        self.selectedMaterialId.append(selectedRawMaterial.materialId)
        self.selectedBuyingPrices.append(buyingPrice)
        self.selectedQuantities.append(quantity)
        
        if description == "" {
            self.selectedDescription.append("")
        }else{
            self.selectedDescription.append(description)
        }
        self.refreshTableView()
    }
    
    
}

extension DamageableRawMaterialListViewController{
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: userMessage, message: "", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            //self.navigateDamagedRawMaterialListViewController().
            self.popToSpecificViewController()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is DamagedRawMaterialListViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func navigateDamagedRawMaterialListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamagedRawMaterialListViewController") as! DamagedRawMaterialListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }

}

