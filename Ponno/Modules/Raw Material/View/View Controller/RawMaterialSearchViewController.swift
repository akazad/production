//
//  RawMaterialSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 22/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class RawMaterialSearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var presenter = RawMaterialListPresenter(service: RawMaterialService())
    
    var timer : Timer?
    var rawMaterialList : [MaterialList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var searchText = ""
    
    var currentPage : Int = 1
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    
    var rawMaterialId : Int?
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
        
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configureTableVIew()
        self.setUpSearchBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.presenter.getRawMaterialSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.rawMaterialList = []
            self.refreshTableView()
            
            self.presenter.getRawMaterialSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(RawMaterialListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.rawMaterialList = []
        self.currentPage = 1
        self.presenter.getRawMaterialListDataFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }
    
}

//Mark: Search Delegate
extension RawMaterialSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.RawMaterialSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.rawMaterialList = []
        self.isLoading = true
        self.isSearchActive = true
        
        self.presenter.getRawMaterialSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
    }
}

//MARK: TableView Delegate and Data Source
extension RawMaterialSearchViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableVIew(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(RawMaterialListCell.nib, forCellReuseIdentifier: RawMaterialListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 135
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.rawMaterialList.count > 0  {
            return self.rawMaterialList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : RawMaterialListCell = tableView.dequeueReusableCell(withIdentifier: RawMaterialListCell.identifier, for: indexPath) as! RawMaterialListCell
        cell.selectionStyle = .none
        if self.rawMaterialList.count > 0 {
            let rawMaterial = self.rawMaterialList[indexPath.row]
            
            cell.rawMaterial = rawMaterial
//            
//            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
//            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.rawMaterialImage + rawMaterial.image), placeholderImage: UIImage(named: "Product-1"))
//            
//            cell.nameLabel.text = String(describing: rawMaterial.name)
//            cell.variantLabel.text = "Variant" + ": " + String(describing: rawMaterial.variant)
//            
//            let buyingPrice = rawMaterial.buyingPrice
//            if buyingPrice.isEmpty{
//                cell.buyingPriceLbl.text = "Buying Price" + " : " + "0.00" + Constants.currencySymbol
//            }else{
//                cell.buyingPriceLbl.text = "Buying Price" + " : " + rawMaterial.buyingPrice + Constants.currencySymbol
//            }
//            
//            let quantity = rawMaterial.quantity
//            if quantity.isEmpty{
//                cell.quantityLabel.text =  "Quantity" +  ": " + String(describing: 0.00) + " " + rawMaterial.unit
//            }else{
//                cell.quantityLabel.text =  "Quantity" +  ": " + String(describing: rawMaterial.quantity) + " " + rawMaterial.unit
//                
//                if let quantity = Double(rawMaterial.quantity){
//                    let stockAlert = Double(rawMaterial.stockAlert) ?? 0.00
//                    if quantity <= stockAlert{
//                        cell.quantityLabel.textColor = UIColor.red
//                    }
//                }
//            }
//            
//            cell.categoryLabel.text = "Category" + ": " + String(describing: rawMaterial.categoryName)
            
            cell.selectBtn.isHidden = true
            if isLoading == false && indexPath.row == self.rawMaterialList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getRawMaterialListDataFromServer(page: self.currentPage)
            }
        }
        return cell
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.rawMaterialList.count > 0 {
            let listItem = self.rawMaterialList[indexPath.row]
            self.navigateToRawMaterialDetailsBaseVC(rawMaterial: listItem)
        }
    }
    
    func navigateToRawMaterialDetailsBaseVC(rawMaterial : MaterialList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RawMaterialDetailsBaseViewController") as! RawMaterialDetailsBaseViewController
        viewController.materialInventoryId = rawMaterial.materialInventoryId
        viewController.rawMaterialName = rawMaterial.name
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

//Mark: Api Delegate
extension RawMaterialSearchViewController : RawMaterialListViewDelegate{
    func setRawMaterialData(data: RawMaterialListDataMapper) {
        if let list = data.materialList, list.count > 0 {
            self.isLoading = false
            self.rawMaterialList += list
        }
        
        
        if let pagination = data.pagination {
            self.lastPageNo = pagination.lastPageNo
        }
        
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.currentPage = 1
        self.rawMaterialList = []
        self.presenter.getRawMaterialSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
    }
    
}

extension RawMaterialSearchViewController{
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewRawMaterialViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddRawMaterialCategoryViewController") as! AddRawMaterialCategoryViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToProductionRawMaterialListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductionRawMaterialListViewController") as! ProductionRawMaterialListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToRawMaterialPurchaseListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RawMaterialPurchaseListViewController") as! RawMaterialPurchaseListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
