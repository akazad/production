//
//  RawMaterialDetailsViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class RawMaterialDetailsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!{
        didSet{
            segmentedControl.setTitle(LanguageManager.RecentMaterialStock, forSegmentAt: 0)
            segmentedControl.setTitle(LanguageManager.RecentUse, forSegmentAt: 1)
        }
    }
    @IBOutlet weak var emptyMessage: UILabel!{
        didSet{
            emptyMessage.text = LanguageManager.NoInformationFound
        }
    }
    
    private var presenter = RawMaterialDetailsViewPresenter(service: RawMaterialService())
    
    var materialInventoryId : Int?
    var materialId: Int?
    var vendorName: String?
    var recentStock : [RawMaterialRecentStocksList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var recentUse : [RecentUsagesList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var materialInventoryHistoryId : Int?
    
    //  Segment Propertise
    var isFirst = true
    var selectedSegment : Int?
    var rawMaterialName: String?
    
    //  Pagination Propertise
    var isLoading : Bool = false
    var recentStockCurrentPage : Int = 1
    var recentUsagesCurrentPage : Int = 1
    var recentStockLastPageNo: Int = 0
    var recentUsagesLastPage : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.initialSetup()
        self.configureTableView()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.segmentedControl.addTarget(self, action: #selector(onSegmentChange), for: .valueChanged)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func onSegmentChange(sender: UISegmentedControl){
        if let id = self.materialInventoryId {
            switch segmentedControl.selectedSegmentIndex {
            case 0:
                self.recentStock = []
                self.recentStockCurrentPage = 1
                self.isLoading = true
                self.presenter.getRawMaterialRecentStockDataFromServer(page: self.recentStockCurrentPage, id: id)
                self.refreshTableView()
                
            case 1:
                self.recentUsagesCurrentPage = 1
                self.isLoading = true
                self.recentUse = []
                self.presenter.getRawMaterialRecentUseagesDataFromServer(page: self.recentUsagesCurrentPage, id: id)
                self.refreshTableView()
                
            default:
                break;
            }
        }
        
    }
    
}


//Mark: TableView Delegate and DataSource
extension RawMaterialDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = .clear
        self.tableView.register(RawMaterialRecentStockCell.nib, forCellReuseIdentifier: RawMaterialRecentStockCell.identifier)
        self.tableView.register(RawMaterialRecentUseagesCell.nib, forCellReuseIdentifier: RawMaterialRecentUseagesCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.emptyMessage.isHidden = true
        self.tableView.separatorStyle = .none
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl.selectedSegmentIndex == 0 {
            if self.recentStock.count > 0 {
                self.emptyMessage.isHidden = true
                return self.recentStock.count
            }
        }
        else if segmentedControl.selectedSegmentIndex == 1 {
            if self.recentUse.count > 0 {
                self.emptyMessage.isHidden = true
                return self.recentUse.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if segmentedControl.selectedSegmentIndex == 0 {
            let cell : RawMaterialRecentStockCell = tableView.dequeueReusableCell(withIdentifier: RawMaterialRecentStockCell.identifier, for: indexPath) as! RawMaterialRecentStockCell
            cell.selectionStyle = .none
            if self.recentStock.count > 0 {
                let item = self.recentStock[indexPath.row]
                cell.createdAtLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue, outputDateFormat: DateFormats.dd_MMM_yy_c_HH_mm_a.rawValue)
                cell.createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.currentQuantityLabel.text = "\(item.currentQuantity)" + " " + item.unit + "\n" + LanguageManager.Available
                cell.stockAmountLabel.text = LanguageManager.StockAmount + " : " +  "\(item.quantity)"
                cell.dealerNameLabel.text = LanguageManager.Vendor + " : " + item.vendorName
                cell.buyingPriceLabel.text = LanguageManager.BuyingPrice + " : " + item.buyingPrice
                
                cell.popUpBtn.addTarget(self, action: #selector(onRecentStockPopUpBtnTapped), for: .touchUpInside)
                cell.popUpBtn.tag = item.materialInventoryHistoryId
                
                let expireDate = item.expireDate
                if expireDate.isEmpty == false {
                    cell.expireDateLbl.text = LanguageManager.ExpireDate + " : " + item.expireDate.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
                    
                    let oneMonthAddedDate = Calendar.current.date(byAdding: .month, value: 1, to: Date())
                    
                    
                    let expireDateString = item.expireDate
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    
                    let expireDate = dateFormatter.date(from: expireDateString)
                    
                    if let exDate = expireDate, let oneMonAddedDate = oneMonthAddedDate {
                        if exDate < oneMonAddedDate {
                            cell.expireDateLbl.textColor = UIColor.lightRed
                        }else{
                            cell.expireDateLbl.textColor = UIColor.black
                        }
                    }
                }else{
                    cell.expireDateLbl.text = LanguageManager.ExpireDate + " : " + "---"
                }
                
                //                Pagination
                if isLoading == false && indexPath.row == self.recentStock.count - 1 && self.recentStockCurrentPage < self.recentStockLastPageNo{
                    self.isLoading = true
                    self.recentStockCurrentPage += 1
                    if let id = self.materialInventoryId {
                        self.presenter.getRawMaterialRecentStockDataFromServer(page: self.recentStockCurrentPage, id: id)
                    }
                }
                return cell
            }
        }
        else if segmentedControl.selectedSegmentIndex == 1 {
            let cell : RawMaterialRecentUseagesCell = tableView.dequeueReusableCell(withIdentifier: RawMaterialRecentUseagesCell.identifier, for: indexPath) as! RawMaterialRecentUseagesCell
            cell.selectionStyle = .none
            if self.recentUse.count > 0 {
                let item = self.recentUse[indexPath.row]
                
                cell.createdAtLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue, outputDateFormat: DateFormats.dd_MMM_yy_c_HH_mm_a.rawValue)
                cell.createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.usedQuantityLabel.text = LanguageManager.UsedQuantity + " : " + "\(item.materialQuantity)" + " " + item.unit
                cell.availableQuantityLabel.text = item.currentQuantity + item.unit + "\n " + LanguageManager.Available
                cell.productNameLabel.text = LanguageManager.ProductName + " : " + item.productName
                
                if isLoading == false && indexPath.row == self.recentUse.count - 1 && self.recentUsagesCurrentPage < self.recentUsagesLastPage{
                    self.isLoading = true
                    self.recentUsagesCurrentPage += 1
                    if let id = self.materialInventoryId {
                        self.presenter.getRawMaterialRecentUseagesDataFromServer(page: self.recentUsagesCurrentPage, id: id)
                    }
                }
                return cell
            }
        }
        else{
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if segmentedControl.selectedSegmentIndex == 0{
            return 125.00
        }else{
            return 100.0
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onRecentStockPopUpBtnTapped(sender: UIButton){
        
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if segmentedControl.selectedSegmentIndex == 0 {
            
            if self.recentStock.count > 0 {
                self.materialInventoryHistoryId = sender.tag
                var currentQuantity : Double?
                var quantity : Double?
                for recentStock in self.recentStock{
                    self.materialId = recentStock.materialId
                    self.vendorName = recentStock.vendorName
                    if materialInventoryHistoryId == recentStock.materialInventoryHistoryId{
                        currentQuantity = Double(recentStock.currentQuantity)
                        quantity = Double(recentStock.quantity)
                        
                        let returnAction = UIAlertAction(title: LanguageManager.Return, style: UIAlertAction.Style.default) { (action) in
                            self.navigateToRawMaterialRecentStockReturnVC(recentStock: recentStock)
                        }
                        myActionSheet.addAction(returnAction)
                        
                        if quantity == currentQuantity, let id = materialInventoryHistoryId{
                            let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                                
                                self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                            }
                            
                            myActionSheet.addAction(deleteAction)
                            
                        }
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        myActionSheet.addAction(cancelAction)
                        
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToRawMaterialRecentStockReturnVC(recentStock: RawMaterialRecentStocksList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RawMaterialRecentStockReturnViewController") as! RawMaterialRecentStockReturnViewController
        viewController.recentStock = recentStock
        viewController.materialId = self.materialId
        viewController.inventoryId = self.materialInventoryHistoryId
        viewController.rawMaterialName = self.rawMaterialName
        viewController.vendorName = self.vendorName
        viewController.recentStock = recentStock
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let saleDetailsVC = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        saleDetailsVC.iD = iD
        self.navigationController?.pushViewController(saleDetailsVC, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: LanguageManager.Yes, style: .default){
            (action:UIAlertAction!) in
            
            self.presenter.deleteRawMaterialRecentStockDataFromServer(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: LanguageManager.No, style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.recentStock = []
            self.refreshTableView()
            self.recentStockCurrentPage = 1
            if let id = self.materialInventoryId {
                self.presenter.getRawMaterialRecentStockDataFromServer(page: self.recentStockCurrentPage, id: id)
            }
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension RawMaterialDetailsViewController : RawMaterialDetailsViewDelegate{
    func setRawMaterialRecentStockData(data: RawMaterialRecentStockDataMapper) {
        if let list = data.recentStocksList, list.count > 0 {
            self.isLoading = false
            self.recentStock += list
            self.segmentedControl.selectedSegmentIndex = 0
        }
        
        if let pagination = data.pagination {
            self.recentStockLastPageNo = pagination.lastPageNo
        }
    }
    
    func setRawMaterialRecentUse(data: RawMaterialRecentUseDataMapper) {
        if let list = data.recentUsagesList, list.count > 0 {
            self.isLoading = false
            self.recentUse += list
        }
        
        if let pagination = data.pagination {
            self.recentUsagesLastPage = pagination.lastPageNo
        }
    }
    
    func deleteRawMaterialRecentStockData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onRawMaterialDeleteFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func onFailed(data: String) {
        
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
        self.emptyMessage.isHidden = true
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
        self.emptyMessage.isHidden = false
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        if let id = self.materialInventoryId{
            self.presenter.getRawMaterialRecentStockDataFromServer(page: self.recentStockCurrentPage, id: id)
        }
    }
}
