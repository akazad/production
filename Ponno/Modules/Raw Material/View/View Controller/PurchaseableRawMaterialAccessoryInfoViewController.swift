//
//  PurchaseableRawMaterialAccessoryInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 8/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PurchaseableRawMaterialAccessoryInfoViewController: UIViewController {
    
    @IBOutlet weak var totalPriceLbl: UILabel!{
        didSet{
            totalPriceLbl.text = LanguageManager.TotalPrice
        }
    }
    @IBOutlet weak var totalPriceTxtField: UITextField!{
        didSet{
            totalPriceTxtField.placeholder = LanguageManager.TotalPrice
        }
    }
    @IBOutlet weak var discountLbl: UILabel!{
        didSet{
            discountLbl.text = LanguageManager.Discount
        }
    }
    @IBOutlet weak var discountTextField: UITextField!
    @IBOutlet weak var discountUnitTextField: UITextField!
    @IBOutlet weak var payableAmountLbl: UILabel!{
        didSet{
            payableAmountLbl.text = LanguageManager.PayableAmount
        }
    }
    @IBOutlet weak var payableAmountTextField: UITextField!
    @IBOutlet weak var paidLbl: UILabel!{
        didSet{
            paidLbl.text = LanguageManager.Paid
        }
    }
    @IBOutlet weak var paidTxtField: UITextField!{
        didSet{
            paidTxtField.placeholder = LanguageManager.Paid
        }
    }
    @IBOutlet weak var payableLbl: UILabel!{
        didSet{
            payableLbl.text = LanguageManager.Payable
        }
    }
    @IBOutlet weak var payableTxtField: UITextField!{
        didSet{
            payableTxtField.placeholder = LanguageManager.Payable
        }
    }
    @IBOutlet weak var returnLbl: UILabel!{
        didSet{
            returnLbl.text = LanguageManager.CashBack
        }
    }
    @IBOutlet weak var returnTxtField: UITextField!{
        didSet{
            returnTxtField.placeholder = LanguageManager.CashBack
        }
    }
    @IBOutlet weak var vendorLbl: UILabel!{
        didSet{
            vendorLbl.text = LanguageManager.Vendor
        }
    }
    @IBOutlet weak var vendorTxtField: UITextField!{
        didSet{
            vendorTxtField.placeholder = LanguageManager.SelectOne
        }
    }
    @IBOutlet weak var vendorAddBtn: UIButton!{
        didSet{
            vendorAddBtn.setTitle(LanguageManager.NewVendorAdd, for: .normal)
        }
    }
    @IBOutlet weak var purchaseDateLbl: UILabel!{
        didSet{
            purchaseDateLbl.text = LanguageManager.PurchaseDate
        }
    }
    @IBOutlet weak var purchaseDateTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = PurchaseableRawMaterialAccessoryInfoPresenter(service: RawMaterialService())
    
    //
    var selectedRawMaterialId : [Int] = []
    var selectedQuantites : [Double] = []
    var selectedBuyingPrices : [Double] = []
    var selectedExpireDate : [String] = []
    var multipleSelections : [Int] = []
    
    //
    var isLoading : Bool = false
    var currentPage : Int = 1
    
    //
    var vendorsList : [VendorsList] = []{
        didSet{
            self.vendorsListPicker.reloadAllComponents()
        }
    }
    var vendorsListPicker = UIPickerView()
    var vendorsId : Int?
    
    var paidAmount : Double?
    var payableAmount : Double?
    
    var vendorLastPageNo: Int = 0
    
    var discountUnits : [String] = ["%","৳"]
    var discountUnitPicker = UIPickerView()
    var discount : Double?
    var discountAmount : Double?
    var discountUnit : String?
    
    let dateFormatter = DateFormatter()
    var datePicker = UIDatePicker()
    var purchaseDate : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpPickerView()
        self.toolBarSetUp()
        self.showPurchaseDatePicker()
        self.configureTextFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initialSetUp()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
        
    @objc func backBtnPressed(sender: UIBarButtonItem){
        self.totalPriceTxtField = nil
        self.payableAmountTextField.text = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
//            self.navigateRawMaterialPurchaseListViewController()
            self.popToSpecificViewController()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is RawMaterialPurchaseListViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func navigateRawMaterialPurchaseListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RawMaterialPurchaseListViewController") as! RawMaterialPurchaseListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func initialSetUp(){
        
        self.returnTxtField.isEnabled = false
        self.payableTxtField.isEnabled = false
        self.totalPriceTxtField.isEnabled = false
        self.title = LanguageManager.NewPurchase
        self.discountUnit = "%"
        self.discountUnitTextField.text = "%"
        self.discountUnitTextField.contentVerticalAlignment = .center
        self.discountUnitTextField.textAlignment = .center
        self.discountTextField.text = "\(0.0)"
        self.returnTxtField.text = "\(0.0)"
        
        self.paidTxtField.addTarget(self, action: #selector(onReturnTextFieldEdit), for: .editingDidBegin)
        
        guard self.selectedQuantites.count > 0 else {return}
        guard self.selectedBuyingPrices.count > 0 else { return }
        self.discount = 0.0
        var totalPrice = 0.0
        self.paidAmount = 0.0
        self.discountAmount = 0.0
        self.payableAmount = 0.0
        
        for index in 0..<selectedQuantites.count {
            totalPrice += self.selectedQuantites[index] * self.selectedBuyingPrices[index]
        }
        paidTxtField.text = "\(0.0)"
        totalPriceTxtField.text = "\(totalPrice)"
        payableAmountTextField.text = "\(totalPrice)"
        self.payableTxtField.text = "\(totalPrice)"
        
        self.purchaseDateTextField.text = currentDateFormatter(format: "dd MMM, yyy", date: Date())
        self.purchaseDate = currentDateFormatter(format: "dd MMM, yyy", date: Date())
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.vendorAddBtn.addTarget(self, action: #selector(onVendorAddBtnPressed), for: .touchUpInside)
    }
    
    @objc func onReturnTextFieldEdit(sender: UITextField){
        guard let paid = self.paidTxtField.text, paid != "", let paidAmount = Double(paid) else{ return }
        self.paidAmount = paidAmount
        self.refreshScreen()
    }
    
    @objc func onVendorAddBtnPressed(sender: UIButton){
        self.alertWithTextField()
    }
    
    //paidToolBarSetUp
    func toolBarSetUp(){
        let paidToolBar = UIToolbar()
        paidToolBar.barStyle = UIBarStyle.default
        paidToolBar.isTranslucent = true
        paidToolBar.tintColor = UIColor.black
        paidToolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let paidDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingPaidDone(sender:)))
        
        paidToolBar.setItems([cancelButton, spaceButton, paidDoneButton], animated: false)
        paidToolBar.isUserInteractionEnabled = true
        
        self.paidTxtField.inputAccessoryView = paidToolBar
        
        let discountToolbar = UIToolbar()
        discountToolbar.barStyle = UIBarStyle.default
        discountToolbar.isTranslucent = true
        discountToolbar.tintColor = UIColor.black
        discountToolbar.sizeToFit()
        
        let discountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnDiscount(sender:)))
        
        discountToolbar.setItems([cancelButton, spaceButton, discountDoneButton], animated: false)
        discountToolbar.isUserInteractionEnabled = true
        
        self.discountTextField.inputAccessoryView = discountToolbar
        
        let discountUnitToolbar = UIToolbar()
        discountUnitToolbar.barStyle = UIBarStyle.default
        discountUnitToolbar.isTranslucent = true
        discountUnitToolbar.tintColor = UIColor.black
        discountUnitToolbar.sizeToFit()
        
        let discountUnitDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnDiscountUnit(sender:)))
        
        discountUnitToolbar.setItems([cancelButton, spaceButton, discountUnitDoneButton], animated: false)
        discountUnitToolbar.isUserInteractionEnabled = true
        
        self.discountUnitTextField.inputView = discountUnitPicker
        self.discountUnitTextField.inputAccessoryView = discountUnitToolbar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingPaidDone(sender: UIBarButtonItem){
        guard let paid = self.paidTxtField.text, paid != "", let paidAmount = Double(paid) else{ return }
        self.paidAmount = paidAmount
        self.paidTxtField.resignFirstResponder()
        self.vendorTxtField.becomeFirstResponder()
        self.refreshScreen()
    }
    
    @objc func onPressingDoneOnDiscount(sender: UIBarButtonItem){
        self.discountUnitTextField.becomeFirstResponder()
        self.refreshScreen()
    }
    
    @objc func onPressingDoneOnDiscountUnit(sender: UIBarButtonItem){
        self.paidTxtField.becomeFirstResponder()
        self.refreshScreen()
    }
    
    //PickerView
    func setUpPickerView(){
        
        vendorsListPicker.delegate = self
        discountUnitPicker.delegate = self
        
        let vendorsListToolBar = UIToolbar()
        vendorsListToolBar.barStyle = UIBarStyle.default
        vendorsListToolBar.isTranslucent = true
        vendorsListToolBar.tintColor = UIColor.black
        vendorsListToolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        vendorsListToolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        vendorsListToolBar.isUserInteractionEnabled = true
        
        self.vendorTxtField.inputView = vendorsListPicker
        self.vendorTxtField.inputAccessoryView = vendorsListToolBar
        
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    @objc func onSubmit(sender : UIButton){
        self.submitData()
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func submitData(){
        
        if isValidated(){
            guard self.selectedRawMaterialId.count > 0 else {return}
            guard self.selectedQuantites.count > 0 else {return}
            guard self.selectedBuyingPrices.count > 0 else {return}
            
            guard let total = self.totalPriceTxtField.text, total != "", let totalAmount = Double(total), let payableAmountText = self.payableAmountTextField.text, let payableAmount = Double(payableAmountText) else {
                return
            }
            
            if let paidAmount = self.paidAmount, let discount = self.discount, let discountAmount = self.discountAmount, let discountUnit = self.discountUnit {
                
                var payable : Double = 0.0
                //var cashBack : Double = 0.0
                
                if paidAmount > payableAmount{
                    payable = 0.0
                    //cashBack = paidAmount - payableAmount
                }else{
                    payable = payableAmount - paidAmount
                    //cashBack = 0.0
                }
                
                if let vendorsId =  self.vendorsId {
                    let param : [String : Any] = ["material": self.selectedRawMaterialId,  "quantity" : self.selectedQuantites, "buying_price" : self.selectedBuyingPrices, "total" : totalAmount, "discount": discount, "discount_amount": discountAmount, "discount_unit": discountUnit, "payable_amount": payableAmount, "paid" : paidAmount, "payable" : payable, "expire_date": self.selectedExpireDate, "vendor": vendorsId]
                    
                    self.presenter.postRawMaterialPurchaseDataToServer(param: param)
                }
            }
        } 
    }
    
    func isValidated() -> Bool{
        if (self.vendorTxtField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.VendorNameIsRequired, message: "")
            return false
        }
        return true
    }
    
    func dateFormatWith(date: String) -> String{
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd MMM, yyyy"
        
        guard let originalDate = dateFormatter.date(from: date) else {return ""}
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.string(from: originalDate)
        return dateString
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.view.endEditing(true)
    }

}

//Mark: PickerViewDelegate
extension PurchaseableRawMaterialAccessoryInfoViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.vendorsListPicker{
            return vendorsList.count
        }else if pickerView == self.discountUnitPicker{
            return discountUnits.count
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.vendorsListPicker{
            if self.vendorsList.count > 0 {
                let list = self.vendorsList[row]
                self.vendorTxtField.text = list.name
                self.vendorsId = list.id
                
                if isLoading == false && row == self.vendorsList.count - 1 && self.currentPage < self.vendorLastPageNo {
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getVendorsDataFromServer(page: self.currentPage)
                }
                return self.vendorsList[row].name
            }else{
                return ""
            }
        }else if pickerView == self.discountUnitPicker{
            if self.discountUnits.count > 0 {
                let item = self.discountUnits[row]
                self.discountUnitTextField.text = item
            }
            return self.discountUnits[row]
        }
        else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.vendorsListPicker{
            if self.vendorsList.count > 0 {
                let list = self.vendorsList[row]
                self.vendorTxtField.text = list.name
                self.vendorsId = list.id
            }else{
                return
            }
        }else if pickerView == self.discountUnitPicker{
            if self.discountUnits.count > 0 {
                let item = self.discountUnits[row]
                self.discountUnitTextField.text = item
            }
        }
        else{
            return
        }
    }
}

//Mark: TextField Delegate
extension PurchaseableRawMaterialAccessoryInfoViewController : UITextFieldDelegate{
    func configureTextFields(){
        totalPriceTxtField.delegate = self
        discountTextField.delegate = self
        discountUnitTextField.delegate = self
        payableAmountTextField.delegate = self
        paidTxtField.delegate = self
        payableTxtField.delegate = self
        returnTxtField.delegate = self
        vendorTxtField.delegate = self
        totalPriceTxtField.underlined()
        discountTextField.underlined()
        discountUnitTextField.underlined()
        payableAmountTextField.underlined()
        paidTxtField.underlined()
        payableTxtField.underlined()
        returnTxtField.underlined()
        vendorTxtField.underlined()
        purchaseDateTextField.underlined()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //        if textField == self.discountTextField{
        //            self.discountUnitTextField.becomeFirstResponder()
        //        }
        if textField == self.paidTxtField{
            self.vendorTxtField.becomeFirstResponder()
            self.refreshScreen()
        }else if textField == self.discountTextField{
            self.paidTxtField.becomeFirstResponder()
            self.refreshScreen()
        }
        return true
    }
    
}


//Mark: Refresh TextField
extension PurchaseableRawMaterialAccessoryInfoViewController{
    
    fileprivate func refreshScreen(){
        
        guard self.selectedQuantites.count > 0 else {return}
        guard self.selectedBuyingPrices.count > 0 else { return }
        var totalPrice = 0.0
        
        for index in 0..<selectedQuantites.count {
            totalPrice += self.selectedQuantites[index] * self.selectedBuyingPrices[index]
        }
        
        totalPriceTxtField.text = "\(totalPrice)"
        
        
        guard let discountText = self.discountTextField.text, let discount = Double(discountText) else{
            return
        }
        self.discount = discount
        let discountUnit = self.discountUnitTextField.text
        var payableAmount = 0.0
        
        if discountUnit == "%"{
            self.discountUnit = "%"
            self.discountAmount = (discount * totalPrice) / 100
            payableAmount = totalPrice - ((discount * totalPrice) / 100)
        }else{
            self.discountUnit = "৳"
            self.discountAmount = discount
            payableAmount = totalPrice - discount
        }
        
        self.payableAmountTextField.text = "\(payableAmount)"
        self.payableAmount = payableAmount
        
        guard let paidAmount = self.paidAmount else {
            return
        }
        self.paidTxtField.text = "\(paidAmount)"
        
        if payableAmount > paidAmount {
            let totalPayable = payableAmount - paidAmount
            self.payableTxtField.text = "\(totalPayable)"
            self.returnTxtField.text = "0.0"
        }else{
            let totalDue = paidAmount - payableAmount
            self.returnTxtField.text = "\(totalDue)"
            self.payableTxtField.text = "0.0"
        }
    }
}

extension PurchaseableRawMaterialAccessoryInfoViewController : PurchaseableRawMaterialAccessoryInfoViewDelegate{
    func postNewVendorData(data: VendorAddDataMapper) {
        guard let vendor = data.vendor else {
            return
        }
        self.vendorsId = vendor.id
        self.vendorTxtField.text = vendor.name
        self.vendorTxtField.resignFirstResponder()
        guard let message = data.message else{
            return
        }
        self.showAlert(title: message, message: "")
    }
    
    func setVendorsData(data: VendorsDataMapper) {
        guard let list = data.vendors, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.vendorsList += list
        
        guard let pagination = data.pagination else{
            return
        }
        self.vendorLastPageNo = pagination.lastPageNo
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.presenter.getVendorsDataFromServer(page: self.currentPage)
    }
}

extension PurchaseableRawMaterialAccessoryInfoViewController{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewVendorAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.VendorName
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.PhoneNumber
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            // self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showMessage(userMessage: LanguageManager.VendorNameIsRequired)
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                self.showMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
                return
            }
            
//            if(!self.validatePhoneNumber(value: phone)){
//                self.showMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
//            }
            
            let param : [String : Any] = ["name": textField.text!, "phone": textField2.text!, "address": ""]
            
            self.presenter.postNewVendorDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.alertWithTextField()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: PurchaseDate
extension PurchaseableRawMaterialAccessoryInfoViewController{
    func showPurchaseDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        purchaseDateTextField.inputAccessoryView = toolbar
        purchaseDateTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        purchaseDateTextField.text = currentDateFormatter(format: "dd MMM, yyyy", date: datePicker.date)
        self.purchaseDate = currentDateFormatter(format: "yyyy-MM-dd", date: datePicker.date)
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func currentDateFormatter(format: String, date: Date) -> String{
        dateFormatter.dateFormat = format
        let currentDate = dateFormatter.string(from: date)
        return currentDate
    }
    
}
