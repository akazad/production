//
//  RawMaterialDetailsViewPresenter.swift
//  Ponno
//
//  Created by a k azad on 17/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol RawMaterialDetailsViewDelegate : NSObjectProtocol {
    func setRawMaterialRecentStockData(data : RawMaterialRecentStockDataMapper)
    func setRawMaterialRecentUse(data: RawMaterialRecentUseDataMapper)
    func deleteRawMaterialRecentStockData(data: AddDataMapper)
    func onRawMaterialDeleteFailed(data: String)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class RawMaterialDetailsViewPresenter: NSObject {
    
    private let service : RawMaterialService
    weak private var viewDelegate : RawMaterialDetailsViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : RawMaterialDetailsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getRawMaterialRecentStockDataFromServer(page: Int, id : Int) {
        self.viewDelegate?.showLoading()
        self.service.getRawMaterialRecentStock(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setRawMaterialRecentStockData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getRawMaterialRecentUseagesDataFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getRawMaterialRecentUse(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setRawMaterialRecentUse(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func deleteRawMaterialRecentStockDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostRawMaterialRecentStockDeleteData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.deleteRawMaterialRecentStockData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onRawMaterialDeleteFailed(data: message)
        })
    }
    
}
