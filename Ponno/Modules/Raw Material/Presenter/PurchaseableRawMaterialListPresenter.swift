//
//  PurchaseableRawMaterialListPresenter.swift
//  Ponno
//
//  Created by a k azad on 21/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PurchaseableRawMaterialListViewDelegate : NSObjectProtocol {
    func setRawMaterialData(data : RawMaterialListDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class PurchaseableRawMaterialListPresenter: NSObject {
    
    private let service : RawMaterialService
    weak private var viewDelegate : PurchaseableRawMaterialListViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PurchaseableRawMaterialListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getRawMaterialListDataFromServer(page: Int) {
        self.viewDelegate?.showLoading()
        self.service.getRawMaterialListData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setRawMaterialData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getRawMaterialSearchDataFromServer(page: Int, searchString: String){
        self.viewDelegate?.showLoading()
        self.service.getRawMaterialSearchData(page: page, searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setRawMaterialData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}

