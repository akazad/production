//
//  AddRawMaterialAccessoryInfoPresenter.swift
//  Ponno
//
//  Created by a k azad on 18/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol AddRawMaterialAccessoryInfoViewDelegate : NSObjectProtocol {
    func postNewRawMaterialData(data: RawMaterialAddDataMapper)
    func setVendorListData(data: VendorsDataMapper)
    func postNewVendorData(data: VendorAddDataMapper)
    func onImageUpload(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class AddRawMaterialAccessoryInfoPresenter: NSObject {
    private let service : RawMaterialService
    weak private var viewDelegate : AddRawMaterialAccessoryInfoViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AddRawMaterialAccessoryInfoViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsListFromServer(page: Int){
        let service = VendorsService()
        self.viewDelegate?.showLoading()
        service.getVendorsData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorListData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewVendorDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.postAddNewVendorData(vendorData: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.postNewVendorData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewRawMaterialDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewRawMaterialDataToServer(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.postNewRawMaterialData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
            })
    }
    
    func uploadImage(id : Int,image : UIImage?){
        self.viewDelegate?.showLoading()
        self.service.postRawMaterialImage(rawMaterialId: id, image: image, succeed: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onImageUpload(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
