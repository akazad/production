//
//  PerDayPurchaseRawMaterialPresenter.swift
//  Ponno
//
//  Created by a k azad on 19/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PerDayPurchaseRawMaterialViewDelegate : NSObjectProtocol {
    func setPerDayPurchaseList(data : PerDayPurchaseDataMapper)
    func setPerDayPurchaseCancelData(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class PerDayPurchaseRawMaterialPresenter: NSObject {
    
    private let service : RawMaterialService
    weak private var viewDelegate : PerDayPurchaseRawMaterialViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PerDayPurchaseRawMaterialViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func  getPerDayPurchaseRawMaterialDataFromServer(page: Int, date : String){
        self.viewDelegate?.showLoading()
        self.service.getPerDayPurchaseRawMaterialData(page: page, date: date, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPerDayPurchaseList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getPerDayPurchaseRawMaterialSearchDataFromServer(page: Int, date : String, searchText: String){
        self.viewDelegate?.hideLoading()
        self.service.getPerDayPurchaseRawMaterialSearchData(page: page, date: date, searchText: searchText, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPerDayPurchaseList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postPerDayPurchaseRawMaterialCancelDataToServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostPerDayPurchaseCancelData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPerDayPurchaseCancelData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
