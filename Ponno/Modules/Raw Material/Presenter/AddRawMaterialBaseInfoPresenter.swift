//
//  AddRawMaterialBaseInfoPresenter.swift
//  Ponno
//
//  Created by a k azad on 18/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol AddRawMaterialBaseInfoViewDelegate : NSObjectProtocol {
    func setProductUnit(unit: ProductUnitDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class AddRawMaterialBaseInfoPresenter: NSObject {
    private let service : RawMaterialService
    weak private var viewDelegate : AddRawMaterialBaseInfoViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AddRawMaterialBaseInfoViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductUnitDataFromServer(){
        self.viewDelegate?.showLoading()
        let service = ProductService()
        service.getProductUnitData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductUnit(unit: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
