//
//  RawMaterialDetailsPresenter.swift
//  Ponno
//
//  Created by a k azad on 17/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol RawMaterialDetailsBaseViewDelegate : NSObjectProtocol {
    func setRawMaterialDetailsData(data : RawMaterialDetailsDataMapper)
    func deleteRawMaterialData(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class RawMaterialDetailsPresenter: NSObject {
    
    private let service : RawMaterialService
    weak private var viewDelegate : RawMaterialDetailsBaseViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : RawMaterialDetailsBaseViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getRawMaterialDetailsDataFromServer(id : Int) {
        self.viewDelegate?.showLoading()
        self.service.getRawMaterialViewData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setRawMaterialDetailsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postRawMaterialDeleteDataToServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostRawMaterialDeleteData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.deleteRawMaterialData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
