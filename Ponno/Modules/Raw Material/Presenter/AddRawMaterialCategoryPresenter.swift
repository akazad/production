//
//  AddRawMaterialCategoryPresenter.swift
//  Ponno
//
//  Created by a k azad on 18/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol AddRawMaterialCategoryViewDelegate : NSObjectProtocol {
    func setRawMaterialCategoriesList(categoryData: RawMaterialCategoriesDataMapper)
    func setCategoryAddData(data: CategoryAddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class AddRawMaterialCategoryPresenter: NSObject {
    private let service : RawMaterialService
    weak private var viewDelegate : AddRawMaterialCategoryViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AddRawMaterialCategoryViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getRawMaterialCategoryListFromServer(page : Int){
        self.viewDelegate?.showLoading()
        self.service.getRawMaterialCategoryListData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setRawMaterialCategoriesList(categoryData: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postRawMaterialCategoryAddDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postRawMaterialCategoryAddData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCategoryAddData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
