//
//  RawMaterialUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 18/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol RawMaterialUpdateViewDelegate : NSObjectProtocol {
    func setRawMaterialCategoryData(data : RawMaterialCategoriesDataMapper)
    func onRawMaterialCategoryAdd(data: CategoryAddDataMapper)
    func setProductUnitData(data: ProductUnitDataMapper)
    func onImageUpload(data: AddDataMapper)
    func onSuccess(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class RawMaterialUpdatePresenter: NSObject {
    
    private let service : RawMaterialService
    weak private var viewDelegate : RawMaterialUpdateViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : RawMaterialUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getRawMaterialCategoryDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllRawMaterialCategories(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setRawMaterialCategoryData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postProductUpdateDataToServer(params: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postRawMaterialUpdateData(params: params, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getProductUnitDataFromServer(){
        self.viewDelegate?.showLoading()
        let service = ProductService()
        service.getProductUnitData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductUnitData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postRawMaterialCategoryAddDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postRawMaterialCategoryAddData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onRawMaterialCategoryAdd(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func uploadImage(id : Int, image : UIImage?){
        self.viewDelegate?.showLoading()
        self.service.postRawMaterialImage(rawMaterialId: id, image: image, succeed: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onImageUpload(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
