//
//  PurchaseRawMaterialListPresenter.swift
//  Ponno
//
//  Created by a k azad on 19/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PurchaseRawMaterialListViewDelegate : NSObjectProtocol {
    func setPurchaseList(data : PurchaseDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}
class PurchaseRawMaterialListPresenter: NSObject {
    
    private let service : PurchaseService
    weak private var viewDelegate : PurchaseRawMaterialListViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PurchaseRawMaterialListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func  getRawMaterialPurchaseDataFromServer(page : Int){
        self.viewDelegate?.showLoading()
        let service = RawMaterialService()
        service.getRawMaterialPurchaseListData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPurchaseList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func getRawMaterialPurchaseSearchDataFromServer(page: Int, startDate: String, endDate: String){
        self.viewDelegate?.showLoading()
        let service = RawMaterialService()
        service.getRawMaterialPurchaseSearchData(page: page, startDate: startDate, endDate: endDate, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPurchaseList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}


