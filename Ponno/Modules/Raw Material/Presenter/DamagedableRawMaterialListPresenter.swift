//
//  DamagedableRawMaterialListPresenter.swift
//  Ponno
//
//  Created by a k azad on 24/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol DamagedableRawMaterialListViewDelegate : NSObjectProtocol {
    func setRawMaterialData(data : RawMaterialListDataMapper)
    func onDamagedRawMaterialStore(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class DamagedableRawMaterialListPresenter: NSObject {
    
    private let service : RawMaterialService
    weak private var viewDelegate : DamagedableRawMaterialListViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DamagedableRawMaterialListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getRawMaterialSearchDataFromServer(page: Int, searchString: String){
        self.viewDelegate?.showLoading()
        self.service.getRawMaterialSearchData(page: page, searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setRawMaterialData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postDamagedRawMaterialDataToServer(param: [String : Any]){
        self.viewDelegate?.hideLoading()
        self.service.postDamagedRawMaterialData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDamagedRawMaterialStore(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
