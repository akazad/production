//
//  RawMaterialPurchaseDetailsPresenter.swift
//  Ponno
//
//  Created by a k azad on 21/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol RawMaterialPurchaseDetailsViewDelegate : NSObjectProtocol {
    func setPurchaseDetailsData(data : RawMaterialPurchaseDetailsDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class RawMaterialPurchaseDetailsPresenter: NSObject {
    
    private let service : RawMaterialService
    weak private var viewDelegate : RawMaterialPurchaseDetailsViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : RawMaterialPurchaseDetailsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getRawMaterialPurchaseDetailsDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getRawMaterialPurchaseDetailsData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPurchaseDetailsData(data: data) 
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
