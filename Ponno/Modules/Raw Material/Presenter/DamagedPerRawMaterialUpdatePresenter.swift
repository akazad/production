//
//  DamagedPerRawMaterialUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 24/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol DamagedPerRawMaterialUpdateViewDelegate : NSObjectProtocol {
    func updatePerRawMaterialData(data : AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class DamagedPerRawMaterialUpdatePresenter: NSObject {
    
    private let service : RawMaterialService
    weak private var viewDelegate : DamagedPerRawMaterialUpdateViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DamagedPerRawMaterialUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postDamagedPerRawMaterialUpdateDataToServer(param : [String : Any]) {
        self.viewDelegate?.showLoading()
        self.service.postDamagedPerRawMaterialUpdateData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updatePerRawMaterialData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
