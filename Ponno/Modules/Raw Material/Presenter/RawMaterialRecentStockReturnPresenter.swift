//
//  RawMaterialRecentStockReturnPresenter.swift
//  Ponno
//
//  Created by a k azad on 18/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation

protocol RawMaterialRecentStockReturnViewDelegate : NSObjectProtocol {
    func setVendorsList (data: VendorsDataMapper)
    func onSuccess(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class RawMaterialRecentStockReturnPresenter: NSObject {
    private let service : RawMaterialService
    weak private var viewDelegate : RawMaterialRecentStockReturnViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : RawMaterialRecentStockReturnViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsListDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.getAllVendorsData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postRawMaterialRecentStockReturnDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postRawMaterialRecentStockReturnData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
