//
//  DamagedRawMaterialListPresenter.swift
//  Ponno
//
//  Created by a k azad on 23/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol DamagedRawMaterialListViewDelegate : NSObjectProtocol {
    func setDamagedProductData(data : DamagedRawMaterialDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class DamagedRawMaterialListPresenter: NSObject {
    
    private let service : RawMaterialService
    weak private var viewDelegate : DamagedRawMaterialListViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DamagedRawMaterialListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getDamagedRawMaterialListDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getDamageRawMaterialData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDamagedProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getDamagedRawMaterialSearchDataFromServer(page: Int, searchText: String){
        self.viewDelegate?.showLoading()
        self.service.getDamagedRawMaterialSearchData(page: page, searchString: searchText, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDamagedProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
