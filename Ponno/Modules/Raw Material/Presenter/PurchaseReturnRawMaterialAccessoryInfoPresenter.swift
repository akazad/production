//
//  PurchaseReturnRawMaterialAccessoryInfoPresenter.swift
//  Ponno
//
//  Created by a k azad on 19/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PurchaseReturnRawMaterialAccessoryInfoViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func setVendorsData(data: VendorsDataMapper)
    func postNewVendorData(data: VendorAddDataMapper)
    func onVendorsDataFailed(data: String)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class PurchaseReturnRawMaterialAccessoryInfoPresenter: NSObject {
    
    private let service : RawMaterialService
    weak private var viewDelegate : PurchaseReturnRawMaterialAccessoryInfoViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PurchaseReturnRawMaterialAccessoryInfoViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.getVendorsData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewVendorDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.postAddNewVendorData(vendorData: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.postNewVendorData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onVendorsDataFailed(data: message)
        })
    }
    
    func postPurchaseReturnRawMaterialDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postPurchaseReturnRawMaterialData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
