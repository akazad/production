//
//  DamagedPerDayRawMaterialPresenter.swift
//  Ponno
//
//  Created by a k azad on 23/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol DamagedPerDayRawMaterialViewDelegate : NSObjectProtocol {
    func setDamagedPerRawMaterialData(data : DamagedPerRawMaterialDataMapper)
    func onDeleteDamagedData(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}
class DamagedPerDayRawMaterialPresenter: NSObject {
    
    private let service : RawMaterialService
    weak private var viewDelegate : DamagedPerDayRawMaterialViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DamagedPerDayRawMaterialViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func  getDamagedPerRawMaterialDataFromServer(page: Int, id : Int){
        self.viewDelegate?.showLoading()
        self.service.getDamagePerRawMaterialData(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDamagedPerRawMaterialData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    func deleteDamagedPerRawMaterialDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.deleteDamagedPerRawMaterialData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDeleteDamagedData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
