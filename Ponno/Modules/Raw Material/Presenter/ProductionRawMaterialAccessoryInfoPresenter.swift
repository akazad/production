//
//  ProductionRawMaterialAccessoryInfoPresenter.swift
//  Ponno
//
//  Created by a k azad on 19/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ProductionRawMaterialAccessoryInfoViewDelegate : NSObjectProtocol {
    func setProduct(data: ProductListDataMapper)
    func onProductionStore(data: ProductAddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class ProductionRawMaterialAccessoryInfoPresenter: NSObject {
    private let service : RawMaterialService
    weak private var viewDelegate : ProductionRawMaterialAccessoryInfoViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ProductionRawMaterialAccessoryInfoViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        let service = ProductService()
        service.getAllProductData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProduct(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postProductionDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postProductionData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onProductionStore(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
