//
//  PurchaseableRawMaterialAccessoryInfoPresenter.swift
//  Ponno
//
//  Created by a k azad on 21/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PurchaseableRawMaterialAccessoryInfoViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func setVendorsData(data: VendorsDataMapper)
    func postNewVendorData(data: VendorAddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class PurchaseableRawMaterialAccessoryInfoPresenter: NSObject {
    
    private let service : RawMaterialService
    weak private var viewDelegate : PurchaseableRawMaterialAccessoryInfoViewDelegate?
    
    init(service : RawMaterialService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PurchaseableRawMaterialAccessoryInfoViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.getVendorsData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewVendorDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.postAddNewVendorData(vendorData: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.postNewVendorData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postRawMaterialPurchaseDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postRawMaterialPurchaseData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
