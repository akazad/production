//
//  InvestorsListDataMapper.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class InvestorsListDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [CustomerSummary]?
    var investors : [InvestorsList]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        investors <- map["investors"]
        pagination <- map["pagination"]
    }
    
}
