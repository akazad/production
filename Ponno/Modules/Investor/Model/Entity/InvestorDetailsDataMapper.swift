//
//  InvestorDetailsDataMapper.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class InvestorDetailsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var totalAmount : Double = 0.0
    var totalWithdraw : Double = 0.0
    var investorDetails : InvestorDetails?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        totalAmount <- map["total_amount"]
        totalWithdraw <- map["total_withdraw"]
        investorDetails <- map["investor"]
    }
    
}
