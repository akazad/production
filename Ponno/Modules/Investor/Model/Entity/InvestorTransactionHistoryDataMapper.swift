//
//  InvestorTransactionHistoryDataMapper.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class InvestorTransactionHistoryDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var investorsTransactionsHistory : [InvestorsTransactionsHistory]? 
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        investorsTransactionsHistory <- map["investors_transactions"]
        pagination <- map["pagination"]
    }
    
}
