//
//  InvestorsTransactionsHistory.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class InvestorsTransactionsHistory : Mappable {
    var id : Int = 0
    var investorId : Int = 0
    var amount : String = ""
    var type : Int = 0
    var description : String = ""
    var pharmacyId : Int = 0
    var createdAt : String = ""
    var updatedAt : String = ""
    var deletedAt : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        investorId <- map["investor_id"]
        amount <- map["amount"]
        type <- map["type"]
        description <- map["description"]
        pharmacyId <- map["pharmacy_id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
    }
    
}

