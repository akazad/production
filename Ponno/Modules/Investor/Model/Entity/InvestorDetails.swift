//
//  InvestorDetails.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class InvestorDetails : Mappable {
    var id : Int = 0
    var name : String = ""
    var mobile : String = ""
    var address : String = ""
    var currentAmount : String = ""
    var image : String = ""
    var pharmacyId : Int = 0
    var createdAt : String = ""
    var updatedAt : String = ""
    var deletedAt : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        mobile <- map["mobile"]
        address <- map["address"]
        currentAmount <- map["current_amount"]
        image <- map["image"]
        pharmacyId <- map["pharmacy_id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
    }
    
}
