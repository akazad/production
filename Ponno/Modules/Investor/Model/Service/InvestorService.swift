//
//  InvestorService.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

public class InvestorService : NSObject{
    
    func getInvestorsListData(page: Int, success: @escaping (InvestorsListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Investors + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<InvestorsListDataMapper>) in
                if let investorResponse = response.result.value{
                    if let investors = investorResponse.investors, investors.count > 0 {
                        success(investorResponse)
                    }else if let failureResponse = investorResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    //SearchData
    func getInvestorSearchData(page: Int, searchString: String, success: @escaping (InvestorsListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let loanerURL = RestURL.sharedInstance.Investors + RestURL.sharedInstance.getSearchText(text: searchString) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = loanerURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<InvestorsListDataMapper>) in
                if let investorResponse = response.result.value{
                    if let investors = investorResponse.investors, investors.count > 0 {
                        success(investorResponse)
                    }else if let failureResponse = investorResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getInvestorDetailsData(id: Int, success: @escaping (InvestorDetailsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.InvestorView + RestURL.sharedInstance.getCommonId(id: id)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<InvestorDetailsDataMapper>) in
                if let investorDetailsResponse = response.result.value{
                    if investorDetailsResponse.success == true{
                        success(investorDetailsResponse)
                    }else if let failureMessage = investorDetailsResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    //Mark: LoanDelete
    func PostInvestorDeleteData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.InvestorDelete
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let investorDeleteResponse = response.result.value {
                if investorDeleteResponse.success == true{
                    success(investorDeleteResponse)
                }else if let failureResponse = investorDeleteResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func getInvestorTransactionHistoryData(page: Int, id: Int, success: @escaping (InvestorTransactionHistoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.InvestmentTransactionHistory + RestURL.sharedInstance.getCommonId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<InvestorTransactionHistoryDataMapper>) in
                if let investorTransactionHistoryResponse = response.result.value{
                    if let investments = investorTransactionHistoryResponse.investorsTransactionsHistory, investments.count > 0 {
                        success(investorTransactionHistoryResponse)
                    }else if let failureResponse = investorTransactionHistoryResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    //Mark: PostRequest
    func postAddNewInvestorData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.InvestStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let investorAddResponse = response.result.value {
                if investorAddResponse.success == true{
                    success(investorAddResponse)
                }else if let failureMessage = investorAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postInvestorUpdateData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.InvestorUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let investorUpdateResponse = response.result.value {
                if investorUpdateResponse.success == true{
                    success(investorUpdateResponse)
                }else if let failureMessage = investorUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postInvestmentTransactionUpdateData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.InvestmentTransactionUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let investmentTransactionUpdateResponse = response.result.value {
                if investmentTransactionUpdateResponse.success == true{
                    success(investmentTransactionUpdateResponse)
                }else if let failureMessage = investmentTransactionUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func PostInvestmentTransactionDeleteData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.InvestmentTransactionDelete
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let investorDeleteResponse = response.result.value {
                if investorDeleteResponse.success == true{
                    success(investorDeleteResponse)
                }else if let failureResponse = investorDeleteResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
//    func postLoanWithdrawData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
//        
//        if !Connectivity.isConnectedToInternet{
//            failure(CommonMessages.NoInternet)
//        }
//        
//        let url = RestURL.sharedInstance.LoanWithdraw
//        print(url)
//        let header = RestURL.sharedInstance.postCommonHeader()
//        
//        
//        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
//            print(json)
//        }
//        
//        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
//            if let loanWithdrawResponse = response.result.value {
//                if loanWithdrawResponse.success == true{
//                    success(loanWithdrawResponse)
//                }else if let failureMessage = loanWithdrawResponse.message{
//                    failure(failureMessage)
//                }
//            }else{
//                failure(CommonMessages.ApiFailure)
//            }
//        }
//        print(request)
//    }
//    
    //Mark: ImageUpload
    func postUserImage(investorId : Int, image : UIImage? ,  succeed: @escaping((AddDataMapper)->Void), failure : @escaping((String) -> Void)){
        
        let imageUploadUrl = RestURL.sharedInstance.InvestorImageUpload
        let params = ["investor_id" : "\(investorId)"]
        
        let service = PostImageService()
        
        service.postImage(params: params, image: image, imageUrl: imageUploadUrl, succeed: { response in
            if let success = response.success, success == true{
                succeed(response)
            }
            else{
                failure(response.message ?? "")
            }
        }, failure: {message in
            failure(message)
        })
    }
    
    
//    //    func getAccountsSearchData(startDate: String, endDate: String, success: @escaping (AccountsDataMapper) -> (), failure : @escaping (String) -> ()){
//    //
//    //        if !Connectivity.isConnectedToInternet {
//    //            failure(CommonMessages.NoInternet)
//    //        }
//    //
//    //        let accountsURL = RestURL.sharedInstance.AccountsTemp + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate)
//    //
//    //        guard let url = accountsURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
//    //            return
//    //        }
//    //
//    //        let header = RestURL.sharedInstance.getCommonHeader()
//    //
//    //        let request = Alamofire.request(url, headers: header)
//    //            .responseObject { (response: DataResponse<AccountsDataMapper>) in
//    //                if let accountsResponse = response.result.value{
//    //                    if accountsResponse.success == true{
//    //                        success(accountsResponse)
//    //                    }else if let failureMessage = accountsResponse.message{
//    //                        failure(failureMessage)
//    //                    }
//    //                }
//    //                else {
//    //                    failure(CommonMessages.ApiFailure)
//    //                }
//    //        }
//    //        print(request)
//    //    }
}



