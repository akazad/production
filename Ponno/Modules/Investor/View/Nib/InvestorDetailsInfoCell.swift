//
//  InvestorDetailsInfoCell.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class InvestorDetailsInfoCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var totalInvestmentLbl: UILabel!
    @IBOutlet weak var totalInvestmentTitleLbl: UILabel!{
        didSet{
            totalInvestmentTitleLbl.text = LanguageManager.TotalInvestment
        }
    }
    @IBOutlet weak var payBackLbl: UILabel!
    @IBOutlet weak var payBackTitleLbl: UILabel!{
        didSet{
            payBackTitleLbl.text = LanguageManager.PayBack
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: InvestorDetailsInfoCell.self)
    }
    
}
