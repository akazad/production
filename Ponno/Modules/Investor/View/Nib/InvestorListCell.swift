//
//  InvestorListCell.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class InvestorListCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet var imageIcon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    var investorList : InvestorsList?{
        didSet{
            if let item = investorList{ 

                nameLabel.text = item.name
                phoneLabel.text = item.mobile
                
                let address = item.address
                if address.isEmpty{
                    addressLabel.text = "---"
                }else{
                    addressLabel.text = address
                }
                
                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.investorImage + item.image), placeholderImage: UIImage(named: "placeholder"))
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: InvestorListCell.self)
    }
    
}
