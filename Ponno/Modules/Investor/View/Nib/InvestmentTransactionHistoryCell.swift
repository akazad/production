//
//  InvestmentTransactionHistoryCell.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class InvestmentTransactionHistoryCell: UITableViewCell {
    
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var investmentTransactionsPopUpBtn: UIButton!
    @IBOutlet weak var descriptionImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: backgroundCardView)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: InvestmentTransactionHistoryCell.self)
    }
    
}
