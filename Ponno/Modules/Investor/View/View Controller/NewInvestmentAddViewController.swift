//
//  NewInvestorAddViewController.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class NewInvestmentAddViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var investorSelectionLbl: UILabel!
    @IBOutlet weak var investorSelectionTextField: UITextField!
    @IBOutlet weak var addNewInvestorBtn: UIButton!
    @IBOutlet weak var investorNameLbl: UILabel!
    @IBOutlet weak var investorNameTextField: UITextField!
    @IBOutlet weak var investorPhoneLbl: UILabel!
    @IBOutlet weak var investorPhoneTextField: UITextField!
    @IBOutlet weak var investorAddressLbl: UILabel!
    @IBOutlet weak var investorAddressTextField: UITextField!
    @IBOutlet weak var investAmountLbl: UILabel!
    @IBOutlet weak var investAmountTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    
    private var presenter = NewInvestmentAddPresenter(service: InvestorService())

    var investorList : [InvestorsList]?
    
    var investorId : Int = -1
    
    var newInvestorSetup: Bool = false
    var investorPicker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.cofigureTextField()
        self.setUpPickerView()
        self.setUpInitialLanguage()
//        self.showDeadlineDatePicker()
        self.attachPresenter()
        
    }
    
    func initialSetup(){
        
        self.title = LanguageManager.NewInvestment
        self.investorSelectionLbl.text = LanguageManager.InvestorSelection
        self.investAmountLbl.text = LanguageManager.InvestAmount
        self.descriptionLbl.text = LanguageManager.Description
        self.investorNameLbl.isHidden = true
        self.investorNameTextField.isHidden = true
        self.investorPhoneLbl.isHidden = true
        self.investorPhoneTextField.isHidden = true
        self.investorAddressLbl.isHidden = true
        self.investorAddressTextField.isHidden = true
        self.stackViewHeight.constant = 0.0
        
        self.addNewInvestorBtn.addTarget(self, action: #selector(onAddNewInvestorBtnTapped), for: .touchUpInside)
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
    }
    
    @objc func onAddNewInvestorBtnTapped(sender: UIButton){
        self.newInvestorSetup = !self.newInvestorSetup
        
        if self.newInvestorSetup == false{
            self.investorNameLbl.isHidden = true
            self.investorNameTextField.isHidden = true
            self.investorPhoneLbl.isHidden = true
            self.investorPhoneTextField.isHidden = true
            self.investorAddressLbl.isHidden = true
            self.investorAddressTextField.isHidden = true
            self.stackViewHeight.constant = 0.0
            self.investorSelectionTextField.text = ""
        }else{
            self.investorNameLbl.isHidden = false
            self.investorNameTextField.isHidden = false
            self.investorPhoneLbl.isHidden = false
            self.investorPhoneTextField.isHidden = false
            self.investorAddressLbl.isHidden = false
            self.investorAddressTextField.isHidden = false
            self.stackViewHeight.constant = 223.0
            
            self.investorSelectionTextField.text = LanguageManager.NewInvestorAdd
            self.investorNameLbl.text = LanguageManager.InvestorName
            self.investorPhoneLbl.text = LanguageManager.InvestorPhone
            self.investorAddressLbl.text = LanguageManager.InvestorAddress
        }
        
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            
            let param : [String : Any]
            let type : Int = 1
            let description = self.descriptionTextView.text ?? ""
            guard let amount = self.investAmountTextField.text, !amount.isEmpty else {
                return
            }
            
            if newInvestorSetup == true {
                let investor = "add_new"
                guard let name = self.investorNameTextField.text, let phone = self.investorPhoneTextField.text, let address = self.investorAddressTextField.text, let amount = self.investAmountTextField.text else {
                    return
                }
                
                param = ["investor": investor, "name": name, "mobile": phone, "address": address, "amount": amount, "type": type, "description" : description]
                
            }else{
                param = ["investor" : self.investorId, "amount": amount, "type": type, "description": description]
            }
            
            self.presenter.postNewInvestmentAddDataToServer(param: param)
        }
    }
    
    func isValidated() -> Bool {
        if (investorSelectionTextField.text == "") {
            showAlert(title: LanguageManager.InvestorSelectionIsRequired, message: "")
            return false
        }else if investAmountTextField.text == "" {
            showAlert(title: LanguageManager.InvestorAmountIsRequired, message: "")
            return false
        }
        if self.newInvestorSetup == true {
            if (investorNameTextField.text == "") {
                showAlert(title: LanguageManager.InvestorNameIsRequired, message: "")
                return false
            }else if investorPhoneTextField.text == "" {
                showAlert(title: LanguageManager.PhoneNumberIsRequired, message: "")
                return false
            }else if investorAddressTextField.text == "" {
                showAlert(title: LanguageManager.AddressIsRequired, message: "")
                return false
            }
            
            let phone = self.investorPhoneTextField.text ?? ""
//            if (!self.validatePhoneNumber(value: phone)){
//                showAlert(title: LanguageManager.PhoneNumberIsIncorrect, message: "")
//                return false
//            }
        }
        return true
    }
    
    func submitBtnControllWith(isEnabled: Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    
    //PickerView
    func setUpPickerView(){
        
        investorPicker.delegate = self
        
        //InvestorSelection ToolBar
        let investorToolBar = UIToolbar()
        investorToolBar.barStyle = UIBarStyle.default
        investorToolBar.isTranslucent = true
        investorToolBar.tintColor = UIColor.black
        investorToolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let paymentDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnLoanerSelection(sender:)))
        investorToolBar.setItems([cancelButton, spaceButton, paymentDoneButton], animated: false)
        investorToolBar.isUserInteractionEnabled = true
        
        self.investorSelectionTextField.inputView = investorPicker
        self.investorSelectionTextField.inputAccessoryView = investorToolBar
        
    }
    
    @objc func onPressingDoneOnLoanerSelection(sender: UIBarButtonItem){
        self.investAmountTextField.becomeFirstResponder()
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    func toolbarSetup(){
        let amounttoolbar = UIToolbar()
        amounttoolbar.barStyle = UIBarStyle.default
        amounttoolbar.isTranslucent = true
        amounttoolbar.tintColor = UIColor.black
        amounttoolbar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        let amountDoneButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnInvestAmount(sender:)))
        
        amounttoolbar.setItems([cancelButton, spaceButton, amountDoneButton], animated: false)
        amounttoolbar.isUserInteractionEnabled = true
        
        investAmountTextField.inputAccessoryView = amounttoolbar
        
        let phonetoolbar = UIToolbar()
        phonetoolbar.barStyle = UIBarStyle.default
        phonetoolbar.isTranslucent = true
        phonetoolbar.tintColor = UIColor.black
        phonetoolbar.sizeToFit()
        
        
        let interestAmountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnPhone(sender:)))
        
        phonetoolbar.setItems([cancelButton, spaceButton, interestAmountDoneButton], animated: false)
        phonetoolbar.isUserInteractionEnabled = true
        
        investorPhoneTextField.inputAccessoryView = phonetoolbar
        
    }
    
    @objc func onPressingDoneOnInvestAmount(sender: UIBarButtonItem){
        self.descriptionTextView.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnPhone(sender: UIBarButtonItem){
        self.investorAddressTextField.becomeFirstResponder()
    }


}

//Mark: TextField Delegate
extension NewInvestmentAddViewController : UITextFieldDelegate{
    func cofigureTextField(){
        self.investorSelectionTextField.delegate = self
        self.investorNameTextField.delegate = self
        self.investorPhoneTextField.delegate = self
        self.investorAddressTextField.delegate = self
        self.investAmountTextField.delegate  = self
        
        
        self.investorSelectionTextField.underlined()
        self.investorNameTextField.underlined()
        self.investorPhoneTextField.underlined()
        self.investorAddressTextField.underlined()
        self.investAmountTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == investorNameTextField{
            self.investorPhoneTextField.becomeFirstResponder()
        }else if textField == investorPhoneTextField {
            self.investorAddressTextField.becomeFirstResponder()
        }else if textField == investorAddressTextField {
            self.investAmountTextField.becomeFirstResponder()
        }else if textField == investAmountTextField {
            self.descriptionTextView.becomeFirstResponder()
        }else{
            self.view.endEditing(true)
        }
        return false
    }
    
}

//Mark: Picker Delegate and DataSource
extension NewInvestmentAddViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if let list = self.investorList, list.count > 0 {
            return list.count
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let list = self.investorList, list.count > 0 {
            let item = list[row]
            let investor = item.name
            self.investorId = item.id
            self.investorSelectionTextField.text = investor
            return investor
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let list = self.investorList, list.count > 0  {
            let item = list[row]
            let investor = item.name
            self.investorId = item.id
            self.investorSelectionTextField.text = investor
        }else{
            return
        }
    }
}

//Mark: Api Delegate
extension NewInvestmentAddViewController : NewInvestmentAddViewDelegate{
    func onNewInvestmentAddSuccess(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.submitBtnControllWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControllWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

extension NewInvestmentAddViewController {
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

