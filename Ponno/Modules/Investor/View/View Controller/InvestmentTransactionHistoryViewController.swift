//
//  InvestmentTransactionHistoryViewController.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class InvestmentTransactionHistoryViewController: UIViewController {

    @IBOutlet weak var investmentTransactionBtn: UIButton!{
        didSet{
            investmentTransactionBtn.setTitle(LanguageManager.InvestmentTransactions, for: .normal)
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = InvestmentTransactionPresenter(service: InvestorService())
    
    var investmentTransactions : [InvestorsTransactionsHistory] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var investorId : Int?
    //var loanData : LoanData?
    
    var currentPage : Int = 1
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.initialSetup()
        //self.attachPresenter()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.investmentTransactionBtn.contentEdgeInsets = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.loanTransactions = []
    }
    
    
}

//Mark: TableView Delegate and DataSource
extension InvestmentTransactionHistoryViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(InvestmentTransactionHistoryCell.nib, forCellReuseIdentifier: InvestmentTransactionHistoryCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = true
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.investmentTransactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : InvestmentTransactionHistoryCell = tableView.dequeueReusableCell(withIdentifier: InvestmentTransactionHistoryCell.identifier, for: indexPath) as! InvestmentTransactionHistoryCell
        
        cell.selectionStyle = .none
        
        let item = self.investmentTransactions[indexPath.row]
        
        cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_h_mm_a.rawValue)
        cell.amountLabel.text = LanguageManager.Amount + " : " + item.amount + Constants.currencySymbol
        //cell.nameLabel.text = "Investor" + " : " + item.
        
        let description = item.description
        if description == "" {
            cell.descriptionImage.isHidden = true
        }else{
            cell.descriptionImage.isHidden = false
        }
        
        cell.descriptionImage.addTapGestureRecognizer {
            self.showAlert(title: LanguageManager.Description, message: description)
        }
        
        cell.investmentTransactionsPopUpBtn.tag = item.id
        cell.investmentTransactionsPopUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
        
        if item.type == 1 {
            cell.typeLabel.text = LanguageManager.Investment
            cell.colorView.backgroundColor = UIColor(red: CGFloat(232/255.0), green: CGFloat(94/255.0), blue: CGFloat(88/255.0), alpha: CGFloat(0.8))
        }
        if item.type == 0 {
            cell.typeLabel.text = LanguageManager.PayBack
            cell.colorView.backgroundColor = UIColor(red: CGFloat(90/255.0), green: CGFloat(162/255.0), blue: CGFloat(99/255.0), alpha: CGFloat(0.8))
        }
        if isLoading == false && indexPath.row == self.investmentTransactions.count - 1 && self.currentPage < self.lastPageNo{
            self.isLoading = true
            self.currentPage += 1
            guard let id = self.investorId else{
                return cell
            }
            self.presenter.getInvestmentTransactionDataFromServer(page: self.currentPage, id: id)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        let id = sender.tag
        
        for item in self.investmentTransactions{
            if id == item.id{
                let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                    self.navigateToInvestmentTransactionUpdateVC(item: item)
                }
                
                let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                    self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                }
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                    
                    self.view.endEditing(true)
                }
                
                cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                
                myActionSheet.addAction(editAction)
                myActionSheet.addAction(deleteAction)
                myActionSheet.addAction(cancelAction)
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToInvestmentTransactionUpdateVC(item: InvestorsTransactionsHistory){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Investor", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "InvestmentTransactionUpdateViewController") as! InvestmentTransactionUpdateViewController
        
        viewController.investmentTransactions = item
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            self.presenter.postInvestmentTransactionDeleteDataToServer(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension InvestmentTransactionHistoryViewController : InvestmentTransactionHistoryViewDelegate{
    func setInvestmentTransactionData(data: InvestorTransactionHistoryDataMapper) {
        guard let list = data.investorsTransactionsHistory, list.count > 0 else{
            return
        }
        self.investmentTransactions += list
    }
    
    func onTransactionDelete(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.investorId else{
            return
        }
        print(id)
        self.presenter.getInvestmentTransactionDataFromServer(page: self.currentPage, id: id)
    }
}

extension InvestmentTransactionHistoryViewController {
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.investmentTransactions = []
            self.refreshTableView()
            
            if let id = self.investorId {
                self.presenter.getInvestmentTransactionDataFromServer(page: self.currentPage, id: id)
            }
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
