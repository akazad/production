//
//  InvestorSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class InvestorSearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = InvestorListPresenter(service: InvestorService())
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    
    var investorList : [InvestorsList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var searchText = ""
    var currentPage : Int = 1
    var lastPage : Int = 0
    
    var isLoading : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSearchBar()
        self.configureTableView()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
        self.setUpInitialLanguage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.presenter.getInvestorSearchDataFromServer(text: "", page: self.currentPage)
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}


//Mark: Search Delegate
extension InvestorSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.InvestorSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.investorList = []
        self.isLoading = true
        self.isSearchActive = true
        self.presenter.getInvestorSearchDataFromServer(text: self.searchText, page: self.currentPage)
    }
}

//Mark: TableView Delegate and DataSource
extension InvestorSearchViewController : UITableViewDelegate, UITableViewDataSource {
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(InvestorListCell.nib, forCellReuseIdentifier: InvestorListCell.identifier)
        self.tableView.estimatedRowHeight = 100.0
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = true
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorStyle = .none
        //self.tableView.addSubview(refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.investorList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : InvestorListCell = tableView.dequeueReusableCell(withIdentifier: InvestorListCell.identifier, for: indexPath) as! InvestorListCell
        cell.selectionStyle = .none
        
        let item = self.investorList[indexPath.row]
        
        cell.investorList = item
        
        if isLoading == false && indexPath.row == self.investorList.count - 1 && self.currentPage < self.lastPage{
            self.isLoading = true
            self.currentPage += 1
            self.presenter.getInvestorListDataFromServer(page: self.currentPage)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = self.investorList[indexPath.row]
        
        let id = item.id
        
        self.navigateToLoanDetailsViewController(id: id)
        
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    
}

extension InvestorSearchViewController : InvestorListViewDelegate{
    func setInvestorListData(data: InvestorsListDataMapper) {
        guard let list = data.investors, list.count > 0 else{
            return
        }
        self.investorList += list
        
        guard let pagination = data.pagination else{
            return
        }
        self.lastPage = pagination.lastPageNo
        
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.investorList = []
        self.presenter.getInvestorSearchDataFromServer(text: "", page: self.currentPage)
    }
}

extension InvestorSearchViewController{
    
    func navigateToLoanDetailsViewController(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Investor", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "InvestorDetailsViewController") as! InvestorDetailsViewController
        viewController.investorId = id
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewInvestmentViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Investor", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "NewInvestmentAddViewController") as! NewInvestmentAddViewController
        
        viewController.investorList = self.investorList
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToInvestorSerachViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Investor", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "InvestorSearchViewController") as! InvestorSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
