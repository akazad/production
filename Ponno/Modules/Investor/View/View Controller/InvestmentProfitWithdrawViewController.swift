//
//  InvestmentProfitWithdrawViewController.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class InvestmentProfitWithdrawViewController: UIViewController {

    @IBOutlet weak var investmentWithdrawInfoLbl: UILabel!{
        didSet{
            investmentWithdrawInfoLbl.text = LanguageManager.InvestmentWithdrawInfo
        }
    }
    @IBOutlet weak var investorNameLbl: UILabel!
    @IBOutlet weak var investorNameTextField: UITextField!{
        didSet{
            investorNameTextField.placeholder = LanguageManager.InvestorName
        }
    }
    @IBOutlet weak var currentInvestmentLbl: UILabel!
    @IBOutlet weak var currentInvestmentLblHeight: NSLayoutConstraint!
    @IBOutlet weak var currentInvestmentTextField: UITextField!{
        didSet{
            currentInvestmentTextField.placeholder = LanguageManager.CurrentInvestment
        }
    }
    @IBOutlet weak var currentInvestmentTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var investmentWithdrawAmountLbl: UILabel!
    @IBOutlet weak var investmentWithdrawAmountTextField: UITextField!{
        didSet{
            investmentWithdrawAmountTextField.placeholder = LanguageManager.InvestmentWithdrawAmount
        }
    }
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = InvestmentWithdrawPresenter(service: InvestorService())
    
    var investorDetails : InvestorDetails?
    var investorId : Int?
    //var investorName : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.toolBarSetUp()
        self.configureTextFields()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.investorNameLbl.text = LanguageManager.InvestorName
        self.currentInvestmentLbl.isHidden = true
        self.currentInvestmentLblHeight.constant = 0.0
        self.investmentWithdrawAmountLbl.text = LanguageManager.InvestmentWithdrawAmount
        self.descriptionLbl.text = LanguageManager.Description
        
        if let details = self.investorDetails {
            self.investorNameTextField.text = details.name
            self.currentInvestmentTextField.isHidden = true
            self.currentInvestmentTextFieldHeight.constant = 0.0
            self.investorNameTextField.isEnabled = false
            self.currentInvestmentTextField.isEnabled = false
            self.investorId = details.id
        }
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
    }
    
    
    func toolBarSetUp(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.investmentWithdrawAmountTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        self.investmentWithdrawAmountTextField.resignFirstResponder()
        self.descriptionTextView.becomeFirstResponder()
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if self.isValidated(){
            
            let type : Int = 0
            
            if let withdraw = investmentWithdrawAmountTextField.text, let id = self.investorId {
                let details = descriptionTextView.text ?? ""
                
                let params : [String : Any] = ["investor": id,
                                               "amount" : withdraw,
                                               "type" : type,
                                               "description" : details]
                
                if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                    print(json)
                }
                
                self.presenter.postInvestmentWithdrawDataToServer(param: params)
            }
        }
    }
    
    func isValidated()->Bool{
        if self.investmentWithdrawAmountTextField.text == "" {
            showAlert(title: LanguageManager.InvestmentWithdrawAmountIsRequired, message: "")
            return false
        }
        return true
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }


}

//Mark: Api Delegate
extension InvestmentProfitWithdrawViewController : InvestmentWithdrawViewDelegate{
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
        self.submitBtnControlWith(isEnabled: false)
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
    
}

//Mark: TextField Delegate
extension InvestmentProfitWithdrawViewController : UITextFieldDelegate{
    
    fileprivate func configureTextFields(){
        self.investorNameTextField.delegate = self
        self.currentInvestmentTextField.delegate = self
        self.investmentWithdrawAmountTextField.delegate = self
        self.investorNameTextField.underlined()
        self.currentInvestmentTextField.underlined()
        self.investmentWithdrawAmountTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.investmentWithdrawAmountTextField{
            self.descriptionTextView.becomeFirstResponder()
        }
        else{
            self.view.endEditing(true)
        }
        return false
    }
}
