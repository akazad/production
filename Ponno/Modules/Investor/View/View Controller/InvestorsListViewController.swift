//
//  InvestorsListViewController.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class InvestorsListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var presenter = InvestorListPresenter(service: InvestorService())
    
    var investorList : [InvestorsList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    private var summary : [CustomerSummary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var isLoading : Bool = false
    var currentPage: Int = 1
    var lastPage : Int = 1
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initalSetup()
        self.setBarButton()
        self.configureCollectionView()
        self.configureTableView()        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func initalSetup(){
        self.title = LanguageManager.Investor
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarButton(){
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let loanerAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        loanerAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        loanerAddBtn.addTarget(self, action: #selector(onInvestmentAdd(sender:)), for: UIControl.Event.touchUpInside)
        loanerAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        loanerAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        loanerAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: loanerAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2,barButton1], animated: true)
    }
    
    @objc func onSearch(sender: UIBarButtonItem){
        self.navigateToInvestorSerachViewController()
    }
    
    @objc func onInvestmentAdd(sender: UIBarButtonItem){
        self.navigateToAddNewInvestmentViewController()
    }
    
}

//Mark: CollectionView Delegate and DataSource
extension InvestorsListViewController :  UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate{
    
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(CustomerSummaryCell.nib, forCellWithReuseIdentifier: CustomerSummaryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let item = self.summary, item.count > 0 else{
            return 0
        }
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CustomerSummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomerSummaryCell.identifier, for: indexPath) as! CustomerSummaryCell
        guard let list = self.summary, list.count > 0 else{
            return cell
        }
        let summaryItem = list[indexPath.row]
        
//        for (key, _) in list.enumerated() {
//            if key == 0 {
//                list[0].title = "Total Investor"
//            }else if key == 1 {
//                list[1].title = "Pay Back"
//            }else if key == 2 {
//                list[2].title = "Total Investment"
//            }
//        }
        
        cell.titleLabel.text = summaryItem.title
        cell.valueLabel.text = summaryItem.value
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 60.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.summary, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//Mark: TableView Delegate and DataSource
extension InvestorsListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(InvestorListCell.nib, forCellReuseIdentifier: InvestorListCell.identifier)
        self.tableView.estimatedRowHeight = 100.0
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = true
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorStyle = .none
        //self.tableView.addSubview(refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.investorList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : InvestorListCell = tableView.dequeueReusableCell(withIdentifier: InvestorListCell.identifier, for: indexPath) as! InvestorListCell
        cell.selectionStyle = .none
        
        let item = self.investorList[indexPath.row]
        
        cell.investorList = item
        
        //cell.imageIcon.image = UIImage(named: "loan")
        
        if isLoading == false && indexPath.row == self.investorList.count - 1 && self.currentPage < self.lastPage{
            self.isLoading = true
            self.currentPage += 1
            self.presenter.getInvestorListDataFromServer(page: self.currentPage)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = self.investorList[indexPath.row]
        
        let id = item.id
        
        self.navigateToLoanDetailsViewController(id: id)
        
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    
}

extension InvestorsListViewController : InvestorListViewDelegate{
    func setInvestorListData(data: InvestorsListDataMapper) {
        guard let list = data.investors, list.count > 0 else{
            return
        }
        self.investorList += list
        
        guard let pagination = data.pagination else{
            return
        }
        self.lastPage = pagination.lastPageNo
        
        guard let summary = data.summary, summary.count > 0 else{
            return
        }
        self.summary = summary
        self.setUpAutoScroll()
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.investorList = []
        self.presenter.getInvestorListDataFromServer(page: self.currentPage)
    }
}

extension InvestorsListViewController{
    
    func navigateToLoanDetailsViewController(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Investor", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "InvestorDetailsViewController") as! InvestorDetailsViewController
        viewController.investorId = id
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewInvestmentViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Investor", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "NewInvestmentAddViewController") as! NewInvestmentAddViewController
        
        viewController.investorList = self.investorList
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToInvestorSerachViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Investor", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "InvestorSearchViewController") as! InvestorSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
