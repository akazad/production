//
//  InvestorUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class InvestorUpdateViewController: UIViewController{
    
    @IBOutlet weak var imageIcon: CircularImageView!
    @IBOutlet weak var investorNameLbl: UILabel!
    @IBOutlet weak var investorNameTextField: UITextField!
    @IBOutlet weak var investorPhoneLbl: UILabel!
    @IBOutlet weak var investorPhoneTextField: UITextField!
    @IBOutlet weak var investorAddressLbl: UILabel!
    @IBOutlet weak var investorAddressTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = InvestorUpdatePresenter(service: InvestorService())
    
    var investorDetails : InvestorDetails?
    var investorId : Int?
    
    let imagePicker = UIImagePickerController()
    var selectedImage : UIImage?
    var message : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.configureTextFields()
        self.setUpToolBar()
        self.setImageView()
        self.setUpInitialLanguage()
        self.attachPresenter()
        
    }
    
    func initialSetup(){
        
        imagePicker.delegate = self
        self.imageIcon.addTapGestureRecognizer(action: {self.onTapOnImageView()})
        
        self.title = LanguageManager.InvestorUpdate
        self.investorNameLbl.text = LanguageManager.InvestorName
        self.investorPhoneLbl.text = LanguageManager.PhoneNumber
        self.investorAddressLbl.text = LanguageManager.Address
        
        if let info = self.investorDetails{
            self.investorNameTextField.text = info.name
            self.investorPhoneTextField.text = info.mobile
            self.investorAddressTextField.text = info.address
            self.investorId = info.id
            
            imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.investorImage + info.image), placeholderImage: UIImage(named: "placeholder"))
        }
         self.submitBtn.addTarget(self, action: #selector(onSubmiBtnTapped), for: .touchUpInside)
        
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
        
        if (self.investorNameTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.InvestorNameIsRequired)
            return false
        }
        guard let phone = investorPhoneTextField.text else {
            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
            return false
        }
        if (!validatePhoneNumber(value: phone)){
            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
            return false
        }
        else if(self.investorAddressTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.AddressIsRequired)
            return false
        }
        return true
    }
    
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            //self.navigateToInvestorListViewController()
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToInvestorListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Investor", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "InvestorsListViewController") as! InvestorsListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func setUpToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.investorPhoneTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        self.investorPhoneTextField.resignFirstResponder()
        self.investorAddressTextField.becomeFirstResponder()
    }
    
    @objc func onSubmiBtnTapped(sender: UIButton){
        if self.isValidated(){
            if let name = investorNameTextField.text, let phone = investorPhoneTextField.text, let address = investorAddressTextField.text, let id = self.investorId  {
                
                let params : [String: Any] = ["id": id, "name": name, "mobile": phone, "address": address]
                
                self.presenter.postInvestorUpdateDataToServer(param: params)
            }
        }
    }
    
}

//Mark: TextField Delegate
extension InvestorUpdateViewController : UITextFieldDelegate{
    
    fileprivate func configureTextFields(){
        self.investorNameTextField.delegate = self
        self.investorPhoneTextField.delegate = self
        self.investorAddressTextField.delegate = self
        self.investorNameTextField.underlined()
        self.investorPhoneTextField.underlined()
        self.investorAddressTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.investorNameTextField{
            self.investorPhoneTextField.becomeFirstResponder()
        }else if textField == self.investorAddressTextField{
            self.submitBtn.becomeFirstResponder()
        }
        else{
            self.view.endEditing(true)
        }
        return false
    }
}

//Mark: Api Delegate
extension InvestorUpdateViewController : InvestorUpdateViewDelegate{
    func updateInvestorData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
        self.message = message
    }
    
    func onImageUpload(data: AddDataMapper) {
        self.successMessage(userMessage: self.message ?? "")
    }
    
    func onFailed(data: String) {
        self.displayMessage(userMessage: data)
    }
    
    func showLoading() {
        self.showLoader()
        self.submitBtnControlWith(isEnabled: false)
    }
    
    func hideLoading() {
        self.hideLoader()
        self.submitBtnControlWith(isEnabled: true)
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
    
}

extension InvestorUpdateViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    private func setImageView(){
        if let imageUrl = self.investorDetails?.image {
            self.setImageInImageView(imageUrl: ImageUrl.sharedImageInstance.investorImage + imageUrl)
            
            return
        }else{
            self.setImageInImageView(imageUrl: "")
        }
    }
    
    func setImageInImageView(imageUrl : String){
        self.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageIcon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
    }
    
    func onTapOnImageView(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageIcon.contentMode = .scaleAspectFill
            imageIcon.image = pickedImage
            self.selectedImage = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}


