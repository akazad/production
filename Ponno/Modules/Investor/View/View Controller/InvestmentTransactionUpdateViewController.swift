//
//  InvestmentTransactionUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class InvestmentTransactionUpdateViewController: UIViewController {

    @IBOutlet weak var investorNameLbl: UILabel!
    @IBOutlet weak var investorNameTextField: UITextField!{
        didSet{
            investorNameTextField.placeholder = LanguageManager.SelectOne
        }
    }
    @IBOutlet weak var investedAmountLbl: UILabel!
    @IBOutlet weak var investedAmountTextField: UITextField!{
        didSet{
            investedAmountTextField.placeholder = LanguageManager.Amount
        }
    }
    @IBOutlet weak var editedInvestedAmountLbl: UILabel!
    @IBOutlet weak var editedInvestedAmountTextField: UITextField!{
        didSet{
            editedInvestedAmountTextField.placeholder = LanguageManager.EditedInvestedAmount
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = InvestmentTransactionUpdatePresenter(service: InvestorService())
    
    var investmentTransactions : InvestorsTransactionsHistory?
    var investmentTransactionId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initalSetup()
        self.toolbarSetup()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initalSetup(){
        self.title = LanguageManager.Update
        self.investorNameLbl.text = LanguageManager.InvestorName
        self.investedAmountLbl.text = LanguageManager.Amount
        self.editedInvestedAmountLbl.text = LanguageManager.EditedInvestedAmount
        self.investorNameTextField.underlined()
        self.investedAmountTextField.underlined()
        self.editedInvestedAmountTextField.underlined()
        
        if let transaction = self.investmentTransactions {
//            self.investorNameLbl.text = transaction.
            self.investedAmountTextField.text = transaction.amount
            self.investmentTransactionId = transaction.id
        }
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            
            if let editedAmount = self.editedInvestedAmountTextField.text, let amount = Double(editedAmount), let id = self.investmentTransactionId{
                let param : [String :Any] = ["id": id, "amount" : amount]

                self.presenter.postInvestmentTransactionUpdateDataToServer(param: param)
            }
        }
    }
    
    func toolbarSetup(){
        let amounttoolbar = UIToolbar()
        amounttoolbar.barStyle = UIBarStyle.default
        amounttoolbar.isTranslucent = true
        amounttoolbar.tintColor = UIColor.black
        amounttoolbar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        let amountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnInvestedAmount(sender:)))
        
        amounttoolbar.setItems([cancelButton, spaceButton, amountDoneButton], animated: false)
        amounttoolbar.isUserInteractionEnabled = true
        
        investedAmountTextField.inputAccessoryView = amounttoolbar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnInvestedAmount(sender: UIBarButtonItem){
        self.editedInvestedAmountTextField.becomeFirstResponder()
    }
    
    func isValidated() -> Bool {
        if (editedInvestedAmountTextField.text == "") {
            showAlert(title: LanguageManager.EditedInvestmentAmountIsRequired, message: "")
            return false
        }
        return true
    }
    
    func submitBtnControllWith(isEnabled: Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}

//Mark: Api Delegate
extension InvestmentTransactionUpdateViewController : InvestmentTransactionUpdateViewDelegate{
    func updateInvestmentTransactionData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.submitBtnControllWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControllWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}
