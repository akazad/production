//
//  InvestorDetailsViewController.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import FloatingPanel
import SDWebImage

class InvestorDetailsViewController: UIViewController, FloatingPanelControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = InvestorDetailsPresenter(service: InvestorService())
    
    var fpc: FloatingPanelController!
    var bottomShetVC : InvestmentTransactionHistoryViewController!
    
    var totalAmount : String?
    var totalWithdraw : String?
    var investorDetails : InvestorDetails?{
        didSet{
            self.refreshTableView()
        }
    }
    
    var investorId : Int?
    var investorName: String?
    
    enum Sections : Int{
        case InvestorInfo
        case InvestorSummary
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addBottomSheet()
        self.initialSetup()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.title = LanguageManager.InvestorDetails
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func addBottomSheet(){
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = UIColor.lightGreen.withAlphaComponent(0.5)
        fpc.surfaceView.layer.cornerRadius = 9.0
        fpc.surfaceView.clipsToBounds = true
        
        fpc.surfaceView.shadowHidden = false
        bottomShetVC = storyboard?.instantiateViewController(withIdentifier: "InvestmentTransactionHistoryViewController") as? InvestmentTransactionHistoryViewController
        bottomShetVC.investorId = self.investorId
        
        fpc.set(contentViewController: bottomShetVC)
        fpc.track(scrollView: bottomShetVC.tableView)
        fpc.addPanel(toParent: self)
    }
    
    func removeBottomSheet(){
        fpc.removePanelFromParent(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeBottomSheet()
    }
    
    
}

//Mark: TableView Delegate and DataSource
extension InvestorDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(InvestorDetailsCell.nib, forCellReuseIdentifier: InvestorDetailsCell.identifier)
        self.tableView.register(InvestorDetailsInfoCell.nib, forCellReuseIdentifier: InvestorDetailsInfoCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = true
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorStyle = .none
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.InvestorInfo.rawValue:
            return 1
        case Sections.InvestorSummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case Sections.InvestorInfo.rawValue:
            let cell : InvestorDetailsCell = tableView.dequeueReusableCell(withIdentifier: InvestorDetailsCell.identifier, for: indexPath) as! InvestorDetailsCell
            cell.selectionStyle = .none
            
            guard let item = self.investorDetails else{
                return cell
            }
            
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.investorImage + item.image), placeholderImage: UIImage(named: "placeholder"))
            
            cell.nameLabel.text = item.name
            cell.phoneLabel.text = item.mobile
            cell.addressLabel.text = item.address
            self.investorName = item.name
            
            
            cell.payBackBtn.addTarget(self, action: #selector(onInvestmentPayBackBtnTapped), for: .touchUpInside)
            cell.editBtn.addTarget(self, action: #selector(onInvestorEditBtnTapped), for: .touchUpInside)
            cell.investorDeleteBtn.addTarget(self, action: #selector(onInvestorDeleteBtnTapped), for: .touchUpInside)
            
            return cell
            
        case Sections.InvestorSummary.rawValue:
            let cell : InvestorDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: InvestorDetailsInfoCell.identifier, for: indexPath) as! InvestorDetailsInfoCell
            cell.selectionStyle = .none
            
            if let totalAmount = self.totalAmount, let payBack = self.totalWithdraw {
                
                if totalAmount.isEmpty && totalAmount != ""{
                    cell.totalInvestmentLbl.text = "0.00"  + Constants.currencySymbol
                }else{
                    cell.totalInvestmentLbl.text = totalAmount  + Constants.currencySymbol
                }
                
                if payBack.isEmpty && payBack != ""{
                    cell.payBackLbl.text = "0.00" + Constants.currencySymbol
                }else{
                    cell.payBackLbl.text = payBack + Constants.currencySymbol
                }
                
            }
            
            
            
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onInvestmentPayBackBtnTapped(sender: UIButton){
        self.navigateToInvestmentProfitWithdrawVC()
    }
    
    @objc func onInvestorEditBtnTapped(sender: UIButton){
        
        self.navigateToInvestorUpdateVC()
    }
    
    @objc func onInvestorDeleteBtnTapped(sender: UIButton){
        guard let loanerId = self.investorId else {
            return
        }
        self.confirmationMessage(userMessage: "", deleteId: loanerId)
    }
    
    
}

extension InvestorDetailsViewController : InvestorDetailsViewDelegate{
    func setInvestorDetailsData(data: InvestorDetailsDataMapper) {
        guard let info = data.investorDetails else{
            return
        }
        self.investorDetails = info
        self.investorId = info.id
        self.totalAmount = "\(data.totalAmount)"
        self.totalWithdraw = "\(data.totalWithdraw)"
    }
    
    func onLoanerDelete(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        
        self.showDeleteAlert(title: message, message: "")
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.investorId else {
            return
        }
        self.presenter.getInvestorDetailsDataFromServer(id: id)
    }
}

extension InvestorDetailsViewController {
    func navigateToInvestmentProfitWithdrawVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Investor", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "InvestmentProfitWithdrawViewController") as! InvestmentProfitWithdrawViewController
//        viewController.loanerName = self.loanerName
        viewController.investorDetails = self.investorDetails
//        viewController.loanerId = self.loanerId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToInvestorUpdateVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Investor", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "InvestorUpdateViewController") as! InvestorUpdateViewController
        viewController.investorId = self.investorId
        if let details = self.investorDetails {
            viewController.investorDetails = details
        }
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.AreYouSure, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            self.presenter.postInvestorDeleteDataToServer(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showDeleteAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
//    func navigateToLoanListVC(){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoanerListViewController") as! LoanerListViewController
//        
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
    
}
