//
//  InvestorListPresenter.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol InvestorListViewDelegate : NSObjectProtocol {
    func setInvestorListData(data: InvestorsListDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class InvestorListPresenter : NSObject {
    
    private var service : InvestorService
    weak private var viewDelegate : InvestorListViewDelegate?
    
    init(service : InvestorService) {
        self.service = service
    }
    
    func attachView(viewDelegate : InvestorListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getInvestorListDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getInvestorsListData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setInvestorListData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getInvestorSearchDataFromServer(text: String, page: Int){
        self.viewDelegate?.showLoading()
        self.service.getInvestorSearchData(page: page, searchString: text, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setInvestorListData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
//    func getInvestorSearchDataFromServer(page: Int, searchString : String){
//        self.viewDelegate?.showLoading()
//        
//    }
    
}
