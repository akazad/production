//
//  InvestmentTransactionUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 27/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol InvestmentTransactionUpdateViewDelegate : NSObjectProtocol {
    func updateInvestmentTransactionData(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}
class InvestmentTransactionUpdatePresenter: NSObject {
    
    private let service : InvestorService
    weak private var viewDelegate : InvestmentTransactionUpdateViewDelegate?
    
    init(service : InvestorService) {
        self.service = service
    }
    
    func attachView(viewDelegate : InvestmentTransactionUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postInvestmentTransactionUpdateDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postInvestmentTransactionUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateInvestmentTransactionData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    
}
