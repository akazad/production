//
//  InvestorDetailsPresenter.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol InvestorDetailsViewDelegate : NSObjectProtocol {
    func setInvestorDetailsData(data: InvestorDetailsDataMapper)
    func onLoanerDelete(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class InvestorDetailsPresenter : NSObject {
    
    private var service : InvestorService
    weak private var viewDelegate : InvestorDetailsViewDelegate?
    
    init(service : InvestorService) {
        self.service = service
    }
    
    func attachView(viewDelegate : InvestorDetailsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getInvestorDetailsDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getInvestorDetailsData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setInvestorDetailsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postInvestorDeleteDataToServer(id: Int){
        self.viewDelegate?.hideLoading()
        self.service.PostInvestorDeleteData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onLoanerDelete(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    //    func getLoanerSearchDataFromServer(page: Int, searchString : String){
    //        self.viewDelegate?.showLoading()
    //
    //    }
    
}
