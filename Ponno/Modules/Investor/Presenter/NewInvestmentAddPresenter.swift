//
//  NewInvestmentAddPresenter.swift
//  Ponno
//
//  Created by a k azad on 27/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol NewInvestmentAddViewDelegate : NSObjectProtocol {
    func onNewInvestmentAddSuccess(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class NewInvestmentAddPresenter : NSObject {
    
    private var service : InvestorService
    weak private var viewDelegate : NewInvestmentAddViewDelegate?
    
    init(service : InvestorService) {
        self.service = service
    }
    
    func attachView(viewDelegate : NewInvestmentAddViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postNewInvestmentAddDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postAddNewInvestorData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onNewInvestmentAddSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    
}
