//
//  InvestorUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 27/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol InvestorUpdateViewDelegate : NSObjectProtocol {
    func updateInvestorData(data: AddDataMapper)
    func onImageUpload(data : AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}
class InvestorUpdatePresenter: NSObject {
    
    private let service : InvestorService
    weak private var viewDelegate : InvestorUpdateViewDelegate?
    
    init(service : InvestorService) {
        self.service = service
    }
    
    func attachView(viewDelegate : InvestorUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postInvestorUpdateDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postInvestorUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateInvestorData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
        
    }
    
}
