//
//  InvestmentTransactionPresenter.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol InvestmentTransactionHistoryViewDelegate : NSObjectProtocol {
    func setInvestmentTransactionData(data: InvestorTransactionHistoryDataMapper)
    func onTransactionDelete(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class InvestmentTransactionPresenter : NSObject {
    
    private var service : InvestorService
    weak private var viewDelegate : InvestmentTransactionHistoryViewDelegate?
    
    init(service : InvestorService) {
        self.service = service
    }
    
    func attachView(viewDelegate : InvestmentTransactionHistoryViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getInvestmentTransactionDataFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getInvestorTransactionHistoryData(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setInvestmentTransactionData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postInvestmentTransactionDeleteDataToServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostInvestmentTransactionDeleteData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onTransactionDelete(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    //    func getLoanerSearchDataFromServer(page: Int, searchString : String){
    //        self.viewDelegate?.showLoading()
    //        self.service.getLoanerSearchData(page: page, searchString: searchString, success: { (data) in
    //            self.viewDelegate?.hideLoading()
    //            self.viewDelegate?.setLoanerListData(data: data)
    //        }, failure: { (message) in
    //            self.viewDelegate?.hideLoading()
    //            self.viewDelegate?.onFailed(data: message)
    //        })
    //    }
    
}
