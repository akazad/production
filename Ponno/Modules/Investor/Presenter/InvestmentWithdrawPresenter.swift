//
//  InvestmentWithdrawPresenter.swift
//  Ponno
//
//  Created by a k azad on 27/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol InvestmentWithdrawViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class InvestmentWithdrawPresenter: NSObject {
    
    private let service : InvestorService
    weak private var viewDelegate : InvestmentWithdrawViewDelegate?
    
    init(service : InvestorService) {
        self.service = service
    }
    
    func attachView(viewDelegate : InvestmentWithdrawViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postInvestmentWithdrawDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postAddNewInvestorData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
