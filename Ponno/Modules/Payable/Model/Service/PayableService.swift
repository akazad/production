//
//  PayableService.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper

public class PayableService : NSObject{
    
    func getPayableData(page: Int, success: @escaping (PayableListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Payable + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<PayableListDataMapper>) in
                if let payableResponse = response.result.value{
                    if payableResponse.success == true{
                        success(payableResponse)
                    }else if let failureMessage = payableResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //PayableListSearch
    func getPayableListSearchData(page: Int, searchText: String, success: @escaping (PayableListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let payableListURL = RestURL.sharedInstance.Payable + RestURL.sharedInstance.getSearchText(text: searchText) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = payableListURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<PayableListDataMapper>) in
                if let payableResponse = response.result.value{
                    if let payable = payableResponse.payableList, payable.count > 0 {
                        success(payableResponse)
                    }
                    else if let payableListFailure = payableResponse.message{
                        failure(payableListFailure)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getPayableLogData(page: Int, success: @escaping (PayableLogDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PayableLog + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<PayableLogDataMapper>) in
                if let payableResponse = response.result.value{
                    if payableResponse.success == true{
                        success(payableResponse)
                    }else if let failureMessage = payableResponse.message{
                        failure(failureMessage)
                    }
                }else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //PayableLogSearch
    func getPayableLogSearchData(page: Int, startDate: String, endDate: String, success: @escaping (PayableLogDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let payableLogURL = RestURL.sharedInstance.PayableLog + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getPageNumber(page: page)
        
        print(payableLogURL)
        
        guard let url = payableLogURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<PayableLogDataMapper>) in
                if let payableLogResponse = response.result.value{
                    if let payableLog = payableLogResponse.payableLogList, payableLog.count > 0 {
                        success(payableLogResponse)
                    }
                    else if let payableLogFailure = payableLogResponse.message{
                        failure(payableLogFailure)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //Mark: PostRequest
    func postAddNewPayableDueData(payableDueData: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PayableAdd
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = payableDueData
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let payableAddResponse = response.result.value{
                if payableAddResponse.success == true{
                    success(payableAddResponse)
                }else if let failureMessage = payableAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func deletePayableData(id: Int, success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PayableDelete
        print(url +  "\(id)")
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let payableDeleteResponse = response.result.value {
                if payableDeleteResponse.success == true{
                    success(payableDeleteResponse)
                }else if let failureMessage = payableDeleteResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postPayablePaidData(payablePaidData: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PayableAdd
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = payablePaidData
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let payableAddResponse = response.result.value{
                if payableAddResponse.success == true{
                    success(payableAddResponse)
                }else if let failureMessage = payableAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postEditPayableData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.VendorUpdate
        print(url)
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let payableAddResponse = response.result.value {
                if payableAddResponse.success == true{
                    success(payableAddResponse)
                }else if let failureMessage = payableAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postUpdatePayableData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PayableUpdate
        print(url)
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let payableUpdateResponse = response.result.value {
                if payableUpdateResponse.success == true{
                    success(payableUpdateResponse)
                }else if let failureMessage = payableUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
}

