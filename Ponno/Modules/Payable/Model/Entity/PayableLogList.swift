//
//  PayableLogList.swift
//  Ponno
//
//  Created by a k azad on 2/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class PayableLogList : Mappable {
    var id : Int = 0
    var type : Int = 0
    var amount : String = ""
    var description : String = ""
    var createdAt : String = ""
    var vendorId : Int = 0
    var name : String = ""
    var currentPayable : String = ""
    var purhcaseId : Int?
    var invoice : String = ""
    var withdraw : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        type <- map["type"]
        amount <- map["amount"]
        description <- map["description"]
        createdAt <- map["created_at"]
        vendorId <- map["vendor_id"]
        name <- map["name"]
        currentPayable <- map["current_payable"]
        purhcaseId <- map["purhcase_id"]
        invoice <- map["invoice"]
        withdraw <- map["withdraw_amount"]
    }
    
}
