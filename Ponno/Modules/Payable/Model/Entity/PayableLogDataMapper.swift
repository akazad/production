//
//  PayableLogDataMapper.swift
//  Ponno
//
//  Created by a k azad on 2/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PayableLogDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var payableLogList : [PayableLogList]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        payableLogList <- map["payable_list"]
        pagination <- map["pagination"]
    }
    
}

