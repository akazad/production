//
//  PayableList.swift
//  Ponno
//
//  Created by a k azad on 26/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PayableList : Mappable { 
    var id : Int = 0
    var name : String = ""
    var image : String = ""
    var currentPayable : String = ""
    var address : String = ""
    var phone : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        image <- map["image"]
        currentPayable <- map["current_payable"]
        address <- map["address"]
        phone <- map["phone"]
    }
    
}
