//
//  PayableListDataMapper.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PayableListDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var payableList : [PayableList]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        payableList <- map["payable_list"]
        pagination <- map["pagination"]
    }
    
}




