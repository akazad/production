//
//  PayableListSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 18/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class PayableListSearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = PayableListSearchPresenter(service: PayableService())

    var payableList : [PayableList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var vendorsId : Int?
    
    var searchText : String = ""
    var currentPage : Int = 1
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    var type : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpSearchBar()
        self.configureTableView()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
        self.setUpInitialLanguage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.presenter.getPayableListSearchDataFromServer(page: self.currentPage, searchText: "")
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Search Delegate
extension PayableListSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.VendorSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.payableList = []
        self.isLoading = true
        self.isSearchActive = true
        self.presenter.getPayableListSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
    }
}

//MARK: TableView Delegate And DataSoruce
extension PayableListSearchViewController : UITableViewDelegate, UITableViewDataSource{
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PayableLogCell.nib, forCellReuseIdentifier: PayableLogCell.identifier)
        self.tableView.register(PayableListCell.nib, forCellReuseIdentifier: PayableListCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.payableList.count > 0 {
            return self.payableList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PayableListCell = tableView.dequeueReusableCell(withIdentifier: PayableListCell.identifier, for: indexPath) as! PayableListCell
        cell.selectionStyle = .none
        cell.clipsToBounds = true
        
        if self.payableList.count > 0 {
            let Vendor = self.payableList[indexPath.row]
            
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.vendorImage + Vendor.image),  placeholderImage: UIImage(named: "placeholder"))
            cell.nameLabel.text = Vendor.name
            cell.currentPayableLabel.text = LanguageManager.CurrentDue + " : " + "\(Vendor.currentPayable)" + " " + Constants.currencySymbol
            cell.phoneLabel.text = Vendor.phone
            cell.addressLabel.text = Vendor.address
            
            cell.vendorPayablePopUpBtn.tag = Vendor.id
            cell.vendorPayablePopUpBtn.addTarget(self, action: #selector(onVendorPayablePopUpBtnTapped), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.payableList.count - 1 && self.currentPage < self.lastPageNo {
                self.isLoading = true
                self.currentPage += 1
                if isSearchActive{
                    self.presenter.getPayableListSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
                }
            }
        }
        return cell
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    fileprivate func refreshTableView(){
        tableView.reloadData()
        tableView.setContentOffset(.zero, animated: false)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        
        let paid = UITableViewRowAction(style: .normal, title: "দেনা পূরণ") { action, index in
            
            if self.payableList.count > 0 {
                let list = self.payableList[indexPath.row]
                self.navigateToPaidVC(data: list)
            }
        }
        
        paid.backgroundColor = UIColor.orange
        
        let update = UITableViewRowAction(style: .normal, title: "পরিবর্তন")
        { action, index in
            if self.payableList.count > 0 {
                let list = self.payableList[indexPath.row]
                self.navigateToPayableVC(data: list)
            }
        }
        update.backgroundColor = UIColor.green
        
        return [update, paid]
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.payableList.count > 0 {
            let payable = self.payableList[indexPath.row]
            let vendorsId = payable.id
            self.navigateToVendorDetailsVC(vendorId: vendorsId)
        }
    }
    
    @objc func onVendorPayablePopUpBtnTapped(sender : UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
                if self.payableList.count > 0 {
                    self.vendorsId = sender.tag
                    for item in self.payableList{
                        if self.vendorsId == item.id{
                            let duePaidAction = UIAlertAction(title: LanguageManager.PayablePaid, style: UIAlertAction.Style.default) { (action) in
                                self.navigateToPaidVC(data: item)
                            }
                            
                            let udpateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                                self.self.navigateToPayableVC(data: item)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                
                                self.view.endEditing(true)
                            }
                            
                            cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                            
                            myActionSheet.addAction(duePaidAction)
                            myActionSheet.addAction(udpateAction)
                            myActionSheet.addAction(cancelAction)
                        }
                    }
                }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToPayableVC(data: PayableList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewPayableViewController") as! AddNewPayableViewController
        self.type = 0
        viewController.type = self.type
        viewController.payableList = data
        viewController.payableState = PayableState.list.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPaidVC(data: PayableList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewPayableViewController") as! AddNewPayableViewController
        self.type = 1
        viewController.type = self.type
        viewController.payableList = data
        viewController.payableState = PayableState.list.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    fileprivate func navigateToVendorDetailsVC(vendorId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorDetailsBaseViewController") as! VendorDetailsBaseViewController
        viewController.vendorId = vendorId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
//    func displayMessage(userMessage:String) -> Void {
//        DispatchQueue.main.async {
//            let alertController = UIAlertController(title: "Alert", message: userMessage, preferredStyle: .alert)
//
//            let OKAction = UIAlertAction(title: "OK", style: .default){
//                (action:UIAlertAction!) in
//                DispatchQueue.main.async {
//                    self.dismiss(animated: true, completion: nil)
//                    self.payableLog = []
//                    self.refreshTableView()
//                    self.getPayablePerCategoryData()
//                }
//            }
//            alertController.addAction(OKAction)
//            self.present(alertController, animated: true, completion: nil)
//
//        }
//    }
    
//    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
//        DispatchQueue.main.async {
//            let alertController = UIAlertController(title: "Alert", message: userMessage, preferredStyle: .alert)
//
//            let OKAction = UIAlertAction(title: "OK", style: .default){
//                (action:UIAlertAction!) in
//                self.dismiss(animated: true, completion: nil)
//                self.presenter.deletePayableLogDataFromServer(id: deleteId)
//            }
//            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
//                (action:UIAlertAction!) in
//                self.dismiss(animated: true, completion: nil)
//            }
//            alertController.addAction(OKAction)
//            alertController.addAction(cancelAction)
//            self.present(alertController, animated: true, completion: nil)
//
//        }
//    }
    
}

//Mark: Api Delegate
extension PayableListSearchViewController : PayableListSearchViewDelegate {
    func setPayableListSearchData(data: PayableListDataMapper) {
        guard let list = data.payableList, list.count > 0 else {
            return
        }
        self.payableList += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.searchAlert(title: LanguageManager.NoInformationFound, message: data)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getPayableListSearchDataFromServer(page: self.currentPage, searchText: searchText)
    }
}
