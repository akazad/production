//
//  EditPayableViewController.swift
//  Ponno
//
//  Created by a k azad on 24/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class EditPayableViewController: UIViewController {
    
    
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var vendorNameLbl: UILabel!{
        didSet{
            vendorNameLbl.text = LanguageManager.VendorName
            
        }
    }
    @IBOutlet weak var vendorNameText: UITextField!{
        didSet{
            vendorNameText.placeholder = LanguageManager.VendorName
            
        }
    }
    @IBOutlet weak var vendorPhoneLbl: UILabel!{
        didSet{
            vendorPhoneLbl.text = LanguageManager.Phone
        }
    }
    @IBOutlet weak var vendorPhoneText: UITextField!{
        didSet{
            vendorPhoneText.placeholder = LanguageManager.Phone
        }
    }
    @IBOutlet weak var vendorAddressLbl: UILabel!{
        didSet{
            vendorAddressLbl.text = LanguageManager.Address
        }
    }
    @IBOutlet weak var vendorAddressText: UITextField!{
        didSet{
            vendorAddressText.placeholder = LanguageManager.Address
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = EditPayablePresenter(service: PayableService())
    
    var payableList : PayableList?
    var vendorId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
        self.setUpInitialLanguage()
    }
    
    func initialSetUp(){
        self.vendorNameText.underlined()
        self.vendorPhoneText.underlined()
        self.vendorAddressText.underlined()
        
        guard let data = self.payableList else {
            return
        }
        self.vendorNameText.text = data.name
        self.vendorAddressText.text = data.address
        self.vendorPhoneText.text = data.phone
        self.vendorId = data.id
        
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let name = self.vendorNameText, let phone = self.vendorPhoneText, let address = self.vendorAddressText, let id = self.vendorId else {
                return
            }
            let param : [String : Any] = ["id": id, "name": name, "phone": phone, "address": address]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            self.presenter.postPayableEditDataToServer(param: param)
        }
    }
    
    func isValidated()->Bool{
        if self.vendorNameText.text == "" {
            self.showAlert(title: LanguageManager.VendorNameIsRequired, message: "")
            return false
        }else if self.vendorPhoneText.text == ""{
            self.showAlert(title: LanguageManager.PhoneNumberIsRequired, message: "")
            return false
        }else if self.vendorAddressText.text == ""{
            self.showAlert(title: LanguageManager.AddressIsRequired, message: "")
            return false
        }
        return true
    }
}


//Mark: Api Delegate
extension EditPayableViewController : EditPayableViewDelegate{
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.showAlert(title: message, message: "")
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.SorryForTheInterupt, message: data)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}
