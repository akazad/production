//
//  PayablePaidViewController.swift
//  Ponno
//
//  Created by a k azad on 14/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

enum PayableState : Int {
    case list
    case log
}

class PayablePaidViewController: UIViewController {
    
    @IBOutlet weak var dealerLbl: UILabel!
    @IBOutlet weak var dealerAddBtn: UIButton!
    @IBOutlet weak var dealerTextField: UITextField!
    @IBOutlet weak var currentDueLbl: UILabel!
    @IBOutlet weak var currentDueTextField: UITextField!
    @IBOutlet weak var duePaidLbl: UILabel!
    @IBOutlet weak var duePaidTextField: UITextField!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var detailsTextField: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    
    
    private var presenter = PayablePaidPresenter(service: PayableService())
    
    var vendorId : Int?
    
    var payableList : PayableList?
    var payableData : PayableLogList?
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    
    
    var payableId : String?
    var currentPayable : String?
    
    var type : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetUp()
        self.attachPresenter()
        
    }
    
    func initialSetUp(){
        self.dealerLbl.text = LanguageManager.Vendor
        self.currentDueLbl.text = LanguageManager.CurrentPayable
        self.currentDueTextField.isEnabled = true
        self.duePaidLbl.text = LanguageManager.PaidAmount
        self.detailsLbl.text = LanguageManager.Description
        self.title = LanguageManager.PayablePaid
        self.dealerTextField.underlined()
        self.currentDueTextField.underlined()
        self.duePaidTextField.underlined()
//        if payableState == PayableState.list.rawValue{
//            guard let list = payableList else{
//                return
//            }
//            self.dealerTextField.text = list.name
//            self.currentDueTextField.text = list.currentPayable
//        }else{
//            guard let data = payableData else {
//                return
//            }
//            self.payableId = data.id
//            self.vendorId = Int(data.vendorId)
//            self.dealerTextField.text = data.name
//            self.currentDueTextField.text = data.amount
//            self.duePaidTextField.text = ""
//        }
        
       
    }
    
    @IBAction func submitButton(_ sender: UIButton) {
        if isValidated(){
            self.submitBtnControlWith(isEnabled: false)
            guard let id = self.vendorId, let amount = duePaidTextField.text, let description = detailsTextField.text else {
                return
            }
            var payablePaidData : [String: Any] = ["vendor_id" : id, "type" : self.type, "amount" : amount, "description" : description]
            
            if let id = self.payableId{
                payablePaidData["id"] = id
            }
            self.presenter.postPayablePaidDataToServer(data: payablePaidData)
        }
    }
//
//    func setUpPickerView(){
//
//        pickerView.delegate = self
//
//        let toolBar = UIToolbar()
//        toolBar.barStyle = UIBarStyle.default
//        toolBar.isTranslucent = true
//        toolBar.tintColor = UIColor.black
//        toolBar.sizeToFit()
//
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
//
//        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
//
//        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
//
//        self.dealerTextField.inputView = pickerView
//        self.dealerTextField.inputAccessoryView = toolBar
//
//    }
//
//    @objc func onPressingCancel(sender : UIBarButtonItem){
//        self.view.endEditing(true)
//    }
//
//    @objc func onPressingDone(sender: UIBarButtonItem){
//        self.dealerTextField.resignFirstResponder()
//        self.duePaidTextField.becomeFirstResponder()
//    }
    
//    func toolbarSetUp(){
//
//        //PayableAmountToolBar
//        let toolBar = UIToolbar()
//        toolBar.barStyle = UIBarStyle.default
//        toolBar.isTranslucent = true
//        toolBar.tintColor = UIColor.black
//        toolBar.sizeToFit()
//
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
//
//        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDonePaidAmount(sender:)))
//
//        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
//
//        self.dealerTextField.inputView = pickerView
//        self.dealerTextField.inputAccessoryView = toolBar
//    }
//
//    @objc func onPressingDonePaidAmount(sender: UIBarButtonItem){
//        self.duePaidTextField.resignFirstResponder()
//        self.detailsTextField.becomeFirstResponder()
//    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = false
    }
    
    func isValidated()->Bool{
        if (self.dealerTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.VendorNameIsRequired)
            return false
        }
        else if (self.duePaidTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.DuePaymentIsRequired)
            return false
        }else {
            return true
        }
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in

            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }

}


//Mark: API Delegate
extension PayablePaidViewController: PayablePaidViewDelegate{
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.displayMessage(userMessage: data)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
    
}
