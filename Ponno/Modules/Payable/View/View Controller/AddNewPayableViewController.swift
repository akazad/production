//
//  AddNewPayableViewController.swift
//  Ponno
//
//  Created by a k azad on 9/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

enum ChangePayableState : Int {
    case new
    case update
    case withdraw
}

class AddNewPayableViewController: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var vendorLabel: UILabel!
    @IBOutlet weak var vendorAddBtn: UIButton!
    @IBOutlet weak var vendorListTextField: UITextField!
    @IBOutlet weak var vendorCurrentPayableLabel: UILabel!
    @IBOutlet weak var vendorCurrentPayableHeight: NSLayoutConstraint!
    @IBOutlet weak var vendorCurrentPayableTextField: UITextField!
    @IBOutlet weak var vendorCurrentPayableTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var vendorPayableAmountLabel: UILabel!
    @IBOutlet weak var vendorPayableAmountTextField: UITextField!
    @IBOutlet weak var vendorDetailsLabel: UILabel!
    @IBOutlet weak var vendorDetailsView: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    
    private var presenter = AddNewPayablePresenter(service: PayableService())
    
    var vendorId : Int?
    var vendorPicker = UIPickerView(){
        didSet{
            self.vendorPicker.reloadAllComponents()
        }
    }
    var vendorList : [VendorsList] = []
    
    var payableLog : PayableLogList?
    var payableList : PayableList?
    var isLoading : Bool = false
    var currentPage : Int = 1
    var getAll : Bool = true
    
    var payableState = PayableState.log.rawValue
    
    var payableId : Int?
    var payableAmount : String?
    var initaialPayableAmount : Double?
    
    var changePayableState = ChangePayableState.update.rawValue
    
    var type : Int = 0
    
    var logCurrentPayable : Double?
    var listCurrentPayable : Double?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpPickerView()
        self.setUpInitialLanguage()
        self.setUpVendorPayableFormLabel()
        self.initialSetup()
        self.toolbarSetUp()
        self.attachPresenter()
    }
    
    func initialSetup(){
        if changePayableState == ChangePayableState.new.rawValue{
            self.vendorCurrentPayableTextField.isHidden = true
            self.vendorCurrentPayableLabel.isHidden = true
            self.vendorCurrentPayableHeight.constant = 0.0
            self.vendorCurrentPayableTextFieldHeight.constant = 0.0
        }
        else if payableState == PayableState.log.rawValue{
            if let data = payableLog{
                self.payableId = data.id
                self.vendorId = data.vendorId
                self.vendorListTextField.text = data.name
                self.vendorDetailsView.text = data.description
                self.vendorCurrentPayableTextField.isHidden = false
                self.vendorCurrentPayableLabel.isHidden = false
                self.vendorCurrentPayableHeight.constant = 30.0
                self.vendorCurrentPayableTextFieldHeight.constant = 40.0
                self.vendorCurrentPayableTextField.text = data.currentPayable
                self.vendorPayableAmountTextField.text = data.amount
                self.vendorAddBtn.isHidden = true
                self.logCurrentPayable = Double(data.currentPayable)
                self.payableAmount = data.amount
                self.initaialPayableAmount = Double(data.amount)
            }
        }else{
            if let data = payableList {
                self.payableId = data.id
                self.vendorId = data.id
                self.vendorListTextField.text = data.name
                self.vendorCurrentPayableTextField.isHidden = false
                self.vendorCurrentPayableLabel.isHidden = false
                self.vendorCurrentPayableTextField.text = "\(data.currentPayable)"
                self.vendorPayableAmountTextField.text = ""
            }
        }
    }
    
    func setUpVendorPayableFormLabel(){
        
        self.vendorAddBtn.addTarget(self, action: #selector(onVendorAddBtnPressed), for: .touchUpInside)
        
        self.vendorLabel.text = LanguageManager.VendorName
        self.vendorListTextField.placeholder = LanguageManager.VendorName
        self.vendorCurrentPayableLabel.text = LanguageManager.CurrentPayable
        self.vendorCurrentPayableTextField.placeholder = LanguageManager.CurrentPayable
        self.vendorPayableAmountLabel.text = LanguageManager.PayableAmount
        self.vendorPayableAmountTextField.placeholder = LanguageManager.PayableAmount
        self.vendorCurrentPayableTextField.isEnabled = false
        self.vendorDetailsLabel.text = LanguageManager.Description
        if changePayableState == ChangePayableState.update.rawValue{
            if self.type == 0 {
                self.title = LanguageManager.PayableUpdate
            }else{
                self.title = LanguageManager.PayableWithdrawUpdate
            }
            self.vendorLabel.text = LanguageManager.VendorName
            self.vendorPayableAmountLabel.text = LanguageManager.PayableAmount
            self.vendorListTextField.isEnabled = false
            self.vendorCurrentPayableTextField.isEnabled = false
        }else if changePayableState == ChangePayableState.withdraw.rawValue{
            self.title = LanguageManager.PayableWithdraw
            self.vendorListTextField.isEnabled = false
            self.vendorCurrentPayableTextField.isEnabled = false
            self.vendorPayableAmountLabel.text = LanguageManager.PayableWithdrawAmount
            self.vendorAddBtn.isHidden = true
        }else if changePayableState == ChangePayableState.new.rawValue{
            if self.type == 0 {
                self.title = LanguageManager.NewPayable
            }else{
                self.title = LanguageManager.PayableWithdraw
                self.vendorPayableAmountLabel.text = LanguageManager.PayableWithdrawAmount
                self.vendorAddBtn.isHidden = true
            }
            self.vendorLabel.text = LanguageManager.VendorName
            
        }
        
    }
    
    @objc func onVendorAddBtnPressed(sender: UIButton){
        self.alertWithTextField()
        
    }
    
    @IBAction func submitButton(_ sender: UIButton) {
        if isValidated(){
            guard let id = self.vendorId, let amount = vendorPayableAmountTextField.text else {
                return
            }
            let description = vendorDetailsView.text ?? ""
            var param : [String: Any] = ["vendor_id" : id, "type" : self.type, "amount" : amount, "description" : description]
            
            if changePayableState == ChangePayableState.update.rawValue{
                param["id"] = self.payableId
                self.presenter.postUpdatePayableDataToServer(param: param)
            }else if changePayableState == ChangePayableState.withdraw.rawValue {
                self.presenter.postAddPayableDataToServer(payableData: param)
            }else if changePayableState == ChangePayableState.new.rawValue{
                self.presenter.postAddPayableDataToServer(payableData: param)
            }
        }
    }
    
    func setUpPickerView(){
        
        self.vendorListTextField.underlined()
        self.vendorPayableAmountTextField.underlined()
        self.vendorCurrentPayableTextField.underlined()
        
        vendorPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.vendorListTextField.inputView = vendorPicker
        self.vendorListTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func onPressingCancel(sender : UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        self.vendorListTextField.resignFirstResponder()
        self.vendorPayableAmountTextField.becomeFirstResponder()
    }
    
    func toolbarSetUp(){
        
        //PayableAmountToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDonePayableAmount(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.vendorPayableAmountTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingDonePayableAmount(sender: UIBarButtonItem){
        if let payableAmountText = self.vendorPayableAmountTextField.text, let payableAmount = Double(payableAmountText) {
            if changePayableState == ChangePayableState.new.rawValue{
                if type == 1 {
                    if let currentPayable =  self.logCurrentPayable{
                        if payableAmount > currentPayable{
                            self.vendorPayableAmountTextField.text = "\(currentPayable)"
                        }
                    }
                }
            }else if changePayableState == ChangePayableState.update.rawValue{
                if type == 1 {
                    if let currentPayable =  self.logCurrentPayable, let initialPayable = self.initaialPayableAmount{
                        if payableAmount > currentPayable + initialPayable {
                            self.vendorPayableAmountTextField.text = "\(currentPayable + initialPayable)"
                        }
                    }
                }
            }
        }
        
        
        
        
        self.vendorDetailsView.becomeFirstResponder()
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitButton.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
        if (self.vendorListTextField.text?.isEmpty)!{
            showAlert(title: LanguageManager.VendorNameIsRequired, message: "")
            return false
        }else if let withdrawnAmountText = self.vendorPayableAmountTextField.text, let withdrawnAmount = Double(withdrawnAmountText), withdrawnAmount <= 0 {
            if self.type == 1 {
                showAlert(title: LanguageManager.PayableWithdrawAmountIsEmpty, message: "")
                return false
            }else{
                showAlert(title: LanguageManager.PayableAmountIsRequired, message: "")
                return false
            }
            
        }else{
            if let payableAmountText = self.vendorPayableAmountTextField.text, let payableAmount = Double(payableAmountText){
                if changePayableState == ChangePayableState.update.rawValue{
                    if self.type == 1 {
                        if let currentPayable =  self.logCurrentPayable, let initialPayable = self.initaialPayableAmount{
                            if payableAmount > currentPayable + initialPayable {
                                self.vendorPayableAmountTextField.text = "\(currentPayable + initialPayable)"
                                self.showAlert(title: LanguageManager.PayableWithdrawUpdateAmountIsTooMuch, message: "")
                                return false
                            }
                        }
                    }
                }else if changePayableState == ChangePayableState.withdraw.rawValue{
                    if let currentPayable =  self.logCurrentPayable{
                        if payableAmount > currentPayable + payableAmount {
                            self.vendorPayableAmountTextField.text = "\(currentPayable + payableAmount)"
                            self.showAlert(title: LanguageManager.PayableWithdrawnAmountIsTooMuch, message: "")
                            return false
                        }
                    }
                }else if changePayableState == ChangePayableState.new.rawValue{
                    if (self.vendorPayableAmountTextField.text?.isEmpty)!{
                        if self.type == 1 {
                            showAlert(title: LanguageManager.PayableWithdrawAmountIsEmpty, message: "")
                            return false
                        }else{
                            showAlert(title: LanguageManager.PayableAmountIsRequired, message: "")
                            return false
                        }
                    }else{
                        if self.type == 1 {
                            if let currentPayable =  self.logCurrentPayable{
                                if payableAmount > currentPayable {
                                    self.vendorPayableAmountTextField.text = "\(currentPayable)"
                                    self.showAlert(title: LanguageManager.PayableWithdrawnAmountIsTooMuch, message: "")
                                    return false
                                }
                            }
                        }
                    }
                }
            }
        }
        return true
    }
    
    //Alert Message
    func addSuccessMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                //self.vendorListTextField.resignFirstResponder()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigateToPayableVC()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }

    func navigateToPayableVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PayableViewController") as! PayableViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//extension AddNewPayableViewController: UITextFieldDelegate{
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField == self.vendorListTextField{
//            self.setUpPickerView()
//        }
//        return false
//    }
//}

//PickerViewDelegate
extension AddNewPayableViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.vendorList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == vendorPicker{
            let item = self.vendorList[row]
            self.vendorListTextField.text = item.name
            self.vendorCurrentPayableTextField.isHidden = false
            self.vendorCurrentPayableLabel.isHidden = false
            self.vendorCurrentPayableHeight.constant = 30.0
            self.vendorCurrentPayableTextFieldHeight.constant = 40.0
            self.vendorCurrentPayableTextField.text = item.currentPayable
            self.vendorPayableAmountTextField.text = self.payableAmount
            self.logCurrentPayable = Double(item.currentPayable)
            return item.name
            
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let item = self.vendorList[row]
        self.vendorListTextField.text = item.name
        
        self.vendorCurrentPayableTextField.isHidden = false
        self.vendorCurrentPayableLabel.isHidden = false
        self.vendorCurrentPayableHeight.constant = 30.0
        self.vendorCurrentPayableTextFieldHeight.constant = 40.0
        self.vendorCurrentPayableTextField.text = item.currentPayable
        self.vendorPayableAmountTextField.text = self.payableAmount
        self.logCurrentPayable = Double(item.currentPayable)
        self.vendorId = item.id
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.vendorListTextField {
            
            textField.becomeFirstResponder()
        }
    }
    
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewVendorAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.VendorName
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.VendorMobileNumber
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showMessage(userMessage: LanguageManager.VendorNameIsRequired)
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                self.showMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
                return
            }
            
//            if !(self.validatePhoneNumber(value: phone)){
//                self.showMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
//                return
//            }
            
            let param : [String : String] = ["name": textField.text!, "phone": textField2.text!, "address": ""] 
            
            self.presenter.postNewVendorAddDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: API Delegate
extension AddNewPayableViewController: AddNewPayableViewDelegate{  
    func setNewVendorAddData(data: VendorAddDataMapper) {
        guard let vendor = data.vendor else{
            return
        }
        self.vendorId = vendor.id
        self.vendorListTextField.text = vendor.name
        self.vendorCurrentPayableTextField.text = "0.0"
//        guard let message = data.message else {
//            return
//        }
//        self.addSuccessMessage(userMessage: message)
    }
    
    func setVendorsListData(data: VendorsDataMapper) {
        guard let list = data.vendors, list.count > 0 else {
            return
        }
        self.isLoading = false
//        let vendorAdd = VendorsList(id: 0, name: "নতুন ডিলার/কোম্পানি যোগ", address: "", phone: "")
//        list.insert(vendorAdd, at: 0)
        self.vendorList += list
        
    }
    
    func updatePayableLogData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.presenter.getAllVendorListFromServer(getAll: self.getAll)
    }
}

extension AddNewPayableViewController{
    func showMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.alertWithTextField()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
//    func successMessage(userMessage : String) -> Void {
//        let alertController = UIAlertController(title: "Successful", message: userMessage, preferredStyle: .alert)
//
//        let OKAction = UIAlertAction(title: "OK", style: .default){
//            (action:UIAlertAction!) in
//            self.navigationController?.popViewController(animated: true)
//        }
//        alertController.addAction(OKAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
}
