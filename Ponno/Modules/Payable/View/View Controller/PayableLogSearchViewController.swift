//
//  PayableLogSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 18/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PayableLogSearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    
    private var presenter = PayableLogSearchPresenter(service: PayableService())
    
    var payableLog : [PayableLogList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    var purchaseId : Int?
    var withdrawAmount: String?
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var currentPage : Int = 1
    var searchPage : Int = 1
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    var type : Int = 1
    
    var datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetUp()
        self.attachPresenter()
    }
    
    func initialSetUp(){
        self.searchBtn.addTarget(self, action: #selector(onSubmit(sender:)), for: .touchUpInside)
        self.configureTableView()
        self.showEndDatePicker()
        self.showStartDatePicker()
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(PayableLogSearchViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.currentPage = 1
        self.payableLog = []
        self.presenter.getPayableLogDataFromServer(page: self.currentPage)
        refreshControl.endRefreshing()
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
    
    
}

extension PayableLogSearchViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            
            self.payableLog = []
            self.currentPage = 1
            //self.searchIsActive = true
            
            self.presenter.getPayableLogSearchDataFromServer(page: currentPage, startDate: startDate, endDate: endDate)
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

//MARK: TableView Delegate And DataSoruce
extension PayableLogSearchViewController : UITableViewDelegate, UITableViewDataSource{
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PayableLogCell.nib, forCellReuseIdentifier: PayableLogCell.identifier)
        //self.tableView.register(PayableListCell.nib, forCellReuseIdentifier: PayableListCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.payableLog.count > 0 {
            return self.payableLog.count
        }
        return 0

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PayableLogCell = tableView.dequeueReusableCell(withIdentifier: PayableLogCell.identifier, for: indexPath) as! PayableLogCell
        cell.selectionStyle = .none
        cell.clipsToBounds = true
        
        if self.payableLog.count > 0 {
            let payable = self.payableLog[indexPath.row]
            cell.dateLabel.text = payable.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
            //cell.dateLabel.textColor = UIColor(red: CGFloat(91/255.0), green: CGFloat(160/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1))
            cell.amountLabel.text = LanguageManager.Amount + " : " + "\(payable.amount)" + " " + Constants.currencySymbol
            cell.nameLabel.text = payable.name
            
            cell.payableListPopUpBtn.tag = payable.id
            cell.payableListPopUpBtn.addTarget(self, action: #selector(onPayableListPopUpBtnTapped), for: .touchUpInside)
            
            //cell.nameLabel.textColor = UIColor(red: CGFloat(119/255.0), green: CGFloat(170/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1.0))
            if payable.type == 0 {
                cell.typeLabel.text = LanguageManager.Payable
                //cell.typeLabel.textColor = UIColor(red: CGFloat(220/255.0), green: CGFloat(53/255.0), blue: CGFloat(69/255.0), alpha: CGFloat(1.0))
                cell.colorView.backgroundColor = UIColor(red: CGFloat(232/255.0), green: CGFloat(94/255.0), blue: CGFloat(88/255.0), alpha: CGFloat(0.8))
            }
            if payable.type == 1 {
                cell.typeLabel.text = LanguageManager.Paid
                //cell.typeLabel.textColor = UIColor(red: CGFloat(91/255.0), green: CGFloat(160/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1.0))
                cell.colorView.backgroundColor = UIColor(red: CGFloat(90/255.0), green: CGFloat(162/255.0), blue: CGFloat(99/255.0), alpha: CGFloat(0.8))
            }
            if isLoading == false && indexPath.row == self.payableLog.count - 1 {
                if isSearchActive {
                    if self.currentPage < self.lastPageNo {
                        self.isLoading = true
                        self.currentPage += 1
                        self.presenter.getPayableLogDataFromServer(page: self.currentPage)
                    }
                }else{
                    if self.searchPage < self.lastPageNo {
                        self.isLoading = true
                        self.searchPage += 1
                        
                        guard let startDate = self.datePickerTextField.text else{
                            return cell
                        }
                        guard let endDate = self.toDatePickerTextField.text else{
                            return cell
                        }
                        self.presenter.getPayableLogSearchDataFromServer(page: searchPage, startDate: startDate, endDate: endDate)
                    }
                }
            }
        }
        
        
        //            This creates the shadow and modifies the card a little bit
//        cell.backgroundColor = UIColor.white
//        cell.contentView.layer.cornerRadius = 4.0
//        cell.contentView.layer.borderWidth = 1.0
//        cell.contentView.layer.borderColor = UIColor.clear.cgColor
//        cell.contentView.layer.masksToBounds = false
//        cell.layer.shadowColor = UIColor.lightGray.cgColor
//        cell.layer.shadowOffset = CGSize(width: 2.0, height: 1.0)
//        cell.layer.shadowOpacity = 1.0
//        cell.layer.shadowRadius = 4.0
//        cell.layer.masksToBounds = false
//        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.layer.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    fileprivate func refreshTableView(){
        tableView.reloadData()
        tableView.setContentOffset(.zero, animated: false)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let edit : Bool = false
        
        if self.payableLog.count > 0 {
            let payable = self.payableLog[indexPath.row]
            self.purchaseId = payable.purhcaseId
            if self.purchaseId == nil{
                //edit = true
            }else{
                //edit = false
            }
        }
        return edit
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if self.payableLog.count > 0 {
            let payable = self.payableLog[indexPath.row]
            self.purchaseId = payable.purhcaseId
            if self.purchaseId == nil{
                //edit = true
                let edit = UITableViewRowAction(style: .normal, title: "পরিবর্তন") { action, index in
                    if self.payableLog.count > 0 {
                        let payable = self.payableLog[indexPath.row]
                        self.navigateToEditPayableVC(data: payable)
                    }
                }
                
                edit.backgroundColor = UIColor.orange
                
                let delete = UITableViewRowAction(style: .normal, title: "Delete")
                { action, index in
                    if self.payableLog.count > 0 {
                        let payable = self.payableLog[indexPath.row]
                        let payableId = payable.id
                        self.confirmationMessage(userMessage: "Are you sure?", deleteId: payableId)
                    }
                }
                delete.backgroundColor = UIColor.lightRed
                
                return [delete, edit]
            }else{
                //edit = false
                
                if self.payableLog.count > 0 {
                    let list = self.payableLog[indexPath.row]
                    let invoice = UITableViewRowAction(style: .normal, title: list.invoice)
                    { action, index in
//                        let payable = self.payableLog[indexPath.row]
//                        //let payableId = payable.id
//                        let purchaseId = payable.purhcaseId
//                        self.navigateToPurchaseDetailsVC(purchaseId : Int(purchaseId))
                    }
                    invoice.backgroundColor = UIColor(red: 0, green: 100, blue: 0)
                    
                    return [invoice]
                }
            }
        }
        
        //        let edit = UITableViewRowAction(style: .normal, title: "পরিবর্তন") { action, index in
        //            if self.payableLog.count > 0 {
        //                let payable = self.payableLog[indexPath.row]
        //                self.navigateToEditPayableVC(data : payable)
        //            }
        //        }
        //
        //        edit.backgroundColor = UIColor.orange
        //
        //        let delete = UITableViewRowAction(style: .normal, title: "বাতিল")
        //        { action, index in
        //            if self.payableLog.count > 0 {
        //                let payable = self.payableLog[indexPath.row]
        //                let payableId = payable.id
        //                self.confirmationMessage(userMessage: "আপনি কি নিশ্চিত?", deleteId: payableId)
        //            }
        //        }
        //        delete.backgroundColor = UIColor.red
        //
        //        return [delete, edit]
        return [UITableViewRowAction()]
    }
    
    @objc func onPayableListPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.payableLog.count > 0 {
            let id = sender.tag
            for item in self.payableLog{
                if id == item.id{
                    self.purchaseId = item.purhcaseId
                    self.withdrawAmount = item.withdraw
                    if self.purchaseId == nil {
                        if withdrawAmount == "0.00"{
                            let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                                
                                self.navigateToEditPayableVC(data: item)
                            }
                            
                            let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                                let payableId = item.id
                                self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: payableId)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                
                                self.view.endEditing(true)
                            }
                            
                            cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                            
                            myActionSheet.addAction(updateAction)
                            myActionSheet.addAction(deleteAction)
                            myActionSheet.addAction(cancelAction)
                        }else{
                            let withdrawAction = UIAlertAction(title: LanguageManager.Paid + " : " + "\(String(describing: withdrawAmount))", style: UIAlertAction.Style.default) { (action) in
                                
                                //self.navigateToEditPayableVC(data: item)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                
                                self.view.endEditing(true)
                            }
                            
                            cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                            
                            myActionSheet.addAction(withdrawAction)
                            myActionSheet.addAction(cancelAction)
                        }
                    }else{
                        let invoiceAction = UIAlertAction(title: LanguageManager.Invoice, style: UIAlertAction.Style.default) { (action) in
                            guard let id = self.purchaseId else{
                                return
                            }
                            self.navigateToPurchaseDetailsVC(purchaseId : id)
                        }
                        
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(invoiceAction)
                        myActionSheet.addAction(cancelAction)
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if self.payableLog.count > 0 {
//            let vendor = self.payableLog[indexPath.row]
//            let vendorsId = vendor.id
//            self.navigateToVendorDetailsVC(vendorId: vendorsId)
//            
//        }
//    }
    
    func navigateToPayableVC(data: PayableList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewPayableViewController") as! AddNewPayableViewController
        self.type = 0
        viewController.type = self.type
        viewController.payableList = data
        viewController.payableState = PayableState.list.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchaseDetailsVC(purchaseId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseDetailsViewController") as! PurchaseDetailsViewController
        viewController.selectedId = purchaseId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
//    func navigateToEditPayableVC(data: PayableLogList){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "EditPayableViewController") as! EditPayableViewController
//        //        self.type = 0
//        //        viewController.type = self.type
//        //viewController.payableList = data
//        //        viewController.payableState = PayableState.list.rawValue
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
    
    func navigateToEditPayableVC(data : PayableLogList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewPayableViewController") as! AddNewPayableViewController
        self.type = 1
        viewController.type = self.type
        viewController.payableLog = data
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToEditPayableVC(data : PayableList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewPayableViewController") as! AddNewPayableViewController
        self.type = 1
        viewController.type = self.type
        //viewController.payableData = data
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPaidVC(data: PayableList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewPayableViewController") as! AddNewPayableViewController
        self.type = 1
        viewController.type = self.type
        viewController.payableList = data
        viewController.payableState = PayableState.list.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    fileprivate func navigateToVendorDetailsVC(vendorId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorDetailsBaseViewController") as! VendorDetailsBaseViewController
        viewController.vendorId = vendorId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.currentPage = 1
                    self.payableLog = []
                    self.refreshTableView()
                    self.presenter.getPayableLogDataFromServer(page: self.currentPage)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.deletePayableLogDataFromServer(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
} 

// MARK: API Delegate
extension PayableLogSearchViewController : PayableLogSearchViewDelegate{
   
    func deletePayableLogData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func setPayableLogData(data: PayableLogDataMapper) {
        guard let list = data.payableLogList, list.count > 0 else{
            return
        }
        //self.checked = true
        self.isLoading = false
        self.payableLog += list
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.currentPage = 1
        self.presenter.getPayableLogDataFromServer(page: self.currentPage)
    }
}
