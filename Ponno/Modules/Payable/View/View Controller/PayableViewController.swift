//
//  PayableViewController.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

class PayableViewController: BaseViewController {
    
    @IBOutlet weak var segmentControl: UISegmentedControl!{
        didSet{
            segmentControl.setTitle(LanguageManager.PayableHistory, forSegmentAt: 0)
            segmentControl.setTitle(LanguageManager.Vendors, forSegmentAt: 1)
        }
    }
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var payableToggle: UIBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = PayablePresenter(service: PayableService())
    
    var payableList : [PayableList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var payableLog : [PayableLogList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isPayableList = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var purchaseId : Int?
    var invoice : String?
    var vendorsId : Int?
    var payableAmount : String?
    var withdrawAmount: String?
    
    var selectedType : String?
    
    var checked = false
    
    private var summary : [Summary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var filteredPayableList : [PayableList] = []
    var filteredPayableLog : [PayableLogList] = []
    
    var listCurrentPage : Int = 1
    var logCurrentPage : Int = 1
    var isLoading : Bool = false
    
    var type : Int = 1
    
    var toggleStatus : Bool = false
    
    var lastPageNo: Int = 0
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.setNavigationBarTitle()
        self.configureTableView()
        self.configureCollectionView()
        self.initialSetup()
        self.setBarButton()
        self.addFloaty()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setBarButton(){
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let payablePaidBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        payablePaidBtn.setImage(UIImage(named: "payPayable"), for: UIControl.State.normal)
        payablePaidBtn.addTarget(self, action: #selector(onPayablePaidTapped(sender:)), for: UIControl.Event.touchUpInside)
        payablePaidBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        payablePaidBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        payablePaidBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton3 = UIBarButtonItem(customView: payablePaidBtn)
        
        let payableAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        payableAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        payableAddBtn.addTarget(self, action: #selector(onNewPayableTapped(sender:)), for: UIControl.Event.touchUpInside)
        payableAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        payableAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        payableAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: payableAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2, barButton3, barButton1], animated: true)
    }
    
    @objc func onSearch(sender: UIButton){
        //self.navigateToVendorsSearchVC()
        if toggleStatus == true {
            self.navigateToPayableListSearchVC()
        }else{
            self.navigateToPayableLogSearchVC()
        }
    }
    
    @objc func onNewPayableTapped(sender: UIButton){
        self.navigateToNewPayableDueVC()
    }
    
    @objc func onPayablePaidTapped(sender: UIButton){
        self.navigateToPayablePaidVC()
    }
    
    func initialSetup(){
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.segmentControl.addTarget(self, action: #selector(onSegmentChange), for: .valueChanged)
    }
    
    @objc func onSegmentChange(sender: UISegmentedControl){
        isPayableList = !isPayableList
        if segmentControl.selectedSegmentIndex == 0 {
            checked = true
            self.toggleStatus = false
            self.isLoading = true
            self.logCurrentPage = 1
            self.payableLog = []
            self.presenter.getPayableLogDataFromServer(page: self.logCurrentPage)
        }else{
            checked = false
            self.toggleStatus = true
            self.isLoading = true
            self.listCurrentPage = 1
            self.payableList = []
            self.presenter.getPayableListDataFromServer(page: self.listCurrentPage)
        }
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.PayableBook
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0))]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
    }
    
   
    
    func navigateToPayableListSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PayableListSearchViewController") as! PayableListSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToPayableLogSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PayableLogSearchViewController") as! PayableLogSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToEditPayableVC(data : PayableLogList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewPayableViewController") as! AddNewPayableViewController
        //self.type = 1
        viewController.type = self.type
        viewController.payableLog = data
        viewController.payableAmount = self.payableAmount
        viewController.payableState = PayableState.log.rawValue
        viewController.changePayableState = ChangePayableState.update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchaseDetailsVC(purchaseId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseDetailsViewController") as! PurchaseDetailsViewController
        viewController.selectedId = purchaseId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToNewPayableDueVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewPayableViewController") as! AddNewPayableViewController
        self.type = 0
        viewController.type = self.type
        viewController.changePayableState = ChangePayableState.new.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPayablePaidVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewPayableViewController") as! AddNewPayableViewController
        self.type = 1
        viewController.type = self.type
        viewController.changePayableState = ChangePayableState.new.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(PayableViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        if (isPayableList){
            self.listCurrentPage = 1
            self.payableList = []
            self.presenter.getPayableListDataFromServer(page: self.listCurrentPage)
        }else{
            self.logCurrentPage = 1
            self.payableLog = []
            self.presenter.getPayableLogDataFromServer(page: self.logCurrentPage)
        }
        refreshControl.endRefreshing()
    }
    
}

//Mark: Floaty Delegate
extension PayableViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}


//MARK: CollectionView Delegate And DataSource
extension PayableViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(PayableSummaryCell.nib, forCellWithReuseIdentifier: PayableSummaryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let summaryItem = self.summary, summaryItem.count > 0 else{
            return 0
        }
        return summaryItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PayableSummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: PayableSummaryCell.identifier, for: indexPath) as! PayableSummaryCell
        guard let summaryList = self.summary, summaryList.count > 0 else{
            return cell
        }
        let summaryItem = summaryList[indexPath.row]
        
//        for (key, _) in summaryList.enumerated() {
//            if key == 0 {
//                summaryList[0].title = "Total Vendor Number"
//            }else if key == 1 {
//                summaryList[1].title = "Current Payable"
//            }else if key == 2 {
//                summaryList[2].title = "Total Payable Withdraw"
//            }
//        }
        
        cell.titleLabel.text = summaryItem.title
        cell.valueLabel.text = summaryItem.value
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 60.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.summary, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//MARK: TableView Delegate And DataSoruce
extension PayableViewController : UITableViewDelegate, UITableViewDataSource{
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PayableLogCell.nib, forCellReuseIdentifier: PayableLogCell.identifier)
        self.tableView.register(PayableListCell.nib, forCellReuseIdentifier: PayableListCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isPayableList{
            if self.payableLog.count > 0 {
                return self.payableLog.count
            }
            return 0
        }else{
            if self.payableList.count > 0 {
                return self.payableList.count
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if checked{
            let cell : PayableLogCell = tableView.dequeueReusableCell(withIdentifier: PayableLogCell.identifier, for: indexPath) as! PayableLogCell
            cell.selectionStyle = .none

            if self.payableLog.count > 0 {
                let payable = self.payableLog[indexPath.row]
                cell.dateLabel.text = payable.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.amountLabel.text = LanguageManager.Amount + " : " + payable.amount + " " + Constants.currencySymbol
                cell.nameLabel.text = LanguageManager.Vendor + " : " + payable.name
                
                let description = payable.description
                if description == "" {
                    cell.descriptionImage.isHidden = true
                }else{
                    cell.descriptionImage.isHidden = false
                }
                
                cell.descriptionImage.addTapGestureRecognizer {
                    self.showAlert(title: LanguageManager.Description, message: description)
                }
                
                cell.payableListPopUpBtn.tag = payable.id
                cell.payableListPopUpBtn.addTarget(self, action: #selector(onPayableLogPopUpBtnTapped), for: .touchUpInside)
                
                if payable.type == 0 {
                    cell.typeLabel.text = LanguageManager.Payable
                    cell.colorView.backgroundColor = UIColor(red: CGFloat(232/255.0), green: CGFloat(94/255.0), blue: CGFloat(88/255.0), alpha: CGFloat(0.8))
                }
                if payable.type == 1 {
                    cell.typeLabel.text = LanguageManager.Withdraw
                    cell.colorView.backgroundColor = UIColor(red: CGFloat(90/255.0), green: CGFloat(162/255.0), blue: CGFloat(99/255.0), alpha: CGFloat(0.8))
                }
                if isLoading == false && indexPath.row == self.payableLog.count - 1 && self.logCurrentPage < self.lastPageNo{
                    self.isLoading = true
                    self.logCurrentPage += 1
                    self.presenter.getPayableLogDataFromServer(page: self.logCurrentPage)
                }
            }
            return cell
        }else{
            let cell : PayableListCell = tableView.dequeueReusableCell(withIdentifier: PayableListCell.identifier, for: indexPath) as! PayableListCell
            cell.selectionStyle = .none
            cell.clipsToBounds = true
            
            if self.payableList.count > 0 {
                let vendor = self.payableList[indexPath.row]
                
                cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.vendorImage + vendor.image),  placeholderImage: UIImage(named: "placeholder"))
                cell.nameLabel.text = vendor.name
                cell.currentPayableLabel.text = LanguageManager.CurrentPayable + " : " + "\(vendor.currentPayable)" + " " + Constants.currencySymbol
                cell.phoneLabel.text = vendor.phone
                cell.addressLabel.text = vendor.address
                
                cell.vendorPayablePopUpBtn.tag = vendor.id
                cell.vendorPayablePopUpBtn.addTarget(self, action: #selector(onVendorPayablePopUpBtnTapped), for: .touchUpInside)
                
                if isLoading == false && indexPath.row == self.payableList.count - 1 && self.listCurrentPage < self.lastPageNo {
                    self.isLoading = true
                    self.listCurrentPage += 1
                    self.presenter.getPayableListDataFromServer(page: self.listCurrentPage)
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    fileprivate func refreshTableView(){
        tableView.reloadData()
        tableView.setContentOffset(.zero, animated: false)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        let edit : Bool = false
        if (checked){
            if !self.isPayableList{
                if self.payableLog.count > 0 {
                    let payable = self.payableLog[indexPath.row]
                    self.purchaseId = payable.purhcaseId
                    if self.purchaseId == nil{
                        //edit = true
                    }else{
                        //edit = false
                    }
                }
            }
        }
        return edit
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if (checked){
            
            if !self.isPayableList{
                if self.payableLog.count > 0 {
                    let payable = self.payableLog[indexPath.row]
                    self.purchaseId = payable.purhcaseId
                    if self.purchaseId == nil {
                        //edit = true
                        let edit = UITableViewRowAction(style: .normal, title: "Update") { action, index in
                            if !self.isPayableList{
                                if self.payableLog.count > 0 {
                                    let payable = self.payableLog[indexPath.row]
                                    self.navigateToEditPayableVC(data: payable)
                                }
                            }
                        }
                        
                        edit.backgroundColor = UIColor.orange
                        
                        let delete = UITableViewRowAction(style: .normal, title: "Delete")
                        { action, index in
                            if !self.isPayableList{
                                if self.payableLog.count > 0 {
                                    let payable = self.payableLog[indexPath.row]
                                    let payableId = payable.id
                                    self.confirmationMessage(userMessage: "Are you sure?", deleteId: payableId)
                                }
                            }
                        }
                        delete.backgroundColor = UIColor.lightRed
                        
                        return [delete, edit]
                    }else{
                        //edit = false
                        
                        if !self.isPayableList{
                            if self.payableLog.count > 0 {
                                let list = self.payableLog[indexPath.row]
                                let invoice = UITableViewRowAction(style: .normal, title: list.invoice)
                                { action, index in
                                    //let payable = self.payableLog[indexPath.row]
                                    //let payableId = payable.id
                                    //let purchaseId = payable.purhcaseId
                                    //self.navigateToPurchaseDetailsVC(purchaseId : purchaseId)
                                }
                                invoice.backgroundColor = UIColor(red: 0, green: 100, blue: 0)
                                
                                return [invoice]
                            }
                        }
                    }
                }
            }
        }else{
            let paid = UITableViewRowAction(style: .normal, title: "Payable Withdraw") { action, index in
                
                if self.isPayableList{
                    if self.payableList.count > 0 {
                        let list = self.payableList[indexPath.row]
                        self.navigateToPaidVC(data: list)
                    }
                }
            }
            
            paid.backgroundColor = UIColor.orange
            
            let update = UITableViewRowAction(style: .normal, title: "Update")
            { action, index in
                if self.isPayableList{
                    if self.payableList.count > 0 {
                        let list = self.payableList[indexPath.row]
                        self.navigateToEditPayableVC(data: list)
                    }
                }
            }
            update.backgroundColor = UIColor.green
            
            return [update, paid]
        }
        return [UITableViewRowAction()]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isPayableList{
            if self.payableList.count > 0 {
                let payable = self.payableList[indexPath.row]
                let vendorsId = payable.id
                self.navigateToVendorDetailsVC(vendorId: vendorsId)
            }
        }
    }
    
    @objc func onPayableLogPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if (checked){
            if !self.isPayableList{
                if self.payableLog.count > 0 {
                    let id = sender.tag
                    for item in self.payableLog{
                        if id == item.id{
                            self.payableAmount = item.amount
                            self.purchaseId = item.purhcaseId
                            self.withdrawAmount = item.withdraw
                            if self.purchaseId != nil {
                                let invoiceAction = UIAlertAction(title: LanguageManager.Invoice, style: UIAlertAction.Style.default) { (action) in
                                    guard let id = self.purchaseId else{
                                        return
                                    }
                                    self.navigateToPurchaseDetailsVC(purchaseId : id)
                                }
                                
                                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                    
                                    self.view.endEditing(true)
                                }
                                
                                cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                                
                                myActionSheet.addAction(invoiceAction)
                                myActionSheet.addAction(cancelAction)
                            }else{
                                if withdrawAmount == "0.00"{
                                    let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                                        self.type = item.type
                                        self.navigateToEditPayableVC(data: item)
                                    }
                                    
                                    let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                                        let payableId = item.id
                                        self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: payableId)
                                    }
                                    
                                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                        
                                        self.view.endEditing(true)
                                    }
                                    
                                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                                    
                                    myActionSheet.addAction(editAction)
                                    myActionSheet.addAction(deleteAction)
                                    myActionSheet.addAction(cancelAction)
                                }else{
                                    let withdrawAction = UIAlertAction(title: LanguageManager.Payable + " : " + "\(self.withdrawAmount ?? "0.00")", style: UIAlertAction.Style.default) { (action) in
                                        
                                    }
                                    
                                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                        
                                        self.view.endEditing(true)
                                    }
                                    
                                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                                    
                                    myActionSheet.addAction(withdrawAction)
                                    myActionSheet.addAction(cancelAction)
                                }
                            }
                        }
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    @objc func onVendorPayablePopUpBtnTapped(sender : UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if (!checked){
            if self.isPayableList{
                if self.payableList.count > 0 {
                    self.vendorsId = sender.tag
                    for item in self.payableList{
                        if self.vendorsId == item.id{
                            let duePaidAction = UIAlertAction(title: LanguageManager.PayableWithdraw, style: UIAlertAction.Style.default) { (action) in
                                self.navigateToPaidVC(data: item)
                            }
                            
                            let udpateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                                self.navigateToEditPayableVC(data: item)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                
                                self.view.endEditing(true)
                            }
                            
                            cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                            
                            myActionSheet.addAction(duePaidAction)
                            myActionSheet.addAction(udpateAction)
                            myActionSheet.addAction(cancelAction)
                        }
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToPayableVC(data: PayableList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewPayableViewController") as! AddNewPayableViewController
        self.type = 0
        viewController.type = self.type
        viewController.payableList = data
        viewController.payableState = PayableState.list.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToEditPayableVC(data: PayableList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewVendorViewController") as! AddNewVendorViewController
        viewController.payableVendor = data
        viewController.vendorState = VendorState.PayableUpdate.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPaidVC(data: PayableList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewPayableViewController") as! AddNewPayableViewController
        self.type = 1
        viewController.type = self.type
        viewController.payableList = data
        viewController.payableState = PayableState.list.rawValue
        viewController.changePayableState = ChangePayableState.withdraw.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    fileprivate func navigateToVendorDetailsVC(vendorId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorDBaseViewController") as! VendorDBaseViewController
        viewController.vendorId = vendorId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.payableLog = []
                self.refreshTableView()
                self.getPayablePerCategoryData()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.deletePayableLogDataFromServer(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
}



// MARK: API Delegate
extension PayableViewController : PayableViewDelegate{
    func deletePayableLogData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func setPayableLogData(data: PayableLogDataMapper) {
        
        guard let summary = data.summary, summary.count > 0 else {
            return
        }
        self.summary = summary
        self.setUpAutoScroll()
        
        guard let list = data.payableLogList, list.count > 0 else{
            return
        }
        self.checked = true
        self.isLoading = false
        
        self.payableLog += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
        
    }
    
    func setPayableListData(data: PayableListDataMapper) {
        guard let summary = data.summary, summary.count > 0 else {
            return
        }
        self.summary = summary
        self.setUpAutoScroll()
        
        guard let list = data.payableList else{
            return
        }
        self.isLoading = false
        self.payableList += list
        
        
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed() {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.getPayablePerCategoryData()
//        if !checked{
//            self.payableLog = []
//            self.refreshTableView()
//            self.presenter.getPayableLogDataFromServer(page: self.logCurrentPage)
//        }
//        else{
//            self.payableList = []
//            self.refreshTableView()
//            self.presenter.getPayableListDataFromServer(page: self.listCurrentPage)
//        }
    }
    
    func getPayablePerCategoryData(){
        //self.isLoading = true
        self.logCurrentPage = 1
        //self.refreshTableView()
        self.presenter.getPayableLogDataFromServer(page: self.logCurrentPage)
    }
}

