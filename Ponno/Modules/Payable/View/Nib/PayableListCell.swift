//
//  ProductListCell.swift
//  Ponno
//
//  Created by a k azad on 29/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit


class PayableListCell: UITableViewCell {

    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var firstAlphabetView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var currentPayableLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var vendorPayablePopUpBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //setUpViews()
//        setUpCardView()
//        setupCard()
//        self.cardSetUp()
//       self.backgroundCardView.layoutSubviews()
        setUpCardView(uiview: backgroundCardView)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier: String{
        return String(describing: PayableListCell.self)
    }
    
//    func setUpCardView(){
//        contentView.backgroundColor = UIColor.white
//        backgroundCardView.layer.cornerRadius = 5.0
//        backgroundCardView.layer.masksToBounds = false
//        contentView.clipsToBounds = true
//        backgroundCardView.backgroundColor = UIColor.white
//        backgroundCardView.layer.borderWidth = 2.0
//        backgroundCardView.layer.borderColor = UIColor.white.cgColor
////        backgroundCardView.layer.shadowColor = (UIColor(red: 236/255, green: 238/255, blue: 232/255, alpha: 0.5).withAlphaComponent(1.0)).cgColor
////        backgroundCardView.layer.shadowColor = CGGradient.init(colorSpace: <#T##CGColorSpace#>, colorComponents: <#T##UnsafePointer<CGFloat>#>, locations: <#T##UnsafePointer<CGFloat>?#>, count: <#T##Int#>) as! CGColor
//        backgroundCardView.layer.shadowRadius = 0.5
//        backgroundCardView.layer.shadowOpacity = 0.8
//        backgroundCardView.layer.shadowOffset = CGSize(width: -3, height: 3)
//        backgroundCardView.layer.shadowPath = UIBezierPath(rect: contentView.bounds).cgPath
//
//        let gradient = CAGradientLayer()
//        gradient.colors = [UIColor.red.cgColor, UIColor.green.cgColor]
////        gradient.startPoint = CGPoint(x: 5, y: 5)
////        gradient.endPoint = CGPoint(x: 0, y: 0)
////        gradient.locations = [0,1]
////        gradient.frame = backgroundCardView.bounds
//
////        gradient.cornerRadius = 8
////        gradient.colors = [UIColor.red.cgColor]
////
////        backgroundCardView.layer.addSublayer(gradient)
//    }
//    func setupCard() {
//
//        backgroundCardView.layer.cornerRadius = 8
//        backgroundCardView.layer.shadowColor = UIColor.black.cgColor
//        backgroundCardView.layer.shadowOffset = CGSize(width: 0, height: 2)
//        backgroundCardView.layer.shadowRadius = 5
//        backgroundCardView.layer.shadowOpacity = 0.5
//        backgroundCardView.backgroundColor = UIColor.clear
//
//        let gradient = CAGradientLayer()
//        gradient.colors = [UIColor.red.cgColor, UIColor.green.cgColor]
//        gradient.startPoint = CGPoint(x: 0, y: 0)
//        gradient.endPoint = CGPoint(x: 1, y: 1)
//        gradient.locations = [0,1]
////        gradient.frame = backgroundCardView.bounds
//        gradient.cornerRadius = 8
////        gradient.colors = [UIColor(named: "redLight").cgColor,
////                           UIColor(named: "redDark").cgColor]
//        gradient.colors = [UIColor.red.cgColor]
//
//        contentView.layer.addSublayer(gradient)
//    }
//
//    func cardSetUp(){
//        contentView.layer.cornerRadius = 4.0
//        contentView.layer.borderWidth = 1.0
//        contentView.layer.borderColor = UIColor.clear.cgColor
//        contentView.layer.masksToBounds = false
//        layer.shadowColor = UIColor.gray.cgColor
//        layer.shadowOffset = CGSize(width: 0, height: 1.0)
//        layer.shadowOpacity = 1.0
//        layer.shadowRadius = 4.0
//        layer.masksToBounds = false
//        layer.shadowPath = UIBezierPath(roundedRect: layer.bounds, cornerRadius: contentView.layer.cornerRadius).cgPath
//    }
}
