//
//  PayableLogCell.swift
//  Ponno
//
//  Created by a k azad on 2/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PayableLogCell: UITableViewCell {
    
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var payableListPopUpBtn: UIButton!
    @IBOutlet weak var descriptionImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.setUpCardView()
        setUpCardView(uiview: backgroundCardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier: String{
        return String(describing: PayableLogCell.self)
    }
//    func setUpCardView(){
//        contentView.backgroundColor = UIColor.white
//        backgroundCardView.layer.cornerRadius = 2.0
//        backgroundCardView.layer.masksToBounds = false
//        backgroundCardView.backgroundColor = UIColor.white
//        backgroundCardView.layer.borderWidth = 2.0
//        backgroundCardView.layer.borderColor = UIColor.white.cgColor
//        backgroundCardView.layer.shadowColor = (UIColor(red: 236/255, green: 238/255, blue: 232/255, alpha: 0.5).withAlphaComponent(0.8)).cgColor
//        backgroundCardView.layer.shadowRadius = 0.5
//        backgroundCardView.layer.shadowOpacity = 0.8
//        backgroundCardView.layer.shadowOffset = CGSize(width:0, height: 5)
//        backgroundCardView.layer.shadowPath = UIBezierPath(rect: contentView.bounds).cgPath
//    }
}

