//
//  PayablePresenter.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//


import UIKit

protocol PayableViewDelegate : NSObjectProtocol {
    func setPayableListData(data : PayableListDataMapper)  
    func setPayableLogData(data : PayableLogDataMapper)
    func deletePayableLogData(data: AddDataMapper)
    func onFailed()
    func showLoading()
    func hideLoading()
}
class PayablePresenter: NSObject {
    
    private let service : PayableService
    weak private var viewDelegate : PayableViewDelegate?
    
    init(service : PayableService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PayableViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getPayableListDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getPayableData(page: page, success: { (data) in
            self.viewDelegate?.setPayableListData(data: data)
            self.viewDelegate?.hideLoading()
        }, failure: {(mesaage) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
    
    func getPayableLogDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getPayableLogData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPayableLogData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
    
    func deletePayableLogDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.deletePayableData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.deletePayableLogData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
}
