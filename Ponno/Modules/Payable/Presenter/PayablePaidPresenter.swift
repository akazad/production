//
//  PayablePaidPresenter.swift
//  Ponno
//
//  Created by a k azad on 26/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PayablePaidViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class PayablePaidPresenter: NSObject {
    
    private let service : PayableService
    weak private var viewDelegate : PayablePaidViewDelegate?
    
    init(service : PayableService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PayablePaidViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postPayablePaidDataToServer(data : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postPayablePaidData(payablePaidData: data, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message) 
        })
    }
}

