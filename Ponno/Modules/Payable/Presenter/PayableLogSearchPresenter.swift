//
//  PayableLogSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 18/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation

protocol PayableLogSearchViewDelegate : NSObjectProtocol {
    func setPayableLogData(data: PayableLogDataMapper)
    func deletePayableLogData(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class PayableLogSearchPresenter: NSObject {
    
    private let service : PayableService
    weak private var viewDelegate : PayableLogSearchViewDelegate?
    
    init(service : PayableService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PayableLogSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getPayableLogDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getPayableLogData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPayableLogData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getPayableLogSearchDataFromServer(page: Int, startDate: String, endDate: String){
        self.viewDelegate?.showLoading()
        self.service.getPayableLogSearchData(page: page, startDate: startDate, endDate: endDate, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPayableLogData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func deletePayableLogDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.deletePayableData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.deletePayableLogData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
