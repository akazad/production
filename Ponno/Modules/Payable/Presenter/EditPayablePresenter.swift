//
//  EditPayablePresenter.swift
//  Ponno
//
//  Created by a k azad on 24/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol EditPayableViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class EditPayablePresenter: NSObject {
    
    private let service : PayableService
    weak private var viewDelegate : EditPayableViewDelegate?
    
    init(service : PayableService) {
        self.service = service
    }
    
    func attachView(viewDelegate : EditPayableViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postPayableEditDataToServer(param: [String: Any]){
        self.viewDelegate?.hideLoading()
        self.service.postEditPayableData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
