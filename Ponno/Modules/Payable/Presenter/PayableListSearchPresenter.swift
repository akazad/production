//
//  PayableListSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 18/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol PayableListSearchViewDelegate : NSObjectProtocol {
    func setPayableListSearchData(data : PayableListDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class PayableListSearchPresenter: NSObject {
    
    private let service : PayableService
    weak private var viewDelegate : PayableListSearchViewDelegate?
    
    init(service : PayableService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PayableListSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getPayableListSearchDataFromServer(page: Int, searchText: String){
        self.viewDelegate?.showLoading()
        self.service.getPayableListSearchData(page: page, searchText: searchText, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPayableListSearchData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
