//
//  AddNewPayablePresenter.swift
//  Ponno
//
//  Created by a k azad on 11/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol AddNewPayableViewDelegate : NSObjectProtocol {
    func setVendorsListData(data: VendorsDataMapper)
    func setNewVendorAddData(data: VendorAddDataMapper)
    func updatePayableLogData(data: AddDataMapper)
    func onSuccess(data : AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class AddNewPayablePresenter: NSObject {
    
    private let service : PayableService
    weak private var viewDelegate : AddNewPayableViewDelegate?
    
    init(service : PayableService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AddNewPayableViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getAllVendorListFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.getAllVendorsData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsListData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postAddPayableDataToServer(payableData: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postAddNewPayableDueData(payableDueData: payableData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewVendorAddDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.postAddNewVendorData(vendorData: param, success: { (data) in 
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setNewVendorAddData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postUpdatePayableDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postUpdatePayableData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updatePayableLogData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}

