//
//  EmployeeService.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper

public class EmployeeService : NSObject{
    
    func getEmployeeData(page: Int, success: @escaping (EmployeeDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Employee + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<EmployeeDataMapper>) in
                if let employeeResponse = response.result.value {
                    if employeeResponse.success == true{
                        success(employeeResponse)
                    }else if let failureMessage = employeeResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getEmployeeSearchData(page: Int, searchString: String, success: @escaping (EmployeeDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let employeeURL = RestURL.sharedInstance.Employee + RestURL.sharedInstance.getSearchText(text: searchString) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = employeeURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<EmployeeDataMapper>) in
                if let employeeResponse = response.result.value{
                    if let employeeRes = employeeResponse.employee, employeeRes.count > 0 {
                        success(employeeResponse)
                    }
                    else if let failureResponse = employeeResponse.message {
                        failure(failureResponse)
                    }
                }else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getAllEmployeeData(getAll: Bool, success: @escaping (EmployeeDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Employee + RestURL.sharedInstance.getAllData(text: getAll)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<EmployeeDataMapper>) in
                if let employeeResponse = response.result.value {
                    if employeeResponse.success == true{
                        if let employee = employeeResponse.employee, employee.count > 0 {
                            success(employeeResponse)
                        }else if let failureMessage = employeeResponse.message{
                            failure(failureMessage)
                        }
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getEmployeeDetailsData(id: Int, success: @escaping (EmployeeDetailsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.EmployeeDetails + RestURL.sharedInstance.getEmployeeDetailsUrl(id: id)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<EmployeeDetailsDataMapper>) in
                if let employeeDetailsResponse = response.result.value{
                    if employeeDetailsResponse.success == true{
                        success(employeeDetailsResponse)
                    }
                    else if let failureMessage = employeeDetailsResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getEmployeeSaleHistoyrData(page: Int, id: Int, success: @escaping (EmployeeSaleHistoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.EmployeeSaleHistory + RestURL.sharedInstance.getEmployeeDetailsUrl(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page) 
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<EmployeeSaleHistoryDataMapper>) in
                if let employeeSaleResponse = response.result.value{
                    if let sales = employeeSaleResponse.sales, sales.count > 0 {
                        if employeeSaleResponse.success == true {
                            success(employeeSaleResponse)
                        }
                        else if let failureMessage = employeeSaleResponse.message{
                            failure(failureMessage)
                        }
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getEmployeeSalaryHistoyrData(page: Int, id: Int, success: @escaping (EmployeeSalaryHistoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.EmployeeSalaryHistory + RestURL.sharedInstance.getEmployeeDetailsUrl(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<EmployeeSalaryHistoryDataMapper>) in
                if let employeeSalaryResponse = response.result.value{
                    if let salary = employeeSalaryResponse.salaries, salary.count > 0 {
                        if employeeSalaryResponse.success == true {
                            success(employeeSalaryResponse)
                        }
                        else if let failureMessage = employeeSalaryResponse.message{
                            failure(failureMessage)
                        }
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getEmployeePermissionsData(success: @escaping (EmployeePermissionDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.EmployeePermissions
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<EmployeePermissionDataMapper>) in
                if let employeePermissionsResponse = response.result.value{
                    if let permission = employeePermissionsResponse.permissions, permission.count > 0 {
                        if employeePermissionsResponse.success == true {
                            success(employeePermissionsResponse)
                        }
                        else if let failureMessage = employeePermissionsResponse.message{
                            failure(failureMessage)
                        }
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getPermissionsOfEmployeesData(id: Int, success: @escaping (PermissionsOfEmployeeDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PermissionsOfEmployee + RestURL.sharedInstance.getCommonId(id: id)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PermissionsOfEmployeeDataMapper>) in
                if let employeePermissionsResponse = response.result.value{
                    if let permission = employeePermissionsResponse.permissions, permission.count > 0 {
                        if employeePermissionsResponse.success == true {
                            success(employeePermissionsResponse)
                        }
                        else if let failureMessage = employeePermissionsResponse.message{
                            failure(failureMessage)
                        }
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getUnpaidSalariesOfEmployee(id: Int, success: @escaping (UnpaidSalaryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.EmployeeUnpaidSalary + RestURL.sharedInstance.getCommonId(id: id)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<UnpaidSalaryDataMapper>) in
                if let salaryResponse = response.result.value{
                    if let salaries = salaryResponse.unpaidSalaries, salaries.count > 0 {
                        if salaryResponse.success == true {
                            success(salaryResponse)
                        }
                        else if let failureMessage = salaryResponse.message{
                            failure(failureMessage)
                        }
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //Mark: PostRequest
    func postNewEmployeeData(param: [String: Any] , success: @escaping (EmployeeAddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.EmployeeAdd
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }


        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<EmployeeAddDataMapper>) in

            if let employeeAddResponse = response.result.value{
                if employeeAddResponse.success == true{
                    success(employeeAddResponse) 
                }else if let failureMessage = employeeAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postUpdateEmployeeData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.EmployeeUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            
            if let updateEmployeeResponse = response.result.value{
                if updateEmployeeResponse.success == true{
                    success(updateEmployeeResponse)
                }else if let failureMessage = updateEmployeeResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postEmployeeStatusUpdateData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.EmployeeStatusUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            
            if let employeeStatusResponse = response.result.value{
                if employeeStatusResponse.success == true{
                    success(employeeStatusResponse)
                }else if let failureMessage = employeeStatusResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postEmployeeSalaryAddData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.EmployeeSalaryStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            
            if let salaryResponse = response.result.value{
                if salaryResponse.success == true{
                    success(salaryResponse)
                }else if let failureMessage = salaryResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postEmployeePaymentAddData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.EmployeePaymentStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            
            if let requestResponse = response.result.value{
                if requestResponse.success == true{
                    success(requestResponse)
                }else if let failureMessage = requestResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postEmployeeSalaryUpdateData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.EmployeeSalaryUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            
            if let updateResponse = response.result.value{
                if updateResponse.success == true{
                    success(updateResponse)
                }else if let failureMessage = updateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postEmployeeSalaryDeleteData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.EmployeeSalaryDelete
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            
            if let deleteResponse = response.result.value{
                if deleteResponse.success == true{
                    success(deleteResponse)
                }else if let failureMessage = deleteResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    //Mark: Image Upload
    func postEmployeeImage(employeeID : Int,image : UIImage? ,  succeed: @escaping((AddDataMapper)->Void), failure : @escaping((String) -> Void)){
        
        let imageUploadUrl = RestURL.sharedInstance.EmployeeImageUpload
        let params = ["employee_id" : "\(employeeID)"]
        
        let service = PostImageService()
        
        service.postImage(params: params, image: image, imageUrl: imageUploadUrl, succeed: { response in
            if let success = response.success, success == true{
                print(response.message ?? "")
                succeed(response)
            }
            else{
                failure(response.message ?? "")
            }
        }, failure: {message in
            failure(message)
        })
    }
    
}

