//
//  EmployeeAddPermissions.swift
//  Ponno
//
//  Created by a k azad on 19/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class EmployeeAddPermissions : Mappable {
    var id : Int = 0
    var name : String = ""
    var guardName : String = ""
    var displayName : String = ""
    var createdAt : String = ""
    var updatedAt : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        guardName <- map["guard_name"]
        displayName <- map["display_name"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
    }
    
}

