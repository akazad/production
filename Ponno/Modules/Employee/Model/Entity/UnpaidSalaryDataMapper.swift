//
//  UnpaidSalaryDataMapper.swift
//  Ponno
//
//  Created by a k azad on 31/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class UnpaidSalaryDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var unpaidSalaries : [String]?
    var totalUnpaid : Double?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        unpaidSalaries <- map["unpaid_salaries"]
        totalUnpaid <- map["total_unpaid"]
    }

}

