//
//  SalaryHistory.swift
//  Ponno
//
//  Created by a k azad on 16/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class SalaryHistory : Mappable {
    var salaries : [EmployeeSalaries]?
    var currentPage : Int = 0
    var perPage : Int = 0
    var total : Int = 0
    var lastPageNo : Int = 0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        salaries <- map["salaries"]
        currentPage <- map["current_page"]
        perPage <- map["per_page"]
        total <- map["total"]
        lastPageNo <- map["last_page_no"]
    }
    
}

