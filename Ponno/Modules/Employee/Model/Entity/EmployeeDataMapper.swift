//
//  EmployeeDataMapper.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class EmployeeDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [EmployeeSummary]?
    var employee : [EmployeeList]?
    var pagination: Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        employee <- map["employee_list"]
        pagination <- map["pagination"]
    }
    
}
