//
//  EmployeeAdd.swift
//  Ponno
//
//  Created by a k azad on 19/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class EmployeeAdd : Mappable {
    var name : String = ""
    var phone : String = ""
    var pharmacyId : Int = 0
    var status : Int = 0
    var parent : Int = 0
    var language : String = ""
    var updatedAt : String = ""
    var createdAt : String = ""
    var id : Int = 0
    var permissions : [EmployeeAddPermissions]?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
        phone <- map["phone"]
        pharmacyId <- map["pharmacy_id"]
        status <- map["status"]
        parent <- map["parent"]
        language <- map["language"]
        updatedAt <- map["updated_at"]
        createdAt <- map["created_at"]
        id <- map["id"]
        permissions <- map["permissions"]
    }
    
}

