//
//  EmployeeAddDataMapper.swift
//  Ponno
//
//  Created by a k azad on 19/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class EmployeeAddDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var employee : EmployeeAdd? 
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        employee <- map["employee"]
    }
    
}

