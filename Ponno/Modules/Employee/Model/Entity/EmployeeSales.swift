//
//  EmployeeSales.swift
//  Ponno
//
//  Created by a k azad on 16/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class EmployeeSales : Mappable {
    var id : Int = 0
    var createdAt : String = ""
    var invoice : String = ""
    var total : String = ""
    var payable : String = ""
    var due : String = ""
    var customerName : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        createdAt <- map["created_at"]
        invoice <- map["invoice"]
        total <- map["total"]
        payable <- map["payable"]
        due <- map["due"]
        customerName <- map["customer_name"]
    }
    
}

