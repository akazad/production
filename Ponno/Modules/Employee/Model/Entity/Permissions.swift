//
//  Permissions.swift
//  Ponno
//
//  Created by a k azad on 17/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class Permissions : Mappable {
    var id : Int = 0
    var name : String = ""
//    var guardName : String?
//    var displayName : String?
//    var createdAt : String?
//    var updatedAt : String?
    //var pivot : Pivot?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
//        guard_name <- map["guard_name"]
//        display_name <- map["display_name"]
//        created_at <- map["created_at"]
//        updated_at <- map["updated_at"]
//        pivot <- map["pivot"]
    }
    
}

