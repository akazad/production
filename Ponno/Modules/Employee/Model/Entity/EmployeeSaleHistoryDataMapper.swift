//
//  EmployeeSaleHistoryDataMapper.swift
//  Ponno
//
//  Created by a k azad on 1/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class EmployeeSaleHistoryDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var sales : [EmployeeSales]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        sales <- map["sales"]
        pagination <- map["pagination"]
        message <- map["message"]
    }
    
}

