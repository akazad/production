//
//  EmployeeSummary.swift
//  Ponno
//
//  Created by a k azad on 25/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class EmployeeSummary : Mappable {
    var title : String?
    var value : String?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        title <- map["title"]
        value <- map["value"]
    }
    
}
