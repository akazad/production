//
//  EmployeeSalaries.swift
//  Ponno
//
//  Created by a k azad on 16/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class EmployeeSalaries : Mappable {
    var id : Int = 0
    var amount : String = ""
    var status : Int = 0
    var expenseId : Int = 0
    var createdAt : String = ""
    var description : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        status <- map["status"]
        expenseId <- map["expenseId"]
        amount <- map["amount"]
        createdAt <- map["created_at"]
        description <- map["description"]
    }
    
}
