//
//  EmployeeDetailsDataMapper.swift
//  Ponno
//
//  Created by a k azad on 16/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class EmployeeDetailsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var employee : Employee?
    var salesHistory : SalesHistory?
    var salaryHistory : SalaryHistory?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        employee <- map["employee"]
        salesHistory <- map["sales_history"]
        salaryHistory <- map["salary_history"]
    }
    
}

