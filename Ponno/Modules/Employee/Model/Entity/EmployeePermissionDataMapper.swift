//
//  EmployeePermissionDataMapper.swift
//  Ponno
//
//  Created by a k azad on 14/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class EmployeePermissionDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var permissions : [EmployeePermissions]?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        permissions <- map["permissions"]
    }
    
}
