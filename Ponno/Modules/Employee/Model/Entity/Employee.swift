//
//  Employee.swift
//  Ponno
//
//  Created by a k azad on 16/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class Employee : Mappable {
    var id : Int = 0
    var name : String = ""
    var phone : String = ""
    var image : String = ""
    var joiningDate : String = ""
    var totalInvoice : Int = 0
    var totalSale : Double = 0.0
    var totalSalaryPaid : Double = 0.0
    var paid : Double = 0.0
    var unpaid : Double = 0.0
    var status : Int = 0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        image <- map["image"]
        joiningDate <- map["joining_date"]
        totalInvoice <- map["total_invoice"]
        totalSale <- map["total_sale"]
        totalSalaryPaid <- map["total_salary_paid"]
        paid <- map["paid"]
        unpaid <- map["unpaid"]
        status <- map["status"]
    }
    
}

