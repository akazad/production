//
//  EmployeeList.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class EmployeeList : Mappable {
    var id : Int = 0
    var name : String = ""
    var phone : String = ""
    var image : String = ""
    var pharmacyId : Int = 0
    var parent : Int = 0
    var status : Int = 0
    var language : Int = 0
    var createdAt : String = ""
    var updatedAt : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        image <- map["image"]
        pharmacyId <- map["pharmacy_id"]
        parent <- map["parent"]
        status <- map["status"]
        language <- map["language"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
    }
    
}


