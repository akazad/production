//
//  EmployeeDetailsInfoCell.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class EmployeeDetailsInfoCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet var imageIcon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: EmployeeDetailsInfoCell.self)
    }
    
//    private func setUpViews (){
//        self.alphabetView.layer.cornerRadius = self.alphabetView.frame.height/2
//        self.alphabetView.clipsToBounds = true
//    }

}
