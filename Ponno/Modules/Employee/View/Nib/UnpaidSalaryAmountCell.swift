//
//  UnpaidSalaryAmountCell.swift
//  Ponno
//
//  Created by a k azad on 30/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class UnpaidSalaryAmountCell: UITableViewCell {


    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var unpaidSalaryLbl: UILabel!
    @IBOutlet weak var unpaidSalaryAmountLbl: UILabel!
    
    var employeeUnpaidDataMapper: UnpaidSalaryDataMapper?{
        didSet{
            self.unpaidSalaryLbl.text = LanguageManager.UnpaidSalaryAmount
            if let data = self.employeeUnpaidDataMapper{
                if let total = data.totalUnpaid{
                    if let payments = data.unpaidSalaries, payments.count > 0{
                        let paymentsRepresentation = payments.joined(separator: " + ")
                        self.unpaidSalaryAmountLbl.text = paymentsRepresentation + " = " + "\(total)"
                    }
                }

            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.cardView.backgroundColor = UIColor.init(red: 248, green: 226, blue: 228)
        self.selectionStyle = .none
        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: UnpaidSalaryAmountCell.self)
    }
    
    
}
