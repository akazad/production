//
//  EmployeeSalaryHistoryCell.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class EmployeeSalaryHistoryCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var createdAt: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var descriptionPopUpBtn: UIButton!
    @IBOutlet weak var popUpBtn: UIButton!
    
    var employeeSalaries : EmployeeSalaries?{
        didSet{
            if let item = employeeSalaries{
                createdAt.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                amountLabel.text = LanguageManager.Total + " : " + "\(item.amount)" + " " + Constants.currencySymbol
                if item.status == 0{
                    statusLbl.text = LanguageManager.Pending
                    statusLbl.textColor = UIColor.lightRed
                    statusView.backgroundColor = UIColor(red: 248, green: 226, blue: 228)
                }else{
                    statusLbl.text = LanguageManager.Paid
                    statusLbl.textColor = UIColor(red: 111, green: 150, blue: 225)
                    statusView.backgroundColor = UIColor(red: 244, green: 254, blue: 225)
                }
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: EmployeeSalaryHistoryCell.self)
    }
}
