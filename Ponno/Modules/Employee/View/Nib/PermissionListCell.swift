//
//  PermissionListCell.swift
//  Ponno
//
//  Created by a k azad on 15/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PermissionListCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var permissionText: UILabel!
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: PermissionListCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
