//
//  EmployeeDetailsSummaryCell.swift
//  Ponno
//
//  Created by a k azad on 20/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class EmployeeDetailsSummaryCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var joiningDateTitleLbl: UILabel!{
        didSet{
            joiningDateTitleLbl.text = LanguageManager.JoiningDate
        }
    }
    @IBOutlet weak var joiningDateLabel: UILabel!
    @IBOutlet weak var accountStatusTitleLbl: UILabel!{
        didSet{
            accountStatusTitleLbl.text = LanguageManager.AccountStatus
        }
    }
    @IBOutlet weak var accountStatusLbl: UILabel!
    
    @IBOutlet weak var totalInvoiceLabel: UILabel!
    @IBOutlet weak var totalInvoiceTitleLbl: UILabel!{
        didSet{
            totalInvoiceTitleLbl.text = LanguageManager.TotalInvoice
        }
    }
    @IBOutlet weak var totalSaleTitleLbl: UILabel!{
        didSet{
            totalSaleTitleLbl.text = LanguageManager.TotalSale
        }
    }
    @IBOutlet weak var totalSaleLabel: UILabel!
    @IBOutlet weak var paidSalaryTitleLbl: UILabel!{
        didSet{
            paidSalaryTitleLbl.text = LanguageManager.PaidSalary
        }
    }
    @IBOutlet weak var paidSalaryLabel: UILabel!
    
    @IBOutlet weak var unpaidSalaryLbl: UILabel!
    @IBOutlet weak var unpaidSalaryTitleLbl: UILabel!{
        didSet{
            unpaidSalaryTitleLbl.text = LanguageManager.UnpaidSalary
        }
    }
    
    var employeeDetails : Employee?{
        didSet{
            if let details = employeeDetails{
                joiningDateLabel.text = details.joiningDate.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
                totalInvoiceLabel.text = "\(details.totalInvoice)"
                totalSaleLabel.text = "\(details.totalSale)" + " " + Constants.currencySymbol
                paidSalaryLabel.text = "\(details.paid)" + " " + Constants.currencySymbol
                unpaidSalaryLbl.text = "\(details.unpaid)" + " " + Constants.currencySymbol
                
                if details.status == 1{
                    accountStatusLbl.text = LanguageManager.Active
                }else{
                    accountStatusLbl.text = LanguageManager.Inactive
                }
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: EmployeeDetailsSummaryCell.self)
    }
}
