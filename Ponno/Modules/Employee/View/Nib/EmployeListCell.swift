//
//  EmployeListCell.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class EmployeListCell: UITableViewCell {


    @IBOutlet weak var cardView: UIView!
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var employeName: UILabel!
    @IBOutlet weak var employePhone: UILabel!
    @IBOutlet weak var employeeStatus: UISwitch!
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: EmployeListCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }
    
//   private func setUpViews (){
//        self.firstAlphabetView.layer.cornerRadius = self.firstAlphabetView.frame.height/2
//        self.firstAlphabetView.clipsToBounds = true
//    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
