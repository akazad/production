//
//  EmployeeSalesHistoryCell.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class EmployeeSalesHistoryCell: UITableViewCell {
    
    @IBOutlet weak var carrdView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var invoiceLabel: UILabel!
    @IBOutlet weak var totalSale: UILabel!
    @IBOutlet weak var paidLabel: UILabel!
    @IBOutlet weak var dueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: carrdView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: EmployeeSalesHistoryCell.self)
    }
}
