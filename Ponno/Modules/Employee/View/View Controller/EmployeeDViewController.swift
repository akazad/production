//
//  EmployeeDViewController.swift
//  Ponno
//
//  Created by a k azad on 29/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class EmployeeDViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!{
        didSet{
            segmentedControl.setTitle(LanguageManager.SaleHistory, forSegmentAt: 0)
            segmentedControl.setTitle(LanguageManager.SalaryHistory, forSegmentAt: 1)
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessage: UILabel!{
        didSet{
            emptyMessage.text = LanguageManager.NoInformationFound
        }
    }
    
    private var presenter = EmployeeDetailsPresenter(service: EmployeeService())
    
    var employeeId : Int?
    var employeeSalesList : [EmployeeSales] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var employeeSalaries : [EmployeeSalaries] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isFirst = true
    var selectedSegment : Int?
    
    //    PaginationPropertise
    var isLoading : Bool = false
    var salesCurrentPage : Int = 1
    var salaryCurrentPage : Int = 1
    var lastPageNo: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.configureTableView()
        self.initialSetup()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.segmentedControl.addTarget(self, action: #selector(onSegmentChange), for: .valueChanged) 
    }
    
    @objc func onSegmentChange(sender: UISegmentedControl){
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            self.isFirst = true
            self.selectedSegment = 0
            self.employeeSalesList = []
            self.isLoading = true
            self.salesCurrentPage = 1
            guard let id = self.employeeId else{
                return
            }
            self.presenter.getEmployeeSaleHistoryFromServer(page: self.salesCurrentPage, id: id)
            
        case 1:
            self.isFirst = false
            self.selectedSegment = 1
            self.employeeSalaries = []
            self.isLoading = true
            self.salaryCurrentPage = 1
            guard let id = self.employeeId else{
                return
            }
            self.presenter.getEmployeeSalaryHistoryFromServer(page: self.salaryCurrentPage, id: id)
        default:
            break;
        }
    }
    
}

//Mark: TableView Delegate And Data Source
extension EmployeeDViewController: UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = .clear
        self.tableView.register(EmployeeSalesHistoryCell.nib, forCellReuseIdentifier: EmployeeSalesHistoryCell.identifier)
        self.tableView.register(EmployeeSalaryHistoryCell.nib, forCellReuseIdentifier: EmployeeSalaryHistoryCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.emptyMessage.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFirst{
            if self.employeeSalesList.count > 0 {
                self.emptyMessage.isHidden = true
                return self.employeeSalesList.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        } else{
            if self.employeeSalaries.count > 0 {
                self.emptyMessage.isHidden = true
                return self.employeeSalaries.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFirst{
            let cell : EmployeeSalesHistoryCell = tableView.dequeueReusableCell(withIdentifier: EmployeeSalesHistoryCell.identifier, for: indexPath) as! EmployeeSalesHistoryCell
            cell.selectionStyle = .none
            if self.employeeSalesList.count > 0 {
                let item = self.employeeSalesList[indexPath.row]
                cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
                cell.dateLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.invoiceLabel.text = item.invoice
                cell.dueLabel.text = LanguageManager.Due + " : " + "\(item.due)" + " " + Constants.currencySymbol
                cell.totalSale.text = LanguageManager.Total + " : " + "\(item.total)"  + " " + Constants.currencySymbol
                cell.paidLabel.text = LanguageManager.Paid + " : " + "\(item.payable)" + " " + Constants.currencySymbol
                if isLoading == false && indexPath.row == self.employeeSalesList.count - 1 && self.salesCurrentPage < self.lastPageNo {
                    self.isLoading = true
                    self.salesCurrentPage += 1
                    guard let id = self.employeeId else{
                        return cell
                    }
                    self.presenter.getEmployeeSaleHistoryFromServer(page: self.salesCurrentPage, id: id)
                }
            }
            return cell
        }else{
            let cell : EmployeeSalaryHistoryCell = tableView.dequeueReusableCell(withIdentifier: EmployeeSalaryHistoryCell.identifier, for: indexPath) as! EmployeeSalaryHistoryCell
            cell.selectionStyle = .none
            if self.employeeSalaries.count > 0 {
                let item = self.employeeSalaries[indexPath.row]
                
                cell.employeeSalaries = item
                
                if item.description.isEmpty{
                    cell.descriptionPopUpBtn.isHidden = true
                }else{
                    cell.descriptionPopUpBtn.isHidden = false
                }
                
                cell.descriptionPopUpBtn.tag = item.id
                cell.descriptionPopUpBtn.addTarget(self, action: #selector(onDescriptionTap), for: .touchUpInside)
                
                cell.popUpBtn.tag = item.id
                cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
                
                if isLoading == false && indexPath.row == self.employeeSalaries.count - 1 && self.salaryCurrentPage < self.lastPageNo {
                    self.isLoading = true
                    self.salaryCurrentPage += 1
                    guard let id = self.employeeId else{
                        return cell
                    }
                    self.presenter.getEmployeeSalaryHistoryFromServer(page: self.salaryCurrentPage, id: id)
                }
                
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isFirst {
            return 106.0
        }else{
            return 75.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFirst {
            if self.employeeSalesList.count > 0 {
                let item = self.employeeSalesList[indexPath.row]
                let id = item.id
                self.navigateToSaleDetailsVC(iD: id)
            }
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onDescriptionTap(sender: UIButton){
        let id = sender.tag
        for item in self.employeeSalaries{
            if item.id == id{
                //self.showAlert(title: "", message: <#T##String#>)
            }
        }
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.employeeSalaries.count > 0{
            let id = sender.tag
            for item in self.employeeSalaries{
                if id == item.id{
                    let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToEmployeeSalaryUpdateVC(iD : id, employeeSalary: item)
                    }
                    
                    let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    // add action buttons to action sheet
                    myActionSheet.addAction(editAction)
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        // present the action sheet
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let saleDetailsVC = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        saleDetailsVC.iD = iD
        self.navigationController?.pushViewController(saleDetailsVC, animated: true)
    }
    
    func navigateToEmployeeSalaryUpdateVC(iD : Int, employeeSalary: EmployeeSalaries){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "EmployeeSalaryUpdateViewController") as! EmployeeSalaryUpdateViewController
        viewController.salaryHistoryId = iD
        viewController.employeeSalaries = employeeSalary
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: userMessage, message: "", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "Yes", style: .default){
            (action:UIAlertAction!) in
            let param : [String : Any] = ["id": deleteId]
            self.presenter.postEmployeeSalaryDeleteDataToServer(param: param)
        }
        let cancelAction = UIAlertAction(title: "No", style: .default){
            (action:UIAlertAction!) in
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension EmployeeDViewController : EmployeeDetailsViewDelegate{
    
    func setEmployeeSaleHistoryData(data: EmployeeSaleHistoryDataMapper) {
        guard let saleHistory = data.sales, saleHistory.count > 0 else {
            return
        }
        self.isLoading = false
        self.employeeSalesList += saleHistory
        guard let pagination = data.pagination else{
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setEmployeeSalaryHistoryData(data: EmployeeSalaryHistoryDataMapper) {
        guard let salaryHistory = data.salaries, salaryHistory.count > 0 else {
            return
        }
        self.isLoading = false
        self.employeeSalaries += salaryHistory
        guard let pagination = data.pagination else{
            return
        }
        self.lastPageNo = pagination.lastPageNo
        
    }
    
    func onSalaryHistoryDelete(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    
    func setEmployeeDetailsData(data: EmployeeDetailsDataMapper) {
        //This one is Empty
    }
    func onFailed(data: String) {
        //self.showAlert(title: "পুনরায় চেষ্টা করুণ", message: "")
    }
    
    func showLoading() {
        self.emptyMessage.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.emptyMessage.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        guard let id = self.employeeId else{
            return
        }
        self.presenter.getEmployeeSaleHistoryFromServer(page: self.salesCurrentPage, id: id)
    }
}

extension EmployeeDViewController{
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title:  LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.employeeSalaries = []
                guard let id = self.employeeId else{
                    return
                }
                self.presenter.getEmployeeSaleHistoryFromServer(page: self.salesCurrentPage, id: id)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}
