//
//  EmployeeDBaseViewPopOverVC.swift
//  Ponno
//
//  Created by a k azad on 31/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class EmployeeDBaseViewPopOverVC: UIViewController {

    @IBOutlet weak var salaryBtn: UIButton!
    @IBOutlet weak var payBtn: UIButton!
    
    var employeeId: Int?
    var unpaidSalary : Double?
    
    var navigateTo: NavigateFromEmployeeBase?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.intialSetup()
    }
    
    func intialSetup(){
        self.setUpInitialLanguage()
        self.salaryBtn.setTitle(LanguageManager.Salary, for: .normal)
        self.payBtn.setTitle(LanguageManager.Payment, for: .normal)

        self.salaryBtn.addTarget(self, action: #selector(onSalaryBtnTapped), for: .touchUpInside)
        if let unpaidSalaryAmount = self.unpaidSalary{
            if !(unpaidSalaryAmount > 0){
                self.payBtn.isEnabled = false
            }
        }
        self.payBtn.addTarget(self, action: #selector(onPayBtnTapped), for: .touchUpInside)
    }
    
    @objc func onSalaryBtnTapped(sender: UIButton){
        if let id = self.employeeId{
            self.dismiss(animated: true, completion: nil)
            navigateTo?.navigateToAddSalary(id: id)
        }
    }
    
    @objc func onPayBtnTapped(sender: UIButton){
        if let id = self.employeeId{
            self.dismiss(animated: true, completion: nil)
            navigateTo?.navigateToSalaryPayment(id: id)
        }
    }
    

}
