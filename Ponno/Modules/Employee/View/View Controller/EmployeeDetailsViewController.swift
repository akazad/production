//
//  EmployeeDetailsViewController.swift
//  Ponno
//
//  Created by a k azad on 17/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit
import Floaty

class EmployeeDetailsViewController: UIViewController {
    @IBOutlet weak var emptyMessage: UILabel!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet var panView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = EmployeeDetailsPresenter(service: EmployeeService())
    
    var employeeId : Int?
    var employeeSalesList : [EmployeeSales] = []
    var employeeSalaries : [EmployeeSalaries] = []
    
    var isFirst = true
    var selectedSegment : Int?
    
//    PaginationPropertise
    var isLoading : Bool = false
    var currentPage : Int = 1
    
    //    Mark: PanView
    var lastY: CGFloat = 0
    var pan: UIPanGestureRecognizer!
    
    var bottomSheetDelegate: BottomSheetDelegate?
    var parentView: UIView!
    
    var initalFrame: CGRect!
    var topY: CGFloat = 80 //change this in viewWillAppear for top position
    var middleY: CGFloat = 400 //change this in viewWillAppear to decide if animate to top or bottom
    var bottomY: CGFloat = 600 //no need to change this
    let bottomOffset: CGFloat = 80 //sheet height on bottom position
    var lastLevel: SheetLevel = .bottom //choose inital position of the sheet
    
    var disableTableScroll = false
    
    //hack panOffset To prevent jump when goes from top to down
    var panOffset: CGFloat = 0
    var applyPanOffset = false
    
    //tableview variables
    var listItems: [Any] = []
    var headerItems: [Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        pan.delegate = self
        self.panView.addGestureRecognizer(pan)
        
        self.tableView.panGestureRecognizer.addTarget(self, action: #selector(handlePan(_:)))
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap(_:)))
        tap.delegate = self
        tableView.addGestureRecognizer(tap)
        self.configureTableView()
    }
    
    func navigateToNewEmployeeAddVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "NewEmployeeAddViewController") as! NewEmployeeAddViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initalFrame = UIScreen.main.bounds
        self.topY = round(initalFrame.height * 0.05)
        self.middleY = initalFrame.height * 0.6
        self.bottomY = initalFrame.height - bottomOffset
        self.lastY = self.middleY
        
        bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.middleY))
    }
    
    
    @IBAction func segmentedControllAction(_ sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            self.isFirst = true
            self.selectedSegment = 0
            self.employeeSalesList = []
            self.isLoading = true
            self.currentPage = 1
            guard let id = self.employeeId else{
                return
            }
            self.presenter.getEmployeeSaleHistoryFromServer(page: self.currentPage, id: id)
            
        case 1:
            self.isFirst = false
            self.selectedSegment = 1
            self.employeeSalaries = []
            self.isLoading = true
            self.currentPage = 1
            guard let id = self.employeeId else{
                return
            }
            self.presenter.getEmployeeSalaryHistoryFromServer(page: self.currentPage, id: id) 
        default:
            break;
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == tableView else {return}
        
        if (self.parentView.frame.minY > topY){
            self.tableView.contentOffset.y = 0
        }
    }
    
    
    //this stops unintended tableview scrolling while animating to top
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard scrollView == tableView else {return}
        
        if disableTableScroll{
            targetContentOffset.pointee = scrollView.contentOffset
            disableTableScroll = false
        }
    }
    
    
    @objc func handleTap(_ recognizer: UITapGestureRecognizer){
        let p = recognizer.location(in: self.tableView)
        let index = tableView.indexPathForRow(at: p)
        
        if selectedSegment == 0 {
            if self.employeeSalesList.count > 0 {
                guard let salesIndex = index else {
                    return
                }
                let saleItem = self.employeeSalesList[salesIndex.row]
                
                self.navigateToSaleDetailsVC(iD: saleItem.id) 
            }
        }
      
        tableView.selectRow(at: index, animated: false, scrollPosition: .none)
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let saleDetailsVC = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        saleDetailsVC.iD = iD
        self.navigationController?.pushViewController(saleDetailsVC, animated: true)
    }
    
    @objc func handlePan(_ recognizer: UIPanGestureRecognizer){
        
        let dy = recognizer.translation(in: self.parentView).y
        switch recognizer.state {
        case .began:
            applyPanOffset = (self.tableView.contentOffset.y > 0)
        case .changed:
            if self.tableView.contentOffset.y > 0{
                panOffset = dy
                return
            }
            
            if self.tableView.contentOffset.y <= 0{
                if !applyPanOffset{panOffset = 0}
                let maxY = max(topY, lastY + dy - panOffset)
                let y = min(bottomY, maxY)
                //                self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: y)
                bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: y))
            }
            
            if self.parentView.frame.minY > topY{
                self.tableView.contentOffset.y = 0
            }
        case .failed, .ended, .cancelled:
            panOffset = 0
            
            if (self.tableView.contentOffset.y > 0){
                return
            }
            
            self.panView.isUserInteractionEnabled = false
            
            self.disableTableScroll = self.lastLevel != .top
            
            self.lastY = self.parentView.frame.minY
            self.lastLevel = self.nextLevel(recognizer: recognizer)
            
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.9, options: .curveEaseOut, animations: {
                
                switch self.lastLevel{
                case .top:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.topY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.topY))
                    self.tableView.contentInset.bottom = 50
                case .middle:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.middleY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.middleY))
                case .bottom:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.bottomY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.bottomY))
                }
                
            }) { (_) in
                self.panView.isUserInteractionEnabled = true
                self.lastY = self.parentView.frame.minY
            }
        default:
            break
        }
    }
    
    func nextLevel(recognizer: UIPanGestureRecognizer) -> SheetLevel{
        let y = self.lastY
        let velY = recognizer.velocity(in: self.view).y
        if velY < -200{
            return y > middleY ? .middle : .top
        }else if velY > 200{
            return y < (middleY + 1) ? .middle : .bottom
        }else{
            if y > middleY {
                return (y - middleY) < (bottomY - y) ? .middle : .bottom
            }else{
                return (y - topY) < (middleY - y) ? .top : .middle
            }
        }
    }
}

//Mark: TableView Delegate And Data Source
extension EmployeeDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = .clear
        self.tableView.register(EmployeeSalesHistoryCell.nib, forCellReuseIdentifier: EmployeeSalesHistoryCell.identifier)
        self.tableView.register(EmployeeSalaryHistoryCell.nib, forCellReuseIdentifier: EmployeeSalaryHistoryCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.emptyMessage.isHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFirst{
            if self.employeeSalesList.count > 0 {
                self.emptyMessage.isHidden = true
                return self.employeeSalesList.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        } else{
            if self.employeeSalaries.count > 0 {
                self.emptyMessage.isHidden = true
                return self.employeeSalaries.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFirst{
            let cell : EmployeeSalesHistoryCell = tableView.dequeueReusableCell(withIdentifier: EmployeeSalesHistoryCell.identifier, for: indexPath) as! EmployeeSalesHistoryCell
            cell.selectionStyle = .none
            if self.employeeSalesList.count > 0 {
                let item = self.employeeSalesList[indexPath.row]
                cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
                cell.dateLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.invoiceLabel.text = item.invoice
                cell.dueLabel.text = "বাকিঃ " + "\(item.due)" + " " + Constants.currencySymbol
                cell.totalSale.text = "সর্বমোটঃ " + "\(item.total)"  + " " + Constants.currencySymbol
                cell.paidLabel.text = "পরিশোধঃ " + "\(item.payable)" + " " + Constants.currencySymbol
                if isLoading == false && indexPath.row == self.employeeSalesList.count - 1 {
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.employeeId else{
                        return cell
                    }
                    self.presenter.getEmployeeSaleHistoryFromServer(page: self.currentPage, id: id) 
                }
            }
            return cell
        }else{
            let cell : EmployeeSalaryHistoryCell = tableView.dequeueReusableCell(withIdentifier: EmployeeSalaryHistoryCell.identifier, for: indexPath) as! EmployeeSalaryHistoryCell
            cell.selectionStyle = .none
            if self.employeeSalaries.count > 0 {
                let item = self.employeeSalaries[indexPath.row]
                cell.createdAt.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.amountLabel.text = "সর্বমোটঃ " + "\(item.amount)" + " " + Constants.currencySymbol
                if isLoading == false && indexPath.row == self.self.employeeSalaries.count - 1 {
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.employeeId else{
                        return cell
                    }
                    self.presenter.getEmployeeSalaryHistoryFromServer(page: self.currentPage, id: id)
                }
                
            }
            return cell
        }
        //        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isFirst {
            return 88.0
        }else{
            return 68.0
        }
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
}

extension EmployeeDetailsViewController: UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
extension EmployeeDetailsViewController : EmployeeDetailsViewDelegate{
    func onSalaryHistoryDelete(data: AddDataMapper) {
        //
    }
    
    func setEmployeeSaleHistoryData(data: EmployeeSaleHistoryDataMapper) {
        guard let saleHistory = data.sales, saleHistory.count > 0 else {
            return
        }
        self.isLoading = false
        self.employeeSalesList += saleHistory
        self.refreshTableView()
    }
    
    func setEmployeeSalaryHistoryData(data: EmployeeSalaryHistoryDataMapper) {
        guard let salaryHistory = data.salaries, salaryHistory.count > 0 else {
            return
        }
        self.isLoading = false
        self.employeeSalaries += salaryHistory
        self.refreshTableView()
    }
    
    func setEmployeeDetailsData(data: EmployeeDetailsDataMapper) {
        //This one is Empty
    }
    func onFailed(data: String) {
        self.showAlert(title: "পুনরায় চেষ্টা করুণ", message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        guard let id = self.employeeId else{
            return
        }
        self.presenter.getEmployeeSaleHistoryFromServer(page: self.currentPage, id: id)
    }
}


