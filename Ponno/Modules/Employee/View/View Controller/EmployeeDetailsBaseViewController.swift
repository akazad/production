//
//  EmployeeDetailsBaseViewController.swift
//  Ponno
//
//  Created by a k azad on 17/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//


//

import UIKit
import SDWebImage

protocol updateButtonDelegate : NSObjectProtocol {
    func update(employee : Employee)
}

class EmployeeDetailsBaseViewController: UIViewController, BottomSheetDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var container: UIView!
    
    private var presenter = EmployeeDetailsPresenter(service: EmployeeService())
    
    var employee : Employee?
    var employeeId : Int?
    
    enum Sections: Int {
        case EmployeeInfo
        case EmployeeSummary
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        container.layer.cornerRadius = 15
        container.layer.masksToBounds = true
        self.configureTableView()
        //tableView.isHidden = true
        self.setBarButtonSize()
    }
    
    func setBarButtonSize(){
        
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "edit"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onEdit(sender:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func onEdit(sender: UIBarButtonItem){
        self.navigateToUpdateVC()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? EmployeeDetailsViewController{
            viewController.bottomSheetDelegate = self
            viewController.employeeId = self.employeeId
//            viewController.delegate = self
            viewController.parentView = container
        }
    }
    
    func updateBottomSheet(frame: CGRect) {
        container.frame = frame
        //        backView.frame = self.view.frame.offsetBy(dx: 0, dy: 15 + container.frame.minY - self.view.frame.height)
        //        backView.backgroundColor = UIColor.black.withAlphaComponent(1 - (frame.minY)/200)
    }
    
//    @objc func handleTap(_ recognizer: UITapGestureRecognizer){
//        let p = recognizer.location(in: self.tableView)
//        let index = tableView.indexPathForRow(at: p)
////        
////        if Sections.EmployeeInfo.rawValue {
////            
////        }
//        
//        self.navigateToUpdateVC()
//        //        if selectedSegment == 0 {
////            if self.employeeSalesList.count > 0 {
////                guard let salesIndex = index else {
////                    return
////                }
////                let saleItem = self.employeeSalesList[salesIndex.row]
////
////                self.navigateToSaleDetailsVC(iD: String(describing: saleItem.id))
////            }
////        }
//        
//        tableView.selectRow(at: index, animated: false, scrollPosition: .none)
//    }
    
    func navigateToUpdateVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "NewEmployeeAddViewController") as! NewEmployeeAddViewController
        viewController.employee = employee
        viewController.employeeId = self.employeeId
        viewController.employeeState = EmployeeState.Update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//MARK: TableView Delegate and DataSource
extension EmployeeDetailsBaseViewController : UITableViewDataSource, UITableViewDelegate {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(EmployeeDetailsInfoCell.nib, forCellReuseIdentifier: EmployeeDetailsInfoCell.identifier)
        self.tableView.register(EmployeeDetailsSummaryCell.nib, forCellReuseIdentifier: EmployeeDetailsSummaryCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.EmployeeInfo.rawValue:
            return 1
        case Sections.EmployeeSummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case Sections.EmployeeInfo.rawValue:
            let cell : EmployeeDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: EmployeeDetailsInfoCell.identifier, for: indexPath) as! EmployeeDetailsInfoCell
            cell.selectionStyle = .none
            //cell.delegate = self
            
            guard let employeeDetails = self.employee else{
                return cell
            }
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.employeeImage + employeeDetails.image), placeholderImage: UIImage(named: "placeholder"))
            
            cell.nameLabel.text = "নাম" + " " +   employeeDetails.name
             cell.phoneLabel.text = "ফোন" + " " + employeeDetails.phone
            self.employeeId = employeeDetails.id 
            //        cell.addressLabel.text = "ঠিকানাঃ "
            
//            cell.editBtn.addTarget(self, action: #selector(onEdit), for: .touchUpInside)
            
            return cell
        case Sections.EmployeeSummary.rawValue:
            let cell : EmployeeDetailsSummaryCell = tableView.dequeueReusableCell(withIdentifier: EmployeeDetailsSummaryCell.identifier, for: indexPath) as! EmployeeDetailsSummaryCell
            cell.selectionStyle = .none
            guard let employeeDetails = self.employee else{
                return cell
            }
            cell.joiningDateLabel.text = employeeDetails.joiningDate.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
            cell.totalInvoiceLabel.text = "\(employeeDetails.totalInvoice)"
            cell.totalSaleLabel.text = "\(employeeDetails.totalSale)" + " " + Constants.currencySymbol
            //cell.totalSalaryPaidLabel.text = "\(employeeDetails.totalSalaryPaid)" + " " + Constants.currencySymbol
            return cell
        default:
            return UITableViewCell()
        }
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.EmployeeInfo.rawValue:
            return UITableView.automaticDimension
        case Sections.EmployeeSummary.rawValue:
            return 140.0
        default:
            return 0
        }
    }
    
//    @objc func onEdit(sender : UIButton){
//        print("onEdit")
//    }
    
    
}

//Mark: Button Delegate
extension EmployeeDetailsBaseViewController : updateButtonDelegate{
    func update(employee: Employee){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "NewEmployeeAddViewController") as! NewEmployeeAddViewController
        viewController.employee = employee
        viewController.employeeState = EmployeeState.Update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

//Mark: Api Delegate
extension EmployeeDetailsBaseViewController : EmployeeDetailsViewDelegate{
    func onSalaryHistoryDelete(data: AddDataMapper) {
        //
    }
    
    func setEmployeeDetailsData(data: EmployeeDetailsDataMapper) {
        guard let item = data.employee else {
            return
        }
        self.employee = item
        tableView.isHidden = false
        self.refreshTableView()
    }
    
    func setEmployeeSaleHistoryData(data: EmployeeSaleHistoryDataMapper) {
        //ThisOneIsEmpty
    }
    
    func setEmployeeSalaryHistoryData(data: EmployeeSalaryHistoryDataMapper) {
        //ThisOneIsEmpty
    }
    
    func onFailed(data: String) {
        //self.showAlert(title: "পুনরায় চেষ্টা করুণ", message: "")
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        
        guard let id = self.employeeId else{
            return
        }
        self.presenter.getEmployeeDetailsFromServer(id: id)
    }
}

//extension EmployeeDetailsBaseViewController: EmployeeDataUpdate{
//    func updateEmployeeData(data: EmployeeDetailsDataMapper) {
//        guard let item = data.employee else {
//            return
//        }
//        self.employee = item
//        tableView.isHidden = false
//        self.refreshTableView()
//    }
//}

