//
//  EmployeeViewController.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

class EmployeeViewController: BaseViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
//    @IBOutlet weak var rightBarBtn: UIBarButtonItem!
    
    private var presenter = EmployeePresenter(service: EmployeeService())
    
    private var employeList : [EmployeeList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var filteredEmployeeList : [EmployeeList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    private var employeeSummary : [EmployeeSummary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    var switchStatus: Int = 0
    var employeeId: Int?
    var employeeStatus: Bool?
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        //self.attachPresenter()
//        self.setUpSearchBar()
        self.setBarButton()
        self.configureCollectionView()
        self.configureTableView()
        self.setNavigationBarTitle()
        self.addFloaty()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.attachPresenter()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.EmployeeBook
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
   
    
    func navigateToNewEmployeeAddVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "NewEmployeeAddViewController") as! NewEmployeeAddViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
//        let extraItem = FloatyItem()
//        extraItem.icon = UIImage(named: "next")
//        extraItem.buttonColor = UIColor.lightGreen
//        extraItem.title = "নতুন কর্মচারী"
//        extraItem.handler = { item in
//            self.navigateToNewEmployeeAddVC()
//        }
        
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(EmployeeViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.currentPage = 1
        self.employeList = []
        self.presenter.getEmployeeDataFromServer(page: self.currentPage)
        refreshControl.endRefreshing()
    }
    
    func setBarButton(){
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let employeeAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        employeeAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        employeeAddBtn.addTarget(self, action: #selector(onEmployeeAdd(sender:)), for: UIControl.Event.touchUpInside)
        employeeAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        employeeAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        employeeAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: employeeAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2,barButton1], animated: true)
    }
    
    @objc func onSearch(sender: UIButton){
        self.navigateToEmployeeSearchVC()
    }
    
    @objc func onEmployeeAdd(sender: UIButton){
        self.navigateToNewEmployeeAddVC()
    }
    
    func navigateToEmployeeSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "EmployeeSearchViewController") as! EmployeeSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

//Mark: Floaty Delegate
extension EmployeeViewController: FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}

////MARK: SearchBar Delegate
//extension EmployeeViewController: UISearchBarDelegate{
//
//
//    fileprivate func setUpSearchBar(){
//        self.searchBar.delegate = self
//    }
//
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        self.view.endEditing(true)
//
//        isSearchActive = false
//    }
//
//
//}

//MARK: CollectionView Delegate And DataSource
extension EmployeeViewController : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(CustomerSummaryCell.nib, forCellWithReuseIdentifier: CustomerSummaryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let item = self.employeeSummary, item.count > 0 else{
            return 0
        }
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CustomerSummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomerSummaryCell.identifier, for: indexPath) as! CustomerSummaryCell
        guard let list = self.employeeSummary, list.count > 0 else{
            return cell
        }
        let summaryItem = list[indexPath.row]
        
        
        cell.titleLabel.text = summaryItem.title
        cell.valueLabel.text = summaryItem.value
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 60.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.employeeSummary, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//Mark: TableView Delegate And DataSource
extension EmployeeViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(EmployeListCell.nib, forCellReuseIdentifier: EmployeListCell.identifier)
        self.tableView.rowHeight = 108
        self.tableView.tableFooterView = UIView()
        self.tableView.addSubview(refreshControl)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.employeList.count > 0 {
            return self.employeList.count
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : EmployeListCell = tableView.dequeueReusableCell(withIdentifier: EmployeListCell.identifier, for: indexPath) as! EmployeListCell
        cell.selectionStyle = .none
        if self.employeList.count > 0 {
            let employee = self.employeList[indexPath.row]
            
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.employeeImage + employee.image), placeholderImage: UIImage(named: "placeholder"))
            
            cell.employeName.text = employee.name
            cell.employePhone.text = employee.phone
            if employee.status == 1{
                //self.employeeStatus = true
                cell.employeeStatus.setOn(true, animated: true)
            }else{
               // self.employeeStatus = false
                cell.employeeStatus.setOn(false, animated: true)
            }
            cell.employeeStatus.tag = employee.id
            
            cell.employeeStatus.addTarget(self, action: #selector(onSwitchChange
                ), for: .valueChanged)
            if isLoading == false && indexPath.row == self.employeList.count - 1 && self.currentPage < self.lastPageNo {
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getEmployeeDataFromServer(page: self.currentPage)
            }
        }
        return cell
    }
    
    @objc func onSwitchChange(sender: UISwitch){
        print(sender.isOn)
        //employeeStatus = !employeeStatus
//        guard var status = self.employeeStatus else{
//            self.showAlert(title: "Something wrong!", message: "Try again later")
//            return
//        }
        let id = sender.tag
        
        var status = 0
        
        if sender.isOn{
           status = 1
        }else{
            status = 0
        }
        
        let param : [String : Any] = ["id": id, "status": status]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        self.presenter.postEmployeeStatusUpdateData(param: param)
        
        
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.employeList.count > 0 {
            let listItem = self.employeList[indexPath.row]
            let id = listItem.id
            self.navigateToEmployeeDetailsBaseVC(employeeId: id)
        }
    }
    
    fileprivate func navigateToEmployeeDetailsVC(employeeId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "EmployeeDetailsBaseViewController") as! EmployeeDetailsBaseViewController
        viewController.employeeId = employeeId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    fileprivate func navigateToEmployeeDetailsBaseVC(employeeId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "EmployeeDBaseViewController") as! EmployeeDBaseViewController
        viewController.employeeId = employeeId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

//Mark: Api Delegate

extension EmployeeViewController : EmployeeViewDelegate {
    
    func setEmployeList(data: EmployeeDataMapper) {
        guard let item = data.summary, item.count > 0 else {
            return
        }
        self.employeeSummary = item
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        guard let list = data.employee, list.count > 0 else{
            return
        }
        self.isLoading = false
        self.employeList += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
        
    }
    
    func setEmployeeStatus(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        //self.showAlert(title: message, message: "")
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter() {
        self.presenter.attachView(viewDelegate: self)
        self.employeList = []
        self.currentPage = 1
        self.isLoading = true
        self.presenter.getEmployeeDataFromServer(page: self.currentPage)
    }
    
}

extension EmployeeViewController{
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: "Successful", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.currentPage = 1
                    self.employeList = []
                    self.presenter.getEmployeeDataFromServer(page: self.currentPage)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        
    }
}
