//
//  EmployeeSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 18/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class EmployeeSearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = EmployeeSearchPresenter(service: EmployeeService())
    
    private var employeList : [EmployeeList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    
    var textToSearch : String = ""
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    var isLoading: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        self.setUpSearchBar()
        self.configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
        self.setUpInitialLanguage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.getEmployeeSearchDataFromServer(page: self.currentPage, searchString: "")
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }

}

//Mark: Search Delegate
extension EmployeeSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.EmployeeSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.textToSearch = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.employeList = []
        self.isLoading = true
        self.isSearchActive = true
        self.presenter.getEmployeeSearchDataFromServer(page: self.currentPage, searchString: textToSearch)
    }
}

//Mark: TableView Delegate And DataSource
extension EmployeeSearchViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(EmployeListCell.nib, forCellReuseIdentifier: EmployeListCell.identifier)
        self.tableView.rowHeight = 100
        self.tableView.tableFooterView = UIView()
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.employeList.count > 0 {
            return self.employeList.count
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : EmployeListCell = tableView.dequeueReusableCell(withIdentifier: EmployeListCell.identifier, for: indexPath) as! EmployeListCell
        cell.selectionStyle = .none
        if self.employeList.count > 0 {
            let employee = self.employeList[indexPath.row]
            
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.employeeImage + employee.image), placeholderImage: UIImage(named: "placeholder"))
            
            cell.employeName.text = employee.name
            cell.employePhone.text = employee.phone
            if employee.status == 1{
                //self.employeeStatus = true
                cell.employeeStatus.setOn(true, animated: true)
            }else{
                // self.employeeStatus = false
                cell.employeeStatus.setOn(false, animated: true)
            }
            cell.employeeStatus.tag = employee.id
            
            cell.employeeStatus.addTarget(self, action: #selector(onSwitchChange
                ), for: .valueChanged)
            if isLoading == false && indexPath.row == self.employeList.count - 1 && self.currentPage < self.lastPageNo {
                self.isLoading = true
                self.currentPage += 1
                if isSearchActive{
                    self.presenter.getEmployeeSearchDataFromServer(page: self.currentPage, searchString: textToSearch)
                }
            }
        }
        return cell
    }
    
    @objc func onSwitchChange(sender: UISwitch){
        print(sender.isOn)
        //employeeStatus = !employeeStatus
        //        guard var status = self.employeeStatus else{
        //            self.showAlert(title: "Something wrong!", message: "Try again later")
        //            return
        //        }
        let id = sender.tag
        
        var status = 0
        
        if sender.isOn{
            status = 1
        }else{
            status = 0
        }
        
        let param : [String : Any] = ["id": id, "status": status]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        self.presenter.postEmployeeStatusUpdateData(param: param)
        
        
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.employeList.count > 0 {
            let listItem = self.employeList[indexPath.row]
            let id = listItem.id
            self.navigateToEmployeeDetailsVC(employeeId: id)
        }
    }
    
    fileprivate func navigateToEmployeeDetailsVC(employeeId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "EmployeeDBaseViewController") as! EmployeeDBaseViewController
        viewController.employeeId = employeeId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

//Mark: Api Delegate
extension EmployeeSearchViewController: EmployeeSearchViewDelegate{
    
    func setEmployeSearchData(data: EmployeeDataMapper) {
        guard let list = data.employee, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.employeList += list
        
        guard let pagination = data.pagination else{
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setEmployeeStatus(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        //self.showAlert(title: message, message: "")
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.searchAlert(title: LanguageManager.NoInformationFound, message: data)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getEmployeeSearchDataFromServer(page: self.currentPage, searchString: "")
    }
}

extension EmployeeSearchViewController{
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.currentPage = 1
            self.employeList = []
            self.presenter.getEmployeeSearchDataFromServer(page: self.currentPage, searchString: "")
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
}
