//
//  NewEmployeeAddViewController.swift
//  Ponno
//
//  Created by a k azad on 6/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

enum EmployeeState : Int {
    case Add
    case Update
}

class NewEmployeeAddViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate  {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var imageView: CircularImageView!
    @IBOutlet weak var nameLabel: UILabel!{
        didSet{
            nameLabel.text = LanguageManager.EmployeeName
        }
    }
    @IBOutlet weak var nameTextField: UITextField!{
        didSet{
            nameTextField.placeholder = LanguageManager.EmployeeName
        }
    }
    @IBOutlet weak var phoneLabel: UILabel!{
        didSet{
            phoneLabel.text = LanguageManager.EmployeeMobileNo
        }
    }
    @IBOutlet weak var phoneTextField: UITextField!{
        didSet{
            phoneTextField.placeholder = LanguageManager.PhoneNumber
        }
    }
    @IBOutlet weak var passwordLabel: UILabel!{
        didSet{
            passwordLabel.text = LanguageManager.EmployeePassword
        }
    }
    @IBOutlet weak var passwordTextField: UITextField!{
        didSet{
            passwordTextField.placeholder = LanguageManager.Password + LanguageManager.SixToTwenty
        }
    }
    @IBOutlet weak var invisiblePassImageView: UIButton!
    @IBOutlet weak var invisibleRePassImageView: UIButton!
    @IBOutlet weak var languageTitleLbl: UILabel!{
        didSet{
            languageTitleLbl.text = LanguageManager.LanguageLbl + " :"
        }
    }
    @IBOutlet weak var segmentedControll: UISegmentedControl!{
        didSet{
            segmentedControll.setTitle(LanguageManager.BanglaTitle, forSegmentAt: 0)
            segmentedControll.setTitle(LanguageManager.EnglishTitle, forSegmentAt: 1)
        }
    }
    @IBOutlet weak var permissionLbl: UILabel!{
        didSet{
            permissionLbl.text = LanguageManager.Permission
        }
    }
    @IBOutlet weak var submitButtonLabel: UIButton!
    @IBOutlet weak var repeatPasswordLbl: UILabel!{
        didSet{
            repeatPasswordLbl.text = LanguageManager.ConfirmPassword
        }
    }
    @IBOutlet weak var repeatPasswordTextField: UITextField!{
        didSet{
            repeatPasswordTextField.placeholder = LanguageManager.ConfirmPassword
        }
    }
    
    private var presenter = NewEmployeeAddPresenter(service: EmployeeService())
    
    var permissions : [EmployeePermissions]?
    var selectedPermissions : [Int] = []
    var EmployeePermissions : [EmployeePermissions] = []
    
    let checkedIcon = UIImage(named: "check_icon.png")
    let uncheckedIcon = UIImage(named: "uncheck_icon")
    
    var employee : Employee?
    var employeeId : Int?
    var language : Int?
    var employeeState = EmployeeState.Add.rawValue
    
    let imagePicker = UIImagePickerController()
    
    var message : String?
    var selectedImage : UIImage?
    
    let screenHeight = UIScreen.main.bounds.height
    let scrollViewContentHeight = 770 as CGFloat
    
    var deliveryStatus : Int?
    
    var invisiblePassIconClick = true
    var invisibleRePassIconClick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.configureTableView()
        self.attachPresenter()
        self.configureTextFields()
        self.initialSetUp()
        self.setImageView()
    }
    
    func initialSetUp(){
        
        self.invisiblePassImageView.isHidden = true
        self.invisibleRePassImageView.isHidden = true
        if self.employeeState == EmployeeState.Add.rawValue {
            self.title = LanguageManager.NewEmployee
            imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imageView.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.employeeImage), placeholderImage: UIImage(named: "placeholder"))
        }else{
            self.title = LanguageManager.EmployeeInfoUpdate
            
            guard let name = self.employee?.name, let phone = self.employee?.phone else{
                return
            }
            self.nameTextField.text = name
            self.phoneTextField.text = phone
        }
        self.setUpToolBar()
        
        imagePicker.delegate = self
        self.imageView.addTapGestureRecognizer(action: {self.onTapOnImageView()})
        
        self.language = 0
        self.segmentedControll.addTarget(self, action: #selector(setLanguage), for: .valueChanged)
        
        if let shop = getShopData(){
            guard  let deliverySystemStatus = shop.deliverySystem else {
                return
            }
            self.deliveryStatus = deliverySystemStatus
        }
        
        self.passwordTextField.addTarget(self, action: #selector(onPasswordTextFieldChange), for: .editingChanged)
        self.repeatPasswordTextField.addTarget(self, action: #selector(onRepeatPasswordTextFieldChange), for: .editingChanged)
        self.invisiblePassImageView.addTarget(self, action: #selector(onTapOnInvisiblePassImageView), for: .touchUpInside)
        self.invisibleRePassImageView.addTarget(self, action: #selector(onTapOnInvisibleRePassImageView), for: .touchUpInside)
    }
    
    @objc func onPasswordTextFieldChange(textField: UITextField){
        if textField.text?.count ?? 0 > 1{
            self.invisiblePassImageView.isHidden = false
        }else{
            self.invisiblePassImageView.isHidden = true
        }
    }
    
    @objc func onRepeatPasswordTextFieldChange(textField: UITextField){
        if textField.text?.count ?? 0 > 1{
            self.invisibleRePassImageView.isHidden = false
        }else{
            self.invisibleRePassImageView.isHidden = true
        }
    }
    
    @objc func onTapOnInvisiblePassImageView(sender: UIButton){
        self.passwordTextField.isSecureTextEntry.toggle()
    }
    
    @objc func onTapOnInvisibleRePassImageView(sender: UIButton){
        self.repeatPasswordTextField.isSecureTextEntry.toggle()
    }
    
    private func setImageView(){
        guard let imageUrl = self.employee?.image else{
            self.setImageInImageView(imageUrl: "")
            return
        }
        
        self.setImageInImageView(imageUrl: ImageUrl.sharedImageInstance.employeeImage + imageUrl)
    }
    
   fileprivate func scrollViewSetup(){
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: scrollViewContentHeight) 
        scrollView.delegate = self
        scrollView.bounces = false
        tableView.bounces = false
        tableView.isScrollEnabled = false
    }
    
    func setImageInImageView(imageUrl : String){
        self.imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
    }
    
    func onTapOnImageView(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary

        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.contentMode = .scaleAspectFill
            imageView.image = pickedImage
            self.selectedImage = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitButtonLabel.isEnabled = isEnabled
    }
    
    @objc func setLanguage(sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0 {
            self.language = 0
        }else{
            self.language = 1
        }
    }
    
    func isValidated()->Bool{
        if (self.nameTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.EmployeeNameIsRequired)
            return false
        }
        guard let phone = phoneTextField.text else {
            return false
        }
        if (self.phoneTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
            return false
        }
        if (!validatePhoneNumber(value: phone)){
            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
            return false
        }
        else if(self.passwordTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.PasswordIsRequired)
            return false
        }else if (self.repeatPasswordTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.RepeatPasswordIsRequired)
            return false
        }
        else if (self.EmployeePermissions.count == 0){
            self.displayMessage(userMessage: LanguageManager.PermissionIsRequired)
            return false
        }
        
        let password = passwordTextField.text
        let confirmPassword = repeatPasswordTextField.text
        if password != confirmPassword{
            self.displayMessage(userMessage: LanguageManager.PasswordDidNotMatch)
        }
        return true
    }
    
    @IBAction func submitButton(_ sender: UIButton) {
        if isValidated(){
            //self.submitBtnControlWith(isEnabled: false)
            guard let name = self.nameTextField.text, let phone = self.phoneTextField.text, let password = self.passwordTextField.text, let lang = self.language else {
                return
            }
            
            var permissionsToUpload : [Int] = []
            
            for item in self.EmployeePermissions{
                permissionsToUpload.append(item.id)
            }
            
            if self.employeeState == EmployeeState.Add.rawValue{
                let employeeAdd: [String: Any] = ["name": name, "phone": phone, "password": password, "language" : lang, "permissions" : permissionsToUpload]
                self.presenter.postNewEmployeeDataToServer(employeeData: employeeAdd)
            }else{
                guard let id = self.employee?.id else{
                    return
                }
                var employeeUpdate: [String: Any] = ["name": name, "phone": phone, "password": password, "permissions" : permissionsToUpload]
                employeeUpdate["id"] = id
                employeeUpdate["language"] = lang
                self.presenter.updateEmployeeData(data: employeeUpdate)
            }
        }
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func updateSuccess(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func addSuccess(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigateToEmployeeViewController()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToEmployeeViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "EmployeeViewController") as! EmployeeViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is EmployeeViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func navigateToEmployeeVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "EmployeeViewController") as! EmployeeViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToNewEmployeeAddVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "NewEmployeeAddViewController") as! NewEmployeeAddViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func setUpToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.phoneTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        self.phoneTextField.resignFirstResponder()
        self.passwordTextField.becomeFirstResponder()
    }
    
}

//Mark: TextField Delegate
extension NewEmployeeAddViewController : UITextFieldDelegate{
    
    fileprivate func configureTextFields(){
        self.nameTextField.delegate = self
        self.phoneTextField.delegate = self
        self.passwordTextField.delegate = self
        self.repeatPasswordTextField.delegate = self
        self.nameTextField.underlined()
        self.phoneTextField.underlined()
        self.passwordTextField.underlined()
        self.repeatPasswordTextField.underlined()
//        self.nameTextField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.nameTextField{
            self.phoneTextField.becomeFirstResponder()
        }else if textField == self.passwordTextField {
            self.repeatPasswordTextField.becomeFirstResponder()
        }
        else{
            self.view.endEditing(true)
        }
        return false
    }
}

//Mark: TableView Delegate and Data Source
extension NewEmployeeAddViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PermissionListCell.nib, forCellReuseIdentifier: PermissionListCell.identifier)
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        self.tableView.separatorColor = .clear
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let list = permissions, list.count > 0 else{
            return 0
        }
        return list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PermissionListCell = tableView.dequeueReusableCell(withIdentifier: PermissionListCell.identifier, for: indexPath) as! PermissionListCell
        
        if let list = self.permissions, list.count > 0 {
            let permission = list[indexPath.row]
            
            cell.permissionText.text = permission.name
            
            let permissionId = permission.id
            
            if self.EmployeePermissions.contains(where : { $0.id == permissionId}){
                cell.icon.image = checkedIcon
            }
            else{
                cell.icon.image = uncheckedIcon
            }
        }
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let list = self.permissions, list.count > 0 {
            let permission = list[indexPath.row]
            let permissionId = permission.id
            
            if self.EmployeePermissions.contains(where : { $0.id == permissionId}){
                self.EmployeePermissions = self.EmployeePermissions.filter{ $0.id != permissionId }
            }
            else{
                self.EmployeePermissions.append(permission)
            }
            self.refreshTableView()
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        
        if scrollView == self.scrollView {
            if yOffset >= scrollViewContentHeight - screenHeight {
                scrollView.isScrollEnabled = false
                tableView.isScrollEnabled = true
            }
        }
        
        if scrollView == self.tableView {
            if yOffset <= 0 {
                self.scrollView.isScrollEnabled = true
                self.tableView.isScrollEnabled = false
            }
        }
    }
    
}


//Mark: API Delegate
extension NewEmployeeAddViewController: NewEmployeeAddViewDelegate{
    
    func updateEmployeeData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.message = message
        guard let id = self.employeeId, let image = self.selectedImage else{
            self.updateSuccess(userMessage: message)
            return
        }
        //print(employeeId)
        self.presenter.uploadImage(employeeId: id, image: image)
    }
    
    func setEmployeePermissions(data: EmployeePermissionDataMapper) {
        guard let permissions = data.permissions, permissions.count > 0 else{
            return
        }
        if deliveryStatus == 0 {
            self.permissions = permissions.filter({$0.id != 36})
        }else{
            self.permissions = permissions
        }
        
        self.refreshTableView()
    }
    
    func setPermissions(data: PermissionsOfEmployeeDataMapper) {
        guard let permissions = data.permissions, permissions.count > 0 else {
            return
        }
        self.EmployeePermissions = permissions
        self.refreshTableView()
    }
    
    
    func postNewEmployeeData(employeeData: EmployeeAddDataMapper) {
        guard let message = employeeData.message, let employeeID = employeeData.employee?.id else {
            return
        }
        
        self.message = message
        
        guard let image = self.selectedImage else{
            self.addSuccess(userMessage: message)
            return
        }
        self.presenter.uploadImage(employeeId: employeeID, image: image)
    }
    
    func onImageUpload(data: AddDataMapper) {
        self.showAlert(title: self.message ?? "" , message: "")
    }
    
    func onEmployeePermissionFailed(data: String) {
        
    }
    
    func onFailed(data: String) {
        self.displayMessage(userMessage: data)
    }
    
    func showLoading() {
        submitBtnControlWith(isEnabled : false)
        self.showLoader()
    }
    
    func hideLoading() {
        submitBtnControlWith(isEnabled : true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getEmployeePermissionsDataFromServer()
        guard let id = self.employeeId else {
            return
        }
        self.presenter.getPermissionsOfEmployeesFromServer(id: id)
    }
}

