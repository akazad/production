//
//  EmployeeDBaseViewController.swift
//  Ponno
//
//  Created by a k azad on 29/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import FloatingPanel
import SDWebImage

protocol NavigateFromEmployeeBase: NSObjectProtocol {
    func navigateToAddSalary(id: Int)
    func navigateToSalaryPayment(id: Int)
}

class EmployeeDBaseViewController: UIViewController, FloatingPanelControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = EmployeeDetailsPresenter(service: EmployeeService())
    
    var employee : Employee?
    var employeeId : Int?
    var employeeDetails : EmployeeDataMapper?
    
    enum Sections: Int {
        case EmployeeInfo
        case EmployeeSummary
    }
    
    var fpc: FloatingPanelController!
    var bottomShetVC : EmployeeDViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addBottomSheet()
        self.configureTableView()
        self.setBarBtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.initialSetup()
        self.attachPresenter()
    }
    
    func addBottomSheet(){
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = UIColor.lightGreen.withAlphaComponent(0.5)
        fpc.surfaceView.layer.cornerRadius = 9.0
        fpc.surfaceView.clipsToBounds = true
        
        fpc.surfaceView.shadowHidden = false
        bottomShetVC = storyboard?.instantiateViewController(withIdentifier: "EmployeeDViewController") as? EmployeeDViewController
        
        bottomShetVC.employeeId = self.employeeId
        
        fpc.set(contentViewController: bottomShetVC)
        fpc.track(scrollView: bottomShetVC.tableView)
        fpc.addPanel(toParent: self)
    }
    
    func initialSetup(){
        self.title = LanguageManager.EmployeeDetails
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarBtn(){
        let popUpBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        popUpBtn.setImage(UIImage(named: "popupmenudark"), for: UIControl.State.normal)
        popUpBtn.addTarget(self, action: #selector(onBarPopUpBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        popUpBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        popUpBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        popUpBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn3 = UIBarButtonItem(customView: popUpBtn)
        
        navigationItem.setRightBarButtonItems([barBtn3], animated: true)
    }
    
    @objc func onBarPopUpBtnTapped(sender: UIButton){
        self.popOverToDBasePopoverViewController(sender: sender)
    }
    
    
    func popOverToDBasePopoverViewController(sender: UIButton){
        let storyboard : UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: "EmployeeDBaseViewPopOverVC") as? EmployeeDBaseViewPopOverVC{
            controller.preferredContentSize = CGSize(width: 170, height: 110)
            controller.employeeId = self.employeeId
            controller.unpaidSalary = self.employee?.unpaid
            controller.navigateTo = self
            showPopup(controller, sourceView: sender)
        }
        
    }
    

}

//MARK: TableView Delegate and DataSource
extension EmployeeDBaseViewController : UITableViewDataSource, UITableViewDelegate {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(EmployeeDetailsInfoCell.nib, forCellReuseIdentifier: EmployeeDetailsInfoCell.identifier)
        self.tableView.register(EmployeeDetailsSummaryCell.nib, forCellReuseIdentifier: EmployeeDetailsSummaryCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.EmployeeInfo.rawValue:
            return 1
        case Sections.EmployeeSummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case Sections.EmployeeInfo.rawValue:
            let cell : EmployeeDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: EmployeeDetailsInfoCell.identifier, for: indexPath) as! EmployeeDetailsInfoCell
            cell.selectionStyle = .none
            //cell.delegate = self
            
            guard let employeeDetails = self.employee else{
                return cell
            }
            
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.employeeImage + employeeDetails.image), placeholderImage: UIImage(named: "placeholder"))
            
            cell.nameLabel.text = LanguageManager.EmployeeName + " : " + employeeDetails.name
            cell.phoneLabel.text = LanguageManager.PhoneNumber + " : " + employeeDetails.phone
            self.employeeId = employeeDetails.id
            //        cell.addressLabel.text = "ঠিকানাঃ "
            
            cell.editBtn.addTarget(self, action: #selector(onEdit), for: .touchUpInside) 
            
            return cell
        case Sections.EmployeeSummary.rawValue:
            let cell : EmployeeDetailsSummaryCell = tableView.dequeueReusableCell(withIdentifier: EmployeeDetailsSummaryCell.identifier, for: indexPath) as! EmployeeDetailsSummaryCell
            cell.selectionStyle = .none
            guard let employeeDetails = self.employee else{
                return cell
            }
            
            cell.employeeDetails = employeeDetails
            
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.EmployeeInfo.rawValue:
            return 220.0
        case Sections.EmployeeSummary.rawValue:
            return 210.0
        default:
            return 0
        }
    }
    
    @objc func onEdit(sender : UIButton){
        self.navigateToEmployeeUpdateVC()
    }
    
    func navigateToEmployeeUpdateVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "NewEmployeeAddViewController") as! NewEmployeeAddViewController
        viewController.employee = employee
        viewController.employeeId = self.employeeId
        viewController.employeeState = EmployeeState.Update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
}

//Mark: Api Delegate
extension EmployeeDBaseViewController : EmployeeDetailsViewDelegate{
    func onSalaryHistoryDelete(data: AddDataMapper) {
        //
    }
    
    func setEmployeeDetailsData(data: EmployeeDetailsDataMapper) {
        guard let item = data.employee else {
            return
        }
        self.employee = item
        tableView.isHidden = false
        self.refreshTableView()
    }
    
    func setEmployeeSaleHistoryData(data: EmployeeSaleHistoryDataMapper) {
        //ThisOneIsEmpty
    }
    
    func setEmployeeSalaryHistoryData(data: EmployeeSalaryHistoryDataMapper) {
        //ThisOneIsEmpty
    }
    
    func onFailed(data: String) {
        //self.showAlert(title: "পুনরায় চেষ্টা করুণ", message: "")
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        
        guard let id = self.employeeId else{
            return
        }
        self.presenter.getEmployeeDetailsFromServer(id: id)
    }
}

extension EmployeeDBaseViewController : NavigateFromEmployeeBase{
    func navigateToAddSalary(id: Int) {
        self.navigateToAddSalaryViewController(id: id)
    }
    
    func navigateToSalaryPayment(id: Int) {
        self.navigateToEmployeePaymentViewController(id: id)
    }
    
    func navigateToAddSalaryViewController(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "EmployeeSalaryAddViewController") as! EmployeeSalaryAddViewController
        viewController.employeeId = id
        viewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToEmployeePaymentViewController(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "EmployeePaymentViewController") as! EmployeePaymentViewController
        viewController.employeeId = id
        //viewController.employeeUnpaidDataMapper =
        viewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
