//
//  EmployeeSalaryUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 31/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class EmployeeSalaryUpdateViewController: UIViewController {

    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = EmployeeSalaryUpdatePresenter(service: EmployeeService())
    
    var datePicker = UIDatePicker()
    var salaryHistoryId : Int?
    
    var employeeSalaries : EmployeeSalaries?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.setUpToolBar()
        self.showStartDatePicker()
        self.configureTextFields()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.setUpInitialLanguage()
        self.amountLbl.text = LanguageManager.Amount
        self.dateLbl.text = LanguageManager.Date
        self.descriptionLbl.text = LanguageManager.Description
        guard let data = employeeSalaries else{
            return
        }
        self.amountTextField.text = data.amount
        self.dateTextField.text = dateFormatter(date: Date())
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    func setUpToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.amountTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        self.amountTextField.resignFirstResponder()
        self.dateTextField.becomeFirstResponder()
    }

}

extension EmployeeSalaryUpdateViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        dateTextField.text = dateFormatter(date: datePicker.date)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if amountTextField.text == "" {
            showAlert(title: LanguageManager.AmountIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let amount = self.amountTextField.text, let id = self.salaryHistoryId else{
                return
            }
            let date = self.dateTextField.text ?? ""
            let description = self.descriptionTextField.text ?? ""
            
            let param : [String: Any] = ["amount": amount, "date": date, "id": id, "description": description]
            self.presenter.postEmployeeSalaryUpdateDataToServer(param: param)
        }
    }
    
    func dateFormatter(date: Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = formatter.string(from: datePicker.date)
        return dateString
    }
    
}

extension EmployeeSalaryUpdateViewController : UITextFieldDelegate{
    func configureTextFields(){
        self.amountTextField.delegate = self
        self.dateTextField.delegate = self
        self.descriptionTextField.delegate = self
        self.amountTextField.underlined()
        self.dateTextField.underlined()
        self.descriptionTextField.underlined()
    }
}

extension EmployeeSalaryUpdateViewController : EmployeeSalaryUpdateViewDelegate{
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
    
}

extension EmployeeSalaryUpdateViewController{
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}

