//
//  EmployeeSalaryAddViewController.swift
//  Ponno
//
//  Created by a k azad on 31/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class EmployeeSalaryAddViewController: UIViewController {
    
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = EmployeeSalaryAddPresenter(service: EmployeeService())
    
    var employeeId: Int?
    var salaryStatus: Int?
    var datePicker = UIDatePicker()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
        self.showStartDatePicker()
        self.configureTextFields()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.setUpInitialLanguage()
        self.title = LanguageManager.Salary
        self.amountLbl.text = LanguageManager.Amount
        self.dateLbl.text = LanguageManager.Date
        self.dateTextField.text = dateFormatter(date: Date())
        self.statusLbl.text = LanguageManager.Status
        self.segmentController.setTitle(LanguageManager.Paid, forSegmentAt: 0)
        self.segmentController.setTitle(LanguageManager.Unpaid, forSegmentAt: 1)
        self.descriptionLbl.text = LanguageManager.Description
        self.salaryStatus = 1
        
        self.segmentController.addTarget(self, action: #selector(onStatusChange), for: .valueChanged)
        self.setSegmentTintColor(segment: segmentController)
        self.submitBtn.addTarget(self, action: #selector(onSubmit(sender:)), for: .touchUpInside)
    }
    
    @objc func onStatusChange(sender: UISegmentedControl){
        if segmentController.selectedSegmentIndex == 0{
            self.salaryStatus = 1
            self.hideDescription(isHidden: false)
        }else{
            self.salaryStatus = 0
            self.hideDescription(isHidden: true)
        }
    }
    
    func hideDescription(isHidden: Bool){
        self.descriptionLbl.isHidden = isHidden
        self.descriptionTextField.isHidden = isHidden
    }

}

extension EmployeeSalaryAddViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        dateTextField.text = dateFormatter(date: datePicker.date)
        //self.view.endEditing(true)
        dateTextField.resignFirstResponder()
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if amountTextField.text == "" {
            showAlert(title: LanguageManager.AmountIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let amount = self.amountTextField.text, let id = self.employeeId else{
                return
            }
            let date = self.dateTextField.text ?? ""
            let status = self.salaryStatus ?? 0
            let description = self.descriptionTextField.text ?? ""
            
            let param : [String: Any] = ["amount": amount, "date": date, "employee_id": id, "status": status, "description": description]
            self.presenter.postEmployeeSalaryAddDataToServer(param: param)
        }
    }
    
    func dateFormatter(date: Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = formatter.string(from: datePicker.date)
        return dateString
    }
    
}

extension EmployeeSalaryAddViewController : UITextFieldDelegate{
    func configureTextFields(){
        self.amountTextField.delegate = self
        self.dateTextField.delegate = self
        self.descriptionTextField.delegate = self
        self.amountTextField.underlined()
        self.dateTextField.underlined()
        self.descriptionTextField.underlined()
    }
}

extension EmployeeSalaryAddViewController: EmployeeSalaryAddViewDelegate{
    func onSuccessfullySalaryAdd(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

extension EmployeeSalaryAddViewController{
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}
