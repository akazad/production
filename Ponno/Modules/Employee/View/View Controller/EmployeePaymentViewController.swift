//
//  EmployeePaymentViewController.swift
//  Ponno
//
//  Created by a k azad on 31/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class EmployeePaymentViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = EmployeePaymentPresenter(service: EmployeeService())
    
    var employeeId: Int?
    var employeeUnpaidDataMapper: UnpaidSalaryDataMapper?
    var unpaidSalary : Double?
    
    var datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
        self.setUpToolBar()
        self.showStartDatePicker()
        self.configureTextFields()
        self.configureTableView()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.setUpInitialLanguage()
        self.title = LanguageManager.Payment
        self.amountLbl.text = LanguageManager.Amount
        self.dateLbl.text = LanguageManager.Date
        self.descriptionLbl.text = LanguageManager.Description
        
        self.dateTextField.text = dateFormatter(date: Date())
        self.amountTextField.addTarget(self, action: #selector(onAmountTextFieldEdititng), for: .editingChanged)
        self.submitBtn.addTarget(self, action: #selector(onSubmit(sender:)), for: .touchUpInside)
    }
    
    func setUpToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.amountTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        self.amountTextField.resignFirstResponder()
        self.dateTextField.becomeFirstResponder()
    }
    
    @objc func onAmountTextFieldEdititng(sender: UITextField){
        if let textFieldAmount = self.amountTextField.text?.toDouble(), let unpaidSalary = self.unpaidSalary{
            if textFieldAmount > unpaidSalary{
                self.showAlert(title: LanguageManager.PaidSalaryIsTooMuch, message: "")
                self.amountTextField.text = unpaidSalary.toString()
                return
            }
        }
    }

}

extension EmployeePaymentViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        dateTextField.text = dateFormatter(date: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if amountTextField.text == "" {
            showAlert(title: LanguageManager.AmountIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let amount = self.amountTextField.text, let id = self.employeeId else{
                return
            }
            let date = self.dateTextField.text ?? ""
            let description = self.descriptionTextField.text ?? ""
            
            let param : [String: Any] = ["amount": amount, "date": date, "employee_id": id, "description": description] 
            self.presenter.postEmployeePaymentDataToServer(param: param)
        }
    }
    
    func dateFormatter(date: Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = formatter.string(from: datePicker.date)
        return dateString
    }
    
}

extension EmployeePaymentViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UnpaidSalaryAmountCell.nib, forCellReuseIdentifier: UnpaidSalaryAmountCell.identifier)
        self.tableView.separatorStyle = .none
        //self.tableView.separatorColor = UIColor.clear
        self.tableView.backgroundColor = UIColor.init(red: 248, green: 226, blue: 228)
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 100
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UnpaidSalaryAmountCell = tableView.dequeueReusableCell(withIdentifier: UnpaidSalaryAmountCell.identifier, for: indexPath) as! UnpaidSalaryAmountCell
        
        if let unpaidData = self.employeeUnpaidDataMapper{
            cell.employeeUnpaidDataMapper = unpaidData
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
}

extension EmployeePaymentViewController : UITextFieldDelegate{
    func configureTextFields(){
        self.amountTextField.delegate = self
        self.dateTextField.delegate = self
        self.descriptionTextField.delegate = self
        self.amountTextField.underlined()
        self.dateTextField.underlined()
        self.descriptionTextField.underlined()
    }
}

extension EmployeePaymentViewController : EmployeePaymentViewDelegate{
    func setUnpaidSalary(data: UnpaidSalaryDataMapper) {
        self.employeeUnpaidDataMapper = data
        self.unpaidSalary = data.totalUnpaid
        self.refreshTableView()
    }
    
    func onSuccessfulPayment(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        if let id = self.employeeId{
            self.presenter.getEmployeeUnpaidSalariesFromServer(id: id)
        }
        
    }
}

extension EmployeePaymentViewController{
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}


