//
//  EmployeeSalaryUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 31/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol EmployeeSalaryUpdateViewDelegate : NSObjectProtocol {
    func onSuccess(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class EmployeeSalaryUpdatePresenter: NSObject {
    
    private let service : EmployeeService
    weak private var viewDelegate : EmployeeSalaryUpdateViewDelegate?
    
    init(service : EmployeeService) {
        self.service = service
    }
    
    func attachView(viewDelegate : EmployeeSalaryUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postEmployeeSalaryUpdateDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postEmployeeSalaryUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
