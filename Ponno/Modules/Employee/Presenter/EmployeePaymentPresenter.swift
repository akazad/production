//
//  EmployeePaymentPresenter.swift
//  Ponno
//
//  Created by a k azad on 31/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol EmployeePaymentViewDelegate : NSObjectProtocol {
    func setUnpaidSalary(data: UnpaidSalaryDataMapper)
    func onSuccessfulPayment(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class EmployeePaymentPresenter: NSObject {
    
    private let service : EmployeeService
    weak private var viewDelegate : EmployeePaymentViewDelegate?
    
    init(service : EmployeeService) {
        self.service = service
    }
    
    func attachView(viewDelegate : EmployeePaymentViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getEmployeeUnpaidSalariesFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getUnpaidSalariesOfEmployee(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setUnpaidSalary(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postEmployeePaymentDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postEmployeePaymentAddData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccessfulPayment(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
