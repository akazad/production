//
//  EmployeeSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 18/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol EmployeeSearchViewDelegate : NSObjectProtocol {
    func setEmployeSearchData(data : EmployeeDataMapper)
    func setEmployeeStatus(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class EmployeeSearchPresenter: NSObject {
    
    private let service : EmployeeService
    weak private var viewDelegate : EmployeeSearchViewDelegate?
    
    init(service : EmployeeService) {
        self.service = service
    }
    
    func attachView(viewDelegate : EmployeeSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getEmployeeSearchDataFromServer(page: Int, searchString: String){
        self.viewDelegate?.showLoading()
        self.service.getEmployeeSearchData(page: page, searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setEmployeSearchData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postEmployeeStatusUpdateData(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postEmployeeStatusUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setEmployeeStatus(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
