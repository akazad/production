//
//  EmployeeDetailsPreseneter.swift
//  Ponno
//
//  Created by a k azad on 17/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol EmployeeDetailsViewDelegate : NSObjectProtocol {
    func setEmployeeDetailsData(data : EmployeeDetailsDataMapper)
    func setEmployeeSaleHistoryData(data: EmployeeSaleHistoryDataMapper)
    func setEmployeeSalaryHistoryData(data: EmployeeSalaryHistoryDataMapper)
    func onSalaryHistoryDelete(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class EmployeeDetailsPresenter: NSObject {
    
    private let service : EmployeeService
    weak private var viewDelegate: EmployeeDetailsViewDelegate?
    
    init(service : EmployeeService) {
        self.service = service
    }
    
    func attachView(viewDelegate : EmployeeDetailsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getEmployeeDetailsFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getEmployeeDetailsData(id: id, success: { (data)  in 
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setEmployeeDetailsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    func getEmployeeSaleHistoryFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getEmployeeSaleHistoyrData(page: page, id: id, success: { (data) in 
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setEmployeeSaleHistoryData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    func getEmployeeSalaryHistoryFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getEmployeeSalaryHistoyrData(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setEmployeeSalaryHistoryData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postEmployeeSalaryDeleteDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postEmployeeSalaryDeleteData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSalaryHistoryDelete(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
