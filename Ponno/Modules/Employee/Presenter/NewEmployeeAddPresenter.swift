//
//  NewEmployeeAddPresenter.swift
//  Ponno
//
//  Created by a k azad on 7/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol NewEmployeeAddViewDelegate : NSObjectProtocol {
    func setEmployeePermissions(data: EmployeePermissionDataMapper)
    func setPermissions(data: PermissionsOfEmployeeDataMapper)
    func postNewEmployeeData(employeeData : EmployeeAddDataMapper)
    func updateEmployeeData(data: AddDataMapper)
    func onImageUpload(data : AddDataMapper)
    func onEmployeePermissionFailed(data: String)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}
class NewEmployeeAddPresenter: NSObject {
    
    private let service : EmployeeService
    weak private var viewDelegate : NewEmployeeAddViewDelegate?
    
    init(service : EmployeeService) {
        self.service = service
    }
    
    func attachView(viewDelegate : NewEmployeeAddViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getEmployeePermissionsDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getEmployeePermissionsData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setEmployeePermissions(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onEmployeePermissionFailed(data: message)
        })
    }
    
    func postNewEmployeeDataToServer(employeeData: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewEmployeeData(param: employeeData, success: { (employeeData) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.postNewEmployeeData(employeeData: employeeData)
        }, failure: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: data)
        })
    }
    
    func getPermissionsOfEmployeesFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getPermissionsOfEmployeesData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPermissions(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func updateEmployeeData(data: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postUpdateEmployeeData(param: data, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateEmployeeData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func uploadImage(employeeId : Int,image : UIImage?){
        self.viewDelegate?.showLoading()
        self.service.postEmployeeImage(employeeID: employeeId, image: image, succeed: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onImageUpload(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}


