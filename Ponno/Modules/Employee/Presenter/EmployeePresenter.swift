//
//  EmployeePresenter.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol EmployeeViewDelegate : NSObjectProtocol {
    func setEmployeList(data : EmployeeDataMapper)
    func setEmployeeStatus(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class EmployeePresenter: NSObject {
    
    private let service : EmployeeService
    weak private var viewDelegate : EmployeeViewDelegate?
    
    init(service : EmployeeService) {
        self.service = service
    }
    
    func attachView(viewDelegate : EmployeeViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getEmployeeDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getEmployeeData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setEmployeList(data: data)
        }, failure:  { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postEmployeeStatusUpdateData(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postEmployeeStatusUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setEmployeeStatus(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
