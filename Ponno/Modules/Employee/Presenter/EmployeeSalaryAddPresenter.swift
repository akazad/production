//
//  EmployeeSalaryAddPresenter.swift
//  Ponno
//
//  Created by a k azad on 31/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol EmployeeSalaryAddViewDelegate : NSObjectProtocol {
    func onSuccessfullySalaryAdd(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class EmployeeSalaryAddPresenter: NSObject {
    
    private let service : EmployeeService
    weak private var viewDelegate : EmployeeSalaryAddViewDelegate?
    
    init(service : EmployeeService) {
        self.service = service
    }
    
    func attachView(viewDelegate : EmployeeSalaryAddViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postEmployeeSalaryAddDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postEmployeeSalaryAddData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccessfullySalaryAdd(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}

