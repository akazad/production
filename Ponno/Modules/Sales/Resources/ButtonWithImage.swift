//
//  ButtonWithImage.swift
//  Ponno
//
//  Created by a k azad on 13/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class ButtonWithImage: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        left: (bounds.width - 35)
        if imageView != nil {
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)! + 4.0)
            imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 35), bottom: 5, right: 12)
            
        }
    }
}
