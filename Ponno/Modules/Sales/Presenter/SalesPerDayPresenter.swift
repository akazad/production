//
//  SalesPerDayPresenter.swift
//  Ponno
//
//  Created by a k azad on 22/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol SalesPerDayViewDelegate : NSObjectProtocol {
    func setSalesListData(data : SalePerDayDataMapper)
    func cancelPerDaySales(data: AddDataMapper)
    func onFailed(message : String)
    func showLoading()
    func hideLoading()
}
class SalesPerDayPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SalesPerDayViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SalesPerDayViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getSalesPerDayDataFromServer(page: Int, date : String){
        self.viewDelegate?.showLoading()
        self.service.getPerDaySalesData(page: page, date: date, success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSalesListData(data: data)
        }, failure: {(message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message : message)
        })
    }
    
    func cancelPerDaySaleData(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostPerDaySaleCancelData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.cancelPerDaySales(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message : message)
        })
    }
    
    func getSalesCustomerSearchDataFromServer(page: Int, customer : String){
        self.viewDelegate?.showLoading()
        self.service.getSalesCustomerSearchData(page: page, customer: customer, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSalesListData(data: data) 
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}
