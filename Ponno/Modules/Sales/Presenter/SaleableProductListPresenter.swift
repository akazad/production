//
//  SaleableProductListPresenter.swift
//  Ponno
//
//  Created by a k azad on 8/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

protocol SaleableProductListViewDelegate : NSObjectProtocol {
    func setSaleableProductData(data: ProductListDataMapper)
    func onDraftStoreSuccessful(data: AddDataMapper)
    func onPreOrderStoreSuccessful(data: AddDataMapper)
    func onFailed(failure : String)
    func showLoading()
    func hideLoading()
}

class SaleableProductListPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : SaleableProductListViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SaleableProductListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
//    func getProductSearchDataFromServer(page: Int, searchString: String){
//        self.viewDelegate?.showLoading()
//        self.service.getProductSearchData(page: page, searchString: searchString, success: { (data) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.setSaleableProductData(data: data)
//        }, failure: { (message) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onFailed(failure: message)
//        })
//    }
    
    func getAllProductDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllProductData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSaleableProductData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
    func postSaleDraftStoreDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        let service = SalesService()
        service.postSaleDraftData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDraftStoreSuccessful(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
    func postSalePreOrderDataStoreToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        let service = SalesService()
        service.postSalePreOrderData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onPreOrderStoreSuccessful(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
}
