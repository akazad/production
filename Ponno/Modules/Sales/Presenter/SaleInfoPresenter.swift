//
//  SaleInfoPresenter.swift
//  Ponno
//
//  Created by a k azad on 17/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol SaleInfoViewDelegate : NSObjectProtocol {
    func setCustomer(data: AllCustomerDataMapper)
    func onSuccess(data : AddDataMapper)
    func onFailed()
    func showLoading()
    func hideLoading()
}

class SaleInfoViewPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SaleInfoViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SaleInfoViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
//    func getCustomerNameFromServer(page: Int){
//        self.viewDelegate?.showLoading()
//        let service = CustomerService()
//        service.getCustomerData(page: page, success: { (data) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.setCustomerList(data: data)
//        }, failure: { (message) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onFailed()
//        })
//    }
    
    func getAllCustomerDataFromServer(searchString: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllCustomerData(searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCustomer(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
    
    func postSalesDataToServer(params : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewSalesData(params: params, success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
}

