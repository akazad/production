//
//  SaleDraftPresenter.swift
//  Ponno
//
//  Created by a k azad on 6/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

protocol SaleDraftViewDelegate : NSObjectProtocol {
    func setSaleDraftData(data : SaleDraftDataMapper)
    func onSaleDraftDelete(data: AddDataMapper)
    func onFailed(message : String)
    func showLoading()
    func hideLoading()
}
class SaleDraftPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SaleDraftViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SaleDraftViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getSaleDraftDataFromServer(page : Int){
        self.viewDelegate?.showLoading()
        self.service.getSaleDraftData(page : page , success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSaleDraftData(data: data)
        }, failure: {(message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func getSaleDraftDateSearchDataFromServer(page: Int, startDate: String, endDate: String){
        self.viewDelegate?.showLoading()
        self.service.getSaleDraftDateSearchData(page: page, startDate: startDate, endDate: endDate, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSaleDraftData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postSaleDraftDeleteToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postSaleDraftDeleteData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSaleDraftDelete(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}
