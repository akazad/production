//
//  SalePanelProductInfoViewPresenter.swift
//  Ponno
//
//  Created by a k azad on 9/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

protocol SalePanelProductInfoViewDelegate : NSObjectProtocol {
    func setPaymentMethod(data: PaymentMethodDataMapper)
    func onSuccess(data : AddDataMapper)
    func onDraftStoreSuccessful(data: AddDataMapper)
    func onSuccessfulPreOrderUpdate(data: AddDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}

class SalePanelProductInfoViewPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SalePanelProductInfoViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SalePanelProductInfoViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getPaymentMethodDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getPaymentMethodData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPaymentMethod(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postSalesDataToServer(params : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewSalesData(params: params, success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postSaleEditDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postSaleEditData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postSaleDraftStoreDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        let service = SalesService()
        service.postSaleDraftData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDraftStoreSuccessful(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postSalePreOrderUpdateDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postSalePreOrderUpdateData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccessfulPreOrderUpdate(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
//    func postSaleDraftStoreDataToServer(param: [String: Any]){
//        self.viewDelegate?.showLoading()
//        self.service.postSaleDraftData(params: param, success: { (data) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onDraftStoreSuccessful(data: data)
//        }, failure: {message in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onFailed(message: message)
//        })
//    }
    
}


