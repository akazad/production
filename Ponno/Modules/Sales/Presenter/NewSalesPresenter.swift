//
//  NewSalesPresenter.swift
//  Ponno
//
//  Created by a k azad on 16/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol NewSalesViewDelegate : NSObjectProtocol {
    func setNewSalesData(data : AddDataMapper)
    func onFailed(data: AddDataMapper)
    func showLoading()
    func hideLoading()
}
class NewSalesPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : NewSalesViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : NewSalesViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
//    func postNewSalesDataToServer(param : [String: Any]){
//        self.viewDelegate?.showLoading()
//        self.service.postNewSalesData(params: param, success: { (data) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.setNewSalesData(data: data)
//        }, failure: { (message) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onFailed(data: message)
//        })
//    }
    
}

