//
//  SalePreOrderInfoPresenter.swift
//  Ponno
//
//  Created by a k azad on 14/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

protocol SalePreOrderInfoViewDelegate : NSObjectProtocol {
    func onPreOrderStoreSuccessful(data: AddDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}

class SalePreOrderInfoPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SalePreOrderInfoViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SalePreOrderInfoViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postSalePreOrderDataStoreToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        let service = SalesService()
        service.postSalePreOrderData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onPreOrderStoreSuccessful(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}
