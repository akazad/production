//
//  SaleOnCustomerPresenter.swift
//  Ponno
//
//  Created by a k azad on 5/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol SaleOnCustomerViewDelegate : NSObjectProtocol {
    func setCustomerData(data: CustomerDataMapper)
    func onSuccess(data : AddDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}

class SaleOnCustomerPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SaleOnCustomerViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SaleOnCustomerViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getCustomerDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        let service = CustomerService()
        service.getCustomerData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCustomerData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
}
