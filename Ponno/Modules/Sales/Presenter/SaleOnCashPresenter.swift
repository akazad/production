//
//  SaleOnCashPresenter.swift
//  Ponno
//
//  Created by a k azad on 5/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol SaleOnCashViewDelegate : NSObjectProtocol {
    func setPaymentMethodData (data : PaymentMethodDataMapper)
    func onSuccess(data : AddDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}

class SaleOnCashPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SaleOnCashViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SaleOnCashViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getPaymentMethodDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getPaymentMethodData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPaymentMethodData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
}

