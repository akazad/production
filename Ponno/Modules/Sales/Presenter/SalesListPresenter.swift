//
//  SalesListPresenter.swift
//  Ponno
//
//  Created by a k azad on 21/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol SalesListViewDelegate : NSObjectProtocol {
    func setSalesListData(data : SalesDataMapper)
    func onFailed(message : String)
    func showLoading()
    func hideLoading()
}
class SalesListPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SalesListViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SalesListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getSalesDataFromServer(page : Int){
        self.viewDelegate?.showLoading()
        self.service.getSalesData(page : page , success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSalesListData(data: data)
        }, failure: {(message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func getSalesDateSearchDataFromServer(page: Int, startDate: String, endDate: String){
        self.viewDelegate?.showLoading()
        self.service.getSalesDateSearchData(page: page, startDate: startDate, endDate: endDate, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSalesListData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}


