//
//  SalesPerDaySearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 23/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol SalesPerDaySearchViewDelegate : NSObjectProtocol {
    func setSalesListData(data : SalePerDayDataMapper)
    func cancelPerDaySales(data: AddDataMapper)
    func onFailed(message : String)
    func showLoading()
    func hideLoading()
}
class SalesPerDaySearchPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SalesPerDaySearchViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SalesPerDaySearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getSalesPerDayDataFromServer(page: Int, date : String){
        self.viewDelegate?.showLoading()
        self.service.getPerDaySalesData(page: page, date: date, success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSalesListData(data: data)
        }, failure: {(message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message : message)
        })
    }
    
    func getSalesPerDaySearchDataFromServer(page: Int, date : String, searchText: String){
        self.viewDelegate?.showLoading()
        self.service.getPerDaySalesSearchData(page: page, date: date, searchText: searchText, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSalesListData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func cancelPerDaySaleData(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostPerDaySaleCancelData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.cancelPerDaySales(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message : message)
        })
    }
}
