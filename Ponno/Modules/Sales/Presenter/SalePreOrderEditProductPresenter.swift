//
//  SalePreOrderEditProductPresenter.swift
//  Ponno
//
//  Created by a k azad on 14/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

protocol SalePreOrderEditProductViewDelegate : NSObjectProtocol {
    func setSalePreOrderEditData(data : SalePreOrderEditDataMapper)
    func onFailed(message : String)
    func showLoading()
    func hideLoading()
}
class SalePreOrderEditProductPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SalePreOrderEditProductViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SalePreOrderEditProductViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getSalePreOrderEditDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getSalePreOrderEditData(iD: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSalePreOrderEditData(data: data) 
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}

