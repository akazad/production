//
//  SaleInfoViewPresenter.swift
//  Ponno
//
//  Created by a k azad on 17/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol SaleInfoViewDelegate : NSObjectProtocol {
    func setCustomer(data: AllCustomerDataMapper)
    func setPaymentMethod(data: PaymentMethodDataMapper)
    func onDraftStoreSuccessful(data: AddDataMapper)
    func onPreOrderStoreSuccessful(data: AddDataMapper)
    func onSuccess(data : AddDataMapper)
    func onCustomerFailed(message : String)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}

class SaleInfoViewPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SaleInfoViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SaleInfoViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getAllCustomerDataFromServer(searchString: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllCustomerData(searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCustomer(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCustomerFailed(message: message)
        })
    }
    
    func getPaymentMethodDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getPaymentMethodData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPaymentMethod(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postSalesDataToServer(params : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewSalesData(params: params, success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postSaleEditDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postSaleEditData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postSaleDraftStoreDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postSaleDraftData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDraftStoreSuccessful(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postSalePreOrderDataStoreToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        let service = SalesService()
        service.postSalePreOrderData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onPreOrderStoreSuccessful(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}

