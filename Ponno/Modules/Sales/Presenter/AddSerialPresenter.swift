//
//  SaleAddSerialPresenter.swift
//  Ponno
//
//  Created by a k azad on 28/11/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol ProductSerialSearchViewDelegate : NSObjectProtocol {
    func setProductSerialData(data: SerialSearchDataMapper)
    func setProductAllSerialData(data: ProductSerialDataMapper)
    func onFailed(failure : String)
    func showLoading()
    func hideLoading()
}

class AddSerialPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : ProductSerialSearchViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ProductSerialSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductSerialSearchDataFromServer(searchString: String){
        self.viewDelegate?.showLoading()
        self.service.getProductSerialSearchData(searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductSerialData(data: data)
        }, failure: {(message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
    func getAllSerialsDataFromServer(saleId: Int, productId: Int){
        self.viewDelegate?.showLoading()
        self.service.getProductAllSerials(saleId: saleId, productId: productId, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductAllSerialData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
}

