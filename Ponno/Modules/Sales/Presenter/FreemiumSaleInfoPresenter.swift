//
//  FreemiumSaleInfoPresenter.swift
//  Ponno
//
//  Created by a k azad on 21/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol FreemiumSaleInfoViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}

class FreemiumSaleInfoPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : FreemiumSaleInfoViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : FreemiumSaleInfoViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postSalesDataToServer(params : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewSalesData(params: params, success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
}


