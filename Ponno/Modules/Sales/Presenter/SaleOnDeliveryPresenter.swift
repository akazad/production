//
//  SaleOnDeliveryPresenter.swift
//  Ponno
//
//  Created by a k azad on 5/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol SaleOnDeliveryViewDelegate : NSObjectProtocol {
    func setDeliverySystemData(data : DeliveryDataMapper)
    func addDeliverySystemData(data: DeliverySystemAddDataMapper)
    func onSuccess(data : AddDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}

class SaleOnDeliveryPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SaleOnDeliveryViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SaleOnDeliveryViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getAllDeliverySystemDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        let service = DeliveryService()
        service.getAllDeliveryData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDeliverySystemData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postAddNewDeliverySystemDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        let service = DeliveryService()
        service.postNewDeliverySystemAddData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.addDeliverySystemData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
}
