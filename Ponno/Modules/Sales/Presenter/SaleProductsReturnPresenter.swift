//
//  SaleProductsReturnPresenter.swift
//  Ponno
//
//  Created by a k azad on 30/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol SaleProductsReturnViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}
class SaleProductsReturnPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SaleProductsReturnViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SaleProductsReturnViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postSaleProductReturnDataToServer(returnData: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postSaleProductsReturnData(params: returnData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
        
    }
    
    
}

