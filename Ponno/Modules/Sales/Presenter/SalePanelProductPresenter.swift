//
//  SalePanelProductPresenter.swift
//  Ponno
//
//  Created by a k azad on 2/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//


import UIKit

protocol SalesPanelProductViewDelegate : NSObjectProtocol {
    //func setProductList(data: ProductListDataMapper)
    func setSaleEditData(data : SaleEditDataMapper)
    func onFailed(message : String)
    func showLoading()
    func hideLoading()
}
class SalePanelProductPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SalesPanelProductViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SalesPanelProductViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getSalePanelProductDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getSaleEditData(iD: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSaleEditData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
//    func getAllProductsFromServer(getAll: Bool){
//        self.viewDelegate?.showLoading()
//        let service = ProductService()
//        service.getAllProductData(getAll: getAll, success: { (data) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.setProductList(data: data)
//        }, failure: {message in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onFailed(message: message)
//        })
//    }
    
}
