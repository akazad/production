//
//  SaleDraftProductPresenter.swift
//  Ponno
//
//  Created by a k azad on 10/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

protocol SaleDraftProductViewDelegate : NSObjectProtocol {
    func setSaleDraftEditData(data : SaleDraftEditDataMapper)
    func onDraftStoreSuccessful(data: AddDataMapper)
    func onSaleDraftDelete(data: AddDataMapper)
    func onFailed(message : String)
    func showLoading()
    func hideLoading()
}
class SaleDraftProductPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SaleDraftProductViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SaleDraftProductViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getSaleDraftEditDataFromServer(id : Int){
        self.viewDelegate?.showLoading()
        self.service.getSaleDraftEditData(iD: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSaleDraftEditData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
//    func getSalePreOrderEditDataFromServer(id: Int){
//        self.viewDelegate?.showLoading()
//        self.service.getSalePreOrderEditData(iD: id, success: { (data) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.setSaleDraftEditData(data: data)
//        }, failure: {message in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onFailed(message: message)
//        })
//    }
    
    func postSaleDraftStoreDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        let service = SalesService()
        service.postSaleDraftData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDraftStoreSuccessful(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postSaleDraftDeleteToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postSaleDraftDeleteData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSaleDraftDelete(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}
