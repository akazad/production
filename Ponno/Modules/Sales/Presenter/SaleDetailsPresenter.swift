//
//  SaleDetailsPresenter.swift
//  Ponno
//
//  Created by a k azad on 22/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol SaleDetailsViewDelegate : NSObjectProtocol {
    func setSaleDetailsData(data : SaleDetailsDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}
class SaleDetailsPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SaleDetailsViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SaleDetailsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getSaleDetailsData(id : Int){
        self.viewDelegate?.showLoading()
        self.service.getSaleDetailsData(iD: id, success: {(data) in 
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSaleDetailsData(data: data)
        }, failure: {(message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    
}

