//
//  SaleWIthCustomerPresenter.swift
//  Ponno
//
//  Created by a k azad on 14/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol SaleWithCustomerDelegate : NSObjectProtocol {
    func setCustomer(data: AllCustomerDataMapper)
    func addNewCustomer(data: CustomerAddDataMapper)
    func onFailed(failure: String)
    func showLoading()
    func hideLoading()
}

class SaleWithCustomerPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SaleWithCustomerDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SaleWithCustomerDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getAllCustomerDataFromServer(searchString: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllCustomerData(searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCustomer(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
    func postAddNewCustomerDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        let service = CustomerService()
        service.postNewCustomerData(customerDataArray: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.addNewCustomer(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
}
