//
//  SalePreOrderPresenter.swift
//  Ponno
//
//  Created by a k azad on 6/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

protocol SalePreOrderViewDelegate : NSObjectProtocol {
    func setSalePreOrderData(data : SalePreorderDataMapper)
    func onSalePreOrderDelete(data: AddDataMapper)
    func onFailed(message : String)
    func showLoading()
    func hideLoading()
}
class SalePreOrderPresenter: NSObject {
    
    private let service : SalesService
    weak private var viewDelegate : SalePreOrderViewDelegate?
    
    init(service : SalesService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SalePreOrderViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getSalePreOrderDataFromServer(page : Int){
        self.viewDelegate?.showLoading()
        self.service.getSalePreorderData(page : page , success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSalePreOrderData(data: data)
        }, failure: {(message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func getSalePreOrderSearchDataFromServer(page: Int, startDate: String, endDate: String){
        self.viewDelegate?.showLoading()
        self.service.getSalePreorderDateSearchData(page: page, startDate: startDate, endDate: endDate, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSalePreOrderData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postSalePreOrderDeleteToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postSalePreOrderDeleteData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSalePreOrderDelete(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}
