//
//  SalesPresenter.swift
//  Ponno
//
//  Created by a k azad on 16/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol SaleProductListViewDelegate : NSObjectProtocol {
    func setSaleProductData(data : ProductListDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}

class SalesPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : SaleProductListViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : SaleProductListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductListDataFromServer(page: Int) {
        self.viewDelegate?.showLoading()
        self.service.getProductListData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSaleProductData(data: data)
        }, failure: {(message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func getAllProductDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllProductData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setSaleProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}

