//
//  CustomerNameCell.swift
//  Ponno
//
//  Created by a k azad on 14/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class CustomerNameCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var phoneNoLbl: UILabel!
    @IBOutlet weak var cardView: UIView!{
        didSet{
            self.setUpCardView(uiview: cardView)
        }
    }
    
    var customer : AllCustomers?{
        didSet{
            if let item = self.customer{
                nameLbl.text = item.name + " (" + item.phone.suffix(3) + ")"
                phoneNoLbl.text = LanguageManager.Phone + " : " + item.phone
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier : String{
        return String(describing: CustomerNameCell.self)
    }
    
    
}
