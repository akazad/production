//
//  SaleDetailsDueHistoryCell.swift
//  Ponno
//
//  Created by a k azad on 8/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SaleDetailsDueHistoryCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var duePaidLbl: UILabel!
    @IBOutlet weak var duePaidLblHeight: NSLayoutConstraint!
    @IBOutlet weak var duePaid: UILabel!
    @IBOutlet weak var duePaidHeight: NSLayoutConstraint!
    @IBOutlet weak var invoiceLbl: UILabel!
    @IBOutlet weak var invoice: UILabel!
    @IBOutlet weak var dividerOne: UILabel!
    @IBOutlet weak var dividerOneHeight: NSLayoutConstraint!
    @IBOutlet weak var dividerTwo: UILabel!
    @IBOutlet weak var dividerTwoHeight: NSLayoutConstraint!
    @IBOutlet weak var remainingDueLbl: UILabel!
    @IBOutlet weak var remainingDueLblHeight: NSLayoutConstraint!
    @IBOutlet weak var remainingDue: UILabel!
    @IBOutlet weak var remainingDueHeight: NSLayoutConstraint!
    @IBOutlet weak var othersDueLbl: UILabel!
    @IBOutlet weak var othersDue: UILabel!
    @IBOutlet weak var latestDueLbl: UILabel!
    @IBOutlet weak var latestDue: UILabel!
    
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: SaleDetailsDueHistoryCell.self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
