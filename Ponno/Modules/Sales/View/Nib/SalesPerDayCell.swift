//
//  SalesPerDayCell.swift
//  Ponno
//
//  Created by a k azad on 22/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SalesPerDayCell: UITableViewCell {

    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var invoiceLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var dueLabel: UILabel!
    @IBOutlet weak var deliverySystemLabel: UILabel!
    @IBOutlet weak var customerNameLbl: UILabel!
    @IBOutlet weak var popUpBtn: UIButton!
    
    var deliveryStatus : Int = 0
    
    var salePerDay : SaleListDateWise?{
        didSet{
            setLabelTextColor(amount: salePerDay?.due.toDouble(), label: dueLabel)
        }
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: SalesPerDayCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        invoiceLabel.numberOfLines = 1
        invoiceLabel.lineBreakMode = .byTruncatingMiddle
        setUpCardView(uiview : backgroundCardView) 
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func setUpCardView(uiview : UIView){
//        uiview.backgroundColor = UIColor.white
//        //contentView.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 0.5)
//        uiview.layer.cornerRadius = 5.0
//        uiview.layer.masksToBounds = false
//
//        uiview.layer.shadowColor = (UIColor(red: 150/255, green: 150/255, blue: 150/255)).withAlphaComponent(0.5).cgColor
//        uiview.layer.shadowOffset = CGSize(width: 0, height: 0)
//        uiview.layer.shadowOpacity = 0.5
//    }
    
}




