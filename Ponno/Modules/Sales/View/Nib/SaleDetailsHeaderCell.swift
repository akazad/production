//
//  SaleDetailsHeaderCell.swift
//  Ponno
//
//  Created by a k azad on 7/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SaleDetailsHeaderCell: UITableViewCell {
    
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var invoiceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var paidLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var dueLabel: UILabel!
    @IBOutlet weak var paymentMethodLabel: UILabel!
    @IBOutlet weak var paymentMethodNumberLbl: UILabel!
    @IBOutlet weak var deliveryChargeLabel: UILabel!
    @IBOutlet weak var deliverySystemLabel: UILabel!
//    @IBOutlet weak var nameView: UIView!
//    
//    @IBOutlet weak var nameLabel: UILabel!
//    
//    @IBOutlet weak var customerNameLabel: UILabel!
//    @IBOutlet weak var customerPhoneLabel: UILabel!
//    @IBOutlet weak var customerAddressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier : String{
        return String(describing: SaleDetailsHeaderCell.self)
    }
}
