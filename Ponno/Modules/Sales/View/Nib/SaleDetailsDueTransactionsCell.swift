//
//  SaleDetailsDueTransactionsCell.swift
//  Ponno
//
//  Created by a k azad on 8/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SaleDetailsDueTransactionsCell: UITableViewCell {
    
    
    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var dateDetailsLbl: UILabel!
    @IBOutlet weak var paidAmount: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var payableAmountLbl: UILabel!
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: SaleDetailsDueTransactionsCell.self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
