//
//  SaleDetailsTotalCell.swift
//  Ponno
//
//  Created by a k azad on 8/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SaleDetailsTotalCell: UITableViewCell {
    
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: SaleDetailsTotalCell.self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
