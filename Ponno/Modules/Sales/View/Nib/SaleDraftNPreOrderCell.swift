//
//  SaleDraftNPreOrderCell.swift
//  Ponno
//
//  Created by a k azad on 7/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class SaleDraftNPreOrderCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            self.setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var customerLbl: UILabel!
    @IBOutlet weak var totalTitleLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var popUpBtn: UIButton!
    
    var saleDraftLists : SaleDraftLists?{
        didSet{
            if let draft = self.saleDraftLists{
                dateLbl.text = draft.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_h_mm_a.rawValue)
                
                customerLbl.text = LanguageManager.Customer + " : " + draft.customerName
                totalTitleLbl.text = LanguageManager.Total
                totalAmountLbl.text = Constants.currencySymbol + " " + draft.total
            }
        }
    }
    
    var salePreOrderLists : SalePreOrderLists?{
        didSet{
            if let preOrder = self.salePreOrderLists{
                dateLbl.text = preOrder.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_h_mm_a.rawValue)
                
                customerLbl.text = LanguageManager.Customer + " : " + preOrder.customerName
                totalTitleLbl.text = LanguageManager.Total
                totalAmountLbl.text = preOrder.total
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: SaleDraftNPreOrderCell.self)
    }
    
}
