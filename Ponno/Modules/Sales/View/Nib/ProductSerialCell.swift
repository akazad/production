//
//  ProductSerialCell.swift
//  Ponno
//
//  Created by a k azad on 17/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductSerialCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var serialNumber: UILabel!
    
    @IBOutlet weak var checkIcon: UIImageView!
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier : String{
        return String(describing: ProductSerialCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
