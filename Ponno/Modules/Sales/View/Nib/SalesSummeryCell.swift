//
//  SalesSummeryCell.swift
//  Ponno
//
//  Created by a k azad on 6/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SalesSummeryCell: UICollectionViewCell {

    
    @IBOutlet weak var SalesSummeryNumber: UILabel!
    @IBOutlet weak var SalesSummeryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier : String{
        return String(describing: SalesSummeryCell.self)
    }

}
