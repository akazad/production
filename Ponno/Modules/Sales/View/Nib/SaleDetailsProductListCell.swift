//
//  SaleDetailsProductListCell.swift
//  Ponno
//
//  Created by a k azad on 7/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SaleDetailsProductListCell: UITableViewCell {
    
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var perUnitPriceLabel: UILabel!
    @IBOutlet weak var totalPriceTitleLbl: UILabel!{
        didSet{
            totalPriceTitleLbl.text = LanguageManager.Total
        }
    }
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var popUpBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier : String{
        return String(describing: SaleDetailsProductListCell.self)
    }
}
