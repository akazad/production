//
//  SaleDetailsDeliveryStausCell.swift
//  Ponno
//
//  Created by a k azad on 2/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SaleDetailsDeliveryStausCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var deliveryStatusLbl: UILabel!
    @IBOutlet weak var deliveryStatus: UILabel!
    @IBOutlet weak var deliverySystemDueLbl: UILabel!
    @IBOutlet weak var deliverySystemDueAmountLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: SaleDetailsDeliveryStausCell.self)
    }
    
}
