//
//  SaleDetailsCustomerTableViewCell.swift
//  Ponno
//
//  Created by a k azad on 9/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SaleDetailsCustomerTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var customerPhoneLabel: UILabel!
    @IBOutlet weak var customerAddressLabel: UILabel!
    
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier : String{
        return String(describing: SaleDetailsCustomerTableViewCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
