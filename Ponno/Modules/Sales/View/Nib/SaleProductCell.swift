//
//  SaleProductCell.swift
//  Ponno
//
//  Created by a k azad on 2/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class SaleProductCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!{
        didSet{
            self.setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productVariantLabel: UILabel!
    @IBOutlet weak var productQuantityLabel: UILabel!
    @IBOutlet weak var productCategoryLabel: UILabel!
    @IBOutlet weak var productSellingPriceLabel: UILabel!
    @IBOutlet weak var soldQuantityLbl: UILabel!
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var warningBtn: UIButton!
    
    var saleableProduct : SaleProductList?{
        didSet{
            if let productItem = saleableProduct{
                if let productQuantity = productItem.quantity.toInt(){
                    if productQuantity <= 0{
                        productQuantityLabel.textColor = UIColor.red
                    }else{
                        productQuantityLabel.textColor = UIColor.black
                    }
                }
                
                soldQuantityLbl.isHidden = true
                warningBtn.isHidden = true
                
                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + productItem.image), placeholderImage: UIImage(named: "Product-1"))
                productNameLabel.text = String(describing: productItem.name)
                productVariantLabel.text = LanguageManager.Varient + " : " + productItem.variant
                
                selectBtn.isHidden = true
                
                if let category = getBusinessCategory(){
                    if category.businessCategory == 1{
                        productQuantityLabel.isHidden = true
                    }else{
                        productQuantityLabel.text = LanguageManager.Quantity + " : " + productItem.quantity + " " + productItem.unit
                    }
                }
                
                productCategoryLabel.text = LanguageManager.Category + " : " + productItem.categoryName
                
                if productItem.sellingPrice.isEmpty{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + "\(0.00)" + " " + Constants.currencySymbol
                }else{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + String(describing: productItem.sellingPrice) + " " + Constants.currencySymbol
                }

            }
        }
    }
    
    var productId: Int = -1
    
    var saleProducts : SaleProductList?{
        didSet{
            if let item = saleProducts{
                
                selectBtn.isHidden = false
                warningBtn.isHidden = true
                productQuantityLabel.textColor = UIColor.black
                if let category = getBusinessCategory(){
                    if category.businessCategory == 1{
                        productQuantityLabel.isHidden = true
                    }else{
                        productQuantityLabel.text = LanguageManager.Quantity + " : " + String(describing: item.quantity) + " " + item.unit
                    }
                }
                
                if soldQuantityAvailable(soldQuantity: String(describing: item.soldQuantity)) == false{
                    soldQuantityLbl.isHidden = true
                }else{
                    soldQuantityLbl.isHidden = false
                    soldQuantityLbl.text = String(describing: item.soldQuantity)
                }
                
                if item.soldSellingPrice.isEmpty{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + "\(0.00)" + " " + Constants.currencySymbol
                }else{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + String(describing: item.soldSellingPrice) + " " + Constants.currencySymbol
                }
                
                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + item.image), placeholderImage: UIImage(named: "Product-1"))
                productNameLabel.text = String(describing: item.name)
                productVariantLabel.text = LanguageManager.Varient + " : " + String(describing: item.variant)
                
                productCategoryLabel.text = LanguageManager.Category + " : " + String(describing: item.categoryName)

            }
        }
    }
    
    var editedProducts : EditedProducts?{
        didSet{
            if let item = editedProducts{
                
                selectBtn.isHidden = false
                
                if let productQuantity = item.quantity.toInt(){
                    if productQuantity < Int(item.soldQuantity){
                        productQuantityLabel.textColor = UIColor.red
                    }else{
                        productQuantityLabel.textColor = UIColor.black
                    }
                }
                
                if let sellingPrice = item.sellingPrice.toDouble(){
                    if item.soldSellingPrice < sellingPrice{
                        warningBtn.isHidden = false
                    }else{
                        warningBtn.isHidden = true
                    }
                }
                
                
                if let category = getBusinessCategory(){
                    if category.businessCategory == 1{
                        productQuantityLabel.isHidden = true
                    }else{
                        productQuantityLabel.text = LanguageManager.Quantity + " : " + String(describing: item.quantity) + " " + item.unit
                    }
                }
                
//                if soldQuantityAvailable(soldQuantity: String(describing: item.soldQuantity)) == false{
//                    soldQuantityLbl.isHidden = true
//                }else{
//                    soldQuantityLbl.isHidden = false
//                    soldQuantityLbl.text = "\(item.soldQuantity)"
//                }
                
                if soldQuantityAvailable(soldQuantity: item.soldQuantity) == false{
                    soldQuantityLbl.isHidden = true
                }else{
                    soldQuantityLbl.isHidden = false
                    soldQuantityLbl.text = "\(item.soldQuantity)"
                }
                
                if item.soldSellingPrice != 0{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + "\(0.00)" + " " + Constants.currencySymbol
                }else{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + String(describing: item.soldSellingPrice) + " " + Constants.currencySymbol
                }
                
                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + item.image), placeholderImage: UIImage(named: "Product-1"))
                productNameLabel.text = String(describing: item.name)
                productVariantLabel.text = LanguageManager.Varient + " : " + String(describing: item.variant)
                
                productCategoryLabel.text = LanguageManager.Category + " : " + String(describing: item.categoryName)

            }
        }
    }
    
    var editableProduct : EditedProducts?{
        didSet{
            if let productItem = editableProduct{
                if let productQuantity = productItem.quantity.toInt(){
                    if productQuantity <= 0{
                        productQuantityLabel.textColor = UIColor.red
                    }else{
                        productQuantityLabel.textColor = UIColor.black
                    }
                }
                
                warningBtn.isHidden = true
                
                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + productItem.image), placeholderImage: UIImage(named: "Product-1"))
                productNameLabel.text = String(describing: productItem.name)
                productVariantLabel.text = LanguageManager.Varient + " : " + productItem.variant
                
                selectBtn.isHidden = true
                
                if let category = getBusinessCategory(){
                    if category.businessCategory == 1{
                        productQuantityLabel.isHidden = true
                    }else{
                        productQuantityLabel.text = LanguageManager.Quantity + " : " + productItem.quantity + " " + productItem.unit
                    }
                }
                
                productCategoryLabel.text = LanguageManager.Category + " : " + productItem.categoryName
                
//                if soldQuantityAvailable(soldQuantity: productItem.soldQuantity.toString()) == false{
//                    soldQuantityLbl.isHidden = true
//                }else{
//                    soldQuantityLbl.isHidden = false
//                    soldQuantityLbl.text = "\(productItem.soldQuantity)"
//                }
                
                if soldQuantityAvailable(soldQuantity: productItem.soldQuantity) == false{
                    soldQuantityLbl.isHidden = true
                }else{
                    soldQuantityLbl.isHidden = false
                    soldQuantityLbl.text = "\(productItem.soldQuantity)"
                }
                
                if productItem.sellingPrice.isEmpty{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + "\(0.00)" + " " + Constants.currencySymbol
                }else{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + String(describing: productItem.sellingPrice) + " " + Constants.currencySymbol
                }

            }
        }
    }
    
    var preOrderEditedProducts : SalePreOrderEditProducts?{
        didSet{
            if let item = preOrderEditedProducts{
                
                selectBtn.isHidden = false
                
                if item.quantity < item.soldQuantity{
                    productQuantityLabel.textColor = UIColor.red
                }else{
                    productQuantityLabel.textColor = UIColor.black
                }
                
                if item.soldSellingPrice < item.sellingPrice{
                    warningBtn.isHidden = false
                }else{
                    warningBtn.isHidden = true
                }
                let serialArr = item.serials.components(separatedBy: ",")
                if item.hasSerial != 0{
                    if Int(item.soldQuantity) > serialArr.count{
                        warningBtn.isHidden = false
                    }else{
                        warningBtn.isHidden = true
                    }
                }
                
                
                if let category = getBusinessCategory(){
                    if category.businessCategory == 1{
                        productQuantityLabel.isHidden = true
                    }else{
                        productQuantityLabel.text = LanguageManager.Quantity + " : " + String(describing: item.quantity) + " " + item.unit
                    }
                }
                
                if soldQuantityAvailable(soldQuantity: String(describing: item.soldQuantity)) == false{
                    soldQuantityLbl.isHidden = true
                }else{
                    soldQuantityLbl.isHidden = false
                    soldQuantityLbl.text = item.soldQuantity.toString()
                }
                
                if item.soldSellingPrice == 0{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + "\(0.00)" + " " + Constants.currencySymbol
                }else{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + String(describing: item.soldSellingPrice) + " " + Constants.currencySymbol
                }
                
                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + item.image), placeholderImage: UIImage(named: "Product-1"))
                productNameLabel.text = String(describing: item.name)
                productVariantLabel.text = LanguageManager.Varient + " : " + String(describing: item.variant)
                
                productCategoryLabel.text = LanguageManager.Category + " : " + String(describing: item.categoryName)

            }
        }
    }
    
    var preOrderEditableProduct : SalePreOrderEditProducts?{
        didSet{
            if let productItem = preOrderEditableProduct{
                if productItem.quantity <= 0{
                    productQuantityLabel.textColor = UIColor.red
                }else{
                    productQuantityLabel.textColor = UIColor.black
                }
                
                warningBtn.isHidden = true
                soldQuantityLbl.isHidden = true
                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + productItem.image), placeholderImage: UIImage(named: "Product-1"))
                productNameLabel.text = String(describing: productItem.name)
                productVariantLabel.text = LanguageManager.Varient + " : " + productItem.variant
                
                selectBtn.isHidden = true
                
                if let category = getBusinessCategory(){
                    if category.businessCategory == 1{
                        productQuantityLabel.isHidden = true
                    }else{
                        productQuantityLabel.text = LanguageManager.Quantity + " : " + "\(productItem.quantity)" + " " + productItem.unit
                    }
                }
                
                productCategoryLabel.text = LanguageManager.Category + " : " + productItem.categoryName
                
                if productItem.sellingPrice == 0{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + "\(0.00)" + " " + Constants.currencySymbol
                }else{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + String(describing: productItem.sellingPrice) + " " + Constants.currencySymbol
                }

            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        soldQuantityLbl.backgroundColor = UIColor.white.withAlphaComponent(0.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: SaleProductCell.self)
    }
    
}
