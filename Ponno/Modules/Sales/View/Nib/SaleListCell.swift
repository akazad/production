//
//  SaleListCell.swift
//  Ponno
//
//  Created by a k azad on 28/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SaleListCell: UITableViewCell {
    
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var daySaleLabel: UILabel!
    @IBOutlet weak var daySaleAmountLabel: UILabel!
    @IBOutlet weak var dayDueLabel: UILabel!
    
    var salesData : SalesListData? {
        didSet{
            setViewColor(amount: salesData?.dayDue.toDouble(), view: colorView)
        }
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier : String{
        return String(describing: SaleListCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        setUpCardView(uiview)
        setUpCardView(uiview: cardView)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
//    func setUpCardView(){
//        cardView.layer.shadowOpacity = 0.10
//        cardView.layer.shadowColor = UIColor.black.cgColor
//    }
}
