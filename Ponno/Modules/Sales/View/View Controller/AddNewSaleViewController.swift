//
//  AddNewSaleViewController.swift
//  Ponno
//
//  Created by a k azad on 12/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

struct SaleObject {
    static var invoice :String?
    static var products: [Int]?
    static var quantities : [Int]?
    static var prices : [Double]?
    static var serials : [String]?
    static var totalAmount : Double?
    static var discount : Double?
    static var discountUnit : String?
    static var payableAmount : Double?
    static var cash : Double?
    static var due : Double?
    static var cashback : Double?
    static var deliverySystem : String?
    static var deliveryCharge : Double?
    static var paymentMethodId : String?
    static var customerId : Int?
    
    static func resetBundleData(){
        self.invoice = nil
        self.products = nil
        self.quantities = nil
        self.prices = nil
        self.serials = nil
        self.totalAmount = nil
        self.discount = nil
        self.discountUnit = nil
        self.cash = nil
        self.due = nil
        self.cashback  = nil
        self.deliverySystem = nil
        self.paymentMethodId = nil
        self.customerId = nil
    }
}

struct SaleStatusObject {
    static var isDraft: Bool = false
    static var isPreOrder: Bool = false
}

class AddNewSaleViewController: UIViewController {

    //@IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var stackView: UIStackView!{
        didSet{
            //self.setUpCardView(uiview: st)
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var preOrderLbl: UILabel!{
        didSet{
            preOrderLbl.text = LanguageManager.PreOrder
        }
    }
    @IBOutlet weak var preOrderBtn: UIButton!{
        didSet{
            self.setUpCardView(uiview: preOrderBtn)
            preOrderBtn.setImage(UIImage(named: "uncheck_icon"), for: .normal)
            let preOrder = LanguageManager.PreOrder
            let atPreOrder = NSAttributedString(string: preOrder)
            self.preOrderBtn.setAttributedTitle(atPreOrder, for: .normal)
        }
    }
    @IBOutlet weak var refreshBtn: UIButton!{
        didSet{
            self.setUpCardView(uiview: refreshBtn)
            
            let refresh = LanguageManager.Refresh
            let atRefresh = NSAttributedString(string: refresh)
            self.refreshBtn.setAttributedTitle(atRefresh, for: .normal)
        }
    }
    @IBOutlet weak var nextView: UIView!{
        didSet{
            self.setUpCardView(uiview: nextView)
        }
    }
    @IBOutlet weak var nextBtn: UIButton!{
        didSet{
            self.nextBtn.setTitle(LanguageManager.Next, for: .normal)
        }
    }
    @IBOutlet weak var draftView: UIView!{
        didSet{
            self.setUpCardView(uiview: draftView)
        }
    }
    @IBOutlet weak var draftBtn: UIButton!{
        didSet{
            self.draftBtn.setTitle(LanguageManager.Draft, for: .normal)
        }
    }
    private var presenter = SaleableProductListPresenter(service: ProductService())
        
    var isLoading : Bool = false
    var currentPage : Int = 1
    var productList : [ProductList] = []
    
    var selectedProductList : [ProductList] = []{
        didSet{
            if self.selectedProductList.count > 0{
                self.selectedProductList = self.selectedProductList.sorted(by: { $0.soldQuantity > $1.soldQuantity})
            }
            //refreshTableView()
        }
    }
    
    var selectableProductList : [ProductList] = []{
        didSet{
            refreshTableView()
        }
    }
    
    var filteredList : [ProductList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedSellingPrices : [Double] = []
    var selectedSerials : [String] = []
    
    var selectedProduct : ProductList?
    var selectedProductName : String?
    
    var crossImage = UIImage(named: "error_red.png")
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    //var filteredList : [ProductList] = []
    
    //var lastPageNo: Int = 0
    //var getAll : Bool = true
    
    var sellingPrice : Double?
    var saleableQuantity : Double?
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 5, height: 44))
    
    var timer : Timer?
    
    
    var searchText = ""
    var lastPage : Int = 0
    
    var isDraftEnable: Bool = false
    var isPreOrderEnable: Bool = false
    
    //CodeScan
    //var codeScannerActive: Bool?
//    var searchCode : String?{
//        didSet{
//            self.currentPage = 1
//            self.codeScannerActive = true
//            self.productList = []
//            self.isLoading = true
//            //self.isSearchActive = true
//            if let serial = searchCode{
//                self.presenter.getProductSearchDataFromServer(page: self.currentPage, searchString: serial)
//            }
//        }
//    }
    //var codeSearchProduct : ProductList?
//    {
//        didSet{
//            if let data = codeSearchProduct, isObjectNotNil(object: data){
//                self.selectedProduct = data
//                self.sellingPrice = Double(data.sellingPrice)
//
//                let quantity = data.quantity
//                if quantity.isEmpty == false && quantity > "0.00"{
//                    if data.hasSerial == 0{
//                        self.navigateToSaleAddVC()
//                    }
//                    else{
//                        self.navigateToSaleAddWithSerialVC(serials: data.serials)
//                    }
//                }else{
//                    self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
//                }
//            }
//        }
//    }
    
    //let checkedIcon = UIImage(named: "check_icon")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableVIew()
        self.setUpSearchBar()
        self.setUpViews()
        self.resetBundleData()
        self.setupTopButtonsBtn()
        self.attachPresenter()
        //self.searchBar.becomeFirstResponder()
    }
    
    func resetBundleData(){
        SaleObject.resetBundleData()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext(sender:)), for: .touchUpInside)
        self.draftBtn.addTarget(self, action: #selector(onDraftBtnTapped(sender:)), for: .touchUpInside)
        self.preOrderBtn.addTarget(self, action: #selector(onPreOrderBtnTapped(sender:)), for: .touchUpInside)
        self.refreshBtn.addTarget(self, action: #selector(onRefreshBtnTapped), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setUpInitialLanguage()
        self.setNavigationBar()
        self.setupRefreshBtn()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.resignFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    func setupTopButtonsBtn(){
        
        preOrderBtn.isSelected = true
        
        let spacing: CGFloat = 8.0
        refreshBtn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: spacing)
        refreshBtn.imageEdgeInsets = UIEdgeInsets(top:8, left:-12, bottom:8, right:0)
        
        preOrderBtn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: spacing)
        preOrderBtn.imageEdgeInsets = UIEdgeInsets(top:8, left:-4, bottom:8, right:0)
        
        preOrderBtn.imageView?.contentMode = .scaleAspectFit
        refreshBtn.imageView?.contentMode = .scaleAspectFit
        
    }
    
//    func setupScannerSearch(){
//        if codeScannerActive == true{
//            if let data = codeSearchProduct, isObjectNotNil(object: data){
//                self.selectedProduct = data
//                self.sellingPrice = Double(data.sellingPrice)
//
//                let quantity = data.quantity
//                if quantity.isEmpty == false && quantity > "0.00"{
//                    if data.hasSerial == 0{
//                        self.navigateToSaleAddVC()
//                    }
//                    else{
//                        self.navigateToSaleAddWithSerialVC(serials: data.serials)
//                    }
//                }else{
//                    self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
//                }
//            }
//        }
//    }
    
    @objc func onPreOrderBtnTapped(sender : UIButton){
        if preOrderBtn.isSelected {
            if self.selectedProductId.isEmpty{
                preOrderBtn.setImage(UIImage(named: "check_icon"), for: .normal)
                SaleStatusObject.isPreOrder = true
                self.isPreOrderEnable = true
                //self.isDraftEnable = false
                //self.refreshBtn.isEnabled = true
                refreshBtnStatus(status: true)
            }
        } else {
            if self.selectedProductId.isEmpty{
                preOrderBtn.setImage(UIImage(named: "uncheck_icon"), for: .normal)
                SaleStatusObject.isPreOrder = false
                self.isPreOrderEnable = false
                refreshBtnStatus(status: false)
            }
            
        }
        preOrderBtn.isSelected = !preOrderBtn.isSelected
        self.setupSaleStatus()
        self.setupSaveAsDraftBtn()
        self.refreshTableView()
        
    }
    
    @objc func onRefreshBtnTapped(sender: UIButton){
        if self.selectedProductId.count > 0{
            for id in self.selectedProductId{
                let product = self.findProductInfo(productId: id)
                let quantity = product?.quantity
                self.addDataToSelectableProductList(productId: id, quantity: quantity ?? "0.00")
            }
            self.selectedProductId.removeAll()
            self.selectedAmounts.removeAll()
            self.selectedSerials.removeAll()
            self.selectedSellingPrices.removeAll()
        }
        self.selectedProductList = []
        preOrderBtn.setImage(UIImage(named: "uncheck_icon"), for: .normal)
        SaleStatusObject.isPreOrder = false
        self.setupSaveAsDraftBtn()
        self.setupRefreshBtn()
        self.isPreOrderEnable = false
        self.refreshTableView()
        
    }
    
    func setupSaveAsDraftBtn(){
        if SaleStatusObject.isPreOrder == true{
            self.draftBtn.isHidden = true
            self.draftView.isHidden = true
        }else{
            self.draftBtn.isHidden = false
            self.draftView.isHidden = false
        }
    }
    
    func setupRefreshBtn(){
        if self.selectedProductId.count > 0{
            refreshBtnStatus(status: true)
        }else{
            refreshBtnStatus(status: false)
        }
    }
    
    func refreshBtnStatus(status: Bool){
        if status == true{
            self.refreshBtn.isEnabled = true
            self.refreshBtn.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0), for: .normal)
            self.refreshBtn.backgroundColor = UIColor.init(red: 255, green: 255, blue: 255)
        }else{
            self.refreshBtn.isEnabled = false
            self.refreshBtn.setTitleColor(UIColor.init(red: 189, green: 189, blue: 189), for: .normal)
            self.refreshBtn.backgroundColor = UIColor.init(red: 234, green: 234, blue: 234)
        }
    }
    
    @objc func onDraftBtnTapped(sender: UIButton){
        if self.selectedProductId.count > 0{
                    
            guard self.selectedAmounts.count > 0 else { return }
            guard self.selectedSerials.count > 0 else { return }
            guard self.selectedSellingPrices.count > 0 else { return }
            
            // সর্বমোট
            let totalPrice = self.totalSelligPrice(amounts: self.selectedAmounts, sellingPrices: self.selectedSellingPrices)
            
            let params : [String : Any] = ["draft_id": "", "products" : self.selectedProductId, "quantities" : self.selectedAmounts, "prices": self.selectedSellingPrices, "serials": self.selectedSerials, "total_amount": totalPrice, "discount": "", "discount_unit": "", "payable_amount" : "" , "cash" : "", "due" : "" , "cash_back" : "", "delivery_system" : "", "delivery_charge" : "" , "payment_method" : "", "payment_number": "", "customer" : "", "date": "", "wallet": "", "step_no": 1, "sale_token": ""]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
//            if SaleStatusObject.isPreOrder == true{
//                self.presenter.postSalePreOrderDataStoreToServer(param: params)
//            }else{
//
//            }
            self.presenter.postSaleDraftStoreDataToServer(param: params)
        }
    }
    
    @objc func onNext(sender : UIButton){
//        guard let product = self.selectedProduct else {
//            return
//        }
//        print(self.selectedProductId)
        if self.selectedProductId.count > 0{
            if isPreOrderEnable == true{
                self.navigateToSalePreOrderInfoViewController()
            }else{
                self.navigateToSaleInfoViewController()
            }
        }
    }
    
    func navigateToSaleInfoViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleInfoViewController") as! SaleInfoViewController
        
        viewController.selectedProductId = self.selectedProductId
        viewController.selectedAmounts = selectedAmounts
        viewController.selectedSellingPrices = selectedSellingPrices
        viewController.selectedSerials = selectedSerials
        viewController.isDraftEnable = self.isDraftEnable
        
         self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSalePreOrderInfoViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalePreOrderInfoViewController") as! SalePreOrderInfoViewController
        
        viewController.selectedProductId = self.selectedProductId
        viewController.selectedAmounts = selectedAmounts
        viewController.selectedSellingPrices = selectedSellingPrices
        viewController.selectedSerials = selectedSerials
        //viewController.isDraftEnable = self.isDraftEnable
        
         self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleInfoVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleInfoVC") as! SaleInfoVC
        
        viewController.selectedAmounts = selectedAmounts
        viewController.selectedSellingPrices = selectedSellingPrices
        viewController.selectedSerials = selectedSerials
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func removeDraftProducts(){
        for item in self.selectedAmounts{
            if item == 0{
                let productId = self.findIdOfAnItem(item: item)
                self.removeDataofProduct(productId: productId)
            }
        }
    }
    
    func findIdOfAnItem(item: Double) -> Int{
        var productId = -1
//        for index in 0..<self.selectedAmounts.count{
//            if self.selectedAmounts[index] == item{
//                productId = self.selectedProductId[index]
//            }
//        }
        if let index = selectedAmounts.firstIndex(of: item) {
            productId = selectedProductId[index]
        }
        return productId
    }
    
    func removeZeroStockPreOrderProduct(){
        for item in self.productList{
            if let quantity = item.quantity.toDouble(){
                if quantity <= 0{
                    let productId = item.productId
                    if self.selectedProductId.contains(productId){
                        self.removeDataofProduct(productId: productId)
                    }
                }
            }
        }
    }
    
    func setupSaleStatus(){
        if SaleStatusObject.isDraft == true{
            self.isDraftEnable = true
            self.isPreOrderEnable = false
        }
        if SaleStatusObject.isPreOrder == true{
            self.isPreOrderEnable = true
            self.isDraftEnable = false
        }
        self.refreshTableView()
    }
    
    
}


//MARK: SearchBar Delegate
extension AddNewSaleViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.ProductSearch
        self.navigationItem.titleView = searchBar
        //CodeScan
        self.searchBar.showsBookmarkButton = false
        self.searchBar.sizeToFit()
        //self.searchBar.setImage(UIImage(named: "scan"), for: .bookmark, state: .normal)
        self.searchBar.setPositionAdjustment(UIOffset(horizontal: -4, vertical: 2), for: .bookmark)
        //self.codeScannerActive = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if self.selectableProductList.count > 0 {
            if searchText == "" {
                self.isSearchActive = false
                return
            }else{
                isSearchActive = true
            }
        }
        
        guard let textToSearch = searchBar.text else {
            return
        }
        
        if textToSearch.count > 1{
            filteredList = self.selectableProductList.filter { (product : ProductList) -> Bool in
                return product.name.lowercased().contains(textToSearch.lowercased())  || product.categoryName.lowercased().contains(textToSearch.lowercased()) || product.variant.lowercased().contains(textToSearch.lowercased())
                
            }
        }
        
    }
    
    //CodeScan
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        //print("click")
        //navigateToScannerViewController()
    }
    
    func navigateToScannerViewController(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Scanner", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ScannerViewController") as! ScannerViewController
        viewController.navigateTo = Navigate.sale
        viewController.modalPresentationStyle = .fullScreen
        viewController.delegate = self
        viewController.productDelegate = self
        //1
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

//MARK: TableView Delegate and Data Source
extension AddNewSaleViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableVIew(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductListCell.nib, forCellReuseIdentifier: ProductListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if isSearchActive == true{
                return 0
            }else{
                if self.selectedProductList.count > 0 {
                    return self.selectedProductList.count
                }
                return 0
            }
            
        case 1:
            if isSearchActive{
                guard self.filteredList.count > 0 else {
                    return 0
                }
                return self.filteredList.count
            }else{
                if self.selectableProductList.count > 0 {
                    return self.selectableProductList.count
                }
                return 0
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductListCell = tableView.dequeueReusableCell(withIdentifier: ProductListCell.identifier, for: indexPath) as! ProductListCell
        cell.selectionStyle = .none
        
        switch indexPath.section {
            
        case 0:
            if self.selectedProductList.count > 0 {
                let product = self.selectedProductList[indexPath.row]
                
                
                self.saleableQuantity = product.quantity.toDouble()
                cell.selectedProduct = product
                
                cell.selectBtn.tag = product.productId
                cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
            }
            
        case 1:
            if isSearchActive{
                if self.filteredList.count > 0{
                    let product = self.filteredList[indexPath.row]
                    
                    cell.selectableProduct = product
                    if self.isPreOrderEnable == true{
                        cell.productQuantityLabel.textColor = UIColor.black
                    }
                }
            }else{
                if self.selectableProductList.count > 0 {
                    let product = self.selectableProductList[indexPath.row]
                    
                    cell.selectableProduct = product
                    if self.isPreOrderEnable == true{
                        cell.productQuantityLabel.textColor = UIColor.black
                    }
                }
            }
        default:
            return cell
        }
        
        return cell
        
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    //
    @objc func onRemove(sender : UIButton){
        let productId = sender.tag
        
        if self.selectedProductId.contains(productId){
            for item in self.selectedProductList{
                if item.productId == productId{
                    let soldQuantity = item.soldQuantity
                    removeDataFromSelectedProducts(productId: productId, quantity: "\(soldQuantity)")
                    removeDataofProduct(productId : productId)
                }
            }
        }
    }
    
    func removeDataFromSelectedProducts(productId : Int, quantity: String){
        self.selectedProductList = self.selectedProductList.filter{ $0.productId != productId}
        self.addDataToSelectableProductList(productId: productId, quantity: quantity)
        self.refreshTableView()
    }
    
    func removeDataofProduct(productId : Int){
        var productAtIndex = -1
        for index in 0..<self.selectedProductId.count{
            if self.selectedProductId[index] == productId{
                productAtIndex = index
            }
        }
        
        self.selectedSellingPrices.remove(at: productAtIndex)
        self.selectedAmounts.remove(at: productAtIndex)
        self.selectedSerials.remove(at: productAtIndex)
        self.selectedProductId.remove(at: productAtIndex)
        self.refreshTableView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            if self.selectedProductList.count > 0 {
                let data = self.selectedProductList[indexPath.row]
                
                self.didSelectOnSelectedProductList(data: data)
            }
        case 1:
            if isSearchActive{
                if self.filteredList.count > 0{
                    let data = self.filteredList[indexPath.row]
                    
                    self.didSelectOnSelectableProductList(data: data)
                }
            }else{
                if self.selectableProductList.count > 0 {
                    let data = self.selectableProductList[indexPath.row]
                    
                    self.didSelectOnSelectableProductList(data: data)
                }
            }
        default:
            return
        }
        
    }
    
    func didSelectOnSelectedProductList(data: ProductList){
        self.sellingPrice = Double(data.soldSellingPrice)
        self.saleableQuantity = Double(data.soldQuantity)
        
        for item in self.selectedProductList{
            if item.productId == data.productId{
                self.selectedProduct = item
            }
        }
        
        if self.isPreOrderEnable == true{
            self.navigateToSaleAddVC()
        }else{
            let quantity = data.quantity
            if quantity.isEmpty == false && quantity != "0.00"{
                if data.hasSerial == 0{
                    self.navigateToSaleAddVC()
                }
                else{
                    self.navigateToSaleAddWithSerialVC(data: data)
                }
            }else{
                self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
            }
        }
        
    }
        
        func didSelectOnSelectableProductList(data: ProductList){
            self.selectedProductName = data.name
            self.selectedProduct = data
            self.saleableQuantity = 1.0
            self.sellingPrice = Double(data.sellingPrice)
            
            if self.isPreOrderEnable == true{
                self.navigateToSaleAddVC()
            }else{
                let quantity = data.quantity
                if quantity.isEmpty == false && quantity != "0.00"{
                    if data.hasSerial == 0{
                        self.navigateToSaleAddVC()
                    }
                    else{
                        self.navigateToSaleAddWithSerialVC(data: data)
                    }
                }else{
                    self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
                }
            }
        }
    
    
    func navigateToSaleAddVC(){
        let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSecondVC") as! SaleAddSecondVC
        
        viewController.delegate = self
        viewController.saleAblePrice = self.sellingPrice
        viewController.selectedProductName = self.selectedProductName
        viewController.saleAbleQuantity = self.saleableQuantity
        viewController.isPreOrderEnable = self.isPreOrderEnable
        
        viewController.product = self.selectedProduct
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleAddSerialVC(data: ProductList){
        let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSerialViewController") as! SaleAddSerialViewController
        if self.selectedProductList.count > 0{
            
            viewController.saleAblePrice = self.sellingPrice
            viewController.selectedProductName = self.selectedProductName
            viewController.delegate = self
            viewController.productSerials = data.soldSerialNo
            viewController.productId = data.productId
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleAddWithSerialVC(data: ProductList){
        if self.prepareSerialNumbers(serials: data.serials).count > 0{
            let serialList = self.prepareSerialNumbers(serials: data.serials)
            
            let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSerialViewController") as! SaleAddSerialViewController
            
            viewController.serialList = serialList
            viewController.saleAblePrice = self.sellingPrice
            viewController.delegate = self
            viewController.selectedProductName = self.selectedProductName
            viewController.isCodeScannerActive = false
            viewController.productSerials = data.soldSerialNo
//            if codeScannerActive == true{
//                viewController.isCodeScannerActive = true
//                viewController.searchSerial = self.searchText
//            }else{
//                viewController.isCodeScannerActive = false
//            }
            
            //viewController.isCodeScannerActive = self.codeScannerActive
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
       
    }
    
//    func prepareSerialNumbers(serials : String)->[String]{
//        guard serials != "" else {
//            return []
//        }
//        
//        let serialList = serials.components(separatedBy: ",")
//        
//        guard serialList.count > 0 else {
//            return []
//        }
//        return serialList
//    }
    
    
}

extension AddNewSaleViewController: SaleableProductListViewDelegate{
    func onDraftStoreSuccessful(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.showMessage(userMessage: message)
    }
    
    func onPreOrderStoreSuccessful(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.showMessage(userMessage: message)
    }
    
//    func setProductSearchData(data: ProductListDataMapper) {
//        guard let list = data.productList, list.count > 0 else {
//            return
//        }
////        if codeScannerActive == true{
////            //if codeScannerActive == true{
////
////            //}
////            self.codeSearchProduct = list[0]
////            self.setupScannerSearch()
////        }
//
////        for item in list{
////            print(selectedProductId)
////            if self.selectedProductId.contains(item.productId){
////                let product = item
////                list.filter{($0.productId == product.productId)}
////                if list[0].productId != item.productId{
////                    list.insert(product, at: 0)
////                }
////
////            }
////
////        }
//
//        self.productList += list
//        self.refreshTableView()
//
//
//        guard let pagination = data.pagination else {
//            return
//        }
//        self.lastPage = pagination.lastPageNo
//
//        //print(lastPage)
//    }
    
    func setSaleableProductData(data: ProductListDataMapper) {
        guard let list = data.productList, list.count > 0 else{
            return
        }
        self.productList = list
        
        self.selectedProductList = self.productList.filter{selectedProductId.contains($0.productId)}
        self.selectableProductList = self.productList.filter{ !selectedProductId.contains($0.productId)}
    }
    
    func onFailed(failure: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: failure)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.productList = []
        //self.presenter.getProductSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
        self.presenter.getAllProductDataFromServer(getAll: true)
    }
    
}

extension AddNewSaleViewController{
    func showMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: userMessage, message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigateToSaleListViewController()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToSaleListViewController(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalesListViewController") as! SalesListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.YouCannotSaleThisProduct, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "Draft", style: .default){
            (action:UIAlertAction!) in
            SaleStatusObject.isDraft = true
            SaleStatusObject.isPreOrder = false
            self.addZeroProduct()
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func addZeroProduct(){
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        self.selectedProductId.append(selectedProduct.productId)
        selectedSellingPrices.append(0)
        selectedAmounts.append(0)
        selectedSerials.append("")
        self.refreshTableView()
    }
    
}

//Product Delegate
extension AddNewSaleViewController : SalesAddProductDelegate{
    func setSerialData(serial: [String], sellingPrice: Double) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        if self.selectedProductId.contains(selectedProduct.productId){
            self.removeDataofProductObject(productId: selectedProduct.productId)
        }
        
        print(serial)
        
        self.selectedProductId.append(selectedProduct.productId)
        self.selectedSerials.append(serial.joined(separator: ","))
        self.selectedSellingPrices.append(sellingPrice)
        self.selectedAmounts.append(Double(serial.count))
        
        self.addSerialDataToSelectedProducts(productId: selectedProduct.productId, soldQuantity: "\(serial.count)", soldSerials: serial, soldSellingPrice: (sellingPrice.toString()))
        self.removeDataFromSelectableProductList(productId: selectedProduct.productId)
        self.resetSearchBar()
    }
    
    func setProductData(amount: Double, sellingPrice: Double) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        if self.selectedProductId.contains(selectedProduct.productId){
            self.removeDataofProductObject(productId: selectedProduct.productId)
        }
        self.selectedProductId.append(selectedProduct.productId)
        selectedSellingPrices.append(sellingPrice)
        selectedAmounts.append(amount) 
        selectedSerials.append("")
        
        self.addDataToSelectedProducts(productId: selectedProduct.productId, soldQuantity: amount.toString(), soldSellingPrice: sellingPrice.toString())
        self.removeDataFromSelectableProductList(productId: selectedProduct.productId)
        self.resetSearchBar()
    }
    
    func resetSearchBar(){
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
}

extension AddNewSaleViewController : SelectedProduct{
    func setProductData(data: ProductList) {
        self.selectedProduct = data
    }
}

extension AddNewSaleViewController{
    func addDataToSelectedProducts(productId: Int, soldQuantity: String, soldSellingPrice: String){
        if let product = findProductInfo(productId: productId){
            let selectedProduct = ProductList(inventoryId: product.inventoryId, productId: productId, quantity: product.quantity, sellingPrice: product.sellingPrice, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, soldSerials: [""], name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
            for item in self.selectedProductList{
                if item.productId == productId{
                    self.selectedProductList = self.selectedProductList.filter{($0.productId != productId)}
                }
            }
            self.selectedProductList.append(selectedProduct)
            self.refreshTableView()
        }
    }
    
    func addSerialDataToSelectedProducts(productId: Int, soldQuantity: String, soldSerials: [String], soldSellingPrice: String){
        if let product = findProductInfo(productId: productId){
            let selectedProduct = ProductList(inventoryId: product.inventoryId,productId: productId, quantity: product.quantity, sellingPrice: product.sellingPrice, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, soldSerials: soldSerials, name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
            for item in self.selectedProductList{
                if item.productId == productId{
                    self.selectedProductList = self.selectedProductList.filter{($0.productId != productId)}
                }
            }
            self.selectedProductList.append(selectedProduct)
            //self.refreshTableView()
        }
    }
    
    func findProductInfo(productId: Int) -> ProductList?{
        var productItem : ProductList?
        for item in self.productList{
            if item.productId == productId{
                productItem = item
            }
        }
        return productItem
    }
    
    func removeDataFromSelectableProductList(productId: Int){
        if let product = findProductInfo(productId: productId){
            self.selectableProductList = self.selectableProductList.filter{($0.productId != product.productId)}
            self.refreshTableView()
        }
    }
    
    func removeDataofProductObject(productId : Int){
        var productAtIndex = -1
        if let index = self.selectedProductId.firstIndex(of: productId){
            productAtIndex = index
        }
        
        self.selectedSellingPrices.remove(at: productAtIndex)
        self.selectedAmounts.remove(at: productAtIndex)
        self.selectedSerials.remove(at: productAtIndex)
        self.selectedProductId.remove(at: productAtIndex)
    }
    
    func addDataToSelectableProductList(productId: Int, quantity: String){
        if self.productList.count > 0{
            for item in self.productList{
                if item.productId == productId{
                    let inventoryId = item.inventoryId
                    let productId = productId
                    let quantity = item.quantity
                    let sellingPrice = item.sellingPrice
                    let hasSerial = item.hasSerial
                    let serials = item.serials
                    let name = item.name
                    let image = item.image
                    let categoryName = item.categoryName
                    let variant = item.variant
                    let unit = item.unit
                    let categoryId = item.categoryId
                    
                    let selectableProduct = ProductList(inventoryId: inventoryId, productId: productId, quantity: quantity, sellingPrice: sellingPrice, hasSerial: hasSerial, serials: serials, name: name, image: image, categoryName: categoryName, categoryId: categoryId, variant: variant, unitName: unit)
                    
                    self.selectableProductList.append(selectableProduct)
                    self.selectableProductList = self.selectableProductList.sorted(by: { $0.name < $1.name })
                }
            }
        }
    }
    
}
