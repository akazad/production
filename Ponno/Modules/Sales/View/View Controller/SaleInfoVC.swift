//
//  SaleInfoVC.swift
//  Ponno
//
//  Created by a k azad on 17/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SVProgressHUD

enum ProductUnits : String {
    case Currency = "%"
    case Percentage = "৳"
}

class SaleInfoVC: UIViewController {
    
    
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var deliveryChargeLbl: UILabel!
    @IBOutlet weak var payableAmountLbl: UILabel!
    @IBOutlet weak var cashLbl: UILabel!
    @IBOutlet weak var returnLbl: UILabel!
    @IBOutlet weak var dueLbl: UILabel!
    
    @IBOutlet weak var discountTextField: UITextField!{
        didSet {
            discountTextField?.addDoneCancelToolbar(onDone: (target: self, action: #selector(onPressingDoneInDiscountTF)))
        }
    }
    
    @IBOutlet weak var discountUnitTxtField: UITextField!
    @IBOutlet weak var deliverySystemTxtField: UITextField!
    @IBOutlet weak var deliveryChargeTxtField: UITextField!{
        didSet {
            deliveryChargeTxtField?.addDoneCancelToolbar(onDone: (target: self, action: #selector(onPressingDoneInDeliveryCharge)))
        }
    }
    @IBOutlet weak var cashTxtField: UITextField!{
        didSet {
            cashTxtField?.addDoneCancelToolbar(onDone: (target: self, action: #selector(onPressingDoneInCash)))
        }
    }
    @IBOutlet weak var paymentMethodTxtField: UITextField!
    @IBOutlet weak var customerTxtField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = SaleInfoViewPresenter(service: SalesService())
    
    var deliverySystemList : [DeliverySystems] = []
    var paymentMethodList : [PaymentMethodList] = []
    var customerList : [Customers] = [] {
        didSet{
            self.customerPicker.reloadAllComponents()
        }
    }
    
    var deliverySystemId : String?
    var paymentMethodId : String?
    var customerId : String?
    
    var isLoading : Bool = false
    var deliveryCurrentPage : Int = 1
    var customerCurrentPage : Int = 1
    
    var discountUnits : [String] = [ProductUnits.Currency.rawValue, ProductUnits.Percentage.rawValue]
    
    var deliverySystemPicker = UIPickerView()
    var discountUnitPicker = UIPickerView()
    var paymentMethodPicker = UIPickerView()
    var customerPicker = UIPickerView()
    
    //
    var discountPrice : Double?
    var discountUnit : String?
    var payableAmount : Double?
    var deliverySystem : String?
    var deliveryCharge : Double?
    var cash : Double?
    var paymentMethod : Int?
    
    //
    var selectedProductId : [String] = []
    var selectedAmounts : [Double] = []
    var selectedSellingPrices : [Double] = []
    var selectedSerials : [String] = []
    
    //
    let dateFormatter = DateFormatter()
    var invoice : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpPickerView()
        //self.attachPresenter()
        self.initialSetUp()
    }
    
    func initialSetUp(){
        
        self.discountUnitPicker.selectedRow(inComponent: 0)
        self.deliverySystemPicker.selectedRow(inComponent: 0)
        self.paymentMethodPicker.selectedRow(inComponent: 0)
        self.customerPicker.selectedRow(inComponent: 0)
        
        self.generateInvoice()
        
        guard self.selectedAmounts.count > 0 else { return }
        var temp : Int = 0
        guard self.selectedSellingPrices.count > 0 else { return }
        var totalPrice = 0.0
        for price in self.selectedSellingPrices {
            let tempTotal = price * self.selectedAmounts[temp]
            totalPrice += tempTotal
            temp += 1
        }
        totalPriceLabel.text = "সর্বমোটঃ "    + "\(totalPrice)"
        discountLbl.text = "ডিসকাউন্ট: "
        deliveryChargeLbl.text = "ডেলিভারি চার্জ: "
        payableAmountLbl.text = "পরিশোধ মূল্য: "    + "\(totalPrice)"
        cashLbl.text = "ক্যাশ: "
        returnLbl.text = "বাকি: " +    "\(totalPrice)"
        dueLbl.text = "ফেরত: "

    }
    
    //PickerView
    func setUpPickerView(){
        discountTextField.delegate = self
        discountUnitTxtField.delegate = self
        deliverySystemTxtField.delegate = self
        deliveryChargeTxtField.delegate = self
        cashTxtField.delegate = self
        paymentMethodTxtField.delegate = self
        customerTxtField.delegate = self
        
        
        deliverySystemPicker.delegate = self
        paymentMethodPicker.delegate = self
        customerPicker.delegate = self
        discountUnitPicker.delegate = self
        
        let discountUnitToolBar = UIToolbar()
        discountUnitToolBar.barStyle = UIBarStyle.default
        discountUnitToolBar.isTranslucent = true
        discountUnitToolBar.tintColor = UIColor.black
        discountUnitToolBar.sizeToFit()
        
        let discountUnitCancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        let discountUnitSpaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let discountUnitDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneInDiscountUnit(sender:)))
        
        discountUnitToolBar.setItems([discountUnitCancelButton, discountUnitSpaceButton, discountUnitDoneButton], animated: false)
        discountUnitToolBar.isUserInteractionEnabled = true
        
        self.discountUnitTxtField.inputView = discountUnitPicker
        self.discountUnitTxtField.inputAccessoryView = discountUnitToolBar
        
        let deliverySystemToolBar = UIToolbar()
        deliverySystemToolBar.barStyle = UIBarStyle.default
        deliverySystemToolBar.isTranslucent = true
        deliverySystemToolBar.tintColor = UIColor.black
        deliverySystemToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneInDeliverySystem(sender:)))
        
        deliverySystemToolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        deliverySystemToolBar.isUserInteractionEnabled = true
        
        self.deliverySystemTxtField.inputView = deliverySystemPicker
        self.deliverySystemTxtField.inputAccessoryView = deliverySystemToolBar
        
        //Payment ToolBar
        let paymentMethodToolBar = UIToolbar()
        paymentMethodToolBar.barStyle = UIBarStyle.default
        paymentMethodToolBar.isTranslucent = true
        paymentMethodToolBar.tintColor = UIColor.black
        paymentMethodToolBar.sizeToFit()
    
        
        let paymentCancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        let paymentSpaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let paymentDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnPaymentMethod(sender:)))
        paymentMethodToolBar.setItems([paymentCancelButton, paymentSpaceButton, paymentDoneButton], animated: false)
        paymentMethodToolBar.isUserInteractionEnabled = true
        
        self.paymentMethodTxtField.inputView = paymentMethodPicker
        self.paymentMethodTxtField.inputAccessoryView = paymentMethodToolBar
        
        let customerToolBar = UIToolbar()
        customerToolBar.barStyle = UIBarStyle.default
        customerToolBar.isTranslucent = true
        customerToolBar.tintColor = UIColor.black
        customerToolBar.sizeToFit()
        
        
        let customerCancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        let customerSpaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let customerDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        customerToolBar.setItems([customerCancelButton, customerSpaceButton, customerDoneButton], animated: false)
        customerToolBar.isUserInteractionEnabled = true
        
        self.customerTxtField.inputView = customerPicker
        self.customerTxtField.inputAccessoryView = customerToolBar
        
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    func generateInvoice(){
        dateFormatter.dateFormat = "yyMMdd"
        let date = dateFormatter.string(from: Date())
        let userInfo = getUserData()
        guard let id = userInfo?.shopId else {
            return
        }
        let sellString = "SEL"
        let extraItemInInvoice = "09"

        let invoiceNumber = sellString + "\(id)" + date + extraItemInInvoice
        self.invoice = invoiceNumber
    }
    
    @objc func onPressingDoneOnPaymentMethod(sender: UIBarButtonItem){
        self.paymentMethodTxtField.resignFirstResponder()
        self.customerTxtField.becomeFirstResponder()
    }
    
    @objc func onSubmit(sender : UIButton){
        self.submitData()
    }
    
    func submitData(){
        
        guard self.selectedProductId.count > 0 else {
            return
        }
        
        guard self.selectedAmounts.count > 0 else { return }
        
        guard self.selectedSellingPrices.count > 0 else {return}
        
        var totalPrice = 0.0
        for price in self.selectedSellingPrices {
            totalPrice += price
        }
        
        guard self.selectedSerials.count > 0 else{
            return
        }

        guard let discount = self.discountPrice else {
            return
        }
        
        guard let discountUnit = self.discountUnit else {
            return
        }
        
        
        guard let deliveryCharge = self.deliveryCharge else{
            return
        }
        
        let totalPayable = totalPrice - discount + deliveryCharge
        
        guard let cash = self.cash else{
            return
        }
        
        var due = 0.0
        
        if totalPayable > cash {
            due = totalPayable - cash
        }
        else {
            due = 0.0
        }
        
        var cashback = 0.0
        
        if cash > totalPayable {
            cashback = cash - totalPayable
        }
        else {
            cashback = 0.0
        }
        
        guard let deliverySystem = self.deliverySystem else{
            return
        }
        
        guard let paymentMethodId = self.paymentMethodId else{
            return
        }
        guard let customerId = self.customerId else {
            return
        }
        guard let invoiceNumber = self.invoice else{
            return
        }
        
        let params : [String : Any] = ["invoice": invoiceNumber,"products" : selectedProductId, "quantities" : self.selectedAmounts, "prices": self.selectedSellingPrices, "serials": self.selectedSerials, "total_amount": totalPrice, "discount": discount, "discount_unit": discountUnit, "payable_amount" : totalPayable , "cash" : cash,"due" : due , "cash_back" : cashback, "delivery_system" : deliverySystem, "delivery_charge" : deliveryCharge , "payment_method" : paymentMethodId, "customer" : customerId]
        
        
        self.presenter.postSalesDataToServer(params : params)
        
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: "Successful", message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Refresh TextField
extension SaleInfoVC{
    
    fileprivate func refreshScreen(){
        
        // সর্বমোট
        guard self.selectedAmounts.count > 0 else { return }
        var temp : Int = 0
        guard self.selectedSellingPrices.count > 0 else { return }
        var totalPrice = 0.0
        for price in self.selectedSellingPrices {
            let tempTotal = price * self.selectedAmounts[temp]
            totalPrice += tempTotal
            temp += 1
        }
        totalPriceLabel.text = "সর্বমোটঃ " + "\(totalPrice)"
        
        // ডিস্কাউন্ট
        guard var discount = self.discountPrice else {
            return
        }
        guard let discountUnit = self.discountUnit else {
            return
        }
        if discountUnit == ProductUnits.Percentage.rawValue {
            discount = (totalPrice * discount) / 100.0
            discountLbl.text = "ডিস্কাউন্টঃ " + "\(discount)"
        }else{
            discountLbl.text = "ডিস্কাউন্টঃ " + "\(discount)"
        }
//        self.discountLbl.text = "ডিস্কাউন্টঃ " + "\(discount)"
//        if discountUnit == ProductUnits.Percentage.rawValue {
//            discount = (totalPrice * discount) / 100.0
//            discountLbl.text = "ডিস্কাউন্টঃ " + "\(discount)"
//        }
//        else{
//            discountLbl.text = "ডিস্কাউন্টঃ " + "\(discount)"
//        }
        
        // ডেলিভেরি চার্জঃ
        guard let deliveryCharge = self.deliveryCharge else{
            return
        }
        self.deliveryChargeLbl.text = "ডেলিভেরি চার্জঃ " + "\(deliveryCharge)"
        
        // পরিশোধ মূল্য:
        guard var payable = self.payableAmount else {
            return
        }
        payable = (totalPrice - discount + deliveryCharge)
        self.payableAmountLbl.text = "পরিশোধ মূল্য: "   + "\(payable)"
    
//        let totalPayable = totalPrice - discount + deliveryCharge
//
//        self.payableAmountLbl.text = "দেনাঃ " + "\(totalPayable)"
        
        guard let cash = self.cash else{
            return
        }
        self.cashLbl.text = "ক্যাশঃ " + "\(cash)"
        
        if payable > cash {
            self.dueLbl.text = "বাকিঃ " + "\(payable - cash)"
            self.returnLbl.text = "ফেরতঃ "  +  "0.0"
        }
        else {
            self.dueLbl.text = "বাকিঃ "  +  "0.0"
            self.returnLbl.text = "ফেরতঃ "  + "\(cash - payable)"
        }
        
//        if cash > payable {
//            self.returnLbl.text = "ফেরতঃ " + "\(cash - payable)"
//        }
//        else {
//            self.returnLbl.text = "ফেরতঃ " + "0.0"
//        }
    }
}

//Mark: TextField Delegate
extension SaleInfoVC : UITextFieldDelegate{
    
    @objc func onPressingDoneInDiscountTF() {
        guard let discount = self.discountTextField.text, discount != "", let discountPrice = Double(discount) else{ return}
        self.discountPrice = discountPrice
        self.discountTextField.resignFirstResponder()
        self.discountUnitTxtField.becomeFirstResponder()
        self.refreshScreen()
    }
    
    @objc func onPressingDoneInDiscountUnit(sender : UIBarButtonItem){
        guard let discountUnit = self.discountUnitTxtField.text, discountUnit != "" else{ return}
        self.discountUnit = discountUnit
        self.discountUnitTxtField.resignFirstResponder()
        self.deliverySystemTxtField.becomeFirstResponder()
        
        self.refreshScreen()
    }
    
    @objc func onPressingDoneInDeliverySystem(sender : UIBarButtonItem){
        guard let deliverySystem = self.deliverySystemTxtField.text, deliverySystem != "" else{ return}
        self.deliverySystem = deliverySystem
        self.deliverySystemTxtField.resignFirstResponder()
        self.deliveryChargeTxtField.becomeFirstResponder()
        
        
    }
    
    @objc func onPressingDoneInDeliveryCharge() {
        guard let deliveryCharge = self.deliveryChargeTxtField.text, deliveryCharge != "", let deliveryChargePrice = Double(deliveryCharge) else{ return  }
        self.deliveryCharge = deliveryChargePrice
        self.deliveryChargeTxtField.resignFirstResponder()
        self.cashTxtField.becomeFirstResponder()
        
        self.refreshScreen()
    }
    
    @objc func onPressingDoneInCash() {
        guard let cashText = self.cashTxtField.text, cashText != "", let cash = Double(cashText) else{ return  }
        self.cash = cash
        self.cashTxtField.resignFirstResponder()
        self.paymentMethodTxtField.becomeFirstResponder()
        self.refreshScreen()
    }
    
    @objc func onPressingDoneInPaymentMethod(sender : UIBarButtonItem){
        guard let paymentMethodText = self.paymentMethodTxtField.text, paymentMethodText != ""else{ return}
       // self.paymentMethod = deliverySystem
        self.deliverySystemTxtField.resignFirstResponder()
        self.deliveryChargeTxtField.becomeFirstResponder()
    }
}

//Mark: PickerViewDelegate
extension SaleInfoVC : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.discountUnitPicker{
            return discountUnits.count
        }
        else if pickerView == self.deliverySystemPicker{
            return deliverySystemList.count
        }
        else if pickerView == self.paymentMethodPicker{
            return paymentMethodList.count
        }
        else if pickerView == self.customerPicker{
            return customerList.count
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.discountUnitPicker{
            if self.discountUnits.count > 0 {
                let unit = self.discountUnits[row]
                self.discountUnitTxtField.text = unit
                return unit
            }else{
                return ""
            }
        }
        else if pickerView == self.deliverySystemPicker{
            if self.deliverySystemList.count > 0 {
                let list = self.deliverySystemList[row]
                self.deliverySystemTxtField.text = list.name
                self.deliverySystemId = "\(list.id)"
                if isLoading == false && row == self.deliverySystemList.count - 1 {
                    self.isLoading = true
                    self.deliveryCurrentPage += 1
//                    self.presenter.getDeliverySystemDataFromServer(page: self.deliveryCurrentPage)
                }
                return self.deliverySystemList[row].name
            }else{
                return ""
            }
        }
        else if pickerView == self.paymentMethodPicker{
            if self.paymentMethodList.count > 0 {
                let list = self.paymentMethodList[row]
                self.paymentMethodTxtField.text = list.name
                self.paymentMethodId = "\(list.id)"
                return list.name
            }else{
                return ""
            }
        }
        else if pickerView == self.customerPicker{
            if self.customerList.count > 0{
                let list = self.customerList[row]
                self.customerTxtField.text = list.name
                self.customerId = "\(list.id)"
                if isLoading == false && row == self.customerList.count - 1 {
                    self.isLoading = true
                    self.customerCurrentPage += 1
//                    self.presenter.getCustomerDataFromServer(page: self.customerCurrentPage)
                }
                return self.customerList[row].name
            }else{
                return ""
            }
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.discountUnitPicker{
            if self.discountUnits.count > 0 {
                let unit = self.discountUnits[row]
                self.discountUnitTxtField.text = unit
            }
        }
        if pickerView == self.deliverySystemPicker{
            if self.deliverySystemList.count > 0 {
                let list = self.deliverySystemList[row]
                self.deliverySystemTxtField.text = list.name
                self.deliverySystemId = "\(list.id)"
            }else{
                return
            }
        }else if pickerView == self.paymentMethodPicker{
            if self.paymentMethodList.count > 0 {
                let list = self.paymentMethodList[row]
                self.paymentMethodTxtField.text = list.name
                self.paymentMethodId = "\(list.id)"
            }else{
                return
            }
        }else if pickerView == self.customerPicker{
            if self.customerList.count > 0 {
                let list = self.customerList[row]
                self.customerTxtField.text = list.name
                self.customerId = "\(list.id)"
            }
        }else{
            return
        }
    }
}

//Mark: Api Delegate
//extension SaleInfoVC : SaleInfoViewDelegate {
//    func onDraftStoreSuccessful(data: AddDataMapper) {
//        //
//    }
//    
//    func onCustomerFailed(message: String) {
//        
//    }
//    
//    func setPaymentMethod(data: PaymentMethodDataMapper) {
//        
//    }
//    
//    func setCustomer(data: AllCustomerDataMapper) {
//        
//    }
//    
//    func setCustomerList(data: CustomerDataMapper) {
//        
//    }
//    
//    func onSuccess(data: AddDataMapper) {
//        guard let message = data.message else {
//            return
//        }
//        self.displayMessage(userMessage: message)
//    }
//    
//    func setDeliverySystemData(data: DeliveryDataMapper) {
//        guard let deliveryData = data.deliverySystems, deliveryData.count > 0 else {
//            return
//        }
//        self.isLoading = false
//        self.deliverySystemList += deliveryData
//    }
//    
//    func setPaymentMethodData(data: PaymentMethodDataMapper) {
//        guard let paymentMethodData = data.paymentMethodList, paymentMethodData.count > 0 else {
//            return
//        }
//        self.paymentMethodList += paymentMethodData
//    }
//    
//    func setCustomerData(data: CustomerDataMapper) {
//        guard let customerData = data.customers, customerData.count > 0 else {
//            return
//        }
//        self.isLoading = false
//        self.customerList += customerData
//    }
//    
//    func onFailed(message: String) {
////        self.
//    }
//    
//    func showLoading() {
//        self.showLoader()
//    }
//    
//    func hideLoading() {
//        self.hideLoader()
//    }
//    
//    func attachPresenter(){
//        self.presenter.attachView(viewDelegate: self)
//        self.isLoading = true
//        self.getDataFromServer()
//    }
//    
//    func getDataFromServer(){
////        self.presenter.getDeliverySystemDataFromServer(page: self.deliveryCurrentPage)
////        self.presenter.getPaymentMethodDataFromServer()
////        self.presenter.getCustomerDataFromServer(page: self.customerCurrentPage)
//    }
//}
