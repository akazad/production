//
//  SalesPerDayViewController.swift
//  Ponno
//
//  Created by a k azad on 22/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SVProgressHUD
import Floaty

class SalesPerDayViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = SalesPerDayPresenter(service: SalesService())
    
    var salePerDaylist : [SaleListDateWise] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    let manager = CollectionViewScrollManager()
    
    var selectedDate : String?
    var salePerdaySummery : [SalesPerDaySummery]?
    var perdaySaleId: Int?
    
    var deliveryStatus : Int = 0
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    var pickerDate: String?
    //var datePicker = UIPickerView()
    var toolBar = UIToolbar()
    var picker  = UIDatePicker()
    
    
    let datePicker : UIDatePicker = UIDatePicker()
    
    var parentId : Int?
    var userId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTableView()
        self.configureCollectionView()
        self.addFloaty()
        self.setBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.setNavigationBarTitle()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setNavigationBarTitle(){
        guard let date = self.selectedDate else {
            return
        }
        self.title = date.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        if let user = getUserData(){
            guard let parent = user.parent, let uId = user.id else{
                return
            }
            self.parentId = Int(parent)
            self.userId = Int(uId)
        }
        
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToFreemiumSaleableViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumSaleableProductsVC") as! FreemiumSaleableProductsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func setBarButton(){
        
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onPressed(sender:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = barButton
        
        if let shop = getShopData(){
            guard  let deliverySystemStatus = shop.deliverySystem else {
                return
            }
            self.deliveryStatus = deliverySystemStatus
        }
        
    }
    
    @objc func onPressed(sender: UIBarButtonItem){
        self.navigateToSalesPerDaySearchVC()
    }
    
    func navigateToSalesPerDaySearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalesPerDaySearchViewController") as! SalesPerDaySearchViewController
        viewController.selectedDate = self.selectedDate
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Delete, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
                self.presenter.cancelPerDaySaleData(id: deleteId) 
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title:  LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.salePerDaylist = []
                    
                    guard let date = self.selectedDate else{
                        return
                    }
                    
                    self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: date)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(SalesPerDayViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.salePerDaylist = []
        self.currentPage = 1
        guard let date = self.selectedDate else{
            return  
        }
        self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: date)
        
        refreshControl.endRefreshing()
    }

}

//Mark: Floaty Delegate
extension SalesPerDayViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        if let viewSettings = getViewSettings(){
            if viewSettings.salePanel == "FreemiumSale"{
                self.navigateToFreemiumSaleableViewController()
            }else{
                self.navigateToAddNewSaleViewController()
            }
        }
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}



//MARK: CollectionView Delegate & Data Source
extension SalesPerDayViewController : UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(SalesPerDaySummeryCell.nib, forCellWithReuseIdentifier: SalesPerDaySummeryCell.identifier)

    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let saleSummeryList = self.salePerdaySummery, saleSummeryList.count > 0 else{
            return 0
        }
        return saleSummeryList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : SalesPerDaySummeryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SalesPerDaySummeryCell", for: indexPath) as! SalesPerDaySummeryCell
        guard let saleSummeryList = self.salePerdaySummery else{
            return cell
        }
        let list = saleSummeryList[indexPath.row]

        cell.nameLabel.text = list.title 
        cell.numberLabel.text = list.value

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 60.0)
    }
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.salePerdaySummery, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
}

// MARK: TableView Delegate & Data Source
extension SalesPerDayViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SalesPerDayCell.nib, forCellReuseIdentifier: SalesPerDayCell.identifier)
        self.tableView.tableFooterView = UIView()
        //self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
        self.tableView.separatorStyle = .none

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.salePerDaylist.count > 0 {
            return self.salePerDaylist.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SalesPerDayCell = tableView.dequeueReusableCell(withIdentifier: SalesPerDayCell.identifier, for: indexPath) as! SalesPerDayCell
        cell.selectionStyle = .none
        
        if self.salePerDaylist.count > 0 {
            let saleItem = self.salePerDaylist[indexPath.row]
            
            cell.salePerDay = saleItem
            cell.deliveryStatus = self.deliveryStatus
            
            cell.totalLabel.text = LanguageManager.Total + " : " + "\(saleItem.total)" + " " + Constants.currencySymbol
            cell.invoiceLabel.text = saleItem.invoice
            cell.dueLabel.text = LanguageManager.Due + " : " + "\(saleItem.invoiceDue) " + Constants.currencySymbol
            cell.timeLabel.text = saleItem.time.convertDateFormater(inputDateFormat: DateFormats.HH_mm_ss.rawValue, outputDateFormat: DateFormats.h_mm_a.rawValue)
            if saleItem.customerName.isEmpty{
                cell.customerNameLbl.text = LanguageManager.CustomerName + " : " + "N/A"
            }else{
                cell.customerNameLbl.text = LanguageManager.CustomerName + " : " + saleItem.customerName
            }
            
            let addedBy = saleItem.addedBy
            
            if let parentId = self.parentId, let userId = self.userId{
                if parentId == 1 || userId == addedBy{
                    cell.popUpBtn.isHidden = false
                }else{
                    cell.popUpBtn.isHidden = true
                }
            }
            
            cell.popUpBtn.tag = saleItem.id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
            
//            if self.deliveryStatus == 0 {
//                cell.deliverySystemLabel.isHidden = true
//            }else{
//                let deliverySystem = saleItem.deliverySystemName
//                if deliverySystem == "" {
//                    cell.deliverySystemLabel.text = "Delivery System: --- "
//                }else{
//                    cell.deliverySystemLabel.text = "Delivery System: " + saleItem.deliverySystemName
//                }
//            }
            
            if isLoading == false && indexPath.row == self.salePerDaylist.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                guard let date = self.selectedDate else{
                    return cell
                }
                self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: date)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.salePerDaylist.count > 0 {
            let saleItem = self.salePerDaylist[indexPath.row]
            self.navigateToSaleDetailsVC(iD: saleItem.id)
        }
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        viewController.iD = iD 
        viewController.state = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete{
//            if self.salePerDaylist.count > 0 {
//                self.salePerDaylist.remove(at: indexPath.row)
//                tableView.deleteRows(at: [indexPath], with: .fade)
//
//            }
//        }
//    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title: LanguageManager.Delete) { action, index in
            
            if self.salePerDaylist.count > 0{
                let salePerDay = self.salePerDaylist[indexPath.row]
                self.perdaySaleId = salePerDay.id
                
                guard let id = self.perdaySaleId else{
                    return
                }
                self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
            }
            
        }
        delete.backgroundColor = UIColor.red
        
        return [delete]
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.salePerDaylist.count > 0{
            self.perdaySaleId = sender.tag
            for saleItem in self.salePerDaylist{
                if self.perdaySaleId == saleItem.id{
                    
                    let due = saleItem.due
                    //                    let addedBy = saleItem.addedBy
                    
                    guard let id = self.perdaySaleId else{
                        return
                    }
                    
                    if let category = getBusinessCategory(){
                        if category.businessCategory == 1{
                            let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                                self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                
                                self.view.endEditing(true)
                            }
                            deleteAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                            cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                            
                            myActionSheet.addAction(deleteAction)
                            myActionSheet.addAction(cancelAction)
                        }else{
                            if due != saleItem.invoiceDue && saleItem.invoiceDue != "0.00" && saleItem.deliverySystemsId == 0{
                                
                                if !(due != saleItem.invoiceDue && saleItem.deliverySystemsId == 0 || saleItem.returnCash != "0.00"){
                                    let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                                        self.navigateToSalePanelProductViewController(saleId: id)
                                    }
                                    
                                    myActionSheet.addAction(updateAction)
                                }
                                
                                let due = saleItem.due.toDouble() ?? 0
                                let invoiceDue = saleItem.invoiceDue.toDouble() ?? 0
                                                                
                                let duePaidAction = UIAlertAction(title: "\(due - invoiceDue) " + Constants.currencySymbol + " " + LanguageManager.Paid, style: UIAlertAction.Style.default) { (action) in
                                }
                                
                                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                    
                                    self.view.endEditing(true)
                                }
                                
                                duePaidAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                                cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                                
                                myActionSheet.addAction(duePaidAction)
                                myActionSheet.addAction(cancelAction)
                            }else{
                                
                                if !(due != saleItem.invoiceDue && saleItem.deliverySystemsId == 0 || saleItem.returnCash != "0.00"){
                                    let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                                        self.navigateToSalePanelProductViewController(saleId: id)
                                    }
                                    
                                    myActionSheet.addAction(updateAction)
                                }
                                
                                let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                                    self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                                }
                                
                                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                    self.view.endEditing(true)
                                }
                                
                                cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                                
                                myActionSheet.addAction(deleteAction)
                                myActionSheet.addAction(cancelAction)
                            }
                        }
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToSalePanelProductViewController(saleId: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalePanelProductViewController") as! SalePanelProductViewController
        viewController.saleId = saleId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}


// MARK: API Delegate
extension SalesPerDayViewController: SalesPerDayViewDelegate{
    func setSalesListData(data: SalePerDayDataMapper) {

        guard let item = data.summary else {
            return
        }
        self.salePerdaySummery = item
        self.setUpAutoScroll()
        self.collectionView.reloadData()
        
        guard let list = data.saleListDateWise, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.salePerDaylist += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func cancelPerDaySales(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(message : String) {
        showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let date = self.selectedDate else{
            return
        }
        self.isLoading = true
        self.currentPage = 1
        self.salePerDaylist = []
        self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: date)
    }
    
}
