//
//  InvoiceReturnViewController.swift
//  Ponno
//
//  Created by a k azad on 30/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class InvoiceReturnViewController: UIViewController {

    @IBOutlet weak var returnRelatedInfoLbl: UILabel!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productNameTextField: UITextField!
    @IBOutlet weak var returnQuantityLbl: UILabel!
    @IBOutlet weak var quantityTextField: UITextField!
    @IBOutlet weak var pricePerUnitLbl: UILabel!
    @IBOutlet weak var returnPriceTextField: UITextField!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var invoiceDueLbl: UILabel!
    @IBOutlet weak var dueAdjustSwitch: UISwitch!
    @IBOutlet weak var dueAdjustLbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = SaleProductsReturnPresenter(service: SalesService())
    
    var returnProduct : SaleProducts?
    var saleDetails : SaleDetails?
    var saleId : Int?
    var productId: Int?
    var saleProductId: Int?
    var returnQunatity : Double?
    var serialNo : String?
    var maxPricePerUnit : Double = 0.0
    var reducedReturnPrice : Double = 0.0
    var dueAdjust : String = "off"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleSetup()
        self.setUpToolBar()
        self.discountSetup()
        self.dueAdjustSetup()
        self.initialSetUp()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpInitialLanguage()
    }
    
    func initialSetUp(){
        guard let data = self.returnProduct else {
            return
        }
        self.productNameTextField.text = data.name
        if self.serialNo == "" {
            self.quantityTextField.text = data.quantity
            self.quantityTextField.isEnabled = true
        }else{
            self.quantityTextField.text = self.serialNo
            self.quantityTextField.isEnabled = false
        }
        
        self.returnPriceTextField.text = "\(maxPricePerUnit)"
        self.productId = data.productId
        self.saleProductId = data.saleProductId
        
        returnPriceTextField.addTarget(self, action: #selector(onReturnPriceTextFieldChange), for: .editingChanged)
        submitBtn.addTarget(self, action: #selector(submitButtonAction(sender:)), for: .touchUpInside)
    }
    
    func titleSetup(){
        self.returnRelatedInfoLbl.text = LanguageManager.ReturnRelatedInfo
        self.productNameLbl.text = LanguageManager.ProductName
        self.productNameTextField.placeholder = LanguageManager.ProductName
        self.productNameTextField.underlined()
        self.productNameTextField.isEnabled = false
        self.returnQuantityLbl.text = LanguageManager.ReturnQuantity
        self.quantityTextField.placeholder = LanguageManager.ReturnQuantity
        self.quantityTextField.underlined()
        self.pricePerUnitLbl.text = LanguageManager.PricePerUnit
        self.returnPriceTextField.placeholder = LanguageManager.PricePerUnit
        self.returnPriceTextField.underlined()
    }
    
    func dueAdjustSetup(){
        guard let details = self.saleDetails else{
            return
        }
        
        let invoiceDue = details.invoiceDue.toDouble() ?? 0.0
        let deliveryCharge = details.deliveryCharge.toDouble() ?? 0.0
        let due = invoiceDue - deliveryCharge
        if due > 0 && details.deliverySystem.isEmpty == true{
            self.invoiceDueLbl.text = LanguageManager.InvoiceDue + " " + details.invoiceDue + Constants.currencySymbol
            self.dueAdjustSwitch.isOn = false
            self.dueAdjustLbl.text = LanguageManager.DueAdjust
            self.dueAdjustShow(is: true)
        }else{
            self.dueAdjustShow(is: false)
        }
    }
    
    func discountSetup(){
        var total : Double = 0.0
        
        var unitPrice : Double = 0.0
        var discountAmount : Double = 0.0
        guard let details = self.saleDetails else{
            return
        }
        
        discountAmount = details.discountAmount.toDouble() ?? 0.0
        total = details.total.toDouble() ?? 0.0
        
        guard let product = self.returnProduct else{
            return
        }
        unitPrice = product.unitPrice.toDouble() ?? 0.0
        
        reducedReturnPrice = (discountAmount * unitPrice) / total
        maxPricePerUnit = unitPrice - reducedReturnPrice
        
        if reducedReturnPrice > 0{
            self.discountLbl.isHidden = false
            self.discountLbl.text = "Product Price is equals to" + product.unitPrice
            + Constants.currencySymbol + "(given discount" + "\(reducedReturnPrice)" + Constants.currencySymbol + ")"
            print(reducedReturnPrice)
            self.discountLbl.numberOfLines = 0
        }else{
            self.discountLbl.isHidden = true
        }
        
    }
    
    @objc func onReturnPriceTextFieldChange(sender: UITextField){
        let pricePerUnit = self.returnPriceTextField.text?.toDouble() ?? 0.0
        if pricePerUnit > self.maxPricePerUnit{
            self.returnPriceTextField.text = "\(maxPricePerUnit)"
        }
    }
    
    @objc func submitButtonAction(sender: UIButton){
        if isValidated(){
            var productQuantity : Double?
            guard  let cashBack = self.returnPriceTextField.text, let saleId = self.saleId, let productId = self.productId, let saleProductId = self.saleProductId else {
                return
            }
            //let quantity = 0
            
            if self.serialNo == "" {
                guard let quantityText = self.quantityTextField.text, let quantity = Double(quantityText) else{
                    return
                }
                productQuantity = quantity
            }else{
                productQuantity = 1.0
            }
            
            guard let quantity = productQuantity else{
                return
            }
            
            if dueAdjustSwitch.isOn == true{
                self.dueAdjust = "on"
            }
            
            let updateParam : [String: Any] = ["sale_id": saleId, "sale_product_id": saleProductId, "product_id": productId, "quantity": quantity, "price" : cashBack, "due_adjust": self.dueAdjust]
            
            self.presenter.postSaleProductReturnDataToServer(returnData: updateParam)
        }
    }
    
    func setUpToolBar(){
        
        //quantityToolBaar
        let quantityToolBar = UIToolbar()
        quantityToolBar.barStyle = UIBarStyle.default
        quantityToolBar.isTranslucent = true
        quantityToolBar.tintColor = UIColor.black
        quantityToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
        
        quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        quantityToolBar.isUserInteractionEnabled = true
        
        self.quantityTextField.inputAccessoryView = quantityToolBar
        
        //DeliveryDueAmountToolBaar
        let cashBackToolBar = UIToolbar()
        cashBackToolBar.barStyle = UIBarStyle.default
        cashBackToolBar.isTranslucent = true
        cashBackToolBar.tintColor = UIColor.black
        cashBackToolBar.sizeToFit()
        
        let cashBackDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCashBack(sender:)))
        
        cashBackToolBar.setItems([cancelButton, spaceButton, cashBackDoneButton], animated: false)
        cashBackToolBar.isUserInteractionEnabled = true
        
        self.returnPriceTextField.inputAccessoryView = cashBackToolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
        self.quantityTextField.resignFirstResponder()
        self.returnPriceTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnCashBack(sender: UIBarButtonItem){
        self.returnPriceTextField.resignFirstResponder()
        self.submitBtn.becomeFirstResponder()
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
//        guard let data = self.returnProduct else {
//            return false
//        }
        if (self.productNameTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.ProductNameIsRequired)
            return false
        }
        else if (self.quantityTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.QuantityIsRequired)
            return false
        }
        else if let quantityText = self.quantityTextField.text, let quantity = Double(quantityText), let quantityToCheck = returnQunatity, quantity > Double(quantityToCheck){
            self.displayMessage(userMessage: LanguageManager.YouCantRetrunMoreThan + "\(quantityToCheck)")
            return false
        }
        else if (self.returnPriceTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.CashBackPriceIsRequired)
            return false
        }
        return true
    }
    
    func dueAdjustShow(is show: Bool){
        if show == false{
            self.invoiceDueLbl.isHidden = true
            self.dueAdjustSwitch.isHidden = true
            self.dueAdjustLbl.isHidden = true
        }else{
            self.invoiceDueLbl.isHidden = false
            self.dueAdjustSwitch.isHidden = false
            self.dueAdjustLbl.isHidden = false
        }
    }
    
    func checkMaxQuantity(){
        
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
//    func popToSpecifiedController(){
//        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
//        for aViewController in viewControllers {
//            if aViewController is PurchaseViewController {
//                self.navigationController!.popToViewController(aViewController, animated: true)
//            }
//        }
//    }

}

//Mark: Api Delegate
extension InvoiceReturnViewController: SaleProductsReturnViewDelegate{ 
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(message: String) {
        self.showAlert(title: message, message: "")
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
    
}
