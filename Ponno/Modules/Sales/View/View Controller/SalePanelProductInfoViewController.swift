//
//  SalePanelProductInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 4/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

enum NavigatedFrom: String {
    case Draft = "Draft"
    case PreOrder = "PreOrder"
}

class SalePanelProductInfoViewController: UIViewController {
    
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var deliveryChargeLbl: UILabel!
    @IBOutlet weak var payableAmountLbl: UILabel!
    @IBOutlet weak var paidAmountLbl: UILabel!
    @IBOutlet weak var cashLbl: UILabel!
    @IBOutlet weak var walletLbl: UILabel!
    @IBOutlet weak var cashBackLbl: UILabel!
    @IBOutlet weak var dueLbl: UILabel!
    @IBOutlet weak var saleBtn: UIButton!{
        didSet{
            self.setUpCardView(uiview: saleBtn)
        }
    }
    @IBOutlet weak var draftBtn: UIButton!{
        didSet{
            self.setUpCardView(uiview: draftBtn)
        }
    }
    @IBOutlet weak var cashTitleLbl: UILabel!
    @IBOutlet weak var cashText: UITextField!
    @IBOutlet weak var paymentPhoneNoLbl: UILabel!
    @IBOutlet weak var paymentPhoneNo: UITextField!
    //@IBOutlet weak var paymentPhoneHeight: NSLayoutConstraint!
    @IBOutlet weak var customerLbl: UILabel!
    @IBOutlet weak var customerTextField: UITextField!
    @IBOutlet weak var walletAmountTitleLbl: UILabel!
    @IBOutlet weak var walletAmountTitleLblHeight: NSLayoutConstraint!
    @IBOutlet weak var walletAmountTextField: UITextField!
    @IBOutlet weak var walletAmountTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var walletPayLbl: UILabel!
    @IBOutlet weak var walletPayLblHeight: NSLayoutConstraint!
    @IBOutlet weak var walletPayTextField: UITextField!
    @IBOutlet weak var walletPayTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var paymentMethodText: UITextField!
    @IBOutlet weak var discountTitleLbl: UILabel!
    @IBOutlet weak var discountTextField: UITextField!
    @IBOutlet weak var discountSegmentedControl: UISegmentedControl!
    @IBOutlet weak var deliveryTitleLbl: UILabel!
    @IBOutlet weak var deliveryTextField: UITextField!
    
    private var presenter = SalePanelProductInfoViewPresenter(service: SalesService())
    
    var saleId: Int?
    var saleInfo : SaleInfo?
    
    var draftId : Int?
    var saleEditInfo : SaleEditInfo?
    var preOrderId: Int?

    var customerId : Int?
    var totalPrice : Double?
    
    var paymentMethodList : [PaymentMethodList] = []
    var paymentMethodPicker = UIPickerView()
    var paymentMethodId : Int?
    
    //
    var discountPrice : Double?
    var discountUnit : String?
    var payableAmount : Double?
    var dueAmount : Double?
    var cashBackAmount : Double?
    var deliverySystem : String?
    var deliveryCharge : Double?
    
    //
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedSellingPrices : [Double] = []
    var selectedSerials : [String] = []
    
    var deliveryStatus: Int?
    
    var datePicker = UIDatePicker()
    
    var discount : Double?
    
    var dateFormatter = DateFormatter()
    
    var pushedInFromSaleEdit : Bool = false
    
    var navigatedFrom : NavigatedFrom?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.resetSaleInfoObject()
        //self.resetInitialSumOfProduct()
        self.titleSetup()
        self.initialSetUp()
        self.initialInfoSetup()
        self.setUpPickerView()
        self.showSaleDatePicker()
        self.attachPresenter()
    }
    
    func resetSaleInfoObject(){
        SaleInfoObject.resetSaleInfoObject()
    }
    
    func back(sender: UIBarButtonItem) {
        SaleInfoObject.resetSaleInfoObject()
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        self.initialInfoSetup()
    }
    
    func titleSetup(){
        self.setUpInitialLanguage()
        cashTitleLbl.text = LanguageManager.Amount
        cashText.placeholder = LanguageManager.Amount
        paymentPhoneNo.text = LanguageManager.PhoneNumber
        customerLbl.text = LanguageManager.Customer
        customerTextField.placeholder = LanguageManager.Customer
        walletAmountTitleLbl.text = LanguageManager.WalletAmount
        walletPayLbl.text = LanguageManager.WalletPay
        dateLbl.text = LanguageManager.Date
        discountTitleLbl.text = LanguageManager.Discount
        deliveryTitleLbl.text = LanguageManager.Delivery
        saleBtn.setTitle(LanguageManager.Sale, for: .normal)
        draftBtn.setTitle(LanguageManager.UpdateDraft, for: .normal)
        
        self.walletAmountTextField.isEnabled = false
        self.customerTextField.delegate = self
        self.deliveryTextField.delegate = self
        
    }
    
    func initialSetUp(){
        
        if let shop = getShopData(){
            guard  let deliverySystemStatus = shop.deliverySystem else {
                return
            }
            self.deliveryStatus = deliverySystemStatus
        }
        
        guard self.selectedAmounts.count > 0 else { return }
        guard self.selectedSerials.count > 0 else { return }
        
        var temp : Int = 0
        guard self.selectedSellingPrices.count > 0 else { return }
        var totalPrice = 0.0
        for price in self.selectedSellingPrices {
            if self.selectedAmounts.count > 0{
                let tempTotal = price * self.selectedAmounts[temp]
                totalPrice += tempTotal
                temp += 1
            }
        }
        
        self.totalPrice = totalPrice
//        print(self.totalPrice)
        totalPriceLabel.text = LanguageManager.Total + " : " + "\(totalPrice)" + Constants.currencySymbol
        SaleInfoObject.payableAmount = totalPrice
        SaleInfoObject.due = totalPrice
//        payableAmountLbl.text = LanguageManager.PayableAmount + " : " + "\(totalPrice)" + Constants.currencySymbol
//        dueLbl.text = LanguageManager.Due + " : " + "\(totalPrice)" + Constants.currencySymbol
        
        if self.deliveryStatus == 1{
            self.deliveryTitleLbl.isHidden = false
            self.deliveryTextField.isHidden = false
        }else{
            self.deliveryTitleLbl.isHidden = true
            self.deliveryTextField.isHidden = true
        }
        
        self.cashText.addTarget(self, action: #selector(cashTextDidChange(_textfield:)), for: .editingChanged)
        self.walletPayTextField.addTarget(self, action: #selector(onWalletPay(_textfield:)), for: .editingChanged)
        self.discountTextField.addTarget(self, action: #selector(discountTextDidChange), for: .editingChanged)
        self.discountSegmentedControl.addTarget(self, action: #selector(onDiscountSegmentChange), for: .valueChanged)
        self.deliveryTextField.addTarget(self, action: #selector(onDeliveryChange), for: .touchUpInside)
        if pushedInFromSaleEdit == true{
            self.draftBtn.isHidden = true
        }else{
            self.draftBtn.isHidden = false
            self.draftBtn.addTarget(self, action: #selector(onDraftBtnTapped), for: .touchUpInside)
        }
        self.saleBtn.addTarget(self, action: #selector(onSaleBtnTapped), for: .touchUpInside)
    }
    
    
    
    func initialInfoSetup(){
        
        if let info = self.saleInfo{
            //Sale Edit
            self.setUpSaleInfoObject(info: info)
            
            self.saleId = info.id
            self.cashText.text = info.cash?.toString()
            
            cashLbl.text = LanguageManager.Cash + " : "  + "\(SaleInfoObject.cash ?? 0.0)" + Constants.currencySymbol
            paidAmountLbl.text = LanguageManager.PaidAmount + " : "  + "\(SaleInfoObject.paidAmount ?? 0.0)" + Constants.currencySymbol
            cashBackLbl.text = LanguageManager.CashBack + " : " + (SaleInfoObject.cashback?.toString() ?? "0.0") + Constants.currencySymbol
            payableAmountLbl.text = LanguageManager.PayableAmount + " : " + "\(SaleInfoObject.payableAmount ?? 0)" + Constants.currencySymbol
            dueLbl.text = LanguageManager.Due + " : " + "\(SaleInfoObject.due ?? 0)" + Constants.currencySymbol
            self.cashBackAmount = SaleInfoObject.cashback
            self.paymentMethodText.text = SaleInfoObject.paymentMethod
            
            self.refreshPaymentMethods(id: SaleInfoObject.paymentMethodId ?? 1)

            self.paymentPhoneNo.text = SaleInfoObject.paymentPhone
            
            self.customerTextField.text = SaleInfoObject.customerName
            
            if let wallet = SaleInfoObject.walletPay, wallet > 0.0{
                self.walletAmountTextField.text = info.customerWalletAmount
                self.walletLbl.text = LanguageManager.CustomerWallet + " : " + "\(wallet)" + Constants.currencySymbol
                self.walletPayTextField.text = "\(Int(wallet))"
            }else{
                self.walletLbl.isHidden = true
            }
            
            self.dateTextField.text = SaleInfoObject.saleDate
            
            discountLbl.text = LanguageManager.Discount + " : " + "\(SaleInfoObject.discount ?? 0.0)" + Constants.currencySymbol
            deliveryChargeLbl.text = LanguageManager.DeliveryCharge + " : "  +  "\(SaleInfoObject.deliveryCharge ?? 0.0)" + Constants.currencySymbol
            
            self.discountTextField.text = "\(SaleInfoObject.discount ?? 0)"
            if info.discountUnit == "%"{
                self.discountSegmentedControl.selectedSegmentIndex = 1
            }else{
                self.discountSegmentedControl.selectedSegmentIndex = 0
            }
            
            self.deliveryTextField.text = SaleInfoObject.deliverySystem
            
        }else if let info = self.saleEditInfo{
            //SaleDraft
            
            self.setUpSaleInfoObjectForDraft(info: info)
            
            self.cashText.text = info.cash?.toString()
            SaleInfoObject.cash = info.cash
            
            cashLbl.text = LanguageManager.Cash + " : "  + "\(SaleInfoObject.cash ?? 0.0)" + Constants.currencySymbol
            
            paidAmountLbl.text = LanguageManager.PaidAmount + " : "  + "\(SaleInfoObject.paidAmount ?? 0.0)" + Constants.currencySymbol
            cashBackLbl.text = LanguageManager.CashBack + " : " + (SaleInfoObject.cashback?.toString() ?? "0.0") + Constants.currencySymbol
            
            self.paymentMethodText.text = info.paymentMethodName ?? "Cash"
            self.refreshPaymentMethods(id: SaleInfoObject.paymentMethodId ?? 1)

            self.paymentPhoneNo.text = info.paymentNumber
            
            self.customerTextField.text = info.customerName
            SaleInfoObject.customerName = info.customerName
            SaleInfoObject.customerId = info.customerId
            
            self.customerTextField.text = SaleInfoObject.customerName
            
//            SaleInfoObject.walletPay = info.wall
            
            if let wallet = SaleInfoObject.walletPay, wallet > 0.0{
                self.walletAmountTextField.text = info.customerWalletAmount?.toString()
                self.walletLbl.text = LanguageManager.CustomerWallet + " : " + "\(wallet)" + Constants.currencySymbol
                self.walletPayTextField.text = "\(Int(wallet))"
            }else{
                self.walletLbl.isHidden = true
            }
            
            self.dateTextField.text = info.date
            
            discountLbl.text = LanguageManager.Discount + " : " + (info.discountAmount?.toString() ?? "0") + Constants.currencySymbol
            deliveryChargeLbl.text = LanguageManager.DeliveryCharge + " : "  +  "\(info.deliveryCharge ?? 0.00)" + Constants.currencySymbol
            
            self.discountTextField.text = info.discountAmount?.toString()
            if info.discountUnit == "%"{
                self.discountSegmentedControl.selectedSegmentIndex = 1
            }else{
                self.discountSegmentedControl.selectedSegmentIndex = 0
            }
            
            self.deliveryTextField.text = SaleInfoObject.deliverySystem
        }
                
    }
    
    func refreshPaymentMethods(id: Int){
        if id == 1{
            self.showPaymentNumber(bool: true)
            if SaleInfoObject.hasWallet == 1{
                showCustomerWallet(bool: false)
            }else{
                showCustomerWallet(bool: true)
            }
        }else if id == 4{
            showCustomerWallet(bool: true)
            self.showPaymentNumber(bool: true)
            self.cashText.isEnabled = false
            SaleInfoObject.walletPay = 0.0
            SaleInfoObject.cash = 0.0
        }else{
            self.cashText.isEnabled = true
            self.showPaymentNumber(bool: false)
            self.paymentPhoneNoLbl.text = LanguageManager.AccountNumber
            if SaleInfoObject.hasWallet == 1{
                showCustomerWallet(bool: false)
            }else{
                showCustomerWallet(bool: true)
            }
        }
        
    }
    
    func resetInitialSumOfProduct(){
        self.totalPrice = 0.0
        self.payableAmount = 0.0
        self.dueAmount = 0.0
        SaleInfoObject.payableAmount = 0.0
        SaleInfoObject.due = 0.0
    }
    
    @objc func cashTextDidChange(_textfield: UITextField){
        guard let cashPaid = self.cashText.text, let cash = Double(cashPaid) else {
            SaleInfoObject.cash = 0.0
            self.refreshView()
            return
        }
        SaleInfoObject.cash = cash
        self.refreshView()
    }
    
    @objc func onWalletPay(_textfield: UITextField){
        let walletPaidAmount = self.walletPayTextField.text?.toDouble() ?? 0
        
        var initialWalletPay = 0.0
        var initialWalletAmount = 0.0
        
        if let info = self.saleInfo{
            initialWalletPay = info.wallet?.toDouble() ?? 0
            initialWalletAmount = info.customerWalletAmount?.toDouble() ?? 0
        }
        
        if let draftInfo = self.saleEditInfo{
            initialWalletPay = draftInfo.wallet ?? 0
            initialWalletAmount = draftInfo.customerWalletAmount ?? 0
        }
        
        SaleInfoObject.walletPay = walletPaidAmount
                
        let maxWalletPay = (initialWalletPay) + (initialWalletAmount)
        SaleInfoObject.hasWallet = 1
        
        if walletPaidAmount > maxWalletPay{
            self.showAlert(title: Constants.currencySymbol + " \(maxWalletPay)", message: LanguageManager.YouCanNotPayMoreThan)
            
            SaleInfoObject.walletPay = maxWalletPay
            SaleInfoObject.walletAmount = initialWalletAmount.toString()
        }
        else{
            SaleInfoObject.walletAmount = "\(initialWalletAmount + (initialWalletPay - walletPaidAmount))"
        }
        self.refreshView()
       
    }
    
    @objc func onCustomer(sender: UITextField){
        self.navigateToSaleWithCustomerVC()
    }
    
    @objc func discountTextDidChange(_textfiled: UITextField){
//        guard let discounted = self.discountTextField.text, discounted != "" else{
//            SaleInfoObject.discount = 0
//            self.refreshView()
//            return
//        }
//        SaleInfoObject.discount = Double(discounted)
//        print(discounted)
//        self.refreshView()
        self.setMaxDiscount()
    }
    
    func setMaxDiscount(){
        if let discount = self.discountTextField.text?.toDouble(), let totalAmount = self.totalPrice{
            if SaleInfoObject.discountUnit == "%"{
                if discount >= 100{
                    self.discountTextField.text = "\(100)"
                }
            }else{
                if discount > totalAmount{
                    self.discountTextField.text = "\(totalAmount)"
                }
            }
            SaleInfoObject.discount = self.discountTextField.text?.toDouble()
            self.refreshView()
        }else{
            SaleInfoObject.discount = 0
            self.refreshView()
        }
    }
    
    @objc func onDiscountSegmentChange(sender: UISegmentedControl){
        if self.discountSegmentedControl.selectedSegmentIndex == 0 {
            SaleInfoObject.discountUnit = "৳"
            setMaxDiscount()
        }else{
            SaleInfoObject.discountUnit = "%"
            setMaxDiscount()
        }
        self.refreshView()
    }
    
    @objc func onDeliveryChange(sender: UITextField){
        self.navigateToSaleOnDeliveryVC()
    }
    
    func navigateToSaleWithCustomerVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleWithCustomerViewController") as! SaleWithCustomerViewController
        viewController.delegate = self
        viewController.salePanelProductInfoVC = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleListVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalesListViewController") as! SalesListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleOnDeliveryVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleOnDeliveryViewController") as! SaleOnDeliveryViewController
        viewController.delegate = self
        
        viewController.deliveryCharge = SaleInfoObject.deliveryCharge
        viewController.deliverySystemId = SaleInfoObject.deliverySystemId
        viewController.deliverySystem = SaleInfoObject.deliverySystem
        viewController.deliveryInstanceOfSalePanelProductInfoVC = self
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func isValidated()->Bool {
        if(SaleInfoObject.paymentMethodId != 1) && (SaleInfoObject.paymentMethodId != 4){
            if (self.paymentPhoneNo.text?.isEmpty)!{
                self.showAlert(title: LanguageManager.Warning, message: LanguageManager.AccountNumberIsRequired)
                return false
            }
            return true
        }else if(SaleInfoObject.paymentMethodId == 4){
            if (SaleInfoObject.deliverySystemId == 0
                ){
                self.showAlert(title: LanguageManager.Warning, message: LanguageManager.DeliverySystemIsRequired)
                return false
            }
        }
        
        if (SaleInfoObject.paymentMethodId == 1) {
            guard let cashPaid = self.cashText.text, cashPaid != "", let cash = Double(cashPaid) else{
                if self.customerTextField.text == ""{
                    self.showAlert(title: LanguageManager.Warning, message: LanguageManager.CashIsRequired)
                    return false
                }
                return true
            }
            guard let payable = SaleInfoObject.payableAmount else{
                return false
            }
            if cash < payable {
                if self.customerTextField.text == "" {
                    self.showAlert(title: LanguageManager.Warning, message: LanguageManager.CustomerIsRequired)
                    return false
                }
            }
            return true
        }else{
            if self.customerTextField.text == "" {
                self.showAlert(title: LanguageManager.Warning, message: LanguageManager.CustomerIsRequired)
                return false
            }else{
                return true
            }
        }
        
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.saleBtn.isEnabled = isEnabled
    }
    
    func setUpToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCash(sender:)))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.cashText.inputAccessoryView = toolBar
        
        let paymentPhoneNoToolBar = UIToolbar()
        paymentPhoneNoToolBar.barStyle = UIBarStyle.default
        paymentPhoneNoToolBar.isTranslucent = true
        paymentPhoneNoToolBar.tintColor = UIColor.black
        paymentPhoneNoToolBar.sizeToFit()
        
        let paymentPhoneNoDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnPaymentPhone(sender:)))
        paymentPhoneNoToolBar.setItems([cancelButton, spaceButton, paymentPhoneNoDoneButton], animated: false)
        paymentPhoneNoToolBar.isUserInteractionEnabled = true
        
        self.paymentPhoneNo.inputAccessoryView = paymentPhoneNoToolBar
        
        let walletPayToolbar = UIToolbar()
        walletPayToolbar.barStyle = UIBarStyle.default
        walletPayToolbar.isTranslucent = true
        walletPayToolbar.tintColor = UIColor.black
        walletPayToolbar.sizeToFit()
        
        let walletPaydoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnWalletPay(sender:)))
        toolBar.setItems([cancelButton, spaceButton, walletPaydoneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.walletPayTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingDoneOnCash(sender: UIBarButtonItem){
        self.cashText.resignFirstResponder()
        guard let cash = self.cashText.text, cash != "", let cashPaid = Double(cash) else{ return }
        SaleInfoObject.cash = cashPaid
        self.refreshScreen()
    }
    
    @objc func onPressingDoneOnWalletPay(sender: UIBarButtonItem){
        guard let walletPayText = self.walletPayTextField.text, walletPayText != "", let walletPay = walletPayText.toDouble() else {return}
        SaleInfoObject.walletPay = walletPay
        self.refreshScreen()
    }
    
    //PickerView
    func setUpPickerView(){
        
        paymentMethodPicker.delegate = self
        
        //PaymentMethod ToolBar
        let paymentMethodToolBar = UIToolbar()
        paymentMethodToolBar.barStyle = UIBarStyle.default
        paymentMethodToolBar.isTranslucent = true
        paymentMethodToolBar.tintColor = UIColor.black
        paymentMethodToolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let paymentDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnPaymentMethod(sender:)))
        paymentMethodToolBar.setItems([cancelButton, spaceButton, paymentDoneButton], animated: false)
        paymentMethodToolBar.isUserInteractionEnabled = true
        
        self.paymentMethodText.inputView = paymentMethodPicker
        self.paymentMethodText.inputAccessoryView = paymentMethodToolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnPaymentMethod(sender: UIBarButtonItem){
        self.paymentMethodText.resignFirstResponder()
        if self.paymentMethodId != 1 && self.paymentMethodId != 4{
            self.showPaymentNumber(bool: false)
            if SaleInfoObject.hasWallet == 1{
                self.showCustomerWallet(bool: false)
            }else{
                self.showCustomerWallet(bool: true)
            }
        }else if self.paymentMethodId == 1 {
            self.showPaymentNumber(bool: true)
            if SaleInfoObject.hasWallet == 1{
                self.showCustomerWallet(bool: false)
            }else{
                self.showCustomerWallet(bool: true)
            }
        }else if self.paymentMethodId == 4{
            self.showPaymentNumber(bool: true)
            self.showCustomerWallet(bool: true)
        }else{
            self.showPaymentNumber(bool: true)
            if SaleInfoObject.hasWallet == 1{
                self.showCustomerWallet(bool: false)
            }else{
                self.showCustomerWallet(bool: true)
            }
        }
    }
    
    @objc func onPressingDoneOnPaymentPhone(sender: UIBarButtonItem){
        if (self.paymentMethodId == 2) {
            SaleInfoObject.paymentPhone = self.paymentPhoneNo.text
            //self.submitBtn.becomeFirstResponder()
        }else if (self.paymentMethodId == 3){
            SaleInfoObject.paymentPhone = self.paymentPhoneNo.text
            //self.submitBtn.becomeFirstResponder()
        }
    }
    
    @objc func onSaleBtnTapped(sender: UIButton){
        if isValidated(){
            self.submitData()
        }
    }
    
    func submitData(){
        
        guard let totalPrice = self.totalPrice else {
            return
        }
        
        
        
        guard let totalPayable = SaleInfoObject.payableAmount else {
            return
        }
        
        let discount = SaleInfoObject.discount ?? 0
        
        let discountUnit = SaleInfoObject.discountUnit ?? ""
        
        let deliveryCharge = SaleInfoObject.deliveryCharge ?? 0
        
        let cash = SaleInfoObject.cash ?? 0
        
        let walletPay = SaleInfoObject.walletPay ?? 0.0
        //        let saleDate =
        
        guard let due = SaleInfoObject.due else {
            return
        }
        
        guard let cashBack = self.cashBackAmount else{
            return
        }
        
        let paymentMethodId = SaleInfoObject.paymentMethodId ?? 1
        
        //let deliverySystem = SaleInfoObject.deliverySystem ?? ""
        let deliverySystemId = SaleInfoObject.deliverySystemId ?? nil
        let saleToken = SaleInfoObject.deliveryToken ?? ""
        //let deliveryId = "\(String(describing: deliverySystemId))"
        
        //print(deliverySystemId)
        
        //let paymentMethodId = SaleInfoObject.paymentMethodId ?? 0
        let customerId = SaleInfoObject.customerId ?? nil
        //let cusId = "\(String(describing: customerId))"
        SaleInfoObject.paymentPhone = self.paymentPhoneNo.text
        let paymentPhoneNo = SaleInfoObject.paymentPhone ?? ""
        //        guard let invoiceNumber = self.invoice else{
        //            return
        //        }
        
        let date = SaleInfoObject.saleDate ?? currentDateFormatter(format: "yyyy-MM-dd", date: Date())
        
        var params : [String : Any] = ["products" : self.selectedProductId, "quantities" : self.selectedAmounts, "prices": self.selectedSellingPrices, "serials": self.selectedSerials, "total_amount": totalPrice, "discount": discount, "discount_unit": discountUnit, "payable_amount" : totalPayable , "cash" : cash, "due" : due , "cash_back" : cashBack, "delivery_system" : deliverySystemId, "delivery_charge" : deliveryCharge , "payment_method" : paymentMethodId, "payment_number": paymentPhoneNo, "customer" : customerId, "date": date, "wallet": walletPay, "sale_token": saleToken]
        
        
        var id : Int = 0
        if pushedInFromSaleEdit == true{
            if let sId = self.saleId{
                id = sId
                params ["id"] = id
                
                if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                    print(json)
                    
                }
                
                self.presenter.postSaleEditDataToServer(param: params)
                
            }
            
        }else{
            if let dId = self.draftId{
                id = dId
                params ["draft_id"] = id
                
                if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                    print(json)
                    
                }
                
                self.presenter.postSalesDataToServer(params: params)
                
            }
            
        }
        
        
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigateToSaleListVC()
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayDraftMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.popToSaleDraftListViewController()
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSaleDraftListViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is SaleDraftListViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func displayPreOrderMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.popToSalePreOrderListViewController()
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSalePreOrderListViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is SalePreorderListViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    @objc func onDraftBtnTapped(sender: UIButton){
        self.submitDraftData()
    }
    
    func submitDraftData(){
        guard let totalPrice = self.totalPrice else {
            return
        }
        
        guard let id = self.draftId else{
            return
        }
        
        let totalPayable = SaleInfoObject.payableAmount ?? 0
        
        let discount = SaleInfoObject.discount ?? 0
        
        let discountUnit = SaleInfoObject.discountUnit ?? ""
        
        let deliveryCharge = SaleInfoObject.deliveryCharge ?? 0
        
        let cash = SaleInfoObject.cash ?? 0
        
        let walletPay = SaleInfoObject.walletPay ?? 0.0
        
        let due = SaleInfoObject.due ?? 0
        
        let cashBack = self.cashBackAmount ?? 0
        
        let paymentMethodId = SaleInfoObject.paymentMethodId ?? 1
        
        let deliverySystemId = SaleInfoObject.deliverySystemId ?? nil
        let deliveryToken = SaleInfoObject.deliveryToken ?? ""
        
        let customerId = SaleInfoObject.customerId ?? nil
        SaleInfoObject.paymentPhone = self.paymentPhoneNo.text
        let paymentPhoneNo = SaleInfoObject.paymentPhone ?? ""
       
        let date = SaleInfoObject.saleDate ?? currentDateFormatter(format: "yyyy-MM-dd", date: Date())
        
        let params : [String : Any] = ["draft_id": id, "products" : self.selectedProductId, "quantities" : self.selectedAmounts, "prices": self.selectedSellingPrices, "serials": self.selectedSerials, "total_amount": totalPrice, "discount": discount, "discount_unit": discountUnit, "payable_amount" : totalPayable , "cash" : cash, "due" : due , "cash_back" : cashBack, "delivery_system" : deliverySystemId, "delivery_charge" : deliveryCharge , "payment_method" : paymentMethodId, "payment_number": paymentPhoneNo, "customer" : customerId, "date": date, "wallet": walletPay, "sale_token": deliveryToken,"step_no": 2]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
            
        }
        self.presenter.postSaleDraftStoreDataToServer(param : params)
        
    }
    
}

extension SalePanelProductInfoViewController {
    func refreshView(){
        
        // সর্বমোট
        guard self.selectedAmounts.count > 0 else { return }
        guard self.selectedSerials.count > 0 else { return }
        
        var temp : Int = 0
        guard self.selectedSellingPrices.count > 0 else { return }
        var totalPrice = 0.0
        for price in self.selectedSellingPrices {
            if self.selectedAmounts.count > 0{
                let tempTotal = price * self.selectedAmounts[temp]
                totalPrice += tempTotal
                temp += 1
            }
        }
        
        totalPriceLabel.text = LanguageManager.Total + " : "  + " " + "\(totalPrice)" + Constants.currencySymbol
        
        var cWallet = SaleInfoObject.walletPay ?? 0.0
        if cWallet < 0{
            cWallet = 0
        }
        if SaleInfoObject.hasWallet == 1{
            self.walletLbl.text = LanguageManager.CustomerWallet + ": " + "\(cWallet)" + Constants.currencySymbol
            self.walletAmountTextField.text = SaleInfoObject.walletAmount
            self.walletPayTextField.text = "\(Int(SaleInfoObject.walletPay ?? 0))"
            self.showCustomerWallet(bool: false)
        }
        
        let cash = SaleInfoObject.cash ?? 0
        
        self.cashLbl.text = LanguageManager.Cash + " : " + "\(cash)" + Constants.currencySymbol
        
        let paidAmount = (SaleInfoObject.cash ?? 0) + (SaleInfoObject.walletPay ?? 0)
        
        self.paidAmountLbl.text = LanguageManager.PaidAmount + " : " + "\(paidAmount)"

        var totalDiscount = 0.0
        
        let discount = SaleInfoObject.discount ?? 0
        
        if let unit = SaleInfoObject.discountUnit , unit == "৳" {
            totalDiscount = discount
        }else if SaleInfoObject.discountUnit ?? "" == "%" {
            let discounted = (totalPrice * (discount/100))
            totalDiscount = discounted
        }
        print(totalDiscount)
        self.discountLbl.text = LanguageManager.Discount + " : " + "\(totalDiscount)"
        
        let deliveryCharge = SaleInfoObject.deliveryCharge ?? 0.0
        self.deliveryChargeLbl.text = LanguageManager.DeliveryCharge + " : " + " \(deliveryCharge)" + Constants.currencySymbol
        self.deliveryTextField.text = SaleInfoObject.deliverySystem
        
        var totalPayable = totalPrice + deliveryCharge - totalDiscount
        
        if totalPayable < 0 {
            totalPayable = 0.0
        }
        SaleInfoObject.payableAmount = totalPayable
        self.payableAmountLbl.text = LanguageManager.PayableAmount + ": " + " \(SaleInfoObject.payableAmount ?? 0.0)" + Constants.currencySymbol
        
        var totalDue = 0.0
        var totalReturn = 0.0
        
        if totalPayable > paidAmount{
            totalDue = totalPayable - (paidAmount)
            totalReturn = 0.0
        }
        else if totalPayable < paidAmount{
            totalDue = 0.0
            totalReturn = paidAmount - totalPayable
        }
        else{
            totalDue = 0.0
            totalReturn = 0.0
        }
        SaleInfoObject.due = totalDue
        
        self.dueLbl.text = LanguageManager.Due + " : " + " \(SaleInfoObject.due ?? 0)" + Constants.currencySymbol
        
        self.cashBackLbl.text = LanguageManager.CashBack + " : " + " \(totalReturn)" + Constants.currencySymbol
        SaleInfoObject.cashback = totalReturn
        self.cashBackAmount = totalReturn
        self.customerTextField.text = SaleInfoObject.customerName ?? ""
        let paymentMethodId = SaleInfoObject.paymentMethodId
        if paymentMethodId == 4{
            self.showCustomerWallet(bool: true)
            self.showPaymentNumber(bool: true)
        }else if paymentMethodId == 1{
            self.showPaymentNumber(bool: true)
        }else{
            self.showPaymentNumber(bool: false)
        }
        
    }
}

//Mark: TextField Delegate
extension SalePanelProductInfoViewController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.customerTextField{
            self.navigateToSaleWithCustomerVC()
        }else if textField == self.deliveryTextField{
            self.navigateToSaleOnDeliveryVC()
        }
        return false
    }
}

//Mark: PickerViewDelegate
extension SalePanelProductInfoViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.paymentMethodPicker{
            return paymentMethodList.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.paymentMethodPicker{
            if self.paymentMethodList.count > 0 {
                let item = self.paymentMethodList[row]
                self.paymentMethodId = item.id
                self.paymentMethodText.text = item.name
                if item.id != 1 || item.id != 4 {
                    self.showPaymentNumber(bool: false)
                    self.paymentPhoneNo.placeholder = LanguageManager.AccountNumber
                    self.showCustomerWallet(bool: true)
                }else if item.id == 4{
                    self.showCustomerWallet(bool: true)
                    self.showPaymentNumber(bool: true)
                }else if item.id == 1{
                    self.showPaymentNumber(bool: true)
                    if SaleInfoObject.hasWallet == 1{
                        self.showCustomerWallet(bool: false)
                    }else{
                        self.showCustomerWallet(bool: true)
                    }
                }else{
                    self.cashText.isEnabled = true
                    self.showPaymentNumber(bool: true)
                    if SaleInfoObject.hasWallet == 1{
                        self.showCustomerWallet(bool: false)
                    }else{
                        self.showCustomerWallet(bool: true)
                    }
                }
                return item.name
            }
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.paymentMethodPicker{
            if self.paymentMethodList.count > 0 {
                let list = self.paymentMethodList[row]
                self.paymentMethodText.text = list.name
                self.paymentMethodId = list.id
                if self.paymentMethodId == 4 {
                    self.cashText.isEnabled = false
                    self.cashText.text = ""
                    SaleInfoObject.cash = 0.0
                    self.paymentMethodId = list.id
                    SaleInfoObject.paymentMethodId = list.id
                    self.showPaymentNumber(bool: true)
                    SaleInfoObject.walletPay = 0.0
                    self.showCustomerWallet(bool: true)
                    self.refreshView()
                }else if self.paymentMethodId == 1{
                    self.showPaymentNumber(bool: true)
                    if SaleInfoObject.hasWallet == 1{
                        self.showCustomerWallet(bool: false)
                    }else{
                        self.showCustomerWallet(bool: true)
                    }
                    self.refreshView()
                }else if (self.paymentMethodId != 4 && self.paymentMethodId != 1){
                    self.cashText.isEnabled = true
                    self.showPaymentNumber(bool: false)
                    //self.paymentMethodId = list.id
                    SaleInfoObject.paymentMethodId = list.id
                    self.paymentPhoneNo.placeholder = LanguageManager.AccountNumber
                    if SaleInfoObject.hasWallet == 1{
                        self.showCustomerWallet(bool: false)
                    }else{
                        self.showCustomerWallet(bool: true)
                    }
                    self.refreshView()
                }else{
                    self.cashText.isEnabled = true
                    self.paymentMethodId = list.id
                    SaleInfoObject.paymentMethodId = list.id
                    self.showPaymentNumber(bool: true)
                    if SaleInfoObject.hasWallet == 1{
                        self.showCustomerWallet(bool: false)
                    }else{
                        self.showCustomerWallet(bool: true)
                    }
                    self.refreshView()
                }
            }else{
                return
            }
        }
    }
    
}

//Mark: SetData
extension SalePanelProductInfoViewController : SaleInfoRefreshDelegate{
    func refreshScreen() {
        self.refreshView()
    }
}

//Mark: SaleDate
extension SalePanelProductInfoViewController{
    func showSaleDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        dateTextField.text = currentDateFormatter(format: "dd MMM, yyyy", date: datePicker.date)
        SaleInfoObject.saleDate = currentDateFormatter(format: "yyyy-MM-dd", date: datePicker.date)
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func currentDateFormatter(format: String, date: Date) -> String{
        dateFormatter.dateFormat = format
        let currentDate = dateFormatter.string(from: date)
        return currentDate
    }
    
}

extension SalePanelProductInfoViewController{
    func showCustomerWallet(bool: Bool){
        self.walletLbl.isHidden = bool
        self.walletPayLbl.isHidden = bool
        self.walletAmountTitleLbl.isHidden = bool
        self.walletAmountTextField.isHidden = bool
        self.walletPayTextField.isHidden = bool
    }
    
    func showPaymentNumber(bool: Bool){
        self.paymentPhoneNoLbl.isHidden = bool
        self.paymentPhoneNo.isHidden = bool
    }
}

//Mark: Api Delegate
extension SalePanelProductInfoViewController : SalePanelProductInfoViewDelegate {
    func onDraftStoreSuccessful(data: AddDataMapper) {
        self.resetBundleData()
        guard let message = data.message else {
            return
        }
        self.displayDraftMessage(userMessage: message)
    }
    
    func setPaymentMethod(data: PaymentMethodDataMapper) {
        guard let list = data.paymentMethodList, list.count > 0 else {
            return
        }
        self.paymentMethodList += list
    }
    
    func onSuccess(data: AddDataMapper) {
        self.resetBundleData()
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onSuccessfulPreOrderUpdate(data: AddDataMapper) {
        self.resetBundleData()
        guard let message = data.message else {
            return
        }
        self.displayPreOrderMessage(userMessage: message)
    }
    
    fileprivate func resetBundleData(){
        SaleObject.resetBundleData()
    }
    
    func onFailed(message: String) {
        self.showAlert(title: message, message: "")
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getPaymentMethodDataFromServer()
    }
    
}

extension SalePanelProductInfoViewController{
    func setUpSaleInfoObject(info: SaleInfo){
            
            SaleInfoObject.paymentMethod = info.paymentMethodName
            SaleInfoObject.paymentMethodId = info.paymentMethod
            SaleInfoObject.paymentPhone = info.paymentNumber
            SaleInfoObject.customerName = info.customerName
            SaleInfoObject.customerId = info.customerId
            SaleInfoObject.walletAmount = info.customerWalletAmount
            SaleInfoObject.walletPay = info.wallet?.toDouble()
            
    //        var cashAmount = info.cash?.toDouble()
            if let wallet = SaleInfoObject.walletAmount?.toDouble(), wallet > 0.0{
                SaleInfoObject.hasWallet = 1
            }else{
                SaleInfoObject.hasWallet = 0
            }
            SaleInfoObject.cash = info.cash
            SaleInfoObject.paidAmount = (SaleInfoObject.cash ?? 0.0) + (SaleInfoObject.walletPay ?? 0.0)
            SaleInfoObject.saleDate = info.date
            SaleInfoObject.discount = info.discountAmount?.toDouble()
            if info.discountUnit != nil{
                if info.discountUnit == "%"{
                    SaleInfoObject.discountUnit = "%"
                }else{
                    SaleInfoObject.discountUnit = "৳"
                }
            }else{
                SaleInfoObject.discountUnit = "৳"
            }
            SaleInfoObject.deliverySystemId = info.deliverySystem
            SaleInfoObject.deliveryCharge = info.deliveryCharge?.toDouble()
            SaleInfoObject.deliverySystem = info.deliverySystemName
            SaleInfoObject.deliveryToken = info.saleToken
            self.refreshView()
        }
    
    func setUpSaleInfoObjectForDraft(info: SaleEditInfo){
        
        SaleInfoObject.paymentMethod = info.paymentMethodName
        SaleInfoObject.paymentMethodId = info.paymentMethod
        SaleInfoObject.paymentPhone = info.paymentNumber
        SaleInfoObject.customerName = info.customerName
        SaleInfoObject.customerId = info.customerId
        SaleInfoObject.walletAmount = info.customerWalletAmount?.toString()
        SaleInfoObject.walletPay = info.wallet
        
        if let wallet = SaleInfoObject.walletAmount?.toDouble(), wallet > 0.0{
            SaleInfoObject.hasWallet = 1
        }else{
            SaleInfoObject.hasWallet = 0
        }
        SaleInfoObject.cash = info.cash
        SaleInfoObject.paidAmount = (SaleInfoObject.cash ?? 0.0) + (SaleInfoObject.walletPay ?? 0.0)
        SaleInfoObject.saleDate = info.date
        SaleInfoObject.discount = info.discountAmount
        if info.discountUnit != nil{
            if info.discountUnit == "%"{
                SaleInfoObject.discountUnit = "%"
            }else{
                SaleInfoObject.discountUnit = "৳"
            }
        }else{
            SaleInfoObject.discountUnit = "৳"
        }
        SaleInfoObject.deliverySystemId = info.deliverySystem
        SaleInfoObject.deliveryCharge = info.deliveryCharge
        SaleInfoObject.deliverySystem = info.deliverySystemName
        SaleInfoObject.deliveryToken = info.saleToken
        self.refreshView()
    }
}
