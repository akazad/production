//
//  SalesPerDaySearchViewController.swift
//  Ponno
//
//  Created by a k azad on 23/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SalesPerDaySearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = SalesPerDaySearchPresenter(service: SalesService())
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var salePerDaylist : [SaleListDateWise] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var searchText = ""
    var timer : Timer?
    
    var selectedDate : String?
    var perdaySaleId: Int?
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpSearchBar()
        self.configureTableView()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: LanguageManager.Yes, style: .default){
                (action:UIAlertAction!) in
                    self.salePerDaylist = []
                    //self.refreshTableView()
                    guard let date = self.selectedDate else{
                        return
                    }
                    
                self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: date)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: LanguageManager.Yes, style: .default){
                (action:UIAlertAction!) in
                self.presenter.cancelPerDaySaleData(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: LanguageManager.No, style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    guard let date = self.selectedDate else{
                        return
                    }
                    
                    self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: date)
                }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
    }

}

//Mark: Search Delegate
extension SalesPerDaySearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.SearchSerial
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.salePerDaylist = []
        self.isLoading = true
        self.isSearchActive = true
        guard let date = self.selectedDate else {
            return
        }
         self.presenter.getSalesPerDaySearchDataFromServer(page: self.currentPage, date: date, searchText: self.searchText)
    }
}

// MARK: TableView Delegate & Data Source
extension SalesPerDaySearchViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SalesPerDayCell.nib, forCellReuseIdentifier: SalesPerDayCell.identifier)
        self.tableView.tableFooterView = UIView()
        //self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.salePerDaylist.count > 0 {
            return self.salePerDaylist.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SalesPerDayCell = tableView.dequeueReusableCell(withIdentifier: SalesPerDayCell.identifier, for: indexPath) as! SalesPerDayCell
        cell.selectionStyle = .none
        
        if self.salePerDaylist.count > 0 {
            let saleItem = self.salePerDaylist[indexPath.row]
            
            cell.salePerDay = saleItem
            
            cell.totalLabel.text = LanguageManager.Total + " : " + "\(saleItem.total)" + " " + Constants.currencySymbol
            cell.invoiceLabel.text = saleItem.invoice
            cell.dueLabel.text = LanguageManager.Due + " : " + "\(saleItem.due)" + Constants.currencySymbol
            cell.timeLabel.text = saleItem.time.convertDateFormater(inputDateFormat: DateFormats.HH_mm_ss.rawValue, outputDateFormat: DateFormats.h_mm_a.rawValue)
           if saleItem.customerName.isEmpty{
                cell.customerNameLbl.text = LanguageManager.CustomerName + " : " + "N/A"
            }else{
                cell.customerNameLbl.text = LanguageManager.CustomerName + " : " + saleItem.customerName
            }
            
            
            cell.popUpBtn.tag = saleItem.id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.salePerDaylist.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                guard let date = self.selectedDate else{
                    return cell
                }
                self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: date)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.salePerDaylist.count > 0 {
            let saleItem = self.salePerDaylist[indexPath.row]
            self.navigateToSaleDetailsVC(iD: saleItem.id)
        }
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        viewController.iD = iD
        viewController.state = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }

    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            
            if self.salePerDaylist.count > 0{
                let salePerDay = self.salePerDaylist[indexPath.row]
                self.perdaySaleId = salePerDay.id
                
                guard let id = self.perdaySaleId else{
                    return
                }
                self.confirmationMessage(userMessage: "Are you sure?", deleteId: id)
            }
            
        }
        delete.backgroundColor = UIColor.red
        
        return [delete]
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.salePerDaylist.count > 0{
            self.perdaySaleId = sender.tag
            for saleItem in self.salePerDaylist{
                if self.perdaySaleId == saleItem.id{
                    
                    let due = saleItem.due
                    
                    if due != "0.00"{
                        let duePaidAction = UIAlertAction(title: due + Constants.currencySymbol + " " + LanguageManager.Paid, style: UIAlertAction.Style.default) { (action) in
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        duePaidAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(duePaidAction)
                        myActionSheet.addAction(cancelAction)
                    }else{
                        guard let id = self.perdaySaleId else{
                            return
                        }
                        let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                            self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(deleteAction)
                        myActionSheet.addAction(cancelAction)
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension SalesPerDaySearchViewController : SalesPerDaySearchViewDelegate{
    func setSalesListData(data: SalePerDayDataMapper) {
        guard let list = data.saleListDateWise, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.salePerDaylist += list
    }
    
    func cancelPerDaySales(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(message: String) {
        self.searchAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let date = self.selectedDate else{
            return
        }
       self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: date)
    }
}
