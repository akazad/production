//
//  SaleOnCashViewController.swift
//  Ponno
//
//  Created by a k azad on 2/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol SaleInfoRefreshDelegate {
    func refreshScreen()
}

enum DiscountUnits : String {
    case Currency = "%"
    case Percentage = "৳"
}

class SaleOnCashViewController: UIViewController {

    @IBOutlet weak var discountText: UITextField!
    @IBOutlet weak var discountUnitText: UITextField!
    @IBOutlet weak var paymentMethodText: UITextField!
    @IBOutlet weak var newPaymentMethodText: UITextField!
    @IBOutlet weak var cashText: UITextField!
    @IBOutlet weak var newPaymentHeight: NSLayoutConstraint!
    
    private var presenter = SaleOnCashPresenter(service: SalesService())
    
    var discountUnitPicker = UIPickerView()
    var paymentMethodPicker = UIPickerView()
    
    var paymentMethodList : [PaymentMethodList] = []
    var discountUnits: [String] = [DiscountUnits.Currency.rawValue, DiscountUnits.Percentage.rawValue]
    
    var discountPrice : Double?
    var discountUnit : String?
    var payableAmount : Double?
    var paymentMethod : Int?
    var paymentMethodId : String?
    
    //
    var delegate: SaleInfoRefreshDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        self.initialSetUp()
        self.setUpPickerView()
        self.setTextFields()
    }
    
    func setTextFields(){
        self.discountText.addTarget(self, action: #selector(discountTextFieldDidChange), for: .editingChanged)
        self.discountUnitText.addTarget(self, action: #selector(discountUnitTextFieldDidChange), for: .editingChanged)
        self.paymentMethodText.addTarget(self, action: #selector(paymentMethodTextFieldDidChange), for: .editingChanged)
        self.newPaymentMethodText.addTarget(self, action: #selector(newPaymentMethodTextDidChange), for: .editingChanged)
        self.cashText.addTarget(self, action: #selector(cashTextFieldDidChange), for: .editingChanged)
    }
    
    @objc func discountTextFieldDidChange(textfield : UITextField){
        if let discountText = textfield.text, let discount = Double(discountText){
            SaleInfoObject.discount = discount
            self.delegate?.refreshScreen()
        }
    }
    
    @objc func discountUnitTextFieldDidChange(textfield : UITextField){
        if let discountUnitText = textfield.text{
            SaleInfoObject.discountUnit = discountUnitText
            //self.delegate?.refreshScreen()
        }
    }
    
    @objc func paymentMethodTextFieldDidChange(textfield : UITextField){
        if let paymentMethod = textfield.text, paymentMethod != "" {
            SaleInfoObject.paymentMethod = paymentMethod
            //self.delegate?.refreshScreen()
        }
    }
    
    @objc func newPaymentMethodTextDidChange(textfield : UITextField){
        if let newMethod = textfield.text, newMethod != "" {
            SaleInfoObject.newPayment = newMethod
        }
    }
    
    @objc func cashTextFieldDidChange(textfield : UITextField){
        if let cashText = textfield.text, let cash = Double(cashText){
            SaleInfoObject.cash = cash
            self.delegate?.refreshScreen()
        }
    }
    
    //Initial SetUp couldn't serve what is required :(
    func initialSetUp(){
        self.discountUnitPicker.selectedRow(inComponent: 0)
        self.paymentMethodPicker.selectedRow(inComponent: 0)
    }
    
    //PickerView
    func setUpPickerView(){

        paymentMethodPicker.delegate = self
        discountUnitPicker.delegate = self
        
        
        //DiscountUnit ToolBar
        let discountUnitToolBar = UIToolbar()
        discountUnitToolBar.barStyle = UIBarStyle.default
        discountUnitToolBar.isTranslucent = true
        discountUnitToolBar.tintColor = UIColor.black
        discountUnitToolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let discountUnitDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneInDiscountUnit(sender:)))
        
        discountUnitToolBar.setItems([cancelButton, spaceButton, discountUnitDoneButton], animated: false)
        discountUnitToolBar.isUserInteractionEnabled = true
        
        self.discountUnitText.inputView = discountUnitPicker
        self.discountUnitText.inputAccessoryView = discountUnitToolBar
        
        //PaymentMethod ToolBar
        let paymentMethodToolBar = UIToolbar()
        paymentMethodToolBar.barStyle = UIBarStyle.default
        paymentMethodToolBar.isTranslucent = true
        paymentMethodToolBar.tintColor = UIColor.black
        paymentMethodToolBar.sizeToFit()
        
        let paymentDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnPaymentMethod(sender:)))
        paymentMethodToolBar.setItems([cancelButton, spaceButton, paymentDoneButton], animated: false)
        paymentMethodToolBar.isUserInteractionEnabled = true
        
        self.paymentMethodText.inputView = paymentMethodPicker
        self.paymentMethodText.inputAccessoryView = paymentMethodToolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneInDiscountUnit(sender : UIBarButtonItem){
        guard let discountUnit = self.discountUnitText.text, discountUnit != "" else{ return}
        self.discountUnit = discountUnit
        self.discountUnitText.resignFirstResponder()
        
        //self.refreshScreen()
    }
    
    @objc func onPressingDoneOnPaymentMethod(sender: UIBarButtonItem){
        self.paymentMethodText.resignFirstResponder()
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: "Successful", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popToRootViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: PickerViewDelegate
extension SaleOnCashViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.discountUnitPicker{
            return discountUnits.count
        }
        else if pickerView == self.paymentMethodPicker{
            return paymentMethodList.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.discountUnitPicker{
            if self.discountUnits.count > 0 {
                let unit = self.discountUnits[row]
                self.discountUnitText.text = unit
                return self.discountUnits[row]
            }else{
                return ""
            }
        }
        else if pickerView == self.paymentMethodPicker{
            if self.paymentMethodList.count > 0 {
                let list = self.paymentMethodList[row]
                self.paymentMethodText.text = list.name 
                return self.paymentMethodList[row].name
            }else{
                return ""
            }
        }
        else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.discountUnitPicker{
            if self.discountUnits.count > 0 {
                let unit = self.discountUnits[row]
                self.discountUnitText.text = unit
            }
        }
        else if pickerView == self.paymentMethodPicker{
            if self.paymentMethodList.count > 0 {
                let list = self.paymentMethodList[row]
                self.paymentMethodText.text = list.name
                self.paymentMethodId = "\(list.id)"
            }else{
                return
            }
        }else{
            return
        }
    }
}

//extension Array{
//    mutating func appendAtBeginning(newItem: PaymentMethodList){
//        let copy = self
//        self = []
//        self.append(newItem)
//        self.append(contentsOf: <#T##Sequence#>)
//    }
//}

//Mark: Api Delegate
extension SaleOnCashViewController: SaleOnCashViewDelegate{
    func setPaymentMethodData(data: PaymentMethodDataMapper) {
        guard let list = data.paymentMethodList, list.count > 0 else{
            return
        }
        self.paymentMethodList = list
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(message: String) {
        self.showAlert(title: message, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getPaymentMethodDataFromServer()
    }
    
}
