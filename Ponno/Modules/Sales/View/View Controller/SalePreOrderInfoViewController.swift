//
//  SalePreOrderInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 14/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

struct PreOrderInfoObject {
    static var total : Double?
    static var customer: Int?
    static var customerName : String?
    static var description: String?
    static var date: String?
    
    static func resetPreOrderInfoObject(){
        self.total = nil
        self.customer = nil
        self.description = nil
        self.date = nil
    }
}

protocol SalePreOrderInfoRefreshDelegate {
    func refreshScreen()
}

class SalePreOrderInfoViewController: UIViewController {

    @IBOutlet weak var totalLbl: UILabel!{
        didSet{
            self.setUpCardView(uiview: totalLbl)
        }
    }
    @IBOutlet weak var customerLbl: UILabel!
    @IBOutlet weak var customerTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!{
        didSet{
            self.setUpCardView(uiview: submitBtn)
        }
    }
    @IBOutlet weak var updateBtn: UIButton!{
        didSet{
            self.setUpCardView(uiview: updateBtn)
        }
    }
    
    fileprivate var presenter = SalePreOrderInfoPresenter(service: SalesService())
    
    //
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedSellingPrices : [Double] = []
    var selectedSerials : [String] = []
    var totalPrice : Double?
    
    let dateFormatter = DateFormatter()
    var datePicker = UIDatePicker()
    
    var preOrderId: Int?
    var salePreOrderEditInfo : SalePreOrderEditInfo?
    var editedProducts : [SalePreOrderEditProducts] = []
    var editableProducts : [SalePreOrderEditProducts] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
        self.showPreOrderDatePicker()
        self.attachPresneter()
    }
    
    func back(sender: UIBarButtonItem){
        PreOrderInfoObject.resetPreOrderInfoObject()
    }
    
    func initialSetup(){
        self.setUpInitialLanguage()
        
        guard self.selectedAmounts.count > 0 else { return }
        //guard self.selectedSerials.count > 0 else { return }

        var temp : Int = 0
        guard self.selectedSellingPrices.count > 0 else { return }
        var totalSellingPrice = 0.0
        for price in self.selectedSellingPrices {
            if self.selectedAmounts.count > 0{
                let tempTotal = price * self.selectedAmounts[temp]
                totalSellingPrice += tempTotal
                temp += 1
            }
        }
        
        self.totalPrice = totalSellingPrice
        PreOrderInfoObject.total = totalPrice
        self.totalLbl.text = " " + LanguageManager.Total + " : " + "\(totalSellingPrice)" + Constants.currencySymbol + "  "
        
        self.customerLbl.text = LanguageManager.CustomerName
        self.descriptionLbl.text = LanguageManager.Description
        self.dateLbl.text = LanguageManager.DeliveryDate

        if (self.preOrderId != nil){
            if let info = self.salePreOrderEditInfo{
                self.customerTextField.text = info.customerName
                SalePreOrderInfoObject.customerName = info.customerName
                SalePreOrderInfoObject.customerId = info.customerId
                SalePreOrderInfoObject.date = info.deliveryDate
                SalePreOrderInfoObject.preOrderId = self.preOrderId
                PreOrderInfoObject.customer = info.customerId
                PreOrderInfoObject.description = info.description
                self.descriptionTextField.text = info.description
                self.dateTextField.text = info.deliveryDate
                PreOrderInfoObject.date = info.deliveryDate
                self.updateBtn.isHidden = false
                self.updateBtn.setTitle(LanguageManager.Update, for: .normal)
                self.submitBtn.setTitle(LanguageManager.Sale, for: .normal)
                if checkProductQuantity(data: self.editedProducts) == false{
                    self.submitBtn.isEnabled = false
                    self.submitBtn.setTitleColor(UIColor.init(red: 189, green: 189, blue: 189), for: .normal)
                    self.submitBtn.backgroundColor = UIColor.init(red: 234, green: 234, blue: 234)
                }else{
                    self.submitBtn.isEnabled = true
                }
            }
        }else{
            self.customerTextField.placeholder = LanguageManager.Customer
            self.descriptionTextField.placeholder = LanguageManager.Description
            self.dateTextField.text = currentDateFormatter(format: "dd MMM, yyy", date: Date())
            PreOrderInfoObject.date = currentDateFormatter(format: "yyyy-MM-dd", date: Date())
            self.updateBtn.isHidden = true
            self.submitBtn.setTitle(LanguageManager.Complete, for: .normal)
        }
       
        self.customerTextField.delegate = self
        self.updateBtn.addTarget(self, action: #selector(onUpdateBtnTapped), for: .touchUpInside)
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if checkProductQuantity(data: self.editedProducts) == true && self.preOrderId != nil{
            self.navigateToSalePreOrderedProductListVC()
        }else{
            if isValidated(){
                self.submitPreOrderData()
            }
        }
        
    }
    
    @objc func onUpdateBtnTapped(sender: UIButton){
        if isValidated(){
            self.submitPreOrderData()
        }
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
        self.updateBtn.isEnabled = isEnabled
    }
    
    func isValidated() -> Bool{
        if self.customerTextField.text == ""{
            self.showAlert(title: LanguageManager.CustomerIsRequired, message: "")
            return false
        }
        return true
    }
    
    func submitPreOrderData(){
        if self.selectedProductId.count > 0{
            
            guard self.selectedAmounts.count > 0 else { return }
            guard self.selectedSerials.count > 0 else { return }
            guard self.selectedSellingPrices.count > 0 else { return }
            
            // সর্বমোট
            let totalPrice = self.totalSelligPrice(amounts: self.selectedAmounts, sellingPrices: self.selectedSellingPrices)
            let preOrderDate = PreOrderInfoObject.date ?? ""
            let description = self.descriptionTextField.text ?? ""
            PreOrderInfoObject.description = description
            
            if let customerId = PreOrderInfoObject.customer{
                let params : [String : Any] = ["pre_order_id": self.preOrderId ?? nil, "products" : self.selectedProductId, "quantities" : self.selectedAmounts, "prices": self.selectedSellingPrices, "serials": self.selectedSerials, "total_amount": totalPrice, "customer" : customerId, "date": preOrderDate, "description": description]
                
                if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                    print(json)
                }
                
                self.presenter.postSalePreOrderDataStoreToServer(param: params)
            }
        }
    }
    
    func navigateToSalePreOrderedProductListVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalePreOrderedProductListViewController") as! SalePreOrderedProductListViewController
        viewController.editedProducts = self.editedProducts
        //viewController.editableProducts = self.editableProducts
        viewController.selectedProductId = self.selectedProductId
        viewController.selectedAmounts = self.selectedAmounts
        viewController.selectedSerials = self.selectedSerials
        viewController.selectedSellingPrices = self.selectedSellingPrices
        self.navigationController?.pushViewController(viewController, animated: true)
    }

}

//Mark: TextField Delegate
extension SalePreOrderInfoViewController : UITextFieldDelegate{
    func configureTextField(){
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.customerTextField{
            self.navigateToSaleWithCustomerVC()

        }
        return false
    }
    
    func navigateToSaleWithCustomerVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleWithCustomerViewController") as! SaleWithCustomerViewController
        viewController.preOrderDelegate = self
        //viewController.saleInfoVCInstance = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: SetData
extension SalePreOrderInfoViewController : SalePreOrderInfoRefreshDelegate{
    func refreshScreen() {
        self.refreshView()
    }
    
    func refreshView(){
        
        self.customerTextField.text =  PreOrderInfoObject.customerName
    }
}

//Mark: SaleDate
extension SalePreOrderInfoViewController{
    func showPreOrderDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        dateTextField.text = currentDateFormatter(format: "dd MMM, yyyy", date: datePicker.date)
        PreOrderInfoObject.date = currentDateFormatter(format: "yyyy-MM-dd", date: datePicker.date)
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func currentDateFormatter(format: String, date: Date) -> String{
        dateFormatter.dateFormat = format
        let currentDate = dateFormatter.string(from: date)
        return currentDate
    }
    
}

//Mark: Api Delegate
extension SalePreOrderInfoViewController : SalePreOrderInfoViewDelegate{
    func onPreOrderStoreSuccessful(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.showMessage(userMessage: message)
    }
    
    func onFailed(message: String) {
        self.showAlert(title: message, message: "")
    }
    
    func showLoading() {
        submitBtnControlWith(isEnabled : false)
        self.showLoader()
    }
    
    func hideLoading() {
        submitBtnControlWith(isEnabled : true)
        self.hideLoader()
    }
    
    func attachPresneter(){
        self.presenter.attachView(viewDelegate: self)
    }
    
}

extension SalePreOrderInfoViewController{
    
    func showMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: userMessage, message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigateToSaleListViewController()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToSaleListViewController(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalesListViewController") as! SalesListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func checkProductQuantity(data: [SalePreOrderEditProducts]) -> Bool{
        var quantityCheck : Bool = true
        for item in data{
            if item.quantity < item.soldQuantity{
                quantityCheck = false
            }
        }
        
        return quantityCheck
    }
    
}

