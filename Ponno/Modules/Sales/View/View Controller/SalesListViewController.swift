//
//  SalesListViewController.swift
//  Ponno
//
//  Created by a k azad on 19/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SVProgressHUD
import Floaty

protocol NavigateToSpecificController: NSObjectProtocol {
    func navigateToSalePreOrderVC()
    func navigateToSaleDraftVC()
}

class SalesListViewController: BaseViewController {

    @IBOutlet weak var dateSearchHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    
    var datePicker = UIDatePicker()
    
    var backgroundView : UIView?
    
    private var presenter = SalesListPresenter(service: SalesService())
    
    var salesList : [SalesListData] = [] {
        didSet{
            self.refreshTableView()
        }
    }
    var salesSummery : [Summary]?
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    var searchIsActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var searchPage: Int = 1
    var searchEnable : Bool = false
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.configureCollectionView()
        self.configureTableView()
        
        self.addFloaty()
        self.initialSetUp()
        self.showStartDatePicker()
        self.showEndDatePicker()
        //self.dateFormatters()
    }

    
    func initialSetUp(){
        self.generatingDate()
        self.searchBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        self.setBarButton()
        self.dateSearchHeight.constant = 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.setNavigationBarTitle()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func navigateToSaleProductsReturnVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleProductsReturnViewController") as! SaleProductsReturnViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.SaleBook
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarButton(){
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onCustomerSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let dateSearchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        dateSearchBtn.setImage(UIImage(named: "dateSearch"), for: UIControl.State.normal)
        dateSearchBtn.addTarget(self, action: #selector(onDateSearch(sender:)), for: UIControl.Event.touchUpInside)
        dateSearchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        dateSearchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        dateSearchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: dateSearchBtn)
        
        let popUpBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        popUpBtn.setImage(UIImage(named: "popupmenudark"), for: UIControl.State.normal)
        popUpBtn.addTarget(self, action: #selector(onBarPopUpBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        popUpBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        popUpBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        popUpBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton3 = UIBarButtonItem(customView: popUpBtn)
        
        if let category = getBusinessCategory(){
            if category.businessCategory == 1{
                navigationItem.setRightBarButtonItems([barButton2,barButton1], animated: true)
            }else{
                navigationItem.setRightBarButtonItems([barButton3,barButton2,barButton1], animated: true)
            }
        }
    }
    
    @objc func onCustomerSearch(sender: UIBarButtonItem){
        self.navigateToSaleCustomerSearchVC()
    }
    
    @objc func onDateSearch(sender: UIBarButtonItem){
        if searchEnable == false{
            self.dateSearchHeight.constant = 30.0
            self.searchEnable = !searchEnable
        }else{
            self.dateSearchHeight.constant = 0.0
            searchEnable = !searchEnable
        }
    }
    
    func navigateToSaleCustomerSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalesCustomerSearchViewController") as! SalesCustomerSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func onBarPopUpBtnTapped(sender: UIButton){
        self.popOverToSaleListViewController(sender: sender)
    }
    
    func popOverToSaleListViewController(sender: UIButton){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "SaleListPopOverViewController") as? SaleListPopOverViewController{
            controller.preferredContentSize = CGSize(width: 170, height: 110)
            controller.navigateTo = self
            showPopup(controller, sourceView: sender)
        }
        
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(SalesListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.salesList = []
        self.currentPage = 1
        self.searchIsActive = false
        self.datePickerTextField.text = ""
        self.toDatePickerTextField.text = ""
        self.presenter.getSalesDataFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }
    
    func generatingDate(){
        
        let today = Date()
        print(today)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        //self.datePickerTextField.text = dateFormatter.string(from: today)
        //self.toDatePickerTextField.text = dateFormatter.string(from: today)
        //self.fifteenDaysBeforeDate = dateFormatter.string(from: fifteenDaysBeforeToday)
        
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.currentPage = 1
                    //self.presenter.getSalesDataFromServer(page: self.currentPage)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func dateFormatters(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.calendar = Calendar(identifier: .gregorian)
        let date = Date()
        let dateInGrogrian = formatter.string(from: date)
        
        print(dateInGrogrian)
        
        formatter.calendar = Calendar(identifier: .persian)
        formatter.dateFormat = "dd/MM/yyyy"
        print("Converted date to Hijri = \(formatter.string(from: date))")
    }
    
    
    
}

//Mark: Floaty Delegate
extension SalesListViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        if let viewSettings = getViewSettings(){
            if viewSettings.salePanel == "FreemiumSale"{
                self.navigateToFreemiumSaleableViewController()
            }else{
                self.navigateToAddNewSaleViewController()
            }
        }  
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToFreemiumSaleableViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumSaleableProductsVC") as! FreemiumSaleableProductsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension SalesListViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            
            self.salesList = []
            self.searchPage = 1
            self.searchIsActive = true
            
            self.presenter.getSalesDateSearchDataFromServer(page: self.searchPage, startDate: startDate, endDate: endDate)
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

//MARK: CollectionView Delegate And DataSource
extension SalesListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        //self.collectionView.backgroundColor = UIColor(red: CGFloat(87/255.0), green: CGFloat(160/255.0), blue: CGFloat(102/255.0), alpha: CGFloat(0.8))
        self.collectionView.register(SalesSummeryCell.nib, forCellWithReuseIdentifier: SalesSummeryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let item = self.salesSummery, item.count > 0 else {
            return 0
        }
//        return item.count
        return 100
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SalesSummeryCell = collectionView.dequeueReusableCell(withReuseIdentifier: SalesSummeryCell.identifier, for: indexPath) as! SalesSummeryCell
        
        guard let item = self.salesSummery, item.count > 0 else{
            return cell
        }
        let salesSummeryList = item[indexPath.row % item.count]
        
        cell.SalesSummeryName.text = salesSummeryList.title 
        cell.SalesSummeryNumber.text = salesSummeryList.value 

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 60.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.salesSummery, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
}

// MARK: TableView Delegate & Data Source
extension SalesListViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SaleListCell.nib, forCellReuseIdentifier: SaleListCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.tableView.estimatedRowHeight = 89.0
        self.automaticallyAdjustsScrollViewInsets = false
        
//        self.tableView.bounces = false
        self.tableView.addSubview(self.refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if self.salesList.count == 0 {
//            //self.tableView.setEmptyMessage("No data Available")
//            //return self.salesList.count
//        }
//        else
        if self.salesList.count > 0{
            return self.salesList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SaleListCell = tableView.dequeueReusableCell(withIdentifier: SaleListCell.identifier, for: indexPath) as! SaleListCell
        cell.selectionStyle = .none
        if self.salesList.count > 0 {
            let saleItem = self.salesList[indexPath.row]
            
            cell.salesData = saleItem
            
            cell.dateLabel.text = saleItem.date.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
            cell.dayDueLabel.text = LanguageManager.TotalDue +  " : " + String(saleItem.dayDue) + " " + Constants.currencySymbol
            cell.daySaleLabel.text = LanguageManager.TotalSale + " : " + String(saleItem.daySale)
            cell.daySaleAmountLabel.text = String(saleItem.daySaleAmount) + " " + Constants.currencySymbol
            
            
            if isLoading == false && indexPath.row == self.salesList
                .count - 1{
                self.isLoading = true
                if searchIsActive == false{
                    if self.currentPage < self.lastPageNo {
                        self.currentPage += 1
                        self.presenter.getSalesDataFromServer(page: self.currentPage)
                    }
                }else{
                    if self.searchPage < self.lastPageNo {
                        guard let startDate = self.datePickerTextField.text else{
                            return cell
                        }
                        guard let endDate = self.toDatePickerTextField.text else{
                            return cell
                        }
                        self.searchPage += 1
                        //self.salesList = []
                        self.presenter.getSalesDateSearchDataFromServer(page: self.searchPage, startDate: startDate, endDate: endDate)
                    }
                }
                
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.salesList.count > 0{
            let saleItem = self.salesList[indexPath.row]
            self.navigateToSalesPerDayVC(selectedDate: saleItem.date)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToSalesPerDayVC(selectedDate : String){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let salesPerDayVC = storyBoard.instantiateViewController(withIdentifier: "SalesPerDayViewController") as! SalesPerDayViewController
        salesPerDayVC.selectedDate = selectedDate
        self.navigationController?.pushViewController(salesPerDayVC, animated: true)
    }
}

// MARK: API Delegate
extension SalesListViewController: SalesListViewDelegate{ 
    func setSalesListData(data: SalesDataMapper) {
        
        self.salesSummery = data.summary
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        guard let list = data.saleBook, list.count > 0 else {
            self.searchAlert(title: LanguageManager.NoInformationFound, message: "")
            return
        }
        self.isLoading = false
        self.salesList += list
        
        guard let pagination = data.pagination else{
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(message : String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.salesList = []
        self.currentPage = 1
        self.presenter.getSalesDataFromServer(page : self.currentPage)
    }
    
}

extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

extension SalesListViewController : NavigateToSpecificController{
    func navigateToSalePreOrderVC() {
        self.navigateToSalePreOrderViewController()
    }
    
    func navigateToSaleDraftVC() {
        self.navigateToSaleDraftViewController()
    }
    
    func navigateToSalePreOrderViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalePreorderListViewController") as! SalePreorderListViewController
        viewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleDraftViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDraftListViewController") as! SaleDraftListViewController
        viewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

