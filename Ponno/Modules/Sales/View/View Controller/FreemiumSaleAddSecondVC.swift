//
//  FreemiumSaleAddSecondVC.swift
//  Ponno
//
//  Created by a k azad on 2/2/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class FreemiumSaleAddSecondVC: UIViewController {

    @IBOutlet weak var productNameLbl: UILabel!{
        didSet{
            if let productName = self.selectedProductName{
                productNameLbl.text = productName
            }
        }
    }
    @IBOutlet weak var quantityLbl: UILabel!{
        didSet{
            quantityLbl.text = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var quantity: UITextField!{
        didSet{
            quantity.placeholder = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var sellingPriceLbl: UILabel!{
        didSet{
            sellingPriceLbl.text = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var sellingPrice: UITextField!{
        didSet{
            sellingPrice.placeholder = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var sellingPriceHintLbl: UILabel!{
        didSet{
            self.sellingPriceHintLbl.textColor = UIColor.orange
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    var freemiumProduct : FreemiumProducts?
    var selectedProductName : String?
    var saleAblePrice : Double?
    //For Freemium
    var freemiumSaleDelegate : FreemiumProductSaleDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpInitialLanguage()
        self.setUpViews()
        self.toolBarSetUp()
        
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext), for: .touchUpInside)
        self.quantity.underlined()
        self.sellingPrice.underlined()
        
        self.title = LanguageManager.SaleBook
        sellingPriceHintLbl.isHidden = true
        
        guard let price = self.saleAblePrice else {
            return
        }
        self.sellingPrice.text = "\(price)"
        
    }
        
    func toolBarSetUp(){
        //QuantityToolBar
        let quantityToolBar = UIToolbar()
        quantityToolBar.barStyle = UIBarStyle.default
        quantityToolBar.isTranslucent = true
        quantityToolBar.tintColor = UIColor.black
        quantityToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
        
        quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        quantityToolBar.isUserInteractionEnabled = true
        
        self.quantity.inputAccessoryView = quantityToolBar
        
        //SellingPriceToolBar
        let sellingPriceToolBar = UIToolbar()
        sellingPriceToolBar.barStyle = UIBarStyle.default
        sellingPriceToolBar.isTranslucent = true
        sellingPriceToolBar.tintColor = UIColor.black
        sellingPriceToolBar.sizeToFit()
        
        let sellingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnSellingPrice(sender:)))
        
        sellingPriceToolBar.setItems([cancelButton, spaceButton, sellingPriceDoneButton], animated: false)
        sellingPriceToolBar.isUserInteractionEnabled = true
        
        self.sellingPrice.inputAccessoryView = sellingPriceToolBar
    }
        
        @objc func onPressingCancel(sender: UIBarButtonItem){
            self.view.endEditing(true)
        }
        
        @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
            self.quantity.resignFirstResponder()
            self.sellingPrice.becomeFirstResponder()
        }
        
        @objc func onPressingDoneOnSellingPrice(sender: UIBarButtonItem){
            self.sellingPrice.resignFirstResponder()
            self.nextBtn.becomeFirstResponder()
        }
        
        @objc func onNext(sender : UIButton){
            if self.isValidated(){
                guard let quantity = self.quantity.text, let quantityDouble = Double(quantity), let sellingPrice = self.sellingPrice.text, let sellingPriceDouble = Double(sellingPrice)  else{
                    return
                }
                self.freemiumSaleDelegate?.setProductData(amount: quantityDouble, sellingPrice: sellingPriceDouble)
                self.navigationController?.popViewController(animated: true)
            }
        }
        func isValidated()->Bool{
            if self.quantity.text == ""{
                self.showAlert(title: LanguageManager.QuantityIsRequired, message: "")
                return false
            }
            else if self.sellingPrice.text == ""{
                self.showAlert(title: LanguageManager.SellingPriceIsRequired, message: "")
                return false
            }
            return true
        }
    
}
