//
//  SalePanelProductViewController.swift
//  Ponno
//
//  Created by a k azad on 2/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class SalePanelProductViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    
    fileprivate var presenter = SalePanelProductPresenter(service: SalesService())
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var saleProducts : [SaleProductList] = []{
        didSet{
            if self.saleProducts.count > 0{
                self.saleProducts = self.saleProducts.sorted(by: { $0.soldQuantity > $1.soldQuantity})
            }
            self.refreshTableView()
        }
    }
    
    var saleableProducts : [SaleProductList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var saleProductList : [SaleProductList] = []
    
    var productList : [ProductList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var filteredList : [SaleProductList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var saleId: Int?
    var saleInfo : SaleInfo?
    
    var draftId: Int?
    
    var searchText = ""
    var timer : Timer?
    
    var selectedProduct : SaleProductList?
    var selectedProductName : String?
    var sellingPrice : Double?
    var saleableQuantity : Double?
    
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedSellingPrices : [Double] = []
    var selectedSerials : [String] = []
    
    var filteredProduct : [ProductList] = []
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpViews()
        self.setUpSearchBar()
        self.configureTableView()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setUpInitialLanguage()
        self.setNavigationBar()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.resignFirstResponder()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext(sender:)), for: .touchUpInside)
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    @objc func onNext(sender : UIButton){
        if self.selectedProductId.count > 0{
            self.navigateToSalePanelProductInfoViewController()
        }
    }
        
    func navigateToSalePanelProductInfoViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalePanelProductInfoViewController") as! SalePanelProductInfoViewController
        
        viewController.saleInfo = self.saleInfo
        //Initialize saleInfo here may be
        viewController.pushedInFromSaleEdit = true
        viewController.selectedProductId = self.selectedProductId
        viewController.selectedAmounts = self.selectedAmounts
        viewController.selectedSellingPrices = self.selectedSellingPrices
        viewController.selectedSerials = self.selectedSerials
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func updateSaleInfo(info : SaleInfo){
        
    }

}

//MARK: SearchBar Delegate
extension SalePanelProductViewController: UISearchBarDelegate{
    fileprivate func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.ProductSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if self.saleableProducts.count > 0 {
            if searchText == "" {
                self.isSearchActive = false
                return
            }else{
                self.isSearchActive = true
            }
        }
        
        guard let textToSearch = searchBar.text else {
            return
        }
        
        if textToSearch.count > 2{
            filteredList = self.saleableProducts.filter { (product : SaleProductList) -> Bool in
                return product.name.lowercased().contains(textToSearch.lowercased()) || product.categoryName.lowercased().contains(textToSearch.lowercased()) || product.variant.lowercased().contains(textToSearch.lowercased())
            }
        }
    }
}

//Mark: TableView Delegate and DataSource
extension SalePanelProductViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SaleProductCell.nib, forCellReuseIdentifier: SaleProductCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if isSearchActive == true{
                return 0
            }else{
                if self.saleProducts.count > 0 {
                    return self.saleProducts.count
                }
                return 0
            }
            
        case 1:
            if isSearchActive{
                guard self.filteredList.count > 0 else {
                    return 0
                }
                return self.filteredList.count
            }else{
                if self.saleableProducts.count > 0 {
                    return self.saleableProducts.count
                }
                return 0
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SaleProductCell.identifier, for: indexPath) as! SaleProductCell
                
        switch indexPath.section {
            
        case 0:
            if self.saleProducts.count > 0 {
                let product = self.saleProducts[indexPath.row]
                
                cell.saleProducts = product
                self.saleableQuantity = product.quantity.toDouble()
                
//                if self.selectedProductId.contains(data.productId){
//                    //self.selectedProductAlert(userMessage: LanguageManager.TheProductIsAlreadySelectedDeselectTheProductForUpdate)
//                }
                
                cell.productId = product.productId
                
                cell.selectBtn.tag = product.productId
                cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
            }
            
        case 1:
            if isSearchActive{
                if self.filteredList.count > 0{
                    let product = self.filteredList[indexPath.row]
                    
                    cell.productId = product.productId
                    cell.saleableProduct = product
                }
            }else{
                if self.saleableProducts.count > 0 {
                    let product = self.saleableProducts[indexPath.row]
                    
                    cell.productId = product.productId
                    cell.saleableProduct = product
                }
            }
        default:
            return cell
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            if self.saleProducts.count > 0 {
                let data = self.saleProducts[indexPath.row]
                
                self.sellingPrice = Double(data.sellingPrice)
                self.saleableQuantity = Double(data.soldQuantity)
                
                for item in self.saleProducts{
                    if item.productId == data.productId{
                        self.selectedProduct = item
                    }
                }
                
                if data.hasSerial == 0{
                    self.navigateToSaleAddVC()
                }
                else{
//                    if data.soldSerialId.count > 0{
////                        var serials : String = ""
////                        if selectedProductId.contains(data.productId){
////                            serials = data.soldSerialNo
////                        }
//                        navigateToSaleAddSerialVC(id: data.productId, serials: data.soldSerialNo)
//                    }else{
//                        var serials = data.soldSerialNo
//                        serials.append(contentsOf: data.soldSerialNo)
//                        //serials = "," + data.soldSerialNo.joined(separator: ",")
//                        navigateToSaleAddSerialVC(id: data.productId, serials: serials)
//                    }
                    
                    navigateToSaleAddSerialVC(id: data.productId, serials: data.soldSerialNo)
                    
//                    var serials = data.serials
//                    if !data.soldSerialNo.isEmpty{
//                        serials += "," + data.soldSerialNo
//                    }
                    
                    //navigateToSaleAddSerialVC(id: data.productId, serials: data.serials)
                    
                    //navigateToSaleAddSerialVC(productId: data.productId)
                }
            }
        case 1:
            if isSearchActive{
                if self.filteredList.count > 0{
                    let data = self.filteredList[indexPath.row]
                    
                    self.selectedProductName = data.name
                    self.selectedProduct = data
                    self.saleableQuantity = data.quantity.toDouble()
                    self.sellingPrice = Double(data.sellingPrice)
                    
                    let quantity = data.quantity
                    if quantity.isEmpty == false && quantity != "0.00"{
                        if data.hasSerial == 0{
                            self.navigateToSaleAddVC()
                        }
                        else{
                            self.navigateToSaleAddWithSerialVC(serials: data.serials)
                        }
                    }else{
                        self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
                    }
                }
            }else{
                if self.saleableProducts.count > 0 {
                    let data = self.saleableProducts[indexPath.row]
                    
                    self.selectedProductName = data.name
                    self.selectedProduct = data
                    self.saleableQuantity = data.quantity.toDouble()
                    self.sellingPrice = Double(data.sellingPrice)
                    
                    let quantity = data.quantity
                    if quantity.isEmpty == false && quantity != "0.00"{
                        if data.hasSerial == 0{
                            self.navigateToSaleAddVC()
                        }
                        else{
                            self.navigateToSaleAddWithSerialVC(serials: data.serials)
                        }
                    }else{
                        self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
                    }
                }
            }
        default:
            return
        }
        
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onRemove(sender : UIButton){
        let productId = sender.tag
        
        
        if self.selectedProductId.contains(productId){
            for item in self.saleProducts{
                if item.productId == productId{
                    let soldQuantity = item.soldQuantity
                    removeDataFromSaleProducts(productId: productId, quantity: "\(soldQuantity)")
                }
            }
            
            removeDataofProductObject(productId: productId)
        }
    }
    
    func removeDataFromSaleProducts(productId : Int, quantity: String){
        self.saleProducts = self.saleProducts.filter{ $0.productId != productId}
        self.addDataToSaleableProductList(productId: productId, quantity: quantity)
        self.refreshTableView()
    }
    
    func removeDataofProductObject(productId : Int){
        var productAtIndex = -1
        for index in 0..<self.selectedProductId.count{
            if self.selectedProductId[index] == productId{
                productAtIndex = index
            }
        }
        
        self.selectedSellingPrices.remove(at: productAtIndex)
        self.selectedAmounts.remove(at: productAtIndex)
        self.selectedSerials.remove(at: productAtIndex)
        self.selectedProductId.remove(at: productAtIndex)
    }
    
//    func addProductIntoProductList(product: ProductList){
//        self.productList.append(product)
//    }
    
    func addDataToSaleableProductList(productId: Int, quantity: String){
        if self.saleProductList.count > 0{
            for item in self.saleProductList{
                if item.productId == productId{
                    let productId = productId
                    let quan = item.quantity.toDouble()
                    let soldQuan = quantity.toDouble()
                    var updatedQuantity: String = ""
                    if item.saleId == 0{
                        if let quantity = quan{
                            updatedQuantity = "\(quantity)"
                        }
                    }else{
                        if let quantity = quan, let soldQuantity = soldQuan{
                            updatedQuantity = "\(quantity + soldQuantity)"
                        }
                    }
                    
                    let sellingPrice = item.sellingPrice
                    let serials = item.serials
                    let name = item.name
                    let image = item.image
                    let categoryName = item.categoryName
                    let variant = item.variant
                    let unit = item.unit
                    let categoryId = item.categoryId
                    
                    let saleableProduct = SaleProductList(productId: productId, quantity: updatedQuantity, sellingPrice: sellingPrice, serials: serials, name: name, image: image, categoryName: categoryName, categoryId: categoryId, variant: variant, unitName: unit)
                    self.saleableProducts.append(saleableProduct)
                    self.saleableProducts = self.saleableProducts.sorted(by: { $0.name < $1.name })
                }
            }
        }
    }
    
    func addDataToSaleProducts(productId: Int, soldQuantity: String, soldSellingPrice: String){
        if let product = findProductInfo(productId: productId){
            let selectedProduct = SaleProductList(productId: productId, quantity: product.quantity, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, soldSerials: [""], name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
            for item in self.saleProducts{
                if item.productId == productId{
                    self.saleProducts = self.saleProducts.filter{($0.productId != productId)}
                }
            }
            self.saleProducts.append(selectedProduct)
            self.refreshTableView()
        }
    }
    
    func addSerialDataToSaleProducts(productId: Int, soldQuantity: String, soldSerials: [String], soldSellingPrice: String){
            if let product = findProductInfo(productId: productId){
                let selectedProduct = SaleProductList(productId: productId, quantity: product.quantity, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, soldSerials: soldSerials, name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
                for item in self.saleProducts{
                    if item.productId == productId{
                        self.saleProducts = self.saleProducts.filter{($0.productId != productId)}
                    }
                }
                self.saleProducts.append(selectedProduct)
                self.refreshTableView()
            }
        }
    
    func navigateToSaleAddVC(){
        let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSecondVC") as! SaleAddSecondVC
        
        viewController.delegate = self
        viewController.saleAblePrice = self.sellingPrice
        viewController.saleAbleQuantity = self.saleableQuantity
        viewController.selectedProductName = self.selectedProductName
        viewController.saleEditProduct = self.selectedProduct
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleAddSerialVC(id: Int,serials : [String]){
        let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSerialViewController") as! SaleAddSerialViewController
        if self.saleProducts.count > 0{
            
            //let serialList = self.prepareSerialNumbers(serials: serials)
            viewController.saleAblePrice = self.sellingPrice
            viewController.selectedProductName = self.selectedProductName
            //viewController.serialList = serialList
            viewController.delegate = self
            viewController.productSerials = serials
            viewController.productId = id
            viewController.saleId = self.saleId
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleAddWithSerialVC(serials : String){
        if self.prepareSerialNumbers(serials: serials).count > 0{
            let serialList = self.prepareSerialNumbers(serials: serials)
            
            let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSerialViewController") as! SaleAddSerialViewController
            
            viewController.serialList = serialList
            viewController.saleAblePrice = self.sellingPrice
            viewController.delegate = self
            viewController.selectedProductName = self.selectedProductName
            viewController.isCodeScannerActive = false
            //            if codeScannerActive == true{
            //                viewController.isCodeScannerActive = true
            //                viewController.searchSerial = self.searchText
            //            }else{
            //                viewController.isCodeScannerActive = false
            //            }
            
            //viewController.isCodeScannerActive = self.codeScannerActive
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    func removeDataFromSaleableProductList(productId: Int){
        if let product = findProductInfo(productId: productId){
            self.saleableProducts = self.saleableProducts.filter{($0.productId != product.productId)}
            self.refreshTableView()
        }
    }
        
    func findProductInfo(productId: Int) -> SaleProductList?{
        var productItem : SaleProductList?
        for item in self.saleProductList{
            if item.productId == productId{
                productItem = item
            }
        }
        return productItem
    }
    
}

//Product Delegate
extension SalePanelProductViewController : SalesAddProductDelegate{
    func setSerialData(serial: [String], sellingPrice: Double) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        if self.selectedProductId.contains(selectedProduct.productId){
            self.removeDataofProductObject(productId: selectedProduct.productId)
        }
        self.selectedProductId.append(selectedProduct.productId)
        //self.selectedSerials = [serial.joined(separator: ",")]
        //self.selectedSerials = serial
        self.selectedSerials.append(serial.joined(separator: ","))
        self.selectedSellingPrices.append(sellingPrice)
        self.selectedAmounts.append(Double(serial.count))
        
        self.addSerialDataToSaleProducts(productId: selectedProduct.productId, soldQuantity: "\(serial.count)", soldSerials: serial, soldSellingPrice: (sellingPrice.toString()))
        self.removeDataFromSaleableProductList(productId: selectedProduct.productId)
        self.resetSearchBar()
    }
    
    func setProductData(amount: Double, sellingPrice: Double) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        if self.selectedProductId.contains(selectedProduct.productId){
            self.removeDataofProductObject(productId: selectedProduct.productId)
        }
        self.selectedProductId.append(selectedProduct.productId)
        self.selectedSellingPrices.append(sellingPrice)
        self.selectedAmounts.append(amount)
        self.selectedSerials.append("")
        self.addDataToSaleProducts(productId: selectedProduct.productId, soldQuantity: amount.toString(), soldSellingPrice: sellingPrice.toString())
        self.removeDataFromSaleableProductList(productId: selectedProduct.productId)
        self.resetSearchBar()
    }
    
    func resetSearchBar(){
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
}


//Mark: Api Delegate
extension SalePanelProductViewController : SalesPanelProductViewDelegate{
    func setSaleEditData(data: SaleEditDataMapper) {
        guard let products = data.saleProducts, products.count > 0 else{
            return
        }
        
        self.saleProductList = products
        
        for item in products{
            if item.soldQuantity != 0{
//                item.soldItemFlag = true
                self.selectedProductId.append(item.productId)
                if let sellingPrice = item.soldSellingPrice.toDouble(){
                    self.selectedSellingPrices.append(sellingPrice)
                    self.selectedAmounts.append(Double(item.soldQuantity))
                    if item.hasSerial != 0{
                        self.selectedSerials.append(item.soldSerialNo.joined(separator: ","))
                    }else{
                        self.selectedSerials.append("")
                    }
                }
            }
        }
        self.saleProducts = products.filter{selectedProductId.contains($0.productId)}
        self.saleableProducts = products.filter{ !selectedProductId.contains($0.productId)}
        
        self.saleInfo = data.sale
    }
    
    
    func onFailed(message: String) {
        self.showAlert(title: message, message: "")
    }
    
    func showLoading() {
        
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.saleId else{
            return
        }
        self.presenter.getSalePanelProductDataFromServer(id: id)
    }
    
}
