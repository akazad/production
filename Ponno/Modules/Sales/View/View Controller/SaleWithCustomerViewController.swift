//
//  SaleWithCustomerViewController.swift
//  Ponno
//
//  Created by a k azad on 14/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class SaleWithCustomerViewController: UIViewController { 

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = SaleWithCustomerPresenter(service: SalesService())
    
    var customerName : [AllCustomers] = [] {
        didSet{
            self.refreshTableView()
        }
    }
    var customerId: Int?
    var name: String?
    var searchText = ""
    
    //    SearchBar
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var filteredList : [AllCustomers] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var delegate: SaleInfoRefreshDelegate?
    var saleInfoVCInstance : SaleInfoViewController?
    var salePanelProductInfoVC : SalePanelProductInfoViewController?
    
    var preOrderDelegate: SalePreOrderInfoRefreshDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.configureTableView()
        //self.addFloaty()
        self.setUpSearchBar()
        self.setBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.attachPresenter()
    }
    
    func setBarButton(){
        let dueAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        dueAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        dueAddBtn.addTarget(self, action: #selector(onCustomerAdd(sender:)), for: UIControl.Event.touchUpInside)
        dueAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        dueAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        dueAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: dueAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2], animated: true)
    }
    
    @objc func onCustomerAdd(sender: UIButton){
        self.alertWithTextField()
    }
    
    func addFloaty(){
        let floaty = Floaty()
        floaty.buttonColor = UIColor.lightGreen
        
        let extraItem = FloatyItem()
        extraItem.icon = UIImage(named: "next")
        extraItem.buttonColor = UIColor.lightGreen
        extraItem.title = LanguageManager.CustomerAdd
        extraItem.handler = { item in
            self.alertWithTextField()
        }
        
        floaty.addItem(item: extraItem)
        self.view.addSubview(floaty)
    }

}

//MARK: SearchBar Delegate
extension SaleWithCustomerViewController: UISearchBarDelegate{
    fileprivate func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.SearchCustomer
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearchActive = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearchActive = false
        
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
//        1
        
        guard let textToSearch = searchBar.text else {
            return
        }
        
        self.searchText = textToSearch
        
        filteredList = self.customerName.filter { (customer : AllCustomers) -> Bool in
            return customer.name.lowercased().contains(textToSearch.lowercased()) || customer.phone.lowercased().contains(textToSearch.lowercased())
        }
        
        if filteredList.count == 0 {
            isSearchActive = false
        }
        else {
            isSearchActive = true
        }
        
    }
}

//Mark: TableView Delegate
extension SaleWithCustomerViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(CustomerNameCell.nib, forCellReuseIdentifier: CustomerNameCell.identifier)
        self.tableView.separatorStyle = .none
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive{
            guard self.filteredList.count > 0 else {
                return 0
            }
            return self.filteredList.count
        }else{
            guard self.customerName.count > 0 else {
                return 0
            }
            return self.customerName.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CustomerNameCell = tableView.dequeueReusableCell(withIdentifier: "CustomerNameCell", for: indexPath) as! CustomerNameCell
        cell.selectionStyle = .none
        if isSearchActive{
            guard self.filteredList.count > 0 else {
                return cell
            }
            let item = self.filteredList[indexPath.row]
            
            cell.customer = item
            
            if self.searchText != ""{
                cell.phoneNoLbl.attributedText = self.boldSearchResult(searchString: self.searchText, resultString: item.phone)
            }
            
            self.customerId = item.id
            return cell
        }else{
            guard self.customerName.count > 0 else {
                return cell
            }
            let item = self.customerName[indexPath.row]
            
            cell.customer = item
            if self.searchText != ""{
                cell.phoneNoLbl.attributedText = self.boldSearchResult(searchString: self.searchText, resultString: item.phone)
            }
            
            
            self.customerId = item.id
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearchActive{
            guard self.filteredList.count > 0 else{
                return
            }
            let customer = self.filteredList[indexPath.row]
            didSelectOnCustomer(customer : customer)
        }else{
            guard self.customerName.count > 0 else{
                return
            }
            let customer = self.customerName[indexPath.row]
            didSelectOnCustomer(customer : customer)
        }
    }
    
    func didSelectOnCustomer(customer : AllCustomers){
        self.name = customer.name + " (" + customer.phone.suffix(3) + ")"
        self.customerId = customer.id
        
        SaleInfoObject.customerId = self.customerId
        PreOrderInfoObject.customer = self.customerId
        PreOrderInfoObject.customerName = self.name
        SaleInfoObject.customerName = self.name
        SaleInfoObject.walletAmount = customer.amount.toString()
        SaleInfoObject.hasWallet = customer.hasWallet
        if customer.hasWallet == 0{
            saleInfoVCInstance?.hideCustomerWallet(bool: true)
            salePanelProductInfoVC?.showCustomerWallet(bool: true)
        }else{
            saleInfoVCInstance?.hideCustomerWallet(bool: false)
            salePanelProductInfoVC?.showCustomerWallet(bool: false)
        }
        
        salePanelProductInfoVC?.saleEditInfo?.customerWalletAmount = customer.amount
        salePanelProductInfoVC?.saleEditInfo?.wallet = 0
        salePanelProductInfoVC?.saleInfo?.customerWalletAmount = customer.amount.toString()
        salePanelProductInfoVC?.saleInfo?.wallet = "0"
        
        SaleInfoObject.walletPay = 0
        saleInfoVCInstance?.walletPayTextField.text = ""
        self.delegate?.refreshScreen()
        self.preOrderDelegate?.refreshScreen()
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.00
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToSaleInfoViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleInfoViewController") as! SaleInfoViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewCustomerAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.CustomerName
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.CustomerPhone
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.NameIsRequired, message: "")
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                self.showAlert(title: LanguageManager.PhoneNumberIsRequired, message: "")
                return
            }
            
            
//            if let phoneNumber = textField2.text, !self.validatePhoneNumber(value: phoneNumber){
//                self.showAlert(title: LanguageManager.PhoneNumberIsIncorrect, message: "")
//                return
//            }
            
            let param : [String : Any] = ["name": textField.text!, "phone": textField2.text!, "address": ""]

            self.presenter.postAddNewCustomerDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: userMessage, message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                SaleInfoObject.walletPay = 0.0
                self.delegate?.refreshScreen()
                self.preOrderDelegate?.refreshScreen()
                self.navigationController?.popViewController(animated: true)
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension SaleWithCustomerViewController : SaleWithCustomerDelegate{
    func addNewCustomer(data: CustomerAddDataMapper) {
        guard let customer = data.customer else {
            return
        }
        self.name = customer.name
        self.customerId = customer.id
        
        SaleInfoObject.customerId = self.customerId
        SaleInfoObject.customerName = self.name
        
        guard let message = data.message else {
            return
        }

        self.displayMessage(userMessage: message)
        
    }
    
    func setCustomer(data: AllCustomerDataMapper) {
        guard let list = data.customers, list.count > 0 else {
            return
        }
        
        self.customerName = list
        
        
    }
    
    func onFailed(failure: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.customerName = []
        self.presenter.getAllCustomerDataFromServer(searchString: true)
    }
    
}

extension SaleWithCustomerViewController{
    func boldSearchResult(searchString: String, resultString: String) -> NSMutableAttributedString {

        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: resultString)
        let pattern = searchString.lowercased()
        let range: NSRange = NSMakeRange(0, resultString.count)

        let regex = try! NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options())

        regex.enumerateMatches(in: resultString.lowercased(), options: NSRegularExpression.MatchingOptions(), range: range) { (textCheckingResult, matchingFlags, stop) -> Void in
            let subRange = textCheckingResult?.range
            //attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 15.0), range: subRange!)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.green, range: subRange!)

        }

        return attributedString

    }
}
