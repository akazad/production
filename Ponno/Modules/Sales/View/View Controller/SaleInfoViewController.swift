//
//  SaleInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 2/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

struct SaleInfoObject {
    static var discount : Double?
    static var discountUnit : String?
    static var paymentMethod : String?
    static var paymentMethodId : Int?
    static var newPayment: String?
    static var payableAmount : Double?
    static var paidAmount : Double?
    static var cash : Double?
    static var due : Double?
    static var cashback : Double?
    static var deliverySystem : String?
    static var deliverySystemId: Int?
    static var newDeliverySystem: String?
    static var deliveryCharge : Double?
    static var deliveryToken : String?
    static var customerId : Int?
    static var customerName : String?
    static var newCustomer: String?
    static var phone: String?
    static var customerPhone: String?
    static var address: String?
    static var paymentPhone : String?
    static var saleDate : String?
    static var hasWallet: Int?
    static var walletAmount: String?
    static var walletPay: Double?
    
    static func resetSaleInfoObject(){
        self.discount = nil
        self.discountUnit = nil
        self.paymentMethod = nil
        self.paymentMethodId = 1
        self.newPayment = nil
        self.payableAmount = nil
        self.paidAmount = nil
        self.cash = nil
        self.due = nil
        self.cashback = nil
        self.deliverySystem = nil
        self.deliverySystemId = nil
        self.newDeliverySystem = nil
        self.deliveryCharge = nil
        self.deliveryToken = nil
        self.customerId = nil
        self.customerName = nil
        self.newCustomer = nil
        self.phone = nil
        self.customerPhone = nil
        self.address = nil
        self.paymentPhone = nil
        self.saleDate = nil
        self.hasWallet = nil
        self.walletAmount = nil
        self.walletPay = nil
    }
}

struct SalePreOrderInfoObject {
    static var customerName : String?
    static var customerId: Int?
    static var preOrderId: Int?
    static var date: String?
}

class SaleInfoViewController: UIViewController {

    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var deliveryChargeLbl: UILabel!
    @IBOutlet weak var payableAmountLbl: UILabel!
    @IBOutlet weak var paidAmountLbl: UILabel!
    @IBOutlet weak var cashLbl: UILabel!
    @IBOutlet weak var walletLbl: UILabel!
    @IBOutlet weak var cashBackLbl: UILabel!
    @IBOutlet weak var dueLbl: UILabel!
    @IBOutlet weak var discountSwitch: UISwitch!
    @IBOutlet weak var discountSwitchTitleLbl: UILabel!{
        didSet{
            discountSwitchTitleLbl.text = LanguageManager.Discount
        }
    }
    @IBOutlet weak var delivarySwitch: UISwitch!
    @IBOutlet weak var deliverySwitchTitleLbl: UILabel!{
        didSet{
            deliverySwitchTitleLbl.text = LanguageManager.Delivery
        }
    }
    @IBOutlet weak var deliverySwitchHeight: NSLayoutConstraint!
    @IBOutlet weak var submitBtnView: UIView!{
        didSet{
            self.setUpCardView(uiview: submitBtnView)
        }
    }
    @IBOutlet weak var submitBtn: UIButton!{
        didSet{
            self.submitBtn.setTitleColor(UIColor.init(red: 61, green: 172, blue: 65), for: .normal)
            self.submitBtn.setTitle(LanguageManager.Submit, for: .normal)
            self.setUpCardView(uiview: submitBtn)
        }
    }
    @IBOutlet weak var saveAsDraftBtnView: UIView!{
        didSet{
            self.setUpCardView(uiview: saveAsDraftBtnView)
        }
    }
    @IBOutlet weak var saveAsDraftBtn: UIButton!{
        didSet{
            self.setUpCardView(uiview: saveAsDraftBtn)
            self.saveAsDraftBtn.setTitle(LanguageManager.Draft, for: .normal)
        }
    }
    @IBOutlet weak var cashText: UITextField!{
        didSet{
            cashText.placeholder = LanguageManager.Amount
        }
    }
    @IBOutlet weak var paymentPhoneNo: UITextField!
    @IBOutlet weak var paymentPhoneHeight: NSLayoutConstraint!
    @IBOutlet weak var customerText: UITextField!{
        didSet{
            customerText.placeholder = LanguageManager.Customer
        }
    }
    @IBOutlet weak var walletAmountTitleLbl: UILabel!{
        didSet{
            walletAmountTitleLbl.text = LanguageManager.WalletAmount
        }
    }
    @IBOutlet weak var walletAmountTextField: UITextField!
    @IBOutlet weak var walletPayLbl: UILabel!{
        didSet{
            walletPayLbl.text = LanguageManager.WalletPay
        }
    }
    @IBOutlet weak var walletPayTextField: UITextField!
    
    @IBOutlet weak var dateTextField: UITextField!
    
    
    @IBOutlet weak var paymentMethodText: UITextField!
    
    //Discount
    
    @IBOutlet weak var discountCheckBtn: UIButton!{
        didSet{
            self.discountCheckBtn.isSelected = false
        }
    }
    
    @IBOutlet weak var discountDetailsLbl: UILabel!
    
    @IBOutlet weak var discountEditBtn: UIButton!
    @IBOutlet weak var deliveryCheckBtn: UIButton!{
        didSet{
            self.deliveryCheckBtn.isSelected = false
        }
    }
    @IBOutlet weak var deliveryDetailsLbl: UILabel!
    @IBOutlet weak var deliveryChargeAmountLbl: UILabel!
    
    
    @IBOutlet weak var deliveryEditBtn: UIButton!
    
    private var presenter = SaleInfoViewPresenter(service: SalesService())
    
    var customerNameList : [AllCustomers]?
    var name : String?
    var customerId : Int?
    var currentPage: Int = 1
    var getAll : Bool = true
    var totalPrice : Double?
    //var totalPayable : Double?
    
    var paymentMethodList : [PaymentMethodList] = []
    var paymentMethodPicker = UIPickerView()
    var paymentMethod : Int?
    var paymentMethodId : Int?
    
    //
    var discountPrice : Double?
    var discountUnit : String?
    var payableAmount : Double?
    var dueAmount : Double?
    var cashBackAmount : Double?
    var deliverySystem : String?
    var deliveryCharge : Double?

    //
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedSellingPrices : [Double] = []
    var selectedSerials : [String] = []
    
    var isLoading : Bool = false
    var deliveryCurrentPage : Int = 1
    var deliveryStatus: Int?
    //
    let dateFormatter = DateFormatter()
    var invoice : String?
    
    var datePicker = UIDatePicker()
    
    var isDraftEnable: Bool = false
    var isPreOrderEnable: Bool = false
    
    var product : SalePreOrderEditProducts?
    
    //DeliveryCheckBtn
    var deliveryCheck = false
    var discountCheck = false
    
    //DraftButtonHide
    var draftBtnHide: Bool = false
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetUp()
        //self.generateInvoice()
        self.setUpPickerView()
        self.showSaleDatePicker()
        //self.switchSetUp()
        //self.submitBtnSetup()
        self.resetSaleInfoViewController()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if SaleInfoObject.discount == nil{
            //discountSwitch.setOn(false, animated: true)
        }
        if SaleInfoObject.deliveryCharge == nil{
            //delivarySwitch.setOn(false, animated: true)
        }
    }
    
    func resetSaleInfoViewController(){
        SaleInfoObject.resetSaleInfoObject()
    }
    
    @IBAction func unwindWithSegue(_ segue: UIStoryboardSegue) {

    }
    
    func back(sender: UIBarButtonItem) {
        SaleInfoObject.resetSaleInfoObject()
        _ = navigationController?.popViewController(animated: true)
    }

    
    func initialSetUp(){
        
        if let shop = getShopData(){
            guard  let deliverySystemStatus = shop.deliverySystem else {
                return
            }
            self.deliveryStatus = deliverySystemStatus
            
        }
        
        guard self.selectedAmounts.count > 0 else { return }
        guard self.selectedSerials.count > 0 else { return }

        var temp : Int = 0
        guard self.selectedSellingPrices.count > 0 else { return }
        var totalPrice = 0.0
        for price in self.selectedSellingPrices {
            if self.selectedAmounts.count > 0{
                let tempTotal = price * self.selectedAmounts[temp]
                totalPrice += tempTotal
                temp += 1
            }
        }
        
        self.totalPrice = totalPrice
        
        self.paidAmountLbl.text = LanguageManager.PaidAmount + " : " + "\(0.0)" + Constants.currencySymbol
        
        self.paymentPhoneNo.isHidden = true
        self.paymentPhoneHeight.constant = 0.0
        SaleInfoObject.paymentPhone = ""
        self.walletAmountTextField.isEnabled = false
        //self.customerText.text = SaleInfoObject.customerName ?? ""
        
        
        totalPriceLabel.text = LanguageManager.Total + " : " + "\(totalPrice)" + Constants.currencySymbol
        discountLbl.text = LanguageManager.Discount + " : 0.00"
        deliveryChargeLbl.text = LanguageManager.DeliveryCharge + " : 0.00"
        payableAmountLbl.text = LanguageManager.PayableAmount + " : " + "\(totalPrice)" + Constants.currencySymbol
        cashLbl.text = LanguageManager.Cash + " : 0.00"
        cashBackLbl.text = LanguageManager.CashBack + " : 0.00"
        dueLbl.text = LanguageManager.Due + " : " + "\(totalPrice)" + Constants.currencySymbol
        
        //CustomerWallet
        SaleInfoObject.hasWallet = -1
        SaleInfoObject.walletPay = 0.0
        self.hideCustomerWallet(bool: true)
                
        SaleInfoObject.cash = 0.0
        
//        //pickerPreselection
        paymentMethodText.text = "Cash"
        self.paymentMethodId = 1
        SaleInfoObject.paymentMethodId = 1
        
        self.dateTextField.text = currentDateFormatter(format: "dd MMM, yyy", date: Date())
        
        SaleInfoObject.deliverySystemId = nil
        
        
        self.customerText.delegate = self
        
        self.discountDetailsSetup()
        self.discountCheckBtn.addTarget(self, action: #selector(onDiscountCheckBtnTapped), for: .touchUpInside)
        self.discountEditBtn.addTarget(self, action: #selector(onDiscountEditBtnTapped), for: .touchUpInside)
        
        //self.paymentMethodPicker.selectedRow(inComponent: 1)
        //self.discountSwitch.addTarget(self, action: #selector(discountSwitchValueDidChange), for: .valueChanged)
        if self.deliveryStatus == 1{
            self.deliveryDetailsSetup()
            self.deliveryCheckBtn.addTarget(self, action: #selector(deliveryCheckBtnDidChange), for: .touchUpInside)
            self.deliveryEditBtn.addTarget(self, action: #selector(onDeliverEditBtnTapped), for: .touchUpInside)
        }else{
            self.hideDeliverySystem(is : true)
        }
        
        if SalePreOrderInfoObject.preOrderId != nil{
            self.customerText.text = SalePreOrderInfoObject.customerName
            SaleInfoObject.customerId = SalePreOrderInfoObject.customerId
            SaleInfoObject.saleDate = currentDateFormatter(format: "yyyy-MM-dd", date: Date())
            SaleInfoObject.customerName = SalePreOrderInfoObject.customerName
        }else{
            SaleInfoObject.customerId = nil
            SaleInfoObject.saleDate = currentDateFormatter(format: "yyyy-MM-dd", date: Date())
        }
       
//        self.customerText.addTarget(self, action: #selector(customerTextDidChange(_textfield: )), for: .editingChanged)
        self.cashText.addTarget(self, action: #selector(cashTextDidChange(_textfield:)), for: .editingChanged)
        self.walletPayTextField.addTarget(self, action: #selector(onWalletPay(_textfield:)), for: .editingChanged)
        
        if draftBtnHide == true{
            self.saveAsDraftBtn.isHidden = true
        }else{
            self.saveAsDraftBtn.isHidden = false
            self.saveAsDraftBtn.addTarget(self, action: #selector(onSaveAsDraftBtnTapped(sender:)), for: .touchUpInside)
        }
        
        
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    func discountDetailsSetup(){
        self.discountCheckBtn.setImage(UIImage(named: "uncheck_icon"), for: .normal)
        self.discountDetailsLbl.text = LanguageManager.Discount
        self.hideDiscountDetails(is: true)
    }
    
    func deliveryDetailsSetup(){
        self.deliveryCheckBtn.setImage(UIImage(named: "uncheck_icon"), for: .normal)
        self.deliveryDetailsLbl.text = LanguageManager.DeliverySystem
        self.hideDeliveryDetails(is: true)
    }
        
    @objc func cashTextDidChange(_textfield: UITextField){
        guard let cashPaid = self.cashText.text, let cash = Double(cashPaid) else {
            SaleInfoObject.cash = 0.0
            self.refreshView()
            return
        }
        SaleInfoObject.cash = cash
        self.refreshView()
    }
    
    @objc func onWalletPay(_textfield: UITextField){
        guard let wallet = self.walletPayTextField.text, let walletPaid = Double(wallet) else{
            SaleInfoObject.walletPay = 0.0
            self.refreshView()
            return
        }
        if let walletAmount = SaleInfoObject.walletAmount?.toDouble(), walletPaid > walletAmount{
            self.showAlert(title: Constants.currencySymbol + " \(walletAmount)", message: LanguageManager.YouCanNotPayMoreThan)
            SaleInfoObject.walletPay = walletAmount
            self.refreshView()
            return
        }
        
        if let payableAmount = SaleInfoObject.payableAmount, walletPaid > payableAmount{
            self.showAlert(title: LanguageManager.WalletPayExceedsPayableAmount, message: "")
            SaleInfoObject.walletPay = payableAmount
            self.refreshView()
            return
        }
        
        SaleInfoObject.walletPay = walletPaid
        self.refreshView()
    }
    
    func submitBtnSetup(){
        if isDraftEnable == true{
            self.submitBtn.isEnabled = false
            self.submitBtn.setTitleColor(UIColor.init(red: 208, green: 208, blue: 208), for: .normal)
        }else{
            self.submitBtn.setTitleColor(UIColor.init(red: 61, green: 172, blue: 65), for: .normal)
            self.submitBtn.isEnabled = true
        }
    }
    
    func switchSetUp(){
        self.discountSwitch.setOn(false, animated: true)
        self.delivarySwitch.setOn(false, animated: true)
        
    }
    
    @objc func discountSwitchValueDidChange(sender: UISwitch){
        if discountSwitch.isOn == true {
            self.navigateToSaleWithDiscountVC()
            self.discountSwitch.setOn(true, animated: false)
        }
        else if discountSwitch.isOn == false {
            SaleInfoObject.discount = nil
            self.refreshView()
            self.discountSwitch.setOn(false, animated: true)
        }
    }
    
    @objc func deliverySwitchValueDidChange(sender: UISwitch){
        if (delivarySwitch.isOn == true) {
            self.navigateToSaleOnDeliveryVC()
            //self.delivarySwitch.setOn(true, animated: false)
        }
        else if (delivarySwitch.isOn == false) {
            SaleInfoObject.deliveryCharge = nil
            self.refreshView()
            //self.delivarySwitch.setOn(false, animated: true)
        }
    }
    
    @objc func deliveryCheckBtnDidChange(sender: UIButton){
        //self.deliveryCheckBtn.isSelected = true
        if deliveryCheck{
            sender.setImage(UIImage(named:"uncheck_icon"), for: .normal)
            self.hideDeliveryDetails(is: true)
            self.deliveryDetailsLbl.text = LanguageManager.DeliverySystem
            self.emptyDeliveryData()
            deliveryCheck = false
            self.refreshView()
        }else{
//            sender.setImage( UIImage(named:"check_icon"), for: .normal)
            deliveryCheck = true
            self.navigateToSaleOnDeliveryVC()
            //self.hideDeliveryDetails(is: false)
            //self.setDeliveryData()
        }
        
    }
    
    @objc func onDiscountCheckBtnTapped(sender: UIButton){
        if discountCheck{
            sender.setImage(UIImage(named:"uncheck_icon"), for: .normal)
            self.discountDetailsLbl.text = LanguageManager.Discount
            self.emptyDiscountData()
            discountCheck = false
            self.hideDiscountDetails(is : true)
            self.refreshView()
        }else{
//            sender.setImage( UIImage(named:"check_icon"), for: .normal)
            discountCheck = true
            self.navigateToSaleWithDiscountVC()
//            self.hideDiscountDetails(is : false)
            //self.setDiscountData()
        }
    }
    
    @objc func onDeliverEditBtnTapped(sender: UIButton){
        self.navigateToSaleOnDeliveryVC()
    }
    
    @objc func onDiscountEditBtnTapped(sender: UIButton){
        self.navigateToSaleWithDiscountVC()
    }
    
    func navigateToSaleWithDiscountVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleWithDiscountViewController") as! SaleWithDiscountViewController
        viewController.delegate = self
        viewController.totalPrice = self.totalPrice
        viewController.discountInstanceOfSaleInfoVC = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleOnDeliveryVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleOnDeliveryViewController") as! SaleOnDeliveryViewController
        viewController.delegate = self
        viewController.deliveryCharge = SaleInfoObject.deliveryCharge
        viewController.deliverySystemId = SaleInfoObject.deliverySystemId
        viewController.deliverySystem = SaleInfoObject.deliverySystem
        viewController.deliveryInstanceOfSaleInfoVC = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
//    @objc func onCustomer(sender: UITextField){
//        self.navigateToSaleWithCustomerVC()
//    }
    
    func navigateToSaleWithCustomerVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleWithCustomerViewController") as! SaleWithCustomerViewController
        viewController.delegate = self
        viewController.saleInfoVCInstance = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func navigateToSaleListVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalesListViewController") as! SalesListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func isValidated()->Bool {
        if(SaleInfoObject.paymentMethodId != 1) && (SaleInfoObject.paymentMethodId != 4){
            if (self.paymentPhoneNo.text?.isEmpty)!{
                self.showAlert(title: LanguageManager.Warning, message: LanguageManager.AccountNumberIsRequired)
                return false
            }
            return true
        }else if(SaleInfoObject.paymentMethodId == 4){
            if !(SaleInfoObject.deliverySystemId != nil){
                self.showAlert(title: LanguageManager.Warning, message: LanguageManager.DeliverySystemIsRequired)
                return false
            }
        }
        
        if (SaleInfoObject.paymentMethodId == 1) {
            guard let cashPaid = self.cashText.text, cashPaid != "", let cash = Double(cashPaid) else{
                if self.customerText.text == ""{
                    self.showAlert(title: LanguageManager.Warning, message: LanguageManager.CashIsRequired)
                    return false
                }
                return true
            }
            guard let payable = SaleInfoObject.payableAmount else{
                return false
            }
            if cash < payable {
                if self.customerText.text == "" {
                    self.showAlert(title: LanguageManager.Warning, message: LanguageManager.CustomerIsRequired)
                    return false
                }
            }
            return true
        }else{
            if self.customerText.text == "" {
                self.showAlert(title: LanguageManager.Warning, message: LanguageManager.CustomerIsRequired)
                return false
            }else{
                return true
            }
        }
        
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func generateInvoice(){
        dateFormatter.dateFormat = "yyMMdd"
        let date = dateFormatter.string(from: Date())
        let userInfo = getUserData()
        guard let id = userInfo?.shopId else {
            return
        }
        let sellString = "SEL"
        let extraItemInInvoice = "13"
        
        let invoiceNumber = sellString + "\(id)" + date + extraItemInInvoice
        self.invoice = invoiceNumber
    }
    
    func setUpToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCash(sender:)))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.cashText.inputAccessoryView = toolBar
        
        let paymentPhoneNoToolBar = UIToolbar()
        paymentPhoneNoToolBar.barStyle = UIBarStyle.default
        paymentPhoneNoToolBar.isTranslucent = true
        paymentPhoneNoToolBar.tintColor = UIColor.black
        paymentPhoneNoToolBar.sizeToFit()
        
        let paymentPhoneNoDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnPaymentPhone(sender:)))
        paymentPhoneNoToolBar.setItems([cancelButton, spaceButton, paymentPhoneNoDoneButton], animated: false)
        paymentPhoneNoToolBar.isUserInteractionEnabled = true
        
        self.paymentPhoneNo.inputAccessoryView = paymentPhoneNoToolBar
        
        let walletPayToolbar = UIToolbar()
        walletPayToolbar.barStyle = UIBarStyle.default
        walletPayToolbar.isTranslucent = true
        walletPayToolbar.tintColor = UIColor.black
        walletPayToolbar.sizeToFit()
        
        let walletPaydoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnWalletPay(sender:)))
        toolBar.setItems([cancelButton, spaceButton, walletPaydoneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.walletPayTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingDoneOnCash(sender: UIBarButtonItem){
        self.cashText.resignFirstResponder()
        guard let cash = self.cashText.text, cash != "", let cashPaid = Double(cash) else{ return }
        SaleInfoObject.cash = cashPaid
        self.refreshScreen()
    }
    
    @objc func onPressingDoneOnWalletPay(sender: UIBarButtonItem){
        guard let walletPayText = self.walletPayTextField.text, walletPayText != "", let walletPay = walletPayText.toDouble() else {return}
        SaleInfoObject.walletPay = walletPay
        self.refreshScreen()
    }
    
    //PickerView
    func setUpPickerView(){
        
        paymentMethodPicker.delegate = self

        //PaymentMethod ToolBar
        let paymentMethodToolBar = UIToolbar()
        paymentMethodToolBar.barStyle = UIBarStyle.default
        paymentMethodToolBar.isTranslucent = true
        paymentMethodToolBar.tintColor = UIColor.black
        paymentMethodToolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let paymentDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnPaymentMethod(sender:)))
        paymentMethodToolBar.setItems([cancelButton, spaceButton, paymentDoneButton], animated: false)
        paymentMethodToolBar.isUserInteractionEnabled = true
        
        self.paymentMethodText.inputView = paymentMethodPicker
        self.paymentMethodText.inputAccessoryView = paymentMethodToolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnPaymentMethod(sender: UIBarButtonItem){
        self.paymentMethodText.resignFirstResponder()
        if self.paymentMethodId != 1 && self.paymentMethodId != 4{
            self.paymentPhoneNo.isHidden = false
            if SaleInfoObject.hasWallet == 1{
                self.hideCustomerWallet(bool: false)
            }else{
                self.hideCustomerWallet(bool: true)
            }
        }else if self.paymentMethodId == 4{
            self.paymentPhoneNo.isHidden = true
            self.hideCustomerWallet(bool: true)
        }else if self.paymentMethodId == 1{
            self.paymentPhoneNo.isHidden = true
            if SaleInfoObject.hasWallet == 1{
                self.hideCustomerWallet(bool: false)
            }else{
                self.hideCustomerWallet(bool: true)
            }
        }else{
            self.paymentPhoneNo.isHidden = false
            self.paymentPhoneHeight.constant = 40.0
            if SaleInfoObject.hasWallet == 1{
                self.hideCustomerWallet(bool: false)
            }else{
                self.hideCustomerWallet(bool: true)
            }
        }
    }
    
    @objc func onPressingDoneOnPaymentPhone(sender: UIBarButtonItem){
        if (self.paymentMethodId == 2) {
            SaleInfoObject.paymentPhone = self.paymentPhoneNo.text
            self.submitBtn.becomeFirstResponder()
        }else if (self.paymentMethodId == 3){
            SaleInfoObject.paymentPhone = self.paymentPhoneNo.text
            self.submitBtn.becomeFirstResponder()
        }
    }
    
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            self.submitSaleStoreData()
        }
        
    }
    
    func submitSaleStoreData(){
        
        guard let totalPrice = self.totalPrice else {
            return
        }
        
        guard let totalPayable = SaleInfoObject.payableAmount else {
            return
        }
        
        let discount = SaleInfoObject.discount ?? 0

        let discountUnit = SaleInfoObject.discountUnit ?? ""
        
        let deliveryCharge = SaleInfoObject.deliveryCharge ?? 0
        
        let cash = SaleInfoObject.cash ?? 0
        
        let walletPay = SaleInfoObject.walletPay ?? 0.0
//        let saleDate =
        
        guard let due = SaleInfoObject.due else {
            return
        }
        
        guard let cashBack = self.cashBackAmount else{
            return
        }

        let paymentMethodId = SaleInfoObject.paymentMethodId ?? 1
        
        //let deliverySystem = SaleInfoObject.deliverySystem ?? ""
        let deliverySystemId = SaleInfoObject.deliverySystemId ?? nil
        let deliveryToken = SaleInfoObject.deliveryToken ?? ""
        //let deliveryId = "\(String(describing: deliverySystemId))"
        
        //print(deliverySystemId)

        let customerId = SaleInfoObject.customerId ?? SalePreOrderInfoObject.customerId
        //let cusId = "\(String(describing: customerId))"
        SaleInfoObject.paymentPhone = self.paymentPhoneNo.text
        let paymentPhoneNo = SaleInfoObject.paymentPhone ?? ""
//        guard let invoiceNumber = self.invoice else{
//            return
//        }
        
        let date = SaleInfoObject.saleDate ?? currentDateFormatter(format: "yyyy-MM-dd", date: Date())
        
        var params : [String : Any] = ["products" : self.selectedProductId, "quantities" : self.selectedAmounts, "prices": self.selectedSellingPrices, "serials": self.selectedSerials, "total_amount": totalPrice, "discount": discount, "discount_unit": discountUnit, "payable_amount" : totalPayable , "cash" : cash, "due" : due , "cash_back" : cashBack, "delivery_system" : deliverySystemId, "delivery_charge" : deliveryCharge , "sale_token" : deliveryToken, "payment_method" : paymentMethodId, "payment_number": paymentPhoneNo, "customer" : customerId, "date": date, "wallet": walletPay]
        
        if SalePreOrderInfoObject.preOrderId != nil{
            params["pre_order_id"] = SalePreOrderInfoObject.preOrderId ?? 0
        }
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        self.presenter.postSalesDataToServer(params : params)
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigateToSaleListVC()
            }
        
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecifiedController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is SalesListViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    @objc func onSaveAsDraftBtnTapped(sender: UIButton){
        if self.selectedProductId.count > 0{
            self.submitDraftData()
        }
    }
    
    func submitDraftData(){
        
        let totalPrice = self.totalSelligPrice(amounts: self.selectedAmounts, sellingPrices: self.selectedSellingPrices)
        
        let totalPayable = totalPrice
        
        let discount = SaleInfoObject.discount ?? nil
        
        let discountUnit = SaleInfoObject.discountUnit ?? ""
        
        let deliveryCharge = SaleInfoObject.deliveryCharge ?? nil
        let deliveryToken = SaleInfoObject.deliveryToken ?? ""
        
        let cash = SaleInfoObject.cash ?? 0
        
        let walletPay = SaleInfoObject.walletPay ?? 0.0
        
        let due = self.dueAmount ?? 0.0
        
        let cashBack = self.cashBackAmount ?? 0.0
        
        let paymentMethodId = SaleInfoObject.paymentMethodId ?? 1
        
        let deliverySystemId = SaleInfoObject.deliverySystemId ?? nil
        let customerId = SaleInfoObject.customerId ?? nil
        SaleInfoObject.paymentPhone = self.paymentPhoneNo.text
        let paymentPhoneNo = SaleInfoObject.paymentPhone ?? ""
        
        let date = SaleInfoObject.saleDate ?? currentDateFormatter(format: "yyyy-MM-dd", date: Date())
        
        let params : [String : Any] = ["draft_id": "","products" : self.selectedProductId, "quantities" : self.selectedAmounts, "prices": self.selectedSellingPrices, "serials": self.selectedSerials, "total_amount": totalPrice, "discount": discount, "discount_unit": discountUnit, "payable_amount" : totalPayable , "cash" : cash, "due" : due , "cash_back" : cashBack, "delivery_system" : deliverySystemId, "delivery_charge" : deliveryCharge , "sale_token": deliveryToken, "payment_method" : paymentMethodId, "payment_number": paymentPhoneNo, "customer" : customerId, "date": date, "wallet": walletPay, "step_no": 2]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        self.presenter.postSaleDraftStoreDataToServer(param: params)
        
    }
    
    func validateSalePreOrder() -> Bool{
        if SaleInfoObject.customerId == nil{
            self.showAlert(title: LanguageManager.CustomerIsRequired, message: "")
            return false
        }
        return true
    }
    
    func submitPreOrderData(){
        
        let totalPrice = self.totalSelligPrice(amounts: self.selectedAmounts, sellingPrices: self.selectedSellingPrices)
        
        let totalPayable = totalPrice
        
        let discount = SaleInfoObject.discount ?? 0
        
        let discountUnit = SaleInfoObject.discountUnit ?? ""
        
        let deliveryCharge = SaleInfoObject.deliveryCharge ?? 0
        
        let cash = SaleInfoObject.cash ?? 0
        
        let walletPay = SaleInfoObject.walletPay ?? 0.0
        
        let due = self.dueAmount ?? 0.0
        
        let cashBack = self.cashBackAmount ?? 0.0
        
        let paymentMethodId = SaleInfoObject.paymentMethodId ?? 1
        
        let deliverySystemId = SaleInfoObject.deliverySystemId ?? nil
        let customerId = SaleInfoObject.customerId ?? nil
        SaleInfoObject.paymentPhone = self.paymentPhoneNo.text
        let paymentPhoneNo = SaleInfoObject.paymentPhone ?? ""
        
        let date = SaleInfoObject.saleDate ?? currentDateFormatter(format: "yyyy-MM-dd", date: Date())
        
        let params : [String : Any] = ["products" : self.selectedProductId, "quantities" : self.selectedAmounts, "prices": self.selectedSellingPrices, "serials": self.selectedSerials, "total_amount": totalPrice, "discount": discount, "discount_unit": discountUnit, "payable_amount" : totalPayable , "cash" : cash, "due" : due , "cash_back" : cashBack, "delivery_system" : deliverySystemId, "delivery_charge" : deliveryCharge , "payment_method" : paymentMethodId, "payment_number": paymentPhoneNo, "customer" : customerId, "date": date, "wallet": walletPay]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        self.presenter.postSalePreOrderDataStoreToServer(param: params)
        
    }

}

extension SaleInfoViewController {
    func refreshView(){
        
        // সর্বমোট
        guard self.selectedAmounts.count > 0 else { return }
        guard self.selectedSerials.count > 0 else { return }
        
        var temp : Int = 0
        guard self.selectedSellingPrices.count > 0 else { return }
        var totalPrice = 0.0
        for price in self.selectedSellingPrices {
            if self.selectedAmounts.count > 0{
                let tempTotal = price * self.selectedAmounts[temp]
                totalPrice += tempTotal
                temp += 1
            }
        }
        
        totalPriceLabel.text = LanguageManager.Total + " : "  + " " + "\(totalPrice)" + Constants.currencySymbol
        
        var cWallet : Double = 0.0
        
        if SaleInfoObject.hasWallet == 1{
            cWallet = SaleInfoObject.walletPay ?? 0.0
            if cWallet <= 0{
                cWallet = 0
            }
            self.walletLbl.text = LanguageManager.CustomerWallet + ": " + "\(cWallet)" + Constants.currencySymbol
            self.walletAmountTextField.text = SaleInfoObject.walletAmount
//            self.walletPayTextField.text = SaleInfoObject.walletPay?.toString()
            self.hideCustomerWallet(bool: false)
        }else{
            self.hideCustomerWallet(bool: true)
        }
        
        let paidAmount = (SaleInfoObject.cash ?? 0) + (SaleInfoObject.walletPay ?? 0)
        
        self.paidAmountLbl.text = LanguageManager.PaidAmount + " : " + "\(paidAmount)"
        
        let cash = SaleInfoObject.cash ?? 0
        
        self.cashLbl.text = LanguageManager.Cash + " : " + "\(cash)" + Constants.currencySymbol

        var totalDiscount = 0.0
        
        let discount = SaleInfoObject.discount ?? 0
        
        if let unit = SaleInfoObject.discountUnit , unit == "৳" {
            totalDiscount = discount
        }else if SaleInfoObject.discountUnit ?? "" == "%" {
            let discounted = (totalPrice * (discount/100))
            totalDiscount = discounted
        }
        
        SaleInfoObject.discount = totalDiscount
        
        self.discountLbl.text = LanguageManager.Discount + " : " + "\(totalDiscount)"
        if SaleInfoObject.discount != nil{
            self.discountDetailsLbl.text = LanguageManager.Discount + " " + "\(totalDiscount)" + (" ৳")
        }else{
            self.discountDetailsLbl.text = LanguageManager.Discount
        }
        
        
        let deliveryCharge = SaleInfoObject.deliveryCharge ?? 0.0
        self.deliveryChargeLbl.text = LanguageManager.DeliveryCharge + " : " + " \(deliveryCharge)" + Constants.currencySymbol
        self.deliveryChargeAmountLbl.text = Constants.currencySymbol + " " + "\(deliveryCharge)"
        self.deliveryDetailsLbl.text = SaleInfoObject.deliverySystem ?? "Delivery System"
        
        var totalPayable = totalPrice + deliveryCharge - totalDiscount
        self.payableAmount = totalPayable
        if totalPayable < 0 {
            totalPayable = 0.0
        }
        SaleInfoObject.payableAmount = totalPayable
        self.payableAmountLbl.text = LanguageManager.PayableAmount + ": " + " \(totalPayable)" + Constants.currencySymbol
        
        var totalDue = 0.0
        var totalReturn = 0.0
        
        if totalPayable > paidAmount {
            totalDue = totalPayable - paidAmount
            totalReturn = 0.0
        }
        else if totalPayable < paidAmount  {
            totalDue = 0.0
            totalReturn = cash - totalPayable
        }
        else{
            totalDue = 0.0
            totalReturn = 0.0
        }
        
        self.dueLbl.text = LanguageManager.Due + " : " + " \(totalDue)" + Constants.currencySymbol
        self.dueAmount = totalDue
        SaleInfoObject.due = totalDue
        self.cashBackLbl.text = LanguageManager.CashBack + " : " + " \(totalReturn)" + Constants.currencySymbol
        self.cashBackAmount = totalReturn
        
        self.customerText.text = SaleInfoObject.customerName ?? SalePreOrderInfoObject.customerName
        
        let paymentMethodId = SaleInfoObject.paymentMethodId
        if paymentMethodId == 4{
            self.hideCustomerWallet(bool: true)
            self.paymentPhoneNo.isHidden = true
        }else if paymentMethodId == 1{
            self.paymentPhoneNo.isHidden = true
        }
        
        //print(SaleInfoObject.customerName)
    }
}

//Mark: TextField Delegate
extension SaleInfoViewController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.customerText{
            self.navigateToSaleWithCustomerVC()

        }
        return false
    }
}

//Mark: PickerViewDelegate
extension SaleInfoViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.paymentMethodPicker{
            return paymentMethodList.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.paymentMethodPicker{
            if self.paymentMethodList.count > 0 {
                let item = self.paymentMethodList[row]
                self.paymentMethodId = item.id
                SaleInfoObject.paymentMethodId = item.id
                if self.paymentMethodId != 1 && self.paymentMethodId != 4 {
                    self.paymentPhoneNo.isHidden = false
                    self.paymentPhoneHeight.constant = 40.0
                    self.paymentPhoneNo.placeholder = LanguageManager.AccountNumber
                    self.hideCustomerWallet(bool: true)
                }else if self.paymentMethodId == 4{
                    self.hideCustomerWallet(bool: true)
                }
                else{
                    self.paymentPhoneNo.isHidden = true
                    
                    self.paymentPhoneHeight.constant = 0.0
                    if SaleInfoObject.hasWallet == 1{
                        self.hideCustomerWallet(bool: false)
                    }
                }
                SaleInfoObject.paymentMethodId = item.id
                return item.name
            }
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.paymentMethodPicker{
            if self.paymentMethodList.count > 0 {
                let list = self.paymentMethodList[row]
                self.paymentMethodText.text = list.name
                SaleInfoObject.paymentMethodId = list.id
                if list.id == 4 {
                    self.cashText.isEnabled = false
                    self.cashText.text = ""
                    SaleInfoObject.cash = 0.0
                    self.paymentMethodId = list.id
                    SaleInfoObject.paymentMethodId = list.id
                    self.paymentPhoneNo.isHidden = true
                    self.paymentPhoneHeight.constant = 0.0
                    self.hideCustomerWallet(bool: true)
                    self.refreshView()
                }else if (list.id != 1) && (list.id != 4){
                    self.paymentPhoneNo.isHidden = false
                    self.paymentPhoneHeight.constant = 40.0
                    self.paymentMethodId = list.id
                    SaleInfoObject.paymentMethodId = list.id
                    self.paymentPhoneNo.placeholder = LanguageManager.AccountNumber
                    self.hideCustomerWallet(bool: true)
                    self.refreshView()
                }else{
                    self.cashText.isEnabled = true
                    self.paymentMethodId = list.id
                    SaleInfoObject.paymentMethodId = list.id
                    self.paymentPhoneNo.isHidden = true
                    self.paymentPhoneHeight.constant = 0.0
                    if SaleInfoObject.hasWallet == 1{
                        self.hideCustomerWallet(bool: false)
                    }
                    self.refreshView()
                }
            }else{
                return
            }
        }
    }
    
}

//Mark: SetData
extension SaleInfoViewController : SaleInfoRefreshDelegate{
    func refreshScreen() {
        self.refreshView()
    }
}

//Mark: Api Delegate
extension SaleInfoViewController : SaleInfoViewDelegate {
    func onPreOrderStoreSuccessful(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.showMessage(userMessage: message)
    }
    
    func onDraftStoreSuccessful(data: AddDataMapper) {
        guard let message = data.message else{ 
            return
        }
        self.showMessage(userMessage: message)
    }
    
    func setPaymentMethod(data: PaymentMethodDataMapper) {
        guard var list = data.paymentMethodList, list.count > 0 else {
            return
        }
        for item in list{
            if item.id == 1{
                list = list.filter{$0.id != item.id}
            }
        }
        let cash = PaymentMethodList.init(id: 1, name: "Cash")
        list.insert(cash, at: 0) 
        
//        let cash = list.filter{$0.id == 1}
//        list.insert(cash, at: 0)
//        let cashId = list.filter{$0.id == 1}
//        let cashName = list.filter{$0.name == "Cash"}
//        let item = PaymentMethodList.init(id: cash, name: <#T##String#>)
        self.paymentMethodList = list
        
    }
    
    func setCustomer(data: AllCustomerDataMapper) {
        guard let list = data.customers, list.count > 0 else {
            return
        }
        self.customerNameList = list
        //self.setUpSearchTextField()
    }

    func onSuccess(data: AddDataMapper) {
        self.resetBundleData()
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    fileprivate func resetBundleData(){
        SaleObject.resetBundleData()
    }
    
    func onFailed(message: String) {
        self.showAlert(title: message, message: "")
    }
    
    func onCustomerFailed(message: String) {
        
    }
    
    func showLoading() {
//        self.submitBtnControlWith(isEnabled: false)
        self.submitBtnSetup()
        self.showLoader()
    }
    
    func hideLoading() {
        //self.submitBtnControlWith(isEnabled: true)
        self.submitBtnSetup()
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.getDataFromServer()
    }
    
    func getDataFromServer(){
        self.presenter.getPaymentMethodDataFromServer()
        self.presenter.getAllCustomerDataFromServer(searchString: self.getAll)
    }
}

//Mark: SaleDate
extension SaleInfoViewController{
    func showSaleDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        dateTextField.text = currentDateFormatter(format: "dd MMM, yyyy", date: datePicker.date)
        SaleInfoObject.saleDate = currentDateFormatter(format: "yyyy-MM-dd", date: datePicker.date)
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func currentDateFormatter(format: String, date: Date) -> String{
        dateFormatter.dateFormat = format
        let currentDate = dateFormatter.string(from: date)
        return currentDate
    }
    
}

extension SaleInfoViewController{
    func hideCustomerWallet(bool: Bool){
        self.walletLbl.isHidden = bool
        self.walletPayLbl.isHidden = bool
        self.walletAmountTitleLbl.isHidden = bool
        self.walletAmountTextField.isHidden = bool
        self.walletPayTextField.isHidden = bool
    }
    
    func hideSubmitBtn(){
        if self.isDraftEnable == true{
            self.submitBtn.isEnabled = false
        }else{
            self.submitBtn.isEnabled = true
        }
    }
    
}

extension SaleInfoViewController{
    
    func showMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: userMessage, message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigateToSaleListViewController()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToSaleListViewController(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalesListViewController") as! SalesListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension SaleInfoViewController{
    func hideDiscountDetails(is hidden : Bool){
        self.discountEditBtn.isHidden = hidden
    }
    
    func hideDeliveryDetails(is hidden : Bool){
        self.deliveryChargeAmountLbl.isHidden = hidden
        self.deliveryEditBtn.isHidden = hidden
    }
    
    func setDeliveryData(){
        self.deliveryDetailsLbl.text = SaleInfoObject.deliverySystem
        self.deliveryChargeAmountLbl.text = Constants.currencySymbol + " " + (SaleInfoObject.deliveryCharge?.toString() ?? "0.00")
    }
    
    func setDiscountData(){
        self.discountDetailsLbl.text = LanguageManager.Discount + " " + "\(SaleInfoObject.discount?.toString() ?? "")" + (SaleInfoObject.discountUnit ?? "")
    }
    
    func hideDeliverySystem(is hidden : Bool){
        self.deliveryCheckBtn.isHidden = hidden
        self.deliveryDetailsLbl.isHidden = hidden
        self.deliveryChargeAmountLbl.isHidden = hidden
        self.deliveryEditBtn.isHidden = hidden
    }
    
    func emptyDeliveryData(){
        SaleInfoObject.deliverySystemId = nil
        SaleInfoObject.deliverySystem = nil
        SaleInfoObject.deliveryCharge = nil
        SaleInfoObject.deliveryToken = nil
    }
    
    func emptyDiscountData(){
        SaleInfoObject.discount = nil
        SaleInfoObject.discountUnit = nil
    }
    
}


