//
//  FreemiumSaleInfoVC.swift
//  Ponno
//
//  Created by a k azad on 21/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class FreemiumSaleInfoVC: UIViewController {
    
    @IBOutlet weak var saleSummaryView: UIView!{
        didSet{
            self.setUpCardView(uiview: saleSummaryView)
        }
    }
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var payableAmountLbl: UILabel!
    @IBOutlet weak var cashLbl: UILabel!
    @IBOutlet weak var cashBackLbl: UILabel!
    @IBOutlet weak var dueLbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var cashText: UITextField!
    @IBOutlet weak var discountTitleLbl: UILabel!
    @IBOutlet weak var discountTextField: UITextField!
    
    @IBOutlet weak var discountSegment: UISegmentedControl!
    @IBOutlet weak var cashTitleLbl: UILabel!
    @IBOutlet weak var customerTitleLbl: UILabel!
    @IBOutlet weak var customerText: UITextField!
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    
    fileprivate var presenter = FreemiumSaleInfoPresenter(service: SalesService())
    
    var name : String?
    var customerId : Int?
    var currentPage: Int = 1
    var getAll : Bool = true
    var totalPrice : Double?
    
    var discountPrice : Double?
    var discountUnit : String?
    var payableAmount : Double?
    var dueAmount : Double?
    var cashBackAmount : Double?
    
    //
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedSellingPrices : [Double] = []
    
    var datePicker = UIDatePicker()
    var dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setAttributesTitle()
        self.initialSetup()
        self.showSaleDatePicker()
        self.resetSaleInfoViewController() 
        self.attachPresenter()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if SaleInfoObject.discount == nil || SaleInfoObject.discountUnit == nil{
            SaleInfoObject.discountUnit = "৳"
        }
    }
    
    func setAttributesTitle(){
        self.setUpInitialLanguage()
        cashText.placeholder = LanguageManager.Amount
        discountTitleLbl.text = LanguageManager.Discount
        cashTitleLbl.text = LanguageManager.Cash
        dateLbl.text = LanguageManager.SaleDate
        customerTitleLbl.text = LanguageManager.Customer
        customerText.placeholder = LanguageManager.Customer
        discountTextField.placeholder = LanguageManager.Amount
        
        discountSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
        
    }
    
    func initialSetup(){
        guard self.selectedAmounts.count > 0 else { return }
        
        var temp : Int = 0
        guard self.selectedSellingPrices.count > 0 else { return }
        var totalPrice = 0.0
        for price in self.selectedSellingPrices {
            if self.selectedAmounts.count > 0{
                let tempTotal = price * self.selectedAmounts[temp]
                totalPrice += tempTotal
                temp += 1
            }
        }
        
        self.totalPrice = totalPrice
        
        totalPriceLabel.text = LanguageManager.Total + " : " + "\(totalPrice)" + Constants.currencySymbol
        discountLbl.text = LanguageManager.Discount + " : ___"
        payableAmountLbl.text = LanguageManager.PayableAmount + " : " + "\(totalPrice)" + Constants.currencySymbol
        cashLbl.text = LanguageManager.Cash + " : ___"
        cashBackLbl.text = LanguageManager.CashBack + " : ___"
        dueLbl.text = LanguageManager.Due + " : " + "\(totalPrice)" + Constants.currencySymbol
        
        SaleInfoObject.cash = 0.0
        SaleInfoObject.customerId = nil
        SaleInfoObject.discountUnit = "৳"
        discountSegment.selectedSegmentIndex = 0
        
        self.dateTextField.text = dateFormatter.string(from: Date())
        
        self.customerText.delegate = self
        
        self.cashText.addTarget(self, action: #selector(cashTextDidChange(_textfield:)), for: .editingChanged)
        self.discountTextField.addTarget(self, action: #selector(discountTextDidChange), for: .editingChanged)
        self.discountSegment.addTarget(self, action: #selector(onSegmentControl), for: .valueChanged)
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        
    }
    
    @objc func cashTextDidChange(_textfield: UITextField){
        guard let cashPaid = self.cashText.text, let cash = Double(cashPaid) else {
            SaleInfoObject.cash = 0.0
            self.refreshView()
            return
        }
        SaleInfoObject.cash = cash
        self.refreshView()
    }
    
    @objc func onCustomer(sender: UITextField){
        self.navigateToSaleWithCustomerVC()
    }
    
    @objc func discountTextDidChange(_textfiled: UITextField){
        guard let discounted = self.discountTextField.text, discounted != "", let discountPrice = Double(discounted) else{
            SaleInfoObject.discount = 0.0
            self.refreshView()
            return
        }
        SaleInfoObject.discount = discountPrice
        self.refreshView()
    }
    
    
    func navigateToSaleWithCustomerVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleWithCustomerViewController") as! SaleWithCustomerViewController
        viewController.delegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func isValidated()->Bool {
        guard let cashPaid = self.cashText.text, cashPaid != "", let cash = Double(cashPaid) else{
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.CashIsRequired)
            return false
        }
        guard let payable = self.payableAmount else{
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.PayableAmountIsRequired)
            return false
        }
        if cash < payable {
            if self.customerText.text == "" {
                self.showAlert(title: LanguageManager.Warning, message: LanguageManager.CustomerIsRequired)
                return false
            }
        }
        return true
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func setupToolbar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCash(sender:)))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.cashText.inputAccessoryView = toolBar
    }
    
    @objc func onPressingDoneOnCash(sender: UIBarButtonItem){
        self.cashText.resignFirstResponder()
        guard let cash = self.cashText.text, cash != "", let cashPaid = Double(cash) else{ return }
        SaleInfoObject.cash = cashPaid
        self.refreshScreen()
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onSegmentControl(sender: UISegmentedControl){
        if self.discountSegment.selectedSegmentIndex == 0 {
            SaleInfoObject.discountUnit = "৳"
        }else{
            SaleInfoObject.discountUnit = "%"
        }
        self.refreshScreen()
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            self.submitData()
        }
    }
    
    func submitData(){
        
        guard let totalPrice = self.totalPrice else {
            return
        }
        
        guard let totalPayable = self.payableAmount else {
            return
        }
                
        let discount = SaleInfoObject.discount ?? 0
        let discountUnit = SaleInfoObject.discountUnit ?? ""
        
        let cash = SaleInfoObject.cash ?? 0
        
        guard let due = self.dueAmount else {
            return
        }
        
        guard let cashBack = self.cashBackAmount else{
            return
        }
        
        let customerId = SaleInfoObject.customerId ?? nil
        
        guard let date = self.dateTextField.text else{
            return
        }
        
        dateFormatter.dateFormat = "yyyy-mm-dd"
        let dateString = dateFormatter.date(from: date)
        
        let params : [String : Any] = ["products" : self.selectedProductId, "quantities" : self.selectedAmounts, "prices": self.selectedSellingPrices, "total_amount": totalPrice, "discount": discount, "discount_unit": discountUnit, "payable_amount" : totalPayable , "cash" : cash, "due" : due , "cash_back" : cashBack, "customer" : customerId, "date": dateString]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        self.presenter.postSalesDataToServer(params : params)
    }
    
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigateToSaleListVC()
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToSaleListVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalesListViewController") as! SalesListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func resetSaleInfoViewController(){
        SaleInfoObject.resetSaleInfoObject()
    }
    
}

//Mark: SetData
extension FreemiumSaleInfoVC : SaleInfoRefreshDelegate{
    func refreshScreen() {
        self.refreshView()
    }
}

//Mark: SaleDate
extension FreemiumSaleInfoVC{
    func showSaleDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        dateTextField.text = currentDateFormatter(format: "dd MMM, yyyy", date: datePicker.date)
        SaleInfoObject.saleDate = currentDateFormatter(format: "yyyy-MM-dd", date: datePicker.date)
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func currentDateFormatter(format: String, date: Date) -> String{
        dateFormatter.dateFormat = format
        let currentDate = dateFormatter.string(from: date)
        return currentDate
    }
    
}


extension FreemiumSaleInfoVC {
    func refreshView(){
        
        // সর্বমোট
        guard self.selectedAmounts.count > 0 else { return }
        
        var temp : Int = 0
        guard self.selectedSellingPrices.count > 0 else { return }
        var totalPrice = 0.0
        for price in self.selectedSellingPrices {
            if self.selectedAmounts.count > 0{
                let tempTotal = price * self.selectedAmounts[temp]
                totalPrice += tempTotal
                temp += 1
            }
        }
        
        totalPriceLabel.text = LanguageManager.Total + " : "  + " " + "\(totalPrice)" + Constants.currencySymbol

        var totalCash = SaleInfoObject.cash ?? 0
        
        if totalCash < 0{
            totalCash = 0
        }
        self.cashLbl.text = LanguageManager.Cash + " : " + "\(totalCash)" + Constants.currencySymbol
  
        var totalDiscount = 0.0
        
        let discount = SaleInfoObject.discount ?? 0
        
        if let unit = SaleInfoObject.discountUnit , unit == "৳" {
            totalDiscount = discount
        }else if SaleInfoObject.discountUnit ?? "" == "%" {
            let discounted = (totalPrice * (discount/100))
            totalDiscount = discounted
        }
        
        self.discountLbl.text = LanguageManager.Discount + " : " + "\(totalDiscount)"
        
        var totalPayable = totalPrice - totalDiscount
        self.payableAmount = totalPayable
        if totalPayable < 0 {
            totalPayable = 0.0
        }
        
        self.payableAmountLbl.text = LanguageManager.PayableAmount + ": " + " \(totalPayable)" + Constants.currencySymbol
        
        var totalDue = 0.0
        var totalReturn = 0.0
        
        if totalPayable > totalCash {
            totalDue = totalPayable - totalCash
            totalReturn = 0.0
        }
        else if totalPayable < totalCash  {
            totalDue = 0.0
            totalReturn = totalCash - totalPayable
        }
        else{
            totalDue = 0.0
            totalReturn = 0.0
        }
        
        self.dueLbl.text = LanguageManager.Due + " : " + " \(totalDue)" + Constants.currencySymbol
        self.dueAmount = totalDue
        self.cashBackLbl.text = LanguageManager.CashBack + " : " + " \(totalReturn)" + Constants.currencySymbol
        self.cashBackAmount = totalReturn
        
        self.customerText.text = SaleInfoObject.customerName ?? ""
        
    }
}

//Mark: TextField Delegate
extension FreemiumSaleInfoVC : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.customerText{
            self.navigateToSaleWithCustomerVC()

        }
        return false
    }
}

extension FreemiumSaleInfoVC : FreemiumSaleInfoViewDelegate{
    func onSuccess(data: AddDataMapper) {
        self.resetBundleData()
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    fileprivate func resetBundleData(){
        SaleObject.resetBundleData()
    }
    
    func onFailed(message: String) {
        self.showAlert(title: message, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

//SaleInfoObject.discountUnit = "৳"

