//
//  SaleWithDiscountViewController.swift
//  Ponno
//
//  Created by a k azad on 14/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SaleWithDiscountViewController: UIViewController {

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var discountText: UITextField!
    @IBOutlet weak var discountSubmitBtn: UIButton!
    
    var delegate : SaleInfoRefreshDelegate?
    
    var totalPrice : Double?
    var discount : Double?
    
    var discountInstanceOfSaleInfoVC : SaleInfoViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpToolBar()
        self.initialSetUp()
    }
    
    func back(sender: UIBarButtonItem){
        SaleInfoObject.discount = nil
        SaleInfoObject.discountUnit = nil
        _ = navigationController?.popViewController(animated: true)
        discountInstanceOfSaleInfoVC?.onDiscountCheckBtnTapped(sender: UIButton())
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //guard let identifier = segue.identifier else {return}
        SaleInfoObject.discount = nil
        SaleInfoObject.discountUnit = nil
        //_ = navigationController?.popViewController(animated: true)
        discountInstanceOfSaleInfoVC?.discountCheckBtn.setImage(UIImage(named:"uncheck_icon"), for: .normal)
        discountInstanceOfSaleInfoVC?.discountDetailsLbl.text = LanguageManager.Discount
        discountInstanceOfSaleInfoVC?.discountEditBtn.isHidden = true
    }
    
    func initialSetUp(){
        SaleInfoObject.discountUnit = "৳"
        if SaleInfoObject.discount != nil{
            self.discountText.text = SaleInfoObject.discount?.toString() ?? ""
            if SaleInfoObject.discountUnit != "%"{
                self.segmentControl.selectedSegmentIndex = 0
            }else{
                self.segmentControl.selectedSegmentIndex = 1
            }
        }
        
        self.discountSubmitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        
        self.discountText.addTarget(self, action: #selector(discountTextDidChange), for: .editingChanged)
        self.segmentControl.addTarget(self, action: #selector(onSegmentControl), for: .valueChanged)
    }
    
    @objc func discountTextDidChange(_textfiled: UITextField){
        self.setMaxDiscount()
        //self.discount = _textfiled.text?.toDouble()
    }
    
    func setMaxDiscount(){
        if let discount = self.discountText.text?.toDouble(), let totalAmount = self.totalPrice{
            if SaleInfoObject.discountUnit == "%"{
                if discount >= 100{
                    self.discountText.text = "\(100)"
                }
            }else{
                if discount > totalAmount{
                    self.discountText.text = "\(totalAmount)"
                }
            }
        }
    }
    
    
    
    @objc func onSubmit(sender: UIButton){
        if self.isValidated(){
            if segmentControl.selectedSegmentIndex == 0 {
                SaleInfoObject.discountUnit = "৳"
            }else{
                SaleInfoObject.discountUnit = "%"
            }
            SaleInfoObject.discount = self.discountText.text?.toDouble()
            discountInstanceOfSaleInfoVC?.discountCheckBtn?.setImage( UIImage(named:"check_icon"), for: .normal)
            discountInstanceOfSaleInfoVC?.hideDiscountDetails(is : false)
            self.navigationController?.popViewController(animated: true)
            self.delegate?.refreshScreen()
        }
    }
    
    @objc func onSegmentControl(sender: UISegmentedControl){
        if self.segmentControl.selectedSegmentIndex == 0 {
            SaleInfoObject.discountUnit = "৳"
            self.setMaxDiscount()
        }else{
            SaleInfoObject.discountUnit = "%"
            self.setMaxDiscount()
        }
    }

    
    func isValidated()->Bool{
        if self.discountText.text == ""{
            self.showAlert(title: LanguageManager.DiscountIsRequired, message: "")
        }
        return true
    }
    
    func navigateToSaleInfoViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleInfoViewController") as! SaleInfoViewController
        //self.navigationController?.pushViewController(viewController, animated: true)
        self.navigationController?.present(viewController, animated: true, completion: nil
        )
    }
    
    
    func setUpToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.discountText.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        self.discountText.resignFirstResponder()
        
    }
    
}
