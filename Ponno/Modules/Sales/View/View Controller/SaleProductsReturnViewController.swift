//
//  SaleProductsReturnViewController.swift
//  Ponno
//
//  Created by a k azad on 30/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SaleProductsReturnViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = SalesPerDayPresenter(service: SalesService())
    //
    var salePerDaylist : [SaleListDateWise] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    

    let dateSearchTextField: UITextField = UITextField(frame: CGRect(x: 0, y: 0, width: 350.00, height: 35.00))
    
    //
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    var isSearchActive : Bool = false
    
    var perdaySaleId: Int?
    
    //
    var datePicker : UIDatePicker = UIDatePicker()
    var pickerDate: String?
    var toolBar = UIToolbar()
    
    var selectedDate : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.generateTodaysDate()
        self.setUpNavigationBarButton()
        self.configureTableView()
        self.attachPresenter()
        //self.setUpSearchBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.setUpDatePicker()
        self.dateSearchTextField.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
        //self.navigationController?.navigationItem.performSelector(inBackground: #selector(onBackButtonPressed), with: nil)
        //self.navigationItem.performSelector(inBackground: #selector(onBackButtonPressed), with: nil)
        
    }
    
    
    
    @objc func onBackButtonPressed(sender: UIBarButtonItem){
        self.datePicker.removeFromSuperview()
        self.toolBar.removeFromSuperview()
    }
    
    func generateTodaysDate(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.pickerDate = dateFormatter.string(from: datePicker.date)
        self.selectedDate = dateFormatter.string(from: datePicker.date)
    }
    
    func setUpNavigationBarButton(){
        dateSearchTextField.placeholder = "Search Sale Date"
        dateSearchTextField.font = UIFont.systemFont(ofSize: 17)
        dateSearchTextField.textColor = UIColor.black
        dateSearchTextField.text = self.selectedDate
        //textField.backgroundColor = UIColor.red
        dateSearchTextField.textAlignment = .left
        //textField.resignFirstResponder()
        //textField.addTarget(self, action: #selector(onDateBtn), for: .editingDidBegin)
        self.navigationItem.titleView = dateSearchTextField
        //let dateBarBtn = UIBarButtonItem(customView: textField)
        
//        searchBar.disa
        
//        let dateBtn = UIButton(type: .custom)
//        dateBtn.setImage(UIImage (named: "edit"), for: .normal)
//
//        dateBtn.frame = CGRect(x: 0.0, y: 0.0, width: 35.0, height: 35.0)
//
//        let dateBarBtn = UIBarButtonItem(customView: dateBtn)
//
//
//        textField.heightAnchor.constraint(equalToConstant: 35).isActive = true
//        textField.widthAnchor.constraint(equalToConstant: 35).isActive = true
//
        
//
//        self.navigationItem.rightBarButtonItem = textField
        //self.searchBar.keyboardAppearance = false
        
        //self.searchBar.inputView = datePicker
        
        //  self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Date", style: .done, target: self, action: #selector(onDateBtn))
    }
    
    @objc func onDateBtn(sender : UIButton){
        self.setUpDatePicker()
        //self.setUpAnotherDatePicker()
    }
    
    //Mark: SaleReturn
    func setUpDatePicker(){
        datePicker.datePickerMode = UIDatePicker.Mode.date
        datePicker.frame = CGRect(x: 0, y: self.view.frame.height - 200, width: self.view.frame.width, height: 200)
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        toolBar = UIToolbar(frame: CGRect(x: 0, y: self.datePicker.frame.origin.y - 44, width: self.view.frame.width, height: 44))
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        
        //datePicker = UIDatePicker(frame: CGRect(x: 0, y: self.toolBar.frame.height - 10, width: self.view.frame.width, height: 300))
        
        datePicker.backgroundColor = UIColor.white
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)

        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelDatePicker(sender:)))

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))

        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        dateSearchTextField.inputAccessoryView = toolBar
        dateSearchTextField.inputView = datePicker

        //self.view.addSubview(toolBar)
        //self.view.addSubview(datePicker)
        
//        datePicker.datePickerMode = .date
//        datePicker.maximumDate = Date()
//
//        let toolbar = UIToolbar();
//        toolbar.sizeToFit()
//        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(onPressingDone(sender:)));
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//        let startDate = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
//        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
//
//        searchBar.inputAccessoryView = toolbar
        //searchBar.inputView = datePicker
    }
    
//    func setUpDatePicker(){
//        datePicker.datePickerMode = .date
//        datePicker.maximumDate = Date()
//
//        let toolbar = UIToolbar();
//        toolbar.sizeToFit()
//        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(onPressingDone));
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
//        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
//
////        datePickerTextField.inputAccessoryView = toolbar
////        datePickerTextField.inputView = datePicker
//        searchBar.inputAccessoryView = toolbar
//        //searchBar.inputView = datePicker
//        //searchBar.inputView = datePicker
//
//
//    }
    
    @objc func cancelDatePicker(sender: UIBarButtonItem){
        self.datePicker.removeFromSuperview()
        self.toolBar.removeFromSuperview()
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.pickerDate = dateFormatter.string(from: datePicker.date)
        self.dateSearchTextField.text = self.pickerDate
        self.currentPage = 1
        self.dateSearchTextField.text = self.selectedDate
        guard let dateSelected = self.pickerDate else {
            return
        }
        self.dateSearchTextField.text = dateSelected.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
        self.salePerDaylist = []
        //self.refreshTableView()
        //self.datePicker.isHidden = true
        self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: dateSelected)
        
        self.datePicker.removeFromSuperview()
        self.toolBar.removeFromSuperview()
    }

}


// MARK: TableView Delegate & Data Source
extension SaleProductsReturnViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SalesPerDayCell.nib, forCellReuseIdentifier: SalesPerDayCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.salePerDaylist.count > 0 {
            return self.salePerDaylist.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SalesPerDayCell = tableView.dequeueReusableCell(withIdentifier: SalesPerDayCell.identifier, for: indexPath) as! SalesPerDayCell
        cell.selectionStyle = .none
        
        if self.salePerDaylist.count > 0 {
            let saleItem = self.salePerDaylist[indexPath.row]
            
            cell.totalLabel.text = "Total" + " : " + "\(saleItem.total)" + " " + Constants.currencySymbol
            cell.invoiceLabel.text = saleItem.invoice
            cell.dueLabel.text = "Due" + " : " + "\(saleItem.due)" + Constants.currencySymbol
            cell.timeLabel.text = saleItem.time.convertDateFormater(inputDateFormat: DateFormats.HH_mm_ss.rawValue, outputDateFormat: DateFormats.h_mm_a.rawValue)
            cell.deliverySystemLabel.text = "Delivery System" + " : " + saleItem.deliverySystemName
            
            if isLoading == false && indexPath.row == self.salePerDaylist.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                guard let selectedDate = self.selectedDate else{
                    return cell
                }
                self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: selectedDate)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.salePerDaylist.count > 0 {
            let saleItem = self.salePerDaylist[indexPath.row]
            self.navigateToSaleDetailsVC(iD: saleItem.id)
        }
    }
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        viewController.iD = iD 
        let state: Bool = false
        viewController.state = state
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete{
//            if self.salePerDaylist.count > 0 {
//                self.salePerDaylist.remove(at: indexPath.row)
//                tableView.deleteRows(at: [indexPath], with: .fade)
//
//            }
//        }
//    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            
            if self.salePerDaylist.count > 0{
                let salePerDay = self.salePerDaylist[indexPath.row]
                self.perdaySaleId = salePerDay.id
                
                guard let id = self.perdaySaleId else{
                    return
                }
                self.confirmationMessage(userMessage: "Are you sure?", deleteId: id)
            }
            
        }
        delete.backgroundColor = UIColor.red
        
        return [delete]
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: "Delete", message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            self.presenter.cancelPerDaySaleData(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
// MARK: API Delegate
extension SaleProductsReturnViewController: SalesPerDayViewDelegate{
    
    
    func cancelPerDaySales(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func setSalesListData(data: SalePerDayDataMapper) {
        
        guard let list = data.saleListDateWise, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.salePerDaylist += list
        //self.refreshTableView()
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(message: String) {
        self.showAlert(title: "No Information Found", message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let date = self.selectedDate else{
            return
        }
        //print("attachPresenter")
        self.isLoading = true
        self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: date)
    }
    
}

extension SaleProductsReturnViewController {
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: "Warning", message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.salePerDaylist = []
            self.refreshTableView()
            guard let date = self.selectedDate else{
                return
            }
            
            self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: date)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

