//
//  SaleListPopOverViewController.swift
//  Ponno
//
//  Created by a k azad on 9/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class SaleListPopOverViewController: UIViewController {

      @IBOutlet weak var saleDraftBtn: UIButton!
      @IBOutlet weak var salePreOrderBtn: UIButton!
      
      var navigateTo: NavigateToSpecificController?
      
      override func viewDidLoad() {
          super.viewDidLoad()

          self.intialSetup()
      }
      
      func intialSetup(){
          self.setUpInitialLanguage()
          self.saleDraftBtn.setTitle(LanguageManager.SaleDraft, for: .normal)
          self.salePreOrderBtn.setTitle(LanguageManager.SalePreOrder, for: .normal)
          
          self.saleDraftBtn.addTarget(self, action: #selector(onSaleDraftBtnTapped), for: .touchUpInside)
          self.salePreOrderBtn.addTarget(self, action: #selector(onSalePreOrderBtnTapped), for: .touchUpInside)
      }
      
      @objc func onSaleDraftBtnTapped(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
        navigateTo?.navigateToSaleDraftVC()
      }
      
      @objc func onSalePreOrderBtnTapped(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
        navigateTo?.navigateToSalePreOrderVC()
      }

}
