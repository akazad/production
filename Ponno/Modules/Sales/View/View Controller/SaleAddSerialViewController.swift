//
//  SaleAddSerialViewController.swift
//  Ponno
//
//  Created by a k azad on 17/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

struct ScanningStratFrom {
    var AddNewSale = "FromAddSale"
    var SaleAddSerial = "FromSaleAddSerial"
}

protocol SelectedProduct : NSObjectProtocol {
    func setProductData(data: ProductList)
}

protocol SalesAddProductDelegate : NSObjectProtocol{
    func setSerialData(serial : [String], sellingPrice : Double)
    func setProductData(amount : Double, sellingPrice : Double)
}

class SaleAddSerialViewController: UIViewController {
    
    @IBOutlet weak var productNameLbl: UILabel!{
        didSet{
            if let productName = self.selectedProductName{
                productNameLbl.text = productName
            }
        }
    }
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sellingPriceLbl: UILabel!{
        didSet{
            sellingPriceLbl.text = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var sellingPrice: UITextField!{
        didSet{
            sellingPrice.placeholder = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    
    fileprivate var presenter = AddSerialPresenter(service: ProductService())
    
    var serialList : [String] = []
    
    var productSerials : [String] = []
    
    var product : ProductList?
    var delegate : SalesAddProductDelegate?
    var selectedProductDelegate : SelectedProduct?
    //var startScaningFromDelegate : ScaningFrom?
    
    var saleAblePrice : Double?
    var saleAbleQuantity : Int?
    
    var selectedProductName : String?
    
    //CodeScanner
    var isCodeScannerActive : Bool?
    var scaningStart: String?
    var searchSerial : String?
    
    var productId: Int?
    var saleId : Int?
    var salePanelProductVCInstance = SalePanelProductViewController()
    var salePreOrderProductId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpViews()
        
//        self.configureTableView()
        //CodeScan
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.configureTableView()
        self.attachPresenter()
        if self.isCodeScannerActive == true{
            self.checkIfScannerActive()
            self.setBarButton()
        }
        
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNextBtn(sender:)), for: .touchUpInside)
        self.title = LanguageManager.SaleBook
        self.sellingPrice.underlined()
        guard let price = self.saleAblePrice else {
            return
        }
        self.sellingPrice.text = "\(price)"
    }
    
    //CodeScan
    func checkIfScannerActive(){
//        switch self.scaningStart{
//        case ScaningFrom.AddNewSale.rawValue:
//            if let serial = self.searchSerial?.lowercased(){
//                self.productSerials.append(serial)
//            }
//            self.attachPresenter()
//        case ScaningFrom.SaleAddSerial.rawValue:
//            if let serial = self.searchSerial?.lowercased(){
//                self.productSerials.append(serial)
//            }
//        default:
//            break
//        }
        if let startFrom = self.scaningStart{
            if startFrom == "FromAddSale"{
                if let serial = self.searchSerial?.lowercased(){
                    self.productSerials.append(serial)
                }
                self.attachPresenter()
            }else if startFrom == "SaleAddSerial"{
                if let serial = self.searchSerial?.lowercased(){
                    self.productSerials.append(serial)
                }
            }
        }
    }
    
    @objc func onDismissBtn(sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setBarButton(){
        
        let scannerBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        scannerBtn.setImage(UIImage(named: "scan"), for: UIControl.State.normal)
        scannerBtn.addTarget(self, action: #selector(onScanBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        scannerBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        scannerBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        scannerBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn1 = UIBarButtonItem(customView: scannerBtn)
        
        navigationItem.setRightBarButtonItems([barBtn1], animated: true)
    }
    
    @objc func onScanBtnTapped(sender: UIBarButtonItem){
        self.navigateToScannerViewController()
        //self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onNextBtn(sender : UIButton){
        if isValidated(){
            
            guard let price = sellingPrice.text,let priceDouble = Double(price) else{
                return
            }
            if isCodeScannerActive == true{
                if let item = self.product{
                    self.selectedProductDelegate?.setProductData(data: item)
                }
            }
            self.productSerials = self.productSerials.filter{$0 != ""}
            print(self.productSerials)
            self.delegate?.setSerialData(serial: self.productSerials, sellingPrice: priceDouble)
            //salePanelProductVCInstance.selectedSerials = self.productSerials
            //print(salePanelProductVCInstance.selectedSerials.count)
            self.navigationController?.popViewController(animated: true)
                //self.popToAddNewSaleVC()
//            if isCodeScannerActive == true{
//                //navigateToAddNewSaleViewController()
//            }else{
                //self.navigationController?.popViewController(animated: true)
//            }
        }
    }
    func isValidated()->Bool{
        if self.productSerials.count == 0 {
            showAlert(title: "", message: LanguageManager.ProductSerialIsRequired)
            return false
        }
        else if self.sellingPrice.text == ""{
            showAlert(title: "", message: LanguageManager.SellingPriceIsRequired)
            return false
        }
        return true
    }
    
    func popToAddNewSaleVC(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is AddNewSaleViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
//
////Mark: TableView Delegate DataSource
extension SaleAddSerialViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductSerialCell.nib, forCellReuseIdentifier: ProductSerialCell.identifier)
        self.tableView.estimatedRowHeight = 30
        self.refreshTableView()
        self.tableView.allowsMultipleSelection = true
        self.tableView.allowsMultipleSelectionDuringEditing = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) ->  Int {
        if self.serialList.count > 0{
            return self.serialList.count
        }
        return 0
        //print(list.count)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductSerialCell = tableView.dequeueReusableCell(withIdentifier: ProductSerialCell.identifier, for: indexPath) as! ProductSerialCell
        
        if self.serialList.count > 0{
            let serial = self.serialList[indexPath.row]
            cell.serialNumber.text = serial
            
            //print(self.productSerials)

            if self.productSerials.contains(serial){
                cell.checkIcon.image = UIImage(named : "check_icon")
            }
            else{
                cell.checkIcon.image = UIImage(named: "uncheck_icon")
            }

        }
        
        return cell

    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.serialList.count > 0 {
            let serial = self.serialList[indexPath.row]
            
            if !self.productSerials.contains(serial){
                self.productSerials.append(serial)
                //            let joined : [String] = [self.productSerials.joined(separator: ",")]
                //            print(joined)
            }
            else {
                self.productSerials = self.productSerials.filter({ $0 != serial})
            }
            
            self.refreshTableView()
        }
        
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if self.serialList.count > 0 {
            let serial = self.serialList[indexPath.row]
            if self.productSerials.contains(serial){
                self.productSerials.append(serial)
            }
            self.refreshTableView()
        }
    }

    func refreshTableView(){
        self.tableView.reloadData {
            if self.tableView.contentSize.height > 200{
                self.tableViewHeightConstraint.constant = 200
            }
            else {
                self.tableViewHeightConstraint.constant = self.tableView.contentSize.height
            }
        }
    }

}

//CodeScan
extension SaleAddSerialViewController : ProductSerialSearchViewDelegate{
    func setProductSerialData(data: SerialSearchDataMapper) {
        guard let serials = data.serials else{
            return
        }
        
        self.serialList += serials
        self.refreshTableView()
//        self.productSerials = serials
        
        
    }
    
    func setProductAllSerialData(data: ProductSerialDataMapper) {
        guard let serials = data.serials else{
            return
        }
        for item in serials{
            self.serialList.append(item.serialNo)
        }
        
        let reducedList = removeDuplicates(array: self.serialList)
        self.serialList = reducedList
        
        self.refreshTableView()
    }
    
    func onFailed(failure: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        if let text = self.searchSerial{
            self.presenter.getProductSerialSearchDataFromServer(searchString: text)
        }
        
        if let id = self.productId, let saleId = self.saleId
        {
            self.presenter.getAllSerialsDataFromServer(saleId: saleId, productId: id)
        }
        
        
    }
}

extension SaleAddSerialViewController{
    func navigateToScannerViewController(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Scanner", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ScannerViewController") as! ScannerViewController
        viewController.navigateTo = Navigate.saleAddSerial
        viewController.modalPresentationStyle = .fullScreen
        //viewController.scanStartFromDelegate = self
        self.navigationController?.popViewController(animated: true)
    }
    
    func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
                // Do not add a duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        return result
    }
    
}

//extension SaleAddSerialViewController : ScaningFrom{
//    func setStartFrom(from: String) {
//        self.scaningStart = from
//    }
//}
