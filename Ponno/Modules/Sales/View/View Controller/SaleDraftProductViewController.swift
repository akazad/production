//
//  SaleDraftProductViewController.swift
//  Ponno
//
//  Created by a k azad on 10/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

struct CheckString {
  static func blank(text: String) -> Bool {
    let trimmed = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    return trimmed.isEmpty
  }
}

class SaleDraftProductViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var nextView: UIView!{
        didSet{
            self.setUpCardView(uiview: nextView)
        }
    }
    @IBOutlet weak var draftView: UIView!{
        didSet{
            self.setUpCardView(uiview: draftView)
        }
    }
    @IBOutlet weak var draftBtn: UIButton!
    
    fileprivate var presenter = SaleDraftProductPresenter(service: SalesService())
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var editedProducts : [EditedProducts] = []{
        didSet{
            if self.editedProducts.count > 0{
                self.editedProducts = self.editedProducts.sorted(by: { $0.soldQuantity > $1.soldQuantity})
            }
            self.refreshTableView()
        }
    }
    
    var editableProducts : [EditedProducts] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var editedProductList : [EditedProducts] = []
    
    var filteredList : [EditedProducts] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var draftId: Int?
    var saleEditInfo : SaleEditInfo?
    
    var preOrderId: Int?
    
    var searchText = ""
    var timer : Timer?
    
    var selectedProduct : EditedProducts?
    var selectedProductName : String?
    var sellingPrice : Double?
    var saleableQuantity : Double?
    
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedSellingPrices : [Double] = []
    var selectedSerials : [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpViews()
        self.setUpSearchBar()
        self.configureTableView()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setUpInitialLanguage()
        self.setNavigationBar()
        //self.setupDiscardBtn()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.resignFirstResponder()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext(sender:)), for: .touchUpInside)
        self.draftBtn.addTarget(self, action: #selector(onDraftBtnTapped(sender:)), for: .touchUpInside)
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    @objc func onNext(sender : UIButton){
        if self.selectedProductId.count > 0{
            self.navigateToSalePanelProductInfoViewController()
        }
    }
    
    func setupDiscardBtn(products: [EditedProducts]){
        if self.checkProductQuantity(data: products) == true{
            self.draftBtn.setTitle(LanguageManager.Discard, for: .normal)
            self.draftBtn.setTitleColor(UIColor.red, for: .normal)
            self.nextView.isHidden = true
            self.nextBtn.isHidden = true
        }else{
            self.draftBtn.setTitle(LanguageManager.UpdateDraft, for: .normal)
            self.draftBtn.setTitleColor(UIColor.init(red: 61, green: 172, blue: 65), for: .normal)
            self.nextView.isHidden = false
            self.nextBtn.isHidden = false
        }
    }
        
    func navigateToSalePanelProductInfoViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalePanelProductInfoViewController") as! SalePanelProductInfoViewController
        if self.draftId != nil{
            viewController.navigatedFrom? = NavigatedFrom(rawValue: "Draft")!
        }else{
            viewController.navigatedFrom? = NavigatedFrom(rawValue: "PreOrder")!
        }
        viewController.draftId = self.draftId
        viewController.preOrderId = self.preOrderId
        viewController.saleEditInfo = self.saleEditInfo
        viewController.pushedInFromSaleEdit = false
        viewController.selectedProductId = self.selectedProductId
        viewController.selectedAmounts = selectedAmounts
        viewController.selectedSellingPrices = selectedSellingPrices
        viewController.selectedSerials = selectedSerials
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func onDraftBtnTapped(sender: UIButton){
        if self.selectedProductId.count > 0{
                    
            guard self.selectedAmounts.count > 0 else { return }
            guard self.selectedSerials.count > 0 else { return }
            guard self.selectedSellingPrices.count > 0 else { return }
            
            guard let id = self.draftId else{
                return
            }
            
            // সর্বমোট
            let totalPrice = self.totalSelligPrice(amounts: self.selectedAmounts, sellingPrices: self.selectedSellingPrices)
            
            let params : [String : Any] = ["draft_id": id, "products" : self.selectedProductId, "quantities" : self.selectedAmounts, "prices": self.selectedSellingPrices, "serials": self.selectedSerials, "total_amount": totalPrice, "discount": "", "discount_unit": "", "payable_amount" : "" , "cash" : "", "due" : "" , "cash_back" : "", "delivery_system" : "", "delivery_charge" : "" , "payment_method" : "", "payment_number": "", "customer" : "", "date": "", "wallet": "", "step_no": 1, "sale_token": ""]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            if self.checkProductQuantity(data: self.editedProducts) == true{
                self.presenter.postSaleDraftDeleteToServer(param: params)
            }else{
                self.presenter.postSaleDraftStoreDataToServer(param: params)
            }
            
            
//            if SaleStatusObject.isPreOrder == true{
//                self.presenter.postSalePreOrderDataStoreToServer(param: params)
//            }else{
//                self.presenter.postSaleDraftStoreDataToServer(param: params)
//            }
        }
    }

}

//MARK: SearchBar Delegate
extension SaleDraftProductViewController: UISearchBarDelegate{
    fileprivate func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.ProductSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.isSearchActive = true
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if self.editableProducts.count > 0 {
            if searchText == "" {
                self.isSearchActive = false
                return
            }else{
                self.isSearchActive = true
            }
        }
        
        guard let textToSearch = searchBar.text else {
            return
        }
        
        if textToSearch.count > 2{
            filteredList = self.editableProducts.filter { (product : EditedProducts) -> Bool in
                return product.name.lowercased().contains(textToSearch.lowercased())  || product.categoryName.lowercased().contains(textToSearch.lowercased()) || product.variant.lowercased().contains(textToSearch.lowercased())
            }
        }
    }
}

//Mark: TableView Delegate and DataSource
extension SaleDraftProductViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SaleProductCell.nib, forCellReuseIdentifier: SaleProductCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if isSearchActive == true{
                return 0
            }else{
                if self.editedProducts.count > 0 {
                    return self.editedProducts.count
                }
                return 0
            }
            
        case 1:
            if isSearchActive{
                guard self.filteredList.count > 0 else {
                    return 0
                }
                return self.filteredList.count
            }else{
                if self.editableProducts.count > 0 {
                    return self.editableProducts.count
                }
                return 0
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SaleProductCell.identifier, for: indexPath) as! SaleProductCell
                
        switch indexPath.section {
            
        case 0:
            if self.editedProducts.count > 0 {
                let product = self.editedProducts[indexPath.row]
                
                cell.editedProducts = product
                self.saleableQuantity = product.quantity.toDouble()
                
                cell.productId = product.productId
                
                
                
                cell.selectBtn.tag = product.productId
                cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
            }
            
        case 1:
            if isSearchActive{
                if self.filteredList.count > 0{
                    let product = self.filteredList[indexPath.row]
                    
                    cell.productId = product.productId
                    cell.editableProduct = product
                }
            }else{
                if self.editableProducts.count > 0 {
                    let product = self.editableProducts[indexPath.row]
                    
                    cell.productId = product.productId
                    cell.editableProduct = product
                }
            }
        default:
            return cell
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            if self.editedProducts.count > 0 {
                let data = self.editedProducts[indexPath.row]
                
                self.didSelectOnEditedProduct(data: data)
            }
        case 1:
            if isSearchActive{
                if self.filteredList.count > 0{
                    let data = self.filteredList[indexPath.row]
                    
                    self.didSelectOnEditableProducts(data: data)
                }
            }else{
                if self.editableProducts.count > 0 {
                    let data = self.editableProducts[indexPath.row]
                    
                    self.didSelectOnEditableProducts(data: data)
                }
            }
        default:
            return
        }
        
    }
    
    func didSelectOnEditedProduct(data: EditedProducts){
        self.sellingPrice = Double(data.soldSellingPrice)
        self.saleableQuantity = Double(data.soldQuantity)
        
        for item in self.editedProducts{
            if item.productId == data.productId{
                self.selectedProduct = item
            }
        }
        
        let quantity = data.quantity
        if quantity.isEmpty == false && quantity != "0.00"{
            if data.hasSerial == 0{
                self.navigateToSaleAddVC()
            }
            else{
                navigateToSaleAddSerialVC(id: data.productId, serials: data.soldSerialNo)
            }
        }else{
            self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
        }
        
    }
    
    func didSelectOnEditableProducts(data: EditedProducts){
        self.selectedProductName = data.name
        self.selectedProduct = data
        self.saleableQuantity = data.quantity.toDouble()
        self.sellingPrice = Double(data.sellingPrice)
        
        let quantity = data.quantity
        if quantity.isEmpty == false && quantity != "0.00"{
            if data.hasSerial == 0{
                self.navigateToSaleAddVC()
            }
            else{
                self.navigateToSaleAddWithSerialVC(serials: data.serials)
            }
        }else{
            self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onRemove(sender : UIButton){
        let productId = sender.tag
        
        if self.selectedProductId.contains(productId){
            for item in self.editedProducts{
                if item.productId == productId{
                    let soldQuantity = item.soldQuantity
                    removeDataFromEditedProducts(productId: productId, quantity: "\(soldQuantity)")
                }
            }
        }
    }
    
    func removeDataFromEditedProducts(productId : Int, quantity: String){
        self.editedProducts = self.editedProducts.filter{ $0.productId != productId}
        self.addDataToEditableProductList(productId: productId, quantity: quantity)
        self.refreshTableView()
    }
    
    func removeDataofProductObject(productId : Int){
        var productAtIndex = -1
        if let index = self.selectedProductId.firstIndex(of: productId){
            productAtIndex = index
        }
//        var productAtIndex = -1
//        for index in 0..<self.selectedProductId.count{
//            if self.selectedProductId[index] == productId{
//                productAtIndex = index
//            }
//        }
        
        self.selectedSellingPrices.remove(at: productAtIndex)
        self.selectedAmounts.remove(at: productAtIndex)
        self.selectedSerials.remove(at: productAtIndex)
        self.selectedProductId.remove(at: productAtIndex)
    }
    
    func addDataToEditableProductList(productId: Int, quantity: String){
        if self.editedProductList.count > 0{
            for item in self.editedProductList{
                if item.productId == productId{
                    let inventoryId = item.inventoryId
                    let productId = productId
                    let quan = item.quantity.toDouble()
                    let soldQuan = quantity.toDouble()
                    var updatedQuantity: String = ""
                    if let quantity = quan, let soldQuantity = soldQuan{
                        updatedQuantity = "\(quantity + soldQuantity)"
                    }
                    let sellingPrice = item.sellingPrice
                    let serials = item.serials
                    let name = item.name
                    let image = item.image
                    let categoryName = item.categoryName
                    let variant = item.variant
                    let unit = item.unit
                    let categoryId = item.categoryId
                    
                    let editableProduct = EditedProducts(inventoryId: inventoryId, productId: productId, quantity: updatedQuantity, sellingPrice: sellingPrice, serials: serials, name: name, image: image, categoryName: categoryName, categoryId: categoryId, variant: variant, unitName: unit)
                    
                    self.editableProducts.append(editableProduct)
                    self.editableProducts = self.editableProducts.sorted(by: { $0.name < $1.name })
                }
            }
        }
    }
    
    func addDataToEditedProducts(productId: Int, soldQuantity: Double, soldSellingPrice: Double){
        if let product = findProductInfo(productId: productId){
            let selectedProduct = EditedProducts(inventoryId: product.inventoryId, productId: productId, quantity: product.quantity, sellingPrice: product.sellingPrice, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, soldSerials: [""], name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
            for item in self.editedProducts{
                if item.productId == productId{
                    self.editedProducts = self.editedProducts.filter{($0.productId != productId)}
                }
            }
            self.editedProducts.append(selectedProduct)
            self.refreshTableView()
        }
    }
    
    func addSerialDataToSaleProducts(productId: Int, soldQuantity: Double, soldSerials: [String], soldSellingPrice: Double){
            if let product = findProductInfo(productId: productId){
                let selectedProduct = EditedProducts(inventoryId: product.inventoryId,productId: productId, quantity: product.quantity, sellingPrice: product.sellingPrice, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, soldSerials: soldSerials, name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
                for item in self.editedProducts{
                    if item.productId == productId{
                        self.editedProducts = self.editedProducts.filter{($0.productId != productId)}
                    }
                }
                self.editedProducts.append(selectedProduct)
                self.refreshTableView()
            }
        }
    
    func navigateToSaleAddVC(){
        let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSecondVC") as! SaleAddSecondVC
        
        viewController.delegate = self
        viewController.saleAblePrice = self.sellingPrice
        viewController.saleAbleQuantity = self.saleableQuantity
        viewController.selectedProductName = self.selectedProductName
        
        viewController.draftProduct = self.selectedProduct
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleAddSerialVC(id: Int,serials : [String]){
        let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSerialViewController") as! SaleAddSerialViewController
        var serialList : [String] = []
        if self.editedProducts.count > 0{
            
            if let product = findProductInfo(productId: id){
                serialList = prepareSerialNumbers(serials: product.serials)
            }
            //let serialList = self.prepareSerialNumbers(serials: serials)
            viewController.saleAblePrice = self.sellingPrice
            viewController.selectedProductName = self.selectedProductName
            viewController.serialList = serialList
            viewController.delegate = self
            viewController.productSerials = serials
            viewController.productId = id
            //viewController.saleId = self.saleId
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleAddWithSerialVC(serials : String){
        if self.prepareSerialNumbers(serials: serials).count > 0{
            let serialList = self.prepareSerialNumbers(serials: serials)
            
            let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSerialViewController") as! SaleAddSerialViewController
            
            viewController.serialList = serialList
            viewController.saleAblePrice = self.sellingPrice
            viewController.delegate = self
            viewController.selectedProductName = self.selectedProductName
            viewController.isCodeScannerActive = false
            //            if codeScannerActive == true{
            //                viewController.isCodeScannerActive = true
            //                viewController.searchSerial = self.searchText
            //            }else{
            //                viewController.isCodeScannerActive = false
            //            }
            
            //viewController.isCodeScannerActive = self.codeScannerActive
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    func removeDataFromSaleableProductList(productId: Int){
        if let product = findProductInfo(productId: productId){
            self.editableProducts = self.editableProducts.filter{($0.productId != product.productId)}
            self.refreshTableView()
        }
    }
        
    func findProductInfo(productId: Int) -> EditedProducts?{
        var productItem : EditedProducts?
        for item in self.editedProductList{
            if item.productId == productId{
                productItem = item
            }
        }
        return productItem
    }
    
}


//Product Delegate
extension SaleDraftProductViewController : SalesAddProductDelegate{
    func setSerialData(serial: [String], sellingPrice: Double) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        if self.selectedProductId.contains(selectedProduct.productId){
            self.removeDataofProductObject(productId: selectedProduct.productId)
        }
        self.selectedProductId.append(selectedProduct.productId)
        self.selectedSerials.append(serial.joined(separator: ","))
        self.selectedSellingPrices.append(sellingPrice)
        self.selectedAmounts.append(Double(serial.count))
        
        self.addSerialDataToSaleProducts(productId: selectedProduct.productId, soldQuantity: Double(serial.count), soldSerials: serial, soldSellingPrice: (sellingPrice))
        self.removeDataFromSaleableProductList(productId: selectedProduct.productId)
        
        self.setupDiscardBtn(products: self.editedProducts)
        self.resetSearchBar()
    }
    
    func setProductData(amount: Double, sellingPrice: Double) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        
        if self.selectedProductId.contains(selectedProduct.productId){
            self.removeDataofProductObject(productId: selectedProduct.productId)
        }
        
        self.selectedProductId.append(selectedProduct.productId)
        self.selectedSellingPrices.append(sellingPrice)
        self.selectedAmounts.append(amount)
        self.selectedSerials.append("")
        
        self.addDataToEditedProducts(productId: selectedProduct.productId, soldQuantity: amount, soldSellingPrice: sellingPrice)
        self.removeDataFromSaleableProductList(productId: selectedProduct.productId)
        
        self.setupDiscardBtn(products: self.editedProducts)
        self.resetSearchBar()
    }
    
    func resetSearchBar(){
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
}


//Mark: Api Delegate
extension SaleDraftProductViewController : SaleDraftProductViewDelegate{
    func onDraftStoreSuccessful(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayDraftMessage(userMessage: message)
    }
    
    func onSaleDraftDelete(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        displayDraftDeleteMessage(userMessage: message)
    }
    
    func setSaleDraftEditData(data: SaleDraftEditDataMapper) {
        guard let products = data.editedProducts, products.count > 0 else{
            return
        }
        self.editedProductList = products
        
        for item in products{
            if item.soldQuantity != 0{
                self.selectedProductId.append(item.productId)
                self.selectedSellingPrices.append(item.soldSellingPrice)
                self.selectedAmounts.append(item.soldQuantity)
                if item.hasSerial != 0{
                    self.selectedSerials.append(item.soldSerialNo.joined(separator: ","))
                }else{
                    self.selectedSerials.append("")
                }
            }
        }
        
        self.editedProducts = products.filter{selectedProductId.contains($0.productId)}
        self.editableProducts = products.filter{ !selectedProductId.contains($0.productId)}
        
        self.saleEditInfo = data.saleEditInfo
        
        if let step = self.saleEditInfo?.stepNo{
            if step == 2 && self.checkProductQuantity(data: self.editedProducts) == false{
                self.navigateToSalePanelProductInfoViewController()
            }
        }
        
        self.setupDiscardBtn(products: self.editedProducts)
        
    }
    
    func checkProductQuantity(data: [EditedProducts]) -> Bool{
        var soldQuantityCheck : Bool = false
        for item in data{
            if let quantity = item.quantity.toDouble(), quantity < item.soldQuantity{
                soldQuantityCheck = true
            }
            
        }
        return soldQuantityCheck
    }
    
    func onFailed(message: String) {
        self.showAlert(title: message, message: "")
    }
    
    func showLoading() {
        
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        if let id = self.draftId {
            self.presenter.getSaleDraftEditDataFromServer(id: id)
        }
        
    }
    
}

extension SaleDraftProductViewController{
    func displayDraftMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.popToSaleDraftListViewController()
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSaleDraftListViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is SaleDraftListViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func displayDraftDeleteMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title:  LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
//    func navigateToSaleDraftListViewController(){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDraftListViewController") as! SaleDraftListViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
    
}





