//
//  SalesCustomerSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 23/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SalesCustomerSearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = SalesPerDayPresenter(service: SalesService())
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var salePerDaylist : [SaleListDateWise] = []{
        didSet{
            self.refreshTableView()
        }
    }

    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var searchText = ""
    var currentPage : Int = 1
    var lastPage : Int = 0
    var isLoading : Bool = false
    
    var timer : Timer?
    
    var selectedDate : String?
    var perdaySaleId: Int?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpSearchBar()
        self.configureTableView()
        self.generatingDate()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    //Search Alert
//    func searchAlert(title :String, message : String) -> Void {
//        DispatchQueue.main.async {
//            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//
//            let OKAction = UIAlertAction(title: "OK", style: .default){
//                (action:UIAlertAction!) in
//                DispatchQueue.main.async {
//                    self.currentPage = 1
//                    self.presenter.getSalesDataFromServer(page: self.currentPage)
//                    self.dismiss(animated: true, completion: nil)
//                }
//            }
//            alertController.addAction(OKAction)
//            self.present(alertController, animated: true, completion: nil)
//
//        }
//    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.salePerDaylist = []
                    self.refreshTableView()
                    guard let date = self.selectedDate else{
                        return
                    }
                    self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: date)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.cancelPerDaySaleData(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
    
    func generatingDate(){
        
        let today = Date()
        print(today)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        self.selectedDate = dateFormatter.string(from: today)
        
    }
    
}

//Mark: Search Delegate
extension SalesCustomerSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.SearchCustomer
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.salePerDaylist = []
        self.isLoading = true
        self.isSearchActive = true
        print(self.searchText)
        self.presenter.getSalesCustomerSearchDataFromServer(page: self.currentPage, customer: self.searchText)
    }
    
}

// MARK: TableView Delegate & Data Source
extension SalesCustomerSearchViewController : UITableViewDelegate, UITableViewDataSource{

    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(CustomerSearchCell.nib, forCellReuseIdentifier: CustomerSearchCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        //self.tableView.addSubview(refreshControl)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.salePerDaylist.count > 0 {
            return self.salePerDaylist.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell : CustomerSearchCell = tableView.dequeueReusableCell(withIdentifier: CustomerSearchCell.identifier, for: indexPath) as! CustomerSearchCell
        cell.selectionStyle = .none

        if self.salePerDaylist.count > 0 {
            let saleItem = self.salePerDaylist[indexPath.row]
            
            cell.salePerDay = saleItem

            cell.totalLabel.text = LanguageManager.Total + " : " + "\(saleItem.total)" + " " + Constants.currencySymbol
            cell.invoiceLabel.text = saleItem.invoice
            cell.dueLabel.text = LanguageManager.Due + " : " + "\(saleItem.due)" + Constants.currencySymbol
            
            cell.popUpBtn.tag = saleItem.id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)

            if isLoading == false && indexPath.row == self.salePerDaylist.count - 1 && self.currentPage < self.lastPage{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getSalesCustomerSearchDataFromServer(page: self.currentPage, customer: self.searchText)
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74.00
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if self.salePerDaylist.count > 0 {
            let saleItem = self.salePerDaylist[indexPath.row]
            self.navigateToSaleDetailsVC(iD: saleItem.id)
        }
    }

    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        viewController.iD = iD
        viewController.state = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    func refreshTableView(){
        self.tableView.reloadData()
    }

    //    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    //        if editingStyle == .delete{
    //            if self.salePerDaylist.count > 0 {
    //                self.salePerDaylist.remove(at: indexPath.row)
    //                tableView.deleteRows(at: [indexPath], with: .fade)
    //
    //            }
    //        }
    //    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {

        return false
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let delete = UITableViewRowAction(style: .normal, title: LanguageManager.Delete) { action, index in

            if self.salePerDaylist.count > 0{
                let salePerDay = self.salePerDaylist[indexPath.row]
                self.perdaySaleId = salePerDay.id

                guard let id = self.perdaySaleId else{
                    return
                }
                self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
            }

        }
        delete.backgroundColor = UIColor.red

        return [delete]
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.salePerDaylist.count > 0{
            self.perdaySaleId = sender.tag
            for saleItem in self.salePerDaylist{
                if self.perdaySaleId == saleItem.id{
                    //let isItSold = serial.isSold
                    guard let id = self.perdaySaleId else{
                        return
                    }
                    let editAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    // add action buttons to action sheet
                    myActionSheet.addAction(editAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        // present the action sheet
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }

}

// MARK: API Delegate
extension SalesCustomerSearchViewController: SalesPerDayViewDelegate{
    func setSalesListData(data: SalePerDayDataMapper) {
        guard let list = data.saleListDateWise, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.salePerDaylist += list

        guard let pagination = data.pagination else {
            return
        }
        self.lastPage = pagination.lastPageNo
    }

    func cancelPerDaySales(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }

    func onFailed(message : String) {
        showAlert(title: LanguageManager.NoInformationFound, message: "")
    }

    func showLoading() {
        self.showLoader()
    }

    func hideLoading() {
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
//        guard let date = self.selectedDate else{
//            return
//        }
//        self.isLoading = true
//        self.presenter.getSalesPerDayDataFromServer(page: self.currentPage, date: date)
    }

}
