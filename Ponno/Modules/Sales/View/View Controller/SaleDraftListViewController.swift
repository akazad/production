//
//  SaleDraftListViewController.swift
//  Ponno
//
//  Created by a k azad on 6/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit
import Floaty

class SaleDraftListViewController: UIViewController {
    
    @IBOutlet weak var emptyMessageLbl: UILabel!{
        didSet{
            emptyMessageLbl.text = LanguageManager.NoInformationFound
        }
    }
    @IBOutlet weak var dateSearchHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    
    fileprivate var presenter = SaleDraftPresenter(service: SalesService())
    
    var saleDraftList : [SaleDraftLists] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var datePicker = UIDatePicker()
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    var searchIsActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var searchPage: Int = 1
    var searchEnable : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addFloaty()
        self.setBarButton()
        self.initialSetup()
        self.showStartDatePicker()
        self.showEndDatePicker()
        self.configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = LanguageManager.SaleDraft
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func initialSetup(){
        self.setUpInitialLanguage()
        
        self.emptyMessageLbl.isHidden = true
        self.dateSearchHeight.constant = 0.0

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarButton(){
        let dateSearchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        dateSearchBtn.setImage(UIImage(named: "dateSearch"), for: UIControl.State.normal)
        dateSearchBtn.addTarget(self, action: #selector(onDateSearch(sender:)), for: UIControl.Event.touchUpInside)
        dateSearchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        dateSearchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        dateSearchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: dateSearchBtn)
        
        navigationItem.setRightBarButtonItems([barButton2], animated: true)
        
    }
    
    @objc func onDateSearch(sender: UIBarButtonItem){
        if searchEnable == false{
            self.dateSearchHeight.constant = 30.0
            self.searchEnable = !searchEnable
        }else{
            self.dateSearchHeight.constant = 0.0
            searchEnable = !searchEnable
        }
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }

}

//Search DatePicker
extension SaleDraftListViewController {
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            
            self.saleDraftList = []
            self.searchPage = 1
            self.searchIsActive = true
            self.presenter.getSaleDraftDateSearchDataFromServer(page: self.searchPage, startDate: startDate, endDate: endDate)
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

//Mark: Floaty Delegate
extension SaleDraftListViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        if let viewSettings = getViewSettings(){
            if viewSettings.salePanel == "FreemiumSale"{
                self.navigateToFreemiumSaleableViewController()
            }else{
                self.navigateToAddNewSaleViewController()
            }
        }
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToFreemiumSaleableViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumSaleableProductsVC") as! FreemiumSaleableProductsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: TableViewDelegate and DataSource
extension SaleDraftListViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SaleDraftNPreOrderCell.nib, forCellReuseIdentifier: SaleDraftNPreOrderCell.identifier)
        self.tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.saleDraftList.count > 0{
            return self.saleDraftList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SaleDraftNPreOrderCell = tableView.dequeueReusableCell(withIdentifier: SaleDraftNPreOrderCell.identifier, for: indexPath) as! SaleDraftNPreOrderCell
        
        if self.saleDraftList.count > 0{
            let draftItem = self.saleDraftList[indexPath.row]
            
            cell.saleDraftLists = draftItem
            
            cell.popUpBtn.tag = draftItem.id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped(sender:)), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.saleDraftList
                .count - 1{
                self.isLoading = true
                if searchIsActive == false{
                    if self.currentPage < self.lastPageNo {
                        self.currentPage += 1
                        self.presenter.getSaleDraftDataFromServer(page: self.currentPage)
                    }
                }else{
                    if self.searchPage < self.lastPageNo {
                        guard let startDate = self.datePickerTextField.text else{
                            return cell
                        }
                        guard let endDate = self.toDatePickerTextField.text else{
                            return cell
                        }
                        self.searchPage += 1
                        self.presenter.getSaleDraftDateSearchDataFromServer(page: self.searchPage, startDate: startDate, endDate: endDate)
                    }
                }
                
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.saleDraftList.count > 0{
            return 85.0
        }
        return 0
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.saleDraftList.count > 0{
            let id = sender.tag
            for draftItem in self.saleDraftList{
                if id == draftItem.id{
                    let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToSaleDraftProductViewController(draftId: id)
                    }
                    
                    let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(updateAction)
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)

    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Delete, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: LanguageManager.Yes, style: .default){
            (action:UIAlertAction!) in
            
            let param : [String: Any] = ["id": deleteId]
            self.presenter.postSaleDraftDeleteToServer(param: param)
        }
        let cancelAction = UIAlertAction(title: LanguageManager.No, style: .default){
            (action:UIAlertAction!) in
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title:  LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.saleDraftList = []
            self.presenter.getSaleDraftDataFromServer(page: self.currentPage)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToSaleDraftProductViewController(draftId: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDraftProductViewController") as! SaleDraftProductViewController
        viewController.draftId = draftId
        self.navigationController?.pushViewController(viewController, animated: true)
    }

}

//Mark: Api Delegate
extension SaleDraftListViewController : SaleDraftViewDelegate{
    func onSaleDraftDelete(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func setSaleDraftData(data: SaleDraftDataMapper) {
        
//        self.summery = data.summary
//        self.collectionView.reloadData()
//        self.setUpAutoScroll()
        
        guard let draftList = data.saleDraftLists, draftList.count > 0 else{
            return
        }
        self.saleDraftList += draftList
        self.isLoading = false
        
        guard let pagination = data.pagination else{
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(message: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.saleDraftList = []
        self.currentPage = 1
        self.presenter.getSaleDraftDataFromServer(page: self.currentPage)
    }
}
