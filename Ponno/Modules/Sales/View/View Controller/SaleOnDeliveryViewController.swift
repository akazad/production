//
//  SaleOnDeliveryViewController.swift
//  Ponno
//
//  Created by a k azad on 2/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

protocol SaleOnDeliveryDelegate {
    func setDeliveryInfo(deliverySystemId: String, newDeliverySystem: String, phone: String, deliveryCharge: Double)
}

class SaleOnDeliveryViewController: UIViewController {

    
    @IBOutlet weak var deliverySystemText: UITextField!{
        didSet{
            deliverySystemText.placeholder = LanguageManager.DeliverySystem
        }
    }
    @IBOutlet weak var deliveryChargeText: UITextField!{
        didSet{
            deliveryChargeText.placeholder = LanguageManager.DeliveryCharge
        }
    }
    @IBOutlet weak var deliveryTokenTextField: UITextField!
    @IBOutlet weak var deliverySubmitBtn: UIButton!
    
    //HeightContstraint
    @IBOutlet weak var newDeliveryHeight: NSLayoutConstraint!
    @IBOutlet weak var phoneHeight: NSLayoutConstraint!
    @IBOutlet weak var newDeliveryViewHeight: NSLayoutConstraint!
    
    private var presenter = SaleOnDeliveryPresenter(service: SalesService())
    
    var deliverySystemList : [DeliverySystems] = [] {
        didSet{
            self.deliverySystemPicker.reloadAllComponents()
        }
    }
    
    var deliverySystemId : Int?
    var deliverySystemName : String?
    var deliverySystemPicker = UIPickerView(){
        didSet{
            self.deliverySystemPicker.selectedRow(inComponent: 0)
        }
    }

    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    var getAll: Bool = true
    
    var delegate : SaleInfoRefreshDelegate?
    
    //
    var deliverySystem : String?
    var deliveryCharge : Double?
    var deliveryToken : String?
    
    var deliveryInstanceOfSaleInfoVC : SaleInfoViewController?
    var deliveryInstanceOfSalePanelProductInfoVC : SalePanelProductInfoViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpPickerView()
        self.initialSetUp()
        self.addFloaty()
        //self.dropDownImageIntoTextField()
        self.attachPresenter()
        //self.setUpToolBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.deliverySystemPicker.reloadAllComponents()
    }
    
    func initialSetUp(){
        //self.newDeliveryHeight.constant = 0.0
        //self.phoneHeight.constant = 0.0
        //self.newDeliveryViewHeight.constant = 0.0
        
        if SaleInfoObject.deliverySystem?.isEmpty == false{
            deliverySystemText.text = SaleInfoObject.deliverySystem
            deliveryChargeText.text = SaleInfoObject.deliveryCharge?.toString()
            deliveryTokenTextField.text = SaleInfoObject.deliveryToken
        }
        
        deliverySystemText.setRightPaddingPoints(32)
        
        self.deliverySubmitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        self.deliveryChargeText.addTarget(self, action: #selector(deliveryChargeTextDidChange), for: .editingChanged)
    }
    
    func dropDownImageIntoTextField(){
//        let imageView = UIImageView()
//        if let size = imageView.image?.size {
//            imageView.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
//        }
//        //imageView.frame = CGRect(x: 0, y: 0, width: 10, height: 16)
//        let dropDownImage = UIImage(named: "dropDown")
//        //imageView.contentMode = .center
//        imageView.image = dropDownImage
        let imageView = UIImageView(image: UIImage(named: "dropDown"))
        //imageView.contentMode = UIView.ContentMode.center
        imageView.frame = CGRect(x: 0.0, y: 0.0, width: imageView.image!.size.width / 25, height: 20)
        deliverySystemText.rightView = imageView
        deliverySystemText.rightViewMode = .always
    }
    
    func addFloaty(){
        let floaty = Floaty()
        floaty.buttonColor = UIColor.lightGreen
        
        let extraItem = FloatyItem()
        extraItem.icon = UIImage(named: "next")
        extraItem.buttonColor = UIColor.lightGreen
        extraItem.title = LanguageManager.DeliverySystemAdd
        extraItem.handler = { item in
            self.alertWithTextField()
        }
        
        floaty.addItem(item: extraItem)
        self.view.addSubview(floaty)
    }
    
    //PickerView
    func setUpPickerView(){
        
        self.deliverySystemPicker.delegate = self
        self.deliverySystemPicker.selectedRow(inComponent: 0)
        
        
        let deliverySystemToolBar = UIToolbar()
        deliverySystemToolBar.barStyle = UIBarStyle.default
        deliverySystemToolBar.isTranslucent = true
        deliverySystemToolBar.tintColor = UIColor.black
        deliverySystemToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneInDeliverySystem(sender:)))
        
        deliverySystemToolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        deliverySystemToolBar.isUserInteractionEnabled = true
        
        self.deliverySystemText.inputView = deliverySystemPicker
        self.deliverySystemText.inputAccessoryView = deliverySystemToolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneInDeliverySystem(sender: UIBarButtonItem){
        guard let deliverySystem = self.deliverySystemText.text, deliverySystem != "" else{ return}
        SaleInfoObject.deliverySystem = deliverySystem
        self.deliverySystemText.resignFirstResponder()
    }
    
    @objc func deliveryChargeTextDidChange(_textfield: UITextField){
//        self.deliveryChargeText.resignFirstResponder()
        
        guard let charge = self.deliveryChargeText.text, charge != "" , let deliveryCharge = Double(charge) else{
            return
        }
        
        
        self.deliveryCharge = deliveryCharge
    }
    
    func setUpToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.deliveryChargeText.inputAccessoryView = toolBar
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        
        

    }
    
    @objc func onSubmit(sender: UIButton){
        if self.isValidated(){
            self.deliveryToken = self.deliveryTokenTextField.text ?? ""
            SaleInfoObject.deliveryCharge = self.deliveryCharge
            SaleInfoObject.deliverySystemId = self.deliverySystemId
            SaleInfoObject.deliverySystem = self.deliverySystem
            SaleInfoObject.deliveryToken = self.deliveryToken
            
            deliveryInstanceOfSaleInfoVC?.deliveryCheckBtn?.setImage( UIImage(named:"check_icon"), for: .normal)
            deliveryInstanceOfSaleInfoVC?.hideDeliveryDetails(is: false)
            deliveryInstanceOfSaleInfoVC?.setDeliveryData()
//            deliveryInstanceOfSalePanelProductInfoVC?
            self.navigationController?.popViewController(animated: true)
            self.delegate?.refreshScreen()
        }
    }
    
    func isValidated()->Bool {
        if self.deliverySystemText.text == "" {
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.DeliverySystemIsRequired)
            return false
        }
        if self.deliveryChargeText.text == "" {
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.DeliveryChargeIsRequired)
            return false
        }
        return true
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popToRootViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: PickerViewDelegate
extension SaleOnDeliveryViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.deliverySystemPicker{
            return deliverySystemList.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.deliverySystemPicker{
            if self.deliverySystemList.count > 0 {

                let list = self.deliverySystemList[row]
                //                if isLoading == false && row == self.deliverySystemList.count - 1 && self.currentPage < self.lastPageNo {
//                    self.isLoading = true
//                    self.currentPage += 1
//                    self.presenter.getDeliverySystemDataFromServer(page: self.currentPage)
//                }
                self.deliverySystemText.text = list.name
                self.deliverySystem = list.name
                self.deliverySystemId = list.id
                return self.deliverySystemList[row].name
            }else{
                return ""
            }
        }
        else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.deliverySystemPicker{
            if self.deliverySystemList.count > 0 {
                let list = self.deliverySystemList[row]
                
                self.deliverySystemText.text = list.name
                self.deliverySystemId = list.id
                self.deliverySystem = list.name
//                let selectedRow = [self.deliverySystemPicker.selectedRow(inComponent: 0)]
//                if selectedRow == [0] {
//                    self.alertWithTextField()
//                }else{
//                    self.deliverySystemText.text = list.name
//                    self.deliverySystemId = list.id
//                }
            }else{
                return
            }
        }
    }
    
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewDeliverySystemAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.DeliverySystem
            textField.textAlignment = .center
        }
                
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.PhoneNumber
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.DeliverySystemIsRequired, message: "")
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                self.addAlert(title: LanguageManager.PhoneNumberIsRequired, message: "")
                return
            }
            
//            if !(self.validatePhoneNumber(value: phone)){
//                self.addAlert(title: LanguageManager.PhoneNumberIsIncorrect, message: "")
//                return
//            }
            
            let param : [String : String] = ["name": textField.text!, "phone": textField2.text!,"description": "", "current_due": ""]
            
            self.presenter.postAddNewDeliverySystemDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}


//Mark: Api Delegate
extension SaleOnDeliveryViewController: SaleOnDeliveryViewDelegate{
    
    func setDeliverySystemData(data: DeliveryDataMapper) {
        guard let list = data.deliverySystems, list.count > 0 else {
            return
        }

//        let deliveryAdd = DeliverySystems(id: 0, name: "নতুন ডেলিভারি পদ্ধতি যোগ", phone: "")
//        list.insert(deliveryAdd, at: 0)
        self.deliverySystemList = list
    }
    
    func addDeliverySystemData(data: DeliverySystemAddDataMapper) {
        guard let deliverySystem = data.deliverySystem else {
            return
        }
        self.deliverySystemId = deliverySystem.id
        self.deliverySystemText.text = deliverySystem.name
        
        let dSystem = DeliverySystems.init(id: deliverySystem.id, name: deliverySystem.name, phone: deliverySystem.phone)
        self.deliverySystemList.append(dSystem)
        //self.deliverySystemList.
        
        //self.deliverySystemPicker.reloadAllComponents()
        
//        guard let message = data.message else {
//            return
//        }
//        self.alertMessage(userMessage: message)
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(message : String) {
        //showAlert(title: "Delivery systems not found", message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getAllDeliverySystemDataFromServer(getAll: self.getAll)
    }
}

extension SaleOnDeliveryViewController{
    //Alert Message
    func alertMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.deliverySystemText.resignFirstResponder()
                self.deliveryChargeText.becomeFirstResponder()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}

extension SaleOnDeliveryViewController {
    func addAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.alertWithTextField()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
