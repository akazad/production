//
//  SalePreOrderedProductUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 14/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class SalePreOrderedProductUpdateViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!{
           didSet{
               titleLbl.text = LanguageManager.ProductInfo
           }
       }
       @IBOutlet weak var quantityLbl: UILabel!{
           didSet{
               quantityLbl.text = LanguageManager.Quantity
           }
       }
       @IBOutlet weak var quantity: UITextField!{
           didSet{
               quantity.placeholder = LanguageManager.Quantity
           }
       }
       @IBOutlet weak var quantitySerialTextField: UITextField!{
           didSet{
               quantitySerialTextField.placeholder = LanguageManager.UseCommaToSeparateSerial
           }
       }
       @IBOutlet weak var quantitySerialTextFieldHeight: NSLayoutConstraint!
       @IBOutlet weak var sellingPriceLbl: UILabel!{
           didSet{
               sellingPriceLbl.text = LanguageManager.SellingPrice
           }
       }
       @IBOutlet weak var sellingPrice: UITextField!{
           didSet{
               sellingPrice.placeholder = LanguageManager.SellingPrice
           }
       }
       @IBOutlet weak var nextBtn: UIButton!
    
    var hasSerial : Int?
    var serails :  [String]?
    var productID : Int?
    var productName : String?
    
    var serialString : String?
    
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedSellingPrices : [Double] = []
    var selectedSerials : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    func initialSetup(){
        self.title = LanguageManager.Update
        self.titleLbl.text = LanguageManager.UpdateInfo
        self.quantitySerialTextField.addTarget(self, action: #selector(onSerialChange), for: .editingChanged)
        
        //self.detailsSetup()
        
//        if let productInfo = self.productInfo {
//            self.quantity.text = productInfo.quantity
//            self.buyingPrice.text = productInfo.buyingPrice
//            self.sellingPrice.text = productInfo.sellingPrice
//            self.productName = productInfo.productName
//            self.productID = productInfo.productId
//            self.id = productInfo.id
//
//
//        }
        if let has_Serial = self.hasSerial {
            if has_Serial == 0 {
                self.quantitySerialTextFieldHeight.constant = 0.0
            }else{
                self.quantitySerialTextFieldHeight.constant = 40.0
            }
        }
    }
    
    func setUpViews(){
            self.nextBtn.addTarget(self, action: #selector(onNext), for: .touchUpInside)
        }
        
        @objc func onSerialChange(sender: UITextField){
            self.refreshQuantityTextField()
        }
        
        func refreshQuantityTextField(){
            guard let serial = self.quantitySerialTextField.text else{
                return
            }
            let serialList = self.prepareSerialNumbers(serials: serial)
            //self.serails = serialList
            self.serialString = serial
            print (serialList)
            let count = serialList.count
            self.quantity.text = "\(count)"
        }
        
    func toolBarSetUp(){
            //QuantityToolBar
            let quantityToolBar = UIToolbar()
            quantityToolBar.barStyle = UIBarStyle.default
            quantityToolBar.isTranslucent = true
            quantityToolBar.tintColor = UIColor.black
            quantityToolBar.sizeToFit()
            
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
            
            let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
            
            quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
            quantityToolBar.isUserInteractionEnabled = true
            
            self.quantity.inputAccessoryView = quantityToolBar
            
            //SellingPriceToolBar
            let sellingPriceToolBar = UIToolbar()
            sellingPriceToolBar.barStyle = UIBarStyle.default
            sellingPriceToolBar.isTranslucent = true
            sellingPriceToolBar.tintColor = UIColor.black
            sellingPriceToolBar.sizeToFit()
            
            let sellingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnSellingPrice(sender:)))
            
            sellingPriceToolBar.setItems([cancelButton, spaceButton, sellingPriceDoneButton], animated: false)
            sellingPriceToolBar.isUserInteractionEnabled = true
            
            self.sellingPrice.inputAccessoryView = sellingPriceToolBar
        }
        
        @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
            self.quantity.resignFirstResponder()
            self.sellingPrice.becomeFirstResponder()
        }
        
        @objc func onPressingDoneOnSellingPrice(sender: UIBarButtonItem){
            self.sellingPrice.resignFirstResponder()
        }
        
        @objc func onPressingCancel(sender: UIBarButtonItem){
            self.view.endEditing(true)
        }
        
        @objc func onNext(sender : UIButton){
            if self.isValidated(){
                guard let quantity = self.quantity.text, let sellingPrice = self.sellingPrice.text, let name = self.productName,let productId = self.productID else{
                    return
                }
                
                    guard let hasSerial = self.hasSerial else{
                        return
                    }
                    
                    let serials = self.quantitySerialTextField.text ?? ""
                    //let product = PreOrderProductData(id: productId, name: name, quantity: quantity, buyingPrice: buyingprice, sellingPrice: sellingPrice, hasSerial: hasSerial)
                    //let product = PreOrderProductData(id: productId, name: name, quantity: quantity, buyingPrice: buyingprice, sellingPrice: sellingPrice, hasSerial: hasSerial, serialArray: serials)
                    //self.updateDelegate?.updateProduct(product: product)
                    
    //                if hasSerial == 1 {
    //                    guard let serials = self.serialString else{
    //                        return
    //                    }
    //                    self.updateSerialsDelegate?.updateSerials(productId: productId, serials: serials)
    //                }
                    self.navigationController?.popViewController(animated: true)
            }
        }

    func isValidated()->Bool{
        if self.quantity.text == ""{
            self.showAlert(title: LanguageManager.QuantityIsRequired, message: "")
            return false
        }else if hasSerial == 1{
            if self.quantity.text == "\(0)"{
                self.showAlert(title: LanguageManager.SerialNumberIsRequired, message: "")
                return false
            }
        }
        else if self.sellingPrice.text == ""{
            self.showAlert(title: LanguageManager.SellingPriceIsRequired, message: "")
            return false
        }
        return true
    }
    
}
