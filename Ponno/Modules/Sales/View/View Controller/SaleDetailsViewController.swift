//
//  SaleDetailsViewController.swift
//  Ponno
//
//  Created by a k azad on 22/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SVProgressHUD

class SaleDetailsViewController: UIViewController {
   
    enum Sections : Int{
        case SaleInfo
        case CustomerInfo
        case Product
        case SaleReturn
        case SaleTotal
        case DeliverySystem
        case DueTransactionsHeader
        case DueTransactions
        case DueHistory
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessage: UILabel!{
        didSet{
            emptyMessage.text = LanguageManager.NoInformationFound
        }
    }
    
    private var presenter = SaleDetailsPresenter(service: SalesService())
    
    var saleDetails : SaleDetails?{
        didSet{
            self.refreshTableView()
        }
    }
    var iD : Int?
    var saleProductList: [SaleProducts]?{
        didSet{
            self.refreshTableView()
        }
    }
    
    var saleReturns : [SaleReturns]?{
        didSet{
            self.refreshTableView()
        }
    }
    
    var dueTransactions : [DueTransactions]?{
        didSet{
            self.refreshTableView()
        }
    }
    
    var refundQuantity : Double?
    var productId : Int?
    var serialNumber : String?
    var invoiceDue : Double?
    var currentDue : Double?
    var customer : String?
    var deliverySystem : String?
    
    var state: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarTitle()
        self.configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.attachPresenter()
    }

    func setNavigationBarTitle(){
        self.title = LanguageManager.SalesInvoice
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
    }
    
    
}

//MARK: TableView Delegate and DataSource
extension SaleDetailsViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SaleDetailsProductListCell.nib, forCellReuseIdentifier: SaleDetailsProductListCell.identifier)
        self.tableView.register(SaleDetailsHeaderCell.nib, forCellReuseIdentifier: SaleDetailsHeaderCell.identifier)
        self.tableView.register(SaleDetailsCustomerTableViewCell.nib, forCellReuseIdentifier: SaleDetailsCustomerTableViewCell.identifier)
        self.tableView.register(SaleDetailsTotalCell.nib, forCellReuseIdentifier: SaleDetailsTotalCell.identifier)
        self.tableView.register(SaleDetailsDeliveryStausCell.nib, forCellReuseIdentifier: SaleDetailsDeliveryStausCell.identifier)
        self.tableView.register(SaleDetailsDueTransactionsHeaderCell.nib, forCellReuseIdentifier: SaleDetailsDueTransactionsHeaderCell.identifier)
        self.tableView.register(SaleDetailsDueTransactionsCell.nib, forCellReuseIdentifier: SaleDetailsDueTransactionsCell.identifier)
        self.tableView.register(SaleDetailsDueHistoryCell.nib, forCellReuseIdentifier: SaleDetailsDueHistoryCell.identifier)
        //self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        if section == Sections.SaleReturn.rawValue{
            guard let list = self.saleReturns, list.count > 0 else{
                return 0.01
            }
            return 30.0
        }
        return 0.01
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//
//        if section == Sections.Product.rawValue{
//            return 40.0
//        }
//        return 0.01
//    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        if section == Sections.Product.rawValue{
//            let header = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.height, height: 40.0))
//
//            guard let data = self.saleDetails else{
//                return UIView()
//            }
//            let label = UILabel(frame: CGRect(x: 8, y: 10, width: header.frame.width - 16, height: 20))
//            label.text = ""
//            if let deliverySystem = data.deliverySystem{
//                label.text = "ডেলিভারি পদ্ধতি: " + deliverySystem
//            }
//            header.addSubview(label)
//            return header
//        }
//        return UIView()
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case Sections.SaleInfo.rawValue:
            return 1
        case Sections.CustomerInfo.rawValue:
            if let customer = self.saleDetails?.customerName {
                if customer.isEmpty{
                    return 0
                }else{
                    return 1
                }
            }
            return 0
        case Sections.Product.rawValue:
            guard let list = self.saleProductList, list.count > 0 else {
                return 0
            }
            return list.count
        case Sections.SaleReturn.rawValue:
            guard let list = self.saleReturns, list.count > 0 else{
                return 0
            }
            return list.count
        case Sections.SaleTotal.rawValue:
            return 1
        case Sections.DeliverySystem.rawValue:
            return 1
        case Sections.DueTransactionsHeader.rawValue:
            return 1
        case Sections.DueHistory.rawValue:
            return 1
        case Sections.DueTransactions.rawValue:
            guard let list = self.dueTransactions, list.count > 0 else{
                return 0
            }
            return list.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case Sections.SaleInfo.rawValue:
            let cell : SaleDetailsHeaderCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsHeaderCell.identifier, for: indexPath) as! SaleDetailsHeaderCell
            cell.selectionStyle = .none
            guard let list = self.saleDetails else{
                return cell
            }
            self.iD = list.id
            cell.invoiceLabel.text = LanguageManager.Invoice + "#" + " : " + list.invoice
            cell.dateLabel.text = LanguageManager.Date + " : " + list.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
            cell.timeLabel.text = LanguageManager.Time + " : " + list.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.h_mm_a.rawValue)
            cell.discountLabel.text = LanguageManager.Discount + " : " + "\(list.discount)" + Constants.currencySymbol
            cell.paymentMethodLabel.text = LanguageManager.PaymentMethod + " : " + list.paymentMethod
            
            let paymentMethod = list.paymentMethod
            if paymentMethod == "Bkash" || paymentMethod == "Rocket"{
                cell.paymentMethodNumberLbl.text = LanguageManager.PaymentNumber + " : " + "\(list.paymentMethodNumber)"
            }else{
                cell.paymentMethodNumberLbl.isHidden = true
            }
            cell.paidLabel.text = LanguageManager.PaidAmount + " : " + "\(list.paid)" + " " + Constants.currencySymbol
            
            let deliverySystemStatus = list.deliverySystem
            if deliverySystemStatus != "" {
                cell.deliveryChargeLabel.text = LanguageManager.DeliveryCharge + " : " + "\(list.deliveryCharge)" + " " + Constants.currencySymbol
                cell.deliverySystemLabel.text = LanguageManager.DeliverySystem + " : " + list.deliverySystem
            }else{
                cell.deliveryChargeLabel.isHidden = true
                cell.deliverySystemLabel.isHidden = true
            }
            
            cell.dueLabel.text = LanguageManager.Due + " : " + "\(list.due)" + " " + Constants.currencySymbol
            
            return cell
            
            
        case Sections.CustomerInfo.rawValue:
            let cell : SaleDetailsCustomerTableViewCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsCustomerTableViewCell.identifier, for: indexPath) as! SaleDetailsCustomerTableViewCell

            cell.selectionStyle = .none
            if let data = self.saleDetails {
                cell.customerNameLabel.text = LanguageManager.CustomerName + " : " + data.customerName
                cell.customerPhoneLabel.text = LanguageManager.MobileNumber + " : " + data.customerPhone
                cell.customerAddressLabel.text = LanguageManager.Address + " : " + " " + data.customerAddress
            }
            
            return cell
            
        case Sections.Product.rawValue:
            let cell: SaleDetailsProductListCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsProductListCell.identifier, for: indexPath) as! SaleDetailsProductListCell
            cell.selectionStyle = .none
            guard let list = self.saleProductList, list.count > 0 else{
                return cell
            }
            let item = list[indexPath.row]

            cell.NameLabel.text = item.name
            cell.quantityLabel.text = LanguageManager.Quantity + " : " + "\(item.quantity)"
            cell.perUnitPriceLabel.text = LanguageManager.PricePerUnit + " : " + "\(item.unitPrice)"
            let total = item.totalPrice.toDouble()?.rounded(toPlaces: 2)
            if let totalPrice = total{
                print(totalPrice)
                cell.totalPriceLabel.text = Constants.currencySymbol + " " +  "\(totalPrice)"
            }
            
            if let category = getBusinessCategory(){
            if category.businessCategory == 1{
                    cell.popUpBtn.isHidden = true
                }else{
                    cell.popUpBtn.tag = item.saleProductId
                    cell.popUpBtn.addTarget(self, action: #selector(onPopBtnTapped), for: .touchUpInside)
                }
            }
            return cell
                        
        case Sections.SaleTotal.rawValue:
            let cell: SaleDetailsTotalCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsTotalCell.identifier, for: indexPath) as! SaleDetailsTotalCell
            
            cell.selectionStyle = .none
            
            guard let total = self.saleDetails?.total else {
                return cell
            }
            
            cell.totalLbl.text = LanguageManager.Total + " : " + total + Constants.currencySymbol
            
            return cell
            
        case Sections.DeliverySystem.rawValue:
            let cell : SaleDetailsDeliveryStausCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsDeliveryStausCell.identifier, for: indexPath) as! SaleDetailsDeliveryStausCell
            
            if let details = self.saleDetails{
                let deliverySystem = details.deliverySystem
                if (deliverySystem.isEmpty){
                    cell.isHidden = true
                }else{
                    if let due = Double(details.due), let deliveryCharge = Double(details.deliveryCharge){
                        let deliverySystemDue = (due - deliveryCharge)
                        cell.deliveryStatusLbl.text = LanguageManager.DeliveryStatus + " : "
                        if let dueTransaction = self.dueTransactions?.count, dueTransaction != 0 {
                            cell.deliveryStatus.text = LanguageManager.Delivered
                            cell.deliveryStatus.textColor = UIColor(red: 59, green: 149, blue: 31)
                        }else{
                            cell.deliveryStatus.text = LanguageManager.Pending
                            cell.deliveryStatus.textColor = UIColor.red
                        }
                        cell.deliverySystemDueLbl.text = LanguageManager.DeliverySystemDue + " : "
                        cell.deliverySystemDueAmountLbl.text = "\(deliverySystemDue)"
                    }
                }
            }
            
            return cell
            
        case Sections.DueHistory.rawValue:
            let cell: SaleDetailsDueHistoryCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsDueHistoryCell.identifier, for: indexPath) as! SaleDetailsDueHistoryCell
            
            if let customer = self.customer, let deliverySystem = self.deliverySystem {
                
                if deliverySystem.isEmpty == true{
                    if customer.isEmpty == true{
                        cell.isHidden = true
                    }else{
                        if let currentDue = self.currentDue, let invoiceDue = self.invoiceDue{
                            
                            cell.invoiceLbl.text = LanguageManager.InvoiceDue + " : "
                            cell.invoice.text = "\(invoiceDue)" + Constants.currencySymbol
                            
                            cell.latestDueLbl.text = LanguageManager.LatestDue + " : "
                            cell.latestDue.text = "\(currentDue)" + Constants.currencySymbol
                            
                            var othersDue : Double = 0.0
                            
                            if let data = self.dueTransactions, data.count > 0 {
                                if let list = self.dueTransactions, list.count > 0{
                                    var duePaid : Double = 0.00
                                    for item in list{
                                        let paid = Double(item.paid) ?? 0.00
                                        duePaid += paid
                                    }
                                    cell.duePaidLbl.text = LanguageManager.DuePaid + " : "
                                    cell.duePaid.text = "\(duePaid)" + Constants.currencySymbol
                                    
                                    let remainigDue : Double = invoiceDue - duePaid
                                    
                                    cell.remainingDueLbl.text = LanguageManager.RemainingDue + " : "
                                    cell.remainingDue.text = "\(remainigDue)" + Constants.currencySymbol
                                    
                                    if currentDue > remainigDue{
                                        othersDue = currentDue - remainigDue
                                    }else{
                                        othersDue = remainigDue - currentDue
                                    }
                                    
                                    cell.othersDueLbl.text = LanguageManager.OthersDue + " : "
                                    cell.othersDue.text = "\(othersDue)" + Constants.currencySymbol
                                }
                            }else{
                                let remainigDue : Double = invoiceDue
                                
                                cell.remainingDueLbl.text = LanguageManager.RemainingDue + " : "
                                cell.remainingDue.text = "\(remainigDue)" + Constants.currencySymbol
                                
                                
                                if currentDue > remainigDue{
                                    othersDue = currentDue - remainigDue
                                }else{
                                    othersDue = remainigDue - currentDue
                                }
                                
                                cell.othersDueLbl.text = LanguageManager.OthersDue + " : "
                                cell.othersDue.text = "\(othersDue)" + Constants.currencySymbol
                                cell.duePaidLbl.isHidden = true
                                cell.duePaid.isHidden = true
                                cell.remainingDueLbl.isHidden = true
                                cell.remainingDue.isHidden = true
                            }
                        }
                    }
                }else{
                    cell.isHidden = true
                }
            }
            return cell
            
        case Sections.SaleReturn.rawValue:
            let cell: SaleDetailsProductListCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsProductListCell.identifier, for: indexPath) as! SaleDetailsProductListCell
            cell.selectionStyle = .none
            guard let list = self.saleReturns, list.count > 0 else{
                return cell
            }
            let item = list[indexPath.row]
            cell.NameLabel.text = item.name
            cell.quantityLabel.text = LanguageManager.Quantity + ": " + "\(item.quantity)"
            cell.perUnitPriceLabel.text = LanguageManager.PricePerUnit + ": " +  "\(item.unitPrice)"
            
            let total = item.totalPrice.toDouble()?.rounded(toPlaces: 2)
            if let totalPrice = total{
                print(totalPrice)
                cell.totalPriceLabel.text = Constants.currencySymbol + " " +  "\(totalPrice)"
            }
            
            cell.popUpBtn.isHidden = true
            return cell
        
        case Sections.DueTransactionsHeader.rawValue:
            let cell: SaleDetailsDueTransactionsHeaderCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsDueTransactionsHeaderCell.identifier, for: indexPath) as! SaleDetailsDueTransactionsHeaderCell
            cell.selectionStyle = .none
            
            if let deliverySystem = self.deliverySystem, deliverySystem.isEmpty == true{
                if self.dueTransactions?.count ?? 0 < 1{
                    cell.isHidden = true
                }else{
                    cell.dateLbl.text = LanguageManager.Data
                    cell.paidAmountLbl.text = LanguageManager.PaidAmount
                }
            }else{
                cell.isHidden = true
            }
            
            return cell
            
        case Sections.DueTransactions.rawValue:
            let cell : SaleDetailsDueTransactionsCell = tableView.dequeueReusableCell(withIdentifier: SaleDetailsDueTransactionsCell.identifier, for: indexPath) as! SaleDetailsDueTransactionsCell
            cell.selectionStyle = .none
            
            if let deliverySystem = self.deliverySystem, deliverySystem.isEmpty == true{
                if self.dueTransactions?.count ?? 0 > 0{
                    if let item = self.dueTransactions,  item.count > 0{
                        let list = item[indexPath.row]
                        let customer = self.customer ?? nil
                        
                        if customer?.isEmpty == true{
                            cell.isHidden = true
                        }else{
                            cell.dateDetailsLbl.text = list.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_dd_yyyy.rawValue)
                            cell.paidAmount.text = list.paid + Constants.currencySymbol
                        }
                    }
                    
                }
            }else{
                cell.isHidden = true
            }
            return cell
        default: 
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.SaleInfo.rawValue:
                return 275.0
        case Sections.CustomerInfo.rawValue:
            return 95.0
        case Sections.Product.rawValue:
            return 87.0
        case Sections.SaleReturn.rawValue:
            if let list = self.saleReturns, list.count > 0{
                return 87.0
            }else{
                return 0.01
            }
            
        case Sections.SaleTotal.rawValue:
            return 44.0
        case Sections.DeliverySystem.rawValue:
            if self.deliverySystem?.isEmpty == false{
               return 90.0
            }else{
                return 0.01
            }
            
        case Sections.DueTransactionsHeader.rawValue:
            return 24.0
        case Sections.DueTransactions.rawValue:
            return 32.0
        case Sections.DueHistory.rawValue:
            return 195.0
        default:
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == Sections.SaleReturn.rawValue{
            guard let ret = self.saleReturns, ret.count > 0 else{
                return UIView()
            }
            let header = UIView(frame: CGRect(x: 0, y:0, width: self.tableView.frame.width, height: 20))
            let label = UILabel(frame: CGRect(x: 10, y: 5, width: header.frame.width, height: 20))

            label.text = LanguageManager.Return

            label.textAlignment = .left
            label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
            header.addSubview(label)
            return header
        }
        return UIView()
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if section == Sections.SaleReturn.rawValue{
//            let footer = UIView(frame: CGRect(x: -8, y: 2, width: self.tableView.frame.width, height: 40))
//            let label = UILabel(frame: CGRect(x: 120, y: 0, width: footer.frame.width, height: 20))
//            guard let data = self.saleDetails else{
//                return UIView()
//            }
//
//            label.text = "Total" + " : " +  "\(data.total)" + " " + Constants.currencySymbol
//
//            label.textAlignment = .center
//            label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
//            footer.addSubview(label)
//            return footer
//        }
//        return UIView()
//    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if section == Sections.SaleReturn.rawValue{
//            return 40.0
//        }
//        return 0.1
//    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        switch indexPath.section {
        case Sections.CustomerInfo.rawValue:
            return false
        case Sections.Product.rawValue:
            return false
        case Sections.SaleInfo.rawValue:
            return false
        case Sections.SaleReturn.rawValue:
            return false
        case Sections.SaleTotal.rawValue:
            return false
        case Sections.DeliverySystem.rawValue:
            return false
        case Sections.DueTransactionsHeader.rawValue:
            return false
        case Sections.DueTransactions.rawValue:
            return false
        case Sections.DueHistory.rawValue:
            return false
            
        default:
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        switch indexPath.section {
        case Sections.CustomerInfo.rawValue:
            return []
        
        case Sections.SaleInfo.rawValue:
            return []
          
        case Sections.Product.rawValue:
            let edit = UITableViewRowAction(style: .normal, title: "Product Return") { action, index in
                
                guard let list = self.saleProductList, list.count > 0 else{
                    return
                }
                let item = list[indexPath.row]
                self.productId = item.productId
                self.serialNumber = item.serialNumber
                self.refundQuantity = Double(item.quantity)
                //print(self.returnQuantity)
                self.productQuantityCheck()
                self.navigateToInvoiceReturnVC(returnProduct: item)
                //            print(item)
            }
            
            edit.backgroundColor = UIColor.orange
            
            return [edit]
           
        case Sections.SaleReturn.rawValue:
            return []
            
        default:
            return []
            
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        guard let list = self.saleProductList, list.count > 0 else{
            return
        }
        if list.count > 0{
            self.productId = sender.tag
            for product in list{
                if self.productId == product.saleProductId{
                    self.productId = product.productId
                    self.serialNumber = product.serialNumber
                    self.refundQuantity = Double(product.quantity)
                    self.productQuantityCheck()
                    let editAction = UIAlertAction(title: LanguageManager.ProductReturn, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToInvoiceReturnVC(returnProduct: product)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(editAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToInvoiceReturnVC(returnProduct: SaleProducts){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "InvoiceReturnViewController") as! InvoiceReturnViewController
        viewController.returnProduct = returnProduct
        viewController.productId = self.productId
        viewController.saleDetails = self.saleDetails
        viewController.saleId = self.iD
        if self.serialNumber == nil {
            viewController.returnQunatity = self.refundQuantity
        }else{
            viewController.serialNo = self.serialNumber
        }
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func productQuantityCheck(){
        guard let productList = self.saleReturns, productList.count > 0 else{
            return
        }
        for product in productList{
            let returnProductId = product.productId
            if self.serialNumber == "" {
                if self.productId == returnProductId {
                    //                if let serialNo = product.
                    let quan = Double(product.quantity)
                    guard let backQunatity = self.refundQuantity, let quantity = quan else{
                        return
                    }
                    self.refundQuantity = backQunatity - quantity
                    // print(returnQuantity)
                }
            }
        }
    }
    
}

// MARK: API Delegate
extension SaleDetailsViewController : SaleDetailsViewDelegate{
    func setSaleDetailsData(data: SaleDetailsDataMapper) {

        guard let saleDetail = data.saleDetails else{
            self.emptyMessage.isHidden = false
            return
        }
        self.emptyMessage.isHidden = true
        self.saleDetails = saleDetail
        self.invoiceDue = Double(saleDetail.due)
        self.currentDue = Double(saleDetail.currentDue)
        self.customer = saleDetail.customerName
        self.deliverySystem = saleDetail.deliverySystem
        
        self.tableView.reloadData()
        
        guard let saleProduct = data.saleProducts, saleProduct.count > 0 else{
            return
        }
        self.saleProductList = saleProduct
        
        
        
        if let dueTransactions = data.dueTransactions, dueTransactions.count > 0 {
            self.dueTransactions = dueTransactions
            self.tableView.reloadData()
        }
        
        
        if let saleReturn = data.saleReturns, saleReturn.count > 0 {
            self.saleReturns = saleReturn
            self.tableView.reloadData()
        }
        
        
        
        
    }
    
    func onFailed(message: String) {
        self.showAlert(title: message, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.emptyMessage.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.emptyMessage.isHidden = false
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.iD else {
            return
        }
        print(id)
        self.presenter.getSaleDetailsData(id: id)  
    }
}
extension String {
    func convertDateFormater(inputDateFormat : String, outputDateFormat : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputDateFormat // "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = outputDateFormat // "dd MMM, yy"
        return  dateFormatter.string(from: date!)
    }

////
////    let roundedValue1 = formatter.string(from: 0.6844)
//    func convertNumberFormatter(from: String) -> String{
//        let formatter = NumberFormatter()
//        formatter.numberStyle = NumberFormatter.Style.decimal
//        formatter.roundingMode = NumberFormatter.RoundingMode.halfUp
//        formatter.maximumFractionDigits = 2
//        return formatter
//    }

}

enum DateFormats : String{
    case yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss"
    case dd_MMM_yy_c_HH_mm_a = "dd MMM, yy || hh:mm a"
    case dd_MMM_yy = "dd MMM, yy"
    case HH_mm_ss = "HH:mm:ss"
    case h_mm_a = "h:mm a" //MARK: AM/ PM
    case yyyy_MM_dd = "yyyy-MM-dd"
    case MMM_d_h_mm_a = "MMM d, h:mm a."
    case MMM_d_yy_h_mm_a = "MMM d yy, h:mm a"
    case yyyy_MM_dd_c_HH_mm_ss = "yyyy-MM-dd || HH:mm:ss"
    case MMM_dd_yyyy = "MMM dd, yyyy"
    
}

public extension UITableView {
    
    /// This method returns the indexPath of the cell that contains the specified view
    /// - parameter view: The view to find.
    /// - returns: The indexPath of the cell containing the view, or nil if it can't be found
    
    func indexPathForView(_ view: UIView) -> IndexPath? {
        let origin = view.bounds.origin
        let viewOrigin = self.convert(origin, from: view)
        let indexPath = self.indexPathForRow(at: viewOrigin)
        return indexPath
    }
}
