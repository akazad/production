//
//  SalePreOrderProductEditViewController.swift
//  Ponno
//
//  Created by a k azad on 14/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class SalePreOrderProductEditViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var nextView: UIView!{
        didSet{
            self.setUpCardView(uiview: nextView)
        }
    }
    
    fileprivate var presenter = SalePreOrderEditProductPresenter(service: SalesService())
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var editedProducts : [SalePreOrderEditProducts] = []{
        didSet{
            if self.editedProducts.count > 0{
                self.editedProducts = self.editedProducts.sorted(by: { $0.soldQuantity > $1.soldQuantity})
            }
            self.refreshTableView()
        }
    }
    
    var editableProducts : [SalePreOrderEditProducts] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var editedProductList : [SalePreOrderEditProducts] = []
    
    var filteredList : [SalePreOrderEditProducts] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var preOrderId: Int?
    var salePreOrderEditInfo : SalePreOrderEditInfo?
    
    var searchText = ""
    var timer : Timer?
    
    var selectedProduct : SalePreOrderEditProducts?
    var selectedProductName : String?
    var sellingPrice : Double?
    var saleableQuantity : Double?
    
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedSellingPrices : [Double] = []
    var selectedSerials : [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpViews()
        self.setUpSearchBar()
        self.configureTableView()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setUpInitialLanguage()
        self.setNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.resignFirstResponder()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext(sender:)), for: .touchUpInside)
        
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    @objc func onNext(sender : UIButton){
        if self.selectedProductId.count > 0{
            self.navigateToSalePreOrderInfoViewController()
        }
    }
    
    func navigateToSalePreOrderInfoViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalePreOrderInfoViewController") as! SalePreOrderInfoViewController
        
        viewController.selectedProductId = self.selectedProductId
        viewController.selectedAmounts = selectedAmounts
        viewController.selectedSellingPrices = selectedSellingPrices
        
        viewController.preOrderId = self.preOrderId
        viewController.salePreOrderEditInfo = self.salePreOrderEditInfo
        viewController.editedProducts = self.editedProducts
        //viewController.editableProducts = self.editableProducts
        viewController.selectedSerials = self.selectedSerials
         self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//MARK: SearchBar Delegate
extension SalePreOrderProductEditViewController: UISearchBarDelegate{
    fileprivate func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.ProductSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if self.editableProducts.count > 0 {
            if searchText == "" {
                self.isSearchActive = false
                return
            }else{
                self.isSearchActive = true
            }
        }
        
        guard let textToSearch = searchBar.text else {
            return
        }
        
        if textToSearch.count > 2{
            filteredList = self.editableProducts.filter { (product : SalePreOrderEditProducts) -> Bool in
                return product.name.lowercased().contains(textToSearch.lowercased())  || product.categoryName.lowercased().contains(textToSearch.lowercased()) || product.variant.lowercased().contains(textToSearch.lowercased())
            }
        }
    }
}

//Mark: TableView Delegate and DataSource
extension SalePreOrderProductEditViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SaleProductCell.nib, forCellReuseIdentifier: SaleProductCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if isSearchActive == true{
                return 0
            }else{
                if self.editedProducts.count > 0 {
                    return self.editedProducts.count
                }
                return 0
            }
            
        case 1:
            if isSearchActive{
                guard self.filteredList.count > 0 else {
                    return 0
                }
                return self.filteredList.count
            }else{
                if self.editableProducts.count > 0 {
                    return self.editableProducts.count
                }
                return 0
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SaleProductCell.identifier, for: indexPath) as! SaleProductCell
                
        switch indexPath.section {
            
        case 0:
            if self.editedProducts.count > 0 {
                let product = self.editedProducts[indexPath.row]
                
                cell.preOrderEditedProducts = product
                self.saleableQuantity = product.quantity
                
                cell.productId = product.productId
                                
                cell.selectBtn.tag = product.productId
                cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
            }
            
        case 1:
            if isSearchActive{
                if self.filteredList.count > 0{
                    let product = self.filteredList[indexPath.row]
                    
                    cell.productId = product.productId
                    cell.preOrderEditableProduct = product
                }
            }else{
                if self.editableProducts.count > 0 {
                    let product = self.editableProducts[indexPath.row]
                    
                    cell.productId = product.productId
                    cell.preOrderEditableProduct = product
                }
            }
        default:
            return cell
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            if self.editedProducts.count > 0 {
                let data = self.editedProducts[indexPath.row]
                
                self.didSelectOnEditedProduct(data: data)
            }
        case 1:
            if isSearchActive{
                if self.filteredList.count > 0{
                    let data = self.filteredList[indexPath.row]
                    
                    self.didSelectOnEditableProducts(data: data)
                }
            }else{
                if self.editableProducts.count > 0 {
                    let data = self.editableProducts[indexPath.row]
                    
                    self.didSelectOnEditableProducts(data: data)
                }
            }
        default:
            return
        }
        
    }
    
    func didSelectOnEditedProduct(data: SalePreOrderEditProducts){
        self.sellingPrice = Double(data.soldSellingPrice)
        self.saleableQuantity = Double(data.soldQuantity)
        
        for item in self.editedProducts{
            if item.productId == data.productId{
                self.selectedProduct = item
            }
        }
        
        self.navigateToSaleAddVC()
//        let quantity = data.quantity
//        if quantity > 0{
//
//        }
//        else{
//            self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
//        }
        
//        if data.hasSerial == 0{
//            self.navigateToSaleAddVC()
//        }
//        else{
//            navigateToSaleAddSerialVC(id: data.productId, serials: data.soldSerialNo)
//        }
    }
    
    func didSelectOnEditableProducts(data: SalePreOrderEditProducts){
        self.selectedProductName = data.name
        self.selectedProduct = data
        self.saleableQuantity = 1.0
        self.sellingPrice = Double(data.sellingPrice)
        self.navigateToSaleAddVC()
//        let quantity = data.quantity
//        if quantity > 0 {
//            if data.hasSerial == 0{
//
//            }
//            else{
//                self.navigateToSaleAddWithSerialVC(serials: data.serials)
//            }
//        }else{
//            self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
//        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onRemove(sender : UIButton){
        let productId = sender.tag
        
        if self.selectedProductId.contains(productId){
            for item in self.editedProducts{
                if item.productId == productId{
                    let soldQuantity = item.soldQuantity
                    removeDataFromEditedProducts(productId: productId, quantity: soldQuantity)
                }
            }
        }
    }
    
    func removeDataFromEditedProducts(productId : Int, quantity: Double){
        self.editedProducts = self.editedProducts.filter{ $0.productId != productId}
        self.addDataToEditableProductList(productId: productId, quantity: quantity)
        self.refreshTableView()
    }
    
    func removeDataofProductObject(productId : Int){
        var productAtIndex = -1
        if let index = self.selectedProductId.firstIndex(of: productId){
            productAtIndex = index
        }
        
        self.selectedSellingPrices.remove(at: productAtIndex)
        self.selectedAmounts.remove(at: productAtIndex)
        self.selectedSerials.remove(at: productAtIndex)
        self.selectedProductId.remove(at: productAtIndex)
    }
    
    func addDataToEditableProductList(productId: Int, quantity: Double){
        if self.editedProductList.count > 0{
            for item in self.editedProductList{
                if item.productId == productId{
                    let inventoryId = item.inventoryId
                    let productId = productId
                    let soldQuan = quantity
                    let updatedQuantity: Double = item.quantity + soldQuan
                    
//                    if let quantity = quan, let soldQuantity = soldQuan{
//                        updatedQuantity = "\(quantity + soldQuantity)"
//                    }
                    let sellingPrice = item.sellingPrice
                    let serials = item.serials
                    let name = item.name
                    let image = item.image
                    let categoryName = item.categoryName
                    let variant = item.variant
                    let unit = item.unit
                    let categoryId = item.categoryId
                    
                    let editableProduct = SalePreOrderEditProducts(inventoryId: inventoryId, productId: productId, quantity: updatedQuantity, sellingPrice: sellingPrice, serials: serials, name: name, image: image, categoryName: categoryName, categoryId: categoryId, variant: variant, unitName: unit)
                    //let editableProduct = EditedProducts(inventoryId: inventoryId, productId: productId, quantity: updatedQuantity, sellingPrice: sellingPrice, serials: serials, name: name, image: image, categoryName: categoryName, categoryId: categoryId, variant: variant, unitName: unit)
                    
                    self.editableProducts.append(editableProduct)
                    self.editableProducts = self.editableProducts.sorted(by: { $0.name < $1.name })
                }
            }
        }
    }
    
    func addDataToEditedProducts(productId: Int, soldQuantity: Double, soldSellingPrice: Double){
        if let product = findProductInfo(productId: productId){
            let selectedProduct = SalePreOrderEditProducts(inventoryId: product.inventoryId, productId: productId, quantity: product.quantity, sellingPrice: product.sellingPrice, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
            //let selectedProduct = EditedProducts(inventoryId: product.inventoryId, productId: productId, quantity: product.quantity, sellingPrice: product.sellingPrice, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, soldSerials: [""], name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
            for item in self.editedProducts{
                if item.productId == productId{
                    self.editedProducts = self.editedProducts.filter{($0.productId != productId)}
                }
            }
            self.editedProducts.append(selectedProduct)
            self.refreshTableView()
        }
    }
    
//    func addSerialDataToSaleProducts(productId: Int, soldQuantity: String, soldSerials: [String], soldSellingPrice: String){
//            if let product = findProductInfo(productId: productId){
//                let selectedProduct = EditedProducts(inventoryId: product.inventoryId,productId: productId, quantity: product.quantity, sellingPrice: product.sellingPrice, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, soldSerials: [""], name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
//                for item in self.editedProducts{
//                    if item.productId == productId{
//                        self.editedProducts = self.editedProducts.filter{($0.productId != productId)}
//                    }
//                }
//                self.editedProducts.append(selectedProduct)
//                self.refreshTableView()
//            }
//        }
    
    func navigateToSaleAddVC(){
        let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSecondVC") as! SaleAddSecondVC
        
        viewController.delegate = self
        viewController.saleAblePrice = self.sellingPrice
        viewController.saleAbleQuantity = self.saleableQuantity
        viewController.selectedProductName = self.selectedProductName
        viewController.isPreOrderEnable = true
        //viewController.draftProduct = self.selectedProduct
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
//    func navigateToSaleAddSerialVC(id: Int,serials : [String]){
//        let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSerialViewController") as! SaleAddSerialViewController
//        if self.editedProducts.count > 0{
//
//            //let serialList = self.prepareSerialNumbers(serials: serials)
//            viewController.saleAblePrice = self.sellingPrice
//            viewController.selectedProductName = self.selectedProductName
//            //viewController.serialList = serialList
//            viewController.delegate = self
//            viewController.productSerials = serials
//            viewController.productId = id
//            //viewController.saleId = self.saleId
//        }
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
    
//    func navigateToSaleAddWithSerialVC(serials : String){
//        if self.prepareSerialNumbers(serials: serials).count > 0{
//            let serialList = self.prepareSerialNumbers(serials: serials)
//
//            let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSerialViewController") as! SaleAddSerialViewController
//
//            viewController.serialList = serialList
//            viewController.saleAblePrice = self.sellingPrice
//            viewController.delegate = self
//            viewController.selectedProductName = self.selectedProductName
//            viewController.isCodeScannerActive = false
//            //            if codeScannerActive == true{
//            //                viewController.isCodeScannerActive = true
//            //                viewController.searchSerial = self.searchText
//            //            }else{
//            //                viewController.isCodeScannerActive = false
//            //            }
//
//            //viewController.isCodeScannerActive = self.codeScannerActive
//
//            self.navigationController?.pushViewController(viewController, animated: true)
//        }
//
//    }
    
    func removeDataFromSaleableProductList(productId: Int){
        if let product = findProductInfo(productId: productId){
            self.editableProducts = self.editableProducts.filter{($0.productId != product.productId)}
            self.refreshTableView()
        }
    }
        
    func findProductInfo(productId: Int) -> SalePreOrderEditProducts?{
        var productItem : SalePreOrderEditProducts?
        for item in self.editedProductList{
            if item.productId == productId{
                productItem = item
            }
        }
        return productItem
    }
    
}


//Product Delegate
extension SalePreOrderProductEditViewController : SalesAddProductDelegate{
    func setSerialData(serial: [String], sellingPrice: Double) {
//        guard let selectedProduct = self.selectedProduct else{
//            return
//        }
//        if self.selectedProductId.contains(selectedProduct.productId){
//            self.removeDataofProductObject(productId: selectedProduct.productId)
//        }
//        self.selectedProductId.append(selectedProduct.productId)
//        self.selectedSerials.append(serial.joined(separator: ","))
//        self.selectedSellingPrices.append(sellingPrice)
//        self.selectedAmounts.append(Double(serial.count))
//
//        self.addSerialDataToSaleProducts(productId: selectedProduct.productId, soldQuantity: "\(serial.count)", soldSerials: serial, soldSellingPrice: (sellingPrice.toString()))
//        self.removeDataFromSaleableProductList(productId: selectedProduct.productId)
//
//        self.setupDiscardBtn(products: self.editedProducts)
    }
    
    func setProductData(amount: Double, sellingPrice: Double) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        
        if self.selectedProductId.contains(selectedProduct.productId){
            self.removeDataofProductObject(productId: selectedProduct.productId)
        }
        
        self.selectedProductId.append(selectedProduct.productId)
        self.selectedSellingPrices.append(sellingPrice)
        self.selectedAmounts.append(amount)
        self.selectedSerials.append("")
        
        self.addDataToEditedProducts(productId: selectedProduct.productId, soldQuantity: amount, soldSellingPrice: sellingPrice)
        self.removeDataFromSaleableProductList(productId: selectedProduct.productId)
        self.resetSearchBar()
    }
    
    func resetSearchBar(){
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
}

//Api Delegate
extension SalePreOrderProductEditViewController : SalePreOrderEditProductViewDelegate{
    func setSalePreOrderEditData(data: SalePreOrderEditDataMapper) {
        guard let products = data.salePreOrderEditProducts, products.count > 0 else{
            return
        }
        self.editedProductList = products
        
        for item in products{
            if item.soldQuantity != 0{
                self.selectedProductId.append(item.productId)
                self.selectedSellingPrices.append(item.soldSellingPrice)
                self.selectedAmounts.append(item.soldQuantity)
                if item.hasSerial != 0{
                    self.selectedSerials.append(item.soldSerialNo.joined(separator: ","))
                }else{
                    self.selectedSerials.append("")
                }
            }
        }
        
        self.editedProducts = products.filter{selectedProductId.contains($0.productId)}
        self.editableProducts = products.filter{ !selectedProductId.contains($0.productId)}
        
        self.salePreOrderEditInfo = data.salePreOrderEditInfo
    }
    
    func onFailed(message: String) {
         self.showAlert(title: message, message: "")
     }
     
     func showLoading() {
         
         self.showLoader()
     }
     
     func hideLoading() {
         self.hideLoader()
     }
     
     func attachPresenter(){
         self.presenter.attachView(viewDelegate: self)
         if let id = self.preOrderId {
             self.presenter.getSalePreOrderEditDataFromServer(id: id)
         }
         
     }
    
    
}

extension SalePreOrderProductEditViewController{
    func displayDraftMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.popToSaleDraftListViewController()
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSaleDraftListViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is SaleDraftListViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func displayDraftDeleteMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title:  LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
//    func navigateToSaleDraftListViewController(){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDraftListViewController") as! SaleDraftListViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
    
}

