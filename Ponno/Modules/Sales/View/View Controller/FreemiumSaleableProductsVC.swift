//
//  FreemiumSaleableProductsVC.swift
//  Ponno
//
//  Created by a k azad on 20/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import CoreData

protocol FreemiumProductSaleDelegate : NSObjectProtocol{
    func setProductData(amount : Double, sellingPrice : Double)
}

class FreemiumSaleableProductsVC: UIViewController {
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!{
        didSet{
            self.setUpCollectionCardView(uiCollectionView: categoryCollectionView)
        }
    }
    @IBOutlet weak var productCollectionView: UICollectionView!{
        didSet{
            self.setUpCollectionCardView(uiCollectionView: productCollectionView)
        }
    }
    @IBOutlet weak var emptyMessage: UILabel!{
        didSet{
            emptyMessage.isHidden = true
            emptyMessage.text = LanguageManager.NoInformationFound
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    
    fileprivate var presenter = FreemiumSaleableProductsPresenter(service: ProductService())
    
    var categories : [ProductCategories] = []{
        didSet{
            self.categoryCollectionView.reloadData()
        }
    }
    
    var freemiumProducts : [FreemiumProducts] = []{
        didSet{
            self.productCollectionView.reloadData()
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    
    var isSearchActive = false
//    {
//        didSet{
//            self.productCollectionView.reloadData()
//        }
//    }
    var searchText = ""
    
    var allCategory = ProductCategories(id: -1, name: "All", gen: 0, parent: 0)
    
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedSellingPrices : [Double] = []
    
    var selectedProduct : FreemiumProducts?
    var selectedProductName : String?
    var sellingPrice : Double?
    
    var crossImage = UIImage(named: "error_red.png")
    
//    lazy var collectionViewFlowLayout : CustomCollectionViewFlowLayout = {
//        let layout = CustomCollectionViewFlowLayout(display: .list, containerWidth: self.view.bounds.width)
//        return layout
//    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.setUpSearchBar()
        self.configureCollectionView()
        self.setUpInitialLanguage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.setNavigationBar()
        self.attachPresenter()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        selectFirstIndexOfCategory()
        //self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
        
    }
    
    func initialSetup(){
        //self.searchBar.endEditing(true  )
        self.fetchData(searchText: "")
        self.nextBtn.addTarget(self, action: #selector(onNext(sender:)), for: .touchUpInside)
    }
    
    @objc func onNext(sender: UIButton){
        guard let product = self.selectedProduct else {
            return
        }
        if self.selectedProductId.contains(Int(product.productId)){
            self.navigateToSaleInfoViewController()
        }
    }
    
    func navigateToSaleInfoViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumSaleInfoVC") as! FreemiumSaleInfoVC
        
        viewController.selectedProductId = self.selectedProductId
        viewController.selectedAmounts = selectedAmounts
        viewController.selectedSellingPrices = selectedSellingPrices
        
         self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Search Delegate
extension FreemiumSaleableProductsVC: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.ProductSearch
        self.navigationItem.titleView = searchBar
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //self.view.endEditing(true)
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        if self.searchText.count > 2 || self.searchText.count == 0{
            timer?.invalidate()
            
            timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
        }
        
        
    }
    
    @objc func onSearch(){
        self.isSearchActive = true
        selectFirstIndexOfCategory()
        //print("HolaSearch")
        fetchData(searchText: self.searchText.lowercased())
        
    }
    
    func selectFirstIndexOfCategory(){
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        categoryCollectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: .left)
    }
}

extension FreemiumSaleableProductsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func configureCollectionView(){
        self.categoryCollectionView.delegate = self
        self.categoryCollectionView.dataSource = self
        self.productCollectionView.delegate = self
        self.productCollectionView.dataSource = self
        self.categoryCollectionView.register(FreemiumCategoryCell.nib, forCellWithReuseIdentifier: FreemiumCategoryCell.identifier)
        self.productCollectionView.register(FreemiumProductsCell.nib, forCellWithReuseIdentifier: FreemiumProductsCell.identifier)
        self.productCollectionView.keyboardDismissMode = .interactive
        self.emptyMessage.isHidden = true
        self.categoryCollectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .left)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCollectionView{
            if self.categories.count > 0{
                return self.categories.count
            }
            return 0
        }else{
            if self.freemiumProducts.count > 0 {
                self.emptyMessage.isHidden = true
                return self.freemiumProducts.count
            }else{
                self.emptyMessage.isHidden = false
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoryCollectionView{
            let cell: FreemiumCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: FreemiumCategoryCell.identifier, for: indexPath) as! FreemiumCategoryCell

                        
            if self.categories.count > 0{
                let categoryItem = self.categories[indexPath.row]
                cell.category = categoryItem
                if cell.isSelected{
                    fetchCategoryData(searchText: categoryItem.name)
                }
            }
            
            return cell
            
        }else{
            let cell: FreemiumProductsCell = collectionView.dequeueReusableCell(withReuseIdentifier: FreemiumProductsCell.identifier, for: indexPath) as! FreemiumProductsCell
                        
            if self.freemiumProducts.count > 0{
                let productItem = self.freemiumProducts[indexPath.row]
                
                cell.freemiumProducts = productItem
                
                if self.selectedProductId.contains(Int(productItem.productId)){
                    cell.deSelectBtn.isHidden = false
                }
                else{
                    cell.deSelectBtn.isHidden = true
                }
                
                cell.deSelectBtn.tag = Int(productItem.productId)
                cell.deSelectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
                
            }
            
            return cell
        }
    }
    
    @objc func onRemove(sender : UIButton){
        let productId = sender.tag
        
        if self.selectedProductId.contains(productId){
            removeDataofProduct(productId: productId)
        }
    }
    
    func removeDataofProduct(productId : Int){
        var productAtIndex = -1
        for index in 0..<self.selectedProductId.count{
            if self.selectedProductId[index] == productId{
                productAtIndex = index
            }
        }
        
        self.selectedSellingPrices.remove(at: productAtIndex)
        self.selectedAmounts.remove(at: productAtIndex)
        self.selectedProductId.remove(at: productAtIndex)
        self.productCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == categoryCollectionView{
            return CGSize(width: 200,height: 60)
        }else{
            return CGSize(width: 110, height: 150)
        }

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoryCollectionView{
            if self.categories.count > 0{
                let item = self.categories[indexPath.row]
                if item.name == "All"{
                    self.searchBar.text = ""
                    fetchData(searchText: "")
                }else{
                    self.searchBar.text = ""
                    fetchCategoryData(searchText: item.name)
                }
                
            }
        }else{
            if self.freemiumProducts.count > 0{
                let data = self.freemiumProducts[indexPath.row]
                
                self.selectedProduct = data
                self.selectedProductName = data.name
                
                if self.selectedProductId.contains(Int(data.productId)){
                    self.showAlert(title: LanguageManager.TheProductIsAlreadySelectedDeselectTheProductForUpdate, message: "")
                }
                else{
                    self.sellingPrice = Double(data.sellingPrice)
                    self.navigateToFreemiumSaleAddSecondVC()
                }
                
            }
        }
    }
    
    func navigateToFreemiumSaleAddSecondVC(){
        let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "FreemiumSaleAddSecondVC") as! FreemiumSaleAddSecondVC
        
        viewController.freemiumSaleDelegate = self
        viewController.saleAblePrice = self.sellingPrice
        viewController.selectedProductName = self.selectedProductName
                
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.emptyMessage.isHidden = true
    }
    
}

extension FreemiumSaleableProductsVC: FreemiumSaleableProductsViewDelegate{
    func setFreemiumCategoryData(data: ProductCategoryListDataMapper) {
        guard var categoryItem = data.categories, categoryItem.count > 0 else{
            return
        }
        categoryItem.insert(allCategory, at: 0)
        self.categories = categoryItem
       
    }
    
    func onFailed(data: String) {
        //self.showAlert(title: LanguageManager.NoCategoryFound, message: "")
    }
    
    func showLoading() {
        self.emptyMessage.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.emptyMessage.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getProductCategoryFromServer(getAll: true)
    }
    
}

extension FreemiumSaleableProductsVC : FreemiumProductSaleDelegate{
    func setProductData(amount: Double, sellingPrice: Double) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        self.selectedProductId.append(Int(selectedProduct.productId))
        selectedSellingPrices.append(sellingPrice)
        selectedAmounts.append(amount)
        self.productCollectionView.reloadData()
    }
}

extension FreemiumSaleableProductsVC{
    func fetchData(searchText: String){

        freemiumProducts.removeAll()
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FreemiumProducts")
        
        if searchText != ""{
            let productNamePredicate = NSPredicate(format: "name contains[c] %@", searchText)
            let categoryNamePredicate = NSPredicate(format: "categoryName contains[c] %@", searchText)
            let compoundPredicate = NSCompoundPredicate(type: .or, subpredicates: [productNamePredicate,categoryNamePredicate])
            fetchRequest.predicate = compoundPredicate
        }

        do {
            let results = try context.fetch(fetchRequest)
            let  products = results as! [FreemiumProducts]
            
            //print(products)
            
            if products.count <= 0{
                self.emptyMessage.isHidden = false
            }else{
                self.emptyMessage.isHidden = true
            }
            
            for product in products {
                freemiumProducts.append(product)
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }

    }
    
    func fetchCategoryData(searchText: String){

        freemiumProducts.removeAll()
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FreemiumProducts")
        
        fetchRequest.predicate = NSPredicate(format: "categoryName contains[c] %@", searchText)

        do {
            let results = try context.fetch(fetchRequest)
            let  products = results as! [FreemiumProducts]

            for product in products {
                freemiumProducts.append(product)
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }

    }
    
}

