//
//  SaleAddSecondVC.swift
//  Ponno
//
//  Created by a k azad on 16/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class SaleAddSecondVC: UIViewController {

    @IBOutlet weak var productNameLbl: UILabel!{
        didSet{
            if let productName = self.selectedProductName{
                productNameLbl.text = productName
            }
        }
    }
    @IBOutlet weak var quantityLbl: UILabel!{
        didSet{
            quantityLbl.text = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var quantity: UITextField!{
        didSet{
            quantity.placeholder = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var sellingPriceLbl: UILabel!{
        didSet{
            sellingPriceLbl.text = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var sellingPrice: UITextField!{
        didSet{
            sellingPrice.placeholder = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var sellingPriceHintLbl: UILabel!{
        didSet{
            self.sellingPriceHintLbl.textColor = UIColor.orange
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    
    var product: ProductList?
    var freemiumProduct : FreemiumProducts?
    
    //For Premium
    var delegate : SalesAddProductDelegate?
    
    //For Freemium
    var freemiumSaleDelegate : FreemiumProductSaleDelegate?
    
    var saleAblePrice : Double?
    var saleAbleQuantity : Double?
    var quantiy : Int?
    var maxQuantity: Double = 0.0
    var selectedProductName : String?
    var productSellingPrice : Double = 0.0
    
    var draftProduct : EditedProducts?
    
    var saleEditProduct : SaleProductList?
    
    var isPreOrderEnable: Bool = false
    
    var selectedPreOrderProduct : SalePreOrderEditProducts?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpViews()
        self.toolBarSetUp()
        self.setupMaxQuantity()
        self.setDraftMaxQuantity()
        self.setupSaleEditedProductMaxQuantity()
        self.sellingPriceHintLbl.isHidden = true
        self.setupSellingPriceHint()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext), for: .touchUpInside)
        self.quantity.addTarget(self, action: #selector(onQuantityTextEditing), for: .editingChanged)
        self.sellingPrice.addTarget(self, action: #selector(onSellingPriceChange(sender:)), for: .editingChanged)
        self.quantity.underlined()
        self.sellingPrice.underlined()
        
        self.title = LanguageManager.SaleBook
        
        guard let price = self.saleAblePrice else {
            return
        }
        self.sellingPrice.text = "\(price)"
        guard let quantity = self.saleAbleQuantity else{
            return
        }
        self.quantity.text = "\(quantity)"
        
    }
    
    func setupMaxQuantity(){
        if let item = self.product{
            if let max = item.quantity.toDouble(){
                self.maxQuantity = max
            }
        }
        
    }
    
    func setDraftMaxQuantity(){
        if let item = self.draftProduct{
            if let max = item.quantity.toDouble(){
                self.maxQuantity = max
            }
        }
    }
    
    func setupSaleEditedProductMaxQuantity(){
        if let item = self.saleEditProduct{
            if let max = item.quantity.toDouble(){
                self.maxQuantity = max
            }
        }
    }
    
    @objc func onSellingPriceChange(sender: UITextField){
        //self.setupSellingPriceHint()
        if let draftSellingPrice = self.sellingPrice.text?.toDouble(){
            if draftSellingPrice < productSellingPrice{
                sellingPriceHintLbl.isHidden = false
                sellingPriceHintLbl.text = "\(productSellingPrice)"
            }else{
                sellingPriceHintLbl.isHidden = true
            }
        }
        
        if let selectedSellingPrice = self.sellingPrice.text?.toDouble(){
            if selectedSellingPrice < productSellingPrice{
                sellingPriceHintLbl.isHidden = false
                sellingPriceHintLbl.text = "\(productSellingPrice)"
            }else{
                sellingPriceHintLbl.isHidden = true
            }
        }
//        if let data = self.draftProduct{
//            if let sellingPrice = data.sellingPrice.toDouble(){
//                if let draftSellingPrice = self.sellingPrice.text?.toDouble(){
//                    if draftSellingPrice < sellingPrice{
//                        sellingPriceHintLbl.isHidden = false
//                        sellingPriceHintLbl.text = "\(sellingPrice)"
//                    }else{
//                        sellingPriceHintLbl.isHidden = true
//                    }
//                }
//            }
//        }
    }
    
    func setupSellingPriceHint(){
        if let data = self.draftProduct{
            if let sellingPrice = data.sellingPrice.toDouble(){
                self.productSellingPrice = sellingPrice
                if data.soldSellingPrice < sellingPrice{
                    sellingPriceHintLbl.isHidden = false
                    sellingPriceHintLbl.text = "\(sellingPrice)"
                }else{
                    sellingPriceHintLbl.isHidden = true
                }
            }
        }else{
            if let data = self.product{
                if let sellingPrice = data.sellingPrice.toDouble(){
                    self.productSellingPrice = sellingPrice
                    if let soldPrice = data.soldSellingPrice.toDouble(){
                        if soldPrice < sellingPrice{
                            sellingPriceHintLbl.isHidden = false
                            sellingPriceHintLbl.text = "\(sellingPrice)"
                        }else{
                            sellingPriceHintLbl.isHidden = true
                        }
                    }
                }
            }
        }
    }
    
    func toolBarSetUp(){
        //QuantityToolBar
        let quantityToolBar = UIToolbar()
        quantityToolBar.barStyle = UIBarStyle.default
        quantityToolBar.isTranslucent = true
        quantityToolBar.tintColor = UIColor.black
        quantityToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
        
        quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        quantityToolBar.isUserInteractionEnabled = true
        
        self.quantity.inputAccessoryView = quantityToolBar
        
        //SellingPriceToolBar
        let sellingPriceToolBar = UIToolbar()
        sellingPriceToolBar.barStyle = UIBarStyle.default
        sellingPriceToolBar.isTranslucent = true
        sellingPriceToolBar.tintColor = UIColor.black
        sellingPriceToolBar.sizeToFit()
        
        let sellingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnSellingPrice(sender:)))
        
        sellingPriceToolBar.setItems([cancelButton, spaceButton, sellingPriceDoneButton], animated: false)
        sellingPriceToolBar.isUserInteractionEnabled = true
        
        self.sellingPrice.inputAccessoryView = sellingPriceToolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
        self.quantity.resignFirstResponder()
        self.sellingPrice.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnSellingPrice(sender: UIBarButtonItem){
        self.sellingPrice.resignFirstResponder()
        self.nextBtn.becomeFirstResponder()
    }
    
    @objc func onQuantityTextEditing(sender: UITextField){
        if let quantity = self.quantity.text?.toDouble(){
            if isPreOrderEnable != true{
                //CheckHere
                if quantity > maxQuantity{
                    self.quantity.text = "\(maxQuantity)"
                }
            }
        }
    }
    
    @objc func onNext(sender : UIButton){
        if self.isValidated(){
            guard let quantity = self.quantity.text, let quantityDouble = Double(quantity), let sellingPrice = self.sellingPrice.text, let sellingPriceDouble = Double(sellingPrice)  else{
                return
            }
            self.delegate?.setProductData(amount : quantityDouble, sellingPrice : sellingPriceDouble)
            self.freemiumSaleDelegate?.setProductData(amount: quantityDouble, sellingPrice: sellingPriceDouble)
            self.navigationController?.popViewController(animated: true)
        }
    }
    func isValidated()->Bool{
        if self.quantity.text == ""{
            self.showAlert(title: LanguageManager.QuantityIsRequired, message: "")
            return false
        }
        else if self.sellingPrice.text == ""{
            self.showAlert(title: LanguageManager.SellingPriceIsRequired, message: "")
            return false
        }
        return true
    }
    
    
    func navigateToSaleAddVC(){
        let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleInfoVC")
        self.present(viewController, animated: false, completion: nil)
    }
}
