//
//  SaleOnCustomerViewController.swift
//  Ponno
//
//  Created by a k azad on 3/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol SaleOnCustomerDelegate {
    func setCustomerInfo(customerId: String, newCustomerName: String, phone: String, address: String)
}

class SaleOnCustomerViewController: UIViewController {
    
    @IBOutlet weak var customerNameText: UITextField!
    @IBOutlet weak var newCustomerName: UITextField!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var addressText: UITextField!
    
    //HeightConstraint
    @IBOutlet weak var newCustomerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var customerNameHeight: NSLayoutConstraint!
    @IBOutlet weak var phoneHeight: NSLayoutConstraint!
    @IBOutlet weak var addressHeight: NSLayoutConstraint!
    
    private var presenter = SaleOnCustomerPresenter(service: SalesService())
    
    var customerList : [Customers] = [] {
        didSet{
            self.customerPicker.reloadAllComponents()
        }
    }
    
    var customerId : String?
    var customerPicker = UIPickerView()

    var isLoading : Bool = false
    var currentPage : Int = 1
    
    var delegate: SaleOnCustomerViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpPickerView()
        self.attachPresenter()
    }
    
    //PickerView
    func setUpPickerView(){
        
        self.customerPicker.delegate = self
        
        let customerToolBar = UIToolbar()
        customerToolBar.barStyle = UIBarStyle.default
        customerToolBar.isTranslucent = true
        customerToolBar.tintColor = UIColor.black
        customerToolBar.sizeToFit()
        
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let customerDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCustomerDone(sender:)))
        
        customerToolBar.setItems([cancelButton, spaceButton, customerDoneButton], animated: false)
        customerToolBar.isUserInteractionEnabled = true
        
        self.customerNameText.inputView = customerPicker
        self.customerNameText.inputAccessoryView = customerToolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingCustomerDone(sender: UIBarButtonItem){
        self.customerNameText.resignFirstResponder()
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: "Successful", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popToRootViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: PickerViewDelegate
extension SaleOnCustomerViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.customerPicker{
            return customerList.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.customerPicker{
            if self.customerList.count > 0{
                let list = self.customerList[row]
                self.customerNameText.text = list.name
                self.customerId = "\(list.id)"
                
                if isLoading == false && row == self.customerList.count - 1 {
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getCustomerDataFromServer(page: self.currentPage) 
                }
                return self.customerList[row].name
            }else{
                return ""
            }
        }
        else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.customerPicker{
            if self.customerList.count > 0 {
                let list = self.customerList[row]
                self.customerNameText.text = list.name
                self.customerId = "\(list.id)"
            }
        }else{
            return
        }
    }
}

//Mark: Api Delegate
extension SaleOnCustomerViewController: SaleOnCustomerViewDelegate{ 
    func setCustomerData(data: CustomerDataMapper) {
        guard let list = data.customers, list.count > 0 else{
            return
        }
        self.customerList += list
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(message: String) {
        self.showAlert(title: message, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getCustomerDataFromServer(page: self.currentPage)
    }
}
