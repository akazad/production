//
//  SalePreOrderedProductListViewController.swift
//  Ponno
//
//  Created by a k azad on 14/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class SalePreOrderedProductListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var nextView: UIView!{
        didSet{
            self.setUpCardView(uiview: nextView)
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var editedProducts : [SalePreOrderEditProducts] = []{
        didSet{
            if self.editedProducts.count > 0{
                self.editedProducts = self.editedProducts.sorted(by: { $0.soldQuantity > $1.soldQuantity})
            }
            //self.refreshTableView()
        }
    }
    
    var filteredList : [SalePreOrderEditProducts] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedSellingPrices : [Double] = []
    var selectedSerials : [String] = []
    
    var searchText = ""
    var timer : Timer?
    
    var sellingPrice : Double?
    var saleableQuantity : Double?
    var selectedProduct : SalePreOrderEditProducts?
    var selectedProductName : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configureTableView()
        self.setUpSearchBar()
        self.setUpViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setUpInitialLanguage()
        self.setNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.resignFirstResponder()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext(sender:)), for: .touchUpInside)
        
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    @objc func onNext(sender : UIButton){
//        if self.selectedProductId.count > 0{
//            //self.navigateToSalePreOrderInfoViewController()
//        }
        
        if self.editedProducts.count > 0{
            for product in self.editedProducts {
                if product.hasSerial == 1 {
                    if self.countSerialNumber(item: product) == 0{
                        self.showAlert(title: product.name, message: LanguageManager.SerialNumberIsRequired)
                        return
                    }
                }
            }
            if self.selectedProductId.count > 0{
                self.navigateToSaleInfoViewController()
            }
        }
        

    }
    
    func countSerialNumber(item: SalePreOrderEditProducts)-> Int{
        var itemCount: Int = 0
        if item.hasSerial == 1 {
            for serial in item.soldSerialNo{
                if serial == ""{
                    if let index = item.soldSerialNo.firstIndex(of: serial) {
                        item.soldSerialNo.remove(at: index)
                    }
                    
                }
            }
           // item.soldSerialNo = item.soldSerialNo.compactMap{ $0 }
            itemCount = item.soldSerialNo.count
        }
        return itemCount
    }
    
    func navigateToSaleInfoViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleInfoViewController") as! SaleInfoViewController
        
        viewController.selectedProductId = self.selectedProductId
        viewController.selectedAmounts = self.selectedAmounts
        viewController.selectedSellingPrices = self.selectedSellingPrices
        viewController.selectedSerials = self.selectedSerials
        viewController.draftBtnHide = true
         self.navigationController?.pushViewController(viewController, animated: true)
    }

}

//MARK: SearchBar Delegate
extension SalePreOrderedProductListViewController: UISearchBarDelegate{
    fileprivate func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.ProductSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.isSearchActive = true
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if self.editedProducts.count > 0 {
            if searchText == "" {
                self.isSearchActive = false
                return
            }else{
                self.isSearchActive = true
            }
        }
        
        guard let textToSearch = searchBar.text else {
            return
        }
        
        if textToSearch.count > 2{
            filteredList = self.editedProducts.filter { (product : SalePreOrderEditProducts) -> Bool in
                return product.name.lowercased().contains(textToSearch.lowercased())  || product.categoryName.lowercased().contains(textToSearch.lowercased()) || product.variant.lowercased().contains(textToSearch.lowercased())
            }
        }
    }
}

//Mark: TableViewDelegate
extension SalePreOrderedProductListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SaleProductCell.nib, forCellReuseIdentifier: SaleProductCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive{
            guard self.filteredList.count > 0 else {
                return 0
            }
            return self.filteredList.count
        }else{
            if self.editedProducts.count > 0 {
                return self.editedProducts.count
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SaleProductCell.identifier, for: indexPath) as! SaleProductCell
        
        if self.editedProducts.count > 0 {
            let product = self.editedProducts[indexPath.row]
            
            cell.preOrderEditedProducts = product
            self.saleableQuantity = product.quantity
            cell.selectBtn.isHidden = true
            cell.productId = product.productId
            
        }
        
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSearchActive{
            if self.filteredList.count > 0{
                let data = self.filteredList[indexPath.row]
                
                self.didSelectOnSelectedProductList(data: data)
            }
        }else{
            if self.editedProducts.count > 0 {
                let data = self.editedProducts[indexPath.row]
                
                self.didSelectOnSelectedProductList(data: data)
            }
        }
    }
    
    func didSelectOnSelectedProductList(data: SalePreOrderEditProducts){
        self.sellingPrice = Double(data.soldSellingPrice)
        self.saleableQuantity = Double(data.soldQuantity)
        
        for item in self.editedProducts{
            if item.productId == data.productId{
                self.selectedProduct = item
            }
        }
        
        if data.hasSerial == 0{
            self.navigateToSaleAddVC()
        }
        else{
            navigateToSaleAddWithSerialVC(item: data)
        }
        
    }
    
    func navigateToSaleAddVC(){
        let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSecondVC") as! SaleAddSecondVC
        
        viewController.delegate = self
        viewController.saleAblePrice = self.sellingPrice
        viewController.selectedProductName = self.selectedProductName
        viewController.saleAbleQuantity = self.saleableQuantity
        viewController.selectedPreOrderProduct = self.selectedProduct
        viewController.isPreOrderEnable = true
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func navigateToSaleAddWithSerialVC(item: SalePreOrderEditProducts){
        if self.prepareSerialNumbers(serials: item.serials).count > 0{
            let serialList = self.prepareSerialNumbers(serials: item.serials)
            
            let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSerialViewController") as! SaleAddSerialViewController
            
            viewController.serialList = serialList
            viewController.saleAblePrice = self.sellingPrice
            viewController.delegate = self
            viewController.selectedProductName = item.name
            viewController.isCodeScannerActive = false
            viewController.productSerials = item.soldSerialNo
            
            //            if codeScannerActive == true{
            //                viewController.isCodeScannerActive = true
            //                viewController.searchSerial = self.searchText
            //            }else{
            //                viewController.isCodeScannerActive = false
            //            }
            
            //viewController.isCodeScannerActive = self.codeScannerActive
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
}

//Product Delegate
extension SalePreOrderedProductListViewController : SalesAddProductDelegate{
    func setSerialData(serial: [String], sellingPrice: Double) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        //let soldSerial = serial.filter{ $0 != "" }
        print(serial)
        
        if self.selectedProductId.contains(selectedProduct.productId){
            self.removeDataofProductObject(productId: selectedProduct.productId)
        }
        self.selectedProductId.append(selectedProduct.productId)
        self.selectedSerials.append(serial.joined(separator: ","))
        self.selectedSellingPrices.append(sellingPrice)
        self.selectedAmounts.append(Double(serial.count))
        
        print(serial)
        
        self.addSerialDataToSelectedProducts(productId: selectedProduct.productId, soldQuantity: Double(serial.count), soldSerials: serial, soldSellingPrice: sellingPrice)
        
    }
    
    func setProductData(amount: Double, sellingPrice: Double) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        if self.selectedProductId.contains(selectedProduct.productId){
            self.removeDataofProductObject(productId: selectedProduct.productId)
        }
        self.selectedProductId.append(selectedProduct.productId)
        selectedSellingPrices.append(sellingPrice)
        selectedAmounts.append(amount)
        selectedSerials.append("")
        
        self.addDataToSelectedProducts(productId: selectedProduct.productId, soldQuantity: amount, soldSellingPrice: sellingPrice)
        
    }
    
    func removeDataofProductObject(productId : Int){
        var productAtIndex = -1
        if let index = self.selectedProductId.firstIndex(of: productId){
            productAtIndex = index
        }
        
        self.selectedSellingPrices.remove(at: productAtIndex)
        self.selectedAmounts.remove(at: productAtIndex)
        self.selectedSerials.remove(at: productAtIndex)
        self.selectedProductId.remove(at: productAtIndex)
    }
    
}

extension SalePreOrderedProductListViewController {
    func addSerialDataToSelectedProducts(productId: Int, soldQuantity: Double, soldSerials: [String], soldSellingPrice: Double){
        if let product = findProductInfo(productId: productId){
            let selectedProduct = SalePreOrderEditProducts(inventoryId: product.inventoryId, productId: productId, quantity: product.quantity, sellingPrice: product.sellingPrice, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, soldSerials: soldSerials, name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
            for item in self.editedProducts{
                if item.productId == productId{
                    self.editedProducts = self.editedProducts.filter{($0.productId != productId)}
                }
            }
            self.editedProducts.append(selectedProduct)
            self.refreshTableView()
        }
    }
    
    func findProductInfo(productId: Int) -> SalePreOrderEditProducts?{
        var productItem : SalePreOrderEditProducts?
        for item in self.editedProducts{
            if item.productId == productId{
                productItem = item
            }
        }
        return productItem
    }
    
    func addDataToSelectedProducts(productId: Int, soldQuantity: Double, soldSellingPrice: Double){
        if let product = findProductInfo(productId: productId){
            let selectedProduct = SalePreOrderEditProducts(inventoryId: product.inventoryId, productId: productId, quantity: product.quantity, sellingPrice: product.sellingPrice, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, soldSerials: [""], name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
            for item in self.editedProducts{
                if item.productId == productId{
                    self.editedProducts = self.editedProducts.filter{($0.productId != productId)}
                }
            }
            self.editedProducts.append(selectedProduct)
            self.refreshTableView()
        }
    }
    
}
