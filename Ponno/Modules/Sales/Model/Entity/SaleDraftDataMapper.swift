//
//  SaleDraftDataMapper.swift
//  Ponno
//
//  Created by a k azad on 6/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SaleDraftDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var saleDraftLists : [SaleDraftLists]? 
    var pagination : Pagination?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        saleDraftLists <- map["draft_lists"]
        pagination <- map["pagination"]
    }

}
