//
//  SaleEditDataMapper.swift
//  Ponno
//
//  Created by a k azad on 2/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SaleEditDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var sale : SaleInfo?
    var saleProducts : [SaleProductList]?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        sale <- map["sale"]
        saleProducts <- map["sale_products"]
    }

}

