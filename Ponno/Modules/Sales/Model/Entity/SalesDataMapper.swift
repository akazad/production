//
//  SalesDataMapper.swift
//  Ponno
//
//  Created by a k azad on 21/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

//import Foundation
//import ObjectMapper
//
//class SalesDataMapper: Mappable {
//    var success: Bool?
//    var message : String?
//    var totalSales : Int? = 0
//    var totalSalesAmount : Int? = 0
//    var totalDue : Int? = 0
//    var salesBook : [SalesListData]?
//
//    required init?(map: Map){
//
//    }
//
//    func mapping(map: Map) {
//        success <- map["success"]
//        message  <- map["message"]
//        totalSales <- map["total_sales"]
//        totalSalesAmount <- map["total_sale_amount"]
//        totalDue <- map["total_due"]
//        salesBook <- map["sale_book"]
//    }
//}

import Foundation
import ObjectMapper

class SalesDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var saleBook : [SalesListData]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        saleBook <- map["sale_book"]
        pagination <- map["pagination"]
    }
    
}




