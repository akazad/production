//
//  SaleInfo.swift
//  Ponno
//
//  Created by a k azad on 2/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SaleInfo : Mappable {
    var id : Int?
    var invoice : String?
    var remark : String?
    var total : String?
    var cash : Double?
    var wallet: String?
    var payable : String?
    var paid : String?
    var due : String?
    var discount : String?
    var discountUnit : String?
    var discountAmount : String?
    var paymentMethod : Int?
    var paymentMethodName : String?
    var paymentNumber : String?
    var deliverySystem : Int?
    var deliverySystemName : String?
    var deliveryCharge : String?
    var saleToken : String?
    var deliveryCommission : String?
    var commissionUnit : String?
    var commissionAmount : String?
    var date : String?
    var returnCash : String?
    var returnDate : String?
    var customerId : Int?
    var customerName : String?
    var customerWalletAmount : String?
    var addedBy : Int?
    var createdAt : String?
    var updatedAt : String?
    var deletedAt : String?
    var pharmacyId : Int?

    required init(map: Map) {
        
    }
    
    init(id: Int, invoice: String, remark: String, total : String,cash : String,wallet: String,payable : String,paid : String,due : String,discount : String,discountUnit : String,discountAmount : String, paymentMethodId : Int,paymentMethodName : String,paymentNumber : String,deliverySystemId : Int,deliverySystemName : String,deliveryCharge : String,saleToken : String,date : String,returnCash : String,customerId : Int,customerName : String,customerWalletAmount : String) {
        self.id = id
        self.invoice = invoice
        self.remark = remark
        self.total = total
        self.cash = cash.toDouble()
        self.wallet = wallet
        self.payable = payable
        self.paid = paid
        self.due = due
        self.discount = discount
        self.discountUnit = discountUnit
        self.discountAmount = discountAmount
        self.paymentMethod = paymentMethodId
        self.paymentMethodName = paymentMethodName
        self.paymentNumber = paymentNumber
        self.deliverySystem = deliverySystemId
        self.deliverySystemName = deliverySystemName
        self.deliveryCharge = deliveryCharge
        self.saleToken = saleToken
        self.date = date
        self.customerId = customerId
        self.customerName = customerName
        self.customerWalletAmount = customerWalletAmount
    }

    func mapping(map: Map) {

        id <- map["id"]
        invoice <- map["invoice"]
        remark <- map["remark"]
        total <- map["total"]
        cash <- map["cash"]
        wallet <- map["wallet"]
        payable <- map["payable"]
        paid <- map["paid"]
        due <- map["due"]
        discount <- map["discount"]
        discountUnit <- map["discount_unit"]
        discountAmount <- map["discount_amount"]
        paymentMethod <- map["payment_method"]
        paymentMethodName <- map["payment_method_name"]
        paymentNumber <- map["payment_number"]
        deliverySystem <- map["delivery_system"]
        deliverySystemName <- map["delivery_system_name"]
        deliveryCharge <- map["delivery_charge"]
        saleToken <- map["sale_token"]
        deliveryCommission <- map["delivery_commission"]
        commissionUnit <- map["commission_unit"]
        commissionAmount <- map["commission_amount"]
        date <- map["date"]
        returnCash <- map["return_cash"]
        returnDate <- map["return_date"]
        customerId <- map["customer_id"]
        customerName <- map["customer_name"]
        customerWalletAmount <- map["customer_wallet_amount"]
        addedBy <- map["added_by"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
        pharmacyId <- map["pharmacy_id"]
    }

}

