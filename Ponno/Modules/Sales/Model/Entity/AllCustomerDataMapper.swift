//
//  AllCustomerDataMapper.swift
//  Ponno
//
//  Created by a k azad on 14/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class AllCustomerDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var customers : [AllCustomers]? 
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        customers <- map["customers"]
    }
    
}
