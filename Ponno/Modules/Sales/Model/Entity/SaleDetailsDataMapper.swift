//
//  SaleDetailsDataMapper.swift
//  Ponno
//
//  Created by a k azad on 22/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SaleDetailsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var saleDetails : SaleDetails?
    var saleProducts : [SaleProducts]?
    var saleReturns : [SaleReturns]?
    var dueTransactions : [DueTransactions]? 
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        saleDetails <- map["sale_details"]
        saleProducts <- map["sale_products"]
        saleReturns <- map["sale_returns"]
        dueTransactions <- map["due_transactions"]
    }
    
}
