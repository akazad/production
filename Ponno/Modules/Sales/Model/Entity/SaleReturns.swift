//
//  SaleReturns.swift
//  Ponno
//
//  Created by a k azad on 30/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SaleReturns : Mappable {
    var productId : Int = 0
    var name : String = ""
    var categoryName : String = ""
    var variant : String = ""
    var saleProductId: Int = 0
    var quantity : String = ""
    var serialNumber : String = ""
    var unitPrice : String = ""
    var totalPrice : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        productId <- map["product_id"]
        name <- map["name"]
        categoryName <- map["category_name"]
        variant <- map["variant"]
        saleProductId <- map["sale_product_id"]
        quantity <- map["quantity"]
        serialNumber <- map["serial_no"]
        unitPrice <- map["unit_price"]
        totalPrice <- map["total_price"]
    }
    
}
