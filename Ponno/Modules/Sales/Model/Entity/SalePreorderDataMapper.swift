//
//  SalePreorderDataMapper.swift
//  Ponno
//
//  Created by a k azad on 6/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SalePreorderDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var salePreOrderLists : [SalePreOrderLists]?
    var pagination : Pagination?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        salePreOrderLists <- map["pre_order_lists"]
        pagination <- map["pagination"]
    }

}
