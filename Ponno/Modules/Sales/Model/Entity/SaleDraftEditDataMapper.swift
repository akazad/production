//
//  SaleDraftEditDataMapper.swift
//  Ponno
//
//  Created by a k azad on 10/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SaleDraftEditDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var saleEditInfo : SaleEditInfo?
    var editedProducts : [EditedProducts]? 

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        saleEditInfo <- map["sale_info"]
        editedProducts <- map["products_info"]
    }

}

