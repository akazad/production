//
//  SalePerDayDataMapper.swift
//  Ponno
//
//  Created by a k azad on 22/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class SalePerDayDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var date : String = ""
    var summary : [SalesPerDaySummery]?
    var saleListDateWise : [SaleListDateWise]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        date <- map["date"]
        summary <- map["summary"]
        saleListDateWise <- map["sale_list_date_wise"]
        pagination <- map["pagination"]
    }
    
}

