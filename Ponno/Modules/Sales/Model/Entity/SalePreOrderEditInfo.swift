//
//  SalePreOrderEditInfo.swift
//  Ponno
//
//  Created by a k azad on 14/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SalePreOrderEditInfo : Mappable {
    var customerId : Int?
    var customerName : String?
    var deliveryDate : String?
    var description : String?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        customerId <- map["customer_id"]
        customerName <- map["customer_name"]
        deliveryDate <- map["delivery_date"]
        description <- map["description"]
    }

}

