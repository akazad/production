//
//  SaleDetailsData.swift
//  Ponno
//
//  Created by a k azad on 22/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class SaleDetails : Mappable {
    var id : Int = 0
    var invoice : String = ""
    var total : String = ""
    var payable : String = ""
    var paid : String = ""
    var due : String = ""
    var discountUnit : String = ""
    var discountAmount : String = ""
    var discount : String = ""
    var deliveryCharge : String = ""
    var createdAt : String = ""
    var shopName : String = ""
    var shopAddress : String = ""
    var shopContact : String = ""
    var shopImage : String = ""
    var shopInvoiceNote : String = ""
    var customerName : String = ""
    var customerPhone : String = ""
    var customerAddress : String = ""
    var currentDue : String = ""
    var deliverySystem : String = ""
    var paymentMethod : String = ""
    var paymentMethodNumber : Int = 0
    var invoiceDue : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        invoice <- map["invoice"]
        total <- map["total"]
        payable <- map["payable"]
        paid <- map["paid"]
        due <- map["due"]
        discountUnit <- map["discount_unit"]
        discountAmount <- map["discount_amount"]
        discount <- map["discount"]
        deliveryCharge <- map["delivery_charge"]
        createdAt <- map["created_at"]
        shopName <- map["shop_name"]
        shopAddress <- map["shop_address"]
        shopContact <- map["shop_contact"]
        shopImage <- map["shop_image"]
        shopInvoiceNote <- map["shop_invoice_note"]
        customerName <- map["customer_name"]
        customerPhone <- map["customer_phone"]
        customerAddress <- map["customer_address"]
        currentDue <- map["current_due"]
        deliverySystem <- map["delivery_system"]
        paymentMethod <- map["payment_method"]
        paymentMethodNumber <- map["payment_number"]
        invoiceDue <- map["invoice_due"]
    }
    
}


