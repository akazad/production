//
//  SalesListData.swift
//  Ponno
//
//  Created by a k azad on 21/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
//

import Foundation
import ObjectMapper

class SalesListData : Mappable {
    var date : String = ""
    var daySale : Int = 0
    var daySaleAmount : String = ""
    var dayDue : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        date <- map["date"]
        daySale <- map["day_sale"]
        daySaleAmount <- map["day_sale_amount"]
        dayDue <- map["day_due"]
    }
    
}
