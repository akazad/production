//
//  SalesPerDaySummery.swift
//  Ponno
//
//  Created by a k azad on 3/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
//import Foundation
//import ObjectMapper
//
//class SalesSummery : Mappable {
//    var totalSales : String?
//    var totalSaleAmount : String?
//    var totalProduct : String?
//
//    required init(map: Map) {
//
//    }
//
//     func mapping(map: Map) {
//
//        totalSales <- map["total_sales"]
//        totalSaleAmount <- map["total_sale_amount"]
//        totalProduct <- map["total_product"]
//    }
//
//}


import Foundation
import ObjectMapper

class SalesPerDaySummery : Mappable {
    var title : String = ""
    var value : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        title <- map["title"]
        value <- map["value"]
    }
    
}
