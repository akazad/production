//
//  SaleDraftLists.swift
//  Ponno
//
//  Created by a k azad on 6/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SaleDraftLists : Mappable {
    var id : Int = -1
    var pharmacyId : Int = -1
    var addedBy : Int = -1
    var isUsed : Int = -1
    var total : String = ""
    var createdAt : String = ""
    var updatedAt : String = ""
    var deletedAt : String = ""
    var customerName : String = ""
    var due : String = ""

    required init(map: Map) {

    }

    func mapping(map: Map) {

        id <- map["id"]
        pharmacyId <- map["pharmacy_id"]
        addedBy <- map["added_by"]
        isUsed <- map["is_used"]
        total <- map["total"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
        customerName <- map["customer_name"]
        due <- map["due"]
    }

}

