//
//  SaleEditInfo.swift
//  Ponno
//
//  Created by a k azad on 10/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SaleEditInfo : Mappable {
    var remark : String?
    var total : Double?
    var payable : Double?
    var paid : Double?
    var due : Double?
    var discount : Double?
    var discountUnit : String?
    var discountAmount : Double?
    var paymentMethod : Int?
    var paymentMethodName : String?
    var paymentNumber : String?
    var deliverySystem : Int?
    var deliverySystemName : String?
    var deliveryCharge : Double?
    var date : String?
    var customerId : Int?
    var customerName : String?
    var customerWalletAmount : Double?
    var cash : Double?
    var wallet : Double?
    var stepNo : Int?
    var saleToken : String?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        remark <- map["remark"]
        total <- map["total"]
        payable <- map["payable"]
        paid <- map["paid"]
        due <- map["due"]
        discount <- map["discount"]
        discountUnit <- map["discount_unit"]
        discountAmount <- map["discount_amount"]
        paymentMethod <- map["payment_method"]
        paymentMethodName <- map["payment_method_name"]
        paymentNumber <- map["payment_number"]
        deliverySystem <- map["delivery_system"]
        deliverySystemName <- map["delivery_system_name"]
        deliveryCharge <- map["delivery_charge"]
        date <- map["date"]
        customerId <- map["customer_id"]
        customerName <- map["customer_name"]
        customerWalletAmount <- map["customer_wallet_amount"]
        cash <- map["cash"]
        wallet <- map["wallet"]
        stepNo <- map["step_no"]
        saleToken <- map["sale_token"]
    }

}

