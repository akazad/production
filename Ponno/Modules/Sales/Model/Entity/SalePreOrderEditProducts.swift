//
//  SalePreOrderEditProducts.swift
//  Ponno
//
//  Created by a k azad on 14/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SalePreOrderEditProducts : Mappable {
    var inventoryId : Int = -1
    var quantity : Double = 0
    var unit : String = ""
    var sellingPrice : Double = 0
    var buyingPrice : Double = 0
    var productId : Int = -1
    var name : String = ""
    var variant : String = ""
    var image : String = ""
    var categoryId : Int = -1
    var categoryName : String = ""
    var hasSerial : Int = -1
    var serials : String = ""
    var soldQuantity : Double = 0
    var soldSellingPrice :Double = 0
    var soldSerialNo : [String] = [""]

    required init(map: Map) {

    }
    
    init(inventoryId: Int, productId: Int, quantity: Double, sellingPrice: Double, serials: String, name: String, image: String, categoryName: String, categoryId: Int, variant: String, unitName: String) {
        self.inventoryId = inventoryId
        self.productId = productId
        self.quantity = quantity
        self.sellingPrice = sellingPrice
        self.serials = serials
        self.name = name
        self.image = image
        self.categoryName = categoryName
        self.categoryId = categoryId
        self.variant = variant
        self.unit = unitName
    }
    
    init(inventoryId: Int,productId: Int, quantity: Double, sellingPrice: Double, soldQuantity: Double, soldSellingPrice: Double, hasSerial: Int, serials: String, name: String, image: String, categoryName: String, categoryId: Int, variant: String, unitName: String) {
        self.inventoryId = inventoryId
        self.productId = productId
        self.quantity = quantity
        self.soldQuantity = soldQuantity
        self.soldSellingPrice = soldSellingPrice
        self.sellingPrice = sellingPrice
        self.hasSerial = hasSerial
        self.serials = serials
        self.name = name
        self.image = image
        self.categoryName = categoryName
        self.categoryId = categoryId
        self.variant = variant
        self.unit = unitName
    }
    
    init(inventoryId: Int,productId: Int, quantity: Double, sellingPrice: Double, soldQuantity: Double, soldSellingPrice: Double, hasSerial: Int, serials: String, soldSerials: [String], name: String, image: String, categoryName: String, categoryId: Int, variant: String, unitName: String) {
        self.inventoryId = inventoryId
        self.productId = productId
        self.quantity = quantity
        self.soldQuantity = soldQuantity
        self.soldSellingPrice = soldSellingPrice
        self.sellingPrice = sellingPrice
        self.hasSerial = hasSerial
        self.serials = serials
        self.soldSerialNo = soldSerials
        self.name = name
        self.image = image
        self.categoryName = categoryName
        self.categoryId = categoryId
        self.variant = variant
        self.unit = unitName
    }

    func mapping(map: Map) {

        inventoryId <- map["inventory_id"]
        quantity <- map["quantity"]
        unit <- map["unit"]
        sellingPrice <- map["selling_price"]
        buyingPrice <- map["buying_price"]
        productId <- map["product_id"]
        name <- map["name"]
        variant <- map["variant"]
        image <- map["image"]
        categoryId <- map["category_id"]
        categoryName <- map["category_name"]
        hasSerial <- map["has_serial"]
        serials <- map["serials"]
        soldQuantity <- map["sold_quantity"]
        soldSellingPrice <- map["sold_selling_price"]
    }

}

