//
//  SalePreOrderEditDataMapper.swift
//  Ponno
//
//  Created by a k azad on 14/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class SalePreOrderEditDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var salePreOrderEditInfo : SalePreOrderEditInfo?
    var salePreOrderEditProducts : [SalePreOrderEditProducts]?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        salePreOrderEditInfo <- map["sale_info"]
        salePreOrderEditProducts <- map["products_info"]
    }

}

