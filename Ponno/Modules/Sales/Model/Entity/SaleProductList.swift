//
//  SaleProductList.swift
//  Ponno
//
//  Created by a k azad on 2/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SaleProductList : Mappable {
    var inventoryId : Int = -1
    var saleId : Int = 0
    var quantity : String = ""
    var unit : String = ""
    var sellingPrice : String = ""
    var buyingPrice : String = ""
    var stockAlert : String = ""
    var productId : Int = -1
    var name : String = ""
    var company : String = ""
    var variant : String = ""
    var sku : String = ""
    var image : String = ""
    var categoryId : Int = -1
    var categoryName : String = ""
    var hasSerial : Int = 0
    var serials : String = ""
    var soldQuantity : Int = 0
    var soldSellingPrice : String = ""
    var soldSerialId : [Int] = []
    var soldSerialNo : [String] = []
    var soldItemFlag : Bool = false
    
    required init(map: Map) {

    }
    
    init(productId: Int, quantity: String, sellingPrice: String, serials: String, name: String, image: String, categoryName: String, categoryId: Int, variant: String, unitName: String) {
        self.productId = productId
        self.quantity = quantity
        self.sellingPrice = sellingPrice
        self.serials = serials
        self.name = name
        self.image = image
        self.categoryName = categoryName
        self.categoryId = categoryId
        self.variant = variant
        self.unit = unitName
    }
    
    init(productId: Int, quantity: String, soldQuantity: String, soldSellingPrice: String, hasSerial: Int, serials: String, soldSerials: [String], name: String, image: String, categoryName: String, categoryId: Int, variant: String, unitName: String) {
        self.productId = productId
        self.quantity = quantity
        self.soldQuantity = soldQuantity.toInt() ?? 0
        self.soldSellingPrice = soldSellingPrice
        self.sellingPrice = soldSellingPrice
        self.hasSerial = hasSerial
        self.serials = serials
        self.soldSerialNo = soldSerials
        self.name = name
        self.image = image
        self.categoryName = categoryName
        self.categoryId = categoryId
        self.variant = variant
        self.unit = unitName
    }
    
//    init(productId: Int, quantity: String) {
//        self.productId = productId
//        self.quantity = quantity
//    }

    func mapping(map: Map) {

        inventoryId <- map["inventory_id"]
        saleId <- map["sale_id"]
        quantity <- map["quantity"]
        unit <- map["unit"]
        sellingPrice <- map["selling_price"]
        buyingPrice <- map["buying_price"]
        stockAlert <- map["stock_alert"]
        productId <- map["product_id"]
        name <- map["name"]
        company <- map["company"]
        variant <- map["variant"]
        sku <- map["sku"]
        image <- map["image"]
        categoryId <- map["category_id"]
        categoryName <- map["category_name"]
        hasSerial <- map["has_serial"]
        serials <- map["serials"]
        soldQuantity <- map["sold_quantity"]
        soldSellingPrice <- map["sold_selling_price"]
        soldSerialId <- map["sold_serial_id"]
        soldSerialNo <- map["sold_serial_no"]
    }

}
