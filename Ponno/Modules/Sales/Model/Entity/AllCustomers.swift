//
//  AllCustomer.swift
//  Ponno
//
//  Created by a k azad on 14/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class AllCustomers : Mappable {
    var id : Int = 0
    var name : String = ""
    var phone: String = ""
    var address: String = ""
    var currentDue : Double = 0.0
    var hasWallet: Int = -1
    var amount : Double = 0
    
    required init(map: Map) {
        
    }
    
    init(id: Int, name: String, currentDue: Double) {
        self.id = id
        self.name = name
        self.currentDue = currentDue
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        address <- map["address"]
        currentDue <- map["current_due"]
        hasWallet <- map["has_wallet"]
        amount <- map["amount"]
    }
    
}
