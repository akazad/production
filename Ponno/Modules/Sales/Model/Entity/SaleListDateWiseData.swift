//
//  SaleListDateWiseData.swift
//  Ponno
//
//  Created by a k azad on 28/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SaleListDateWise : Mappable {
    var id : Int = 0
    var addedBy : Int = 0
    var invoice : String = ""
    var total : String = ""
    var payable : String = ""
    var due : String = ""
    var paid : String = ""
    var invoiceDue : String = ""
    var time : String = ""
    var deliverySystemsId : Int = 0
    var deliverySystemName : String = ""
    var paymentMethodName : String = ""
    var customerId : Int = 0
    var customerName : String = ""
    var returnCash : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        addedBy <- map["added_by"]
        invoice <- map["invoice"]
        total <- map["total"]
        payable <- map["payable"]
        due <- map["due"]
        paid <- map["paid"]
        invoiceDue <- map["invoice_due"]
        time <- map["time"]
        deliverySystemsId <- map["delivery_systems_id"]
        deliverySystemName <- map["delivery_system_name"]
        paymentMethodName <- map["payment_method_name"]
        customerId <- map["customer_id"]
        customerName <- map["customer_name"]
        returnCash <- map["return_cash"]
    }
    
}

