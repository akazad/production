//
//  PaymentMethodDataMapper.swift
//  Ponno
//
//  Created by a k azad on 17/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class PaymentMethodDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var paymentMethodList : [PaymentMethodList]?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        paymentMethodList <- map["payment_methods"]
    }
    
}

