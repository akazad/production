//
//  SalesService.swift
//  Ponno
//
//  Created by a k azad on 21/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

public class SalesService : NSObject{
    
    func getSalesData(page : Int, success: @escaping (SalesDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Sales + RestURL.sharedInstance.getSalesParams(page: page)
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SalesDataMapper>) in
                if let salesResponse = response.result.value{
                    if let response = salesResponse.saleBook, response.count > 0 {
                        success(salesResponse)
                    }else if let failureMessage = salesResponse.message{
                        failure(failureMessage)
                    }
                }else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        
    }
    
    func getSalesDateSearchData(page : Int, startDate: String, endDate : String, success: @escaping (SalesDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Sales + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getDateRangePage(text: page)
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SalesDataMapper>) in
                if let salesResponse = response.result.value{
                    if let response = salesResponse.saleBook, response.count > 0 {
                        success(salesResponse)
                    }else if let failureMessage = salesResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //SaleCustomer Search
    func getSalesCustomerSearchData(page : Int, customer : String, success: @escaping (SalePerDayDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let searchUrl = RestURL.sharedInstance.Sales + RestURL.sharedInstance.getSearchText(text: customer) + RestURL.sharedInstance.getDateRangePage(text: page)
        
        guard let url = searchUrl.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SalePerDayDataMapper>) in
                if let salesResponse = response.result.value{
                    if let response = salesResponse.saleListDateWise, response.count > 0 {
                        success(salesResponse)
                    }else if let failureMessage = salesResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getPerDaySalesData(page: Int, date : String , success: @escaping (SalePerDayDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SalesPerDay + RestURL.sharedInstance.getSalesPerDayUrl(date: date) + RestURL.sharedInstance.getSalesPerDayParams(page: page)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SalePerDayDataMapper>) in
                if let salesPerDayResponse = response.result.value{
                    if let response = salesPerDayResponse.saleListDateWise, response.count > 0 {
                        success(salesPerDayResponse)
                    }else if let failureResponse = salesPerDayResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getPerDaySalesSearchData(page: Int, date : String, searchText: String, success: @escaping (SalePerDayDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SalesPerDay + RestURL.sharedInstance.getSalesPerDayUrl(date: date) +
            RestURL.sharedInstance.getSearchText(text: searchText) +
            RestURL.sharedInstance.getPageNumber(page: page)
        
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SalePerDayDataMapper>) in
                if let salesPerDayResponse = response.result.value{
                    if let response = salesPerDayResponse.saleListDateWise, response.count > 0 {
                        success(salesPerDayResponse)
                    }else if let failureResponse = salesPerDayResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getSaleDetailsData(iD : Int , success: @escaping (SaleDetailsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SaleDetails + RestURL.sharedInstance.getSaleDetailsUrl(id: iD) 
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SaleDetailsDataMapper>) in
                if let salesPerDayResponse = response.result.value{
                    if (salesPerDayResponse.saleDetails != nil) {
                        success(salesPerDayResponse)
                    }else if let failureResponse = salesPerDayResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getPaymentMethodData(success: @escaping (PaymentMethodDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SalesPaymentMethod
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<PaymentMethodDataMapper>) in
                if let paymentMethodResoponse = response.result.value{
                    if let paymentMethod = paymentMethodResoponse.paymentMethodList, paymentMethod.count > 0 {
                        success(paymentMethodResoponse)
                    }else if let failureMessage = paymentMethodResoponse.message{
                        failure(failureMessage)
                    }
                    
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getSaleEditData(iD : Int , success: @escaping (SaleEditDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SaleEdit + RestURL.sharedInstance.getSaleDetailsUrl(id: iD)
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SaleEditDataMapper>) in
                if let editResponse = response.result.value{
                    if (editResponse.success == true) {
                        success(editResponse)
                    }else if let failureResponse = editResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getSaleDraftEditData(iD : Int , success: @escaping (SaleDraftEditDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SaleDraftEdit + RestURL.sharedInstance.getSaleDetailsUrl(id: iD)
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SaleDraftEditDataMapper>) in
                if let editResponse = response.result.value{
                    if (editResponse.success == true) {
                        success(editResponse)
                    }else if let failureResponse = editResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getSaleDraftData(page : Int, success: @escaping (SaleDraftDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SaleDraft + RestURL.sharedInstance.getSalesParams(page: page)
        let headers = RestURL.sharedInstance.getCommonHeader()
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SaleDraftDataMapper>) in
                if let saleDraftResponse = response.result.value{
                    if let response = saleDraftResponse.saleDraftLists, response.count > 0 {
                        success(saleDraftResponse)
                    }else if let failureMessage = saleDraftResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getSaleDraftDateSearchData(page : Int, startDate: String, endDate : String, success: @escaping (SaleDraftDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SaleDraft + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getDateRangePage(text: page)
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SaleDraftDataMapper>) in
                if let saleDraftResponse = response.result.value{
                    if let response = saleDraftResponse.saleDraftLists, response.count > 0 {
                        success(saleDraftResponse)
                    }else if let failureMessage = saleDraftResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getSalePreorderData(page : Int, success: @escaping (SalePreorderDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SalePreorder + RestURL.sharedInstance.getSalesParams(page: page)
        let headers = RestURL.sharedInstance.getCommonHeader()
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SalePreorderDataMapper>) in
                if let salePreorderResponse = response.result.value{
                    if let response = salePreorderResponse.salePreOrderLists, response.count > 0 {
                        success(salePreorderResponse)
                    }else if let failureMessage = salePreorderResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getSalePreorderDateSearchData(page : Int, startDate: String, endDate : String, success: @escaping (SalePreorderDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SalePreorder + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getDateRangePage(text: page)
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SalePreorderDataMapper>) in
                if let salePreorderResponse = response.result.value{
                    if let response = salePreorderResponse.salePreOrderLists, response.count > 0 {
                        success(salePreorderResponse)
                    }else if let failureMessage = salePreorderResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getSalePreOrderEditData(iD : Int , success: @escaping (SalePreOrderEditDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SalePreOrderEdit + RestURL.sharedInstance.getSaleDetailsUrl(id: iD)
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<SalePreOrderEditDataMapper>) in
                if let editResponse = response.result.value{
                    if (editResponse.success == true) {
                        success(editResponse)
                    }else if let failureResponse = editResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //Mark: PostRequest
    func postNewSalesData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SalesAdd
        print(url)
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let salesAddResponse = response.result.value{
                if salesAddResponse.success == true{
                    success(salesAddResponse)
                }else if let failureMessage = salesAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                 failure(CommonMessages.ApiFailure)
            }
        }
        //print(request)
    }
    
    //SaleProductsReturn
    func postSaleProductsReturnData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SaleProductsReturn
        //print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let salesProductsReturnResponse = response.result.value {
                if salesProductsReturnResponse.success == true{
                    success(salesProductsReturnResponse)
                }else if let failureMessage = salesProductsReturnResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    //Mark: SaleCancel
    func PostPerDaySaleCancelData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SaleCancel
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let cancelSaleResponse = response.result.value {
                if cancelSaleResponse.success == true{
                    success(cancelSaleResponse)
                }else if let failureResponse = cancelSaleResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func getAllCustomerData(searchString: Bool, success: @escaping (AllCustomerDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Customer
            + RestURL.sharedInstance.getAllData(text: searchString)
        print(url)

        let customerHeaders = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: customerHeaders)
            .responseObject { (response: DataResponse<AllCustomerDataMapper>) in
                if let customerResponse = response.result.value{
                    if let response = customerResponse.customers, response.count > 0 {
                        success(customerResponse)
                    }
                    else if let failureMessage = customerResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
        
    }
    
    func postSaleEditData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SaleUpdate
        print(url)
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let salesUpdateResponse = response.result.value{
                if salesUpdateResponse.success == true{
                    success(salesUpdateResponse)
                }else if let failureMessage = salesUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                 failure(CommonMessages.ApiFailure)
            }
        }
        //print(request)
    }
    
    func postSaleDraftData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SaleDraftStore
        //print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let saleDraftResponse = response.result.value {
                if saleDraftResponse.success == true{
                    success(saleDraftResponse)
                }else if let failureMessage = saleDraftResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postSaleDraftUpdateData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SaleDraftUpdate
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let saleDraftUpdateResponse = response.result.value {
                if saleDraftUpdateResponse.success == true{
                    success(saleDraftUpdateResponse)
                }else if let failureMessage = saleDraftUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postSaleDraftDeleteData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SaleDraftDelete
        print(url)
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let draftDeleteResponse = response.result.value{
                if draftDeleteResponse.success == true{
                    success(draftDeleteResponse)
                }else if let failureMessage = draftDeleteResponse.message{
                    failure(failureMessage)
                }
            }else{
                 failure(CommonMessages.ApiFailure)
            }
        }
        //print(request)
    }
    
    func postSalePreOrderData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SalePreOrderStore
        //print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let salePreOrderResponse = response.result.value {
                if salePreOrderResponse.success == true{
                    success(salePreOrderResponse)
                }else if let failureMessage = salePreOrderResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postSalePreOrderDeleteData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SalePreOrderDelete
        print(url)
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let preOrderDeleteResponse = response.result.value{
                if preOrderDeleteResponse.success == true{
                    success(preOrderDeleteResponse)
                }else if let failureMessage = preOrderDeleteResponse.message{
                    failure(failureMessage)
                }
            }else{
                 failure(CommonMessages.ApiFailure)
            }
        }
        //print(request)
    }
    
    func postSalePreOrderUpdateData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.SaleDraftUpdate
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let saleDraftUpdateResponse = response.result.value {
                if saleDraftUpdateResponse.success == true{
                    success(saleDraftUpdateResponse)
                }else if let failureMessage = saleDraftUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
}


