//
//  ContactViewController.swift
//  Ponno
//
//  Created by a k azad on 2/10/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ContactViewController: BaseViewController {
    
    
    @IBOutlet weak var contactCardView: UIView!
    @IBOutlet weak var ponnoTitleLbl: UILabel!{
        didSet{
            ponnoTitleLbl.text = LanguageManager.PonnoTitle
        }
    }
    @IBOutlet weak var ponnoAddressLbl: UILabel!{
        didSet{
            ponnoAddressLbl.text = LanguageManager.PonnoAddress
        }
    }
    @IBOutlet weak var ponnoMobileTitleLbl: UILabel!{
        didSet{
            ponnoMobileTitleLbl.text = LanguageManager.PonnoMobile
        }
    }
    @IBOutlet weak var connectionCardView: UIView!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var webBtn: UIButton!
    @IBOutlet weak var facebookBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.setUpInitialLanguage()
        self.intialSetup()
    }
    
    func intialSetup(){
        self.title = LanguageManager.Contact
        
        //setUpCardView(uiview: contactCardView)
        setUpCardView(uiview: connectionCardView)
        
        self.callBtn.addTarget(self, action: #selector(onCallBtnTapped), for: .touchUpInside)
        self.webBtn.addTarget(self, action: #selector(onWebBtnTapped), for: .touchUpInside)
        self.facebookBtn.addTarget(self, action: #selector(onFacebookBtnTapped), for: .touchUpInside)
    }
    
    
    @objc func onCallBtnTapped(sender: UIButton){
        self.makePhoneCall(phoneNumber: "01999088654")
    }
    
    @objc func onWebBtnTapped(sender: UIButton){
        if let link = URL(string: "https://ponno.co"){
            if UIApplication.shared.canOpenURL(link) {
                UIApplication.shared.open(link, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func onFacebookBtnTapped(sender: UIButton){
        if let link = URL(string: "https://www.facebook.com/ponnoservice"){
            if UIApplication.shared.canOpenURL(link) {
                UIApplication.shared.open(link, options: [:], completionHandler: nil)
            }
        }
    }

}
