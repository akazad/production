//
//  ShopUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 7/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ShopUpdateViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func onImageUpload(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class ShopUpdatePresenter: NSObject {
    
    private let service : ShopService
    weak private var viewDelegate : ShopUpdateViewDelegate?
    
    init(service : ShopService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ShopUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postShopUpdateDataToServer(params: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.PostShopUpdateData(params: params, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postShopImageToServer(image : UIImage){
        self.viewDelegate?.showLoading()
        self.service.postUserImage(image: image, succeed: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onImageUpload(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}

