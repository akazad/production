//
//  ShopReferralPresenter.swift
//  Ponno
//
//  Created by a k azad on 30/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ShopReferralViewDelegate : NSObjectProtocol {
    //func getShopPaymentInfo(data: ShopPaymentDataMapper)
    func getRefferelInfo(data: ReferralInfoDataMapper)
    func getRefferelList(data: ReferralListDataMapper)
    //func getShopCategories()
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class ShopReferralPresenter: NSObject {
    
    private let service : ShopService
    weak private var viewDelegate : ShopReferralViewDelegate?
    
    init(service : ShopService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ShopReferralViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getReferralInfoDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getShopRefferalInfoData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.getRefferelInfo(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
