//
//  ShopPresenter.swift
//  Ponno
//
//  Created by a k azad on 22/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ShopViewDelegate : NSObjectProtocol {
    func setShopData(data: ShopInfoDataMapper)
    func onShopGeneralInfoUpdate(data: AddDataMapper)
    func onImageUpload(data: AddDataMapper)
    func onShopLoginInfoUpdate(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class ShopPresenter: NSObject {
    
    private let service : ShopService
    weak private var viewDelegate : ShopViewDelegate?
    
    init(service : ShopService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ShopViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getShopDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getShopData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setShopData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postShopGeneralInfoUpdateDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.PostShopGeneralInfoUpdateData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onShopGeneralInfoUpdate(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postShopLoginInfoUpdateDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.PostShopLoginInfoUpdateData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onShopLoginInfoUpdate(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postShopImageToServer(image : UIImage){
        self.viewDelegate?.showLoading()
        self.service.postUserImage(image: image, succeed: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onImageUpload(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
