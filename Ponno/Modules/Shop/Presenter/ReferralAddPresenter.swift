//
//  ReferralAddPresenter.swift
//  Ponno
//
//  Created by a k azad on 2/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ReferralAddViewDelegate : NSObjectProtocol {
    func setShopCategories(data: ShopCategoryDataMapper)
    func onreferralAdd(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class ReferralAddPresenter: NSObject {
    
    private let service : ShopService
    weak private var viewDelegate : ReferralAddViewDelegate?
    
    init(service : ShopService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ReferralAddViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getShopCategoriesFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getShopCategoriesData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setShopCategories(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postShopReferralAddDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.PostReferralAddData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onreferralAdd(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
            })
    }
    
}
