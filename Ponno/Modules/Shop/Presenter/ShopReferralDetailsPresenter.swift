//
//  ShopReferralDetailsPresenter.swift
//  Ponno
//
//  Created by a k azad on 1/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ShopReferralDetailsViewDelegate : NSObjectProtocol {
    func getRefferelList(data: ReferralListDataMapper)
    func postReferralRemoveData(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class ShopReferralDetailsPresenter: NSObject {
    
    private let service : ShopService
    weak private var viewDelegate : ShopReferralDetailsViewDelegate?
    
    init(service : ShopService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ShopReferralDetailsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getReferralDetailsDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllShopRefferalListData(get_all: getAll,success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.getRefferelList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postReferralRemoveDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.PostReferralRemoveData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.postReferralRemoveData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    } 
    
}
