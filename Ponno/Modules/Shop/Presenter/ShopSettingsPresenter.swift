//
//  ShopSettingsPresenter.swift
//  Ponno
//
//  Created by a k azad on 30/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ShopSettingsViewDelegate : NSObjectProtocol {
    func getShopSettingsData(data: ShopSettingsDataMapper)
    func onSettingsUpdate(data: AddDataMapper)
    func clearShopData(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class ShopSettingsPresenter: NSObject {
    
    private let service : ShopService
    weak private var viewDelegate : ShopSettingsViewDelegate?
    
    init(service : ShopService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ShopSettingsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getShopSettingsDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getShopSettingsData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.getShopSettingsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postShopSettingsUpdateDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.PostShopSettingsUpdateData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSettingsUpdate(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postClearShopDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.PostShopResetData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.clearShopData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
