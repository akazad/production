//
//  ShopService.swift
//  Ponno
//
//  Created by a k azad on 6/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper

public class ShopService : NSObject{
    
    func getShopData(success: @escaping (ShopInfoDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Shop
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ShopInfoDataMapper>) in
                if let shopResponse = response.result.value{
                    if shopResponse.success == true{
                        success(shopResponse)
                    }
                    if let shopInfo = shopResponse.shopDetails{
                        guard let shop = Ponno.getShopData()else{
                            return
                        }
                        let deliveryStatus = shop.deliverySystem
                        let paymentStatus = shop.paymentStatus
                        let inventoryStatus = shop.inventorySystem
                        let accStatus = shop.accStatus
                        let invoiceNote = shop.invoiceNote
                        let category = shop.category
                        
                        let shopData = ShopInfo(name: shopInfo.name , registrationNumber: shopInfo.regNo, address: shopInfo.address, image : shopInfo.image, inventorySystem: inventoryStatus, deliverySystem : deliveryStatus, invoiceNote: invoiceNote, paymentStatus: paymentStatus, accStatus: accStatus, category: category)
                        setShopData(shopInfo: shopData)
                    }
                    else if let failureMessage = shopResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //Mark: Shop Update
    func PostShopUpdateData(params: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ShopUpdate
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = params
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let shopUpdateResponse = response.result.value {
                if shopUpdateResponse.success == true{
                    success(shopUpdateResponse)
                }else if let failureMessage = shopUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    //Mark: ImageUpload
    func postUserImage(image : UIImage? ,  succeed: @escaping((AddDataMapper)->Void), failure : @escaping((String) -> Void)){
        
        let imageUploadUrl = RestURL.sharedInstance.ShopImageUpload
        
        let service = PostImageService()
        
        service.postShopImage(image: image, imageUrl: imageUploadUrl, succeed: { response in
            if let success = response.success, success == true{
                print(response.message ?? "")
                succeed(response)
            }
            else{
                failure(response.message ?? "")
            }
        }, failure: {message in
            failure(message)
        })
    }
    
    //Mark: Shop General Info Update
    func PostShopGeneralInfoUpdateData(params: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ShopGeneralInfoUpdate
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = params
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let shopGeneralInfoUpdateResponse = response.result.value {
                if shopGeneralInfoUpdateResponse.success == true{
                    success(shopGeneralInfoUpdateResponse)
                }else if let failureMessage = shopGeneralInfoUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    //Mark: Shop Login Info Update
    func PostShopLoginInfoUpdateData(params: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ShopLoginInfoUpdate
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = params
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let shopLoginInfoUpdateResponse = response.result.value {
                if shopLoginInfoUpdateResponse.success == true{
                    success(shopLoginInfoUpdateResponse)
                }else if let failureMessage = shopLoginInfoUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func getShopSettingsData(success: @escaping (ShopSettingsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ShopSettings
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ShopSettingsDataMapper>) in
                if let shopSettingResponse = response.result.value{
                    if shopSettingResponse.success == true{
                        success(shopSettingResponse)
                    }
                    else if let failureMessage = shopSettingResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //Mark: Shop Settings Update
    func PostShopSettingsUpdateData(params: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ShopSettingsUpdate
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = params
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let shopSettingsUpdateResponse = response.result.value {
                if shopSettingsUpdateResponse.success == true{
                    success(shopSettingsUpdateResponse)
                }else if let failureMessage = shopSettingsUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func getShopPaymentInfoData(success: @escaping (ShopPaymentDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ShopPaymentInfo
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ShopPaymentDataMapper>) in
                if let shopPaymentInfoResponse = response.result.value{
                    if shopPaymentInfoResponse.success == true{
                        success(shopPaymentInfoResponse)
                    }
                    else if let failureMessage = shopPaymentInfoResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getShopRefferalInfoData(success: @escaping (ReferralInfoDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ShopRefferalInfo
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ReferralInfoDataMapper>) in
                if let shopRefferalInfoResponse = response.result.value{
                    if shopRefferalInfoResponse.success == true{
                        success(shopRefferalInfoResponse)
                    }
                    else if let failureMessage = shopRefferalInfoResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getAllShopRefferalListData(get_all: Bool, success: @escaping (ReferralListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ShopRefferalList
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ReferralListDataMapper>) in
                if let shopRefferalListResponse = response.result.value{
                    if shopRefferalListResponse.success == true{
                        success(shopRefferalListResponse)
                    }
                    else if let failureMessage = shopRefferalListResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getShopCategoriesData(success: @escaping (ShopCategoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ShopCategories
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ShopCategoryDataMapper>) in
                if let shopCategoriesResponse = response.result.value{
                    if shopCategoriesResponse.success == true{
                        success(shopCategoriesResponse)
                    }
                    else if let failureMessage = shopCategoriesResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //Mark: Shop Update
    func PostReferralRemoveData(params: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.RemoveReferral
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = params
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let referralRemoveResponse = response.result.value {
                if referralRemoveResponse.success == true{
                    success(referralRemoveResponse)
                }else if let failureMessage = referralRemoveResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    //Mark: Shop Update
    func PostReferralAddData(params: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ShopReferralAdd
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = params
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let referralAddResponse = response.result.value {
                if referralAddResponse.success == true{
                    success(referralAddResponse)
                }else if let failureMessage = referralAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    //Mark: Shop Reset
    func PostShopResetData(params: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ClearShopData
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = params
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let clearShopDataResponse = response.result.value {
                if clearShopDataResponse.success == true{
                    success(clearShopDataResponse)
                }else if let failureMessage = clearShopDataResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    
}


