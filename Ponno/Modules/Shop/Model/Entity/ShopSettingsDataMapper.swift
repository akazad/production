//
//  ShopSettingsDataMapper.swift
//  Ponno
//
//  Created by a k azad on 30/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ShopSettingsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var shopSettingDetails : ShopSettingDetails?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        shopSettingDetails <- map["shop_details"]
    }
    
}
