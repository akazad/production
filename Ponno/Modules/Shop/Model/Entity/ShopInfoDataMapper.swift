//
//  ShopInfoDataMapper.swift
//  Ponno
//
//  Created by a k azad on 22/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ShopInfoDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var shopDetails : ShopDetails? 
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        shopDetails <- map["shop_details"]
    }
    
}
