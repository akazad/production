//
//  PaymentInfo.swift
//  Ponno
//
//  Created by a k azad on 30/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PaymentInfo : Mappable {
    var paymentStatus : Int = 0
    var expireDate : String = ""
    var invoiceNumber : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        paymentStatus <- map["payment_status"]
        expireDate <- map["expire_date"]
        invoiceNumber <- map["invoice_number"]
    }
    
}
