//
//  ReferralInfoDataMapper.swift
//  Ponno
//
//  Created by a k azad on 30/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ReferralInfoDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var referralInfo : ReferralInfo? 
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        referralInfo <- map["referral_info"]
    }
    
}

