//
//  ReferralList.swift
//  Ponno
//
//  Created by a k azad on 30/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ReferralList : Mappable {
    var id : Int = 0
    var name : String = ""
    var ownerName : String = ""
    var contact : String = ""
    var image : String = ""
    var expireDate : String = ""
    var paymentStatus : Int = 0
    var createdAt : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        ownerName <- map["owner_name"]
        contact <- map["contact"]
        image <- map["image"]
        expireDate <- map["expire_date"]
        paymentStatus <- map["payment_status"]
        createdAt <- map["created_at"]
    }
    
}
