//
//  ReferralInfo.swift
//  Ponno
//
//  Created by a k azad on 30/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ReferralInfo : Mappable {
    var referred : Int = 0
    var success : Int = 0
    var pending : Int = 0
    var expired : Int = 0
    var income : String = ""
    var received : String = ""
    var due : Int = 0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        referred <- map["referred"]
        success <- map["success"]
        pending <- map["pending"]
        expired <- map["expired"]
        income <- map["income"]
        received <- map["received"]
        due <- map["due"]
    }
    
}
