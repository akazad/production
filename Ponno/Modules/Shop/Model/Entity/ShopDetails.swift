//
//  ShopDetails.swift
//  Ponno
//
//  Created by a k azad on 22/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ShopDetails : Mappable {
    var name : String = ""
    var ownerName : String = ""
    var regNo : String = ""
    var address : String = ""
    var contact : String = ""
    var image : String = ""
    //var invoiceNote : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
        ownerName <- map["owner_name"]
        regNo <- map["reg_no"]
        address <- map["address"]
        contact <- map["contact"]
        image <- map["image"]
        //invoiceNote <- map["invoice_note"]
    }
    
}
