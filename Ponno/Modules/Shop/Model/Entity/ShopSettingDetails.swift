//
//  ShopSettingDetails.swift
//  Ponno
//
//  Created by a k azad on 30/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ShopSettingDetails : Mappable {
    var language : Int = 0
    var invoiceNote : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        language <- map["language"]
        invoiceNote <- map["invoice_note"]
    }
    
}
