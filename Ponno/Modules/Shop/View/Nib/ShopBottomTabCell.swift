//
//  ShopBottomTabCell.swift
//  Ponno
//
//  Created by a k azad on 31/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ShopBottomTabCell: UICollectionViewCell {

    @IBOutlet weak var backgroundContentView: UIView!{
        didSet{
            setUpCollectionCardView(uiview: backgroundContentView)
        }
    }
    
    @IBOutlet weak var icon: UIImageView!
    
    @IBOutlet weak var titleText: UILabel!
    
    var tabs : String?{
        didSet{
            if let tab = self.tabs{
                titleText.text = tab
            }
        }
    }
    var images : String?{
        didSet{
            if let image = self.images{ 
                icon.image = UIImage(named: image)
            }
        }
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: ShopBottomTabCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
