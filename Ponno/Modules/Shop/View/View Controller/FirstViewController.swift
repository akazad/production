//
//  FirstViewController.swift
//  SOF_SortArrayOfCustomObject
//
//  Created by WiOS Test User on 01/02/18.
//  Copyright © 2018 WiOS Test User. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

class FirstViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageIcon: CircularImageView!
    @IBOutlet weak var shopNameLbl: UILabel!{
        didSet{
            shopNameLbl.text = LanguageManager.ShopName
        }
    }
    @IBOutlet weak var shopNameTextField: UITextField!{
        didSet{
            shopNameTextField.placeholder = LanguageManager.ShopName
        }
    }
    @IBOutlet weak var ownerNameLbl: UILabel!{
        didSet{
            ownerNameLbl.text = LanguageManager.OwnerName
        }
    }
    @IBOutlet weak var ownerTextField: UITextField!
    @IBOutlet weak var registrationNumberLbl: UILabel!{
        didSet{
            registrationNumberLbl.text = LanguageManager.RegistrationNumber
        }
    }
    @IBOutlet weak var registrationTextField: UITextField!
    @IBOutlet weak var shopPhoneNumberLbl: UILabel!{
        didSet{
            shopPhoneNumberLbl.text = LanguageManager.ShopPhoneNumber
        }
    }
    @IBOutlet weak var shopPhoneNumberTextField: UITextField!
    @IBOutlet weak var addressLbl: UILabel!{
        didSet{
            addressLbl.text = LanguageManager.Address
        }
    }
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = ShopPresenter(service: ShopService())
    
    var shopDetails: ShopDetails?
    
    let imagePicker = UIImagePickerController()
    var selectedImage : UIImage?
    var message : String?
    var deliveryStatus : Int?
    var paymentStatus : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.setImageView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.attachPresenter()
    }

    func initialSetup(){
        
        imagePicker.delegate = self
        self.imageIcon.addTapGestureRecognizer(action: {self.onTapOnImageView()})
        
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        
        guard let details = self.shopDetails else{
            //self.containerView.isHidden = true
            return
        }
        
        imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.shopImage + details.image), placeholderImage: UIImage(named: "placeholder"))
        
        
        shopNameTextField.text = details.name
        ownerTextField.text = details.ownerName
        registrationTextField.text = details.regNo
        shopPhoneNumberTextField.text = details.contact
        addressTextField.text = details.address
        
    }
    
    func submitBtnControllWith(isEnabled: Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidate(){
            guard let name = shopNameTextField.text, let ownerName = ownerTextField.text, let registrationNumber = registrationTextField.text, let phone = shopPhoneNumberTextField.text,let address = addressTextField.text else{
                return
            }
            
            let param : [String : Any] = ["name": name, "owner_name": ownerName, "reg_no": registrationNumber, "contact": phone,  "address": address]
            
            self.presenter.postShopGeneralInfoUpdateDataToServer(param: param)
            
        }
    }
    
    func isValidate()->Bool{
        if shopNameTextField.text == ""{
            showAlert(title: LanguageManager.ShopNameIsRequired, message: "")
            shopNameTextField.becomeFirstResponder()
            return false
        }else if ownerTextField.text == "" {
            showAlert(title: LanguageManager.OwnerNameIsRequired, message: "")
            ownerTextField.becomeFirstResponder()
            return false
        }
//        else if registrationTextField.text == "" {
//            showAlert(title: "Registration number is empty", message: "")
//            registrationTextField.becomeFirstResponder()
//            return false
//        }
        guard let phone =  shopPhoneNumberTextField.text else{
            showAlert(title: LanguageManager.PhoneNumberIsRequired, message: "")
            shopPhoneNumberTextField.becomeFirstResponder()
            return false
        }
        if (!validatePhoneNumber(value: phone)){
            showAlert(title: LanguageManager.PhoneNumberIsIncorrect, message: "")
            return false
        }
//        else if addressTextField.text == "" {
//            showAlert(title: "Address is empty", message: "")
//            addressTextField.becomeFirstResponder()
//            return false
//        }
        return true
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    
}


//Mark: Api Delegate
extension FirstViewController : ShopViewDelegate{
    func onImageUpload(data: AddDataMapper) {
        self.successMessage(userMessage: self.message ?? "")
    }
    
    func setShopData(data: ShopInfoDataMapper) {
        guard let details = data.shopDetails else{
            return
        }
        self.shopDetails = details
        self.initialSetup()
        self.setImageView()
    }
    
    func onShopGeneralInfoUpdate(data: AddDataMapper) {
        
        guard let message = data.message else{
            return
        }
        
        guard let shopDetails = self.shopDetails else{
            return
        }
        
        if let shop = getShopData(){
            if let deliverySystemStatus = shop.deliverySystem, let payStatus = shop.paymentStatus {
                self.deliveryStatus = deliverySystemStatus
                self.paymentStatus = payStatus
                let inventoryStatus = shop.inventorySystem
                let accStatus = shop.accStatus
                let invoiceNote = shop.invoiceNote
                let category = shop.category
                
                if let name = shopNameTextField.text {
                    let shopData = ShopInfo(name: name, registrationNumber: shopDetails.regNo, address: shopDetails.address , image : shopDetails.image, inventorySystem: inventoryStatus, deliverySystem: self.deliveryStatus, invoiceNote: invoiceNote, paymentStatus: self.paymentStatus, accStatus: accStatus, category: category)
                    Ponno.setShopData(shopInfo: shopData)
                }
            }
        }
        
        self.successMessage(userMessage: message)
        
        guard let image = self.selectedImage else {
            return
        }
        self.presenter.postShopImageToServer(image: image)
        
    }
    
    func onShopLoginInfoUpdate(data: AddDataMapper) {
        
    }
    
    func onFailed(data: String) {
        self.containerView.isHidden = true
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.submitBtnControllWith(isEnabled: false)
        self.containerView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControllWith(isEnabled: true)
        self.containerView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getShopDataFromServer()
    }
}

//Mark: Image Picker
extension FirstViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    private func setImageView(){
        guard let imageUrl = self.shopDetails?.image else{
            self.setImageInImageView(imageUrl: "")
            return
        }
        self.setImageInImageView(imageUrl: ImageUrl.sharedImageInstance.shopImage + imageUrl)
    }
    
    func setImageInImageView(imageUrl : String){
        self.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageIcon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
    }
    
    func onTapOnImageView(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageIcon.contentMode = .scaleAspectFill
            imageIcon.image = pickedImage
            self.selectedImage = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
