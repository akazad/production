//
//  OwnerLogInInfoUpdateVC.swift
//  Ponno
//
//  Created by a k azad on 30/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class OwnerLogInInfoUpdateVC: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!{
        didSet{
            titleLbl.text = LanguageManager.LoginInformation
        }
    }
    @IBOutlet weak var mobileNumberLbl: UILabel!{
        didSet{
            mobileNumberLbl.text = LanguageManager.PhoneNumber
        }
    }
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var oldPasswordLbl: UILabel!{
        didSet{
            self.oldPasswordLbl.text = LanguageManager.OldPassword
        }
    }
    @IBOutlet weak var oldPasswordTextField: UITextField!{
        didSet{
            oldPasswordTextField.placeholder = LanguageManager.OldPassword
        }
    }
    @IBOutlet weak var newPasswordLbl: UILabel!{
        didSet{
            newPasswordLbl.text = LanguageManager.NewPassword
        }
    }
    @IBOutlet weak var newPasswordTextField: UITextField!{
        didSet{
            newPasswordTextField.placeholder = LanguageManager.NewPassword
        }
    }
    @IBOutlet weak var confirmPasswordLbl: UILabel!{
        didSet{
            confirmPasswordLbl.text = LanguageManager.ConfirmPassword

        }
    }
    @IBOutlet weak var confirmPasswordTextField: UITextField!{
        didSet{
            confirmPasswordTextField.placeholder = LanguageManager.ConfirmPassword
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = ShopPresenter(service: ShopService())
    
    var shopDetails: ShopDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        
        guard let details = self.shopDetails else {
            return
        }
        self.mobileNumberTextField.text = details.contact
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    func submitBtnControllWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidate(){
            guard let phone = mobileNumberTextField.text, let oldPassword = oldPasswordTextField.text, let password = newPasswordTextField.text, let confirmPassword = confirmPasswordTextField.text else{
                return
            }
            
            let param : [String : Any] = ["phone": phone, "password_old": oldPassword, "password": password, "password_confirmation": confirmPassword]
            
            self.presenter.postShopLoginInfoUpdateDataToServer(param: param)
            
        }
    }
    
    func isValidate()->Bool{
        
        guard let phone =  mobileNumberTextField.text else{
            showAlert(title: LanguageManager.PhoneNumberIsRequired, message: "")
            mobileNumberTextField.becomeFirstResponder()
            return false
        }
        if (!validatePhoneNumber(value: phone)){
            showAlert(title: LanguageManager.PhoneNumberIsIncorrect, message: "")
            return false
        }
        if oldPasswordTextField.text == "" {
            showAlert(title: LanguageManager.OldPasswordIsRequired, message: "")
            oldPasswordTextField.becomeFirstResponder()
            return false
        }else if newPasswordTextField.text == "" {
            showAlert(title: LanguageManager.NewPasswordIsRequired, message: "")
            newPasswordTextField.becomeFirstResponder()
            return false
        }else if confirmPasswordTextField.text == "" {
            showAlert(title: LanguageManager.ConfirmPasswordIsRequired, message: "")
            confirmPasswordTextField.becomeFirstResponder()
            return false
        }
        return true
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension OwnerLogInInfoUpdateVC : ShopViewDelegate{
    func onImageUpload(data: AddDataMapper) {
        
    }
    
    func setShopData(data: ShopInfoDataMapper) {
        guard let details = data.shopDetails else{
            return
        }
        self.shopDetails = details
        self.initialSetup()
    }
    
    func onShopGeneralInfoUpdate(data: AddDataMapper) {
        
    }
    
    func onShopLoginInfoUpdate(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.submitBtnControllWith(isEnabled: false)
        self.containerView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControllWith(isEnabled: true)
        self.containerView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getShopDataFromServer()
    }
}
