//
//  SegementedVC.swift
//  SOF_SortArrayOfCustomObject
//
//  Created by WiOS Test User on 01/02/18.
//  Copyright © 2018 WiOS Test User. All rights reserved.
//

import UIKit


class SegementedVC: BaseViewController {
 
    @IBOutlet weak var containerView    : UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var tabs = [LanguageManager.ShopInformation,LanguageManager.ShopSettings, LanguageManager.PaymentAndReferrals,LanguageManager.Promotion]
    var images : [String] = ["personIcon","settingsIcon","money","promotional"]
    
    
    var selectedTab = 0
    
    private lazy var summaryViewController: ShopInfoUpdateVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Shop", bundle: nil)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "ShopInfoUpdateVC") as! ShopInfoUpdateVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var sessionsViewController: SecondViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Shop", bundle: nil)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    
    private lazy var refferelViewController: ThirdViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Shop", bundle: nil)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "ThirdViewController") as! ThirdViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var promotionalViewController: PromotionalViewController = {
        let storyboard = UIStoryboard(name: "Shop", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "PromotionalViewController") as! PromotionalViewController
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    static func viewController() -> SegementedVC {
        return UIStoryboard.init(name: "Shop", bundle: nil).instantiateViewController(withIdentifier: "SegementedVC") as! SegementedVC
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    private func add(asChildViewController viewController: UIViewController) {
        
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        containerView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    //----------------------------------------------------------------
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
    //----------------------------------------------------------------
    
    private func updateView() {
        if let viewSettings = getViewSettings(){
            if viewSettings.settings == "FreemiumShop"{
                if self.selectedTab == 0 {
                    remove(asChildViewController: promotionalViewController)
                    add(asChildViewController: summaryViewController)
                }else{
                    remove(asChildViewController: summaryViewController)
                    add(asChildViewController: promotionalViewController)
                }
            }else{
                setupPremiumShopView()
            }
        }else{
            setupPremiumShopView()
        }
    }
    
    func setupPremiumShopView(){
        if self.selectedTab == 0 {
            remove(asChildViewController: sessionsViewController)
            add(asChildViewController: summaryViewController)
        } else if self.selectedTab == 1{
            remove(asChildViewController: summaryViewController)
            add(asChildViewController: sessionsViewController)
        } else{
            remove(asChildViewController: sessionsViewController)
            add(asChildViewController: refferelViewController)
        }
    }
    
    //----------------------------------------------------------------
    
    func setupView() {
        
        updateView()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.setupTabArray()
        self.setUpInitialLanguage()
        self.configureCollectionView()
    }
    
    func setupTabArray(){
        if let category = getBusinessCategory(){
            if category.businessCategory == 1{
                let ftab = tabs.filter{$0 != LanguageManager.ShopSettings && $0 != LanguageManager.PaymentAndReferrals}
                self.tabs = ftab
            }
        }
        if let category = getBusinessCategory(){
            if category.businessCategory == 1{
                let fimage = images.filter{$0 != "settingsIcon" && $0 != "money"}
                self.images = fimage
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupView()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
}
//Mark: CollectionView Delegate and DataSource
extension SegementedVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            if let category = getBusinessCategory(){
            if category.businessCategory == 1{
                    let itemWidth = (view.bounds.width - 2) / 2.0
                    layout.itemSize = CGSize(width: itemWidth, height: 70.0)
                }else{
                    let itemWidth = (view.bounds.width - 3) / 3.0
                    layout.itemSize = CGSize(width: itemWidth, height: 70.0)
                }
            }
            layout.invalidateLayout()
        }
    }
    
    func configureCollectionView(){
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(ShopBottomTabCell.nib, forCellWithReuseIdentifier: ShopBottomTabCell.identifier)
        self.automaticallyAdjustsScrollViewInsets = false
        self.collectionView.bounces = false
     //   self.cellLayoutSetup()
        self.collectionView.reloadData()
    }
    
    func cellLayoutSetup(){
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        let widthPerItem = (self.view.bounds.width ) / 3
        layout.itemSize = CGSize(width: widthPerItem, height: 70.0)
        layout.scrollDirection = .horizontal
        layout.invalidateLayout()
        collectionView.setCollectionViewLayout(layout, animated: true, completion: nil)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabs.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : ShopBottomTabCell = collectionView.dequeueReusableCell(withReuseIdentifier: ShopBottomTabCell.identifier, for: indexPath) as! ShopBottomTabCell
        
        if self.selectedTab == indexPath.item {
            cell.backgroundContentView.backgroundColor = UIColor.lightGreen
        }
        else{
            cell.backgroundContentView.backgroundColor = UIColor.white
        }
        let item = self.images[indexPath.row]
        let tab = self.tabs[indexPath.row]
        cell.images = item
        cell.tabs = tab
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedTab = indexPath.item
        self.collectionView.reloadData()
        self.updateView()
    }
    
}

//extension SegementedVC : TabChange{
//    func setTab(selectedTab: Int){
//        self.selectedTab = selectedTab
//        self.updateView()
//    }
//    
//    
//}
