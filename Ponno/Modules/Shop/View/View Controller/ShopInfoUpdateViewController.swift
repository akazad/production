//
//  ShopInfoUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 6/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class ShopInfoUpdateViewController: UIViewController {
    
    @IBOutlet var imageIcon: UIImageView!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var ownerName: UITextField!
    @IBOutlet weak var registrationNumber: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var oldPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = ShopUpdatePresenter(service: ShopService())
    
    let imagePicker = UIImagePickerController()
    var selectedImage : UIImage?
    var message : String?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.toolBarSetUp()
        self.attachPresenter()
        self.setUpViews()
        self.setImageView()
    }
    
    func initialSetup(){
        
        imagePicker.delegate = self
        self.imageIcon.addTapGestureRecognizer(action: {self.onTapOnImageView()})
        
        self.name.underlined()
        self.ownerName.underlined()
        self.registrationNumber.underlined()
        self.address.underlined()
        self.phone.underlined()
        self.oldPassword.underlined()
        self.newPassword.underlined()
        self.confirmPassword.underlined()
        if let shop = getShopData(){
            imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.shopImage + shop.image!), placeholderImage: UIImage(named: "placeholder"))
            name.text = shop.name
            registrationNumber.text = shop.registrationNumber
            address.text = shop.address
        }
        if let user = getUserData(){
            ownerName.text = user.name
            phone.text = user.phone
        }
        
    }
    
    func toolBarSetUp(){
        //amount toolBar
        let phoneToolBar = UIToolbar()
        phoneToolBar.barStyle = UIBarStyle.default
        phoneToolBar.isTranslucent = true
        phoneToolBar.tintColor = UIColor.black
        phoneToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnAdviseAmount(sender:)))
        
        phoneToolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        phoneToolBar.isUserInteractionEnabled = true
        
        self.phone.inputAccessoryView = phoneToolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnAdviseAmount(sender: UIBarButtonItem){
        self.phone.resignFirstResponder()
        self.oldPassword.becomeFirstResponder()
    }
    
    func setUpViews(){
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtn(sender:)), for: .touchUpInside)
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    @objc func onSubmitBtn(sender : UIButton){
        if self.isValidated(){
            
            
            guard let name = name.text, let ownerName = ownerName.text, let regNo = registrationNumber.text, let address = address.text, let phone = phone.text else{
                return
            }
            
            let params : [String : Any] = ["name" :name, "owner_name": ownerName,
                                           "reg_no": regNo,
                                           "address": address,
                                           "phone": phone, "old_password": "", "new_password": "", "language": "0"]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            self.presenter.postShopUpdateDataToServer(params: params)
        }
        
    }
    
    func isValidated()->Bool{
        if self.name.text == ""{
            showAlert(title: "Name can't be empty", message: "")
            return false
        }
        else if self.ownerName.text == ""{
            showAlert(title: "Owner name can't be empty", message: "")
            return false
        }
        else if self.registrationNumber.text == ""{
            showAlert(title: "Registration number can't be empty", message: "")
            return false
        }
        else if self.address.text == ""{
            showAlert(title: "Address can't be empty", message: "")
            return false
        }
        else if self.phone.text == ""{
            showAlert(title: "Phone can't be empty", message: "")
            return false
        }
        return true
    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: "Successful", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
                
                
                if let shop = getShopData(){
//                    let shopData = ShopInfo(name: shopInfo.name , address: shopInfo.address, registrationNumber: shopInfo.regNo, image : shopInfo.image)
//                    setShopData(shopInfo: shopData)
                    
                    self.name.text = shop.name
                    self.registrationNumber.text = shop.registrationNumber
                    self.address.text = shop.address
                    
                }
                
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }

}


//Mark: Api Delegate
extension ShopInfoUpdateViewController: ShopUpdateViewDelegate{
    func onImageUpload(data: AddDataMapper) {
        self.successMessage(userMessage: self.message ?? "")
    }
    
    func onFailed(data: String) {
        self.showAlert(title: "পুনরায় চেষ্টা করুণ", message: "")
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
        guard let image = self.selectedImage else {
            return
        }
        self.presenter.postShopImageToServer(image: image)
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

//Mark: Image Picker
extension ShopInfoUpdateViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    private func setImageView(){
        self.setImageInImageView(imageUrl: ImageUrl.sharedImageInstance.shopImage)
    }
    
    func setImageInImageView(imageUrl : String){
        self.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageIcon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
    }
    
    func onTapOnImageView(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageIcon.contentMode = .scaleAspectFill
            imageIcon.image = pickedImage
            self.selectedImage = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
