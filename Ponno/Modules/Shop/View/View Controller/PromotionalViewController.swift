//
//  PromotionalViewController.swift
//  Ponno
//
//  Created by a k azad on 19/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PromotionalViewController: UIViewController {

    @IBOutlet weak var contactBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
    }
    
    func initialSetup(){
        self.contactBtn.addTarget(self, action: #selector(onCallBtnTapped), for: .touchUpInside) 
    }
    
    @objc func onCallBtnTapped(sender: UIButton){
        self.makePhoneCall(phoneNumber: "01999088654")
    }

}
