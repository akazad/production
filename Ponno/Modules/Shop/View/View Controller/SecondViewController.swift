//
//  SecondViewController.swift
//  SOF_SortArrayOfCustomObject
//
//  Created by WiOS Test User on 01/02/18.
//  Copyright © 2018 WiOS Test User. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var deliverySwitch: UISwitch!
    @IBOutlet weak var deliveryBookLbl: UILabel!{
        didSet{
            deliveryBookLbl.text = LanguageManager.DeliveryBook
        }
    }
    @IBOutlet weak var purchaseBaseSwitch: UISwitch!
    @IBOutlet weak var purchaseBaseLbl: UILabel!{
        didSet{
            purchaseBaseLbl.text = LanguageManager.PurchaseBase
        }
    }
    @IBOutlet weak var productionBaseSwitch: UISwitch!
    @IBOutlet weak var productBaseLbl: UILabel!{
        didSet{
            productBaseLbl.text = LanguageManager.ProductionBase
        }
    }
    @IBOutlet weak var languageLbl: UILabel!{
        didSet{
            languageLbl.text =  LanguageManager.LanguageLbl + " :"
        }
    }
    @IBOutlet weak var languageChangeSegment: UISegmentedControl!{
        didSet{
            languageChangeSegment.setTitle(LanguageManager.BanglaTitle, forSegmentAt: 0)
            languageChangeSegment.setTitle(LanguageManager.EnglishTitle, forSegmentAt: 1)
        }
    }
    @IBOutlet weak var invoiceNoteLbl: UILabel!{
        didSet{
            invoiceNoteLbl.text = LanguageManager.InvoiceNote
        }
    }
    @IBOutlet weak var invoiceNote: UITextView!
    @IBOutlet weak var shopResetBtn: UIButton!{
        didSet{
            shopResetBtn.setTitle(LanguageManager.ClearShopData, for: .normal)
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = ShopSettingsPresenter(service: ShopService())
    
    var shopSettings : ShopSettingDetails?
    var deliveryStatus: Int?
    var inventorySystem: Int?
    var purchaseBaseStatus: Int?
    var productionBaseStatus: Int?
    var paymentStatus : Int?
    var parentValue: Int?
    var language : Int?
    var initialLanguage : Int?
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initialSetup()
        self.setupLanguage()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func setupLanguage(){
        if getLanguage(){
            self.languageChangeSegment.selectedSegmentIndex = 0
            self.language = 0
        }else{
            self.languageChangeSegment.selectedSegmentIndex = 1
            self.language = 1
        }
    }

    @IBAction func switchLanguage(_ sender: UISwitch) {
        if sender.isOn{
            setLanguage(isBangla: true)
        }
        else{
            setLanguage(isBangla: false)
        }
    }
    
    func reloadViewController(){
        let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let viewController: HomeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        //viewController.selectedTab = 1
        let navController = UINavigationController(rootViewController: viewController)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: {})
    }
    
    func initialSetup(){
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnPressed), for: .touchUpInside)
        self.shopResetBtn.addTarget(self, action: #selector(onShopResetPressed), for: .touchUpInside)
        self.deliverySwitch.addTarget(self, action: #selector(onDeliverySwitchChange), for: .touchUpInside)
        self.purchaseBaseSwitch.addTarget(self, action: #selector(onPurchaseBaseSwitchChange), for: .touchUpInside)
        self.productionBaseSwitch.addTarget(self, action: #selector(onProuductionBaseSwitchChange), for: .touchUpInside)
        self.languageChangeSegment.addTarget(self, action: #selector(onLanguageChange), for: .valueChanged)
        
        if let shop = getShopData(){
            guard  let deliverySystemStatus = shop.deliverySystem, let inventorySystem = shop.inventorySystem, let shopCateogry = shop.category else {
                return
            }
            //print("\(shop.category)")
            //let shopCateogry = 1
            self.deliveryStatus = deliverySystemStatus
            self.inventorySystem = inventorySystem
            
            if self.inventorySystem == 0 {
                if shopCateogry == 1 || shopCateogry == 5 || shopCateogry == 15{
                    self.hideInventorySystem(isHidden: true)
                }
                
                self.purchaseBaseStatus = 1
                self.productionBaseStatus = 0
            }else if self.inventorySystem == 1 {
                self.hideInventorySystem(isHidden: false)
                self.purchaseBaseStatus = 0
                self.productionBaseStatus = 1
            }else{
                self.hideInventorySystem(isHidden: false)
                self.purchaseBaseStatus = 1
                self.productionBaseStatus = 1
            }
            
        }
        
        if let user = getUserData(){
            guard let parent = user.parent else{
                return
            }
            let parentInt = Int(parent)
            self.parentValue = parentInt
        }
        
        if self.parentValue == 0 {
            self.shopResetBtn.isHidden = true
        }else{
            self.shopResetBtn.isHidden = false
        }
        
        if self.deliveryStatus == 1 {
            self.deliverySwitch.setOn(true, animated: false)
        }else{
            self.deliverySwitch.setOn(false, animated: false)
        }
        
        if self.inventorySystem == 0 {
            self.purchaseBaseSwitch.setOn(true, animated: false)
            self.productionBaseSwitch.setOn(false, animated: false)
        }else if self.inventorySystem == 1 {
            self.purchaseBaseSwitch.setOn(false, animated: false)
            self.productionBaseSwitch.setOn(true, animated: false)
        }else if self.inventorySystem == 2{
            self.purchaseBaseSwitch.setOn(true, animated: false)
            self.productionBaseSwitch.setOn(true, animated: false)
        }
        
        
        
    }
    
    func hideInventorySystem(isHidden: Bool){
        self.purchaseBaseLbl.isHidden = isHidden
        self.purchaseBaseSwitch.isHidden = isHidden
        self.productionBaseSwitch.isHidden = isHidden
        self.productBaseLbl.isHidden = isHidden
        
    }
    
    @objc func onLanguageChange(sender: UISegmentedControl){
        
        switch sender.selectedSegmentIndex {
        case 0:
            setLanguage(isBangla: true)
            self.language = 0
        case 1:
            setLanguage(isBangla: false)
            self.language = 1
        default:
            languageChangeSegment.selectedSegmentIndex = 0
        }
        
    }
    
    @objc func onDeliverySwitchChange(sender: UISwitch){
        if (deliverySwitch.isOn == true){
            self.deliveryStatus = 1
            self.deliverySwitch.setOn(true, animated: false)
        }else{
            self.deliveryStatus = 0
            self.deliverySwitch.setOn(false, animated: false)
        }
    }
    
    @objc func onPurchaseBaseSwitchChange(sender: UISwitch){
        if (purchaseBaseSwitch.isOn == true){
            //self.inventorySystem = 0
            self.purchaseBaseStatus = 1
            self.purchaseBaseSwitch.setOn(true, animated: false)
        }else{
            self.purchaseBaseStatus = 0
            //self.inventorySystem = 1
            self.purchaseBaseSwitch.setOn(false, animated: false)
        }
    }
    
    @objc func onProuductionBaseSwitchChange(sender: UISwitch){
        if (productionBaseSwitch.isOn == true){
            self.productionBaseStatus = 1
            //self.inventorySystem = 1
            self.productionBaseSwitch.setOn(true, animated: false)
        }else{
            self.productionBaseStatus = 0
            self.productionBaseSwitch.setOn(false, animated: false)
        }
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    @objc func onSubmitBtnPressed(sender: UIButton){
        if isValidated(){
//            if self.inventoryStatus == 1 && self.rawMaterialStatus == 1 {
//                self.inventoryStatus = 2
//            }
            
            if self.purchaseBaseStatus == 1 && self.productionBaseStatus == 1 {
                self.inventorySystem = 2
            }else if self.purchaseBaseStatus == 1 && self.productionBaseStatus == 0 {
                self.inventorySystem = 0
            }else if self.purchaseBaseStatus == 0 && self.productionBaseStatus == 1 {
                self.inventorySystem = 1
            }
            
            guard let invoiceNote = self.invoiceNote.text, let deliveryStatus = self.deliveryStatus, let inventoryStatus = self.inventorySystem, let language = self.language else{
                return
            }
            
            let param : [String : Any] = ["language": language, "delivery_system": deliveryStatus, "invoice_note": invoiceNote, "inventory_system": inventoryStatus]
            
            self.presenter.postShopSettingsUpdateDataToServer(param: param)
        }
    }
    
    @objc func onShopResetPressed(sender: UIButton){
        
        let shopId = getUserData()?.shopId
        guard let id = shopId else{ 
            return
        }
        let param : [String : Any] = ["id" : id]
        self.presenter.postClearShopDataToServer(param: param)
    }
    
    func isValidated()-> Bool{
        if self.purchaseBaseStatus == 0 && self.productionBaseStatus == 0{
            showAlert(title: LanguageManager.PurchaseBaseOrProductionBaseIsRequired, message: "")
            return false
        }
        return true
    }
   
}

//Mark: Api Delegate
extension SecondViewController : ShopSettingsViewDelegate{
    func clearShopData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.showAlert(title: LanguageManager.ClearShopData, message: message)
    }
    
    func getShopSettingsData(data: ShopSettingsDataMapper) {
        guard let details = data.shopSettingDetails else {
            return
        }
        self.shopSettings = details
        invoiceNote.text = details.invoiceNote
    }
    
    func onSettingsUpdate(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        if let shop = getShopData(){
            guard let name = shop.name, let address = shop.address, let regNo = shop.registrationNumber, let image = shop.image, let deliverySystemStatus = self.deliveryStatus, let payStatus = shop.paymentStatus, let inventoryStatus = self.inventorySystem, let accStatus = shop.accStatus, let invoiceNote = shop.invoiceNote, let category = shop.category else {
                return
            }
            self.paymentStatus = payStatus
            let shopData = ShopInfo(name: name, registrationNumber: regNo, address: address , image : image, inventorySystem: inventoryStatus, deliverySystem: deliverySystemStatus, invoiceNote: invoiceNote, paymentStatus: self.paymentStatus, accStatus: accStatus, category: category)
            
//            if language == 0 {
//                setLanguage(isBangla: true)
//            }else{
//                setLanguage(isBangla: false)
//            }
            
            //self.setupLanguage()
            //self.setUpInitialLanguage()
            
            
            Ponno.setShopData(shopInfo: shopData)
        }
        //self.showAlert(title: message, message: "")
        self.showLanguageChangeAlert(title: message, message: "")
    }
    
    func onFailed(data: String) {
        
       showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.backGroundView.isHidden = true
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.backGroundView.isHidden = false
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getShopSettingsDataFromServer()
    }
}

extension SecondViewController{
    func showLanguageChangeAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.reloadViewController()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}

