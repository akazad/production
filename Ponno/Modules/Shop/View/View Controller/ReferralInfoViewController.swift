//
//  ReferralInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 1/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ReferralInfoViewController: UIViewController {

    @IBOutlet weak var scrollViewBackGround: UIView!
    @IBOutlet weak var referredLbl: UILabel!
    @IBOutlet weak var successLbl: UILabel!
    @IBOutlet weak var pendingLbl: UILabel!
    @IBOutlet weak var expiryLbl: UILabel!
    @IBOutlet weak var totalIncomeTitleLbl: UILabel!{
        didSet{
            totalIncomeTitleLbl.text = LanguageManager.TotalIncome
        }
    }
    @IBOutlet weak var totalIncomeLbl: UILabel!
    @IBOutlet weak var totalAchiveTitleLbl: UILabel!{
        didSet{
            totalAchiveTitleLbl.text = LanguageManager.TotalReceived
        }
    }
    @IBOutlet weak var totalAchivedLbl: UILabel!
    @IBOutlet weak var currentDueTitleLbl: UILabel!{
        didSet{
            currentDueTitleLbl.text = LanguageManager.CurrentDue
        }
    }
    @IBOutlet weak var currentDueLbl: UILabel!
    
    private var presenter = ShopReferralPresenter(service: ShopService())
    
    var referralInfo : ReferralInfo?{
        didSet{
            guard let info = referralInfo else{
                return
            }
            referredLbl.text = "\(info.referred)" + " " + LanguageManager.Referred
            successLbl.text = "\(info.success)" + " " + LanguageManager.Successful
            pendingLbl.text = "\(info.pending)" + " " + LanguageManager.Pending
            expiryLbl.text = "\(info.expired)" + " " + LanguageManager.Expired
            totalIncomeLbl.text = "\(info.income)" + " " + Constants.currencySymbol
            totalAchivedLbl.text = "\(info.received)" + " " + Constants.currencySymbol
            currentDueLbl.text = "\(info.due)" + " " + Constants.currencySymbol
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        //self.title = "রেফারেলস"
        self.attachPresenter()
    }
    
}

//Mark: Api Delegate
extension ReferralInfoViewController : ShopReferralViewDelegate{
    func getRefferelInfo(data: ReferralInfoDataMapper) {
        guard let info = data.referralInfo else{
            return
        }
        self.referralInfo = info
    }
    
    func getRefferelList(data: ReferralListDataMapper) {
        
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.scrollViewBackGround.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.scrollViewBackGround.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getReferralInfoDataFromServer()
    }
    
}
