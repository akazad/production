//
//  ShopViewController.swift
//  Ponno
//
//  Created by a k azad on 1/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class ShopViewController: BaseViewController{
    
    @IBOutlet weak var tableView: UITableView!

    //private var presenter = LoginPresenter(service : LoginService())
    private var presenter = ShopPresenter(service: ShopService())
    
    var shopDetails: ShopDetails?

    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        //self.attachPresenter()
        self.setNavigationBarTitle()
        self.configureTableView()
        self.setBarButton()
    }
    
    func setNavigationBarTitle(){
        self.title = "দোকান ঘর"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarButton(){
        
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "edit"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onEdit(sender:)), for: UIControl.Event.touchUpInside) 
        button.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        button.widthAnchor.constraint(equalToConstant: 20).isActive = true
        button.heightAnchor.constraint(equalToConstant: 20).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func onEdit(sender: UIBarButtonItem){
        self.navigateToShopInfoUpdateVC()
    }
    
    func navigateToShopInfoUpdateVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Shop", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ShopInfoUpdateViewController") as! ShopInfoUpdateViewController
        //viewController.vendorDetails = vendorDetails
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ShopViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        //self.salesList = []
        //currentPage = 1
        //self.presenter.getDashboardDataFromServer()
        
        refreshControl.endRefreshing()
    }

}

//Mark: TableView Delegate And DataSource
extension ShopViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ShopDetailsCell.nib, forCellReuseIdentifier: ShopDetailsCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ShopDetailsCell = tableView.dequeueReusableCell(withIdentifier: ShopDetailsCell.identifier, for: indexPath) as! ShopDetailsCell
        cell.selectionStyle = .none
        if let shop = self.shopDetails {
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.shopImage + shop.image),  placeholderImage: UIImage(named: "placeholder"))
            cell.nameLabel.text = shop.name
            cell.addressLabel.text = shop.address
            cell.registrationNumberLabel.text = shop.regNo
            cell.ownerLabel.text = shop.ownerName
        }
        //cell.phoneLabel.text = shop.
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500.0
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
}

////Mark: Api Delegate
//extension ShopViewController : ShopViewDelegate{
//    func setShopData(data: ShopInfoDataMapper) {
//        guard let details = data.shopDetails else{
//            return
//        }
//        self.shopDetails = details
//        refreshTableView()
//    }
//
//    func onFailed(data: String) {
//        self.showAlert(title: "কোন তথ্য পাওয়া যায়নি", message: "")
//    }
//
//    func showLoading() {
//        self.showLoader()
//    }
//
//    func hideLoading() {
//        self.hideLoader()
//    }
//
//    func attachPresenter(){
//        self.presenter.attachView(viewDelegate: self)
//        self.presenter.getShopDataFromServer()
//    }
//}




