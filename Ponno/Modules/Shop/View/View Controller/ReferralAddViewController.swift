//
//  ReferralAddViewController.swift
//  Ponno
//
//  Created by a k azad on 1/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ReferralAddViewController: UIViewController {

    @IBOutlet weak var shopNameTextField: UITextField!{
        didSet{
            shopNameTextField.placeholder = LanguageManager.ShopName
        }
    }
    @IBOutlet weak var ownerNameTextField: UITextField!{
        didSet{
            ownerNameTextField.placeholder = LanguageManager.OwnerName
        }
    }
    @IBOutlet weak var shopCategoryTextField: UITextField!{
        didSet{
            shopCategoryTextField.placeholder = LanguageManager.ShopCategory
        }
    }
    @IBOutlet weak var contactNoTextField: UITextField!{
        didSet{
            contactNoTextField.placeholder = LanguageManager.PhoneNumber
        }
    }
    @IBOutlet weak var passwordTextField: UITextField!{
        didSet{
            passwordTextField.placeholder = LanguageManager.Password
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = ReferralAddPresenter(service: ShopService())
    
    var shopCategories : [ShopCategories]?
    var shopCategoryId : Int?
    var categoryPicker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpPickerView()
        self.attachPresenter()
        self.initialSetup()
    }
    
    func initialSetup(){
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnPressed), for: .touchUpInside)
    }
    
    @objc func onSubmitBtnPressed(sender: UIButton){
        if isValidated(){
            guard let name = shopNameTextField.text, let owner = ownerNameTextField.text, let shopCategoryId = self.shopCategoryId, let contactNumber = contactNoTextField.text, let password = passwordTextField.text else{
                return
            }
            
            let param : [String : Any] = ["name": name, "owner_name": owner, "category": shopCategoryId, "phone": contactNumber, "password": password]
            
            self.presenter.postShopReferralAddDataToServer(param: param)
            
        }
    }
    
    func isValidated() ->Bool{
        if (shopNameTextField.text?.isEmpty)!{
            showAlert(title: LanguageManager.ShopNameIsRequired, message: "")
            return false
        }else if (ownerNameTextField.text?.isEmpty)!{
            showAlert(title: LanguageManager.OwnerNameIsRequired, message: "")
            return false
        }else if (shopCategoryTextField.text?.isEmpty)!{
            showAlert(title: LanguageManager.ShopCategoryIsRequired, message: "")
            return false
        }else if (contactNoTextField.text?.isEmpty)!{
            showAlert(title: LanguageManager.PhoneNumberIsRequired, message: "")
            return false
        }else if (passwordTextField.text?.isEmpty)!{
            showAlert(title: LanguageManager.PasswordIsRequired, message: "")
            return false
        }
        guard let phone = contactNoTextField.text, phone != "" else{
            return false
        }
        if (!validatePhoneNumber(value: phone)){
            self.showAlert(title: LanguageManager.PhoneNumberIsIncorrect, message: "")
            return false
        }
        return true
    }
    
    
    func setUpPickerView(){
        
        //self.vendorListTextField.underlined()
        //self.vendorPayableAmountTextField.underlined()
        //self.vendorCurrentDueTextField.underlined()
        
        categoryPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.shopCategoryTextField.inputView = categoryPicker
        self.shopCategoryTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
}

//PickerViewDelegate
extension ReferralAddViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        guard let item = self.shopCategories, item.count > 0 else{
            return 0
        }
        return item.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard let item = self.shopCategories, item.count > 0 else{
            return ""
        }
        self.shopCategoryTextField.text = item[row].name
        return item[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let item = self.shopCategories, item.count > 0 else{
            return
        }
        self.shopCategoryTextField.text = item[row].name
        self.shopCategoryId = item[row].id
        
    }
    
    
    
}



//Mark: Api Delegate
extension ReferralAddViewController : ReferralAddViewDelegate{
    func setShopCategories(data: ShopCategoryDataMapper) {
        guard let category = data.categories, category.count > 0 else{
            return
        }
        self.shopCategories = category
    }
    
    func onreferralAdd(data: AddDataMapper) {
        guard let message = data.message else{ 
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        //showAlert(title: "দোকানের ক্যাটেগরি পাওয়া যায়নি", message: "")
    }
    
    func showLoading() {
        self.submitBtn.isEnabled = false
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtn.isEnabled = true
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getShopCategoriesFromServer()
    }
    
}


extension ReferralAddViewController{
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
