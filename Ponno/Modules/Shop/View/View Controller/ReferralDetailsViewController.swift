//
//  ReferralDetailsViewController.swift
//  Ponno
//
//  Created by a k azad on 1/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

class ReferralDetailsViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var referralAddBarBtn: UIBarButtonItem!
    
    private var presenter = ShopReferralDetailsPresenter(service: ShopService())
    
    var referralList : [ReferralList]?{
        didSet{
            self.refreshTableView()
        }
    }
    
    var filteredList : [ReferralList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    let get_all = true
    var referralId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.addFloaty()
        self.setUpSearchBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    @IBAction func referralAdd(_ sender: Any) {
        self.navigateToAddNewReferralViewController()
    }
    
    func initialSetup(){
//        self.referralAddBarBtn.addTar
    }
    
    func setBarButton(){
        
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        //button.setImage(UIImage(named: "search-2"), for: UIControl.State.normal)
        button.titleLabel?.text = "+ " + LanguageManager.NewReferral
        button.addTarget(self, action: #selector(onNewReferral(sender:)), for: UIControl.Event.touchUpInside) 
        button.frame = CGRect(x: 0, y: 0, width: 100, height: 24)
        button.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func onNewReferral(sender: UIButton){
        
    }
    
    func navigateToAddNewReferralViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Shop", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ReferralAddViewController") as! ReferralAddViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        let floaty = Floaty()
        floaty.buttonColor = UIColor.lightGreen
        
        let extraItem = FloatyItem()
        extraItem.icon = UIImage(named: "next")
        extraItem.buttonColor = UIColor.lightGreen
        extraItem.title = "+ " + LanguageManager.NewReferral
        extraItem.handler = { item in
            self.navigateToAddNewReferralViewController()
        }
        
        floaty.addItem(item: extraItem)
        self.view.addSubview(floaty)
    }

}

//MARK: SearchBar Delegate
extension ReferralDetailsViewController: UISearchBarDelegate{
    fileprivate func setUpSearchBar(){
        self.searchBar.delegate = self
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearchActive = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearchActive = false
        
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let list = self.referralList, list.count > 0 else{
            return
        }
        
        if list.count > 0 {
            if searchText == "" {
                self.isSearchActive = false
                return
            }
        }
        
        guard let textToSearch = searchBar.text else {
            return
        }
        
        filteredList = list.filter { (referralList : ReferralList) -> Bool in
            return referralList.name.lowercased().contains(textToSearch.lowercased()) ||
            referralList.ownerName.lowercased().contains(textToSearch.lowercased()) ||
            referralList.contact.lowercased().contains(textToSearch.lowercased())
        }
        
        if filteredList.count == 0 {
            isSearchActive = false
        }
        else {
            isSearchActive = true
        }
        
    }
}

//Mark: TableView Delegate and DataSource
extension ReferralDetailsViewController : UITableViewDelegate, UITableViewDataSource{

    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ReferralListCell.nib, forCellReuseIdentifier: ReferralListCell.identifier)
        self.tableView.separatorStyle = .none
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive{
            guard self.filteredList.count > 0 else{
                return 0
            }
            return filteredList.count
        }else{
            guard let list = self.referralList, list.count > 0 else{
                return 0
            }
            return list.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ReferralListCell = tableView.dequeueReusableCell(withIdentifier: ReferralListCell.identifier, for: indexPath) as! ReferralListCell

        cell.selectionStyle = .none
        
        if isSearchActive{
            guard self.filteredList.count > 0 else{
                return cell
            }
            let item = self.filteredList[indexPath.row]
            
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.shopImage + item.image), placeholderImage: UIImage(named: "placeholder"))
            
            cell.nameLbl.text = item.name
            cell.ownerNameLbl.text = item.ownerName
            cell.phoneLbl.text = item.contact
            let status = item.paymentStatus
            if status == 1 {
                cell.statusLbl.text = LanguageManager.Successful
            }else{
                cell.statusLbl.text = LanguageManager.Pending
            }
            return cell
        }else{
            guard let list = self.referralList, list.count > 0 else{
                return cell
            }
            let item = list[indexPath.row]
            
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.shopImage + item.image), placeholderImage: UIImage(named: "placeholder"))
            
            cell.nameLbl.text = item.name
            cell.ownerNameLbl.text = item.ownerName
            cell.phoneLbl.text = item.contact
            let status = item.paymentStatus
            if status == 1 {
                cell.statusLbl.text = LanguageManager.Successful
            }else{
                cell.statusLbl.text = LanguageManager.Pending
            }
            
            return cell
        }
       
       // return UITableViewCell()
    }

    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95.0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        var edit : Bool = false
        
        if isSearchActive{
            guard self.filteredList.count > 0 else{
                return false
            }
            let status = self.filteredList[indexPath.row].paymentStatus
            if status == 0 {
                edit = true
            }else{
                edit = false
            }
            return edit
        }else{
            guard let item = self.referralList, item.count > 0 else{
                return false
            }
            let status = item[indexPath.row].paymentStatus
            if status == 0 {
                edit = true
            }else{
                edit = false
            }
            return edit
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if isSearchActive{
            let delete = UITableViewRowAction(style: .normal, title: LanguageManager.Delete) { action, index in
                guard self.filteredList.count > 0 else{
                    return
                }
                let item = self.filteredList[indexPath.row]
                self.referralId = item.id
                guard let id = self.referralId else{
                    return
                }
                self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
            }
            delete.backgroundColor = UIColor.red
            
            return [delete]
        }else{
            let delete = UITableViewRowAction(style: .normal, title: LanguageManager.Delete) { action, index in
                guard let list = self.referralList, list.count > 0 else{
                    return
                }
                let item = list[indexPath.row]
                self.referralId = item.id
                guard let id = self.referralId else{
                    return
                }
                self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
            }
            delete.backgroundColor = UIColor.red
            
            return [delete]
        }
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Delete, message: userMessage, preferredStyle: .alert)
            
        let OKAction = UIAlertAction(title: LanguageManager.Yes, style: .default){
                (action:UIAlertAction!) in
                
                let param : [String : Any] = ["id": deleteId]
                
                self.presenter.postReferralRemoveDataToServer(param: param)
            }
        let cancelAction = UIAlertAction(title: LanguageManager.No, style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }


}

//Mark: Api Delegate
extension ReferralDetailsViewController : ShopReferralDetailsViewDelegate{
    func postReferralRemoveData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayDeleteMessage(userMessage: message)
    }
    
    func getRefferelList(data: ReferralListDataMapper) {
        guard let list = data.referralList, list.count > 0 else{
            return
        }
        self.referralList = list
    }

    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }

    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }

    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }

    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getReferralDetailsDataFromServer(getAll: get_all)
    }

}

extension ReferralDetailsViewController {
    func displayDeleteMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.referralList = []
                    self.refreshTableView()
                self.presenter.getReferralDetailsDataFromServer(getAll: self.get_all)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}
