//
//  PreOrderAddViewController.swift
//  Ponno
//
//  Created by a k azad on 28/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol PreOrderableProductDelegate : NSObjectProtocol{
    func setProductData(name : String, amount : String, buyingPrice : String, sellingPrice : String)
}

class PreOrderAddViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!{
        didSet{
            titleLbl.text = LanguageManager.ProductInfo
        }
    }
    @IBOutlet weak var quantityLbl: UILabel!{
        didSet{
            quantityLbl.text = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var buyingPriceLbl: UILabel!{
        didSet{
            buyingPriceLbl.text = LanguageManager.BuyingPrice
        }
    }
    @IBOutlet weak var quantity: UITextField!{
        didSet{
            quantity.placeholder = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var quantitySerialTextField: UITextField!{
        didSet{
            quantitySerialTextField.placeholder = LanguageManager.UseCommaToSeparateSerial
        }
    }
    @IBOutlet weak var quantitySerialTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var buyingPrice: UITextField!{
        didSet{
            buyingPrice.placeholder = LanguageManager.BuyingPrice
        }
    }
    @IBOutlet weak var sellingPriceLbl: UILabel!{
        didSet{
            sellingPriceLbl.text = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var sellingPrice: UITextField!{
        didSet{
            sellingPrice.placeholder = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    
    var delegate : PreOrderableProductDelegate?
    var preOrderEditDelegate : PreOrderProductEditDelegate?
    var updateDelegate : PreOrderProductUpdatelegate?
    var updateSerialsDelegate : productSerialsUpdateDelegate?
    var summaryUpdateDelegate : PreOrderSummaryUpdateDelegate?
    
    var pageType : ActionType = .Add
    var productID : Int?
    var productInfo : PreOrderProductData?
    var productDetails : PreOrderDetailsData?
    var productList : ProductList?
    var productName : String?
    //var id : Int?
    var hasSerial : Int?
    var serails :  [String]?
    
    var serialString : String?
    
    var id : Int?
    var total : String?
    var purchaseId : Int?
    var orderDate : String?
    var receivedDate : String?
    var advance : String?
    var payable : String?
    var paid : String?
    var cashBack : String = "0.0"
    var vendorId : Int?
    var vendorName: String?
    var remarks : String?
    
    enum ActionType : Int{
        case Add
        case Edit
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpViews()
        self.toolBarSetUp()
        self.initialSetUp()
        self.configureTextFiled()
        
    }
    
    func initialSetUp(){
        
        
        
        if pageType == .Add{
//            self.title = LanguageManager.NewPreOrder
            self.quantitySerialTextFieldHeight.constant = 0.0
            self.hasSerial = 0
            if let productInfo = self.productList{
                self.buyingPrice.text = productInfo.buyingPrice
                self.sellingPrice.text = productInfo.sellingPrice
            }
        }else{
            self.title = LanguageManager.Update
            self.titleLbl.text = LanguageManager.UpdateInfo
            self.quantitySerialTextField.addTarget(self, action: #selector(onSerialChange), for: .editingChanged)
            
            self.detailsSetup()
            
            if let productInfo = self.productInfo {
                self.quantity.text = productInfo.quantity
                self.buyingPrice.text = productInfo.buyingPrice
                self.sellingPrice.text = productInfo.sellingPrice
                self.productName = productInfo.productName
                self.productID = productInfo.productId
                self.id = productInfo.id
                
                if let has_Serial = self.hasSerial {
                    if has_Serial == 0 {
                        self.quantitySerialTextFieldHeight.constant = 0.0
                    }else{
                        self.quantitySerialTextField.text = productInfo.serialArray
                    }
                }
            }
        }
    }
    
    func detailsSetup(){
        guard let details = self.productDetails else{
            return
        }
        self.id = details.id
        self.total = details.total
        self.purchaseId = details.purchaseId
        self.orderDate = details.orderDate
        self.receivedDate = details.receivedDate
        self.advance = details.advance
        self.paid = details.paid
        self.vendorId = details.vendorId
        self.payable = details.payable
        self.vendorName = details.vendorName
        self.remarks = details.remarks
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext), for: .touchUpInside)
    }
    
    @objc func onSerialChange(sender: UITextField){
        self.refreshQuantityTextField()
    }
    
    func refreshQuantityTextField(){
        guard let serial = self.quantitySerialTextField.text else{
            return
        }
        let serialList = self.prepareSerialNumbers(serials: serial)
        //self.serails = serialList
        self.serialString = serial
        print (serialList)
        let count = serialList.count
        self.quantity.text = "\(count)"
    }
    
    
//    fileprivate func prepareSerialNumbers(serials : String)->[String]{
//        guard serials != "" else {
//            return []
//        }
//        let serialList = serials.components(separatedBy: ",")
//
//        guard serialList.count > 0 else {
//            return []
//        }
//        return serialList
//    }
    
//    fileprivate func prepareSerialNumbers(serials : String)->String{
//        guard serials != "" else {
//            return ""
//        }
//        var serials = serials.append(separatedBy: ",")
//
//        guard serialList.count > 0 else {
//            return ""
//        }
//        return serials
//    }
    
    func toolBarSetUp(){
        //QuantityToolBar
        let quantityToolBar = UIToolbar()
        quantityToolBar.barStyle = UIBarStyle.default
        quantityToolBar.isTranslucent = true
        quantityToolBar.tintColor = UIColor.black
        quantityToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
        
        quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        quantityToolBar.isUserInteractionEnabled = true
        
        self.quantity.inputAccessoryView = quantityToolBar
        
        //BuyingPriceToolBar
        let buyingPriceToolBar = UIToolbar()
        buyingPriceToolBar.barStyle = UIBarStyle.default
        buyingPriceToolBar.isTranslucent = true
        buyingPriceToolBar.tintColor = UIColor.black
        buyingPriceToolBar.sizeToFit()
        
        let buyingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnBuyingPrice(sender:)))
        
        buyingPriceToolBar.setItems([cancelButton, spaceButton, buyingPriceDoneButton], animated: false)
        buyingPriceToolBar.isUserInteractionEnabled = true
        
        self.buyingPrice.inputAccessoryView = buyingPriceToolBar
        
        //SellingPriceToolBar
        let sellingPriceToolBar = UIToolbar()
        sellingPriceToolBar.barStyle = UIBarStyle.default
        sellingPriceToolBar.isTranslucent = true
        sellingPriceToolBar.tintColor = UIColor.black
        sellingPriceToolBar.sizeToFit()
        
        let sellingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnSellingPrice(sender:)))
        
        sellingPriceToolBar.setItems([cancelButton, spaceButton, sellingPriceDoneButton], animated: false)
        sellingPriceToolBar.isUserInteractionEnabled = true
        
        self.sellingPrice.inputAccessoryView = sellingPriceToolBar
    }
    
    @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
        self.quantity.resignFirstResponder()
        self.buyingPrice.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnBuyingPrice(sender: UIBarButtonItem){
        self.buyingPrice.resignFirstResponder()
        self.sellingPrice.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnSellingPrice(sender: UIBarButtonItem){
        self.sellingPrice.resignFirstResponder()
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onNext(sender : UIButton){
        if self.isValidated(){
            guard let quantity = self.quantity.text, let buyingprice = self.buyingPrice.text, let sellingPrice = self.sellingPrice.text, let name = self.productName,let productId = self.productID else{
                return
            }
            
            if self.pageType == .Edit{
                guard let hasSerial = self.hasSerial else{
                    return
                }
                
                let serials = self.quantitySerialTextField.text ?? ""
                //let product = PreOrderProductData(id: productId, name: name, quantity: quantity, buyingPrice: buyingprice, sellingPrice: sellingPrice, hasSerial: hasSerial)
                let product = PreOrderProductData(id: productId, name: name, quantity: quantity, buyingPrice: buyingprice, sellingPrice: sellingPrice, hasSerial: hasSerial, serialArray: serials)
                self.updateDelegate?.updateProduct(product: product)
                
//                if hasSerial == 1 {
//                    guard let serials = self.serialString else{
//                        return
//                    }
//                    self.updateSerialsDelegate?.updateSerials(productId: productId, serials: serials)
//                }
                self.navigationController?.popViewController(animated: true)
            }
            else{
                
                guard let hasSerial = self.hasSerial else{
                    return
                }
                
                let serials = self.quantitySerialTextField.text ?? ""
                
                let product = PreOrderProductData(id: productId, name: name, quantity: quantity, buyingPrice: buyingprice, sellingPrice: sellingPrice,hasSerial: hasSerial, serialArray : serials)

                self.preOrderEditDelegate?.addProduct(product: product)
                
                delegate?.setProductData(name: name, amount: quantity, buyingPrice: buyingprice, sellingPrice: sellingPrice)
//                let productQuantity = Double(quantity)
//                let productBuyingPrice = Double(buyingprice)
//                let advanceAmount = Double(advance)
//                let payableAmount = Double(payable)
//
//                guard let quan = productQuantity, let buying = productBuyingPrice, let adv = advanceAmount, var payab = payableAmount  else{
//                    return
//                }
//
//                let totalAmount = quan * buying
//
//
//                total = total + totalAmount
//                payab = total - adv
//
//                let summary = PreOrderDetailsData(total: "\(total)", purchaseId: purchaseId, orderDate: orderDate, receivedDate: receiveDate, advance: advance, payable: "\(payab)", paid: paid, cashBack: cashBack, vendorId: vendorId, vendorName: vendorName, remarks: remarks)
//
//                self.summaryUpdateDelegate?.updateSummary(details: summary)
            
                self.navigationController?.popViewController(animated: true)
                //self.popToSpecifiedController()
                //navigateToPreOrderPendingVC()
            }
        }
    }
    
    func popToSpecifiedController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is PreOrderPendingViewController {
            self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func navigateToPreOrderPendingVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PreOrderPendingViewController") as! PreOrderPendingViewController
//        self.type = 0
//        viewController.postType = type
//        viewController.customerId = customerId
//        viewController.dueData = data
//        viewController.dueState = DueState.dueEdit.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func isValidated()->Bool{
        if self.quantity.text == ""{
            self.showAlert(title: LanguageManager.QuantityIsRequired, message: "")
            return false
        }else if hasSerial == 1{
            if self.quantity.text == "\(0)"{
                self.showAlert(title: LanguageManager.SerialNumberIsRequired, message: "")
                return false
            }
        }
        else if self.sellingPrice.text == ""{
            self.showAlert(title: LanguageManager.SellingPriceIsRequired, message: "")
            return false
        }
        else if self.buyingPrice.text == ""{
            self.showAlert(title: LanguageManager.BuyingPriceIsRequired, message: "")
            return false
        }
        return true
    }

}

//Mark: TextField Delegate
extension PreOrderAddViewController : UITextFieldDelegate {
    func configureTextFiled(){
        self.quantity.delegate = self
        self.buyingPrice.delegate = self
        self.sellingPrice.delegate = self
        self.quantitySerialTextField.delegate = self
        self.quantity.underlined()
        self.buyingPrice.underlined()
        self.sellingPrice.underlined()
        self.quantitySerialTextField.underlined()
    }
}
