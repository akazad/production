//
//  PreOrderAddInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 28/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PreOrderAddInfoViewController: UIViewController {
    
    @IBOutlet weak var totalAmountLbl: UILabel!{
        didSet{
            totalAmountLbl.text = LanguageManager.TotalAmount
        }
    }
    @IBOutlet weak var totalAmountTextField: UITextField!{
        didSet{
            totalAmountTextField.placeholder = LanguageManager.TotalAmount
        }
    }
    @IBOutlet weak var advanceLbl: UILabel!{
        didSet{
            advanceLbl.text = LanguageManager.AdvanceAmount
        }
    }
    @IBOutlet weak var advanceTextField: UITextField!{
        didSet{
            advanceTextField.placeholder = LanguageManager.AdvanceAmount
        }
    }
    @IBOutlet weak var amountToBePaidLbl: UILabel!{
        didSet{
            amountToBePaidLbl.text = LanguageManager.AmountToBePaid
        }
    }
    @IBOutlet weak var amountToBePaidTextField: UITextField!{
        didSet{
            amountToBePaidTextField.placeholder = LanguageManager.AmountToBePaid
        }
    }
    @IBOutlet weak var orderDateLbl: UILabel!{
        didSet{
            orderDateLbl.text = LanguageManager.OrderDate
        }
    }
    @IBOutlet weak var orderDateTextField: UITextField!{
        didSet{
            orderDateTextField.placeholder = "dd/mm/yyyy"
        }
    }
    @IBOutlet weak var receiveDateLbl: UILabel!{
        didSet{
            receiveDateLbl.text = LanguageManager.ReceiveDate
        }
    }
    @IBOutlet weak var receiveDateTextField: UITextField!{
        didSet{
            receiveDateTextField.placeholder = "dd/mm/yyyy"
        }
    }
    @IBOutlet weak var remarksLbl: UILabel!{
        didSet{
            remarksLbl.text = LanguageManager.Remarks
        }
    }
    @IBOutlet weak var remarksTextField: UITextField!{
        didSet{
            remarksTextField.placeholder = LanguageManager.Remarks
        }
    }
    @IBOutlet weak var vendorLbl: UILabel!{
        didSet{
            vendorLbl.text = LanguageManager.Vendor
        }
    }
    @IBOutlet weak var vendorTextField: UITextField!{
        didSet{
            vendorTextField.placeholder = LanguageManager.SelectOne
        }
    }
    @IBOutlet weak var vendorAddBtn: UIButton!{
        didSet{
            vendorAddBtn.setTitle(LanguageManager.NewVendorAdd, for: .normal)
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = PreOrderAddInfoPresenter(service: PurchaseService())
    
    var preOrders : PreOrders?
    var selectedProductId : [Int] = []
    var selectedAmounts : [String] = []
    var selectedBuyingPrices : [String] = []
    var selectedSellingPrices : [String] = []

    var datePicker = UIDatePicker()
    var vendorsListPicker = UIPickerView()
    var vendorsId : Int?
    var getAll : Bool = true
    
    var vendorsList : [VendorsList] = []{
        didSet{
            self.vendorsListPicker.reloadAllComponents()
        }
    }
    
    var startDate = Date()
    var receiveDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetup()
        self.showOrderDatePicker()
        self.showReceiveDatePicker()
        self.setUpPickerView()
        self.attachPresenter()
    }
    
    func initialSetup(){
        
        self.advanceTextField.text = "0" 
        self.amountToBePaidTextField.text = "0"
        self.amountToBePaidTextField.isEnabled = false
        self.orderDateTextField.isEnabled = false
        
        self.vendorAddBtn.addTarget(self, action: #selector(onVendorAddBtnTap), for: .touchUpInside)
        
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        orderDateTextField.text = formatter.string(from: Date())
        
//        let date = Calendar.current.date(byAdding: .day, value: 7, to: Date())
//        guard let dateString = date else{
//            return
//        }
        receiveDateTextField.text = formatter.string(from: Date())
        
        //self.orderDateTextField.text = Date()
        
        guard self.selectedAmounts.count > 0 else {
            return
        }
        guard self.selectedBuyingPrices.count > 0 else {
            return
        }
        var totalPrice = 0.0
        
        for index in 0..<selectedAmounts.count {
            guard let amount = Double(self.selectedAmounts[index]), let buyingPrice = Double(self.selectedBuyingPrices[index])else{
                return
            }
            totalPrice += amount * buyingPrice
        }
        totalAmountTextField.text = "\(totalPrice)"
        
//        guard let preOrderInfo = self.preOrders else{
//            return
//        }
//
//        self.orderDateTextField.text = preOrderInfo.orderDate
//        self.receiveDateTextField.text = preOrderInfo.receivedDate
        
        //self.totalAmountTextField.addTarget(self, action: #selector(onTotalAmountChange), for: .editingChanged)
        self.advanceTextField.addTarget(self, action: #selector(onAdvanceTextChange), for: .editingChanged)
        
        
    }
    
//    @objc func onTotalAmountChange(sender: UITextField){
//        self.refreshView()
//    }
    
    @objc func onAdvanceTextChange(sender: UITextField){
        var maxAdvanceAmount : Double = 0
        var amount : Double = 0.0
        
        guard let totalAmount = self.totalAmountTextField.text?.toDouble() else{
            return
        }
        
        if let advanceAmount = self.advanceTextField.text?.toDouble(){
            if advanceAmount > totalAmount{
                maxAdvanceAmount = totalAmount
                self.advanceTextField.text = "\(maxAdvanceAmount)"
            }
            
            if totalAmount > advanceAmount {
                amount = totalAmount - advanceAmount
            }else{
                amount = 0.0
            }
            self.amountToBePaidTextField.text = "\(amount)"
        }
        
        
        
        //self.amountToBePaidTextField.text = "\(amount)"
        
        //self.refreshView()
    }
    
    @objc func onVendorAddBtnTap(sender: UIButton){
        self.alertWithTextField()
    }
}

extension PreOrderAddInfoViewController {
    func showOrderDatePicker(){
        datePicker.datePickerMode = .date
        //datePicker.maximumDate = Date()
        datePicker.minimumDate = Date()
        //datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        orderDateTextField.inputAccessoryView = toolbar
        orderDateTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        orderDateTextField.text = formatter.string(from: datePicker.date)
        self.startDate = datePicker.date
        receiveDateTextField.becomeFirstResponder()
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func showReceiveDatePicker(){
        datePicker.datePickerMode = .date
        //datePicker.maximumDate = Date()
        datePicker.minimumDate = Date()
        //datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: 1, to: Date())
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(receiveDoneDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        receiveDateTextField.inputAccessoryView = toolbar
        receiveDateTextField.inputView = datePicker
    }
    
    @objc func receiveDoneDatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        receiveDateTextField.text = formatter.string(from: datePicker.date)
        self.receiveDate = datePicker.date
        remarksTextField.becomeFirstResponder()
    }

}

extension PreOrderAddInfoViewController {
    func refreshView(){
        guard let totalText = self.totalAmountTextField.text, let total = Double(totalText), let advanceText = self.advanceTextField.text, let advance = Double(advanceText) else {
            return
        }
        var amount : Double = 0.0
        if total > advance {
            amount = total - advance
            
        }else{
            amount = 0.0
        }
        self.amountToBePaidTextField.text = "\(amount)"
        
        
    }
}

//Mark: PickerViewDelegate
extension PreOrderAddInfoViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.vendorsListPicker{
            return vendorsList.count
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if self.vendorsList.count > 0 {
            let list = self.vendorsList[row]
            self.vendorTextField.text = list.name
            self.vendorsId = list.id
            return list.name
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if self.vendorsList.count > 0 {
            let list = self.vendorsList[row]
            self.vendorTextField.text = list.name
            self.vendorsId = list.id
        }
    }
}

extension PreOrderAddInfoViewController {
    //PickerView
    func setUpPickerView(){
        
        vendorsListPicker.delegate = self
        
        let vendorsListToolBar = UIToolbar()
        vendorsListToolBar.barStyle = UIBarStyle.default
        vendorsListToolBar.isTranslucent = true
        vendorsListToolBar.tintColor = UIColor.black
        vendorsListToolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        vendorsListToolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        vendorsListToolBar.isUserInteractionEnabled = true
        
        self.vendorTextField.inputView = vendorsListPicker
        self.vendorTextField.inputAccessoryView = vendorsListToolBar
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onSubmit(sender : UIButton){
        if isValidated(){
            guard self.selectedProductId.count > 0 else {return}
            guard self.selectedAmounts.count > 0 else {return}
            guard self.selectedBuyingPrices.count > 0 else {return}
            guard self.selectedSellingPrices.count > 0 else {return}
            
            guard let totalText = totalAmountTextField.text, let total = Double(totalText), let advanceText = advanceTextField.text, let advance = Double(advanceText), let payableText = amountToBePaidTextField.text, let payable = Double(payableText), let orderDate = orderDateTextField.text, let receiveDate = receiveDateTextField.text, let remarks = remarksTextField.text, let id = self.vendorsId else{
                return
            }
            
            let param : [String : Any] = ["products": self.selectedProductId, "quantity": self.selectedAmounts, "buying_price" : self.selectedBuyingPrices, "selling_price" : self.selectedSellingPrices, "total": total, "advance": advance, "payable" : payable, "order_date" : orderDate, "receive_date" : receiveDate, "remarks": remarks, "vendor_id": id]
            
            self.presenter.postPreOrdersAddDataToServer(param: param)
        }
    }
    
    func isValidated()->Bool{
        if self.vendorTextField.text == ""{
            showAlert(title: LanguageManager.VendorNameIsRequired, message: "")
            return false
        }
//        if receiveDate.isSmallerThan(startDate){
//            showAlert(title: "", message: <#T##String#>)
//        }
        //let isSmaller = startDate.isSmallerThan
        return true
    }
}

//Mark: Api Delegate
extension PreOrderAddInfoViewController: preOrderAddInfoViewDelegate{
    func postNewVendorData(data: VendorAddDataMapper) {
        guard let vendor = data.vendor else {
            return
        }
        self.vendorsId = vendor.id
        self.vendorTextField.text = vendor.name
        self.vendorTextField.resignFirstResponder()
        guard let message = data.message else{
            return
        }
        self.showAlert(title: message, message: "")
    }
    
    func setVendorsData(data: VendorsDataMapper) {
        guard let list = data.vendors, list.count > 0 else {
            return
        }
//        let newVendor = VendorsList(id: 0, name: "নতুন ডিলার/কোম্পানি", address: "", phone: "")
//        list.insert(newVendor, at: 0)
        self.vendorsList += list
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getVendorsDataFromServer(getAll: self.getAll)
        
    }
    
}

extension PreOrderAddInfoViewController{
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.popToSpecificViewController()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is PreOrderListViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }

    }
    
//    func navigateToPreOrderListVC(){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "PreOrderListViewController") as! PreOrderListViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
}


extension PreOrderAddInfoViewController{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewVendorAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.VendorName
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.PhoneNumber
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
           // self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showMessage(userMessage: LanguageManager.VendorNameIsRequired)
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                self.showMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
                return
            }
            
//            if(!self.validatePhoneNumber(value: phone)){
//                self.showMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
//            }
            
            let param : [String : Any] = ["name": textField.text!, "phone": textField2.text!, "address": ""]
            
            self.presenter.postNewVendorDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.alertWithTextField()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)

    }
    
}
