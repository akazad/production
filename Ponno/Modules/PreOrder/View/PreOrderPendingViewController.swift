//
//  PreOrderPendingViewController.swift
//  Ponno
//
//  Created by a k azad on 2/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol PreOrderProductAddDelegate : NSObjectProtocol {
    func addProducts(product : [PreOrderProductData]?)
}

protocol PreOrderProductEditDelegate : NSObjectProtocol {
    func addProduct(product : PreOrderProductData?)
}

protocol PreOrderSummaryUpdateDelegate : NSObjectProtocol {
    func updateSummary(details: PreOrderDetailsData?)
}

protocol PreOrderProductUpdatelegate : NSObjectProtocol {
    func updateProduct(product : PreOrderProductData?)
}

protocol productSerialsUpdateDelegate : NSObjectProtocol {
    func updateSerials(productId: Int?, serials : String?)
}

//protocol OrderedProductUpdateDelegate: NSObjectProtocol {
//    func setOrderedProductUpdate(name: String, amount: Double, buyingPrice: Double, sellingPrice: Double)
//}


class PreOrderPendingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var updateBtn: UIButton!{
        didSet{
            updateBtn.setTitle(LanguageManager.Update, for: .normal)
        }
    }
    @IBOutlet weak var receiveBtn: UIButton!{
        didSet{
            receiveBtn.setTitle(LanguageManager.Receive, for: .normal)
        }
    }
    
    let presenter = PreOrderUpdatePresenter(service: PurchaseService())
    
    var preorder : PreOrderDetailsData?
    var products : [PreOrderProductData]?
    var preOrderId : Int?
    var productId: Int?
    var vendorId : Int?
    var paidAmount : Double = 0.0
    var payableAmount : Double = 0.0
    var amountToBePaid : Double = 0.0
    var cashBack : Double = 0.0
    //var cashBack : Double = 0.0
    var hasSerial : Int?
    var serialArray : String?
    var serials : [String]?
    var summaryUpdateDelegate : PreOrderSummaryUpdateDelegate!
    var totalAmount = 0.0
    
    
    var id: Int?
    var total : String?
    var purchaseId : Int?
    var orderDate : String?
    var receivedDate : String?
    var advance : String?
    var payable : Double?
    var payableText : String?
    var amountToPay : Double?
    var paidAmountText : String?
    //var paidAmount : Double?
    var advanceAmount : Double?
    var cashBackText : String?
    //var vendorId : Int?
    var vendorName: String?
    var remarks : String?
    
    enum Sections : Int{
        case Summary
        case Product
        case AddProduct
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.initialSetup()
        
        self.attachPresenter()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        //self.attachPresenter()
    }
    
    func initialSetup(){
        //self.title = LanguageManager.Details"Details"
        self.serials = [""]
    }
    
    func objectInitialization(){
        guard let details = self.preorder else {
            return
        }
        self.id = details.id
        self.total = details.total
        self.purchaseId = details.purchaseId
        self.orderDate = details.orderDate
        self.receivedDate = details.receivedDate
        self.advance = details.advance
        self.cashBackText = details.cashBack
        self.paidAmountText = details.paid
        self.payableText = details.payable
        //self.cashBack = Double(details.cashBack)
        self.vendorId = details.vendorId
        self.vendorName = details.vendorName
        self.remarks = details.remarks
    }
    
    @IBAction func updatePreorderAction(_ sender: UIButton) {
        
        self.updateSummaryInfo()
        
        guard let preOrderId = self.preOrderId else {
            return
        }
        
        guard let vendorId = self.vendorId else{
            return
        }
        
        guard let product = self.products, product.count > 0 else{
            return
        }
        
        var products : [String] = []
        var quantity : [String] = []
        var buyingPrice : [String] = []
        var sellingPrice : [String] = []
        var serialNumber : [String] = []
        
        for prod in product{
            products  += [String(describing: prod.productId)]
            quantity += [prod.quantity]
            buyingPrice += [prod.buyingPrice]
            sellingPrice += [prod.sellingPrice]
            if prod.hasSerial == 1 {
                serialNumber.append(prod.serialArray)
            }else{
                serialNumber.append("")
            }
            
        }
        if let advanceAmount = self.advance?.toDouble(){
            let param : [String : Any] = ["preorder_id": preOrderId, "products": products, "quantity": quantity, "buying_price": buyingPrice, "advance": advanceAmount, "selling_price": sellingPrice, "serial_no": serialNumber, "total": self.totalAmount, "to_be_paid": self.amountToBePaid, "vendor_id": vendorId]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            self.presenter.postPreOrderUpdateData(param: param)
        }
        
    }
   
    @IBAction func receivePreorderAction(_ sender: UIButton) {
         self.updateData()
    }
    
    func updateData(){
        if let advanceAmount = self.advance?.toDouble(){
            if self.totalAmount == advanceAmount{
                self.postPreOrderRecieveData()
            }else{
                self.alertWithTextField()
            }
        }
    }
    
    func prepareProductData(){
        guard let product = self.products, product.count > 0 else{
            return
        }
        
//        guard let serials = self.serials else{
//            return
//        }
        
        var products : [String] = []
        var quantity : [String] = []
        var buyingPrice : [String] = []
        var sellingPrice : [String] = []
        var serialNumber : [String] = []

        for prod in product{
            products  += [String(describing: prod.id)]
            quantity += [prod.quantity]
            buyingPrice += [prod.buyingPrice]
            sellingPrice += [prod.sellingPrice]
            if prod.hasSerial == 1 {
                serialNumber += [prod.serialArray]
            }else{
                serialNumber += [""]
            }
            
        }
        
    }
    
}

//Mark: TableView Delegate And DataSource
extension PreOrderPendingViewController : UITableViewDelegate, UITableViewDataSource{
    
    fileprivate func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PreOrderProductSummaryCell.nib, forCellReuseIdentifier: PreOrderProductSummaryCell.identifier)
        self.tableView.register(PreOrderProductCell.nib, forCellReuseIdentifier: PreOrderProductCell.identifier)
        self.tableView.register(AddProductInPreorderCell.nib, forCellReuseIdentifier: AddProductInPreorderCell.identifier)
        self.tableView.separatorStyle = .none
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == Sections.Summary.rawValue {
            return 1
        }
        else if section == Sections.Product.rawValue, let list = self.products, list.count > 0 {
            return list.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == Sections.Summary.rawValue {
            
//            guard let summary = self.preorder, summary != nil else{
//                return cell
//            }
            
            let cell: PreOrderProductSummaryCell = tableView.dequeueReusableCell(withIdentifier: PreOrderProductSummaryCell.identifier, for: indexPath) as! PreOrderProductSummaryCell
            cell.selectionStyle = .none
            
            guard let summary = self.preorder else{
                return cell
            }

            cell.totalAmountLbl.text = LanguageManager.TotalAmount + " : " + "\(summary.total)" + Constants.currencySymbol
            
            
            let totalAmount = Double(summary.total)
            let advanceAmount = Double(summary.advance)
            
            guard let total = totalAmount, let advance = advanceAmount else{
                return cell
            }
            var final : Double = 0.0
            if total < advance{
                cell.advanceLbl.textColor = UIColor.lightRed
                summary.cashBack = "\(advance - total)"
            }else{
                cell.advanceLbl.textColor = UIColor.black
                final = total - advance
            }
            
            cell.advanceLbl.text = LanguageManager.AdvanceAmount + " : " + "\(summary.advance)"  + Constants.currencySymbol
            
            cell.toPayLbl.text = LanguageManager.AmountToBePaid + " : " + "\(final)" + Constants.currencySymbol
            cell.paidLbl.text = LanguageManager.Paid + " : " + summary.paid + Constants.currencySymbol
            cell.payableLbl.text = LanguageManager.Payable + " : " + "\(final)" + Constants.currencySymbol
            cell.returnLbl.text = LanguageManager.CashBack + " : " + summary.cashBack + Constants.currencySymbol
            cell.dealerLbl.text = LanguageManager.Vendor + " : " + summary.vendorName
            //cell.summaryEdit.isHidden = true
            self.vendorId = summary.vendorId
            //self.paidAmount =
            
            cell.summaryEdit.addTarget(self, action: #selector(onSummaryedit), for: .touchUpInside)
            
            return cell
            
            
        }
        else if indexPath.section == Sections.Product.rawValue{
            
            if let list = self.products, list.count > 0 , indexPath.row <= list.count - 1{
                let cell : PreOrderProductCell = tableView.dequeueReusableCell(withIdentifier: PreOrderProductCell.identifier, for: indexPath) as! PreOrderProductCell
                cell.selectionStyle = .none

                let product = list[indexPath.row]
                
                cell.productName.text = LanguageManager.Name + " : " + product.productName
                cell.productAmount.text = LanguageManager.Quantity + " : " + "\(product.quantity)"
                cell.productBuyingPrice.text = LanguageManager.BuyingPrice + " : " +  product.buyingPrice  + Constants.currencySymbol
                cell.productSellingPrice.text = LanguageManager.SellingPrice + " : " +  product.sellingPrice  + Constants.currencySymbol
                
                cell.productPopUpBtn.tag = product.productId
                cell.productPopUpBtn.addTarget(self, action: #selector(onProductPopUpBtnTapped), for: .touchUpInside)
                
                return cell
            }
        }
        else{
            let cell : AddProductInPreorderCell = tableView.dequeueReusableCell(withIdentifier: AddProductInPreorderCell.identifier, for: indexPath) as! AddProductInPreorderCell
            
            cell.selectionStyle = .none
            
            cell.addProductBtn.addTarget(self, action: #selector(onAddProduct), for: .touchUpInside)
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if indexPath.section == Sections.Product.rawValue {
            if let list = self.products, list.count > 0 , indexPath.row <= list.count - 1 {
                let update = UITableViewRowAction(style: .normal, title: LanguageManager.Update) { action, index in
                    
                    let productInfo = list[indexPath.row]
                    self.hasSerial = productInfo.hasSerial
                    self.productId = productInfo.productId
                    //self.serialArray = productInfo.serialArray
                    
//                    productInfo.quantity = "0.0"
//                    productInfo.buyingPrice = "0.0"
//                    productInfo.sellingPrice = "0.0"
//                    guard let quantity =  Double(productInfo.quantity), let buyingPrice = Double(productInfo.buyingPrice) else{
//                        return
//                    }
//                    let productTotal = quantity * buyingPrice
//                    guard let summary = self.preorder else{
//                        return
//                    }
//                    
//                    let summaryTotal = Double(summary.total)
//                    guard let sumTotal = summaryTotal else{
//                        return
//                    }
//                    summary.total = "\(sumTotal - productTotal)"


                    self.navigateToPreOrderAddVC(product: productInfo)
//                    if let index = list.index(where : {$0.productId == self.productId}){
//                        self.products?.remove(at: index)
//                    }
                    //self.refreshTableView()
                    
                }
                
                update.backgroundColor = UIColor.orange
                
                let delete = UITableViewRowAction(style: .normal, title: LanguageManager.Delete) { action, index in
                    let data = list[indexPath.row]
                    self.productId = data.productId
                    
//                    let quantity = Double(data.quantity)
//                    let purchasePrice = Double(data.buyingPrice)
//
//                    guard let quant = quantity, let buy = purchasePrice else{
//                        return
//                    }
//
//                    let totalMinus = quant * buy
//
//                    guard let summary = self.preorder else{
//                        return
//                    }
//
//                    let whole = Double(summary.total)
//                    let preAdvance = Double(summary.advance)
//                    guard let wholeTotal = whole, let advance = preAdvance else{
//                        return
//                    }
//                    summary.total = "\(wholeTotal - totalMinus)"
//                    let total = wholeTotal - totalMinus
//                    summary.payable = "\(total - advance)"
                    
                    if let index = list.firstIndex(where : {$0.productId == self.productId}){
                        self.products?.remove(at: index)
                    }
                    
//                    let count = self.products
//                    if count == 0 {
//
//                    }
                    let productsCount = self.products?.count
                    if productsCount == 0 {
                        self.resetSummary()
                    }else{
                        self.updateSummaryInfo()
                        self.refreshTableView()
                    }
                    //self.updateSummary(details: summary)
                    
                }
                delete.backgroundColor = UIColor.lightRed
                
                return [delete , update]
            }
        }
        return []
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == Sections.Summary.rawValue{
            return 246.0
        }else if indexPath.section == Sections.Product.rawValue{
            return 110.0
        }else{
            return 51.0
        }
    }
    
    fileprivate func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onProductPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if let list = self.products, list.count > 0 {
            self.productId = sender.tag
            for product in list{
                if self.productId == product.productId{
                    let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.hasSerial = product.hasSerial
                        self.navigateToPreOrderAddVC(product: product)
                    }
                    
                    let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        if let index = list.firstIndex(where : {$0.productId == self.productId}){
                            self.products?.remove(at: index)
                        }
                        let productsCount = self.products?.count
                        if productsCount == 0 {
                            self.resetSummary()
                        }else{
                            self.updateSummaryInfo()
                            self.refreshTableView()
                        }
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(editAction)
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    @objc func onAddProduct(sender : UIButton){
        self.navigateToPreOrderProductAddVC()
    }
    
    @objc func onSummaryedit(sender: UIButton){
        self.navigateToPreOrderPendingInfoUpdateVC()
        //navigateToPreOrderSummaryUpdateVC()
        //self.alertWithAdvanceTextField()
    }
    
    func alertWithAdvanceTextField(){
        let alertController = UIAlertController(title: LanguageManager.ChangeAdvance, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.AdvanceAmount
            textField.textAlignment = .center
            textField.keyboardType = .decimalPad
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.AdvanceAmountIsRequired, message: "")
                return
            }
            
            self.advance = text
            
            self.updateSummaryDetails()
            
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func updateSummaryDetails(){
        guard let id = self.id else{
            return
        }
        
        guard let purchaseId = self.purchaseId, let orderDate = self.orderDate, let receivedDate = self.receivedDate, let advance = self.advance, let vendorId = self.vendorId, let remarks = self.remarks else {
            return
        }
        
        guard let list = self.products, list.count > 0 else {
            return
        }
        
        var total = 0.0
        
        for product in list{
            if let buyingPrice = Double(product.buyingPrice), let quantity = Double(product.quantity){
                total += buyingPrice * quantity
            }
            self.totalAmount = total
        }
        
        guard let paid = self.paidAmountText else{
            return
        }
        
        guard let name = self.vendorName else {
            return
        }
        if let advanceAmount = self.advance?.toDouble(){
            if self.totalAmount >= advanceAmount{
                self.payableText = "\(self.totalAmount - advanceAmount)"
            }else{
                self.cashBackText = "\(advanceAmount - self.totalAmount)"
            }
        }
        
        guard let payableAmount = self.payableText else{
            return
        }
        
        guard let cashReturn = self.cashBackText else {
            return
        }
        
        let details = PreOrderDetailsData(id: id, total: "\(self.totalAmount)", purchaseId: purchaseId, orderDate: orderDate, receivedDate: receivedDate, advance: advance, payable: payableAmount, paid: "\(paid)", cashBack: "\(cashReturn)", vendorId: vendorId, vendorName: name, remarks: remarks)
        
        updateSummary(details: details)
        //self.refreshTableView()
        
        //let details = PreOrderDetailsData(id: id, paid: "\(paid)", cashBack: "\(cashReturn)", vendorName: name, payable: "\(payableAmount)")
        
        //        guard let det = details else{
        //            return
        //        }
        
        //delegate.updateSummary(details: details)
        //self.navigationController?.popViewController(animated: true)
    }
    
    func resetSummary(){
        
        guard let details = self.preorder else{
            return
        }
        let id = details.id
        let purchaseId = details.purchaseId
        let orderDate = details.orderDate
        let receiveDate = details.receivedDate
        let advance = details.advance
        let vendorId = details.vendorId
        let vendorName = details.vendorName
        
        let orderDetails = PreOrderDetailsData(id: id,total: "0.0", purchaseId: purchaseId, orderDate: orderDate, receivedDate: receiveDate, advance: advance, payable: "0.0", paid: "0.0", cashBack: "0.0", vendorId: vendorId, vendorName: vendorName, remarks: "")
        updateSummary(details: orderDetails)
    }
    
    func navigateToPreOrderProductAddVC(){
        let viewController = UIStoryboard(name: "Purchase", bundle: nil).instantiateViewController(withIdentifier: "PreOrderEditProductListVC") as! PreOrderEditProductListVC
        viewController.delegate = self
        viewController.updateSummary = self
        guard let details = self.preorder else{
            return
        }
        viewController.productDetails = details
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPreOrderableProductAddVC(){
        let viewController = UIStoryboard(name: "Purchase", bundle: nil).instantiateViewController(withIdentifier: "PreOrderEditProductListVC") as! PreOrderEditProductListVC
        viewController.delegate = self
        viewController.updateSummary = self
        guard let details = self.preorder else{
            return
        }
        viewController.productDetails = details
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPreOrderSummaryUpdateVC(){
        let viewController = UIStoryboard(name: "Purchase", bundle: nil).instantiateViewController(withIdentifier: "PreOrderSummaryUpdateVC") as! PreOrderSummaryUpdateVC
        viewController.preOrderDetails = self.preorder
        viewController.delegate = self
 
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPreOrderPendingInfoUpdateVC(){
        let viewController = UIStoryboard(name: "Purchase", bundle: nil).instantiateViewController(withIdentifier: "PreOrderPendingInfoUpdateVC") as! PreOrderPendingInfoUpdateVC
        viewController.instanceOfPreOrderPendingVC = self
        viewController.preorder = self.preorder
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Delete, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            //self.presenter.postPreOrderCancelDataToServer(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToPreOrderAddVC(product: PreOrderProductData){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PreOrderAddViewController") as! PreOrderAddViewController
        viewController.productInfo = product
        viewController.productDetails = preorder
        viewController.pageType = .Edit
//        viewController.delegate = self
        guard let hasSerial = self.hasSerial else{
            return
        }
        viewController.hasSerial = hasSerial
//        
        viewController.updateDelegate = self
//        if hasSerial == 1 {
//            //viewController.serails = self.serials
//            //viewController.updateSerialsDelegate = self
//            viewController.
//        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
   
}

//Mark: Api Delegate
extension PreOrderPendingViewController : preOrderUpdateViewDelegate{
//    func postPreOrderCancelData(data: AddDataMapper) {
//        guard let message = data.message else{
//            return
//        }
//        self.displayMessage(userMessage: message)
//    }
    func postPreOrderUpdateData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        displayUpdateMessage(userMessage: message)
    }
    
    func setPreOrderEditData(data: PreOrderProductEditDataMapper?) {
        
        guard let data = data, let list = data.products, list.count > 0 , let preOrderData = data.preorder else{
            return
        }
        self.products = list
        self.preorder = preOrderData
        self.objectInitialization()
        self.refreshTableView()
    }
    
    func postPreOrderReceiveData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        displayUpdateMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.preOrderId else{
            return
        }
        self.presenter.getPreOrderDataFromServer(id: id)
    }
}

extension PreOrderPendingViewController : PreOrderProductEditDelegate{
    func addProduct(product: PreOrderProductData?) {
        
        guard let prod = product else {
            return
        }
        self.products?.append(prod)
        //updateSummaryInfo()
        self.refreshTableView()
    }
}

extension PreOrderPendingViewController : PreOrderSummaryUpdateDelegate{
    func updateSummary(details: PreOrderDetailsData?) {
        guard let data = details else{
            return
        }
        self.preorder = data
        self.refreshTableView()
    }
    
}

extension PreOrderPendingViewController : PreOrderProductUpdatelegate{
    func updateProduct(product: PreOrderProductData?) {
        guard let selectedProduct = product , var products = self.products else{
            return
        }
        let productId = selectedProduct.productId
        
        var index = 0
        for prod in products{
            if prod.productId == productId{
                break;
            }
            index += 1
        }
        
//        for productUpdate in products{
//            productUpdate.quantity
//        }
        
        products[index] = selectedProduct
        self.products = products
        updateSummaryInfo()
        self.refreshTableView()
    }
    
    func updateSummaryInfo(){
//        guard let summary = self.preorder else{
//            return
//        }
//
//        guard let list = self.products, list.count > 0 else {
//            return
//        }
//
//        var total = 0.0
//
//        for product in list{
//            if let buyingPrice = Double(product.buyingPrice), let quantity = Double(product.quantity){
//                total += buyingPrice * quantity
//            }
//            self.totalAmount = total
//        }
//
//        summary.total = String(describing: self.totalAmount)
//
//        guard let advanceAmount = Double(summary.advance) else {return}
//
//        if self.totalAmount > advanceAmount{
//            self.amountToBePaid = self.totalAmount - advanceAmount
//        }else{
//            self.amountToBePaid = advanceAmount - self.totalAmount
//        }
//
//
//
//        summary.paid = String(describing: self.paidAmount)
//
//        if self.paidAmount > self.amountToBePaid {
//            self.payableAmount = 0.0
//            self.cashBack = Double(self.paidAmount - amountToBePaid)
//        }else{
//            self.cashBack = 0.0
//            self.payableAmount = Double(amountToBePaid - self.paidAmount)
//        }
//        summary.cashBack = String(describing: cashBack)
//        summary.payable = String(describing: payableAmount)
//
//        self.updateSummary(details: summary)
        
        self.refreshSummaryInfo()
        
    }
    
    func refreshSummaryInfo(){
        guard let summary = self.preorder else{
            return
        }
        
        guard let list = self.products, list.count > 0 else {
            return
        }
        
        var total = 0.0
        
        for product in list{
            if let buyingPrice = Double(product.buyingPrice), let quantity = Double(product.quantity){
                total += buyingPrice * quantity
            }
            self.totalAmount = total
        }
        
        guard let advanceAmount = Double(summary.advance) else {return}
        
        //self.paidAmount = self.paidAmount + advanceAmount
        
        if self.totalAmount > advanceAmount{
            self.payableAmount = self.totalAmount - advanceAmount
            self.amountToBePaid = self.totalAmount - advanceAmount
            self.cashBack = 0.0
        }else{
            self.payableAmount = 0.0
            self.amountToBePaid = 0.0
            self.cashBack = advanceAmount - self.totalAmount
        }
        
        if self.paidAmount > self.amountToBePaid{
            self.payableAmount = 0.0
            self.cashBack = self.paidAmount - self.amountToBePaid
        }else{
            self.cashBack = 0.0
            self.payableAmount = self.amountToBePaid - self.paidAmount
        }
        
        summary.total = String(describing: self.totalAmount)
        summary.paid = String(describing: self.paidAmount)
        summary.cashBack = String(describing: self.cashBack)
        summary.payable = String(describing: self.payableAmount)
        
        self.updateSummary(details: summary)
    }
}

extension PreOrderPendingViewController : productSerialsUpdateDelegate{
    func updateSerials(productId: Int?, serials: String?) {
        guard let serials = serials, serials.count > 0 else{
            return
        }
        self.hasSerial = 1
        self.serialArray = serials
        //self.serials = serials
        guard let product = self.products, product.count > 0, let id = productId else{
            return
        }
        for prod in product {
            if prod.productId == id {
                print(prod.productId)
                prod.serials = [serials]
            }
        }
    }
}

extension PreOrderPendingViewController {
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.products = []
            self.refreshTableView()
            guard let id = self.preOrderId else{
                return
            }
            self.presenter.getPreOrderDataFromServer(id: id)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayUpdateMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
//            self.products = []
//            self.refreshTableView()
//            guard let id = self.preOrderId else{
//                return
//            }
//            self.presenter.getPreOrderDataFromServer(id: id)
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension PreOrderPendingViewController {
    func alertWithTextField(){
        guard let advance = self.preorder?.advance, let advanceAmount = Double(advance), let total = self.preorder?.total, let totalAmount = Double(total) else{
            return
        }
        
        if advanceAmount > totalAmount {
            self.showAlert(title: LanguageManager.AdvanceAmountsNeedToBeModified, message: "")
            return
        }
        
        let alertController = UIAlertController(title: LanguageManager.Receive, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.Amount
            textField.textAlignment = .center
            textField.keyboardType = .decimalPad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.PaidAmountIsRequired, message: "")
                return
            }
            

            guard let paidAmountText = textField.text, let paidAMount = Double(paidAmountText) else{
                return
            }
            
            self.paidAmount = paidAMount
            
            self.postPreOrderRecieveData()
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func postPreOrderRecieveData(){
        guard let product = self.products, product.count > 0 else{
            return
        }
        
        for prod in product {
            if prod.hasSerial == 1 {
                if prod.serialArray == "---" && prod.serials == []{
                    self.showAlert(title: prod.productName, message: LanguageManager.SerialNumberIsRequired)
                    return
                }
            }
        }
        
        guard let preOrderId = self.preOrderId else{
            return
        }
        
        var products : [String] = []
        var quantity : [String] = []
        var buyingPrice : [String] = []
        var sellingPrice : [String] = []
        var serialNumber : [String] = []
        //var serialString : String = ""
        
        for prod in product{
            products  += [String(describing: prod.productId)]
            quantity += [prod.quantity]
            buyingPrice += [prod.buyingPrice]
            sellingPrice += [prod.sellingPrice]
            //let serialArray : String?
            if prod.hasSerial == 1 {
                serialNumber.append(prod.serialArray)
            }else{
                serialNumber.append("")
            }
            //serialString = serialNumber.joined(separator: ",")
        }
        
        //self.serials?.append(serialString)
        
        guard let vendorId = self.vendorId else{
            return
        }
        
        self.updateSummaryInfo()
        if let advanceAmount = self.advance?.toDouble(){
            let param : [String : Any] = ["preorder_id": preOrderId, "products": products,"quantity": quantity, "buying_price": buyingPrice,"selling_price": sellingPrice,"serial_no" : serialNumber, "total" : self.totalAmount, "advance": advanceAmount, "to_be_paid" : self.amountToBePaid, "paid" : self.paidAmount, "payable" : self.payableAmount, "cashback" : self.cashBack, "vendor_id" : vendorId]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            self.presenter.postPreOrderReceiveDataToServer(param: param)
        }
        
        
        
    }
    
}

extension PreOrderPendingViewController : PreOrderProductAddDelegate{
    func addProducts(product: [PreOrderProductData]?) {
        guard let list = product, list.count > 0 else {return}
        self.products?.append(contentsOf: list)
        updateSummaryInfo()
        self.refreshTableView()
    }
}
