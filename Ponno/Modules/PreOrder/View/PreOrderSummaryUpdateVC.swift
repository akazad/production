//
//  PreOrderSummaryUpdateVC.swift
//  Ponno
//
//  Created by a k azad on 21/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit


class PreOrderSummaryUpdateVC: UIViewController {

    @IBOutlet weak var totalAmountLbl: UILabel!{
        didSet{
            totalAmountLbl.text = LanguageManager.TotalAmount
        }
    }
    @IBOutlet weak var totalAmountTextField: UITextField!{
        didSet{
            totalAmountTextField.placeholder = LanguageManager.TotalAmount
        }
    }
    @IBOutlet weak var advanceLbl: UILabel!{
        didSet{
            advanceLbl.text = LanguageManager.AdvanceAmount
        }
    }
    @IBOutlet weak var advanceTextField: UITextField!{
        didSet{
            advanceTextField.placeholder = LanguageManager.AdvanceAmount
        }
    }
    @IBOutlet weak var amountToPayLbl: UILabel!{
        didSet{
            amountToPayLbl.text = LanguageManager.AmountToBePaid
        }
    }
    @IBOutlet weak var amountToPayTextField: UITextField!{
        didSet{
            amountToPayTextField.placeholder = LanguageManager.AmountToBePaid
        }
    }
    @IBOutlet weak var paidLbl: UILabel!{
        didSet{
            paidLbl.text = LanguageManager.Paid
        }
    }
    @IBOutlet weak var paidTextField: UITextField!{
        didSet{
            paidTextField.placeholder = LanguageManager.Paid
        }
    }
    @IBOutlet weak var payableLbl: UILabel!{
        didSet{
            payableLbl.text = LanguageManager.Payable
        }
    }
    @IBOutlet weak var payableTextField: UITextField!{
        didSet{
            payableTextField.placeholder = LanguageManager.Payable
        }
    }
    @IBOutlet weak var cashBackLbl: UILabel!{
        didSet{
            cashBackLbl.text = LanguageManager.CashBack
        }
    }
    @IBOutlet weak var cashBackTextField: UITextField!{
        didSet{
            cashBackTextField.placeholder = LanguageManager.CashBack
        }
    }
    @IBOutlet weak var vendorLbl: UILabel!{
        didSet{
            vendorLbl.text = LanguageManager.Vendor
        }
    }
    @IBOutlet weak var vendorTextField: UITextField!{
        didSet{
            vendorTextField.placeholder = LanguageManager.Vendor
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    var preOrderDetails : PreOrderDetailsData?
    var delegate : PreOrderSummaryUpdateDelegate!
    
    fileprivate var presenter = PreOrderSummaryUpdatePresenter(service: VendorsService())
    
    var vendorPicker = UIPickerView()
    var vendorsList : [VendorsList] = []{
        didSet{
            self.vendorPicker.reloadAllComponents()
        }
    }
    var vendorId : Int?
    
    var id: Int?
    var total : String?
    var purchaseId : Int?
    var orderDate : String?
    var receivedDate : String?
    var advance : String?
    var payable : Double?
    var amountToPay : Double?
    var paidAmount : Double?
    var advanceAmount : Double?
    var cashBack : Double?
    //var vendorId : Int?
    var vendorName: String?
    var remarks : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetup()
        self.setUpPickerView()
        self.attachPresenter()
        self.objectInitialization()
    }
    
    func objectInitialization(){
        guard let details = self.preOrderDetails else {
            return
        }
        self.id = details.id
        self.total = details.total
        self.purchaseId = details.purchaseId
        self.orderDate = details.orderDate
        self.receivedDate = details.receivedDate
        self.advance = details.advance
        self.vendorId = details.vendorId
        self.remarks = details.remarks
    }
    
    func initialSetup(){
        
        self.title = LanguageManager.UpdateInfo
        
        guard let details = self.preOrderDetails else{
            return
        }
        self.id = details.id
//        let payableAmount = details.payable
//        let payable = Double(payableAmount)
        self.totalAmountTextField.text = details.total
        self.totalAmountTextField.isEnabled = false
        self.advanceTextField.text = details.advance
        self.amountToPayTextField.text = details.payable
        self.amountToPayTextField.isEnabled = false
        //self.amountToPayTextField.text = "\(payable)"
        self.paidTextField.text = "\(0.0)"
        self.paidTextField.isEnabled = false
        self.payableTextField.text = details.payable
        self.payableTextField.isEnabled = false
        self.cashBackTextField.text = "\(0.0)"
        self.cashBackTextField.isEnabled = false
        self.vendorTextField.text = details.vendorName
        self.vendorTextField.isEnabled = false
        
        self.vendorName = details.vendorName
        
//        self.paidAmount = 0.0
//        self.cashBack = 0.0
        
        //paidTextField.addTarget(self, action: #selector(onPaidTextChange), for: .editingChanged)
        advanceTextField.addTarget(self, action: #selector(onAdvanceChange), for: .touchUpInside)
        amountToPayTextField.addTarget(self, action: #selector(onAmountToPayChange), for: .editingChanged)
        
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        
    }
    
    @objc func onPaidTextChange(sender: UITextField){
        self.refreshScreen()
    }
    
    @objc func onAdvanceChange(sender: UITextField){
        self.refreshScreen()
    }
    
    @objc func onAmountToPayChange(sender: UITextField){
        self.refreshScreen()
    }
    
    //Mark: PickerView
    func setUpPickerView(){
        vendorPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        self.vendorTextField.inputView = vendorPicker
        self.vendorTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.vendorTextField.resignFirstResponder()
        self.submitBtn.becomeFirstResponder()
    }
    
//    func refreshScreen(){
//        guard let paidAmount = self.paidTextField.text, let paid = Double(paidAmount), let amountToBePaid = self.amountToPayTextField.text, let amount = Double(amountToBePaid) else {
//            return
//        }
//
//        self.paidAmount = paid
//
//        if paid == 0 {
//            payable = amount
//        }else{
//            if paid <= amount {
//                payable = amount - paid
//                cashBack = 0.0
//            }else{
//                payable = 0.0
//                cashBack = paid - amount
//            }
//        }
//
//        guard let payable = self.payable else{
//            return
//        }
//        self.payableTextField.text = "\(payable)"
//
//        guard let cashBack = self.cashBack else {
//            return
//        }
//        self.cashBackTextField.text = "\(cashBack)"
//    }
    
    func refreshScreen(){
        guard let advanceAmount = self.advanceTextField.text, let advance = Double(advanceAmount), let total = self.totalAmountTextField.text, let totalAmount = Double(total) else {
            return
        }
        
        self.advanceAmount = advance
        
        if advance == 0 {
            payable = advance
        }else{
            if advance < totalAmount {
                payable = totalAmount - advance
                amountToPay = totalAmount - advance
                cashBack = 0.0
            }else{
                payable = 0.0
                amountToPay = 0.0
                cashBack = advance - totalAmount
            }
        }
        
        guard let amountToBePaid = amountToPay else{
            return
        }
        
        self.amountToPayTextField.text = "\(amountToBePaid)"
        
        guard let payable = self.payable else{
            return
        }
        self.payableTextField.text = "\(payable)"
        
        guard let cashBack = self.cashBack else {
            return
        }
        self.cashBackTextField.text = "\(cashBack)"
    }
    
    @objc func onSubmit(sender: UIButton){
        guard let id = self.id else{
            return
        }
        
        guard let total = self.totalAmountTextField.text, let purchaseId = self.purchaseId, let orderDate = self.orderDate, let receivedDate = self.receivedDate, let advance = self.advance, let vendorId = self.vendorId, let remarks = self.remarks else {
            return
        }
        
        guard let paid = self.paidAmount else{
            return
        }
        
        guard let name = self.vendorName else {
            return
        }
        
        guard let payableAmount = self.payable else{
            return
        }
        
        guard let cashReturn = self.cashBack else {
            return
        }
        
        let details = PreOrderDetailsData(id: id,total: total, purchaseId: purchaseId, orderDate: orderDate, receivedDate: receivedDate, advance: advance, payable: "\(payableAmount)", paid: "\(paid)", cashBack: "\(cashReturn)", vendorId: vendorId, vendorName: name, remarks: remarks)
        
        //let details = PreOrderDetailsData(id: id, paid: "\(paid)", cashBack: "\(cashReturn)", vendorName: name, payable: "\(payableAmount)")
        
//        guard let det = details else{
//            return
//        }
        
        delegate.updateSummary(details: details)
        self.navigationController?.popViewController(animated: true)
    }

}

//Mark: PickerViewDelegate
extension PreOrderSummaryUpdateVC : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        guard self.vendorsList.count > 0 else {
            return 0
        }
        return self.vendorsList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard self.vendorsList.count > 0 else {
            return ""
        }
        let vendors = self.vendorsList[row]
        self.vendorTextField.text = vendors.name
        self.vendorId = vendors.id
        return vendors.name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard self.vendorsList.count > 0 else {
            return
        }
        let vendors = self.vendorsList[row]
        self.vendorTextField.text = vendors.name
        self.vendorId = vendors.id
    }
}

extension PreOrderSummaryUpdateVC : PreOrderSummaryDelegate{
    func setVendorsList(data: VendorsDataMapper) {
        guard let list = data.vendors, list.count > 0 else {
            return
        }
        self.vendorsList = list
    }
    
    func onFailed(data: String) {
        
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getVendorsListDataFromServer(getAll: true)
    }
}
