//
//  PreOrderableProductViewController.swift
//  Ponno
//
//  Created by a k azad on 28/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class PreOrderableProductViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!{
        didSet{
            searchBar.placeholder = LanguageManager.ProductSearch
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    
    let presenter = PurchasableProductListPresenter(service: ProductService())
    
    var timer : Timer?
    
    var productList : [ProductList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var preOrders : PreOrders?
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var searchText = ""
    var currentPage : Int = 1
    var lastPage : Int = 0
    var isLoading : Bool = false
    var getAll: Bool = true
    
    var productId : Int?
    var selectedProductId : [Int] = []
    var selectedAmounts : [String] = []
    var selectedBuyingPrices : [String] = []
    var selectedSellingPrices : [String] = []
    var selectedName : [String] = []
    
    var selectedProduct : ProductList?
    var filteredList : [ProductList] = []
    
    var productDetails : PreOrderDetailsData?
    var productName : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpSearchBar()
        self.configureTableView()
        self.attachPresenter()
        self.setUpViews()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext(sender:)), for: .touchUpInside)
        self.title = LanguageManager.SelectProduct
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    @objc func onNext(sender : UIButton){
        if self.selectedProductId.count <= 0 {
            self.showAlert(title: LanguageManager.ToOrderSelectAProduct, message: "")
        }else{
            self.navigateToPreOrderAddInfoVC()
        }
    }
    
    func navigateToPreOrderAddInfoVC(){
        let viewController = UIStoryboard(name: "Purchase", bundle: nil).instantiateViewController(withIdentifier: "PreOrderAddInfoViewController") as! PreOrderAddInfoViewController
        
        viewController.preOrders = self.preOrders
        viewController.selectedProductId = self.selectedProductId
        viewController.selectedAmounts = selectedAmounts
        viewController.selectedBuyingPrices = selectedBuyingPrices
        viewController.selectedSellingPrices = selectedSellingPrices
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Search Delegate
extension PreOrderableProductViewController: UISearchBarDelegate{
    
    fileprivate func setUpSearchBar(){
        self.searchBar.delegate = self
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearchActive = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if self.productList.count > 0 {
            if searchText == "" {
                isSearchActive = false
                return
            }
        }
        guard let textToSearch = searchBar.text else {
            return
        }
        
        filteredList = self.productList.filter { (product : ProductList) -> Bool in
            return product.name.lowercased().contains(textToSearch.lowercased()) || product.variant.lowercased().contains(textToSearch.lowercased()) || product.company.lowercased().contains(textToSearch.lowercased()) || product.categoryName.lowercased().contains(textToSearch.lowercased())
            
        }
        
        if filteredList.count == 0 {
            isSearchActive = false
        }
        else {
            isSearchActive = true
        }
        
    }
}

//Mark: TableView Delegate And Data Source
extension PreOrderableProductViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductListCell.nib, forCellReuseIdentifier: ProductListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive{
            if self.filteredList.count > 0 {
                return self.filteredList.count
            }
            else{
                return 0
            }
        }
        else{
            if self.productList.count > 0  {
                return self.productList.count
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductListCell = tableView.dequeueReusableCell(withIdentifier: ProductListCell.identifier, for: indexPath) as! ProductListCell
        cell.selectionStyle = .none
        
        if isSearchActive{
            if self.filteredList.count > 0 {
                let product = self.filteredList[indexPath.row]
                
                cell.product = product
                
                if self.selectedProductId.contains(product.productId){
                    cell.selectBtn.isHidden = false
                }
                else{
                    cell.selectBtn.isHidden = true
                }
                
//                cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
//                cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + product.image), placeholderImage: UIImage(named: "placeholder"))
//                cell.productNameLabel.text = String(describing: product.name).capitalized
//                cell.productVariantLabel.text = "Variant" + " : " + String(describing: product.variant)
//                cell.productQuantityLabel.text = "Quantity" + " : " + String(describing: product.quantity) + " " + product.unit
//                cell.productCategoryLabel.text = "Category" + " : " + String(describing: product.categoryName)
//                cell.productSellingPriceLabel.text = "Selling Price" + " : " + String(describing: product.sellingPrice) + " " + Constants.currencySymbol
                //self.selectedProductName = product.name
                
                cell.selectBtn.tag = product.productId
                cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
            }
        }
        else{
            if self.productList.count > 0 {
                let product = self.productList[indexPath.row]
                
                cell.product = product
                cell.preOrderQuantityLbl.isHidden = true
                
                if self.selectedProductId.contains(product.productId){
                    cell.selectBtn.isHidden = false
                }
                else{
                    cell.selectBtn.isHidden = true
                }
                
//                cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
//                cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + product.image), placeholderImage: UIImage(named: "placeholder"))
//                cell.productNameLabel.text = String(describing: product.name)
//                cell.productVariantLabel.text = "Variant" + " : " + String(describing: product.variant)
//                cell.productQuantityLabel.text =  "Quantity" + " : " + String(describing: product.quantity) + " " + product.unit
//                cell.productCategoryLabel.text = "Category" + " : " + String(describing: product.categoryName)
//                cell.productSellingPriceLabel.text = "Selling Price" + " : " + String(describing: product.sellingPrice) + " " + Constants.currencySymbol
                cell.selectBtn.tag = product.productId
                cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
            }
        }
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        if isSearchActive{
            if self.filteredList.count > 0 {
                
                let data = self.productList[indexPath.row]
                
                self.selectedProduct = data
                self.productName = data.name
                self.productId = data.productId
                
                if self.selectedProductId.contains(data.productId){
                    self.selectedProductAlert(userMessage: LanguageManager.TheProductIsAlreadySelectedDeselectTheProductForUpdate)
                }
                
                self.navigateToPreOrderAddVC()
//                if data.serials == ""{
//                    self.navigateToPurchaseAddVC()
//                }
//                else{
//                    self.navigateToPurchasableProductWithSerialVC(serials: data.serials)
//                }
            }
        }
        else{
            if self.productList.count > 0 {
                let data = self.productList[indexPath.row]
                
                self.selectedProduct = data
                self.productName = data.name
                self.productId = data.productId
                
                if self.selectedProductId.contains(data.productId){
                    self.selectedProductAlert(userMessage: LanguageManager.TheProductIsAlreadySelectedDeselectTheProductForUpdate)
                }
                
                self.navigateToPreOrderAddVC()
//                if data.serials == ""{
//                    self.navigateToPurchaseAddVC()
//                }
//                else{
//                    self.navigateToPurchasableProductWithSerialVC(serials: data.serials)
//                }
            }
        }
    }
    
    func navigateToPreOrderAddVC(){
        let viewController = UIStoryboard(name: "Purchase", bundle: nil).instantiateViewController(withIdentifier: "PreOrderAddViewController") as! PreOrderAddViewController
        
        viewController.delegate = self
        viewController.productName = self.productName
        viewController.productID = self.productId
        viewController.productList = self.selectedProduct
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func onRemove(sender : UIButton){
        let productId = sender.tag
        
        if self.selectedProductId.contains(productId){
            removeDataofProduct(productId: productId)
        }
    }
    
    func removeDataofProduct(productId : Int){
        var productAtIndex = -1
        for index in 0..<self.selectedProductId.count{
            if self.selectedProductId[index] == productId{
                productAtIndex = index
            }
        }
        
        self.selectedSellingPrices.remove(at: productAtIndex)
        self.selectedAmounts.remove(at: productAtIndex)
        self.selectedProductId.remove(at: productAtIndex)
        self.refreshTableView()
    }
    
}

//ProductDelegate
extension PreOrderableProductViewController : PreOrderableProductDelegate{
    func setProductData(name : String, amount: String, buyingPrice: String, sellingPrice: String) { 
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        self.selectedProductId.append(selectedProduct.productId)
        selectedBuyingPrices.append(buyingPrice)
        selectedSellingPrices.append(sellingPrice)
        selectedAmounts.append(amount)
        selectedName.append(name)
        self.refreshTableView()
        //print("\(amount)" + "\(buyingPrice) " + " : " + "\(sellingPrice)")
    }
}

//

//Mark: Api Delegate
extension PreOrderableProductViewController : PurchasableProductListViewDelegate{
    func setPurchasableProductData(data: ProductListDataMapper) {
        guard let list = data.productList, list.count > 0 else{
            return
        }
        self.isLoading = false
        self.productList += list
        
//        guard let pagination = data.pagination else {
//            return
//        }
//        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getAllProductFromServer(getAll: self.getAll)
    }

}

extension PreOrderableProductViewController{
    func selectedProductAlert(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

