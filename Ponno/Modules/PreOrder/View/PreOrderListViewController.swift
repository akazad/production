//
//  PreOrderListViewController.swift
//  Ponno
//
//  Created by a k azad on 28/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class PreOrderListViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    @IBOutlet weak var dateSearchHeight: NSLayoutConstraint!
    
    private var presenter = PreOrdersPresenter(service: PurchaseService())
    
    var preOrders : [PreOrders] = [] {
        didSet{
            self.refreshTableView()
        }
    }
    
    var preOrdersSummary : [Summary] = []{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var isSearchActive = false {
        didSet{
            self.refreshTableView()
        }
    }
    
    var purchaseId: Int?
    var preOrderId: Int?
    var preOrder : PreOrders?
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    var datePicker = UIDatePicker()
    var searchPage : Int = 1
    
    var searchEnable : Bool = false
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.configureTableView()
        self.configureCollectionView()
        self.initialSetup()
        self.setBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func initialSetup(){
        self.title = LanguageManager.PreOrder
        self.dateSearchHeight.constant = 0.0
        //self.setBarButton()
        self.addFloaty()
        self.showStartDatePicker()
        self.showEndDatePicker()
        self.searchBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarButton(){
        
        let dateSearchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        dateSearchBtn.setImage(UIImage(named: "dateSearch"), for: UIControl.State.normal)
        dateSearchBtn.addTarget(self, action: #selector(onDateSearch(sender:)), for: UIControl.Event.touchUpInside)
        dateSearchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        dateSearchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        dateSearchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: dateSearchBtn)
        
        let addBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        addBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        addBtn.addTarget(self, action: #selector(onPreOrderAdd(sender:)), for: UIControl.Event.touchUpInside)
        addBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        addBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        addBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: addBtn)
        
        navigationItem.setRightBarButtonItems([barButton2,barButton1], animated: true)
        
        self.navigationItem.rightBarButtonItem = barButton2
    }
    
    @objc func onDateSearch(sender: UIBarButtonItem){
        if searchEnable == false{
            self.dateSearchHeight.constant = 30.0
            self.searchEnable = !searchEnable
        }else{
            self.dateSearchHeight.constant = 0.0
            searchEnable = !searchEnable
        }
    }
    
    @objc func onAdd(sender: UIButton){
        self.navigateToPreOrderableProductSearchVC()
    }
    
    func navigateToPreOrderableProductSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PreOrderableProductViewController") as! PreOrderableProductViewController
        viewController.preOrders = self.preOrder
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @objc func onPreOrderAdd(sender: UIButton){
        self.navigateToPreOrderableProductSearchVC()
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
}


extension PreOrderListViewController: FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}

//Mark: Search
extension PreOrderListViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            self.searchPage = 1
            self.preOrders = []
            self.isSearchActive = true
            self.presenter.getPreOrderSearchDataFromServer(page: searchPage, startDate: startDate, endDate: endDate)
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

//MARK: CollectionView Delegate And Data Source
extension PreOrderListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(PurchaseSummeryCell.nib, forCellWithReuseIdentifier: PurchaseSummeryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard self.preOrdersSummary.count > 0 else{
            return 0
        }
        return self.preOrdersSummary.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PurchaseSummeryCell = collectionView.dequeueReusableCell(withReuseIdentifier: PurchaseSummeryCell.identifier, for: indexPath) as! PurchaseSummeryCell
        guard self.preOrdersSummary.count > 0 else {
            return cell
        }
        let summeryItem = self.preOrdersSummary[indexPath.row]
        
//        for (key, _) in self.preOrdersSummary.enumerated() {
//            if key == 0 {
//                self.preOrdersSummary[0].title = "Current Pre-order"
//            }else if key == 1 {
//                self.preOrdersSummary[1].title = "Current Pre-order Amount"
//            }else if key == 2 {
//                self.preOrdersSummary[2].title = "Amount to be paid"
//            }
//        }
        
        cell.summeryName.text = summeryItem.title
        cell.summeryNumber.text = summeryItem.value
        return cell
    }
    
    func refreshCollectionView(){
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70.0)
    }
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard self.preOrdersSummary.count > 0 else{
            return
        }
        let listCount = self.preOrdersSummary.count
        
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//Mark: TableViewDelegate
extension PreOrderListViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PreOrdersListCell.nib, forCellReuseIdentifier: PreOrdersListCell.identifier)
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 110.0
        self.automaticallyAdjustsScrollViewInsets = false
//        self.tableView.he
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.preOrders.count > 0 {
            return self.preOrders.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PreOrdersListCell = tableView.dequeueReusableCell(withIdentifier: PreOrdersListCell.identifier) as! PreOrdersListCell
        cell.selectionStyle = .none
        
        if self.preOrders.count > 0 {
            let item = self.preOrders[indexPath.row]
            if item.purchaseId != nil{
                cell.statusView.backgroundColor = UIColor.init(red: 90, green: 160, blue: 100)
                cell.statusLbl.text = LanguageManager.Received + ".."
                cell.statusLbl.textColor = UIColor.white
                cell.receiveDataLbl.text = LanguageManager.Total + " : " + item.total + Constants.currencySymbol
                cell.advanceAmountLbl.text = LanguageManager.OrderDate + " : " + item.orderDate
                cell.AmountToBePaidLbl.text = LanguageManager.ReceiveDate + " : " + item.receivedDate
                cell.preOrderPopUpBtn.isHidden = false
                cell.preOrderRemarks.isHidden = true
                
                cell.preOrderPopUpBtn.tag = item.id
                cell.preOrderPopUpBtn.addTarget(self, action: #selector(onPreOrderPopUpBtnTapped), for: .touchUpInside)
                
            }else{
                cell.statusView.backgroundColor = UIColor.lightRed
                cell.statusLbl.text = LanguageManager.Pending + ".."
                cell.statusLbl.textColor = UIColor.white
                cell.receiveDataLbl.text = LanguageManager.ReceiveDate + " : " + item.receivedDate
                cell.advanceAmountLbl.text = LanguageManager.AdvanceAmount + " : " + item.advance + Constants.currencySymbol
                cell.AmountToBePaidLbl.text = LanguageManager.AmountToBePaid + " : " + item.payable + Constants.currencySymbol
                cell.preOrderPopUpBtn.isHidden = false
                
                cell.preOrderRemarks.addTapGestureRecognizer {
                    let remarks = item.remarks
                    if remarks == "" {
                        self.showAlert(title: LanguageManager.NoRemarks, message: "")
                    }else{
                        self.showAlert(title: item.remarks, message: "")
                    }
                }
//                guard let id = item.id else{
//                    return cell
//                }
                cell.preOrderPopUpBtn.tag = item.id
                cell.preOrderPopUpBtn.addTarget(self, action: #selector(onPreOrderPopUpBtnTapped), for: .touchUpInside)
            }
            
            cell.vendorLbl.text = LanguageManager.Vendor + " : " + item.vendorName
            self.preOrder = item
            
            if isLoading == false && indexPath.row == self.preOrders.count - 1 && self.currentPage < self.lastPageNo {
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getPreOrdersDataFromServer(page: currentPage)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if self.preOrders.count > 0{
            let preOrderItem = self.preOrders[indexPath.row]
            let purchaseId = preOrderItem.purchaseId
            if purchaseId != nil{
                return []
            }else{
                let delete = UITableViewRowAction(style: .normal, title: LanguageManager.Delete) { action, index in
                    self.preOrderId = preOrderItem.id
                    
                    guard let id = self.preOrderId else{
                        return
                    }
                    self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                }
                delete.backgroundColor = UIColor.red
                
                return [delete]
            }
        }
        return [UITableViewRowAction()]
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.preOrders.count > 0 {
            let item = self.preOrders[indexPath.row]
            self.purchaseId = item.purchaseId
            if purchaseId != nil{
                guard let id = self.purchaseId else{
                    return
                }
                self.navigateToPurchaseDetailsVC(purchaseId: id)
            }
            else{
                self.navigateToPreOrderPendingViewController(preOrderId: item.id)
            }
        }
    }
    
    @objc func onPreOrderPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.preOrders.count > 0{
            let itemId = sender.tag
            for item in self.preOrders{
                if itemId == item.id{
                    let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                         self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: itemId)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToPreOrderPendingViewController(preOrderId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PreOrderPendingViewController") as! PreOrderPendingViewController
        viewController.preOrderId = preOrderId
        print(preOrderId)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToPurchaseDetailsVC(purchaseId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseDetailsViewController") as! PurchaseDetailsViewController
        viewController.selectedId = purchaseId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.postPreOrdersCancelDataToServer(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.currentPage = 1
            self.preOrders = []
            self.refreshTableView()
            self.presenter.getPreOrdersDataFromServer(page: self.currentPage)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension PreOrderListViewController : PreOrdersViewDelegate{
    
    func setPreOrdersData(data: PreOrdersDataMapper) {
        guard let preOrderData = data.preorders, preOrderData.count > 0 else {
            return
        }
        self.preOrders += preOrderData
        
        guard let summary = data.summary, summary.count > 0, let pagination = data.pagination else{
            return
        }
        self.preOrdersSummary = summary
        self.setUpAutoScroll()
        
        self.lastPageNo = pagination.lastPageNo
    }
    
    func preOrdersCancelData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        //self.showAlert(title: message, message: "")
        self.successMessage(userMessage: message)
        //self.refreshTableView()
    }
    
    func onFailed(message: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        currentPage = 1
        self.preOrders = []
        self.presenter.getPreOrdersDataFromServer(page: currentPage)
    }
}

extension PreOrderListViewController {
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
