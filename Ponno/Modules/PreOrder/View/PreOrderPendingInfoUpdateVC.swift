//
//  PreOrderPendingInfoUpdateVC.swift
//  Ponno
//
//  Created by a k azad on 22/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class PreOrderPendingInfoUpdateVC: UIViewController {

    @IBOutlet weak var advanceAmountLbl: UILabel!
    @IBOutlet weak var advanceAmountTextField: UITextField!
    @IBOutlet weak var vendorLbl: UILabel!
    @IBOutlet weak var vendorAddBtn: UIButton!{
        didSet{
            self.vendorAddBtn.setTitle(LanguageManager.NewVendor, for: .normal)
        }
    }
    @IBOutlet weak var vendorTextField: UITextField!
    @IBOutlet weak var remarksLbl: UILabel!
    @IBOutlet weak var remarksTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = PreOrderPendingInfoUpdatePresenter(service: PurchaseService())
    
    var vendorPicker = UIPickerView()
    var vendorsList : [VendorsList] = []{
        didSet{
            self.vendorPicker.reloadAllComponents()
        }
    }
    var vendorId : Int?
    
    var instanceOfPreOrderPendingVC : PreOrderPendingViewController?
    
    var preorder : PreOrderDetailsData?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleSetup()
        self.initialSetup()
        self.setUpPickerView()
        self.setupToolBar()
        self.attachPresenter()
    }
    
    func titleSetup(){
        self.setUpInitialLanguage()
        self.title = LanguageManager.UpdateInfo
        self.advanceAmountLbl.text = LanguageManager.AdvanceAmount
        self.vendorLbl.text = LanguageManager.VendorName
        self.remarksLbl.text = LanguageManager.Remarks
    }
    
    func initialSetup(){
        if let details = self.preorder{
            self.advanceAmountTextField.text = details.advance
            self.vendorTextField.text = details.vendorName
            self.remarksTextField.text = details.remarks
        }else{
            self.advanceAmountTextField.placeholder = LanguageManager.AdvanceAmount
            self.vendorTextField.placeholder = LanguageManager.SelectOne
            self.remarksTextField.placeholder = LanguageManager.Remarks
        }
        self.advanceAmountTextField.addTarget(self, action: #selector(onAdvanceAmountChange), for: .editingChanged)
        self.vendorAddBtn.addTarget(self, action: #selector(onVendorAddBtnPressed), for: .touchUpInside)
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
    }
    
    func setupToolBar(){
        let amountToolBar = UIToolbar()
        amountToolBar.barStyle = UIBarStyle.default
        amountToolBar.isTranslucent = true
        amountToolBar.tintColor = UIColor.black
        amountToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let amountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnAdvanceAmount(sender:)))
        
        amountToolBar.setItems([cancelButton,spaceButton, amountDoneButton], animated: false)
        amountToolBar.isUserInteractionEnabled = true
        
        self.advanceAmountTextField.inputAccessoryView = amountToolBar
    }
    
    func setUpPickerView(){
        vendorPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        self.vendorTextField.inputView = vendorPicker
        self.vendorTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.vendorTextField.resignFirstResponder()
        self.submitBtn.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnAdvanceAmount(sender : UIBarButtonItem){
        self.advanceAmountTextField.resignFirstResponder()
        self.vendorTextField.becomeFirstResponder()
    }
    
    @objc func onVendorAddBtnPressed(sender: UIButton){
        self.alertWithTextField()
    }
    
    @objc func onAdvanceAmountChange(sender: UITextField){
        if let details = self.preorder, let totalAmount = details.total.toDouble(){
            if let advanceAmount = self.advanceAmountTextField.text?.toDouble(){
                if totalAmount < advanceAmount{
                    self.advanceAmountTextField.text = "\(totalAmount)"
                }else{
                    self.advanceAmountTextField.text = sender.text
                }
            }
        }
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            if let advanceAmount = self.advanceAmountTextField.text{
                instanceOfPreOrderPendingVC?.advance = advanceAmount
                instanceOfPreOrderPendingVC?.remarks = self.remarksTextField.text ?? ""
                instanceOfPreOrderPendingVC?.vendorName = self.vendorTextField.text ?? ""
                instanceOfPreOrderPendingVC?.updateSummaryDetails()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func isValidated()->Bool{
        if self.vendorTextField.text == ""{
            self.showAlert(title: LanguageManager.VendorNameIsRequired, message: "")
            return false
        }
        return true
    }

}

//Mark: PickerViewDelegate
extension PreOrderPendingInfoUpdateVC : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        guard self.vendorsList.count > 0 else {
            return 0
        }
        return self.vendorsList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard self.vendorsList.count > 0 else {
            return ""
        }
        let vendors = self.vendorsList[row]
        //self.vendorTextField.text = vendors.name
        self.vendorId = vendors.id
        return vendors.name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard self.vendorsList.count > 0 else {
            return
        }
        let vendors = self.vendorsList[row]
        self.vendorTextField.text = vendors.name
        self.vendorId = vendors.id
    }
}

extension PreOrderPendingInfoUpdateVC{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewVendorAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.VendorName
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.PhoneNumber
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
           // self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showMessage(userMessage: LanguageManager.VendorNameIsRequired)
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                self.showMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
                return
            }
            
//            if(!self.validatePhoneNumber(value: phone)){
//                self.showMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
//            }
            
            let param : [String : Any] = ["name": textField.text!, "phone": textField2.text!, "address": ""]
            
            self.presenter.postNewVendorAddDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.alertWithTextField()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension PreOrderPendingInfoUpdateVC: PreOrderPendingInfoUpdateViewDelegate{
    func onSuccess(data: VendorAddDataMapper) {
        guard let vendor = data.vendor else {
            return
        }
        self.vendorId = vendor.id
        self.vendorTextField.text = vendor.name
        let newVendor = VendorsList.init(id: vendor.id, name: vendor.name, phone: vendor.phone)
        self.vendorsList.append(newVendor)
        self.vendorTextField.resignFirstResponder()
        guard let message = data.message else{
            return
        }
        self.showAlert(title: LanguageManager.Successful, message: message)
    }
    
    func setVendorsList(data: VendorsDataMapper) {
        guard let list = data.vendors, list.count > 0 else {
            return
        }
        self.vendorsList = list
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getVendorsListDataFromServer(getAll: true)
    }
}
