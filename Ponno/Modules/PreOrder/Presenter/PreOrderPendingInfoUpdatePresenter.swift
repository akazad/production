//
//  PreOrderPendingInfoUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 22/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation

protocol PreOrderPendingInfoUpdateViewDelegate : NSObjectProtocol {
    func setVendorsList (data: VendorsDataMapper)
    func onSuccess(data: VendorAddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class PreOrderPendingInfoUpdatePresenter: NSObject {
    private let service : PurchaseService
    weak private var viewDelegate : PreOrderPendingInfoUpdateViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PreOrderPendingInfoUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsListDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.getAllVendorsData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewVendorAddDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.postAddNewVendorData(vendorData: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}

