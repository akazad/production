//
//  PreOrderSummaryUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 1/7/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol PreOrderSummaryDelegate : NSObjectProtocol {
    func setVendorsList (data: VendorsDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class PreOrderSummaryUpdatePresenter: NSObject {
    private let service : VendorsService
    weak private var viewDelegate : PreOrderSummaryDelegate?
    
    init(service : VendorsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PreOrderSummaryDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsListDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllVendorsData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
