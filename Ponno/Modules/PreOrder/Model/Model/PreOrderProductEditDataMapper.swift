//
//  PreOrderProductEditDataMapper.swift
//  Ponno
//
//  Created by a k azad on 2/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class PreOrderProductEditDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var preorder : PreOrderDetailsData?
    var products : [PreOrderProductData]?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        preorder <- map["preorder"]
        products <- map["products"]
    }
    
}

