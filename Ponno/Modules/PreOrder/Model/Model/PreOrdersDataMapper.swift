//
//  PreOrdersDataMapper.swift
//  Ponno
//
//  Created by a k azad on 28/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PreOrdersDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var preorders : [PreOrders]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["Summary"]
        preorders <- map["preorders"]
        pagination <- map["pagination"]
    }
    
}

