//
//  PreOrderDetailsData.swift
//  Ponno
//
//  Created by a k azad on 2/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class PreOrderDetailsData : Mappable {
    var id : Int = 0
    var total : String = ""
    var purchaseId : Int = 0
    var orderDate : String = ""
    var receivedDate : String = ""
    var advance : String = ""
    var payable : String = ""
    var paid : String = "0.0"
    var cashBack : String = "0.0"
    var vendorId : Int = 0
    var vendorName: String = ""
    var remarks : String = ""
    
    required init(map: Map) {
        
    }
    
    init(id: Int,total : String, purchaseId : Int, orderDate : String, receivedDate : String, advance : String, payable: String, paid: String, cashBack: String, vendorId : Int, vendorName: String, remarks : String) {
        
        self.id = id
        self.total = total
        self.purchaseId = purchaseId
        self.orderDate = orderDate
        self.receivedDate = receivedDate
        self.advance = advance
        self.payable = payable
        self.paid = paid
        self.cashBack = cashBack
        self.vendorId = vendorId
        self.vendorName = vendorName
        self.remarks = remarks
        
    }
//    init(<#parameters#>) {
//        <#statements#>
//    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        total <- map["total"]
        purchaseId <- map["purchase_id"]
        orderDate <- map["order_date"]
        receivedDate <- map["received_date"]
        advance <- map["advance"]
        payable <- map["payable"]
        vendorId <- map["vendor_id"]
        vendorName <- map["vendor_name"]
        remarks <- map["remarks"]
    }
    
}
