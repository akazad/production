//
//  PreOrderProductData.swift
//  Ponno
//
//  Created by a k azad on 2/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class PreOrderProductData : Mappable {
    var id : Int = 0
    var preorderId : Int = 0
    var productId : Int = 0
    var quantity : String = ""
    var serialArray : String = "---"
    var buyingPrice : String = ""
    var sellingPrice : String = ""
    var createdAt : String = ""
    var productName: String = ""
    var updatedAt : String = ""
    var pharmacyId : Int = 0
    var hasSerial : Int = 0
    var serials : [String] = []
    
    init(productId : Int, quantity : String, buyingPrice : String, sellingPrice : String) {
        self.productId = productId
        self.quantity = quantity
        self.buyingPrice = buyingPrice
        self.sellingPrice = sellingPrice
    }
    
    init(productId : Int, name : String, quantity : String, buyingPrice : String, sellingPrice : String) {
        self.productId = productId
        self.quantity = quantity
        self.buyingPrice = buyingPrice
        self.sellingPrice = sellingPrice
        self.productName = name
    }
    
    init(id: Int, name: String, quantity: String, buyingPrice: String, sellingPrice: String, hasSerial: Int, serialArray : String) {
        self.productId = id
        self.productName = name
        self.quantity = quantity
        self.buyingPrice = buyingPrice
        self.sellingPrice = sellingPrice
        self.hasSerial = hasSerial
        self.serialArray = serialArray
    }
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        preorderId <- map["preorder_id"]
        productId <- map["product_id"]
        quantity <- map["quantity"]
        serialArray <- map["serial_array"]
        buyingPrice <- map["buying_price"]
        sellingPrice <- map["selling_price"]
        createdAt <- map["created_at"]
        productName <- map["product_name"]
        updatedAt <- map["updated_at"]
        pharmacyId <- map["pharmacy_id"]
        hasSerial <- map["has_serial"]
    }
    
}
