//
//  PreOrders.swift
//  Ponno
//
//  Created by a k azad on 28/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PreOrders : Mappable {
    var id : Int = 0
    var total : String = ""
    var purchaseId : Int?
    var orderDate : String = ""
    var receivedDate : String = ""
    var advance : String = ""
    var payable : String = ""
    var vendorId : Int = 0
    var addedBy : Int = 0
    var createdAt : String = ""
    var updatedAt : String = ""
    var pharmacyId : Int = 0
    var remarks : String = ""
    var purchaseInvoice : String = ""
    var vendorName : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        total <- map["total"]
        purchaseId <- map["purchase_id"]
        orderDate <- map["order_date"]
        receivedDate <- map["received_date"]
        advance <- map["advance"]
        payable <- map["payable"]
        vendorId <- map["vendor_id"]
        addedBy <- map["added_by"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        pharmacyId <- map["pharmacy_id"]
        remarks <- map["remarks"]
        purchaseInvoice <- map["purchase_invoice"]
        vendorName <- map["vendor_name"]
    }
    
}
