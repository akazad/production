//
//  DeliveryDuePresenter.swift
//  Ponno
//
//  Created by a k azad on 18/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation

protocol DeliveryDueViewDelegate : NSObjectProtocol {
    func onSuccess(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class DeliveryDuePresenter : NSObject {
    
    private var service : DeliveryService
    weak private var viewDelegate : DeliveryDueViewDelegate?
    
    init(service : DeliveryService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DeliveryDueViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postDeliveryOtherDueStoreData(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postDeliverySystemOtherDueStoreData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postDeliveryOtherDueWithdrawDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postDeliverySystemOtherDuePaidData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
//    func postDeliveryInvoiceRecieveToServer(param : [String: Any]){
//        self.viewDelegate?.showLoading()
//        self.service.postDeliveryInvoiceRecieveData(param: param, success: { (data) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onSuccess(data: data)
//        }, failure: { (message) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onFailed(data: message)
//        })
//    }
}

