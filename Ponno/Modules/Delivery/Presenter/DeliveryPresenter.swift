//
//  DeliveryPresenter.swift
//  Ponno
//
//  Created by a k azad on 31/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol DeliveryViewDelegate : NSObjectProtocol {
    func setDeliveryData(data: DeliveryDataMapper)
    func setDeliveryLog(data: DeliveryLogMapper)
    func addDeliverySystem(data: DeliverySystemAddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class DeliveryPresenter : NSObject {
    
    private var service : DeliveryService
    weak private var viewDelegate : DeliveryViewDelegate?
    
    init(service : DeliveryService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DeliveryViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getDeliveryDataFromServer(page : Int){
        self.viewDelegate?.showLoading()
        self.service.getDeliveryData(page: page, success: { (data) in
            self.viewDelegate?.setDeliveryData(data: data)
            self.viewDelegate?.hideLoading()
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getDeliveryLogFromServer(page: Int){
        let service = DeliveryService()
        service.getDeliveryLog(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDeliveryLog(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postAddNewDeliverySystemDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewDeliverySystemAddData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.addDeliverySystem(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}

