//
//  AddNewDeliverySystemPresenter.swift
//  Ponno
//
//  Created by a k azad on 10/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol AddNewDeliverySystemViewDelegate : NSObjectProtocol {
    func addDeliverySystem(data: DeliverySystemAddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class AddNewDeliverySystemPresenter : NSObject {
    
    private var service : DeliveryService
    weak private var viewDelegate : AddNewDeliverySystemViewDelegate?
    
    init(service : DeliveryService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AddNewDeliverySystemViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postAddNewDeliverySystemDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewDeliverySystemAddData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.addDeliverySystem(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
