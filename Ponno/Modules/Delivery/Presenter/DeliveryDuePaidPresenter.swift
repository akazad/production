//
//  DeliveryDuePaidPresenter.swift
//  Ponno
//
//  Created by a k azad on 15/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation

protocol DeliveryDuePaidViewDelegate : NSObjectProtocol {
    func onSuccess(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class DeliveryDuePaidPresenter : NSObject {
    
    private var service : DeliveryService
    weak private var viewDelegate : DeliveryDuePaidViewDelegate?
    
    init(service : DeliveryService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DeliveryDuePaidViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postDeliveryInvoiceRecieveToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postDeliveryInvoiceRecieveData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
