//
//  DeliverySystemUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 27/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol DeliverySystemUpdateViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class DeliverySystemUpdatePresenter: NSObject {
    
    private let service : DeliveryService
    weak private var viewDelegate : DeliverySystemUpdateViewDelegate?
    
    init(service : DeliveryService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DeliverySystemUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postDeliveryUpdateDataToServer(updateData: [String:Any]){
        self.viewDelegate?.showLoading()
        self.service.postDeliverySystemUpdateData(param: updateData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
