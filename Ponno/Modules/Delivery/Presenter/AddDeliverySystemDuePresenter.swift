//
//  AddDeliverySystemDuePresenter.swift
//  Ponno
//
//  Created by a k azad on 11/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol AddNewDeliveryDueViewDelegate : NSObjectProtocol {
    func setDeliverySystemList(data: DeliveryDataMapper)
    func setAddNewDeliverySystem(data: DeliverySystemAddDataMapper)
    func onSuccess(data : AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class AddNewDeliverySystemDuePresenter: NSObject {
    
    private let service : DeliveryService
    weak private var viewDelegate : AddNewDeliveryDueViewDelegate?
    
    init(service : DeliveryService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AddNewDeliveryDueViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getAllDeliverySystemListFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllDeliveryData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDeliverySystemList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postDeliveryDueDataToServer(deliveryDueData: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewDeliverySystemDueData(deliveryDueData: deliveryDueData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postDeliveryUpdateDataToServer(updateData: [String:Any]){
        self.viewDelegate?.showLoading()
        self.service.postDeliverySystemUpdateData(param: updateData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewDeliverySystemAddDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewDeliverySystemAddData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setAddNewDeliverySystem(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}

