//
//  DeliveryListSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 20/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol DeliveryListSearchViewDelegate : NSObjectProtocol {
    func setDeliveryListData(data: DeliveryDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class DeliveryListSearchPresenter : NSObject {
    
    private var service : DeliveryService
    weak private var viewDelegate : DeliveryListSearchViewDelegate?
    
    init(service : DeliveryService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DeliveryListSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getDeliverylistSearchDataFromServer(page : Int, searchText: String){
        self.viewDelegate?.showLoading()
        self.service.getDeliveryListSearchData(page: page, searchText: searchText, success: { (data) in
            self.viewDelegate?.setDeliveryListData(data: data)
            self.viewDelegate?.hideLoading()
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
