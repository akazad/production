//
//  DeliveryLogSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 20/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
protocol DeliveryLogSearchViewDelegate : NSObjectProtocol {
    func setDeliveryLog(data: DeliveryLogMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class DeliveryLogSearchPresenter : NSObject {
    
    private var service : DeliveryService
    weak private var viewDelegate : DeliveryLogSearchViewDelegate?
    
    init(service : DeliveryService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DeliveryLogSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getDeliveryLogFromServer(page: Int){
        let service = DeliveryService()
        service.getDeliveryLog(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDeliveryLog(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getDeliveryLogSearchDataFromServer(page: Int, startDate: String, endDate: String){
        self.viewDelegate?.showLoading()
        self.service.getDeliveryLogSearchData(page: page, startDate: startDate, endDate: endDate, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDeliveryLog(data: data) 
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
            })
    }
    
}
