//
//  DeliveryDetailsPresenter.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol DeliveryDetailsViewDelegate : NSObjectProtocol {
    func setDeliveryDetailsData(data: DeliveryDetailsDataMapper)
    func setDeliveryDetailsCurrentOrderData(data: DeliveryDetailsCurrentOrderDataMapper)
    func setDeliveryDetailsSaleHistroyData(data: DeliveryDetailsSaleHistoryDataMapper)
    func setDeliveryDetailsDueHistoryData(data: DeliveryDetailsDueHistroyDataMapper)
    func cancelDeliveryDetailsInvoice(data: AddDataMapper)
//    func setDeliveryLog(data: DeliveryLogMapper)
    func onFailed()
    func showLoading()
    func hideLoading()
}

class DeliveryDetailsPresenter : NSObject {
    
    private var service : DeliveryService
    weak private var viewDelegate : DeliveryDetailsViewDelegate?
    
    init(service : DeliveryService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DeliveryDetailsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getDeliveryDetailsDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getDeliveryDetailsData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDeliveryDetailsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
    
    func getDeliveryDetailsCurrentOrderDataFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getDeliveryDetailsCurrentOrder(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDeliveryDetailsCurrentOrderData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
    
    func getDeliveryDetailsSaleHistoryDataFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getDeliveryDetailsSaleHistory(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDeliveryDetailsSaleHistroyData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
    
    func getDeliveryDetailsDueHistoryDataFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getDeliveryDetailsDueHistory(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDeliveryDetailsDueHistoryData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
    
    func postDeliveryDetailsInvoiceToServer(id: Int){
        self.viewDelegate?.showLoading()
        let service =  SalesService()
        service.PostPerDaySaleCancelData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.cancelDeliveryDetailsInvoice(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
    
}
