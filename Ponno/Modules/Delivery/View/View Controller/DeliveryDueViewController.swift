//
//  DeliveryDueViewController.swift
//  Ponno
//
//  Created by a k azad on 18/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

enum DueType: String {
    case OtherDue
    case DuePaid
}

class DeliveryDueViewController: UIViewController {

    @IBOutlet weak var deliverySystemLabel: UILabel!
    @IBOutlet weak var deliverySystemListTextField: UITextField!
    @IBOutlet weak var deliverySystemCurrentDueLbl: UILabel!
    @IBOutlet weak var deliverySystemCurrentDueTextField: UITextField!
    @IBOutlet weak var deliverySystemDueAmountLabel: UILabel!
    @IBOutlet weak var deliverySystemDueAmountTextField: UITextField!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = DeliveryDuePresenter(service: DeliveryService())
    
    var deliverySystemId : Int?
    var details : DeliverySystemDetails?
    
    var dueType = DueType.OtherDue.rawValue
    var type: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.setupTitleLbl()
        self.setupToolBar()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.deliverySystemListTextField.underlined()
        self.deliverySystemCurrentDueTextField.underlined()
        self.deliverySystemDueAmountTextField.underlined()
        self.descriptionTextField.underlined()
        
        guard let data = self.details else{
            return
        }
        self.deliverySystemId = data.id
        self.deliverySystemListTextField.text = data.name
        self.deliverySystemCurrentDueTextField.text = "\(data.otherDue)"
        
        self.deliverySystemListTextField.isEnabled = false
        self.deliverySystemCurrentDueTextField.isEnabled = false
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
    }
    
    func setupTitleLbl(){
        self.setUpInitialLanguage()
        deliverySystemLabel.text = LanguageManager.DeliverySystem
        deliverySystemCurrentDueLbl.text = LanguageManager.CurrentDue
        descriptionLabel.text = LanguageManager.Description
        if type == 0{
            self.title = LanguageManager.OthersDue
            deliverySystemDueAmountLabel.text = LanguageManager.DueAmount
            print(LanguageManager.DueAmount)
        }else{
            self.title = LanguageManager.DueWithdraw
            deliverySystemDueAmountLabel.text = LanguageManager.DueWithdrawAmount
            print(LanguageManager.DueWithdrawAmount)
        }
    }
    
    func setupToolBar(){
        //DeliveryDueAmountToolBaar
        let deliveryDueToolBar = UIToolbar()
        deliveryDueToolBar.barStyle = UIBarStyle.default
        deliveryDueToolBar.isTranslucent = true
        deliveryDueToolBar.tintColor = UIColor.black
        deliveryDueToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let deliveryDueDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnDeliveryDue(sender:)))
        
        deliveryDueToolBar.setItems([cancelButton, spaceButton, deliveryDueDoneButton], animated: false)
        deliveryDueToolBar.isUserInteractionEnabled = true
        
        self.deliverySystemDueAmountTextField.inputAccessoryView = deliveryDueToolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnDeliveryDue(sender: UIBarButtonItem){
        self.deliverySystemDueAmountTextField.resignFirstResponder()
        self.descriptionTextField.becomeFirstResponder()
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
        if (self.deliverySystemDueAmountTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.AmountIsRequired)
            return false
        }
        return true
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        guard let id = self.deliverySystemId, let amount = self.deliverySystemDueAmountTextField.text else{
            return
        }
        let description = self.descriptionTextField.text ?? ""
        let param : [String: Any] = ["delivery_system_id" :  id, "amount" : amount, "description" : description]
        if type == 0{
            self.presenter.postDeliveryOtherDueStoreData(param: param)
        }else{
            self.presenter.postDeliveryOtherDueWithdrawDataToServer(param : param)
        }
        
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: userMessage, message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.deliverySystemDueAmountTextField.becomeFirstResponder()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension DeliveryDueViewController : DeliveryDueViewDelegate{
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
    
}
