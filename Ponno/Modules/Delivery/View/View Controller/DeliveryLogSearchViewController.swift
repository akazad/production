//
//  DeliveryLogSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 20/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DeliveryLogSearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    
    private var presenter = DeliveryLogSearchPresenter(service: DeliveryService())
    
    var datePicker = UIDatePicker()
    
    private var deliveryLog : [DeliveryDueLogList] = [] {
        didSet{
            self.refreshTableView()
        }
    }
    
    //Pagination Propertise
    var isLoading : Bool = false
    var currentPage : Int = 1
    var searchPage: Int = 1
    
    //var searchText: String = ""
    var lastPageNo: Int = 0
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetup()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.searchBtn.addTarget(self, action: #selector(onSubmit(sender:)), for: .touchUpInside)
        self.configureTableView()
        self.showStartDatePicker()
        self.showEndDatePicker()
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.currentPage = 1
                    self.presenter.getDeliveryLogFromServer(page: self.currentPage)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

extension DeliveryLogSearchViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            
            self.deliveryLog = []
            self.currentPage = 1
            //self.searchIsActive = true
            
            self.presenter.getDeliveryLogSearchDataFromServer(page: currentPage, startDate: startDate, endDate: endDate)
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

//MARK: TableView Delegate And DataSource
extension DeliveryLogSearchViewController : UITableViewDelegate, UITableViewDataSource {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //self.tableView.register(DeliverySystemsCell.nib, forCellReuseIdentifier: DeliverySystemsCell.identifier)
        self.tableView.register(DeliveryLogCell.nib, forCellReuseIdentifier: DeliveryLogCell.identifier)
        //self.tableView.estimatedRowHeight = 125
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        //self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.deliveryLog.count > 0{
            return self.deliveryLog.count
        }
        return 0

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : DeliveryLogCell = tableView.dequeueReusableCell(withIdentifier: DeliveryLogCell.identifier, for: indexPath) as! DeliveryLogCell
        cell.selectionStyle = .none
        if self.deliveryLog.count > 0 {
            let log = self.deliveryLog[indexPath.row]
            
            cell.deliveryLog = log
            
            cell.dateLabel.text = log.createdAt
            cell.amountLabel.text = LanguageManager.Amount + " " + "\(log.amount)" + " " + Constants.currencySymbol
            cell.deliverySystemLabel.text = LanguageManager.DeliverySystem + log.deliverySystem
            if log.type == 0 {
                cell.typeLabel.text = LanguageManager.Due
                cell.typeLabel.textColor = UIColor(red: CGFloat(220/255.0), green: CGFloat(53/255.0), blue: CGFloat(69/255.0), alpha: CGFloat(1.0))
                //cell.colorView.backgroundColor = UIColor(red: CGFloat(255/255.0), green: CGFloat(225/255.0), blue: CGFloat(228/255.0), alpha: CGFloat(1.0))
            }
            if log.type == 1 {
                cell.typeLabel.text = LanguageManager.Withdraw
                cell.typeLabel.textColor = UIColor(red: CGFloat(91/255.0), green: CGFloat(160/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1.0))
                //cell.colorView.backgroundColor = UIColor(red: CGFloat(241/255.0), green: CGFloat(255/255.0), blue: CGFloat(222/255.0), alpha: CGFloat(1.0))
            }
            
            
            if isLoading == false && indexPath.row == self.deliveryLog.count - 1 {
                if isSearchActive == false{
                    if self.currentPage < self.lastPageNo {
                        self.isLoading = true
                        self.currentPage += 1
                        self.presenter.getDeliveryLogFromServer(page: self.currentPage)
                    }
                }
                else{
                    if self.searchPage < self.lastPageNo{
                        self.isLoading = true
                        self.searchPage += 1
                        guard let startDate = self.datePickerTextField.text else{
                            return cell
                        }
                        guard let endDate = self.toDatePickerTextField.text else{
                            return cell
                        }
                     self.presenter.getDeliveryLogSearchDataFromServer(page: searchPage, startDate: startDate, endDate: endDate)
                    }
                }
            }
        }
        return cell

    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    //    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    //
    //        return true
    //    }
    //
    //    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    //
    //    }
    
    //    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    //        if isDeliveryLog{
    //            let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
    //
    //                if self.isSearchActive{
    //                    if self.filteredList.count > 0 {
    //                        let deliverySystem = self.filteredList[indexPath.row]
    //
    //                        self.navigateToUpdateDeliveryVC(deliverySystem: deliverySystem)
    //                    }
    //                }
    //
    //                if self.deliverySystems.count > 0{
    //                    let deliverySystem = self.deliverySystems[indexPath.row]
    //
    //                    self.navigateToUpdateDeliveryVC(deliverySystem: deliverySystem)
    //                }
    //            }
    //
    //            edit.backgroundColor = UIColor.orange
    //
    //            return [edit]
    //        }else{
    //            return[]
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.deliveryLog.count > 0 {
            let list = self.deliveryLog[indexPath.row]
            self.navigateToDeliveryDetailsVC(id: list.deliverySystemId)
        }
    }
    
    func navigateToDeliveryDetailsVC(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DBaseViewController") as! DBaseViewController
        viewController.deliverySystem = id
        //viewController.deliverySystemName = self.deliverySystemName
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToUpdateDeliveryVC(deliverySystem : DeliverySystems){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DeliverySystemUpdateViewController") as! DeliverySystemUpdateViewController
        viewController.deliverySystemData = deliverySystem
        //        viewController.customerState = CustomerState.Update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Api Delegate
extension DeliveryLogSearchViewController : DeliveryLogSearchViewDelegate{
    func setDeliveryLog(data: DeliveryLogMapper) {
        guard let deliveryLog = data.dueLog else {
            return
        }
        self.isLoading = false
        self.deliveryLog += deliveryLog
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.searchAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getDeliveryLogFromServer(page: self.currentPage)
    }
}
