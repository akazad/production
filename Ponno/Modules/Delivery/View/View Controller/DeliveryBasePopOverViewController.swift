//
//  DeliveryBasePopOverViewController.swift
//  Ponno
//
//  Created by a k azad on 18/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class DeliveryBasePopOverViewController: UIViewController {

    @IBOutlet weak var otherDueBtn: UIButton!
    @IBOutlet weak var duePaidBtn: UIButton!
    
    var deliverySystemId : Int?
    
    var navigateToDeliveryDueVC: NavigateToDeliveryDue?
    var type : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
    }
    
    func initialSetup(){
        self.setUpInitialLanguage()
        self.otherDueBtn.setTitle(LanguageManager.OthersDue, for: .normal)
        self.duePaidBtn.setTitle(LanguageManager.DueWithdraw, for: .normal)
        
        self.otherDueBtn.addTarget(self, action: #selector(onOtherDueBtnTapped), for: .touchUpInside)
        self.duePaidBtn.addTarget(self, action: #selector(onDuePaidBtnTapped), for: .touchUpInside)
    }
    
    @objc func onOtherDueBtnTapped(sender: UIButton){
        if let id = self.deliverySystemId{
            self.dismiss(animated: true, completion: nil)
            navigateToDeliveryDueVC?.navigateToDeliveryDueVC(id: id, type: 0)
        }
    }
    
    @objc func onDuePaidBtnTapped(sender: UIButton){
        if let id = self.deliverySystemId{
            self.dismiss(animated: true, completion: nil)
            navigateToDeliveryDueVC?.navigateToDeliveryDueVC(id: id, type: 1)
        }
    }

}
