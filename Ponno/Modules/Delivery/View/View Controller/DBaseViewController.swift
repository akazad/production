//
//  DBaseViewController.swift
//  Ponno
//
//  Created by a k azad on 27/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import FloatingPanel

protocol NavigateToDeliveryDue {
    func navigateToDeliveryDueVC(id: Int, type: Int)
}

class DBaseViewController: UIViewController, FloatingPanelControllerDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = DeliveryDetailsPresenter(service: DeliveryService())
    
    var fpc: FloatingPanelController!
    var bottomShetVC : DDetailsViewController!
    
    var deliverySystemDetails : DeliverySystemDetails?
    var deliverySystem : Int?
    var deliveryLog : String?
    
    var deliverySystemId : Int?
    var deliverySystemName : String?
    
    enum Sections : Int {
        case DeliveryInfo
        case DeliverySummary
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTableView()
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addBottomSheet()
        self.initialSetup()
        self.setBarBtn()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func addBottomSheet(){
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = UIColor.lightGreen.withAlphaComponent(0.5)
        fpc.surfaceView.layer.cornerRadius = 9.0
        fpc.surfaceView.clipsToBounds = true
        
        fpc.surfaceView.shadowHidden = false
        bottomShetVC = storyboard?.instantiateViewController(withIdentifier: "DDetailsViewController") as? DDetailsViewController
        bottomShetVC.deliverySystemId = self.deliverySystem
        bottomShetVC.deliverySystemName = self.deliverySystemName
        
        fpc.set(contentViewController: bottomShetVC)
        fpc.track(scrollView: bottomShetVC.tableView)
        fpc.addPanel(toParent: self)
    }
    
    func removeBottomSheet(){
        fpc.removePanelFromParent(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeBottomSheet()
    }
    
    func initialSetup(){
        self.title = LanguageManager.DeliverySystemProfile
        guard let details = self.deliverySystemDetails else{
            return
        }
        self.deliverySystemName = details.name
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarBtn(){
        let popUpBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        popUpBtn.setImage(UIImage(named: "popupmenudark"), for: UIControl.State.normal)
        popUpBtn.addTarget(self, action: #selector(onBarPopUpBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        popUpBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        popUpBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        popUpBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn3 = UIBarButtonItem(customView: popUpBtn)
        
        navigationItem.setRightBarButtonItems([barBtn3], animated: true)
    }
    
    @objc func onBarPopUpBtnTapped(sender: UIButton){
        self.popOverToDBasePopoverViewController(sender: sender)
    }
    
    
    func popOverToDBasePopoverViewController(sender: UIButton){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "DeliveryBasePopOverViewController") as? DeliveryBasePopOverViewController{
            controller.preferredContentSize = CGSize(width: 170, height: 120)
            controller.deliverySystemId = self.deliverySystem
            controller.navigateToDeliveryDueVC = self
            showPopup(controller, sourceView: sender)
        }
        
    }
    
}

//MARK: TableViewDelegate And DataSource
extension DBaseViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DeliveryDetailsInfoCell.nib, forCellReuseIdentifier: DeliveryDetailsInfoCell.identifier)
        self.tableView.register(DeliveryDetailsSummaryCell.nib, forCellReuseIdentifier: DeliveryDetailsSummaryCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.DeliveryInfo.rawValue:
            return 1
        case Sections.DeliverySummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case Sections.DeliveryInfo.rawValue:
            let cell: DeliveryDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier:DeliveryDetailsInfoCell.identifier, for: indexPath) as! DeliveryDetailsInfoCell
            cell.selectionStyle = .none
            guard let deliverySystem = self.deliverySystemDetails else {
                return cell
            }
            //        let item = deliverySystem[indexPath.row]
            cell.alphabetLabel.text = String(describing: Array(deliverySystem.name.capitalized)[0])
            cell.nameLabel.text = deliverySystem.name
            cell.phoneLabel.text = deliverySystem.phone
            //cell.duePaidBtn.addTarget(self, action: #selector(onDuePaidBtnPressed), for: .touchUpInside)
            return cell
            
        case Sections.DeliverySummary.rawValue:
            let cell: DeliveryDetailsSummaryCell = tableView.dequeueReusableCell(withIdentifier:DeliveryDetailsSummaryCell.identifier, for: indexPath) as! DeliveryDetailsSummaryCell
            
            cell.deliverySystemDetails = self.deliverySystemDetails
            
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.DeliveryInfo.rawValue:
            return 180.0
        case Sections.DeliverySummary.rawValue:
            return 260.0
        default:
            return 0
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onDuePaidBtnPressed(sender: UIButton){
        self.navigateToUpdateVC()
    }
  
    func navigateToUpdateVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDeliveryDueViewController") as! AddNewDeliveryDueViewController
        viewController.details = deliverySystemDetails
        viewController.id = self.deliverySystemId
        viewController.type = 1
        viewController.deliveryState = DeliveryState.Update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

//Mark: Api Delegate
extension DBaseViewController : DeliveryDetailsViewDelegate{
    func cancelDeliveryDetailsInvoice(data: AddDataMapper) {
        //Empty
    }
    
    func setDeliveryDetailsData(data: DeliveryDetailsDataMapper) {
        self.deliverySystemDetails = data.deliverySystem
        self.tableView.isHidden = false
        self.refreshTableView()
    }
    
    func setDeliveryDetailsCurrentOrderData(data: DeliveryDetailsCurrentOrderDataMapper) {
        
    }
    
    func setDeliveryDetailsSaleHistroyData(data: DeliveryDetailsSaleHistoryDataMapper) {
        
    }
    
    func setDeliveryDetailsDueHistoryData(data: DeliveryDetailsDueHistroyDataMapper) {
        
    }
    
    func onFailed() {
        //self.showAlert(title: "কোন তথ্য পাওয়া যায়নি", message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        
        guard let id = self.deliverySystem else{
            return
        }
        self.presenter.getDeliveryDetailsDataFromServer(id: id)
    }
    
}

extension DBaseViewController : NavigateToDeliveryDue{
    func navigateToDeliveryDueVC(id: Int, type: Int) {
        navigateToDeliveryDueViewController(id: id, type: type)
    }
    
    func navigateToDeliveryDueViewController(id: Int, type: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DeliveryDueViewController") as! DeliveryDueViewController
        viewController.deliverySystemId = id
        viewController.details = self.deliverySystemDetails
        viewController.modalPresentationStyle = .fullScreen
        viewController.type = type
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}



