//
//  DeliverySystemUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 15/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DeliverySystemUpdateViewController: UIViewController {

    
    @IBOutlet weak var deliverySystemLabel: UILabel!
    @IBOutlet weak var deliverySystemTextField: UITextField!
    @IBOutlet weak var deliverySystemPhoneLabel: UILabel!
    @IBOutlet weak var deliverySystemPhoneTextField: UITextField!{
        didSet{
            deliverySystemPhoneTextField.placeholder = LanguageManager.PhoneNumber
        }
    }
//    @IBOutlet weak var deliverySystemAddressLbl: UILabel!
//    @IBOutlet weak var deliverySystemAddressTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    private var presenter = DeliverySystemUpdatePresenter(service: DeliveryService())
    
    var deliverySystemData : DeliverySystems?
    var deliverySystemId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpToolBar()
        self.initialSetUp()
        self.attachPresenter()
    }
    
    func initialSetUp(){
        self.navigationController?.navigationBar.topItem?.title = " "
        self.submitButton.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        
        self.deliverySystemLabel.text = LanguageManager.DeliverySystem
        self.deliverySystemPhoneLabel.text = LanguageManager.PhoneNumber
        
        //self.deliverySystemAddressLbl.text = "ডেলিভারি পদ্ধতির ঠিকানা"
//        self.deliverySystemAddressLbl.isHidden = true
//        self.deliverySystemAddressTextField.isHidden = true
        guard let data = deliverySystemData else {
            return
        }
        self.deliverySystemTextField.text = data.name
        self.deliverySystemPhoneTextField.text = data.phone
        //self.deliverySystemAddressTextField.text = data.
        self.deliverySystemId = data.id
        //self.deliverySystemAddressTextField.text = data.
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            //self.submitBtnControlWith(isEnabled: false)
            guard let name = self.deliverySystemTextField.text, let phone = self.deliverySystemPhoneTextField.text, let id = self.deliverySystemId else {
                return
            }
            let updateParam : [String: Any] = ["id": id,"name": name, "phone" : phone]
            
            self.presenter.postDeliveryUpdateDataToServer(updateData: updateParam)
        }
    }
    
//    @IBAction func submitButton(_ sender: UIButton) {
//        if isValidated(){
//            //self.submitBtnControlWith(isEnabled: false)
//            guard let name = self.deliverySystemTextField.text, let phone = self.deliverySystemPhoneTextField.text, let address = self.deliverySystemAddressTextField.text, let id = self.deliverySystemId else {
//                return
//            }
//            let updateParam : [String: Any] = ["id": id,"name": name, "phone" : phone, "address" : address]
//            
//            self.presenter.postDeliveryUpdateDataToServer(updateData: updateParam)
//        }
//    }
//    
    func setUpToolBar(){
        
        //DeliveryDueAmountToolBaar
        let phoneToolBar = UIToolbar()
        phoneToolBar.barStyle = UIBarStyle.default
        phoneToolBar.isTranslucent = true
        phoneToolBar.tintColor = UIColor.black
        phoneToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let phoneDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnPhone(sender:)))
        
        phoneToolBar.setItems([cancelButton, spaceButton, phoneDoneButton], animated: false)
        phoneToolBar.isUserInteractionEnabled = true
        
        self.deliverySystemPhoneTextField.inputAccessoryView = phoneToolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnPhone(sender: UIBarButtonItem){
        self.deliverySystemPhoneTextField.resignFirstResponder()
        self.submitButton.becomeFirstResponder()
    }
    
//    @objc func onPressingDoneOnDeliveryDetails(sender: UIBarButtonItem){
//        self.deliverySystemDetailsView.resignFirstResponder()
//        self.submitBtn.becomeFirstResponder()
//    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitButton.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
        if (self.deliverySystemTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.DeliverySystemIsRequired)
            return false
        }
        else if (self.deliverySystemPhoneTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
            return false
        }
        guard let phone = deliverySystemPhoneTextField.text else {
            //self.displayMessage(userMessage: "Please enter a phone number")
            return false
        }
        if (!validatePhoneNumber(value: phone)){
            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
            return false
        }
        return true
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Api Delegate
extension DeliverySystemUpdateViewController : DeliverySystemUpdateViewDelegate{
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.displayMessage(userMessage: data)
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}
