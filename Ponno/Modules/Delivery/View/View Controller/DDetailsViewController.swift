//
//  DDetailsViewController.swift
//  Ponno
//
//  Created by a k azad on 27/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DDetailsViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!{
        didSet{
            segmentedControl.setTitle(LanguageManager.CurrentOrder, forSegmentAt: 0)
            segmentedControl.setTitle(LanguageManager.Invoice, forSegmentAt: 1)
            segmentedControl.setTitle(LanguageManager.DueHistory, forSegmentAt: 2)
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessage: UILabel!{
        didSet{
            emptyMessage.text = LanguageManager.NoInformationFound
        }
    }
    @IBOutlet weak var duePaidView: UIView!
    @IBOutlet weak var duePaidViewHeight: NSLayoutConstraint!
    @IBOutlet weak var invoiceNumberLbl: UILabel!
    @IBOutlet weak var duePaidAmountLbl: UILabel!
    @IBOutlet weak var duePaidBtn: UIButton!
    
    var isFirst = true
    var selectedSegment : Int?
    
    private var presenter = DeliveryDetailsPresenter(service: DeliveryService())
    
    var deliverySystemId : Int?
    var currentOrderId: Int?
    var saleHistoryId: Int?
    var invoice: String?
    var deliverySystemName : String?
    
    var currentOrder : [DeliveryDetailsCurrentOrder] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var saleHistory : [DeliveryDetailsSaleHistory] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var dueHistory : [DeliveryDetailsDueHistory] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isLoading : Bool = false
    var currentPage: Int = 1
    var currentOrderLastPage : Int = 1
    var invoiceLastPage : Int = 1
    var dueHistoryLastPage : Int = 1
    var currentOrderIds : [Int] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var totalCurrentOrderDue: Double = 0
    
    let checkedIcon = UIImage(named: "check_icon.png")
    let uncheckedIcon = UIImage(named: "uncheck_icon")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.setupDuePaidView()
        self.emptyMessage.isHidden = true
        self.configureTableView()
    }
    
    func initialSetup(){
        self.segmentedControl.addTarget(self, action: #selector(onSegmentChange), for: .valueChanged)
        self.hideDuePaidView(isHidden: true)
        self.duePaidBtn.addTarget(self, action: #selector(onDuePaidBtnTapped), for: .touchUpInside)
    }
    
    func setupDuePaidView(){
        self.setUpCardView(uiview: self.duePaidView)
        self.invoiceNumberLbl.text = "\(self.currentOrderIds.count)" + " " + "টি রসিদ নির্বাচিত হয়েছে"
        self.duePaidAmountLbl.text = LanguageManager.TotalDue + " : " + "\(self.totalCurrentOrderDue)" + " " + Constants.currencySymbol
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    @objc func onSegmentChange(sender: UISegmentedControl){
        
        guard let id = self.deliverySystemId else{
            return
        }
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            self.selectedSegment = segmentedControl.selectedSegmentIndex
            if self.currentOrderIds.count > 0{
                self.hideDuePaidView(isHidden: false)
            }else{
                self.hideDuePaidView(isHidden: true)
            }
            self.currentOrder = []
            self.isLoading = true
            self.currentPage = 1
            self.presenter.getDeliveryDetailsCurrentOrderDataFromServer(page: self.currentPage, id: id)
            self.refreshTableView()
        case 1:
            self.selectedSegment = segmentedControl.selectedSegmentIndex
            self.hideDuePaidView(isHidden: true)
            self.currentOrderIds = []
            self.saleHistory = []
            self.isLoading = true
            self.currentPage = 1
            self.presenter.getDeliveryDetailsSaleHistoryDataFromServer(page: self.currentPage, id: id)
            self.refreshTableView()
        case 2:
            self.selectedSegment = segmentedControl.selectedSegmentIndex
            self.hideDuePaidView(isHidden: true)
            self.currentOrderIds = []
            self.dueHistory = []
            self.isLoading = true
            self.currentPage = 1
            self.presenter.getDeliveryDetailsDueHistoryDataFromServer(page: self.currentPage, id: id)
            self.refreshTableView()
        default:
            break;
        }
    }
    
    @objc func onDuePaidBtnTapped(sender: UIButton){
        for item in self.currentOrder{
            if self.currentOrderIds.contains(item.id){
                self.navigateToCurrentOrderVC(currentOrder: item)
                return
            }
        }
    }
 
}

//Mark: TableView Delegate And Data Source
extension DDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = .clear
        self.tableView.register(DeliveryCurrentOrderCell.nib, forCellReuseIdentifier: DeliveryCurrentOrderCell.identifier)
        self.tableView.register(DeliveryInvoiceCell.nib, forCellReuseIdentifier: DeliveryInvoiceCell.identifier)
        self.tableView.register(DeliveryDueHistoryCell.nib, forCellReuseIdentifier: DeliveryDueHistoryCell.identifier)
        self.emptyMessage.isHidden = true
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl.selectedSegmentIndex == 0{
            if self.currentOrder.count > 0 {
                self.emptyMessage.isHidden = true
                return self.currentOrder.count
            }
//            else{
//                self.emptyMessage.isHidden = false
//            }
        }
        else if segmentedControl.selectedSegmentIndex == 1{
            if self.saleHistory.count > 0 {
                self.emptyMessage.isHidden = true
                return self.saleHistory.count
            }
//            else{
//                self.emptyMessage.isHidden = false
//            }
        }
        else if segmentedControl.selectedSegmentIndex == 2{
            if self.dueHistory.count > 0 {
                self.emptyMessage.isHidden = true
                return self.dueHistory.count
            }
//            else{
//                self.emptyMessage.isHidden = false
//            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if segmentedControl.selectedSegmentIndex == 0{
            let cell : DeliveryCurrentOrderCell = tableView.dequeueReusableCell(withIdentifier: DeliveryCurrentOrderCell.identifier, for: indexPath) as! DeliveryCurrentOrderCell
            cell.selectionStyle = .none
            
            if  self.currentOrder.count > 0 {
                let item = self.currentOrder[indexPath.row]
                cell.createdAtLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                if item.saleToken.isEmpty{
                    cell.invoiceLabel.text = item.invoice
                }else{
                    cell.invoiceLabel.text = item.saleToken
                }
                
                cell.sellerLabel.text = LanguageManager.By + " : " + item.addedBy
                cell.quantityLabel.text = LanguageManager.Quantity + " : " + "\(item.amount)" + Constants.currencySymbol
                
                if self.currentOrderIds.count > 0{
                    self.hideDuePaidView(isHidden: false)
                }else{
                    self.hideDuePaidView(isHidden: true)
                }
                
                if self.currentOrderIds.contains(item.id){
                    cell.checkBtn.setImage(checkedIcon, for: .normal)
                }else{
                    cell.checkBtn.setImage(uncheckedIcon, for: .normal)
                }
                
                cell.checkBtn.tag = item.id
                cell.checkBtn.addTarget(self, action: #selector(onCellCheckBtnTapped), for: .touchUpInside)
//                cell.popUpBtn.tag = item.id
//                cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
                
                //Pagination
                if isLoading == false && indexPath.row == self.currentOrder.count - 1 && self.currentPage < self.currentOrderLastPage {
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.deliverySystemId else{
                        return cell
                    }
                    self.presenter.getDeliveryDetailsCurrentOrderDataFromServer(page: self.currentPage, id: id)
                }
                
                return cell
            }
            
        }
        else if segmentedControl.selectedSegmentIndex == 1{
            let cell : DeliveryInvoiceCell = tableView.dequeueReusableCell(withIdentifier: DeliveryInvoiceCell.identifier, for: indexPath) as! DeliveryInvoiceCell
            cell.selectionStyle = .none
            self.hideDuePaidView(isHidden: true)
            if self.saleHistory.count > 0 {
                let item = self.saleHistory[indexPath.row]
                cell.createdAtLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.deliveryChargeLabel.text = LanguageManager.DeliveryCharge + " : " +  "\(item.deliveryCharge)" + " " + Constants.currencySymbol
                if item.saleToken.isEmpty{
                    cell.invoiceLabel.text = item.invoice
                }else{
                    cell.invoiceLabel.text = item.saleToken
                }
                
                let dueText = item.due
                let due = Double(dueText) ?? 0.0
                if due > 0.0 {
                    cell.invoiceLabel.textColor = UIColor.lightRed
                }else{
                    cell.invoiceLabel.textColor = UIColor.deepGreen
                }
                
                cell.paidLabel.text = LanguageManager.TotalPaid + " : " + "\(item.paid)" + " " + Constants.currencySymbol
                cell.dueLabel.text = LanguageManager.Due + " : " + "\(item.due)" + " " + Constants.currencySymbol
                
                if item.due != "0.00"{
                    cell.invoicePopUpBtn.tag = item.id
                    cell.invoicePopUpBtn.addTarget(self, action: #selector(onInvoicePopUpBtnTapped), for: .touchUpInside)
                    cell.invoicePopUpBtn.isHidden = false
                }else{
                    cell.invoicePopUpBtn.isHidden = true
                }
                
                
                //Pagination
                if isLoading == false && indexPath.row == self.saleHistory.count - 1 && self.currentPage < self.invoiceLastPage {
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.deliverySystemId else{
                        return cell
                    }
                    self.presenter.getDeliveryDetailsSaleHistoryDataFromServer(page: self.currentPage, id: id)
                }
            }
            return cell
        }
        else if segmentedControl.selectedSegmentIndex == 2{
            let cell : DeliveryDueHistoryCell = tableView.dequeueReusableCell(withIdentifier: DeliveryDueHistoryCell.identifier, for: indexPath) as! DeliveryDueHistoryCell
            cell.selectionStyle = .none
            self.hideDuePaidView(isHidden: true)
            if self.dueHistory.count > 0 {
                let item = self.dueHistory[indexPath.row]
                cell.createdAtLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.quantityLabel.text = LanguageManager.Amount + " : " + "\(item.amount)" + " " + Constants.currencySymbol
                cell.sellerLabel.text = LanguageManager.By + " : " + item.seller
                if item.type == 0 {
                    cell.typeLabel.text = LanguageManager.Due
                    cell.typeLabel.textColor = UIColor(red: CGFloat(220/255.0), green: CGFloat(53/255.0), blue: CGFloat(69/255.0), alpha: CGFloat(1.0))
                }
                if item.type == 1 {
                    cell.typeLabel.text = LanguageManager.Withdraw
                    cell.typeLabel.textColor = UIColor(red: CGFloat(91/255.0), green: CGFloat(160/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1.0))
                }
                
                //Pagination
                if isLoading == false && indexPath.row == self.dueHistory.count - 1 && self.currentPage < self.dueHistoryLastPage{
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.deliverySystemId else{
                        return cell
                    }
                    self.presenter.getDeliveryDetailsDueHistoryDataFromServer(page: self.currentPage, id: id)
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onCellCheckBtnTapped(sender: UIButton){
        for item in self.currentOrder{
            if sender.tag == item.id{
                if self.currentOrderIds.contains(sender.tag){
                    self.currentOrderIds = self.currentOrderIds.filter{$0 != sender.tag}
                    if let amount = item.amount.toDouble(){
                        self.totalCurrentOrderDue -= amount
                    }
                }else{
                    self.currentOrderIds.append(sender.tag)
                    if let amount = item.amount.toDouble(){
                        self.totalCurrentOrderDue += amount
                    }
                }
            }
            self.setupDuePaidView()
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if segmentedControl.selectedSegmentIndex == 0 {
            return 85.0
        }
        else if segmentedControl.selectedSegmentIndex == 1 {
            return 108.0
        }
        else if segmentedControl.selectedSegmentIndex == 2 {
            return 75.0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if segmentedControl.selectedSegmentIndex == 0 {
            let duePaid = UITableViewRowAction(style: .normal, title: "বাকি পূরণ") { action, index in
                if self.currentOrder.count > 0 {
                    let currentOder = self.currentOrder[indexPath.row]
                    self.currentOrderId = currentOder.id
                    self.navigateToCurrentOrderVC(currentOrder: currentOder)
                }
            }
            
            duePaid.backgroundColor = UIColor.orange
            
            return [duePaid]
            
        }else if segmentedControl.selectedSegmentIndex == 1{
            let delete = UITableViewRowAction(style: .normal, title: "বাতিল") { action, index in
                
                if self.saleHistory.count > 0{
                    let saleHistory = self.saleHistory[indexPath.row]
                    self.saleHistoryId = saleHistory.id
                    
                    guard let id = self.saleHistoryId else{
                        return
                    }
                    self.confirmationMessage(userMessage: "আপনি কি নিশ্চিত?", deleteId: id)
                }
                
            }
            delete.backgroundColor = UIColor.red
            
            return [delete]
        }
        
        return []
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if segmentedControl.selectedSegmentIndex == 0 {
            if self.currentOrder.count > 0 {
                let currentOrder = self.currentOrder[indexPath.row]
                self.currentOrderId = currentOrder.id
//                guard let id = self.currentOrderId else{
//                    return
//                }
                let saleId = currentOrder.saleId
                self.navigateToSaleDetailsVC(iD : saleId)
            }
        }
        if segmentedControl.selectedSegmentIndex == 1{
            if self.saleHistory.count > 0 {
                let saleHistory = self.saleHistory[indexPath.row]
                self.saleHistoryId = saleHistory.id
                guard let id = self.saleHistoryId else{
                    return
                }
                self.navigateToSaleDetailsVC(iD : id)
            }
        }
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.currentOrder.count > 0 {
            self.currentOrderId = sender.tag
            for item in self.currentOrder{
                if self.currentOrderId == item.id{
                    let duePaidAction = UIAlertAction(title: LanguageManager.InvoiceReceive, style: UIAlertAction.Style.default) { (action) in
                         self.navigateToCurrentOrderVC(currentOrder: item)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(duePaidAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    @objc func onInvoicePopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.saleHistory.count > 0{
            self.saleHistoryId = sender.tag
            for item in self.saleHistory{
                if self.saleHistoryId == item.id{
                    let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        guard let id = self.saleHistoryId else{
                            return
                        }
                        self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        viewController.iD = iD
        //viewController.state = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToCurrentOrderVC(currentOrder: DeliveryDetailsCurrentOrder){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DeliveryDuePaidViewController") as! DeliveryDuePaidViewController
        viewController.deliveryCurrentOrder = currentOrder
        let stringRepresentation = (self.currentOrderIds.map{String($0)}).joined(separator: ",")
        viewController.currentOrderId = stringRepresentation
        viewController.totalDue = self.totalCurrentOrderDue
        //viewController.invoice = currentOrder.invoice
        viewController.deliverySystemName = currentOrder.deliverySystemName
        viewController.deliverySystemId = currentOrder.deliverySystem
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: LanguageManager.Yes, style: .default){
            (action:UIAlertAction!) in
            
            //self.showAlert(title: "কাজটি অসম্পূর্ণ", message: "")
            //self.presenter.postProductRecentStockDeleteDataFromServer(id: deleteId)
            self.presenter.postDeliveryDetailsInvoiceToServer(id: deleteId)
            
        }
        let cancelAction = UIAlertAction(title: LanguageManager.No, style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func hideDuePaidView(isHidden: Bool){
        self.duePaidView.isHidden = isHidden
        self.duePaidAmountLbl.isHidden = isHidden
        self.duePaidBtn.isHidden = isHidden
        self.invoiceNumberLbl.isHidden = isHidden
        if isHidden == true{
            self.duePaidViewHeight.constant = 0.01
        }else{
            self.duePaidViewHeight.constant = 60.0
        }
    }
    
}

//Mark: Api Delegate
extension DDetailsViewController : DeliveryDetailsViewDelegate{
    func cancelDeliveryDetailsInvoice(data: AddDataMapper) {
//        guard let message = data.message else {
//            return
//        }
        self.displayMessage(userMessage: data.message ?? "")
    }
    
    
    func setDeliveryDetailsCurrentOrderData(data: DeliveryDetailsCurrentOrderDataMapper) {
        guard let currentOrderItem = data.orders, currentOrderItem.count > 0 else{
            return
        }
        self.selectedSegment = 0
        self.isLoading = false
        self.currentOrder += currentOrderItem
        guard let pagination = data.pagination else{
            return
        }
        self.currentOrderLastPage = pagination.lastPageNo
    }
    
    func setDeliveryDetailsSaleHistroyData(data: DeliveryDetailsSaleHistoryDataMapper) {
        guard let saleHistoryItem = data.sales, saleHistoryItem.count > 0 else {
            return
        }
        self.isLoading = false
        self.saleHistory += saleHistoryItem
        guard let pagination = data.pagination else{
            return
        }
        self.invoiceLastPage = pagination.lastPageNo
    }
    
    func setDeliveryDetailsDueHistoryData(data: DeliveryDetailsDueHistroyDataMapper) {
        guard let dueHistoryItem = data.dues, dueHistoryItem.count > 0 else {
            return
        }
        self.isLoading = false
        self.dueHistory += dueHistoryItem
        guard let pagination = data.pagination else{
            return
        }
        self.dueHistoryLastPage = pagination.lastPageNo
    }
    
    func setDeliveryDetailsData(data: DeliveryDetailsDataMapper) {
        //        self.delegate.updateDeliveryData(data: data)
        
    }
    
    func onFailed() {
    }
    
    func showLoading() {
        self.emptyMessage.isHidden = true
        self.showLoader()
        self.tableView.isHidden = true
    }
    
    func hideLoading() {
        self.emptyMessage.isHidden = false
        self.hideLoader()
        self.tableView.isHidden = false
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        guard let id = self.deliverySystemId else{
            return
        }
        self.presenter.getDeliveryDetailsCurrentOrderDataFromServer(page: self.currentPage, id: id)
    }
}

extension DDetailsViewController {
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.saleHistory = []
            self.refreshTableView()
            
            guard let id = self.deliverySystemId else{
                return
            }
           self.presenter.getDeliveryDetailsSaleHistoryDataFromServer(page: self.currentPage, id: id)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
