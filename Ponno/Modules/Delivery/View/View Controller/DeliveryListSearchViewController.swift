//
//  DeliveryListSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 20/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DeliveryListSearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = DeliveryListSearchPresenter(service: DeliveryService())
    
    private var deliverySystems : [DeliverySystems] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    //Pagination Propertise
    var isLoading : Bool = false
    var currentPage : Int = 1
    
    var searchText: String = ""
    var lastPageNo: Int = 0
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpSearchBar()
        self.configureTableView()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
        self.setUpInitialLanguage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.getDeliverylistSearchDataFromServer(page: self.currentPage, searchText: "")
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Search Delegate
extension DeliveryListSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.DeliverySystemSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.deliverySystems = []
        self.isLoading = true
        self.isSearchActive = true
        
        self.presenter.getDeliverylistSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
    }
}

//MARK: TableView Delegate And DataSource
extension DeliveryListSearchViewController : UITableViewDelegate, UITableViewDataSource {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DeliverySystemsCell.nib, forCellReuseIdentifier: DeliverySystemsCell.identifier)
        self.tableView.register(DeliveryLogCell.nib, forCellReuseIdentifier: DeliveryLogCell.identifier)
        //self.tableView.estimatedRowHeight = 125
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.deliverySystems.count > 0{
            return self.deliverySystems.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DeliverySystemsCell = tableView.dequeueReusableCell(withIdentifier: DeliverySystemsCell.identifier, for: indexPath) as! DeliverySystemsCell
        cell.selectionStyle = .none
        if self.deliverySystems.count > 0 {
            let deliveryData = self.deliverySystems[indexPath.row]
            
            cell.deliverySystems = deliveryData
            
            cell.nameLabel.text = deliveryData.name
            let firstAlphabet = Array(deliveryData.name.capitalized)[0]
            cell.alphabetLabel.text = String(describing: firstAlphabet)
            cell.phoneLabel.text = deliveryData.phone
            cell.currentDueLabel.text = "\(deliveryData.currentDue)"
            
            cell.popUpBtn.tag = deliveryData.id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.deliverySystems.count - 1 && self.currentPage < self.lastPageNo {
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getDeliverylistSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
            }
        }
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            
            if self.deliverySystems.count > 0{
                let deliverySystem = self.deliverySystems[indexPath.row]
                self.navigateToUpdateDeliveryVC(deliverySystem: deliverySystem)
            }
        }
        
        edit.backgroundColor = UIColor.orange
        
        return [edit]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.deliverySystems.count > 0 {
            let deliverySystems = self.deliverySystems[indexPath.row]
            self.navigateToDeliveryDetailsVC(id:  deliverySystems.id)
        }
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.deliverySystems.count > 0 {
            let deliverySystemId = sender.tag
            for item in self.deliverySystems{
                if deliverySystemId == item.id{
                    
                    let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToUpdateDeliveryVC(deliverySystem: item)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(updateAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToDeliveryDetailsVC(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DBaseViewController") as! DBaseViewController
        viewController.deliverySystem = id
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    
    func navigateToUpdateDeliveryVC(deliverySystem : DeliverySystems){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DeliverySystemUpdateViewController") as! DeliverySystemUpdateViewController
        viewController.deliverySystemData = deliverySystem
        //        viewController.customerState = CustomerState.Update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Api Delegate
extension DeliveryListSearchViewController: DeliveryListSearchViewDelegate{
    func setDeliveryListData(data: DeliveryDataMapper) {
        guard let list = data.deliverySystems, list.count > 0 else {
            return
        }
        self.deliverySystems += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.searchAlert(title: LanguageManager.NoInformationFound, message: data)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getDeliverylistSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
    }
}
