//
//  DeliveryDetailsBaseViewController.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SVProgressHUD

class DeliveryDetailsBaseViewController: UIViewController, BottomSheetDelegate { 
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var testBtn: UIButton!
    
    private var presenter = DeliveryDetailsPresenter(service: DeliveryService())
    
    enum Sections : Int {
        case DeliveryInfo
        case DeliverySummary
    }

    var deliverySystemDetails : DeliverySystemDetails?
    var deliverySystem : Int?
    var deliveryLog : String?
    
    var deliverySystemId : Int?
    var deliverySystemName : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        container.layer.cornerRadius = 15
        container.layer.masksToBounds = true
        self.configureTableView()
        self.setBarButtonSize()
        self.initialSetUp()
    }
    
    
    
    func initialSetUp(){
        //self.testBtn.addTarget(self, action: #selector(onBtnClick), for: .touchUpInside)
        self.navigationController?.navigationBar.topItem?.title = " "
    }
    
    @objc func onBtnClick(sender: UIButton){
        self.navigateToUpdateVC()
    }
    
    func setBarButtonSize(){
        
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "edit"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onEdit(sender:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        button.widthAnchor.constraint(equalToConstant: 32).isActive = true
        button.heightAnchor.constraint(equalToConstant: 32).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func onEdit(sender: UIBarButtonItem){
        self.navigateToUpdateVC()
    }
    
    func navigateToUpdateVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDeliveryDueViewController") as! AddNewDeliveryDueViewController
        viewController.details = deliverySystemDetails
        viewController.id = self.deliverySystemId
        viewController.type = 1
        viewController.deliveryState = DeliveryState.Update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? DeliveryDetailsViewController{
            viewController.bottomSheetDelegate = self
            viewController.deliverySystemId = self.deliverySystem
            viewController.deliverySystemName = self.deliverySystemName
//            viewController.delegate = self
            viewController.parentView = container
        }
    }
    func updateBottomSheet(frame: CGRect) {
        container.frame = frame
    }
   
}

//MARK: TableViewDelegate And DataSource
extension DeliveryDetailsBaseViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DeliveryDetailsInfoCell.nib, forCellReuseIdentifier: DeliveryDetailsInfoCell.identifier)
        self.tableView.register(DeliveryDetailsSummaryCell.nib, forCellReuseIdentifier: DeliveryDetailsSummaryCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.DeliveryInfo.rawValue:
            return 1
        case Sections.DeliverySummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case Sections.DeliveryInfo.rawValue:
            let cell: DeliveryDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier:DeliveryDetailsInfoCell.identifier, for: indexPath) as! DeliveryDetailsInfoCell
            guard let deliverySystem = self.deliverySystemDetails else {
                return cell
            }
            //        let item = deliverySystem[indexPath.row]
            cell.alphabetLabel.text = String(describing: Array(deliverySystem.name.capitalized)[0])
            cell.nameLabel.text = deliverySystem.name
            cell.phoneLabel.text = deliverySystem.phone
            //self.deliverySystemName = deliverySystem.name
            return cell
        case Sections.DeliverySummary.rawValue:
            let cell: DeliveryDetailsSummaryCell = tableView.dequeueReusableCell(withIdentifier:DeliveryDetailsSummaryCell.identifier, for: indexPath) as! DeliveryDetailsSummaryCell
            guard let deliverySystem = self.deliverySystemDetails else {
                return cell
            }
//            cell.currentDue.text = "\(deliverySystem.currentDue)"
//            cell.currentOrderLabel.text = "\(deliverySystem.currentOrder)"
//            cell.totalDuePaid.text = "\(deliverySystem.totalDuePaid)"
//            cell.totalOrderLabel.text = "\(deliverySystem.totalOrder)"
            return cell
        default:
            return UITableViewCell()
        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.DeliveryInfo.rawValue:
            return UITableView.automaticDimension
        case Sections.DeliverySummary.rawValue:
            return 140.0
        default:
            return 0
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
}
//
//extension DeliveryDetailsBaseViewController : DeliveryDetailsDataUpdate{
//    func updateDeliveryData(data: DeliveryDetailsDataMapper) {
//        self.deliverySystemDetails = data.deliverySystem
//        self.tableView.isHidden = false
//        self.tableView.reloadData()
//    }
//}
extension DeliveryDetailsBaseViewController : DeliveryDetailsViewDelegate{
    func cancelDeliveryDetailsInvoice(data: AddDataMapper) {
        //Empty
    }
    
    func setDeliveryDetailsData(data: DeliveryDetailsDataMapper) {
        self.deliverySystemDetails = data.deliverySystem
        self.tableView.isHidden = false
        self.refreshTableView()
    }
    
    func setDeliveryDetailsCurrentOrderData(data: DeliveryDetailsCurrentOrderDataMapper) {
        
    }
    
    func setDeliveryDetailsSaleHistroyData(data: DeliveryDetailsSaleHistoryDataMapper) {
        
    }
    
    func setDeliveryDetailsDueHistoryData(data: DeliveryDetailsDueHistroyDataMapper) {
        
    }
    
    func onFailed() {
        
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        SVProgressHUD.show()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        SVProgressHUD.dismiss()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.deliverySystem else{
            return
        }
        self.presenter.getDeliveryDetailsDataFromServer(id: id)
    }
    
}




