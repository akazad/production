//
//  DeliveryDetailsViewController.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SVProgressHUD

class DeliveryDetailsViewController: UIViewController{
    
    @IBOutlet weak var emptyMessage: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet var panView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
//    var delegate : DeliveryDetailsDataUpdate!
    var isFirst = true
    
    var selectedSegment : Int?
    
    private var presenter = DeliveryDetailsPresenter(service: DeliveryService())
//    var customerId : Int?
    var deliverySystemId : Int?
    var currentOrderId: Int?
    var saleHistoryId: Int?
    var invoice: String?
    var deliverySystemName : String?
    
    var currentOrder : [DeliveryDetailsCurrentOrder] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var saleHistory : [DeliveryDetailsSaleHistory] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var dueHistory : [DeliveryDetailsDueHistory] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isLoading : Bool = false //PaginationPropertise
    var currentPage: Int = 1  //PaginationPropertise
    
    
    var lastY: CGFloat = 0
    var pan: UIPanGestureRecognizer!
    
    var bottomSheetDelegate: BottomSheetDelegate?
    var parentView: UIView!
    
    var initalFrame: CGRect!
    var topY: CGFloat = 80 //change this in viewWillAppear for top position
    var middleY: CGFloat = 400 //change this in viewWillAppear to decide if animate to top or bottom
    var bottomY: CGFloat = 600 //no need to change this
    let bottomOffset: CGFloat = 80 //sheet height on bottom position
    var lastLevel: SheetLevel = .bottom //choose inital position of the sheet
    
    var disableTableScroll = false
    
    //hack panOffset To prevent jump when goes from top to down
    var panOffset: CGFloat = 0
    var applyPanOffset = false
    
    //tableview variables
    var listItems: [Any] = []
    var headerItems: [Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        pan.delegate = self
        self.panView.addGestureRecognizer(pan)
        
        self.tableView.panGestureRecognizer.addTarget(self, action: #selector(handlePan(_:)))
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap(_:)))
        tap.delegate = self
        tableView.addGestureRecognizer(tap)
        self.configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initalFrame = UIScreen.main.bounds
        self.topY = round(initalFrame.height * 0.05)
        self.middleY = initalFrame.height * 0.6
        self.bottomY = initalFrame.height - bottomOffset
        self.lastY = self.middleY
        
        bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.middleY))
    }
    
    
    @IBAction func segmentedControllAction(_ sender: UISegmentedControl) {
        
        guard let id = self.deliverySystemId else{
            return
        }
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            self.selectedSegment = segmentedControl.selectedSegmentIndex
            self.currentOrder = []
            self.isLoading = true
            self.currentPage = 1
            self.presenter.getDeliveryDetailsCurrentOrderDataFromServer(page: self.currentPage, id: id)
            self.refreshTableView()
        case 1:
            self.selectedSegment = segmentedControl.selectedSegmentIndex
            self.saleHistory = []
            self.isLoading = true
            self.currentPage = 1
            self.presenter.getDeliveryDetailsSaleHistoryDataFromServer(page: self.currentPage, id: id)
            self.refreshTableView()
        case 2:
            self.selectedSegment = segmentedControl.selectedSegmentIndex
            self.dueHistory = []
            self.isLoading = true
            self.currentPage = 1
            self.presenter.getDeliveryDetailsDueHistoryDataFromServer(page: self.currentPage, id: id)
            self.refreshTableView()
        default:
            break;
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == tableView else {return}
        
        if (self.parentView.frame.minY > topY){
            self.tableView.contentOffset.y = 0
        }
    }
    
    
    //this stops unintended tableview scrolling while animating to top
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard scrollView == tableView else {return}
        
        if disableTableScroll{
            targetContentOffset.pointee = scrollView.contentOffset
            disableTableScroll = false
        }
    }
    
    
    @objc func handleTap(_ recognizer: UITapGestureRecognizer){
        let p = recognizer.location(in: self.tableView)
        let index = tableView.indexPathForRow(at: p)
        if self.currentOrder.count > 0 {
            guard let saleItemIndex = index else {
                return
            }
            
            if selectedSegment == 0 {
                let saleItem = self.currentOrder[saleItemIndex.row]
                self.navigateToSaleDetailsVC(iD: saleItem.id)
            }
            else if selectedSegment == 1{
                let saleItem = self.saleHistory[saleItemIndex.row]
                self.navigateToSaleDetailsVC(iD: saleItem.id)
            }else{
                return
            }
        }

        tableView.selectRow(at: index, animated: false, scrollPosition: .none)
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let saleDetailsVC = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        saleDetailsVC.iD = iD
        self.navigationController?.pushViewController(saleDetailsVC, animated: true)
    }
    
    @objc func handlePan(_ recognizer: UIPanGestureRecognizer){
        
        let dy = recognizer.translation(in: self.parentView).y
        switch recognizer.state {
        case .began:
            applyPanOffset = (self.tableView.contentOffset.y > 0)
        case .changed:
            if self.tableView.contentOffset.y > 0{
                panOffset = dy
                return
            }
            
            if self.tableView.contentOffset.y <= 0{
                if !applyPanOffset{panOffset = 0}
                let maxY = max(topY, lastY + dy - panOffset)
                let y = min(bottomY, maxY)
                //                self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: y)
                bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: y))
            }
            
            if self.parentView.frame.minY > topY{
                self.tableView.contentOffset.y = 0
            }
        case .failed, .ended, .cancelled:
            panOffset = 0
            
            if (self.tableView.contentOffset.y > 0){
                return
            }
            
            self.panView.isUserInteractionEnabled = false
            
            self.disableTableScroll = self.lastLevel != .top
            
            self.lastY = self.parentView.frame.minY
            self.lastLevel = self.nextLevel(recognizer: recognizer)
            
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.9, options: .curveEaseOut, animations: {
                
                switch self.lastLevel{
                case .top:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.topY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.topY))
                    self.tableView.contentInset.bottom = 50
                case .middle:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.middleY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.middleY))
                case .bottom:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.bottomY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.bottomY))
                }
                
            }) { (_) in
                self.panView.isUserInteractionEnabled = true
                self.lastY = self.parentView.frame.minY
            }
        default:
            break
        }
    }
    
    func nextLevel(recognizer: UIPanGestureRecognizer) -> SheetLevel{
        let y = self.lastY
        let velY = recognizer.velocity(in: self.view).y
        if velY < -200{
            return y > middleY ? .middle : .top
        }else if velY > 200{
            return y < (middleY + 1) ? .middle : .bottom
        }else{
            if y > middleY {
                return (y - middleY) < (bottomY - y) ? .middle : .bottom
            }else{
                return (y - topY) < (middleY - y) ? .top : .middle
            }
        }
    }
   
}

//Mark: TableView Delegate And Data Source
extension DeliveryDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = .clear
        self.tableView.register(DeliveryCurrentOrderCell.nib, forCellReuseIdentifier: DeliveryCurrentOrderCell.identifier)
        self.tableView.register(DeliveryInvoiceCell.nib, forCellReuseIdentifier: DeliveryInvoiceCell.identifier)
        self.tableView.register(DeliveryDueHistoryCell.nib, forCellReuseIdentifier: DeliveryDueHistoryCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.emptyMessage.isHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedSegment == 0{
            if self.currentOrder.count > 0 {
                self.emptyMessage.isHidden = true
                return self.currentOrder.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        else if selectedSegment == 1{
            if self.saleHistory.count > 0 {
                self.emptyMessage.isHidden = true
                return self.saleHistory.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        else if selectedSegment == 2{
            if self.dueHistory.count > 0 {
                self.emptyMessage.isHidden = true
                return self.dueHistory.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedSegment == 0{
            let cell : DeliveryCurrentOrderCell = tableView.dequeueReusableCell(withIdentifier: DeliveryCurrentOrderCell.identifier, for: indexPath) as! DeliveryCurrentOrderCell
            cell.selectionStyle = .none
            if  self.currentOrder.count > 0 {
                let item = self.currentOrder[indexPath.row]
                cell.createdAtLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.invoiceLabel.text = item.invoice
                cell.sellerLabel.text = "বিক্রেতা: " + item.addedBy
                cell.quantityLabel.text = "পরিমাণঃ " + "\(item.amount)" + Constants.currencySymbol
                
                //Pagination
                if isLoading == false && indexPath.row == self.currentOrder.count - 1 {
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.deliverySystemId else{
                        return cell
                    }
                    self.presenter.getDeliveryDetailsCurrentOrderDataFromServer(page: self.currentPage, id: id)
                }

                return cell
            }
            
        }
        else if selectedSegment == 1{
            let cell : DeliveryInvoiceCell = tableView.dequeueReusableCell(withIdentifier: DeliveryInvoiceCell.identifier, for: indexPath) as! DeliveryInvoiceCell
            cell.selectionStyle = .none
            if self.saleHistory.count > 0 {
                let item = self.saleHistory[indexPath.row]
                cell.createdAtLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.deliveryChargeLabel.text = "ডেলিভারি চার্জঃ " +  "\(item.deliveryCharge)" + " " + Constants.currencySymbol
                cell.invoiceLabel.text = item.invoice
                cell.paidLabel.text = "সর্বমোট পরিশোধঃ " + "\(item.paid)" + " " + Constants.currencySymbol
                cell.dueLabel.text = "বাকিঃ " + "\(item.due)" + " " + Constants.currencySymbol
                
                //Pagination
                if isLoading == false && indexPath.row == self.saleHistory.count - 1 {
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.deliverySystemId else{
                        return cell
                    }
                    self.presenter.getDeliveryDetailsSaleHistoryDataFromServer(page: self.currentPage, id: id)
                }
            }
            return cell
        }
        else if selectedSegment == 2{
            let cell : DeliveryDueHistoryCell = tableView.dequeueReusableCell(withIdentifier: DeliveryDueHistoryCell.identifier, for: indexPath) as! DeliveryDueHistoryCell
            cell.selectionStyle = .none
            if self.dueHistory.count > 0 {
                let item = self.dueHistory[indexPath.row]
                cell.createdAtLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.quantityLabel.text = "পরিমাণঃ " + "\(item.amount)" + " " + Constants.currencySymbol
                cell.sellerLabel.text = "বিক্রেতা: " + item.seller
                if item.type == 0 {
                    cell.typeLabel.text = "দেনা"
                    cell.typeLabel.textColor = UIColor(red: CGFloat(220/255.0), green: CGFloat(53/255.0), blue: CGFloat(69/255.0), alpha: CGFloat(1.0))
                    cell.typeView.backgroundColor = UIColor(red: CGFloat(255/255.0), green: CGFloat(225/255.0), blue: CGFloat(228/255.0), alpha: CGFloat(1.0))
                }
                if item.type == 1 {
                    cell.typeLabel.text = "পূরণ"
                    cell.typeLabel.textColor = UIColor(red: CGFloat(91/255.0), green: CGFloat(160/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1.0))
                    cell.typeView.backgroundColor = UIColor(red: CGFloat(241/255.0), green: CGFloat(255/255.0), blue: CGFloat(222/255.0), alpha: CGFloat(1.0))
                }
                
                //Pagination
                if isLoading == false && indexPath.row == self.dueHistory.count - 1 {
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.deliverySystemId else{
                        return cell
                    }
                    self.presenter.getDeliveryDetailsDueHistoryDataFromServer(page: self.currentPage, id: id)
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedSegment == 0 {
            return 85.0
        }
        else if selectedSegment == 1 {
            return 88.0
        }
        else if selectedSegment == 2 {
            return 75.0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if segmentedControl.selectedSegmentIndex == 0 {
            let duePaid = UITableViewRowAction(style: .normal, title: "বাকি পূরণ") { action, index in
                if self.currentOrder.count > 0 {
                    let currentOder = self.currentOrder[indexPath.row]
                    self.currentOrderId = currentOder.id
                    self.navigateToCurrentOrderVC(currentOrder: currentOder)
                }
            }
            
            duePaid.backgroundColor = UIColor.orange
            
            return [duePaid]
            
        }else if segmentedControl.selectedSegmentIndex == 1{
            let delete = UITableViewRowAction(style: .normal, title: "বাতিল") { action, index in
                
                if self.saleHistory.count > 0{
                    let saleHistory = self.saleHistory[indexPath.row]
                    self.saleHistoryId = saleHistory.id
                    
                    guard let id = self.saleHistoryId else{
                        return
                    }
                    self.confirmationMessage(userMessage: "আপনি কি নিশ্চিত?", deleteId: id)
                }
                
            }
            delete.backgroundColor = UIColor.red
            
            return [delete]
        }
        
        return []
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if segmentedControl.selectedSegmentIndex == 2{
            if self.saleHistory.count > 0 {
                let saleHistory = self.saleHistory[indexPath.row]
                self.saleHistoryId = saleHistory.id
                guard let id = self.saleHistoryId else{
                    return
                }
                self.navigateToSaleHistoryDetailsVC(iD : id)
            }
        }
    }
    
    func navigateToSaleHistoryDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        viewController.iD = iD
        //viewController.state = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToCurrentOrderVC(currentOrder: DeliveryDetailsCurrentOrder){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DeliveryDuePaidViewController") as! DeliveryDuePaidViewController
        viewController.deliveryCurrentOrder = currentOrder
        //viewController.currentOrderId = currentOrder.id
        viewController.invoice = currentOrder.invoice
        viewController.deliverySystemName = self.deliverySystemName
        viewController.deliverySystemId = self.deliverySystemId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: "Warning", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Yes Delete!", style: .default){
                (action:UIAlertAction!) in
                
                //self.showAlert(title: "কাজটি অসম্পূর্ণ", message: "")
                //self.presenter.postProductRecentStockDeleteDataFromServer(id: deleteId)
                
            }
            let cancelAction = UIAlertAction(title: "No", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

extension DeliveryDetailsViewController: UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

//Mark: Api Delegate
extension DeliveryDetailsViewController : DeliveryDetailsViewDelegate{
    func cancelDeliveryDetailsInvoice(data: AddDataMapper) {
        //Empty
    }
    
    
    func setDeliveryDetailsCurrentOrderData(data: DeliveryDetailsCurrentOrderDataMapper) {
        guard let currentOrderItem = data.orders, currentOrderItem.count > 0 else{
            return
        }
        self.selectedSegment = 0
        self.isLoading = false
        self.currentOrder += currentOrderItem
    }
    
    func setDeliveryDetailsSaleHistroyData(data: DeliveryDetailsSaleHistoryDataMapper) {
        guard let saleHistoryItem = data.sales, saleHistoryItem.count > 0 else {
            return
        }
        self.isLoading = false
        self.saleHistory += saleHistoryItem
    }
    
    func setDeliveryDetailsDueHistoryData(data: DeliveryDetailsDueHistroyDataMapper) {
        guard let dueHistoryItem = data.dues, dueHistoryItem.count > 0 else {
            return
        }
        self.isLoading = false
        self.dueHistory += dueHistoryItem
    }
    
    func setDeliveryDetailsData(data: DeliveryDetailsDataMapper) {
        //        self.delegate.updateDeliveryData(data: data)
        
    }
    
    func onFailed() {
        //self.isLoading = true
    }
    
    func showLoading() {
//        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
//        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        guard let id = self.deliverySystemId else{
            return
        }
        self.presenter.getDeliveryDetailsCurrentOrderDataFromServer(page: self.currentPage, id: id)
    }
}
//
//protocol DeliveryDetailsDataUpdate : NSObjectProtocol {
//    func updateDeliveryData(data : DeliveryDetailsDataMapper)
//}




