//
//  AddNewDeliveryDueViewController.swift
//  Ponno
//
//  Created by a k azad on 9/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

enum DeliveryState : Int{
    case Add
    case Update
}

class AddNewDeliveryDueViewController: UIViewController {
    
    @IBOutlet weak var deliverySystemLabel: UILabel!
    @IBOutlet weak var deliverySystemListTextField: UITextField!
    @IBOutlet weak var deliverySystemCurrentDue: UILabel!
    @IBOutlet weak var deliverySystemCurrentDueTextField: UITextField!
    @IBOutlet weak var deliverySystemDueAmountLabel: UILabel!
    @IBOutlet weak var deliverySystemDueAmountTextField: UITextField!
    @IBOutlet weak var deliverySystemDetailsLabel: UILabel!
    @IBOutlet weak var deliverySystemDetailsView: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    private var presenter = AddNewDeliverySystemDuePresenter(service: DeliveryService())
    
    var deliverySystemId : Int?
    var deliverySystem : [DeliverySystems] = [] {
        didSet{
            self.deliveryPicker.reloadAllComponents()
        }
    }
    
    var deliveryPicker = UIPickerView()
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    var getAll: Bool = true
    
    var deliveryState = DeliveryState.Add.rawValue
    var details : DeliverySystemDetails?
    var id : Int?
    var type : Int?
    var duePayable : Double?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetUp ()
        self.setUpToolBar()
        self.attachPresenter()
    }
    
    func initialSetUp (){

        self.navigationController?.navigationBar.topItem?.title = " "
        
        self.deliverySystemListTextField.underlined()
        self.deliverySystemCurrentDueTextField.underlined()
        self.deliverySystemDueAmountTextField.underlined()
        
        if self.deliveryState == DeliveryState.Add.rawValue{
            self.setUpPickerView()
            self.title = LanguageManager.NewDue
            //self.titleLbl.text = "Due Related Info"
            self.deliverySystemDueAmountLabel.text = LanguageManager.DueAmount
        }else{
            self.title = LanguageManager.DueWithdraw
            //self.titleLbl.text = ""
            self.deliverySystemDueAmountLabel.text = LanguageManager.DueWithdrawAmount
            guard let data = self.details else{
                return
            }
            self.id = data.id
            self.deliverySystemListTextField.text = data.name
            self.deliverySystemCurrentDueTextField.text = "\(data.otherDue)"
            self.duePayable = Double(data.otherDue)
//            self.deliverySystemCurrentDueTextField.isEnabled = false
        }
        self.deliverySystemLabel.text = LanguageManager.DeliverySystem
        self.deliverySystemCurrentDue.text = LanguageManager.CurrentDue
        self.deliverySystemCurrentDueTextField.isEnabled = false
        
        self.deliverySystemDetailsLabel.text = LanguageManager.Description
    }
    
    @IBAction func submitButton(_ sender: UIButton) {
        if isValidated(){
            if deliveryState == DeliveryState.Add.rawValue{
                guard let id = self.deliverySystemId, let amount = self.deliverySystemDueAmountTextField.text, let description = self.deliverySystemDetailsView.text, let type = self.type else {
                    return
                }
                let dueData : [String: Any] = ["delivery_system_id" :  id, "type" : type, "amount" : amount, "description" : description]
                self.presenter.postDeliveryDueDataToServer(deliveryDueData: dueData)
            }else{
                guard let id = self.id, let amount = self.deliverySystemDueAmountTextField.text, let description = self.deliverySystemDetailsView.text, let type = self.type else{
                    return
                }
                let paidData : [String: Any] = ["delivery_system_id" :  id, "type" : type, "amount" : amount, "description" : description]
                self.presenter.postDeliveryDueDataToServer(deliveryDueData: paidData)
            }
            
        }
    }
    
    func setUpPickerView(){
        self.deliverySystemListTextField.underlined()
        self.deliverySystemDueAmountTextField.underlined()
        self.deliverySystemCurrentDueTextField.underlined()
        
        deliveryPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.deliverySystemListTextField.inputView = deliveryPicker
        self.deliverySystemListTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.deliverySystemListTextField.resignFirstResponder()
        self.deliverySystemDueAmountTextField.becomeFirstResponder()
    }
    
    func setUpToolBar(){
        
        //DeliveryDueAmountToolBaar
        let deliveryDueToolBar = UIToolbar()
        deliveryDueToolBar.barStyle = UIBarStyle.default
        deliveryDueToolBar.isTranslucent = true
        deliveryDueToolBar.tintColor = UIColor.black
        deliveryDueToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let deliveryDueDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnDeliveryDue(sender:)))
        
        deliveryDueToolBar.setItems([cancelButton, spaceButton, deliveryDueDoneButton], animated: false)
        deliveryDueToolBar.isUserInteractionEnabled = true
        
        self.deliverySystemDueAmountTextField.inputAccessoryView = deliveryDueToolBar
        
        //DeliveryDueDetailsToolBar
        let deliveryDetailsToolBar = UIToolbar()
        deliveryDetailsToolBar.barStyle = UIBarStyle.default
        deliveryDetailsToolBar.isTranslucent = true
        deliveryDetailsToolBar.tintColor = UIColor.black
        deliveryDetailsToolBar.sizeToFit()
        
        let deliveryDetailsDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnDeliveryDetails(sender:)))
        
        deliveryDetailsToolBar.setItems([cancelButton, spaceButton, deliveryDetailsDoneButton], animated: false)
        deliveryDetailsToolBar.isUserInteractionEnabled = true
        
        self.deliverySystemDetailsView.inputAccessoryView = deliveryDetailsToolBar
    }
    
    @objc func onPressingDoneOnDeliveryDue(sender: UIBarButtonItem){
        self.deliverySystemDueAmountTextField.resignFirstResponder()
        self.deliverySystemDetailsView.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnDeliveryDetails(sender: UIBarButtonItem){
        self.deliverySystemDetailsView.resignFirstResponder()
        self.submitBtn.becomeFirstResponder()
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
        if (self.deliverySystemListTextField.text?.isEmpty)!{
            //self.displayMessage(userMessage: "Please enter a delivery system")
            self.showAlert(title: LanguageManager.DeliverySystemIsRequired, message: "")
            return false
        }else if (self.deliverySystemDueAmountTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.AmountIsRequired)
            return false
        }else if let dueAmountText = self.deliverySystemDueAmountTextField.text, let dueAmount = Double(dueAmountText), let duePayable = self.duePayable {
            if dueAmount > duePayable{
                self.deliverySystemDueAmountTextField.text = "\(duePayable)"
                self.showAlert(title: LanguageManager.WithdrawAmountIsTooMuch, message: "")
                return false
            }
        }
        return true
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: userMessage, message: "", preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.deliverySystemListTextField.resignFirstResponder()
                self.deliverySystemDueAmountTextField.becomeFirstResponder()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: TextFiledDelegate
//extension AddNewDeliveryDueViewController : UITextFieldDelegate{
//    func configureTextField(){
//        self.delv
//    }
//}

//Mark: PickerViewDelegate
extension AddNewDeliveryDueViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.deliverySystem.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        if isLoading == false && row == self.deliverySystem.count - 1 && self.currentPage < self.lastPageNo {
//            self.isLoading = true
//            self.currentPage += 1
//            self.presenter.getDeliverySystemListFromServer(page: self.currentPage)
//        }
        self.deliverySystemListTextField.text = self.deliverySystem[row].name
        self.deliverySystemCurrentDueTextField.text = "\(self.deliverySystem[row].currentDue)"
        self.deliverySystemId = self.deliverySystem[row].id
        return self.deliverySystem[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.deliverySystemListTextField.text = self.deliverySystem[row].name
        
//        let selectedRow = [self.deliveryPicker.selectedRow(inComponent: 0)]
//
//        if selectedRow == [0]{
//            self.alertWithTextField()
//            self.deliverySystemCurrentDueTextField.text = "0.0"
//        }else{
            self.deliverySystemCurrentDueTextField.text = "\(self.deliverySystem[row].currentDue)"
            self.deliverySystemId = self.deliverySystem[row].id
//        }
        
    }
    
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewDeliverySystemAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.DeliverySystem
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.PhoneNumber
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.NameIsRequired, message: "")
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                self.showAlert(title: LanguageManager.PhoneNumberIsRequired, message: "")
                return
            }
            
            let param : [String : String] = ["name": textField.text!, "phone": textField2.text!, "description": "", "current_due": ""]
            
            self.presenter.postNewDeliverySystemAddDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}

// Mark: API Delegate
extension AddNewDeliveryDueViewController: AddNewDeliveryDueViewDelegate{
    
    func setDeliverySystemList(data: DeliveryDataMapper) {
        guard let deliverySystemsList = data.deliverySystems, deliverySystemsList.count > 0 else {
            return
        }
        self.isLoading = false
//        let newDeliverySystem = DeliverySystems(id: 0, name: "", phone: "")
//        deliverySystemsList.insert(newDeliverySystem, at: 0)
        self.deliverySystem = deliverySystemsList
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setAddNewDeliverySystem(data: DeliverySystemAddDataMapper) {
        
        guard let deliverySystem = data.deliverySystem else {
            return
        }
        self.deliverySystemListTextField.text = deliverySystem.name
        self.deliverySystemId = deliverySystem.id
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.presenter.getAllDeliverySystemListFromServer(getAll: self.getAll)
    }
}

// Mark: TextField Delegate
extension AddNewDeliveryDueViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.deliverySystemListTextField{
            self.deliverySystemCurrentDueTextField.becomeFirstResponder()
        }
        return false
    }
}

