//
//  AddNewDeliverySystemViewController.swift
//  Ponno
//
//  Created by a k azad on 10/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class AddNewDeliverySystemViewController: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel!{
        didSet{
            titleLbl.text = LanguageManager.NewDeliverySystemInfo
        }
    }
    @IBOutlet weak var deliveryNameLbl: UILabel!{
        didSet{
            deliveryNameLbl.text = LanguageManager.DeliverySystem
        }
    }
    @IBOutlet weak var deliverySystemTextField: UITextField!{
        didSet{
            deliverySystemTextField.underlined()
            deliverySystemTextField.placeholder = LanguageManager.DeliverySystem
        }
    }
    @IBOutlet weak var deliverySystemPhoneLbl: UILabel!{
        didSet{
            deliverySystemPhoneLbl.text = LanguageManager.Phone
        }
    }
    @IBOutlet weak var deliverySystemPhoneTextField: UITextField!{
        didSet{
            deliverySystemPhoneTextField.underlined()
            deliverySystemPhoneTextField.placeholder = LanguageManager.PhoneNumber
        }
    }
    @IBOutlet weak var deliverySystemDescriptionLbl: UILabel!{
        didSet{
            deliverySystemDescriptionLbl.text = LanguageManager.Address
        }
    }
    @IBOutlet weak var deliverySystemDescriptionTextField: UITextField!{
        didSet{
            deliverySystemDescriptionTextField.underlined()
            deliverySystemDescriptionTextField.placeholder = LanguageManager.Description
        }
    }
//    @IBOutlet weak var deliverySystemCurrentDueLbl: UILabel!{
//        didSet{
//            deliverySystemCurrentDueLbl.text = LanguageManager.CurrentDue
//        }
//    }
//    @IBOutlet weak var deliverySystemCurrentDueTextField: UITextField!{
//        didSet{
//            deliverySystemCurrentDueTextField.underlined()
//            deliverySystemCurrentDueTextField.placeholder = LanguageManager.CurrentDue
//        }
//    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = AddNewDeliverySystemPresenter(service: DeliveryService())

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.attachPresenter()
        self.initialSetUp()
    }
    
    func initialSetUp(){
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let deliverySystem = self.deliverySystemTextField.text, let phone = self.deliverySystemPhoneTextField.text, let description = deliverySystemDescriptionTextField.text else{
                return
            }
            
            let param : [String : Any] = ["name": deliverySystem, "phone": phone, "description": description]
            
            self.presenter.postAddNewDeliverySystemDataToServer(param: param)
        }
    }
    
    func submitBtnControllWith(isEnabled: Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
        guard let deliverySystem = deliverySystemTextField.text, deliverySystem != "" else {
            self.showAlert(title: LanguageManager.DeliverySystemIsRequired, message: "")
            return false
        }
        guard let phone = deliverySystemPhoneTextField.text, phone != "" else {
            self.showAlert(title: LanguageManager.PhoneNumberIsRequired, message: "")
            return false
        }
//        if (!validatePhoneNumber(value: phone)){
//            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
//            return false
//        }
        return true
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
  
}

//Mark: Api Delegate
extension AddNewDeliverySystemViewController : AddNewDeliverySystemViewDelegate{
    func addDeliverySystem(data: DeliverySystemAddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.submitBtnControllWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControllWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

extension AddNewDeliverySystemViewController{
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}
