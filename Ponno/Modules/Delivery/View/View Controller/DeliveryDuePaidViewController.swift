//
//  DeliveryDuePaidViewController.swift
//  Ponno
//
//  Created by a k azad on 15/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DeliveryDuePaidViewController: UIViewController {
    
    @IBOutlet weak var deliverySystemNameLbl: UILabel!{
        didSet{
            deliverySystemNameLbl.text = LanguageManager.DeliverySystem
        }
    }
    @IBOutlet weak var deliverySystemNameText: UITextField!{
        didSet{
            deliverySystemNameText.text = self.deliverySystemName
            deliverySystemNameText.isEnabled = false
        }
    }
    @IBOutlet weak var commissionLbl: UILabel!{
        didSet{
            commissionLbl.text = LanguageManager.Commission + " : "
        }
    }
    @IBOutlet weak var commissionText: UITextField!{
        didSet{
            commissionText.placeholder = LanguageManager.Commission
        }
    }
    @IBOutlet weak var commissionUnitText: UITextField!{
        didSet{
            commissionUnitText.text = "%"
        }
    }
    @IBOutlet weak var totalLbl: UILabel!{
        didSet{
            totalLbl.text = LanguageManager.Total
        }
    }
    @IBOutlet weak var totalText: UITextField!{
        didSet{
            totalText.isEnabled = false
        }
    }
    @IBOutlet weak var duePaidAmountLbl: UILabel!{
        didSet{
            duePaidAmountLbl.text = LanguageManager.DueWithdrawAmount
        }
    }
    @IBOutlet weak var duePaidAmountText: UITextField!{
        didSet{
            duePaidAmountText.placeholder = LanguageManager.DueWithdrawAmount
        }
    }
    @IBOutlet weak var newDueLbl: UILabel!{
        didSet{
            newDueLbl.text = LanguageManager.NewDue
        }
    }
    @IBOutlet weak var newDueText: UITextField!{
        didSet{
            newDueText.isEnabled = false
            newDueText.placeholder = LanguageManager.NewDue
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = DeliveryDuePaidPresenter(service: DeliveryService())
    
    var unitItem: [String] = ["%","৳"]
    var totalDue: Double?
    var duePaid : Double?
    var commission :Double?
    var commissionUnit : String?
    var deliverySystemName :String?
    
    var deliverySystemId: Int?
    //var deliverySystemId: Int?
    var currentOrderId: String?
    var invoice: String?
    
    var deliveryCurrentOrder: DeliveryDetailsCurrentOrder?
    var unitPicker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpPickerView()
        self.setUpInitialLanguage()
    }
    
    func initialSetup(){
        self.title = LanguageManager.DueWithdraw

        guard let currentOrder = self.deliveryCurrentOrder else {
            return
        }
        if currentOrder.amount == "0.00"{
            self.commissionText.isEnabled = false
        }
        self.commissionText.text = "0"
        self.totalText.text = self.totalDue?.toString()
        //self.totalDue = Double(currentOrder.amount)
        self.duePaidAmountText.text = "0"
        self.newDueText.text = self.totalDue?.toString()
        
        self.duePaidAmountText.addTarget(self, action: #selector(onEditDuePaidTextField), for: .editingChanged)
        self.commissionText.addTarget(self, action:  #selector(onEditCommissionTextField), for: .editingChanged)
        self.commissionUnitText.addTarget(self, action: #selector(onCommissionUnitChange), for: .editingChanged)
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let id = self.currentOrderId, let systemId = self.deliverySystemId, let commissionText = self.commissionText.text, let commission = Double(commissionText), let commissionUnit = self.commissionUnitText.text, let newDueText = self.newDueText.text, let newDue = Double(newDueText), let amount = self.duePaidAmountText.text  else{
                return
            }
            let param : [String : Any] = ["delivery_system_id": systemId, "invoice": id, "commission": commission, "commission_unit": commissionUnit, "new_due": newDue, "amount": amount ]
            
            self.presenter.postDeliveryInvoiceRecieveToServer(param: param)
            
        }
    }
    
    func submitBtnControllWith(isEnabled: Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func isValidated() -> Bool{
        if newDueText.text == ""{
            self.showAlert(title: LanguageManager.NewDueIsRequired, message: "")
            return false
        }
        if let commissionText = commissionText.text, let commission = Double(commissionText), commission < 0 {
            if let unit = self.commissionUnitText.text, unit == "" {
                self.showAlert(title: LanguageManager.CommissionUnitIsRequired, message: "")
                return false
            }
        }
        return true
    }
    
    @objc func onEditDuePaidTextField(sender: UITextField){
        self.setMaxDuePaid()
    }
    
    func setMaxDuePaid(){
        guard let duePaidText = self.duePaidAmountText.text, let duePaidAmount = Double(duePaidText) else{
            self.duePaid = 0.0
            self.refreshScreen()
            return
        }
        
        if let totalDue = self.totalText.text?.toDouble(){
            if duePaidAmount > totalDue{
                self.duePaidAmountText.text = totalDue.toString()
                self.duePaid = totalDue
                self.showAlert(title: LanguageManager.YouCanNotPayMoreThan + "\(totalDue)", message: "")
                self.refreshScreen()
                return
            }else{
                self.newDueText.text = "\(totalDue - duePaidAmount)"
            }
        }
        
    }
    
    
    @objc func onEditCommissionTextField(sender: UITextField){
        self.setMaxCommission()
    }
    
    func setMaxCommission(){
        guard let commissionText = self.commissionText.text, let commission = Double(commissionText) else{
            self.commission = 0
            self.refreshScreen()
            return
        }
        let commissionUnitText = self.commissionUnitText.text ?? "%"
        if commissionUnitText == "৳"{
            if let totalDue = self.totalText.text?.toDouble(){
                if commission > totalDue{
                    self.commission = 0
                    self.commissionText.text = "\(0)"
                    self.totalText.text = self.totalDue?.toString()
                    self.showAlert(title: LanguageManager.YouCanNotPayMoreThan + " : ", message: (self.totalDue?.toString() ?? "0") + Constants.currencySymbol)
                    
                    self.refreshScreen()
                    return
                }else{
                    self.commission = commission
                    self.refreshScreen()
                }
            }
        }else{
            self.commission = commission
            self.refreshScreen()
        }
        self.duePaidAmountText.text = "0"
    }
    
    @objc func onCommissionUnitChange(sender: UITextField){
        guard let unitText = self.commissionUnitText.text else {
            return
        }
        self.commissionUnit = unitText
        self.refreshScreen()
    }
    
    func refreshScreen(){
        guard var total = self.totalDue else {
            return
        }
        
        var discounted : Double = 0.0
        let commission = self.commission ?? 0
        let commissionUnitText = self.commissionUnitText.text ?? "%"
        if commissionUnitText == "%"{
            if commission >= 100.0{
                discounted = total
                total = total - discounted
                self.commissionText.text = "100"
            }else{
                discounted = (total * (commission/100))
                total = total - discounted
            }
        }else{
            discounted = commission
            if discounted > total {
                total = 0.00
            }else{
                total = total - discounted
            }
        }
        
        self.totalText.text = "\(total)"
        self.newDueText.text = "\(total)"
        
        let paidAmount = self.duePaid ?? 0
        
        if paidAmount > total {
           self.newDueText.text = "0.0"
        }else{
            self.newDueText.text = "\(total - paidAmount)"
        }
    }
    
    func setUpPickerView(){
        
        unitPicker.delegate = self
        //unitPicker.selectedRow(inComponent: 0)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.commissionUnitText.inputView = unitPicker
        self.commissionUnitText.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.refreshScreen()
        self.commissionText.resignFirstResponder()
        self.duePaidAmountText.becomeFirstResponder()
        
    }
    
}

//Mark: PickerViewDelegate
extension DeliveryDuePaidViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.unitItem.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.commissionUnitText.text = self.unitItem[row]
        return self.unitItem[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //if let selectedRow =
        self.commissionUnitText.text = self.unitItem[row]
    }
    
}

extension DeliveryDuePaidViewController : DeliveryDuePaidViewDelegate{
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.submitBtnControllWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
         self.submitBtnControllWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

extension DeliveryDuePaidViewController{
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
        
    }
}
