//
//  DeliveryViewController.swift
//  Ponno
//
//  Created by a k azad on 31/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class DeliveryViewController: BaseViewController {
    
    @IBOutlet weak var segmentControl: UISegmentedControl!{
        didSet{
            segmentControl.setTitle(LanguageManager.DeliverySystem, forSegmentAt: 0)
            segmentControl.setTitle(LanguageManager.DeliveryHistory, forSegmentAt: 1)
        }
    }
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var deliveryToggle: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = DeliveryPresenter(service: DeliveryService())
    
    private var deliverySystems : [DeliverySystems] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    private var isDeliveryLog = true{
        didSet{
            self.refreshTableView()
        }
    }
    
    private var deliveryLog : [DeliveryDueLogList] = [] {
        didSet{
            self.refreshTableView()
        }
    }

    var segmentCheck = false
    
    private var deliverySummary : [DeliverySummary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
//    SearchBar Propertise
    var isSearchActive = false{
        didSet{
            self.tableView.reloadData()
        }
    }
    var filteredList : [DeliverySystems] = []
    var filteredLogList : [DeliveryDueLogList] = []
    
//    Pagination Propertise
    var isLoading : Bool = false
    var currentLogPage : Int = 1
    var currentListPage : Int = 1
    var lastLogPageNo: Int = 0
    var lastListPageNo: Int = 0
    var deliverySystemName : String?
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.setNavigationBarTitle()
        self.configureTableView()
        self.configureCollectionView()
        self.setBarButton()
        self.initialSetup()
        //self.setUpSearchBar()
        self.addFloaty()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.DeliveryBook
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0))]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarButton(){
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let deliveryAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        deliveryAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        deliveryAddBtn.addTarget(self, action: #selector(onDeliverySystemAdd(sender:)), for: UIControl.Event.touchUpInside)
        deliveryAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        deliveryAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        deliveryAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: deliveryAddBtn)
        
//        let deliveryDueAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
//        deliveryDueAddBtn.setImage(UIImage(named: "deliveryTruck"), for: UIControl.State.normal)
//        deliveryDueAddBtn.addTarget(self, action: #selector(onDeliveryDueAdd(sender:)), for: UIControl.Event.touchUpInside)
//        deliveryDueAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
//        deliveryDueAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
//        deliveryDueAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
//        let barButton3 = UIBarButtonItem(customView: deliveryDueAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2,barButton1], animated: true)
    }
    
    @objc func onSearch(sender: UIButton){
        if segmentCheck == false {
            self.navigateToDeliveryListSearchVC()
        }else{
            self.navigateToDeliveryLogSearchVC()
        }
    }
    
    @objc func onDeliverySystemAdd(sender: UIButton){
        self.navigateToAddNewDeliverySystemViewController()
    }
    
    @objc func onDeliveryDueAdd(sender: UIButton){
        self.navigateToAddNewDeliveryDueVC()
    }
    
    func navigateToDeliveryListSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DeliveryListSearchViewController") as! DeliveryListSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToDeliveryLogSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DeliveryLogSearchViewController") as! DeliveryLogSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }

//    func setUpBarBtn(){
//        let image = UIImage(named: "next")
//        deliveryToggle.setBackgroundImage(image, for: .normal, barMetrics: .default)
//    }
//
    func initialSetup(){
        self.segmentControl.addTarget(self, action: #selector(onSegmentChange), for: .valueChanged)
    }
    
    @objc func onSegmentChange(sender: UISegmentedControl){
        isDeliveryLog = !isDeliveryLog
        if(!segmentCheck){
            segmentCheck = true
            self.isLoading = true
            self.deliveryLog = []
            self.currentLogPage = 1
            self.presenter.getDeliveryLogFromServer(page: self.currentLogPage)
        }
        else{
            segmentCheck = false
            self.deliverySystems = []
            self.isLoading = true
            self.currentListPage = 1
            self.presenter.getDeliveryDataFromServer(page: self.currentListPage)
        }
    }
    
    func navigateToAddNewDeliveryDueVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDeliveryDueViewController") as! AddNewDeliveryDueViewController
         let type : Int = 0
        viewController.type = type
        viewController.deliveryState = DeliveryState.Add.rawValue
//        viewController.deliverySystem = self.deliverySystems
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewDeliverySystemViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDeliverySystemViewController") as! AddNewDeliverySystemViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
//        let extraItem = FloatyItem()
//        extraItem.icon = UIImage(named: "next")
//        extraItem.buttonColor = UIColor.lightRed
//        extraItem.title = "নতুন বাকি"
//        extraItem.handler = { item in
//            self.navigateToAddNewDeliveryDueVC()
//        }
        
//        let anotherItem = FloatyItem()
//        anotherItem.icon = UIImage(named: "next")
//        anotherItem.buttonColor = UIColor.lightGreen
//        anotherItem.title = "নতুন ডেলিভারি পদ্ধতি"
//        anotherItem.handler = { item in
//            self.navigateToAddNewDeliverySystemViewController()
//        }
        
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(DeliveryViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        if(!segmentCheck){
            self.deliveryLog = []
            currentLogPage = 1
            self.presenter.getDeliveryLogFromServer(page: currentLogPage)
        }else{
            currentListPage = 1
            self.deliverySystems = []
            self.presenter.getDeliveryDataFromServer(page: currentListPage)
        }
        refreshControl.endRefreshing()
    }
    
}

//Mark: Floaty Delegate
extension DeliveryViewController: FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}


//MARK: CollectionView Delegate And DataSource
extension DeliveryViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(DeliverySummaryCell.nib, forCellWithReuseIdentifier: DeliverySummaryCell.identifier)
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let summaryItem = self.deliverySummary, summaryItem.count > 0 else{
            return 0
        }
        return summaryItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DeliverySummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: DeliverySummaryCell.identifier, for: indexPath) as! DeliverySummaryCell
        guard let list = self.deliverySummary, list.count > 0 else {
            return cell
        }
        let summaryItem = list[indexPath.row]
        
//        for (key, _) in list.enumerated() {
//            if key == 0 {
//                list[0].title = "Total Delivery System"
//            }else if key == 1 {
//                list[1].title = "Current Due"
//            }else if key == 2 {
//                list[2].title = "Total Due Withdraw"
//            }
//        }
        
        cell.titleLabel.text = summaryItem.title
        cell.valueLabel.text = summaryItem.value
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 60.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.deliverySummary, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//MARK: TableView Delegate And DataSource
extension DeliveryViewController : UITableViewDelegate, UITableViewDataSource {
    func configureTableView(){
      self.tableView.delegate = self
      self.tableView.dataSource = self
      self.tableView.register(DeliverySystemsCell.nib, forCellReuseIdentifier: DeliverySystemsCell.identifier)
      self.tableView.register(DeliveryLogCell.nib, forCellReuseIdentifier: DeliveryLogCell.identifier)
        //self.tableView.estimatedRowHeight = 125
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isDeliveryLog{
            if self.deliverySystems.count > 0{
                return self.deliverySystems.count
            }
            return 0
        }else{
            if self.deliveryLog.count > 0{
                return self.deliveryLog.count
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isDeliveryLog{
            let cell : DeliverySystemsCell = tableView.dequeueReusableCell(withIdentifier: DeliverySystemsCell.identifier, for: indexPath) as! DeliverySystemsCell
            cell.selectionStyle = .none
            if self.deliverySystems.count > 0 {
                let deliveryData = self.deliverySystems[indexPath.row]
                
                cell.deliverySystems = deliveryData
                
                cell.nameLabel.text = deliveryData.name
                self.deliverySystemName = deliveryData.name
                let firstAlphabet = Array(deliveryData.name.capitalized)[0]
                cell.alphabetLabel.text = String(describing: firstAlphabet)
//                if deliveryData.phone == nil{
//                    cell.phoneLabel.text = "---"
//                }
                cell.phoneLabel.text = deliveryData.phone
                cell.currentDueLabel.text = LanguageManager.CurrentDue + " : " +  "\(deliveryData.currentDue)"
                
                cell.popUpBtn.tag = deliveryData.id
                cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
                
                if isLoading == false && indexPath.row == self.deliverySystems.count - 1 && self.currentListPage < self.lastListPageNo{
                    self.isLoading = true
                    self.currentListPage += 1
                    self.presenter.getDeliveryDataFromServer(page: self.currentListPage)
                }
            }
            return cell
        }else{
            let cell : DeliveryLogCell = tableView.dequeueReusableCell(withIdentifier: DeliveryLogCell.identifier, for: indexPath) as! DeliveryLogCell
            cell.selectionStyle = .none
            if self.deliveryLog.count > 0 {
                let log = self.deliveryLog[indexPath.row]
                
                cell.deliveryLog = log
                
                cell.dateLabel.text = log.createdAt
                cell.amountLabel.text = LanguageManager.Amount + " : " + "\(log.amount)" + " " + Constants.currencySymbol
                cell.deliverySystemLabel.text = LanguageManager.DeliverySystem + " : " + log.deliverySystem
                if log.type == 0 {
                    cell.typeLabel.text = LanguageManager.Due
                    cell.typeLabel.textColor = UIColor(red: CGFloat(220/255.0), green: CGFloat(53/255.0), blue: CGFloat(69/255.0), alpha: CGFloat(1.0))
                    //cell.colorView.backgroundColor = UIColor(red: CGFloat(255/255.0), green: CGFloat(225/255.0), blue: CGFloat(228/255.0), alpha: CGFloat(1.0))
                }
                if log.type == 1 {
                    cell.typeLabel.text = LanguageManager.Withdraw
                    cell.typeLabel.textColor = UIColor(red: CGFloat(91/255.0), green: CGFloat(160/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1.0))
                    //cell.colorView.backgroundColor = UIColor(red: CGFloat(241/255.0), green: CGFloat(255/255.0), blue: CGFloat(222/255.0), alpha: CGFloat(1.0))
                }
                if isLoading == false && indexPath.row == self.deliveryLog.count - 1 && self.currentLogPage < self.lastLogPageNo{
                    self.isLoading = true
                    self.currentLogPage += 1
                    self.presenter.getDeliveryLogFromServer(page: self.currentLogPage)
                }
            }
        return cell
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {

        return false
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if isDeliveryLog{
            let edit = UITableViewRowAction(style: .normal, title: "Update") { action, index in

                if self.isSearchActive{
                    if self.filteredList.count > 0 {
                        let deliverySystem = self.filteredList[indexPath.row]
                        self.navigateToUpdateDeliveryVC(deliverySystem: deliverySystem)
                    }
                }

                if self.deliverySystems.count > 0{
                    let deliverySystem = self.deliverySystems[indexPath.row]

                    self.navigateToUpdateDeliveryVC(deliverySystem: deliverySystem)
                }
            }

            edit.backgroundColor = UIColor.orange

            return [edit]
        }else{
            return[]
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isDeliveryLog{
            if self.deliverySystems.count > 0 {
                let deliverySystems = self.deliverySystems[indexPath.row]
                self.navigateToDeliverySystemDetailsVC(data: deliverySystems)
            }
        }
        else{
            if self.deliveryLog.count > 0 {
                let list = self.deliveryLog[indexPath.row]
                self.navigateToDeliveryDetailsVC(id: list.deliverySystemId)
            }
        }
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if isDeliveryLog{
            if self.deliverySystems.count > 0 {
                let deliverySystemId = sender.tag
                for item in self.deliverySystems{
                    if deliverySystemId == item.id{
                        
                        let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                            self.navigateToUpdateDeliveryVC(deliverySystem: item)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(updateAction)
                        myActionSheet.addAction(cancelAction)
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
//
//    func navigateToDeliveryDetailsVC(id: Int){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "DeliveryDetailsBaseViewController") as! DeliveryDetailsBaseViewController
//            viewController.deliverySystem = id
//            //viewController.deliverySystemName = self.deliverySystemName
//            self.navigationController?.pushViewController(viewController, animated: true)
//    }
    
    
    func navigateToDeliveryDetailsVC(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DBaseViewController") as! DBaseViewController
        viewController.deliverySystem = id
        //viewController.deliverySystemName = self.deliverySystemName
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
//    func navigateToDeliverySystemDetailsVC(data: DeliverySystems){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "DeliveryDetailsBaseViewController") as! DeliveryDetailsBaseViewController
//        viewController.deliverySystem = data.id
//        viewController.deliverySystemName = data.name
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
    
    func navigateToDeliverySystemDetailsVC(data: DeliverySystems){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DBaseViewController") as! DBaseViewController
        viewController.deliverySystem = data.id
        viewController.deliverySystemName = data.name
        self.navigationController?.pushViewController(viewController, animated: true)

    }
    
    func navigateToUpdateDeliveryVC(deliverySystem : DeliverySystems){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DeliverySystemUpdateViewController") as! DeliverySystemUpdateViewController
        viewController.deliverySystemData = deliverySystem
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//MARK: Api Delegate
extension DeliveryViewController : DeliveryViewDelegate {
    
    func setDeliveryLog(data: DeliveryLogMapper) {
        guard let deliveryLog = data.dueLog, deliveryLog.count > 0 else{
            return
        }
        self.isLoading = false
        self.deliveryLog += deliveryLog
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastLogPageNo = pagination.lastPageNo
        
    }
    
    func setDeliveryData(data: DeliveryDataMapper) {
        guard let deliverySummaryList = data.summary, deliverySummaryList.count > 0 else {
            return
        }
        self.deliverySummary = deliverySummaryList
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        guard let list = data.deliverySystems, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.deliverySystems += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastListPageNo = pagination.lastPageNo
        
    }
    
    func addDeliverySystem(data: DeliverySystemAddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.showAlert(title: message, message: "")
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
//        self.deliveryLog
        self.deliverySystems = []
        self.presenter.getDeliveryDataFromServer(page: self.currentListPage)
    }
}

extension DeliveryViewController{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewDeliverySystemAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.DeliverySystem
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.PhoneNumber
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.Address
            textField.textAlignment = .center
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
           // self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.DeliveryNameIsRequired, message: "")
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                self.showAlert(title: LanguageManager.PhoneNumberIsRequired, message: "")
                return
            }
            
            let param : [String : String] = ["name": textField.text!, "phone": textField2.text!, "address": ""]
            
            self.presenter.postAddNewDeliverySystemDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
}
