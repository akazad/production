//
//  ProductDataManager.swift
//  Ponno
//
//  Created by a k azad on 15/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

struct ProductObject {
    static var productImage : UIImage?
    static var productName: String?
    static var productCategoryId : Int?
    static var companyName: String?
    static var varient : String?
    static var unitId : Int?
    static var sku : String?
    static var serial: String?
    static var serialNumberStatus : String?
    static var stockAlert : String?
    static var warranty : String?
    static var expireDate : String?
    static var buyingPrice : String?
    static var sellingPrice : String?
    static var vendorId : Int?
    static var quantity : String?
    
}

struct RawMaterialObject {
    static var rawMaterialImage : UIImage?
    static var rawMaterialName: String?
    static var rawMaterialCategoryId : Int?
    static var companyName: String?
    static var varient : String?
    static var unitId : Int?
    static var sku : String?
    static var stockAlert : String?
    static var expireDate : String?
    static var buyingPrice : String?
    static var vendorId : Int?
    static var quantity : String?
    
}

