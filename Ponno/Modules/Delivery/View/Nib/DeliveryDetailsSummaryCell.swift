//
//  DeliveryDetailsSummaryCell.swift
//  Ponno
//
//  Created by a k azad on 20/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DeliveryDetailsSummaryCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var currentOrderTitleLbl: UILabel!

    @IBOutlet weak var currentOrderLabel: UILabel!
    @IBOutlet weak var totalOrderTitleLbl: UILabel!
    @IBOutlet weak var totalOrderLabel: UILabel!
    @IBOutlet weak var invoiceDueTitleLbl: UILabel!
    @IBOutlet weak var invoiceDueLbl: UILabel!
    @IBOutlet weak var invoiceDuePaidTitleLbl: UILabel!
    @IBOutlet weak var invoiceDuePaidLbl: UILabel!
    @IBOutlet weak var othersDueTitleLbl: UILabel!
    @IBOutlet weak var othersDueLbl: UILabel!
    
    @IBOutlet weak var othersDuePaidTitleLbl: UILabel!
    @IBOutlet weak var othersDuePaidLbl: UILabel!
    @IBOutlet weak var totalDueTitleLbl: UILabel!
    @IBOutlet weak var totalDueLbl: UILabel!
    @IBOutlet weak var totalDuePaidTitleLbl: UILabel!
    @IBOutlet weak var totalDuePaidLbl: UILabel!
    
    var deliverySystemDetails : DeliverySystemDetails?{
        didSet{
            if let deliverySystem = self.deliverySystemDetails {
                currentOrderLabel.text = "\(deliverySystem.pendingInvoice)"
                totalOrderLabel.text = "\(deliverySystem.totalInvoice)"
                invoiceDueLbl.text = "\(deliverySystem.pendingInvoiceTotal)" + Constants.currencySymbol
                invoiceDuePaidLbl.text = "\(deliverySystem.receivedInvoiceTotal)" + Constants.currencySymbol
                othersDueLbl.text = "\(deliverySystem.otherDue)" + Constants.currencySymbol
                othersDuePaidLbl.text = "\(deliverySystem.otherWithdraw)" + Constants.currencySymbol
                let totalDue = generateTotal(item1: deliverySystem.pendingInvoiceTotal, item2: deliverySystem.otherDue)
                let totalDuePaid = generateTotal(item1: deliverySystem.receivedInvoiceTotal, item2: deliverySystem.otherWithdraw)
                //totalDuePaidLbl.text = "\(totalDuePaid)"
                totalDueLbl.text = "\(totalDue)"
                totalDuePaidLbl.text = "\(totalDuePaid)"
                checkValue(value: Double(deliverySystem.pendingInvoice), label: currentOrderLabel)
                checkValue(value: deliverySystem.pendingInvoiceTotal, label: invoiceDueLbl)
                checkValue(value: deliverySystem.otherDue, label: othersDueLbl)
                checkValue(value: deliverySystem.otherDue, label: othersDueLbl)
                checkValue(value: totalDue, label: totalDueLbl)
                //checkValue(value: totalDuePaid, label: totalDuePaidLbl)
            }
        }
    }
    
    func checkValue(value: Double, label: UILabel){
        if value > 0{
            label.textColor = UIColor.red
        }else{
            label.textColor = UIColor.black
        }
        
    }
    
    func generateTotal(item1: Double, item2: Double) -> Double{
        return item1 + item2
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupTitleLbl()
        self.selectionStyle = .none
    }
    
    func setupTitleLbl(){
        currentOrderTitleLbl.text = LanguageManager.CurrentOrder
        totalOrderTitleLbl.text = LanguageManager.TotalOrder
        invoiceDuePaidTitleLbl.text = LanguageManager.TotalDuePaid
        othersDueTitleLbl.text = LanguageManager.OthersDue
        othersDuePaidTitleLbl.text = LanguageManager.OthersDuePaid
        totalDueTitleLbl.text = LanguageManager.TotalDue
        totalDuePaidTitleLbl.text = LanguageManager.TotalDuePaid
        invoiceDueTitleLbl.text = LanguageManager.InvoiceDue
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: DeliveryDetailsSummaryCell.self)
    }
}
