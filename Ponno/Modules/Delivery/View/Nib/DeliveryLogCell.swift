//
//  DeliveryLogeCell.swift
//  Ponno
//
//  Created by a k azad on 3/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DeliveryLogCell: UITableViewCell {
    
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var deliverySystemLabel: UILabel!
    
    var deliveryLog : DeliveryDueLogList? {
        didSet{
            if let deliveryHistory = deliveryLog{
                setLabelTextColor(amount: deliveryHistory.amount.toDouble(), label: amountLabel)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: DeliveryLogCell.self)
    }
    
}
