//
//  DeliveryInvoiceCell.swift
//  Ponno
//
//  Created by a k azad on 19/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DeliveryInvoiceCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var invoiceLabel: UILabel!
    @IBOutlet weak var deliveryChargeLabel: UILabel!
    @IBOutlet weak var paidLabel: UILabel!
    @IBOutlet weak var dueLabel: UILabel!
    @IBOutlet weak var invoicePopUpBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: DeliveryInvoiceCell.self)
    }
}
