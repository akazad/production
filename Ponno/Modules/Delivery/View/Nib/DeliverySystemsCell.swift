//
//  DeliverySystemsCell.swift
//  Ponno
//
//  Created by a k azad on 1/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DeliverySystemsCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var alphabetView: UIView!
    @IBOutlet weak var alphabetLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var currentDueLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var popUpBtn: UIButton!
    
    var deliverySystems : DeliverySystems?{
        didSet{
            if let deliverySystem = deliverySystems{
                setLabelTextColor(amount: deliverySystem.currentDue.toDouble(), label: currentDueLabel)
            }
        }
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: DeliverySystemsCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setUpViews()
        setUpCardView(uiview: cardView)
    }
    
    private func setUpViews (){
        self.alphabetView.layer.cornerRadius = self.alphabetView.frame.height/2
        self.alphabetView.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
