//
//  DeliveryDetailsSaleHistory.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DeliveryDetailsSaleHistory : Mappable {
    var id : Int = 0
    var invoice : String = ""
    var paid : String = ""
    var due : String = ""
    var deliveryCharge : String = ""
    var createdAt : String = ""
    var saleToken : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        invoice <- map["invoice"]
        paid <- map["paid"]
        due <- map["due"]
        deliveryCharge <- map["delivery_charge"]
        createdAt <- map["created_at"]
        saleToken <- map["sale_token"]
    }
    
}

