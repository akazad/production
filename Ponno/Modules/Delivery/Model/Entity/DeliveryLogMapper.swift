//
//  DeliveryLogMapper.swift
//  Ponno
//
//  Created by a k azad on 3/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DeliveryLogMapper : Mappable {
    var success : Bool?
    var message : String?
    var dueLog : [DeliveryDueLogList]? 
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        dueLog <- map["due_log"]
        pagination <- map["pagination"]
    }
    
}

