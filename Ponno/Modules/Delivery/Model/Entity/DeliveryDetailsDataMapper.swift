//
//  DeliveryDetailsDataMapper.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DeliveryDetailsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var deliverySystem : DeliverySystemDetails?
    var deliveryDetailsCurrentOrder : DeliveryDetailsCurrentOrder?
    var saleHistory : DeliveryDetailsSaleHistory?
    var dueHistory : DeliveryDetailsDueHistroyDataMapper?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        deliverySystem <- map["delivery_system"]
        deliveryDetailsCurrentOrder <- map["current_order"]
        saleHistory <- map["sale_history"]
        dueHistory <- map["due_history"]
    }
    
}

