//
//  DeliveryDetailsDue.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DeliveryDetailsDueHistory : Mappable {
    var amount : String = ""
    var createdAt : String = ""
    var seller : String = ""
    var type : Int = 0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        amount <- map["amount"]
        createdAt <- map["created_at"]
        seller <- map["seller"]
        type <- map["type"]
    }
    
}

