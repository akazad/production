//
//  DeliverySystemAdd.swift
//  Ponno
//
//  Created by a k azad on 7/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DeliverySystemAdd : Mappable {
    var id : Int = 0
    var name : String = ""
    var phone : String = ""
    var description : String = ""
    var pharmacyId : Int = 0
    var addedBy : Int = 0
    var currentDue : Int = 0
    var updatedAt : String = ""
    var createdAt : String = ""
    
    
    required init(map: Map) {
        
    }
    
    init(id: Int, name: String, phone: String){
        self.id = id
        self.name = name
        self.phone = phone
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        description <- map["description"]
        pharmacyId <- map["pharmacy_id"]
        addedBy <- map["added_by"]
        currentDue <- map["current_due"]
        updatedAt <- map["updated_at"]
        createdAt <- map["created_at"]
        
    }
    
}

