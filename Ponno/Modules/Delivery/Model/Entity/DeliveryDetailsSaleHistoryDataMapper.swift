//
//  DeliveryDetailsSaleHistoryDataMapper.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DeliveryDetailsSaleHistoryDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var sales : [DeliveryDetailsSaleHistory]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        sales <- map["sales"]
        pagination <- map["pagination"]
    }
    
}

