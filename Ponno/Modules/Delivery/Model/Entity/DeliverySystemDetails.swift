//
//  DeliverySystemDetails.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DeliverySystemDetails : Mappable {
    var id : Int = 0
    var name : String = ""
    var phone : String = ""
    var totalInvoice : Int = 0
    var pendingInvoice : Int = 0
    var pendingInvoiceTotal : Double = 0.0
    var receivedInvoiceTotal: Double = 0.0
    var otherDue : Double = 0.0
    var otherWithdraw = 0.0
    
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        totalInvoice <- map["total_invoice"]
        pendingInvoice <- map["pending_invoice"]
        pendingInvoiceTotal <- map["pending_invoice_total"]
        receivedInvoiceTotal <- map["received_invoice_total"]
        otherDue <- map["other_due"]
        otherWithdraw <- map["other_withdraw"]
    }
    
}

