//
//  DueLogList.swift
//  Ponno
//
//  Created by a k azad on 3/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DeliveryDueLogList : Mappable {
    var id : Int = 0
    var createdAt : String = ""
    var amount : String = ""
    var deliverySystem : String = ""
    var deliverySystemId : Int = 0
    var type : Int = 0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        createdAt <- map["created_at"]
        amount <- map["amount"]
        deliverySystem <- map["delivery_system"]
        deliverySystemId <- map["delivery_system_id"]
        type <- map["type"]
    }
    
}

