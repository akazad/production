//
//  DeliveryDetailsDueHistroy.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DeliveryDetailsDueHistroyDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var dues : [DeliveryDetailsDueHistory]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        dues <- map["dues"]
        pagination <- map["pagination"]
    }
    
}
