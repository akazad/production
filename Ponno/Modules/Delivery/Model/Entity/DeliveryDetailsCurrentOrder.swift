//
//  DeliveryDetailsOrder.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DeliveryDetailsCurrentOrder : Mappable {
    var id : Int = -1
    var saleId : Int = -1
    var saleToken : String = ""
    var amount : String = ""
    var deliverySystem : Int = -1
    var deliverySystemName : String = ""
    var deliveryCharge : String = ""
    var commission : String = ""
    var commissionUnit : String = ""
    var commissionAmount : String = ""
    var receivedAmount : String = ""
    var receivedDate : String = ""
    var status : Int = -1
    var createdAt : String = ""
    var updatedAt : String = ""
    var invoice : String = ""
    var addedBy : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        saleId <- map["sale_id"]
        saleToken <- map["sale_token"]
        amount <- map["amount"]
        deliverySystem <- map["delivery_system"]
        deliverySystemName <- map["delivery_system_name"]
        deliveryCharge <- map["delivery_charge"]
        commission <- map["commission"]
        commissionUnit <- map["commission_unit"]
        commissionAmount <- map["commission_amount"]
        receivedAmount <- map["received_amount"]
        receivedDate <- map["received_date"]
        status <- map["status"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        invoice <- map["invoice"]
        addedBy <- map["added_by"]
    }
    
}

