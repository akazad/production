//
//  DeliveryDataMapper.swift
//  Ponno
//
//  Created by a k azad on 31/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class DeliveryDataMapper : Mappable { 
    var success : Bool?
    var message : String?
    var summary : [DeliverySummary]?
    var deliverySystems : [DeliverySystems]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        deliverySystems <- map["delivery_systems"]
        pagination <- map["pagination"]
    }
    
}

