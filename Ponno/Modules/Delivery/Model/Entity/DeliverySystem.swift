//
//  DeliverySystemList.swift
//  Ponno
//
//  Created by a k azad on 31/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DeliverySystems : Mappable {
    var id : Int = 0
    var name : String = ""
    var phone : String = ""
    var description: String = ""
    var invoiceDue: String = ""
    var otherDue : String = ""
    var currentDue: String = ""
    var pharmacyId: Int = -1
    var addedBy: Int = -1
    var createdAt: String = ""
    var updatedAt: String = ""
    var deletedAt: String = ""
    
    required init(map: Map) {
        
    }
    
    init(id: Int, name: String, phone: String) {
        self.id = id
        self.name = name
        self.phone = phone
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        description <- map["description"]
        invoiceDue <- map["invoice_due"]
        otherDue <- map["other_due"]
        currentDue <- map["current_due"]
        addedBy <- map["pharmacy_id"]
        pharmacyId <- map["added_by"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
    }
    
}
