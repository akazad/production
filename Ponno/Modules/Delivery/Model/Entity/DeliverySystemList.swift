//
//  DeliverySystemList.swift
//  Ponno
//
//  Created by a k azad on 31/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DeliverySystems : Mappable {
    var id : Int = 0
    var name : String = ""
    var phone : String = ""
    var currentDue : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        currentDue <- map["current_due"]
    }
    
}
