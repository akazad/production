//
//  DeliveryDetailsCurrentOrderDataMapper.swift
//  Ponno
//
//  Created by a k azad on 18/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import ObjectMapper

class DeliveryDetailsCurrentOrderDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var orders : [DeliveryDetailsCurrentOrder]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        orders <- map["orders"]
        pagination <- map["pagination"]
    }
    
}

