//
//  DeliveryService.swift
//  Ponno
//
//  Created by a k azad on 31/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

public class DeliveryService : NSObject{
    
    func getDeliveryData(page: Int, success: @escaping (DeliveryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let deliveryURL = RestURL.sharedInstance.Delivery + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(deliveryURL)
        
        let deliveryHeader = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(deliveryURL, headers: deliveryHeader)
            .responseObject { (response: DataResponse<DeliveryDataMapper>) in
                if let deliveryResponse = response.result.value{
                    if let delivery = deliveryResponse.deliverySystems, delivery.count > 0 {
                         success(deliveryResponse)
                    }else if let failureMessage = deliveryResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getAllDeliveryData(getAll: Bool, success: @escaping (DeliveryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let deliveryURL = RestURL.sharedInstance.Delivery + RestURL.sharedInstance.getAllData(text: getAll)
        print(deliveryURL)
        
        let deliveryHeader = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(deliveryURL, headers: deliveryHeader)
            .responseObject { (response: DataResponse<DeliveryDataMapper>) in
                if let deliveryResponse = response.result.value{
                    if let delivery = deliveryResponse.deliverySystems, delivery.count > 0 {
                        success(deliveryResponse)
                    }else if let failureMessage = deliveryResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getDeliveryLogSearchData(page: Int, startDate: String, endDate: String, success: @escaping (DeliveryLogMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let deliveryURL = RestURL.sharedInstance.DeliveryLog + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getPageNumber(page: page)
        
        print(deliveryURL)
        
        let deliveryHeader = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(deliveryURL, headers: deliveryHeader)
            .responseObject { (response: DataResponse<DeliveryLogMapper>) in
                if let deliveryResponse = response.result.value{
                    if let delivery = deliveryResponse.dueLog, delivery.count > 0 {
                        success(deliveryResponse)
                    }else if let failureMessage = deliveryResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //DeliveryListSearch
    func getDeliveryListSearchData(page: Int, searchText: String, success: @escaping (DeliveryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let deliveryURL = RestURL.sharedInstance.Delivery + RestURL.sharedInstance.getSearchText(text: searchText) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = deliveryURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<DeliveryDataMapper>) in
                if let deliveryResponse = response.result.value{
                    if let delivery = deliveryResponse.deliverySystems, delivery.count > 0 {
                        success(deliveryResponse)
                    }else if let failureMessage = deliveryResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getDeliveryLog(page: Int, success: @escaping (DeliveryLogMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DeliveryLog + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<DeliveryLogMapper>) in
                if let deliveryLogResponse = response.result.value{
                    if let log = deliveryLogResponse.dueLog, log.count > 0 {
                        success(deliveryLogResponse)
                    }else if let failureMessage = deliveryLogResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getDeliveryDetailsData(id: Int, success: @escaping (DeliveryDetailsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DeliveryDetails + RestURL.sharedInstance.getDeliveryDetailsUrl(id: id)
        //print(url)
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<DeliveryDetailsDataMapper>) in
                if let deliveryDetailsResponse = response.result.value{
                    if deliveryDetailsResponse.success == true{
                        success(deliveryDetailsResponse)
                    }
                    else if let failureMessage = deliveryDetailsResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getDeliveryDetailsCurrentOrder(page: Int, id: Int, success: @escaping (DeliveryDetailsCurrentOrderDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DeliveryDetailsCurrentOrder +  RestURL.sharedInstance.getDeliveryDetailsUrl(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<DeliveryDetailsCurrentOrderDataMapper>) in
                if let deliveryCurrentOrderResponse = response.result.value{
                    if let currentOrder = deliveryCurrentOrderResponse.orders, currentOrder.count > 0 {
                        success(deliveryCurrentOrderResponse)
                    }else if let failureMessage = deliveryCurrentOrderResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getDeliveryDetailsSaleHistory(page: Int, id: Int,  success: @escaping (DeliveryDetailsSaleHistoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DeliveryDetailsSaleHistory +
            RestURL.sharedInstance.getDeliveryDetailsUrl(id: id) +
            RestURL.sharedInstance.getPaginationNumber(page: page)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<DeliveryDetailsSaleHistoryDataMapper>) in
                if let deliverySaleHistoryResponse = response.result.value{
                    if let saleHistory = deliverySaleHistoryResponse.sales, saleHistory.count > 0 {
                        success(deliverySaleHistoryResponse)
                    }else if let failureMessage = deliverySaleHistoryResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getDeliveryDetailsDueHistory(page: Int, id: Int,  success: @escaping (DeliveryDetailsDueHistroyDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DeliveryDetailsDueHistory +
            RestURL.sharedInstance.getDeliveryDetailsUrl(id: id) +
            RestURL.sharedInstance.getPaginationNumber(page: page)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<DeliveryDetailsDueHistroyDataMapper>) in
                if let deliveryDueHistoryResponse = response.result.value{
                    if let DueHistory = deliveryDueHistoryResponse.dues, DueHistory.count > 0 {
                        success(deliveryDueHistoryResponse)
                    }else if let failureMessage = deliveryDueHistoryResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //Mark: PostRequest
    func postNewDeliverySystemDueData(deliveryDueData: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.DeliveryDueAdd
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = deliveryDueData
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(URL, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let deliveryDueResponse = response.result.value{
                if deliveryDueResponse.success == true{
                    success(deliveryDueResponse)
                }else if let failureMessage = deliveryDueResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postDeliverySystemUpdateData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DeliveryUpdate
        //print(URL)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = param
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let deliveryDueAddResponse = response.result.value {
                if deliveryDueAddResponse.success == true{
                    success(deliveryDueAddResponse)
                }else if let failureMessage = deliveryDueAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postNewDeliverySystemAddData(param: [String: Any] , success: @escaping (DeliverySystemAddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DeliveryAdd
        //print(URL)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = param
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<DeliverySystemAddDataMapper>) in
            if let deliveryAddResponse = response.result.value {
                if deliveryAddResponse.success == true{
                    success(deliveryAddResponse)
                }else if let failureMessage = deliveryAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postDeliveryInvoiceRecieveData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DeliveryInvoiceRecieve
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let deliveryInvoiceRecieveResponse = response.result.value {
                if deliveryInvoiceRecieveResponse.success == true{
                    success(deliveryInvoiceRecieveResponse)
                }else if let failureMessage = deliveryInvoiceRecieveResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    //Mark: PostRequest
    func postDeliverySystemOtherDueStoreData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.DeliveryDueAdd
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(URL, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let dueResponse = response.result.value{
                if dueResponse.success == true{
                    success(dueResponse)
                }else if let failureMessage = dueResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postDeliverySystemOtherDuePaidData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.DeliveryOtherDueWithdraw
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(URL, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let dueResponse = response.result.value{
                if dueResponse.success == true{
                    success(dueResponse)
                }else if let failureMessage = dueResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
}
