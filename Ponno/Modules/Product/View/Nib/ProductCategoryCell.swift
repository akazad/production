//
//  ProductCategoryCell.swift
//  Ponno
//
//  Created by a k azad on 17/7/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductCategoryCell: UITableViewCell {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var subCategoryAddBtn: UIButton!
    @IBOutlet weak var categoryItemLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpCardView(uiview: cardView)
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: ProductCategoryCell.self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
