//
//  DamagedPerProductCell.swift
//  Ponno
//
//  Created by a k azad on 2/10/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DamagedPerProductCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var popUpBtn: UIButton!
    @IBOutlet weak var serialBtn: UIButton!
    @IBOutlet weak var infoImage: UIImageView!
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: DamagedPerProductCell.self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
