//
//  ProductListCell.swift
//  Ponno
//
//  Created by a k azad on 28/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class ProductListCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productVariantLabel: UILabel!
    @IBOutlet weak var productQuantityLabel: UILabel!
    @IBOutlet weak var soldQuantityLbl: UILabel!
    @IBOutlet weak var productCategoryLabel: UILabel!
    @IBOutlet weak var productSellingPriceLabel: UILabel!
    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var warningBtn: UIButton!
    @IBOutlet weak var preOrderQuantityLbl: UILabel!
    
    var product : ProductList?{
        didSet{
            if let productItem = product{
                if let productQuantity = productItem.quantity.toInt(){
                    let stockAlert = productItem.stockAlert
                    if productQuantity <= stockAlert{
                        productQuantityLabel.textColor = UIColor.red
                    }else{
                        productQuantityLabel.textColor = UIColor.black
                    }
                }
                
                warningBtn.isHidden = true
                soldQuantityLbl.isHidden = true
                
                if productItem.preOrderQuantity != "0.00"{
                    preOrderQuantityLbl.isHidden = false
                    if let preOrderQuantity = productItem.preOrderQuantity.toInt(), let quantity = productItem.quantity.toInt(){
                        if preOrderQuantity > quantity{
                            preOrderQuantityLbl.backgroundColor = UIColor.init(red: 220, green: 53, blue: 69)
                        }else{
                            preOrderQuantityLbl.backgroundColor = UIColor.init(red: 61, green: 190, blue: 65)
                        }
                    }
                    preOrderQuantityLbl.text = productItem.preOrderQuantity
                }else{
                    preOrderQuantityLbl.isHidden = true
                }
                
                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + productItem.image), placeholderImage: UIImage(named: "Product-1"))
                productNameLabel.text = String(describing: productItem.name)
                productVariantLabel.text = LanguageManager.Varient + " : " + String(describing: productItem.variant)
                
                if let category = getBusinessCategory(){
                    if category.businessCategory == 1{
                        productQuantityLabel.isHidden = true
                    }else{
                        productQuantityLabel.text = LanguageManager.Quantity + " : " + String(describing: productItem.quantity) + " " + productItem.unit
                    }
                }
                
                productCategoryLabel.text = LanguageManager.Category + " : " + String(describing: productItem.categoryName)
                
                if productItem.sellingPrice.isEmpty{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + "\(0.00)" + " " + Constants.currencySymbol
                }else{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + String(describing: productItem.sellingPrice) + " " + Constants.currencySymbol
                }
            }
        }
    }
    
    var freemiumProducts : FreemiumProducts?{
        didSet{
            if let productItem = freemiumProducts{
                
                productQuantityLabel.isHidden = true
                warningBtn.isHidden = true
                soldQuantityLbl.isHidden = true
                preOrderQuantityLbl.isHidden = true

                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                if let image = productItem.image{
                    imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + image), placeholderImage: UIImage(named: "Product-1"))
                }
                
                if let productName = productItem.name{
                    productNameLabel.text = productName
                }
                
                if let varient = productItem.varient{
                    productVariantLabel.text = LanguageManager.Varient + " : " + varient
                }else{
                    productVariantLabel.text = LanguageManager.Varient + " : "
                }
                
                if let categoryName = productItem.categoryName{
                    productCategoryLabel.text = LanguageManager.Category + " : " + categoryName
                }else{
                    productCategoryLabel.text = LanguageManager.Category + " : "
                }
                
                
                if productItem.sellingPrice == 0.00{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + "\(0.00)" + " " + Constants.currencySymbol
                }else{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + String(describing: productItem.sellingPrice) + " " + Constants.currencySymbol
                }
            }
        }
    }
    
    var selectedProduct : ProductList?{
        didSet{
            if let item = selectedProduct{
                
                selectBtn.isHidden = false
                preOrderQuantityLbl.isHidden = true
                
                //self.setupSoldProductQuantityLblTextColor(item: item)
                if let productQuantity = item.quantity.toInt(){
                    if let soldQuan = item.soldQuantity.toInt(){
                        if productQuantity < soldQuan{
                            productQuantityLabel.textColor = UIColor.red
                        }else{
                            productQuantityLabel.textColor = UIColor.black
                        }
                    }
                }
                
                self.setupWarningBtn(item: item)
                
                if item.soldSellingPrice.isEmpty{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + "\(0.00)" + " " + Constants.currencySymbol
                }else{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + item.soldSellingPrice + " " + Constants.currencySymbol
                }
                
                if let category = getBusinessCategory(){
                    if category.businessCategory == 1{
                        productQuantityLabel.isHidden = true
                    }else{
                        productQuantityLabel.text = LanguageManager.Quantity + " : " + item.quantity + " " + item.unit
                    }
                }
                
                if soldQuantityAvailable(soldQuantity: item.soldQuantity) == false{
                    soldQuantityLbl.isHidden = true
                }else{
                    soldQuantityLbl.isHidden = false
                    soldQuantityLbl.text = item.soldQuantity
                    //soldQuantityLbl.backgroundColor = UIColor.red
                }
                
                self.setupOthersLbl(productItem: item)

            }
        }
    }
    
    var selectableProduct : ProductList?{
        didSet{
            if let productItem = selectableProduct{
                
                self.setupProductQuantityLblTextColor(productItem: productItem)

                warningBtn.isHidden = true
                selectBtn.isHidden = true
                preOrderQuantityLbl.isHidden = true
                
                if soldQuantityAvailable(soldQuantity: productItem.soldQuantity) == false{
                    soldQuantityLbl.isHidden = true
                }else{
                    soldQuantityLbl.isHidden = false
                }
                
                self.setupOthersLbl(productItem: productItem)
                
                if let category = getBusinessCategory(){
                    if category.businessCategory == 1{
                        productQuantityLabel.isHidden = true
                    }else{
                        productQuantityLabel.text = LanguageManager.Quantity + " : " + productItem.quantity + " " + productItem.unit
                    }
                }
                
                if productItem.sellingPrice.isEmpty{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + "\(0.00)" + " " + Constants.currencySymbol
                }else{
                    productSellingPriceLabel.text = LanguageManager.SellingPrice + ": " + String(describing: productItem.sellingPrice) + " " + Constants.currencySymbol
                }

            }
        }
    }
    
    var damageableProduct : ProductForDamage?{
        didSet{
            if let product = self.damageableProduct{
                
                warningBtn.isHidden = true
                soldQuantityLbl.isHidden = true
                preOrderQuantityLbl.isHidden = true
                
                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + product.image), placeholderImage: UIImage(named: "placeholder"))
                productNameLabel.text = String(describing: product.productName)
                productVariantLabel.text = LanguageManager.Varient + " : " + String(describing: product.variant)
                productQuantityLabel.text = LanguageManager.Quantity + " : " + String(describing: product.quantity) + " " + product.unit
                productCategoryLabel.text = LanguageManager.Category + " : " + String(describing: product.categoryName)
                productSellingPriceLabel.text = LanguageManager.SellingPrice + " : " + String(describing: product.sellingPrice) + " " + Constants.currencySymbol
            }
            
            
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
        soldQuantityLbl.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: ProductListCell.self)
    }

}

extension ProductListCell{
    func setupProductQuantityLblTextColor(productItem: ProductList){
        if let productQuantity = productItem.quantity.toInt(){
            if productQuantity <= 0{
                productQuantityLabel.textColor = UIColor.red
            }else{
                productQuantityLabel.textColor = UIColor.black
            }
        }
        

    }
    
    func setupSoldProductQuantityLblTextColor(item: ProductList){
        if let productQuantity = item.quantity.toInt(){
            if let soldQuan = item.soldQuantity.toInt(){
                if productQuantity < soldQuan{
                    productQuantityLabel.textColor = UIColor.red
                }else{
                    productQuantityLabel.textColor = UIColor.black
                }
            }
        }
    }
    
    func setupWarningBtn(item: ProductList){
        if let sellingPrice = item.sellingPrice.toDouble(){
            if let soldPrice = item.soldSellingPrice.toDouble(){
                if soldPrice < sellingPrice{
                    warningBtn.isHidden = false
                }else{
                    warningBtn.isHidden = true
                }
            }
        }
    }
    
    func setupOthersLbl(productItem: ProductList){
        imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + productItem.image), placeholderImage: UIImage(named: "Product-1"))
        
        productNameLabel.text = String(describing: productItem.name)
        
        productVariantLabel.text = LanguageManager.Varient + " : " + productItem.variant
        
        productCategoryLabel.text = LanguageManager.Category + " : " + productItem.categoryName
        

    }
    
    
    
}
