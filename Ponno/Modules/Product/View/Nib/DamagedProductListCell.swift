//
//  DamagedProductListCell.swift
//  Ponno
//
//  Created by a k azad on 23/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DamagedProductListCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var damagedProduct: UILabel!
    @IBOutlet weak var numberOfDamages: UILabel!
    @IBOutlet weak var totalDamages: UILabel!
    @IBOutlet weak var totalLoss: UILabel!
    @IBOutlet weak var selectBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: DamagedProductListCell.self)
    }
    
}
