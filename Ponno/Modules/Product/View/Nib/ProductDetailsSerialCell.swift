//
//  ProductDetailsSerialCell.swift
//  Ponno
//
//  Created by a k azad on 10/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductDetailsSerialCell: UITableViewCell {

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var serialLbl: UILabel!
    @IBOutlet weak var isSoldLbl: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var saleStatusView: UIView!
    @IBOutlet weak var popUpBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: ProductDetailsSerialCell.self)
    }
    
}
