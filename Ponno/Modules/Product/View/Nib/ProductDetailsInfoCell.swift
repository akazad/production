//
//  ProductDetailsInfoCell.swift
//  Ponno
//
//  Created by a k azad on 16/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductDetailsInfoCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var alphabetView: UIView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var variantLabel: UILabel!
    @IBOutlet weak var productSku: UILabel!
    @IBOutlet weak var SellingPrice: UILabel!
    @IBOutlet weak var buyingPrice: UILabel!
    @IBOutlet weak var cautionLbl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setUpViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: ProductDetailsInfoCell.self)
    }
    
    private func setUpViews (){
        self.company.text = "কোম্পানিঃ ---"
        self.categoryName.text = "ক্যাটেগরিঃ ---"
        self.variantLabel.text = "ভিন্নতাঃ ---"
        self.productSku.text = "পণ্যের কোডঃ ---"
        self.cautionLbl.text = "পরিমাণ সতর্কতাঃ ---"
    }
}
