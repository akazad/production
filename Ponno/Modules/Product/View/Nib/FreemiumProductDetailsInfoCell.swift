//
//  FreemiumProductDetailsInfoCell.swift
//  Ponno
//
//  Created by a k azad on 19/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class FreemiumProductDetailsInfoCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var alphabetView: UIView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var variantLabel: UILabel!
    @IBOutlet weak var productSku: UILabel!
    @IBOutlet weak var SellingPrice: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    
    var product : Product?{
        didSet{
            if let productDetails = product{
                
                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + productDetails.image), placeholderImage: UIImage(named: "Product-1"))
                
                productName.text = productDetails.name.capitalized
                
                let companyName = productDetails.company
                if companyName.isEmpty{
                    company.text = LanguageManager.Company + " : ---"
                }else{
                    company.text = LanguageManager.Company + " : " + companyName
                }
                
                categoryName.text = LanguageManager.Category + " : " + productDetails.categoryName
                
                let variant = productDetails.variant
                if variant.isEmpty{
                    variantLabel.text = LanguageManager.Varient + " : ---"
                }else{
                    variantLabel.text = LanguageManager.Varient + ": " + variant
                }
                
                let sku = productDetails.sku
                if sku.isEmpty{
                    productSku.text = LanguageManager.ProductCode + " : ---"
                }else{
                    productSku.text = LanguageManager.ProductCode + " : " + sku
                }
                
                let sellingPrice = productDetails.sellingPrice
                if sellingPrice.isEmpty{
                    SellingPrice.text = LanguageManager.SellingPrice + " : " + "0.00" + " " + Constants.currencySymbol
                }else{
                    SellingPrice.text = LanguageManager.SellingPrice + " : " + "\(productDetails.sellingPrice)" + " " + Constants.currencySymbol
                }
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: FreemiumProductDetailsInfoCell.self)
    }
    
}
