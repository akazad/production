//
//  ProductListSummeryCell.swift
//  Ponno
//
//  Created by a k azad on 11/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductListSummeryCell: UICollectionViewCell {

    @IBOutlet weak var summeryTitle: UILabel!
    @IBOutlet weak var summeryValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: ProductListSummeryCell.self)
    }
}
