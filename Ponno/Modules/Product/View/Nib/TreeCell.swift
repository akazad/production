//
//  TreeCell.swift
//  Ponno
//
//  Created by a k azad on 29/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class TreeCell: UITableViewCell {
    
    //MARK:  Properties & Variables

    @IBOutlet weak var treeButton: UIButton!
    @IBOutlet weak var treeLabel: UILabel!
    var treeNode: TreeViewNode!
    
    
    //MARK:  Draw Rectangle for Image
    
    override func draw(_ rect: CGRect) {
        var cellFrame: CGRect = self.treeLabel.frame
        var buttonFrame: CGRect = self.treeButton.frame
        let indentation: Int = self.treeNode.nodeLevel! * 25
        cellFrame.origin.x = buttonFrame.size.width + CGFloat(indentation) + 5
        buttonFrame.origin.x = 2 + CGFloat(indentation)
        self.treeLabel.frame = cellFrame
        self.treeButton.frame = buttonFrame
    }
    
    //MARK:  Set Background image
    
    func setTheButtonBackgroundImage(backgroundImage: UIImage)
    {
        self.treeButton.setImage(backgroundImage, for: .normal)
        //self.treeButton.imageEdgeInsets = UIEdgeInsets(top: 24, left: 24, bottom: 24, right: 24)
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier : String{
        return String(describing: TreeCell.self)
    }

    
    //MARK:  Expand Node
    
    @IBAction func expand(sender: AnyObject)
    {
        if (self.treeNode != nil)
        {
            if self.treeNode.nodeChildren != nil
            {
                if self.treeNode.isExpanded == GlobalVariables.TRUE
                {
                    self.treeNode.isExpanded = GlobalVariables.FALSE
                }
                else
                {
                    self.treeNode.isExpanded = GlobalVariables.TRUE
                }
            }
            else
            {
                self.treeNode.isExpanded = GlobalVariables.FALSE
            }
            
            self.isSelected = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TreeNodeButtonClicked"), object: self)
            print("button clicked")
        }
    }
}



