//
//  FreemiumCategoryCell.swift
//  Ponno
//
//  Created by a k azad on 20/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class FreemiumCategoryCell: UICollectionViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            self.setUpCollectionCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var categoryNameLbl: UILabel!
    
    var category: ProductCategories?{
        didSet{
            if let categoryItem = category{
                categoryNameLbl.text = categoryItem.name
            }
        }
    }
    
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? UIColor.green : UIColor.clear
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: FreemiumCategoryCell.self)
    }

}
