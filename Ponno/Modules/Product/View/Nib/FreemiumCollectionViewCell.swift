//
//  FreemiumCollectionViewCell.swift
//  Ponno
//
//  Created by a k azad on 21/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class FreemiumCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    
    var freemiumProducts: FreemiumProducts?{
        didSet{
            if let product = freemiumProducts{
                if let image = product.image{
                    productImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    productImageView.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + image), placeholderImage: UIImage(named: "box"))
                }
                
                if let name = product.name{
                    self.productNameLbl.text = name
                }
                
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: FreemiumCollectionViewCell.self)
    }

}
