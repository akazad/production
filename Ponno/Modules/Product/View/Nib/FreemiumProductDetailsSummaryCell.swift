//
//  FreemiumProductDetailsSummaryCell.swift
//  Ponno
//
//  Created by a k azad on 19/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class FreemiumProductDetailsSummaryCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            self.setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var totalSaleLbl: UILabel!
    @IBOutlet weak var totalSaleTitleLbl: UILabel!{
        didSet{
            totalSaleTitleLbl.text = LanguageManager.TotalSale
        }
    }
    @IBOutlet weak var totalSaleAmountLbl: UILabel!
    @IBOutlet weak var totalSaleAmountTitleLbl: UILabel!{
        didSet{
            totalSaleAmountTitleLbl.text = LanguageManager.TotalSaleAmount
        }
    }
    
    var product : Product?{
        didSet{
            if let productDetails = product{
                totalSaleLbl.text = String(describing: productDetails.totalSoldAmount) + Constants.times
                totalSaleAmountLbl.text = String(describing: productDetails.totalSell) + Constants.currencySymbol
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: FreemiumProductDetailsSummaryCell.self)
    }
    
}
