//
//  ProductDetailsSummaryCell.swift
//  Ponno
//
//  Created by a k azad on 22/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductDetailsSummaryCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var currentStockLabel: UILabel!
    @IBOutlet weak var currentStockTitleLbl: UILabel!{
        didSet{
            currentStockTitleLbl.text = LanguageManager.CurrentStock
        }
    }
    @IBOutlet weak var sellAbleStockLabel: UILabel!
    @IBOutlet weak var saleableStockTitleLbl: UILabel!{
        didSet{
            saleableStockTitleLbl.text = LanguageManager.SaleableStockAmount
        }
    }
    @IBOutlet weak var totalStockPrice: UILabel!
    @IBOutlet weak var totalStockPriceTitleLbl: UILabel!{
        didSet{
            totalStockPriceTitleLbl.text = LanguageManager.StockAmount
        }
    }
    @IBOutlet weak var totalSaleLabel: UILabel!
    @IBOutlet weak var totalSaleTitleLbl: UILabel!{
        didSet{
            totalSaleTitleLbl.text = LanguageManager.TotalSale
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: ProductDetailsSummaryCell.self)
    }
}
