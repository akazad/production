//
//  ProductDetailsRecentSellCell.swift
//  Ponno
//
//  Created by a k azad on 7/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductDetailsRecentSellCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var invoiceLabel: UILabel!
    @IBOutlet weak var sellingPriceLabel: UILabel!
    @IBOutlet weak var customerLabel: UILabel!
    
    var recentSale : ProductRecentSales?{
        didSet{
            if let item = recentSale{
                createdAtLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_h_mm_a.rawValue)
                createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                quantityLabel.text = LanguageManager.Quantity + " : " + "\(item.quantity)" + " " + item.unit 
                sellingPriceLabel.text = LanguageManager.SellingPrice + " : " + "\(item.totalPrice)" + " " + Constants.currencyPerUnitSymbol
                invoiceLabel.text = item.invoice
                customerLabel.text = LanguageManager.Customer + " : " + item.customerName
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: ProductDetailsRecentSellCell.self)
    }
}
