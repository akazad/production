//
//  ProductDetailsRecentStockCell.swift
//  Ponno
//
//  Created by a k azad on 22/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductDetailsRecentStockCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var quantityView: UIView!
    @IBOutlet weak var currentQuantityLbl: UILabel!
    @IBOutlet weak var stockPriceLabel: UILabel!
    @IBOutlet weak var sellingPriceLabel: UILabel!
    @IBOutlet weak var dealerNameLabel: UILabel!
    @IBOutlet weak var cautionLbl: UILabel!
    @IBOutlet weak var popUpBtn: UIButton!
    
    var recentPurchase : ProductRecentPurchase?{
        didSet{
            if let item = self.recentPurchase{
                createdAtLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_h_mm_a.rawValue)
                createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                currentQuantityLbl.text = "\(item.currentQuantity)" + " " + item.unit + "\n" + LanguageManager.StockAvailable
                if item.currentQuantity == "0.00"{
                    quantityView.backgroundColor = UIColor.init(red: 220, green: 53, blue: 69)
                }else{
                    quantityView.backgroundColor = UIColor.init(red: 90, green: 160, blue: 100)
                }
                quantityLbl.text = LanguageManager.Quantity + " : " + item.quantity + " " + item.unit
                stockPriceLabel.text = LanguageManager.PurchasePrice + " : " +  item.buyingPrice
                sellingPriceLabel.text = LanguageManager.SellingPrice + ": " + item.sellingPrice
                dealerNameLabel.text = LanguageManager.Vendor + " : " + item.name
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: ProductDetailsRecentStockCell.self)
    }
}
