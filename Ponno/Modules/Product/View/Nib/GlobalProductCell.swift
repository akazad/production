//
//  GlobalProductCell.swift
//  Ponno
//
//  Created by a k azad on 22/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class GlobalProductCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            self.setUpCardView(uiview: cardView)
        }
    }
    
    @IBOutlet weak var globalProductNameLbl: UILabel!
    
    var product : GlobalProducts?{
        didSet{
            if let item = self.product{
                globalProductNameLbl.text = item.name
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: GlobalProductCell.self)
    }
    
}
