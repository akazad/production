//
//  ProductDViewController.swift
//  Ponno
//
//  Created by a k azad on 29/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductDViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!{
        didSet{
            segmentedControl.setTitle(LanguageManager.RecentStock, forSegmentAt: 0)
            segmentedControl.setTitle(LanguageManager.RecentSale, forSegmentAt: 1)
            segmentedControl.setTitle(LanguageManager.SerialNo, forSegmentAt: 2)
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessage: UILabel!{
        didSet{
            emptyMessage.text = LanguageManager.NoInformationFound
        }
    }
    
    private var presenter = ProductPanViewPresenter(service: ProductService())
    
    var productId : Int?
    var product: Product?
    var recentPurchase : [ProductRecentPurchase] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var recentSale : [ProductRecentSales] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var serial : [ProductSerial] = [] {
        didSet{
            self.refreshTableView()
        }
    }
    //  Segment Propertise
    var isFirst = true
    var selectedSegment : Int?
    var hasSerial : Int?
    var productName: String?
    
    //  Pagination Propertise
    var isLoading : Bool = false
    var currentPage : Int = 1
    var saleCurrentPage : Int = 1
    var serialCurrentPage : Int = 1
    var lastPageNo: Int = 0
    
    var recentPurchaseId: Int?
    var serialId: Int?
    var serialNumber: String?
    
    var inventoryId : Int?
    var vendorName : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.emptyMessage.isHidden = true
        self.configureTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.segmentedControl.addTarget(self, action: #selector(onSegmentChange), for: .valueChanged)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func onSegmentChange(sender: UISegmentedControl){
        guard let id = self.productId else{
            return
        }
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            self.recentPurchase = []
            self.currentPage = 1
            self.isLoading = true
            self.presenter.getProductRecentPurchaseHistoryFromServer(page: self.currentPage, id: id)
            self.refreshTableView()
            
        case 1:
            self.currentPage = 1
            self.isLoading = true
            self.recentSale = []
            self.presenter.getProductRecentSaleHistoryFromServer(page: self.currentPage, id: id)
            self.refreshTableView()
            
        case 2:
            self.isLoading = true
            self.serial = []
            self.presenter.getProductSerialNumberFromServer(page: self.currentPage, id: id)
            self.refreshTableView()
        default:
            break;
        }
    }
    
}

//Mark: TableView Delegate and DataSource
extension ProductDViewController: UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = .clear
        self.tableView.register(ProductDetailsRecentSellCell.nib, forCellReuseIdentifier: ProductDetailsRecentSellCell.identifier)
        self.tableView.register(ProductDetailsRecentStockCell.nib, forCellReuseIdentifier: ProductDetailsRecentStockCell.identifier)
        self.tableView.register(ProductDetailsSerialCell.nib, forCellReuseIdentifier: ProductDetailsSerialCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.emptyMessage.isHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl.selectedSegmentIndex == 0 {
            if self.recentPurchase.count > 0 {
                self.emptyMessage.isHidden = true
                return self.recentPurchase.count
            }
        }
        else if segmentedControl.selectedSegmentIndex == 1 {
            if self.recentSale.count > 0 {
                self.emptyMessage.isHidden = true
                return self.recentSale.count
            }
        }
        else if segmentedControl.selectedSegmentIndex == 2 {
            if self.serial.count > 0 {
                self.emptyMessage.isHidden = true
                return self.serial.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if segmentedControl.selectedSegmentIndex == 0 {
            let cell : ProductDetailsRecentStockCell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsRecentStockCell.identifier, for: indexPath) as! ProductDetailsRecentStockCell
            cell.selectionStyle = .none
            if self.recentPurchase.count > 0 {
                let item = self.recentPurchase[indexPath.row]
                
                cell.recentPurchase = item
                
                cell.popUpBtn.addTarget(self, action: #selector(onRecentStockPopUpBtnTapped), for: .touchUpInside)
                cell.popUpBtn.tag = item.id
                
                if hasSerial == 1 {
                    let warranty = item.warranty
                    if warranty == "" {
                        cell.cautionLbl.text = LanguageManager.Warranty + "---"
                    }else{
                        cell.cautionLbl.text = LanguageManager.Warranty + " : " + item.warranty
                    }
                    
                }else{
                    let expireDate = item.expireDate
                    if expireDate != "" {
                        cell.cautionLbl.text = LanguageManager.ExpireDate + " : " + item.expireDate.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
                        
                        let oneMonthAddedDate = Calendar.current.date(byAdding: .month, value: 1, to: Date())
                        //let oneDaysAddedDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
                        
                        let expireDateString = item.expireDate
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        
                        let expireDate = dateFormatter.date(from: expireDateString)
                        
                        guard let exDate = expireDate, let oneMonAddedDate = oneMonthAddedDate else{
                            return cell
                        }
                        
                        if exDate < oneMonAddedDate {
                            cell.cautionLbl.textColor = UIColor.lightRed
                        }else{
                            cell.cautionLbl.textColor = UIColor.black
                        }
                        
                    }else{
                        cell.cautionLbl.text = LanguageManager.ExpireDate + " : " + "---"
                    }
                }
                
                
                
                //                Pagination
                if isLoading == false && indexPath.row == self.recentPurchase.count - 1 && self.currentPage < self.lastPageNo{
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.productId else{
                        return cell
                    }
                    self.presenter.getProductRecentPurchaseHistoryFromServer(page: self.currentPage, id: id)
                }
                return cell
            }
        }
        else if segmentedControl.selectedSegmentIndex == 1 {
            let cell : ProductDetailsRecentSellCell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsRecentSellCell.identifier, for: indexPath) as! ProductDetailsRecentSellCell
            cell.selectionStyle = .none
            if self.recentSale.count > 0 {
                let item = self.recentSale[indexPath.row]
                
                cell.recentSale = item
                
                if isLoading == false && indexPath.row == self.recentSale.count - 1 && self.saleCurrentPage < self.lastPageNo{
                    self.isLoading = true
                    self.saleCurrentPage += 1
                    guard let id = self.productId else{
                        return cell
                    }
                    
                    self.presenter.getProductRecentSaleHistoryFromServer(page: self.saleCurrentPage, id: id)
                }
                return cell
            }
        }
        else if segmentedControl.selectedSegmentIndex == 2 {
            let cell : ProductDetailsSerialCell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsSerialCell.identifier, for: indexPath) as! ProductDetailsSerialCell
            cell.selectionStyle = .none
            if self.serial.count > 0 {
                let item = self.serial[indexPath.row]
                cell.dateLbl.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.dateLbl.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.serialLbl.text = item.serialNo
                if item.isSold == 0 {
                    cell.isSoldLbl.text = LanguageManager.UnSold
                    cell.saleStatusView.backgroundColor = UIColor.lightRed
                }else{
                    cell.isSoldLbl.text = LanguageManager.Sold
                    cell.saleStatusView.backgroundColor = UIColor(red: 90, green: 160, blue: 100)
                }
                cell.popUpBtn.tag = item.id
                cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
                
                if isLoading == false && indexPath.row == self.serial.count - 1 && self.serialCurrentPage < self.lastPageNo {
                    self.isLoading = true
                    self.serialCurrentPage += 1
                    guard let id = self.productId else{
                        return cell
                    }
                    self.presenter.getProductSerialNumberFromServer(page: self.serialCurrentPage, id: id)
                }
                
            }
            return cell
        }
        else{
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelect indexPath: IndexPath) {
        if segmentedControl.selectedSegmentIndex == 1{
            if self.recentSale.count > 0{
                let item = self.recentSale[indexPath.row]
                let id = item.invoiceId
                self.navigateToSaleDetailsVC(iD : id)
            }
        }
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {

        return false
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if segmentedControl.selectedSegmentIndex == 0 {
            
            if self.recentPurchase.count > 0 {
                let recentPurchase = self.recentPurchase[indexPath.row]
                
                let currentStock = Double(recentPurchase.currentQuantity)
                guard let currentQuantity = currentStock else{
                    return []
                }
                if currentQuantity > 0.0 {
                    let returnPurchase = UITableViewRowAction(style: .normal, title: LanguageManager.Refund) { action, index in
                        
                        self.recentPurchaseId = recentPurchase.id
                        self.vendorName = recentPurchase.name
                        self.navigateToRecentStockReturnVC(recentStock: recentPurchase)
                    }
                    returnPurchase.backgroundColor = UIColor.orange
                    
                    let edit = UITableViewRowAction(style: .normal, title: LanguageManager.Update) { action, index in
                        self.navigateToRecentStockUpdateVC(recentStock: recentPurchase)
                    }
                    
                    edit.backgroundColor = UIColor.green
                    
                    let delete = UITableViewRowAction(style: .normal, title: LanguageManager.Delete) { action, index in
                        self.recentPurchaseId = recentPurchase.id
                        
                        guard let id = self.recentPurchaseId else{
                            return
                        }
                        self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                    }
                    delete.backgroundColor = UIColor.red
                    
                    return [edit, returnPurchase, delete]
                }
            }
        }
        return []
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if segmentedControl.selectedSegmentIndex == 1{
            if self.recentSale.count > 0{
                let item = self.recentSale[indexPath.row]
                let id = item.invoiceId
                self.navigateToSaleDetailsVC(iD : id)
            }
        }
//        else if segmentedControl.selectedSegmentIndex == 2{
//            if self.serial.count > 0 {
//                let serials = self.serial[indexPath.row]
//                self.serialId = serials.id
//                self.serialNumber = serials.serialNo
//                guard let id = self.serialId, let serialNo = self.serialNumber else{
//                    return
//                }
//                self.alertWithTextField(serialId: id, value: serialNo)
//            }
//        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onRecentStockPopUpBtnTapped(sender: UIButton){
        
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if segmentedControl.selectedSegmentIndex == 0 {
            
            if self.recentPurchase.count > 0 {
                let recentStockId = sender.tag
                var currentQuantity : Double?
                var quantity : Double?
                for recentStock in self.recentPurchase{
                    if recentStockId == recentStock.id{
                        currentQuantity = Double(recentStock.currentQuantity)
                        quantity = Double(recentStock.quantity)
                        self.recentPurchaseId = recentStock.id
                        self.vendorName = recentStock.name
                        if let currentStock = currentQuantity{
                            if currentStock > 0.0 {
                                let returnAction = UIAlertAction(title: LanguageManager.Return, style: UIAlertAction.Style.default) { (action) in
                                    self.navigateToRecentStockReturnVC(recentStock: recentStock)
                                }
                                let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                                    self.navigateToRecentStockUpdateVC(recentStock: recentStock)
                                }
                                
                                let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                                    guard let id = self.recentPurchaseId else{
                                        return
                                    }
                                    self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                                }
                                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                    
                                    self.view.endEditing(true)
                                }
                                
                                cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                                
                                // add action buttons to action sheet
                                myActionSheet.addAction(returnAction)
                                myActionSheet.addAction(updateAction)
                                if let quantity = quantity{
                                    if quantity == currentStock{
                                        myActionSheet.addAction(deleteAction)
                                    }
                                }
                                myActionSheet.addAction(cancelAction)
                            }else{
                                self.showAlert(title: LanguageManager.TheStockIsEmpty, message: "")
                            }
                        }
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if segmentedControl.selectedSegmentIndex == 2 {
            
            if self.serial.count > 0 {
                self.serialId = sender.tag
                for serial in self.serial{
                    if self.serialId == serial.id{
                        self.serialNumber = serial.serialNo
                        guard let id = self.serialId, let serialNo = self.serialNumber else{
                            return
                        }
                        
                        let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                            self.alertWithTextField(serialId: id, value: serialNo)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(editAction)
                        myActionSheet.addAction(cancelAction)
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToRecentStockReturnVC(recentStock: ProductRecentPurchase){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RecentStockReturnViewController") as! RecentStockReturnViewController
        //viewController.productInfo = product
        viewController.hasSerial = self.hasSerial
        viewController.productId = self.productId
        viewController.inventoryId = self.recentPurchaseId
        viewController.productName = self.productName
        viewController.vendorName = self.vendorName
        viewController.recentStock = recentStock
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToRecentStockUpdateVC(recentStock: ProductRecentPurchase){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RecentStockUpdateViewController") as! RecentStockUpdateViewController
        //viewController.productInfo = product
        //        guard let product = self.product else{
        //            return
        //        }
        
        //        viewController.inventoryId = product.inventoryId
        guard let hasSerial = self.hasSerial else {
            return
        }
        viewController.hasSerial = hasSerial
        viewController.inventoryId = recentStock.id
        viewController.recentStock = recentStock
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let saleDetailsVC = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        saleDetailsVC.iD = iD
        self.navigationController?.pushViewController(saleDetailsVC, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
           //UIVisualEffectView.appearance(whenContainedInInstancesOf: [UIAlertController.classForCoder() as! UIAppearanceContainer.Type]).effect = UIBlurEffect(style: .light)
        
            let OKAction = UIAlertAction(title: LanguageManager.Yes, style: .default){
                (action:UIAlertAction!) in
                
                self.presenter.postProductRecentStockDeleteDataFromServer(id: deleteId)
                //self.navigationController?.popViewController(animated: true)
            }
            let cancelAction = UIAlertAction(title: LanguageManager.No, style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
            //UIVisualEffectView.appearance(whenContainedInInstancesOf: [UIAlertController.classForCoder() as! UIAppearanceContainer.Type]).effect = UIBlurEffect(style: .light)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.recentPurchase = []
                    self.refreshTableView()
                    self.currentPage = 1
                    guard let id = self.productId else{
                        return
                    }
                self.presenter.getProductRecentPurchaseHistoryFromServer(page: self.currentPage, id: id)
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func alertWithTextField(serialId: Int, value: String){
        let alertController = UIAlertController(title: LanguageManager.SerialUpdate, message: "", preferredStyle: .alert)
 
        alertController.addTextField { textField in
            textField.placeholder = value
            textField.textAlignment = .center
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.SerialNumberIsRequired, message: "")
                return
            }
            
            let param : [String : Any] = ["id": serialId, "value": text]
            self.presenter.postSerialUpdateDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension ProductDViewController : ProductPanViewDelegate{
    func setProductRecentSaleHistory(data: ProductRecentSaleDataMapper) {
        guard let list = data.sales, list.count > 0 else{
            return
        }
        self.isLoading = false
        self.recentSale += list
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setProductRecentPurchaseHistory(data: ProductRecentPurchaseDataMapper) {
        guard let list = data.purchases, list.count > 0 else{
            return
        }
        self.isLoading = false
        self.recentPurchase += list
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setProductSerialNumber(data: ProductSerialDataMapper) {
        guard let productSerial = data.serials else{
            return
        }
        self.isLoading = false
        self.serial += productSerial
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
        
    }
    
    func deleteRecentStockData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func serialUpdateData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
        //self.refreshTableView()
    }
    
    func onFailed(data: String) {
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
        self.emptyMessage.isHidden = true
        
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
        self.emptyMessage.isHidden = false
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        
        guard let id = self.productId else{
            return
        }
        self.isLoading = true
        self.presenter.getProductRecentPurchaseHistoryFromServer(page: self.currentPage, id: id)
    }
}

extension ProductDViewController{
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
                guard let id = self.productId else{
                    return
                }
                self.serial = []
                self.presenter.getProductSerialNumberFromServer(page: self.currentPage, id: id)
                self.refreshTableView()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}
