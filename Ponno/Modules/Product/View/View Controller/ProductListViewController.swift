//
//  ProductViewController.swift
//  Ponno
//
//  Created by a k azad on 26/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

protocol ProductAdvanceSearchDelegate: NSObjectProtocol {
    func setProductAdvanceSearchParam(name: String, company: String, varient: String, categoryId: Int, dealerId: Int)
}

class ProductListViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var totalProductsLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var totalBuyingPriceLabel: UILabel!
    @IBOutlet weak var totalCategoryLabel: UILabel!
    @IBOutlet weak var addNewProductBarButton: UIBarButtonItem!
    
    private var presenter = ProductListPresenter(service: ProductService())
    
    var timer : Timer?
    var productList : [ProductList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var productListSummery : [ProductListSummery]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var searchText = ""
   // var filteredList : [ProductList] = []
    
    var currentPage : Int = 1
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    
    var productId : Int?
    var hasSerial : String?
    var showNavDrawer : Bool = true
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.showNavDrawer{
            addSlideMenuButton()
        }
        self.setNavigationBarTitle()
        self.configureTableVIew()
        self.configureCollectionView()
        self.addFloaty()
        self.setBarButton()
        //self.setUpSearchBar()
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.Inventory
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0))]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setBarButton(){
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let advanceSearchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        advanceSearchBtn.setImage(UIImage(named: "filterSearch"), for: UIControl.State.normal)
        advanceSearchBtn.addTarget(self, action: #selector(onProductAdvanceSearch(sender:)), for: UIControl.Event.touchUpInside)
        advanceSearchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        advanceSearchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        advanceSearchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: advanceSearchBtn)
        
        let popUpBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        popUpBtn.setImage(UIImage(named: "popupmenudark"), for: UIControl.State.normal)
        popUpBtn.addTarget(self, action: #selector(onBarPopUpBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        popUpBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        popUpBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        popUpBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn3 = UIBarButtonItem(customView: popUpBtn)
        
        navigationItem.setRightBarButtonItems([barBtn3,barButton2,barButton1], animated: true)
    }
    
    @objc func onSearch(sender: UIButton){
        self.navigateToProductSearchVC()
    }
    
    @objc func onProductAdvanceSearch(sender: UIButton){
        self.popOverToProductAdvanceSearchVC()
    }
    
    @objc func onBarPopUpBtnTapped(sender : UIBarButtonItem){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        let damagedProductAction = UIAlertAction(title: LanguageManager.DamagedProduct, style: UIAlertAction.Style.default) { (action) in
            self.navigateToDamagedProductListVC()
        }
        
        let moduleCategoryAction = UIAlertAction(title: LanguageManager.Category, style: UIAlertAction.Style.default) { (action) in
            self.navigateToModuleProductCategoryVC()
        }
        
        let newProductAction = UIAlertAction(title: LanguageManager.NewProduct, style: UIAlertAction.Style.default) { (action) in
            //self.navigateToProductCategoryViewController()
            self.navigateToAddNewProductViewController()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
            
            self.view.endEditing(true)
        }
        
        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
        
        myActionSheet.addAction(newProductAction)
        myActionSheet.addAction(moduleCategoryAction)
        myActionSheet.addAction(damagedProductAction)
        myActionSheet.addAction(cancelAction)
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    
    
    func navigateToDamagedProductListVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamagedProductListViewController") as! DamagedProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToModuleProductCategoryVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ModuleCategoryListViewController") as! ModuleCategoryListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToProductSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductSearchViewController") as! ProductSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewProductViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewProductViewController") as! AddNewProductViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToFreemiumSaleableViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumSaleableProductsVC") as! FreemiumSaleableProductsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToProductCategoryViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductCategoryViewController") as! ProductCategoryViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ProductListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.productList = []
        self.currentPage = 1
        self.presenter.getProductListDataFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }
    
    func popOverToProductAdvanceSearchVC(){
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "ProductAdvanceSearchVC") as? ProductAdvanceSearchVC
        popoverContentController?.modalPresentationStyle = .popover
        popoverContentController?.preferredContentSize = CGSize(width:500,height:450)
        
        
        if let controller = popoverContentController{
            controller.productAdvanceSearchDelegate = self
            if let popoverPresentationController = controller.popoverPresentationController {
                popoverPresentationController.sourceView = self.view
                popoverPresentationController.sourceRect = CGRect(x: 325, y: 56, width: 0, height: 0)
                popoverPresentationController.delegate = self
                if let popoverController = popoverContentController {
                    present(popoverController, animated: true, completion: nil)
                }
            }
        }
    }
    
}

//Mark: Floaty Delegate
extension ProductListViewController: FloatyDelegate{
    func navigateToAddSaleVC() {
        if let viewSettings = getViewSettings(){
            if viewSettings.salePanel == "FreemiumSale"{
                self.navigateToFreemiumSaleableViewController()
            }else{
                self.navigateToAddNewSaleViewController()
            }
        }
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}


//MARK: CollectionView Delegate And Data Source
extension ProductListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(ProductListSummeryCell.nib, forCellWithReuseIdentifier: ProductListSummeryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let item = self.productListSummery, item.count > 0  else {
            return 0
        }
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProductListSummeryCell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductListSummeryCell.identifier, for: indexPath) as! ProductListSummeryCell
        guard let item = self.productListSummery, item.count > 0 else{
            return cell
        }
        
        cell.summeryTitle.text = item[indexPath.row % item.count].title
        cell.summeryValue.text = item[indexPath.row % item.count].value
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70.0)
    }
    
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.productListSummery, list.count > 0 else{
            return
        }
        let listCount = list.count
        
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//MARK: TableView Delegate and Data Source
extension ProductListViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableVIew(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductListCell.nib, forCellReuseIdentifier: ProductListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.productList.count > 0  {
            return self.productList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductListCell = tableView.dequeueReusableCell(withIdentifier: ProductListCell.identifier, for: indexPath) as! ProductListCell
        cell.selectionStyle = .none
        if self.productList.count > 0 {
            let product = self.productList[indexPath.row]
            
            cell.product = product
            
            cell.selectBtn.isHidden = true
            if isLoading == false && indexPath.row == self.productList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getProductListDataFromServer(page: self.currentPage)
            }
        }
        return cell
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.productList.count > 0 {
            let listItem = self.productList[indexPath.row]
            //self.navigateToProductViewVC(product: listItem)
            self.navigateToProductDetailsBaseVC(product : listItem)
        }
    }
    
    func navigateToProductViewVC(product : ProductList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
//        viewController.product = product
        viewController.inventoryId = product.inventoryId
        viewController.productId = product.productId
        viewController.productName = product.name
        if product.serials == ""{
            viewController.hasSerial = 0
        }
        else{
            viewController.hasSerial = 1
        }
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToProductDetailsBaseVC(product : ProductList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsBaseViewController") as! ProductDetailsBaseViewController
        viewController.inventoryId = product.inventoryId
        viewController.productId = product.productId
        viewController.productName = product.name
        if product.serials == ""{
            viewController.hasSerial = 0
        }
        else{
            viewController.hasSerial = 1
        }
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

// MARK: API Delegate
extension ProductListViewController : ProductListViewDelegate {
    func setProductSearchData(data: ProductListDataMapper) {
        guard let list = data.productList else{
            return
        }
        self.isLoading = false
        self.productList += list
        
        
    }
    
    func setProductData(data: ProductListDataMapper) {

        
        
        guard let list = data.productList, list.count > 0 else{
            return
        }
        self.isLoading = false
        self.productList += list
        
        //insertFreemiumProduct(inventoryId: 0, unit: "Pc", sellingPrice: 100.0, productId: 10, name: "Coke", company: "cocaCola", varient: "black", sku: "4623", image: "", categoryId: 15, categoryName: "bevarage")
        
        guard let pagination = data.pagination else{
            return
        }
        self.lastPageNo = pagination.lastPageNo
        
        guard let summeryList = data.summary else {
            return
        }
        self.productListSummery = summeryList
        self.setUpAutoScroll()
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.currentPage = 1
        self.productList = []
        self.presenter.getProductListDataFromServer(page: self.currentPage)
    }
}

extension ProductListViewController : ProductAdvanceSearchDelegate{
    func setProductAdvanceSearchParam(name: String, company: String, varient: String, categoryId: Int, dealerId: Int) {
        var productName: String = "default"
        var companyName: String = "default"
        var variant: String = "default"
        var category: String = "default"
        var dealer: String = "default"
        if name != ""{
            productName = name
        }
        if company != ""{
            companyName = company
        }
        if varient != ""{
            variant = varient
        }
        if categoryId != 0{
            category = "\(categoryId)"
        }
        if dealerId != 0{
            dealer = "\(dealerId)"
        }
        self.productList = []
        self.presenter.getProductAdvanceSearchDateFromServer(name: productName, company: companyName, variant: variant, category: category, dealer: dealer)
    }
    
    
}

extension ProductListViewController: UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
