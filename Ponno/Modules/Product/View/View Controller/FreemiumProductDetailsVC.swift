//
//  FreemiumProductDetailsVC.swift
//  Ponno
//
//  Created by a k azad on 8/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import FloatingPanel


class FreemiumProductDetailsVC: UIViewController , FloatingPanelControllerDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    var fpc: FloatingPanelController!
    var bottomShetVC : FreemiumRecentSaleVC!
    
    let presenter = ProductViewPresenter(service: ProductService())
    
    var product : Product?
    var inventoryId : Int?
    var productId : Int?
    var productName: String?
    var vendor: Vendors?
    
    //Core Data
    var freemiumProducts : [FreemiumProducts] = []
    var newProductList: [OfflineProduct]?{
        didSet{
            resetAllRecords(in : "FreemiumProducts")
            if let products = newProductList{
                for item in products{
                    if let inventoryId = item.inventoryId, let unit = item.unit, let sellingPrice = item.sellingPrice?.toDouble(), let productId = item.productId, let name = item.name, let categoryId = item.categoryId, let categoryName = item.categoryName{
                        
                        insertFreemiumProduct(inventoryId: inventoryId, unit: unit, sellingPrice: sellingPrice, productId: productId, name: name, company: item.company ?? "", varient: item.variant ?? "", sku: item.sku ?? "", image: item.image ?? "", categoryId: categoryId, categoryName: categoryName)
                    }
                    
                }
            }
        }
    }
    var updatedProductList: [OfflineProduct]?{
        didSet{
            if let products = updatedProductList{
                for item in products{
                    if let inventoryId = item.inventoryId, let unit = item.unit, let sellingPrice = item.sellingPrice?.toDouble(), let productId = item.productId, let name = item.name, let categoryId = item.categoryId, let categoryName = item.categoryName{
                        Update(inventoryId: Int64(inventoryId), unit: unit, sellingPrice: sellingPrice, productId: Int64(productId), name: name, company: item.company ?? "", varient: item.variant ?? "", sku: item.sku ?? "", image: item.image ?? "", categoryId: Int64(categoryId), categoryName: categoryName)
                    }
                }
            }
        }
    }
    
    var deletedProductList: [OfflineProduct]?{
        didSet{
            if let products = deletedProductList{
                for item in products{
                    if let inventoryId = item.inventoryId{
                        deleteData(id: inventoryId)
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addBottomSheet()
        //retrieveData()
        self.setUpInitialLanguage()
        self.configureTableView()
        self.initialSetup()
        self.attachPresenter()
    }
    
    func addBottomSheet(){
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = UIColor.lightGreen.withAlphaComponent(0.5)
        fpc.surfaceView.layer.cornerRadius = 9.0
        fpc.surfaceView.clipsToBounds = true
        
        fpc.surfaceView.shadowHidden = false
        bottomShetVC = storyboard?.instantiateViewController(withIdentifier: "FreemiumRecentSaleVC") as? FreemiumRecentSaleVC
        
        bottomShetVC.productId = self.productId
        bottomShetVC.productName = self.productName
        
        fpc.set(contentViewController: bottomShetVC)
        fpc.track(scrollView: bottomShetVC.tableView)
        fpc.addPanel(toParent: self)
    }
    
    func removeBottomSheet(){
        fpc.removePanelFromParent(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeBottomSheet()
    }
    
    func initialSetup(){
        self.title = LanguageManager.ProductDetails
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ProductDetailsViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        guard let id = self.inventoryId else{
            return
        }
        self.presenter.getProductViewDataFromServer(id: Int(id))
        
        refreshControl.endRefreshing()
    }

}

//MARK: TableView Delegate and DataSource
extension FreemiumProductDetailsVC : UITableViewDataSource, UITableViewDelegate {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(FreemiumProductDetailsInfoCell.nib, forCellReuseIdentifier: FreemiumProductDetailsInfoCell.identifier)
        self.tableView.register(FreemiumProductDetailsSummaryCell.nib, forCellReuseIdentifier: FreemiumProductDetailsSummaryCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.white
        self.tableView.separatorStyle = .singleLine
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(self.refreshControl)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.productInfo.rawValue:
            return 1
        case Sections.ProductSummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case Sections.productInfo.rawValue:
            let cell : FreemiumProductDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: FreemiumProductDetailsInfoCell.identifier, for: indexPath) as! FreemiumProductDetailsInfoCell
            cell.selectionStyle = .none
            guard let productDetails = self.product else{
                return cell
            }
            
            cell.product = productDetails
            
            cell.editBtn.tag = productDetails.inventoryId
            cell.editBtn.addTarget(self, action: #selector(onEdit(sender:)), for: .touchUpInside)
            
            let totalSell = productDetails.totalSell
            
            if totalSell != 0.00 {
                cell.deleteBtn.isHidden = true
            }else{
                cell.deleteBtn.isHidden = false
                cell.deleteBtn.tag = productDetails.inventoryId
                cell.deleteBtn.addTarget(self, action: #selector(onDelete(sender:)), for: .touchUpInside)
            }
            
            return cell
        case Sections.ProductSummary.rawValue:
            let cell : FreemiumProductDetailsSummaryCell = tableView.dequeueReusableCell(withIdentifier: FreemiumProductDetailsSummaryCell.identifier, for: indexPath) as! FreemiumProductDetailsSummaryCell
            cell.selectionStyle = .none
            
            guard let productDetails = self.product else{
                return cell
            }
            
            cell.product = productDetails
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.productInfo.rawValue:
            return UITableView.automaticDimension
        case Sections.ProductSummary.rawValue:
            return 70.0
        default:
            return 0
        }
    }
    
    @objc func onDelete(sender: UIButton){
        self.productId = sender.tag
        guard let productId = self.productId else {
            return
        }
        
        self.confirmationMessage(userMessage: "", deleteId: productId)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.AreYouSure, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: LanguageManager.Yes, style: .default){
                (action:UIAlertAction!) in
                self.presenter.deleteProductDataFromServer(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: LanguageManager.No, style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is FreemiumProductListViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    @objc func onEdit(sender: UIButton){
        self.productId = sender.tag
        guard let productDetails = self.product else{
            return
        }
        if productDetails.pharmacyId == 0 {
            guard let productId = self.productId else {
                return
            }
            
            self.alertWithTextField(id: productId)
        }else{
            self.navigateToFreemiumProductUpdateVC()
        }
        
    }
    
    func navigateToFreemiumProductUpdateVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumProductUpdateVC") as! FreemiumProductUpdateVC
        viewController.productInfo = product
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToFreemiumProductListVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumProductListViewController") as! FreemiumProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension FreemiumProductDetailsVC{
    func alertWithTextField(id: Int){
        let alertController = UIAlertController(title: LanguageManager.Update, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.SellingPrice
            textField.textAlignment = .center
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.SellingPriceIsRequired, message: "")
                return
            }
                        
            let params : [String : Any] = ["id": id , "name" :"", "category": "",
                                           "company": "",
                                           "variant": "",
                                           "unit": "",
                                           "stock_alert": "",
                                           "sku": "",
            "selling_price": textField.text!]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            self.presenter.postProductUpdateDataToServer(params: params)
            
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
}

//Mark: Api Delegate
extension FreemiumProductDetailsVC : ProductViewDelegate{
    func deleteProductData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.showDeleteAlert(title: message, message: "")
        self.getFreemiumOfflineData()
        
    }
    
    func setProductViewData(data: ProductDetailsDataMapper) {
        guard let item = data.product else {
            return
        }
        self.product = item
        self.refreshTableView()
    }
    
    func onProductUpdate(data: AddDataMapper){
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
        self.getFreemiumOfflineData()
    }

    func setFreemiumProductData(data: OfflineProductDataMapper) {
        self.newProductList = data.newProductList
        self.updatedProductList = data.updatedProductList
        self.deletedProductList = data.deletedProductList
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.inventoryId else{
            return
        }
        self.presenter.getProductViewDataFromServer(id: id)
    }
    
    func getFreemiumOfflineData(){
        if let lUpdatedTime = getTimeStamp(){
            if lUpdatedTime.timeStamp == "default"{
                deleteAllData(entity: "FreemiumProducts")
                getOfflineProduct(timesStamp: lUpdatedTime.timeStamp ?? "")
            }else{
                getOfflineProduct(timesStamp: lUpdatedTime.timeStamp ?? "")
            }
            
        }
    }
    
    func getOfflineProduct(timesStamp: String){
        self.presenter.getFreemiumOfflineProductFromServer(timeStamp: timesStamp)
    }
    
}

extension FreemiumProductDetailsVC{
    func showDeleteAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigateToFreemiumProductListVC()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.refreshTableView()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
