//
//  DamageableProductListViewController.swift
//  Ponno
//
//  Created by a k azad on 25/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

protocol DamageableProductAddDelegate : NSObjectProtocol{
    func setSerialData(serial : String, buyingPrice : Double, description: String)
    func setProductData(amount : Double, buyingPrice : Double, description: String)
}

class DamageableProductListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    
    private var presenter = DamageableProductPresenter(service: ProductService())
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var productList : [ProductForDamage] = []
    var filteredList : [ProductForDamage] = []
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var selectedProductName : String?
    var selectedProductId : [Int] = []
    var selectedQuantities : [Double] = []
    var selectedBuyingPrices : [Double] = []
    var selectedSerialsId : [String] = []
    var selectedDescription : [String] = []
    
    var selectedProduct : ProductForDamage?
    
    var crossImage = UIImage(named: "error_red.png")
    
    var searchText = ""
    var timer : Timer?
    var lastPage : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpSearchBar()
        self.setUpViews()
        self.configureTableVIew()
        self.attachPresenter()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext(sender:)), for: .touchUpInside)
        //self.title = "Select Products"
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func onNext(sender : UIButton){
        if self.selectedProductId.count <= 0 {
            self.showAlert(title: LanguageManager.ToDamageSelectProduct, message: "")
        }else{
            self.submitData()
        }
        
    }
    
    func submitData(){
        if self.selectedProductId.count > 0 , self.selectedQuantities.count > 0 , self.selectedBuyingPrices.count > 0, self.selectedSerialsId.count > 0 {
            let param : [String : Any] = ["products": self.selectedProductId, "damaged_quantities": self.selectedQuantities, "buying_prices": self.selectedBuyingPrices, "descriptions": self.selectedDescription, "serial": self.selectedSerialsId]
            
            self.presenter.postDamagedProductStoreDataToServer(param: param)
        }
    }

}

//MARK: SearchBar Delegate
extension DamageableProductListViewController: UISearchBarDelegate
    
//    func setUpSearchBar(){
//        self.searchBar.delegate = self
//        self.searchBar.placeholder = "Product Search"
//        self.navigationItem.titleView = searchBar
//    }
//
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        self.isSearchActive = true
//    }
//
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        self.isSearchActive = false
//        self.view.endEditing(true)
//    }
//
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//
//        guard let textToSearch = searchBar.text else {
//            return
//        }
//        self.searchText = textToSearch
//
//        timer?.invalidate()
//
//        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
//    }
//
//    @objc func onSearch(){
//        self.currentPage = 1
//        self.productList = []
//        self.isLoading = true
//        self.isSearchActive = true
//        self.presenter.getDamageableProductSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
//    }
    
    {
        fileprivate func setUpSearchBar(){
            self.searchBar.delegate = self
            self.searchBar.placeholder = LanguageManager.ProductSearch
            self.navigationItem.titleView = searchBar
        }
    
        func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            isSearchActive = false
        }
    
        func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
           // isSearchActive = false
    
            self.view.endEditing(true)
        }
    
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            isSearchActive = false
            self.view.endEditing(true)
        }
    
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            isSearchActive = false
            self.view.endEditing(true)
        }
    
    
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    
    
            if self.productList.count > 0 {
                if searchText == "" {
                    isSearchActive = false
                    return
                }
            }
            guard let textToSearch = searchBar.text else {
                return
            }
    
            filteredList = self.productList.filter { (product : ProductForDamage) -> Bool in
                return product.productName.lowercased().contains(textToSearch.lowercased()) || product.categoryName.lowercased().contains(textToSearch.lowercased()) || product.variant.lowercased().contains(textToSearch.lowercased())
    
            }
    
            if filteredList.count == 0 {
                isSearchActive = false
            }
            else {
                isSearchActive = true
            }
    
        }
    }




//MARK: TableView Delegate and Data Source
extension DamageableProductListViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableVIew(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductListCell.nib, forCellReuseIdentifier: ProductListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearchActive{
            if self.filteredList.count > 0{
                return self.filteredList.count
            }
        }else{
            if self.productList.count > 0  {
                return self.productList.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductListCell = tableView.dequeueReusableCell(withIdentifier: ProductListCell.identifier, for: indexPath) as! ProductListCell
        
        
        if isSearchActive{
            if self.filteredList.count > 0 {
                let product = self.filteredList[indexPath.row]
                                
                if self.selectedProductId.contains(product.productId){
                    cell.selectBtn.isHidden = false
                    cell.selectBtn.tag = product.productId
                    cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
                }
                else{
                    cell.selectBtn.isHidden = true
                }
                
                cell.damageableProduct = product
                
                self.selectedProductName = product.productName
            }
            return cell
        }else{
            if self.productList.count > 0 {
                let product = self.productList[indexPath.row]
                
                if self.selectedProductId.contains(product.productId){
                    cell.selectBtn.isHidden = false
                    cell.selectBtn.tag = product.productId
                    cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
                }
                else{
                    cell.selectBtn.isHidden = true
                }
                
                cell.damageableProduct = product
                
                self.selectedProductName = product.productName
            }
            return cell
        }
        
        
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSearchActive{
            if self.filteredList.count > 0 {
                let data = self.filteredList[indexPath.row]
                
                if self.selectedProductId.contains(data.productId){
                    self.selectedProductAlert(userMessage: LanguageManager.TheProductIsAlreadySelectedDeselectTheProductForUpdate)
                }else{
                    self.selectedProduct = data
                    
                    let quantity = data.quantity
                    if quantity.isEmpty == false && quantity != "0.00"{
                        if let serials = data.serials{
                            if serials.isEmpty{
                                self.navigateToDamageableProductBaseInfoVC()
                            }
                            else{
                                self.navigateToDamageableProductWithSerialVC(serials: serials)
                            }
                        }
                    }else{
                        self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
                    }
                    
                }
            }
        }else{
            if self.productList.count > 0 {
                let data = self.productList[indexPath.row]
                
                if self.selectedProductId.contains(data.productId){
                    self.selectedProductAlert(userMessage: LanguageManager.TheProductIsAlreadySelectedDeselectTheProductForUpdate)
                }else{
                    self.selectedProduct = data
                    
                    let quantity = data.quantity
                    if quantity.isEmpty == false && quantity != "0.00"{
                        if let serials = data.serials{
                            if serials.isEmpty{
                                self.navigateToDamageableProductBaseInfoVC()
                            }
                            else{
                                self.navigateToDamageableProductWithSerialVC(serials: serials)
                            }
                        }
                    }else{
                        self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
                    }
                    
                }
            }
        }
        
    }
    
    func navigateToDamageableProductBaseInfoVC(){
        let viewController = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "DamageableProductBaseInfoViewController") as! DamageableProductBaseInfoViewController
        viewController.delegate = self
        viewController.product = self.selectedProduct 
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToDamageableProductWithSerialVC(serials : [ProductForDamageSerials]){
//        let serial = serials.map{$0.serialNo}
//
//        let serials = serial.joined(separator: ",")
//
//        if self.prepareSerialNumbers(serials: serials).count > 0{
//            let serialList = self.prepareSerialNumbers(serials: serials)
//
//
//        }
        
        let viewController = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "DamageableSerialProductBaseInfoViewController") as! DamageableSerialProductBaseInfoViewController
        viewController.serialList = serials
        viewController.delegate = self
        viewController.product = self.selectedProduct
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @objc func onRemove(sender : UIButton){
        let productId = sender.tag
        
        if self.selectedProductId.contains(productId){
            removeDataofProduct(productId: productId)
        }
    }
    
    func removeDataofProduct(productId : Int){
        var productAtIndex = -1
        for index in 0..<self.selectedProductId.count{
            if self.selectedProductId[index] == productId{
                productAtIndex = index
            }
        }
        
        self.selectedBuyingPrices.remove(at: productAtIndex)
        self.selectedQuantities.remove(at: productAtIndex)
        self.selectedSerialsId.remove(at: productAtIndex)
        self.selectedProductId.remove(at: productAtIndex)
        
        if selectedDescription.indices.contains(productAtIndex){
            self.selectedDescription.remove(at: productAtIndex)
        }
        
        self.refreshTableView()
    }
    
//    fileprivate func prepareSerialNumbers(serials : String)->[String]{
//        guard serials != "" else {
//            return []
//        }
//        let serialList = serials.components(separatedBy: ",")
//        
//        guard serialList.count > 0 else {
//            return []
//        }
//        return serialList
//    }
    
    func selectedProductAlert(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension DamageableProductListViewController : DamageableProductViewDelegate{
    func setDamageableProductData(data: ProductForDamageDataMapper) {
        guard let list = data.inventories, list.count > 0 else{
            return
        }
        self.isLoading = false
        self.productList += list
        self.filteredList += list
        
        self.refreshTableView()
        
//        guard let pagination = data.pagination else {
//            return
//        }
//        self.lastPage = pagination.lastPageNo
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(message: String) {
        self.showAlert(title: message, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.presenter.getProductForDamageDataFromServer(getAll: true)
    }
    
}

//Product Delegate
extension DamageableProductListViewController : DamageableProductAddDelegate{
    func setSerialData(serial: String, buyingPrice: Double, description: String) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        self.selectedProductId.append(selectedProduct.productId)
        self.selectedSerialsId.append(serial)
        self.selectedBuyingPrices.append(buyingPrice)
        self.selectedQuantities.append(Double(self.countFrequencyOf(serial: serial)))
        
        if description == "" {
            self.selectedDescription.append("")
        }else{
            self.selectedDescription.append(description)
        }
        
        self.refreshTableView()
    }
    
    func setProductData(amount: Double, buyingPrice: Double, description: String) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        self.selectedProductId.append(selectedProduct.productId)
        self.selectedBuyingPrices.append(buyingPrice)
        self.selectedQuantities.append(amount)
        self.selectedSerialsId.append("")
        
        if description == "" {
            self.selectedDescription.append("")
        }else{
            self.selectedDescription.append(description)
        }
        
        self.refreshTableView()
    }
    
    func convertToString(serials : [String])->String{
        var serialText = ""
        for i in 0..<serials.count{
            serialText += serials[i]
        }
        return serialText
    }
    
    func countFrequencyOf(serial : String)->Int{
        let count = serial.filter { $0 == "," }.count
        if count > 0{
            return count + 1
        }
        return 1
    }
    
}

extension DamageableProductListViewController{
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: userMessage, message: "", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            //self.navigateDamagedProductListViewController()
            self.popToSpecifiedController()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
        func popToSpecifiedController(){
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is DamagedProductListViewController {
                    self.navigationController!.popToViewController(aViewController, animated: true)
                }
            }
        }
    
    func navigateDamagedProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamagedProductListViewController") as! DamagedProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
