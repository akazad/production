//
//  DamagedProductListViewController.swift
//  Ponno
//
//  Created by a k azad on 23/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class DamagedProductListViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = DamagedProductListPresenter(service: ProductService())
    
    private var damagedProductList : [DamagedProductGroupInfo] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var damagesSummary : [Summary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var damagedProductName : String?
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addFloaty()
        self.configureTableView()
        self.setNavigationBarTitle()
        self.configureCollectionView()
        self.setBarBtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.DamagedProducts
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarBtn(){
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let productAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        productAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        productAddBtn.addTarget(self, action: #selector(onProductAdd(sender:)), for: UIControl.Event.touchUpInside)
        productAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        productAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        productAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: productAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2, barButton1], animated: true)
    }
    
    @objc func onSearch(sender: UIButton){
        self.navigateToDamagedProductSearchViewController()
    }
    
    @objc func onProductAdd(sender: UIButton){
        self.navigateToDamageableProductListViewController()
    }
    
    func navigateToDamagedProductSearchViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamagedProductSearchViewController") as! DamagedProductSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToDamageableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamageableProductListViewController") as! DamageableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //FloatingButton
    func addFloaty(){
        
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(DamagedProductListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.damagedProductList = []
        self.currentPage = 1
        self.presenter.getDamagedProductListDataFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }
    

}

extension DamagedProductListViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC(){
        self.navigateToAddNewExpenseViewController()
    }
    
}

//MARK: CollectionView Delegate And Data Source
extension DamagedProductListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(ExpenseSummaryCell.nib, forCellWithReuseIdentifier: ExpenseSummaryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let summaryItem = self.damagesSummary, summaryItem.count > 0 else{
            return 0
        }
        return summaryItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ExpenseSummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: ExpenseSummaryCell.identifier, for: indexPath) as! ExpenseSummaryCell
        guard let expenseSummaryItem = self.damagesSummary, expenseSummaryItem.count > 0 else {
            return cell
        }
        let list = expenseSummaryItem[indexPath.row]
        
        cell.summaryTitle.text = list.title
        cell.summeryValue.text = list.value
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70.0)
    }
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.damagesSummary, list.count > 0 else{
            return
        }
        let listCount = list.count
        
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//Mark TableView Delegate and Data Source
extension DamagedProductListViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DamagedProductListCell.nib, forCellReuseIdentifier: DamagedProductListCell.identifier)
        self.tableView.rowHeight = 80
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.damagedProductList.count > 0 {
            return self.damagedProductList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DamagedProductListCell = tableView.dequeueReusableCell(withIdentifier: DamagedProductListCell.identifier, for: indexPath) as! DamagedProductListCell
        cell.selectionStyle = .none
        if self.damagedProductList.count > 0 {
            let data = self.damagedProductList[indexPath.row]
            
            cell.damagedProduct.text = data.damagedProductName
            cell.numberOfDamages.text = LanguageManager.Damaged + " : " + String(describing: data.damagedLot) + " " + LanguageManager.Times
            cell.totalDamages.text = LanguageManager.TotalDamages + " : " + "\(data.totalQuantity)" + data.unit
            cell.totalLoss.text = LanguageManager.TotalDamageAmount + " : " + String(describing: data.totalLoss) + " " + Constants.currencySymbol
            
            cell.selectBtn.isHidden = true
            
            if isLoading == false && indexPath.row == self.damagedProductList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getDamagedProductListDataFromServer(page: self.currentPage)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.damagedProductList.count > 0 {
            let damagedItem = self.damagedProductList[indexPath.row]
            self.damagedProductName = damagedItem.damagedProductName
            self.navigateToDamagePerProductVC(id: damagedItem.damagedProductId)
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToDamagePerProductVC(id : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamagedPerProductViewController") as! DamagedPerProductViewController
        viewController.damagedProductId = id
        viewController.damagedMaterialName = self.damagedProductName
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Api Delegate
extension DamagedProductListViewController : DamagedProductListViewDelegate{
    
    func setDamagedProductData(data: DamagedProductListDataMapper) {
        guard let damagesSummaryItem = data.summary, damagesSummaryItem.count > 0 else {
            return
        }
        self.damagesSummary = damagesSummaryItem
        self.collectionView.reloadData()
        self.setUpAutoScroll()

        guard let list = data.damagedProductGroupInfo, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.damagedProductList += list


        guard let pagination = data.pagination else{
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }

    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: data)
    }

    func showLoading() {
        self.showLoader()
    }

    func hideLoading() {
        self.hideLoader()
    }

    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.currentPage = 1
        self.damagedProductList = []
        self.presenter.getDamagedProductListDataFromServer(page: self.currentPage)
    }
}
