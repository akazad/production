//
//  ProductSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 13/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class ProductSearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let presenter = ProductSearchListPresenter(service: ProductService())
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))

    var timer : Timer?
    
    var productList : [ProductList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var searchText = ""
    var currentPage : Int = 1
    var lastPage : Int = 0
    
    var isLoading : Bool = false
    
    var productId : Int?
    var hasSerial : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpSearchBar()
        self.configureTableView()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.setNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.presenter.getProductSearchDataFromServer(page: self.currentPage, searchString: "")
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}

//Mark: Search Delegate
extension ProductSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.ProductSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
//    func navigateToProductSearchVC(){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductSearchViewController") as! ProductSearchViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
//        
//    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.productList = []
        self.isLoading = true
        self.isSearchActive = true
        self.presenter.getProductSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
    }
}

//Mark: TableView Delegate And Data Source
extension ProductSearchViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductListCell.nib, forCellReuseIdentifier: ProductListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.productList.count > 0  {
            return self.productList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductListCell = tableView.dequeueReusableCell(withIdentifier: ProductListCell.identifier, for: indexPath) as! ProductListCell
        cell.selectionStyle = .none
        
        if self.productList.count > 0 {
            let product = self.productList[indexPath.row]
            
           cell.product = product
            
            cell.selectBtn.isHidden = true
            if isLoading == false && indexPath.row == self.productList.count - 1 && self.currentPage < self.lastPage{
                self.isLoading = true
                self.currentPage += 1
//                if isSearchActive{
//
//                }
                self.presenter.getProductSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
            }
        }
        return cell
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.productList.count > 0 {
            let list = self.productList[indexPath.row]
            self.productId = list.productId
            //self.navigateToProductViewVC(id: list.inventoryId)
            self.navigateToProductDetailsBaseVC(product : list)
        }
    }
    
    func navigateToProductViewVC(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
        viewController.inventoryId = id
        viewController.productId = self.productId
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToProductDetailsBaseVC(product : ProductList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsBaseViewController") as! ProductDetailsBaseViewController
        viewController.inventoryId = product.inventoryId
        viewController.productId = product.productId
        viewController.productName = product.name
        if product.serials == ""{
            viewController.hasSerial = 0
        }
        else{
            viewController.hasSerial = 1
        }
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

//Mark: Api Delegate
extension ProductSearchViewController : ProductSearchListViewDelegate{ 
    func setProductSearchData(data: ProductListDataMapper) {
        guard let list = data.productList, list.count > 0 else {
            return
        }
        self.productList += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPage = pagination.lastPageNo
    }
    
    func onFailed(failure : String) {
        self.searchAlert(title: LanguageManager.NoInformationFound, message: failure)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getProductSearchDataFromServer(page: self.currentPage, searchString: "")
    }
}
