//
//  ViewController.swift
//  UBottomSheet
//
//  Created by ugur on 13.08.2018.
//  Copyright © 2018 otw. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class ProductDetailsViewController: UIViewController, BottomSheetDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var container: UIView!
    
    let presenter = ProductViewPresenter(service: ProductService())
    
    var product : Product?
    var inventoryId : Int?
    var productId : Int?
    var hasSerial : Int?
    var productName: String?
    
    enum Sections : Int {
        case productInfo
        case ProductSummary
    }
    
    var bottomSheetDelegate: BottomSheetDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        container.layer.cornerRadius = 15
        container.layer.masksToBounds = true
        self.configureTableView()
        self.tableView.isHidden = true
        
        self.setBarButton()
        self.initialSetUp()
//        setUpImageView()
        //let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap(_:)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.attachPresenter()
    }
    
    func initialSetUp(){
        self.navigationController?.navigationBar.topItem?.title = "পণ্যের তথ্য"
    }
    
    func setBarButton(){
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "edit"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onEdit(sender:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        button.widthAnchor.constraint(equalToConstant: 32).isActive = true
        button.heightAnchor.constraint(equalToConstant: 32).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func onEdit(sender: UIBarButtonItem){
        self.navigateToProductUpdateVC()
    }
    
    func navigateToProductUpdateVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductUpdateViewController") as! ProductUpdateViewController
        viewController.productInfo = product 
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToProductListVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? BottomSheetViewController{
            
            viewController.bottomSheetDelegate = self
            //viewController.product = self.product
            viewController.productId = self.productId
            viewController.hasSerial = self.hasSerial
            viewController.productName = self.productName
            viewController.parentView = container
        }
    }
    
//    @objc func handleTap(_ recognizer: UITapGestureRecognizer){
//        let p = recognizer.location(in: self.tableView)
//        let index = tableView.indexPathForRow(at: p)
////        if segmentedControl.selectedSegmentIndex == 1{
////            if self.recentSale.count > 0{
////                guard let salesIndex = index else {
////                    return
////                }
////                let saleItem = self.recentSale[salesIndex.row]
////
////                self.navigateToSaleDetailsVC(iD: saleItem.invoiceId)
////            }
////        }
//
//        tableView.selectRow(at: index, animated: false, scrollPosition: .none)
//    }
    
    func updateBottomSheet(frame: CGRect) {
        container.frame = frame
        //        backView.frame = self.view.frame.offsetBy(dx: 0, dy: 15 + container.frame.minY - self.view.frame.height)
        //        backView.backgroundColor = UIColor.black.withAlphaComponent(1 - (frame.minY)/200)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ProductDetailsViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {

        guard let id = self.inventoryId else{
            return
        }
        self.presenter.getProductViewDataFromServer(id: id)
        
        refreshControl.endRefreshing()
    }
    
}

//MARK: TableView Delegate and DataSource
extension ProductDetailsViewController : UITableViewDataSource, UITableViewDelegate {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductDetailsInfoCell.nib, forCellReuseIdentifier: ProductDetailsInfoCell.identifier)
        self.tableView.register(ProductDetailsSummaryCell.nib, forCellReuseIdentifier: ProductDetailsSummaryCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.white
        self.tableView.separatorStyle = .singleLine
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(self.refreshControl)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.productInfo.rawValue:
            return 1
        case Sections.ProductSummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case Sections.productInfo.rawValue:
            let cell : ProductDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsInfoCell.identifier, for: indexPath) as! ProductDetailsInfoCell
            cell.selectionStyle = .none
            guard let productDetails = self.product else{
                return cell
            }
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + productDetails.image), placeholderImage: UIImage(named: "placeholder"))
            cell.productName.text = productDetails.name.capitalized
            cell.company.text = "কোম্পানিঃ " + productDetails.company
            cell.categoryName.text = "ক্যাটেগরিঃ " + productDetails.categoryName
            cell.variantLabel.text = "ভিন্নতাঃ " + productDetails.variant
            cell.productSku.text = "পণ্যের কোডঃ " + productDetails.sku
            cell.buyingPrice.text = "ক্রয়মূল্যঃ " + "\(productDetails.buyingPrice)" + " " + Constants.currencySymbol 
            cell.SellingPrice.text = "বিক্রয়মূল্যঃ " + "\(productDetails.sellingPrice)" + " " + Constants.currencySymbol
            //cell.cautionLbl.text = "পরিমাণ সতর্কতাঃ " + productDetails.
            cell.deleteBtn.addTarget(self, action: #selector(onDelete(sender:)), for: .touchUpInside)
            //cell.deleteBtn.addTapGestureRecognizer(action: {self.onTapOnImageView()})
            //cell.productName.addTapGestureRecognizer(action: {self.onTapOnImageView()})
            cell.editBtn.addTarget(self, action: #selector(onEdit(sender:)), for: .touchUpInside)
            self.hasSerial = productDetails.hasSerial
            //self.productName = productDetails.name
            
            return cell
        case Sections.ProductSummary.rawValue:
            let cell : ProductDetailsSummaryCell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsSummaryCell.identifier, for: indexPath) as! ProductDetailsSummaryCell
            cell.selectionStyle = .none
            guard let productDetails = self.product else{
                return cell
            }
            cell.currentStockLabel.text = "\(productDetails.quantity)" + " " + Constants.pcsSymbol
            cell.sellAbleStockLabel.text = "\(productDetails.sellableStockPrice)" + " " + Constants.currencySymbol
            cell.totalStockPrice.text = "\(productDetails.totalStockPrice)" + " " + Constants.currencySymbol
            cell.totalSaleLabel.text = "\(productDetails.totalSell)" + " " + Constants.currencySymbol
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.productInfo.rawValue:
            return UITableView.automaticDimension
        case Sections.ProductSummary.rawValue:
            return 145.0
        default:
            return 0
        }
    }
    
    @objc func onDelete(sender: UIButton){
        guard let productId = self.productId else {
            return
        }
        self.confirmationMessage(userMessage: "Hola", deleteId: productId)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: "Delete", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
                self.presenter.deleteProductDataFromServer(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            
    }
    
}

//Mark: Api Delegate
extension ProductDetailsViewController : ProductViewDelegate{
    func deleteProductData(data: AddDataMapper) {
        guard let message = data.message else { 
            return
        }
        self.showAlert(title: message, message: "")
//        self.popToSpecificViewController()
        self.navigateToProductListVC()
    }
    
    
    func setProductViewData(data: ProductDetailsDataMapper) {
        guard let item = data.product else {
            return
        }
        self.product = item
        self.tableView.isHidden = false
        self.refreshTableView()
    }
    
    func setFreemiumProductData(data: OfflineProductDataMapper) {
        //
    }
    
    func onProductUpdate(data: AddDataMapper) {
        //
    }
    
    func setProductRecentSaleHistory(data: ProductRecentSaleDataMapper) {
        //ThisOneIsEmpty
    }
    
    func setProductRecentPurchaseHistory(data: ProductRecentPurchaseDataMapper) {
        //ThisOneIsEmpty
    }
    
    func setProductSerialNumber(data: ProductSerialDataMapper) {
        //ThisOneIsEmpty
    }
    
    func deleteRecentStockData(data: AddDataMapper) {
       //ThisOneIsEmpty
    }
    
    func serialUpdateData(data: AddDataMapper) {
        //ThisOneIsEmpty
    }
    
    func onFailed(data: String) {
        self.showAlert(title: "কোন তথ্য পাওয়া যায়নি", message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.inventoryId else{
            return
        }
        self.presenter.getProductViewDataFromServer(id: id)  
    }
}

