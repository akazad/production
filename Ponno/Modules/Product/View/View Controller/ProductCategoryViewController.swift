//
//  ProductCategoryViewController.swift
//  Ponno
//
//  Created by a k azad on 17/7/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductCategoryViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = ProductCategoryPresenter(service: ProductService())
    
    var productCategories : [ProductCategories] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var list : [ProductCategories] = []
    
    var categoryName : String?
    var categoryParentId: Int = 0
    var categoryId : Int?
    //var gen : Int = 1
    var parentId : Int?
    var currentPage : Int = 1
    var categoryNameString : String = ""
    
    var nameString : [Int : String] = [:]
    var productCategoryInfo : ProductCategoryInfo?
    //var id : Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.configureTableView()
        
        self.navigationBarSetup()
        self.title = LanguageManager.SelectCategory
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        //self.attachPresenter()
        self.setUpInitialLanguage()
        self.configureTableView()
        self.categoryNameString = ""
        self.attachPresenter()
    }
    
    func navigationBarSetup(){
        let productAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        productAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        productAddBtn.addTarget(self, action: #selector(onCategoryAdd(sender:)), for: UIControl.Event.touchUpInside)
        productAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        productAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        productAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: productAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2], animated: true)
    }
    
    @objc func onCategoryAdd(sender: UIBarButtonItem){
        self.categoryParentId = 0
        alertWithTextField()
    }
    
    
    
}

extension ProductCategoryViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductCategoryCell.nib, forCellReuseIdentifier: ProductCategoryCell.identifier)
        self.tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductCategoryCell = tableView.dequeueReusableCell(withIdentifier: ProductCategoryCell.identifier, for: indexPath) as! ProductCategoryCell
        
        cell.selectionStyle = .none
        
        let item = self.productCategories[indexPath.row]
        
        cell.categoryItemLbl.text = item.name
        cell.subCategoryAddBtn.tag = item.id
        cell.subCategoryAddBtn.addTarget(self, action: #selector(onSubcategoryAdd), for: .touchUpInside)
        
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard self.productCategories.count > 0 else {
            return
        }
        let item = self.productCategories[indexPath.row]
        let id = item.id
        self.categoryId = id
        self.parentId = item.parent
        
        
        self.categoryNameString += item.name
        
        let gen = item.gen
        let name = item.name
        self.nameString [gen] = name
        
        self.didSelect(categories: self.list, categoryId: id)
        
    }
    
    func didSelect(categories: [ProductCategories], categoryId: Int){
        
        var flag = 0
        
        for item in categories{
            if item.parent == categoryId{
                flag = 1
                break
            }
        }
        
        if flag == 0{
            if let id = self.categoryId{
//                self.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
                productCategoryInfo?.setCategoryInfo(name: self.nameString, id: id)
            }
            
            //self.navigateToAddNewProductViewController()
            
        }
        else {
            self.navigateToProductSubCategoryVC(id : categoryId)
        }
    }
    
    @objc func onSubcategoryAdd(sender: UIButton){
        let id = sender.tag
        for item in self.productCategories {
            if id == item.id {
                self.categoryParentId = id
                self.nameString [item.gen] = item.name
            }
        }
        self.alertWithTextField()
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        guard let row = tableView.indexPathForSelectedRow?.row else {
//            return
//        }
//        //let selectedContact = contacts[row]
//        if let viewController = segue.destination as? AddNewProductViewController {
//            //viewController.contact = selectedContact
//        }
//    }
    
    func navigateToProductSubCategoryVC(id : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductSubCategoryVC") as! ProductSubCategoryVC
        viewController.productCategories = self.list
        viewController.id = id
        viewController.parentId = self.parentId
        viewController.nameString = self.nameString
        viewController.productCategoryInfo = self.productCategoryInfo
        //viewController.modalPresentationStyle = .pageSheet
        //viewController.productCategoryInfo = self as? AddNewProductViewController
        //viewController.categoryNameString = self.categoryNameString
        self.navigationController?.pushViewController(viewController, animated: true)
//        let navController = UINavigationController(rootViewController: viewController)
//        self.present(navController, animated: false, completion: nil)
//        self.present(viewController, animated: true, completion: nil)
        
    }
    
    func navigateToAddNewProductViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewProductViewController") as! AddNewProductViewController
        //viewController.categoryName = self.categoryNameString
        viewController.nameString = self.nameString
        guard let categoryId = self.categoryId else{
            return
        }
        viewController.productCategoryId = categoryId
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}


extension ProductCategoryViewController : ProductCategoryViewDelegate{
    func setProductCategoriesList(productCategoryData: ProductCategoryListDataMapper) {
        guard let categories = productCategoryData.categories, categories.count > 0 else{
            return
        }
        
        self.list = categories
        self.productCategories = categories
        self.productCategories = self.productCategories.filter({$0.parent == 0})
        
    }
    
    func setCategoryAddData(data: CategoryAddDataMapper) {
        guard let categoryData = data.category else {
            return
        }
        self.categoryParentId = categoryData.parent
        self.categoryId = categoryData.id
        //self.categoryNameString += "/ " + categoryData.name
        self.nameString [categoryData.gen] = categoryData.name
        
       let newCategory = ProductCategories.init(id: categoryData.id, name: categoryData.name, gen: categoryData.gen, parent: categoryData.parent)
        
        self.list.append(newCategory)
        self.refreshTableView()
        
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
        
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getProductCategoryListFromServer(page: self.currentPage)
    }
}

extension ProductCategoryViewController{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewCategoryAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.CategoryName
            textField.textAlignment = .center
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showMessage(userMessage: LanguageManager.CategoryIsRequired)
                return
            }
            
            self.categoryName = textField.text?.lowercased()
            
            for category in self.productCategories{
                let existedCategory = category.name.lowercased()
                if self.categoryName == existedCategory{
                    self.showAlert(title: LanguageManager.CategoryAlreadyExists, message: "")
                    return
                }
            }
            
            let param : [String : Any] = ["name": textField.text!, "parent": self.categoryParentId]
            
            self.presenter.postProductCategoryAddDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    

    func showMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)

        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.alertWithTextField()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            if let id = self.categoryId{
                //self.navigationController?.popViewController(animated: true)
                self.productCategoryInfo?.setCategoryInfo(name: self.nameString, id: id)
            }
            self.popToSpecificViewController()
            //self.navigateToAddNewProductViewController()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is AddNewProductViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
}
