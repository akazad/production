//
//  ProductAddThirdVC.swift
//  Ponno
//
//  Created by a k azad on 16/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductAddThirdVC: UIViewController {

    @IBOutlet weak var serialNumberLbl: UILabel!{
        didSet{
            serialNumberLbl.text = LanguageManager.SerialNumber
        }
    }
    @IBOutlet weak var serialNumberTextField: UITextField!{
        didSet{
            serialNumberTextField.placeholder = LanguageManager.UseCommaToSeparateSerial
        }
    }
    @IBOutlet weak var scanBtn: UIButton!
    @IBOutlet weak var scanBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var serailNoHeight: NSLayoutConstraint!
    
    @IBOutlet weak var expireDateLbl: UILabel!{
        didSet{
            expireDateLbl.text = LanguageManager.ExpireDate
        }
    }
    @IBOutlet weak var expireDateHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var expireDateTextField: UITextField!
    @IBOutlet weak var expireDateTextFieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var warrantyLbl: UILabel!{
        didSet{
            warrantyLbl.text = LanguageManager.Warranty
        }
    }
    @IBOutlet weak var warrantyHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var warrantyTextField: UITextField!{
        didSet{
            warrantyTextField.placeholder = LanguageManager.Warranty
        }
    }
    @IBOutlet weak var warrantyTextFieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var monthLbl: UILabel!{
        didSet{
            monthLbl.text = LanguageManager.Month
        }
    }
    @IBOutlet weak var monthHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var quantityAlertLbl: UILabel!{
        didSet{
            quantityAlertLbl.text = LanguageManager.QuantityAlertWhenSmallerThan
        }
    }
    @IBOutlet weak var quantityAlertTextField: UITextField!
    @IBOutlet weak var quantityAlertTextFieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var quantityAlertHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var unitLbl: UILabel!{
        didSet{
            unitLbl.text = LanguageManager.Unit
        }
    }
    @IBOutlet weak var unitHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var amountLbl: UILabel!{
        didSet{
            amountLbl.text = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var buyingPriceLbl: UILabel!{
        didSet{
            buyingPriceLbl.text = LanguageManager.BuyingPrice
        }
    }
    @IBOutlet weak var buyingPriceTextField: UITextField!
    @IBOutlet weak var sellingPriceLbl: UILabel!{
        didSet{
            sellingPriceLbl.text = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var sellingPriceTextField: UITextField!
    @IBOutlet weak var vendorLbl: UILabel!{
        didSet{
            vendorLbl.text = LanguageManager.Vendor
        }
    }
    @IBOutlet weak var dealerOrCompanyTextField: UITextField!{
        didSet{
            dealerOrCompanyTextField.placeholder = LanguageManager.SelectOne
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var toggleBtn: UISwitch!
    @IBOutlet weak var newVendorAddBtn: UIButton!{
        didSet{
            newVendorAddBtn.setTitle(LanguageManager.NewVendorAdd, for: .normal)
        }
    }
    @IBOutlet weak var clearTextBtn: UIButton!
    
    private var presenter = ProductAddThirdPresenter(service: ProductService())
    
    var vendorId : Int?
    var vendorsList : [VendorsList] = []
    var isLoading : Bool = false
    var currentPage : Int = 1
    //var serails :  [String]?
    var serials : String = ""
    
    var checked : Bool = false
    //var statusCheck : Bool = true
    
    var message : String?
    
    var vendorListPicker = UIPickerView()
    var datePicker = UIDatePicker()
    
    //CodeScan
    var serialText: String?
    var scanningCode : String?{
        didSet{
            if let code = scanningCode{
                var serials : String = ""
                if var previousValue = serialText{
                    previousValue.append(contentsOf: "," + code)
                    serials = previousValue
                }else{
                    serials.append(code)
                }
                serialNumberTextField.text = serials
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scanBtn.isHidden = true
        self.setUpInitialLanguage()
        self.attachPresenter()
        self.setUpPickerView()
        self.toolBarSetUp()
        //self.setScannerBtn()
        self.configureTextField()
        self.initialSetup()
    }
    
    func initialSetup(){
        self.toggleBtn.setOn(false, animated: true)
        self.serailNoHeight.constant = 0.01
        self.scanBtnHeight.constant = 0.01
        self.warrantyHeightConstraint.constant = 0.01
        self.warrantyTextFieldHeightConstraint.constant = 0.01
        self.monthHeightConstraint.constant = 0.01
        ProductObject.serialNumberStatus = "off"
        self.serials = ""
        ProductObject.serial = ""
        self.amountTextField.isEnabled = true
        ProductObject.warranty = ""
        ProductObject.expireDate = ""
        ProductObject.buyingPrice = nil
        ProductObject.sellingPrice = nil
//        self.buyingPriceTextField.text = ""
//        self.sellingPriceTextField.text = ""
        self.amountTextField.text = "\(0)"
        self.quantityAlertTextField.text = "\(0)"
        ProductObject.vendorId = nil
        self.showStartDatePicker()
        self.clearTextBtn.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpViews()
        
    }
    
    @IBAction func toggleBtnAction(_ sender: UISwitch) {
        if sender.isOn{
            self.serailNoHeight.constant = 32
            self.scanBtnHeight.constant = 32
            self.warrantyHeightConstraint.constant = 32
            self.warrantyTextFieldHeightConstraint.constant = 40
            self.monthHeightConstraint.constant = 32
            self.expireDateHeightConstraint.constant = 0.01
            self.expireDateTextFieldHeightConstraint.constant = 0.01
            //self.unitHeightConstraint.constant = 40
            self.amountTextField.isEnabled = false
            ProductObject.serialNumberStatus = "on"
            ProductObject.expireDate = ""
        }
        else{
            self.serailNoHeight.constant = 0.01
            self.scanBtnHeight.constant = 0.01
            self.warrantyHeightConstraint.constant = 0.01
            self.warrantyTextFieldHeightConstraint.constant = 0.01
            self.monthHeightConstraint.constant = 0.01
            self.expireDateHeightConstraint.constant = 32
            self.expireDateTextFieldHeightConstraint.constant = 40
            self.amountTextField.isEnabled = true
            //self.serialNumber.text = ""
            self.serials = ""
            //self.amount.text = ""
            ProductObject.serial = ""
            ProductObject.serialNumberStatus = "off"
            ProductObject.warranty = ""
        }
    }
    
//    @IBAction func switchBtn(_ sender: UIButton) {
//
//        if (!checked){
////            let btnImage = UIImage(named: "switch")
////            submitBtn.setImage(btnImage , for: .normal)
////            submitBtn.backgroundColor = UIColor.lightGreen
//
//        }else{
//            self.serailNoHeight.constant = 0
////            let btnImage = UIImage(named: "switch")
////            submitBtn.setImage(btnImage , for: .normal)
//            submitBtn.backgroundColor = UIColor.white
//        }
//    }
    
//    func setScannerBtn(){
//        let button = UIButton(type: .custom)
//        if toggleBtn.isOn{
//            //let button = UIButton(type: .custom)
//            button.setImage(UIImage(named: "scan"), for: .normal)
//            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
//            button.frame = CGRect(x: CGFloat(serialNumber.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
//            button.addTarget(self, action: #selector(self.productScan), for: .touchUpInside)
//            serialNumber.rightView = button
//            serialNumber.rightViewMode = .always
//        }else{
//            button.isHidden = true
//        }
//    }
    
    func setUpViews(){
        //self.serialNumber.delegate = self
        self.scanBtn.addTarget(self, action: #selector(onProductScan), for: .touchUpInside)
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtn(sender:)), for: .touchUpInside)
        self.navigationController?.navigationBar.topItem?.title = " "
        self.serialNumberTextField.addTarget(self, action: #selector(onSerialNumberEditing), for: .editingChanged)
        self.newVendorAddBtn.addTarget(self, action: #selector(onVendorAddBtnPressed), for: .touchUpInside)
        self.expireDateTextField.addTarget(self, action: #selector(onExpireDateChange), for: .editingDidEnd)
        self.warrantyTextField.addTarget(self, action: #selector(onWarrantyChange), for: .editingDidEnd)
        self.dealerOrCompanyTextField.addTarget(self, action: #selector(onVendorTextFieldDIdChange), for: .editingDidEnd)
        self.clearTextBtn.addTarget(self, action: #selector(onClearTextBtnTapped), for: .touchUpInside)
    }
    
    @objc func onVendorAddBtnPressed(sender: UIButton){
        self.alertWithTextField()
    }
    
    @objc func onExpireDateChange(sender: UITextField){
        ProductObject.expireDate = self.expireDateTextField.text
    }
    
    @objc func onWarrantyChange(sender: UITextField){
        
        ProductObject.warranty = self.warrantyTextField.text
    }
    
//    @objc func productScan(sender: UIButton){
//        print("Hola Scan")
//    }
    
    @objc func onProductScan(sender: UIButton){
        self.serialText = serialNumberTextField.text
        self.navigateToScannerVC()
    }
    
    @objc func onClearTextBtnTapped(){
        self.dealerOrCompanyTextField.text = ""
        ProductObject.vendorId = nil
        self.clearTextBtn.isHidden = true
    }
    
    @objc func onVendorTextFieldDIdChange(textField: UITextField){
        if textField.text?.count ?? 0 > 1{
            self.clearTextBtn.isHidden = false
        }else{
            self.clearTextBtn.isHidden = true
        }
    }
    
    func navigateToScannerVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ScannerViewController") as! ScannerViewController
        viewController.navigateTo = Navigate.productSerial
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func onSubmitBtn(sender : UIButton){
        
//        guard let serials = self.serails, serials.count > 0 else{
//            return
//        }
//        let stringRepresentation = serials.joined(separator: ",")
//        self.quantitySerialTextField.text = stringRepresentation
        
        guard let amount = self.amountTextField.text, let buyingPrice =  self.buyingPriceTextField.text, let sellingPrice = self.sellingPriceTextField.text else{
            return
        }
        let quantityAlert = self.quantityAlertTextField.text ?? ""
        
//        print(amount)
//        print(buyingPrice)
//        print(sellingPrice)
        ProductObject.stockAlert = quantityAlert
        ProductObject.serial = self.serials
        ProductObject.quantity = amount
        ProductObject.buyingPrice = buyingPrice
        ProductObject.sellingPrice = sellingPrice
        ProductObject.vendorId = self.vendorId
//        ProductObject.productCategoryId = self.productCa
        
        if self.isValidated(){
            guard let name = ProductObject.productName, let category = ProductObject.productCategoryId, let companyName = ProductObject.companyName, let varient = ProductObject.varient, let unit = ProductObject.unitId, let sku = ProductObject.sku, let status = ProductObject.serialNumberStatus,  let buyingPricee = ProductObject.buyingPrice, let sellingPricee = ProductObject.sellingPrice, let quantity = ProductObject.quantity else{
                return
            }
            let vendorId = ProductObject.vendorId ?? nil
            guard let warranty = ProductObject.warranty, let expireDate = ProductObject.expireDate, let stockAlert = ProductObject.stockAlert else{
                return
            }
            guard let serials = ProductObject.serial else{
                return
            }
            
            let params : [String : Any] = ["name" :name, "category": category,
                "company": companyName,
                "variant": varient,
                "unit": unit,
                "sku": sku,
                "serial_no_status": status,
                "serials" : serials,
                "buying_price": buyingPricee,
                "selling_price": sellingPricee,
                "vendor_id": vendorId, 
                "quantity": quantity,
                "stock_alert": stockAlert,
                "warranty": warranty,
                "expire_date": expireDate]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            self.presenter.postNewProductDataToServer(productData: params)
        }
        
    }
    
    func isValidated()->Bool{
//        if ProductObject.serialNumberStatus == "on"{
//            if self.serialNumber.text == ""{
//                showAlert(title: "Serials can't be empty", message: "")
//                return false
//            }
//        }
        
        if let quantityText = self.amountTextField.text, let quantity = Double(quantityText), quantity > 0 {
            if self.buyingPriceTextField.text == ""{
                showAlert(title: LanguageManager.BuyingPriceIsRequired, message: "")
                return false
            }
            else if self.sellingPriceTextField.text == ""{
                showAlert(title: LanguageManager.SellingPriceIsRequired, message: "")
                return false
            }
        }
        
//        if self.amountTextField.text == ""{
//            showAlert(title: "Amount can't be empty", message: "")
//            return false
//        }
        
        
        return true
    }
    
    @objc func onSerialNumberEditing(sender: UITextField){
        guard var serial = self.serialNumberTextField.text else{
            return
        }
        //print(serial)
        
        self.serials = serial
        ProductObject.serial = self.serials
        
        if serial.last == ","{
            serial.remove(at: serial.index(before: serial.endIndex))
        }
        //print(serial)
        
        var serialList = self.prepareSerialNumbers(serials: serial)
        let last = serialList.last
        //print (serialList)
        if last == ","{
            serialList.removeLast()
        }
        let count = serialList.count
        //self.serialNumber.resignFirstResponder()
        self.amountTextField.text = "\(count)"
        //self.buyingPrice.becomeFirstResponder()
    }
    
    
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigateToProductVC()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
    }
    
    func navigateToProductVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    //PickerView
    func setUpPickerView(){
        vendorListPicker.delegate = self
        vendorListPicker.selectedRow(inComponent: 0)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        self.dealerOrCompanyTextField.inputView = vendorListPicker
        self.dealerOrCompanyTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.dealerOrCompanyTextField.resignFirstResponder()
        self.submitBtn.becomeFirstResponder()
    }
    
    func toolBarSetUp(){
        //amount toolBar
        let amountToolBar = UIToolbar()
        amountToolBar.barStyle = UIBarStyle.default
        amountToolBar.isTranslucent = true
        amountToolBar.tintColor = UIColor.black
        amountToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let amountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnAmount(sender:)))
        
        amountToolBar.setItems([cancelButton,spaceButton, amountDoneButton], animated: false)
        amountToolBar.isUserInteractionEnabled = true
        
        self.amountTextField.inputAccessoryView = amountToolBar
        
        //buyingPrice toolBar
        let buyingPriceToolBar = UIToolbar()
        buyingPriceToolBar.barStyle = UIBarStyle.default
        buyingPriceToolBar.isTranslucent = true
        buyingPriceToolBar.tintColor = UIColor.black
        buyingPriceToolBar.sizeToFit()
        
        let buyingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnBuyingPrice(sender:)))
        
        buyingPriceToolBar.setItems([cancelButton,spaceButton, buyingPriceDoneButton], animated: false)
        buyingPriceToolBar.isUserInteractionEnabled = true
        
        self.buyingPriceTextField.inputAccessoryView = buyingPriceToolBar
        
        //SellingPrice toolBar
        let sellingPriceToolBar = UIToolbar()
        sellingPriceToolBar.barStyle = UIBarStyle.default
        sellingPriceToolBar.isTranslucent = true
        sellingPriceToolBar.tintColor = UIColor.black
        sellingPriceToolBar.sizeToFit()
        
        let sellingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnSellingPrice(sender:)))
        
        sellingPriceToolBar.setItems([cancelButton,spaceButton, sellingPriceDoneButton], animated: false)
        sellingPriceToolBar.isUserInteractionEnabled = true
        
        self.sellingPriceTextField.inputAccessoryView = sellingPriceToolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnAmount(sender: UIBarButtonItem){
        //self.amount.resignFirstResponder()
        //self.view.endEditing(true)
        self.buyingPriceTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnBuyingPrice(sender: UIBarButtonItem){
        //self.buyingPrice.resignFirstResponder()
        //self.view.endEditing(true)
        self.sellingPriceTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnSellingPrice(sender: UIBarButtonItem){
        //self.sellingPrice.resignFirstResponder()
        //self.view.endEditing(true)
        self.dealerOrCompanyTextField.becomeFirstResponder()
    }

}

//Mark: PickerViewDelegate
extension ProductAddThirdVC : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        guard self.vendorsList.count > 0 else{
            return 0
        }
        return vendorsList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        if self.vendorsList.count > 0 {
            let list = self.vendorsList[row]
            self.dealerOrCompanyTextField.text = list.name
            self.vendorId = list.id
            return list.name
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if self.vendorsList.count > 0 {
            let list = self.vendorsList[row]
            self.dealerOrCompanyTextField.text = list.name
            self.vendorId = list.id

        }
        
    }
    
}

//Mark: TextField Delegate
extension ProductAddThirdVC : UITextFieldDelegate{
    func configureTextField(){
        self.serialNumberTextField.delegate = self
        self.amountTextField.delegate = self
        self.buyingPriceTextField.delegate = self
        self.warrantyTextField.delegate = self
        self.quantityAlertTextField.delegate = self
        self.expireDateTextField.delegate = self
        //self.serialNumber.underlined()
        self.amountTextField.underlined()
        self.buyingPriceTextField.underlined()
        self.sellingPriceTextField.underlined()
        //self.serialNumber.underlined()
        self.warrantyTextField.underlined()
        self.quantityAlertTextField.underlined()
        self.expireDateTextField.underlined()
        self.dealerOrCompanyTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.serialNumberTextField{
            self.warrantyTextField.becomeFirstResponder()
        }else if textField == self.warrantyTextField{
            self.quantityAlertTextField.becomeFirstResponder()
        }else if textField == self.quantityAlertTextField && ProductObject.serialNumberStatus == "on" {
            self.buyingPriceTextField.becomeFirstResponder()
        }else if textField == self.quantityAlertTextField && ProductObject.serialNumberStatus == "off" {
            self.amountTextField.becomeFirstResponder()
        }else if textField == self.amountTextField{
            self.buyingPriceTextField.becomeFirstResponder()
        }else if textField == self.buyingPriceTextField{
            self.sellingPriceTextField.becomeFirstResponder()
        }else if textField == self.sellingPriceTextField{
            self.dealerOrCompanyTextField.becomeFirstResponder()
        }
        return false
    }
}

//Mark: Api Delegate
extension ProductAddThirdVC : ProductAddThirdViewDelegate{
    
    func postNewVendorData(data: VendorAddDataMapper) {
        guard let vendor = data.vendor else {
            return
        }
        self.vendorId = vendor.id
        self.dealerOrCompanyTextField.text = vendor.name
        self.dealerOrCompanyTextField.resignFirstResponder()
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func getNewProductData(newProductData: ProductAddDataMapper) {
        guard let message = newProductData.message, let product = newProductData.product else {
            return
        }
        self.message = message
        self.successMessage(userMessage: message)
        
        if let id = product.id{ 
            guard let image = ProductObject.productImage else{
                self.successMessage(userMessage: message)
                return
            }
            
            self.presenter.uploadImage(productId: id, image: image)
        }
    }
    
    func onImageUpload(data: AddDataMapper) {
        self.successMessage(userMessage: self.message ?? "")
    }
    
    
    func setVendorListData(data: VendorsDataMapper) {
        guard let list = data.vendors, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.vendorsList += list
    }
    
    func onFailed(data: String) {
        //self.showAlert(title: "Something wrong", message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.presenter.getVendorsListFromServer(page: self.currentPage)
    }
}

extension ProductAddThirdVC{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewVendorAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.VendorName
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.PhoneNumber
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                //self.showAlert(title: "Name Can't be empty", message: "")//
                self.showMessage(userMessage: LanguageManager.VendorNameIsRequired)
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                //self.showAlert(title: "Phone can't be empty", message: "")
                self.showMessage(userMessage: LanguageManager.VendorMobileNumberIsRequired)
                return
            }
            
//            if(!self.validatePhoneNumber(value: phone)){
//                self.showMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
//            }
            
            let param : [String : Any] = ["name": textField.text!, "phone": textField2.text!, "address": ""]
            
            self.presenter.postNewVendorDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.alertWithTextField()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

extension ProductAddThirdVC {
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        expireDateTextField.inputAccessoryView = toolbar
        expireDateTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        expireDateTextField.text = formatter.string(from: datePicker.date)
        self.quantityAlertTextField.becomeFirstResponder()
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
