//
//  DamagedPerProductSerialUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 25/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DamagedPerProductSerialUpdateViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var serialLbl: UILabel!{
        didSet{
            serialLbl.text = LanguageManager.SerialNo
        }
    }
    @IBOutlet weak var descriptionLbl: UILabel!{
        didSet{
            descriptionLbl.text = LanguageManager.Description
        }
    }
    @IBOutlet weak var descriptionTextField: UITextField!{
        didSet{
            descriptionTextField.placeholder = LanguageManager.Description
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var tableViewHeightConstrant: NSLayoutConstraint!
    
    fileprivate var presenter = DamagedPerProductUpdatePresenter(service: ProductService())
    
    var damagedMaterialId : Int?
    var quantity : Double?
    var damagedProduct : DamagedPerProductList?
    var damagedMaterialName : String?
    
    var serialList : [ProductsSerials]?
    var selectedSerial : [String] = []
    var damagedSerial : [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetup()
        self.configureTableView()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.title = self.damagedMaterialName
        
        
        self.descriptionTextField.underlined()
        
        if let damageProduct = self.damagedProduct{
            self.descriptionTextField.text = damageProduct.description
        }
        
        //self.selectedSerial = ["\(String(describing: damagedMaterialId))"]
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
    }
    
    func isValidated()->Bool{
        if self.selectedSerial == [""] {
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.SerialNumberIsRequired)
            return false
        }
        return true
    }
    
    func countFrequencyOf(serial : String)->Int{
        let count = serial.filter { $0 == "," }.count
        if count > 0{
            return count + 1
        }
        return 1
    }
    
    func convertToString(serials : [String])->String{
        var serialText = ""
        for i in 0..<serials.count{
            serialText += serials[i]
        }
        return serialText
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            if let id = self.damagedMaterialId{
                let description = self.descriptionTextField.text ?? ""
                
                
                
                //let quantity = self.convertToString(serials: self.selectedSerial)
                
                let quantity = self.selectedSerial.map{String($0)}.joined(separator: ",")
                let quantityAmount = self.countFrequencyOf(serial: quantity)
                
                let param : [String : Any] = ["id": id, "serial": self.selectedSerial, "quantity": quantityAmount ,"description": description]
             self.presenter.postDamagedPerProductUpdateDataToServer(param: param)
            }
        }
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }

}

//Mark: TableView Delegate DataSource
extension DamagedPerProductSerialUpdateViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductSerialCell.nib, forCellReuseIdentifier: ProductSerialCell.identifier)
        self.tableView.estimatedRowHeight = 30
        self.tableView.separatorStyle = .none
        //self.refreshTableView()
        self.tableView.allowsMultipleSelection = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let list = self.serialList, list.count > 0 else{
            return 0
        }
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductSerialCell = tableView.dequeueReusableCell(withIdentifier: ProductSerialCell.identifier, for: indexPath) as! ProductSerialCell
        guard let list = self.serialList, list.count > 0 else{
            return cell
        }
        let serial = list[indexPath.row]
        cell.serialNumber.text = serial.serialNo
        
        let serialId = serial.id
        
        if self.selectedSerial.contains("\(serialId)"){
            cell.checkIcon.image = UIImage(named : "check_icon")
        }
        else{
            cell.checkIcon.image = UIImage(named: "uncheck_icon")
        }
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let list = self.serialList, list.count > 0 else{
            return
        }
        let serial = list[indexPath.row]
        let serialId = serial.id
        if !self.selectedSerial.contains("\(serialId)"){
            self.selectedSerial.append("\(serialId)")
        }
        else {
            self.selectedSerial = self.selectedSerial.filter({ $0 != "\(serialId)"})
        }
        self.refreshTableView()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let list = self.serialList, list.count > 0 else{
            return
        }
        let serial = list[indexPath.row]
        let serialId = serial.id
        if self.selectedSerial.contains("\(serialId)"){
            self.selectedSerial.append("\(serialId)")
        }
        self.refreshTableView()
    }
    
    func refreshTableView(){
        self.tableView.reloadData {
            if self.tableView.contentSize.height > 200{
                self.tableViewHeightConstrant.constant = 200
            }
            else {
                self.tableViewHeightConstrant.constant = self.tableView.contentSize.height
            }
        }
    }
    
}

extension DamagedPerProductSerialUpdateViewController : DamagedPerProductUpdateViewDelegate{
    func updatePerProductData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func setProductSerialData(data: DamagedPerProductSerialDataMapper) {
        guard let serial = data.productsSerials else{
            return
        }
        self.serialList = serial
        
        var serialArray : [String] = []
        for item in serial{
            serialArray.append(item.serialNo)
        }
        if let damageSerial = self.damagedSerial{
            let damagedSerialNo = serialArray.filter{ damageSerial.contains($0)}
            
            for damageitem in damagedSerialNo{
                for item in serial{
                    if item.serialNo == damageitem{
                        self.selectedSerial.append("\(item.id)")
                    }
                }
            }
            
        }
        
//        if let list = self.serialList{
//            for item in list{
//                if self.damagedSerial == item.serialNo{
//                    self.selectedSerial = ["\(item.id)"]
//                }
//            }
//        }
        
        self.refreshTableView()
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        if let id = self.damagedMaterialId{
            self.presenter.getDamagedPerProductSerialDataFromServer(id: id)
        }
    }
    
}
