//
//  ProductDetailsBaseViewController.swift
//  Ponno
//
//  Created by a k azad on 29/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import FloatingPanel
import SDWebImage

enum Sections : Int {
    case productInfo
    case ProductSummary
}

class ProductDetailsBaseViewController: UIViewController, FloatingPanelControllerDelegate{

    @IBOutlet weak var tableView: UITableView!
    
    var fpc: FloatingPanelController!
    var bottomShetVC : ProductDViewController!
    
    let presenter = ProductViewPresenter(service: ProductService())
    
    var product : Product?
    var inventoryId : Int?
    var productId : Int?
    var hasSerial : Int?
    var productName: String?
    var vendor: Vendors?
    var deletable: Bool?
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addBottomSheet()
        self.setUpInitialLanguage()
        self.configureTableView()
        self.initialSetup()
        self.attachPresenter()
    }
    
    func addBottomSheet(){
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = UIColor.lightGreen.withAlphaComponent(0.5)
        fpc.surfaceView.layer.cornerRadius = 9.0
        fpc.surfaceView.clipsToBounds = true
        
        fpc.surfaceView.shadowHidden = false
        bottomShetVC = storyboard?.instantiateViewController(withIdentifier: "ProductDViewController") as? ProductDViewController
        //bottomShetVC.deliverySystemId = self.deliverySystem
        
        bottomShetVC.productId = self.productId
        bottomShetVC.hasSerial = self.hasSerial
        bottomShetVC.productName = self.productName
        
        fpc.set(contentViewController: bottomShetVC)
        fpc.track(scrollView: bottomShetVC.tableView)
        fpc.addPanel(toParent: self)
    }
    
    func removeBottomSheet(){
        fpc.removePanelFromParent(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeBottomSheet()
    }
    
    func initialSetup(){
        self.title = LanguageManager.ProductDetails
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ProductDetailsViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        guard let id = self.inventoryId else{
            return
        }
        self.presenter.getProductViewDataFromServer(id: id)
        
        refreshControl.endRefreshing()
    }
    
}

//MARK: TableView Delegate and DataSource
extension ProductDetailsBaseViewController : UITableViewDataSource, UITableViewDelegate {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductDetailsInfoCell.nib, forCellReuseIdentifier: ProductDetailsInfoCell.identifier)
        self.tableView.register(ProductDetailsSummaryCell.nib, forCellReuseIdentifier: ProductDetailsSummaryCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.white
        self.tableView.separatorStyle = .singleLine
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(self.refreshControl)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.productInfo.rawValue:
            return 1
        case Sections.ProductSummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case Sections.productInfo.rawValue:
            let cell : ProductDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsInfoCell.identifier, for: indexPath) as! ProductDetailsInfoCell
            cell.selectionStyle = .none
            guard let productDetails = self.product else{
                return cell
            }
            
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + productDetails.image), placeholderImage: UIImage(named: "Product-1"))
            
            cell.productName.text = productDetails.name.capitalized
            
            let company = productDetails.company
            if company.isEmpty{
                cell.company.text = LanguageManager.Company + " : ---"
            }else{
                cell.company.text = LanguageManager.Company + " : " + company
            }
            
            cell.categoryName.text = LanguageManager.Category + " : " + productDetails.categoryName
            
            let variant = productDetails.variant
            if variant.isEmpty{
                cell.variantLabel.text = LanguageManager.Varient + " : ---"
            }else{
                cell.variantLabel.text = LanguageManager.Varient + ": " + variant
            }
            
            let sku = productDetails.sku
            if sku.isEmpty{
                cell.productSku.text = LanguageManager.ProductCode + " : ---"
            }else{
                cell.productSku.text = LanguageManager.ProductCode + " : " + sku
            }
            
            let purchasePrice = productDetails.buyingPrice
            if purchasePrice.isEmpty{
                cell.buyingPrice.text = LanguageManager.PurchasePrice + " : " + "0.00" + " " + Constants.currencySymbol
            }else{
                cell.buyingPrice.text = LanguageManager.PurchasePrice + " : " + "\(productDetails.buyingPrice)" + " " + Constants.currencySymbol
            }
            
            let sellingPrice = productDetails.sellingPrice
            if sellingPrice.isEmpty{
                cell.SellingPrice.text = LanguageManager.SellingPrice + " : " + "0.00" + " " + Constants.currencySymbol
            }else{
                cell.SellingPrice.text = LanguageManager.SellingPrice + " : " + "\(productDetails.sellingPrice)" + " " + Constants.currencySymbol
            }
            
            cell.cautionLbl.text = LanguageManager.QuantityAlert + " : " + (productDetails.stockAlert?.stringValue ?? "---") + " " + productDetails.unit
                        
            cell.editBtn.tag = productDetails.inventoryId
            cell.editBtn.addTarget(self, action: #selector(onEdit(sender:)), for: .touchUpInside)
            
            self.hasSerial = productDetails.hasSerial
            
//            let totalSell = productDetails.totalSell
//            
//            if totalSell != 0.00 {
//                cell.deleteBtn.isHidden = true
//            }else{
//                cell.deleteBtn.isHidden = false
//                cell.deleteBtn.tag = productDetails.inventoryId
//                cell.deleteBtn.addTarget(self, action: #selector(onDelete(sender:)), for: .touchUpInside)
//            }
            
            if self.deletable == true{
                cell.deleteBtn.isHidden = false
                cell.deleteBtn.tag = productDetails.inventoryId
                cell.deleteBtn.addTarget(self, action: #selector(onDelete(sender:)), for: .touchUpInside)
            }else{
                cell.deleteBtn.isHidden = true
            }
            
            return cell
        case Sections.ProductSummary.rawValue:
            let cell : ProductDetailsSummaryCell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsSummaryCell.identifier, for: indexPath) as! ProductDetailsSummaryCell
            cell.selectionStyle = .none
            guard let productDetails = self.product else{
                return cell
            }
            
            let quantity = productDetails.quantity
            if quantity == "" {
                cell.currentStockLabel.text = "0" + " " + productDetails.unit
            }else{
                cell.currentStockLabel.text = (quantity.toDouble()?.rounded(toPlaces: 2).toString() ?? "") + " " + productDetails.unit
            }
            
            let sellableStockPrice = productDetails.sellableStockPrice
            if sellableStockPrice == "" {
                cell.sellAbleStockLabel.text = "0.00" + " " + Constants.currencySymbol
            }else{
                cell.sellAbleStockLabel.text = (sellableStockPrice.toDouble()?.rounded(toPlaces: 2).toString() ?? "") + " " + Constants.currencySymbol
            }
            
            let totalStockPrice = productDetails.totalStockPrice
            if totalStockPrice == "" {
                cell.totalStockPrice.text = "0.00" + " " + Constants.currencySymbol
            }else{
                cell.totalStockPrice.text = (totalStockPrice.toDouble()?.rounded(toPlaces: 2).toString() ?? "") + " " + Constants.currencySymbol
            }
            
            let totalSell = productDetails.totalSell
            if totalSell == 0.00 {
                cell.totalSaleLabel.text = "0.00" + " " + Constants.currencySymbol
            }else{
                cell.totalSaleLabel.text = "\(totalSell)" + " " + Constants.currencySymbol
            }
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.productInfo.rawValue:
            return UITableView.automaticDimension
        case Sections.ProductSummary.rawValue:
            return 145.0
        default:
            return 0
        }
    }
    
    @objc func onDelete(sender: UIButton){
        self.productId = sender.tag
//        guard let details = self.product else {
//            return
//        }
//        if self.productId == details.inventoryId {
//            let totalSell = details.totalSell
//
//            if totalSell != 0.00 {
//
//            }
//        }
        guard let productId = self.productId else {
            return
        }
        
        self.confirmationMessage(userMessage: "", deleteId: productId)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.AreYouSure, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: LanguageManager.Yes, style: .default){
                (action:UIAlertAction!) in
                self.presenter.deleteProductDataFromServer(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: LanguageManager.No, style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is ProductListViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    @objc func onEdit(sender: UIBarButtonItem){
        self.navigateToProductUpdateVC()
    }
    
    func navigateToProductUpdateVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductUpdateViewController") as! ProductUpdateViewController
        viewController.productInfo = product
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToProductListVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Api Delegate
extension ProductDetailsBaseViewController : ProductViewDelegate{
    func setFreemiumProductData(data: OfflineProductDataMapper) {
        //
    }
    
    func onProductUpdate(data: AddDataMapper) {
        //
    }
    
    func deleteProductData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        
        self.showDeleteAlert(title: message, message: "")
        
    }
    
    func setProductViewData(data: ProductDetailsDataMapper) {
        guard let item = data.product else {
            return
        }
        self.product = item
        self.deletable = data.deletable
        self.refreshTableView()
        
       
    }
    
    func setProductRecentSaleHistory(data: ProductRecentSaleDataMapper) {
        //ThisOneIsEmpty
    }
    
    func setProductRecentPurchaseHistory(data: ProductRecentPurchaseDataMapper) {
        //ThisOneIsEmpty
    }
    
    func setProductSerialNumber(data: ProductSerialDataMapper) {
        //ThisOneIsEmpty
    }
    
    func deleteRecentStockData(data: AddDataMapper) {
        //ThisOneIsEmpty
    }
    
    func serialUpdateData(data: AddDataMapper) {
        //ThisOneIsEmpty
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.inventoryId else{
            return
        }
        self.presenter.getProductViewDataFromServer(id: id)
    }
}

extension ProductDetailsBaseViewController{
    func showDeleteAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                //self.popToSpecificViewController()
                self.navigateToProductListVC()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}
