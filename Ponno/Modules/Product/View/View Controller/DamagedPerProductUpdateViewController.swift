//
//  DamagedPerProductUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 25/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DamagedPerProductUpdateViewController: UIViewController {

    @IBOutlet weak var quantityLbl: UILabel!{
        didSet{
            quantityLbl.text = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var quantityTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!{
        didSet{
            descriptionLbl.text = LanguageManager.Description
        }
    }
    @IBOutlet weak var descriptionTextField: UITextField!{
        didSet{
            descriptionTextField.placeholder = LanguageManager.Description
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = DamagedPerProductUpdatePresenter(service: ProductService())
    
    var damagedMaterialId : Int?
    var quantity : Double?
    var damagedProduct : DamagedPerProductList?
    var damagedMaterialName: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetup()
        self.toolBarSetUp()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.title = self.damagedMaterialName
        
        self.quantityTextField.underlined()
        self.descriptionTextField.underlined()
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
        if let data = self.damagedProduct{
            self.quantityTextField.text = data.quantity
            self.descriptionTextField.text = data.description
        }
    }
    
    func toolBarSetUp(){
        //QuantityToolBar
        let quantityToolBar = UIToolbar()
        quantityToolBar.barStyle = UIBarStyle.default
        quantityToolBar.isTranslucent = true
        quantityToolBar.tintColor = UIColor.black
        quantityToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
        
        quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        quantityToolBar.isUserInteractionEnabled = true
        
        self.quantityTextField.inputAccessoryView = quantityToolBar
        
    }
    
    @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
        self.quantityTextField.resignFirstResponder()
        self.descriptionTextField.becomeFirstResponder()
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    func isValidated()->Bool{
        if self.quantityTextField.text == ""{
            self.showAlert(title: LanguageManager.QuantityIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            if let quantityText = self.quantityTextField.text, let quantity = Double(quantityText), let id = self.damagedMaterialId{
                let description = self.descriptionTextField.text ?? ""
                
                let param : [String : Any] = ["id": id, "quantity": quantity, "description": description]
                self.presenter.postDamagedPerProductUpdateDataToServer(param: param)
            }
        }
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    

}

extension DamagedPerProductUpdateViewController : DamagedPerProductUpdateViewDelegate{
    func setProductSerialData(data: DamagedPerProductSerialDataMapper) {
        //Empty
    }
    
    func updatePerProductData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
    
}
