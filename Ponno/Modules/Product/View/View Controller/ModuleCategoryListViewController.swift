//
//  ModuleCategoryListViewController.swift
//  Ponno
//
//  Created by a k azad on 23/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class ModuleCategoryListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessage: UILabel!
    
    fileprivate var presenter = ModuleProductCategoryPresenter(service: ProductService())
    
    var moduleProductCategory: [ModuleProductCategoryList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var productListSummery : [Summary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var categoryId: Int?
    
    var currentPage : Int = 1
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpInitialLanguage()
        
        self.setBarBtn()
        self.configureTableView()
        self.configureCollectionView()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.initialSetup()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func initialSetup(){
        self.emptyMessage.isHidden = true
        self.title = LanguageManager.Category
        self.emptyMessage.text = LanguageManager.NoInformationFound
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarBtn(){
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let categoryAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        categoryAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        categoryAddBtn.addTarget(self, action: #selector(onCategoryAdd(sender:)), for: UIControl.Event.touchUpInside)
        categoryAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        categoryAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        categoryAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: categoryAddBtn)
        
        let popUpBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        popUpBtn.setImage(UIImage(named: "family-tree"), for: UIControl.State.normal)
        popUpBtn.addTarget(self, action: #selector(onTreeBtnTapped), for: UIControl.Event.touchUpInside)
        popUpBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        popUpBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        popUpBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn3 = UIBarButtonItem(customView: popUpBtn)
        
        navigationItem.setRightBarButtonItems([barBtn3,barButton2,barButton1], animated: true)
    }
    
    @objc func onSearch(sender: UIButton){
        self.navigateToModuleCategorySearchVC()
    }
    
    @objc func onCategoryAdd(sender: UIButton){
        self.navigateToModuleCategoryAddVC()
    }
    
    @objc func onTreeBtnTapped(){
        self.navigateToCategoryTreeVC()
    }
    
    
    
    @objc func onBarPopUpBtnTapped(sender : UIBarButtonItem){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        let treeViewAction = UIAlertAction(title: LanguageManager.TreeView, style: UIAlertAction.Style.default) { (action) in
            self.navigateToCategoryTreeVC()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
            
            self.view.endEditing(true)
        }
        
        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
        
        myActionSheet.addAction(treeViewAction)
        myActionSheet.addAction(cancelAction)
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToModuleCategorySearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ModuleCategorySearchViewController") as! ModuleCategorySearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToCategoryTreeVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "TreeViewController") as! TreeViewController
        //viewController.moduleProductCategory = self.moduleProductCategory
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ModuleCategoryListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.moduleProductCategory = []
        self.currentPage = 1
        self.presenter.getModuleProductCategoryFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }
    
    func navigateToModuleCategoryAddVC(){
        
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "ModuleCategoryAddViewController") as? ModuleCategoryAddViewController
        popoverContentController?.modalPresentationStyle = .popover
        popoverContentController?.preferredContentSize = CGSize(width:500,height:300)
        
        
        if let controller = popoverContentController{
            controller.instanceOfModuleCategoryListVC = self
            if let popoverPresentationController = controller.popoverPresentationController {
                popoverPresentationController.sourceView = self.view
                popoverPresentationController.sourceRect = CGRect(x: 315, y: 54, width: 0, height: 0)
                popoverPresentationController.delegate = self
                if let popoverController = popoverContentController {
                    present(popoverController, animated: true, completion: nil)
                }
            }
        }
        
        
    }
    
}

//Mark: Floaty Delegate
extension ModuleCategoryListViewController: FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController() 
    }
}

//MARK: CollectionView Delegate And Data Source
extension ModuleCategoryListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(ProductListSummeryCell.nib, forCellWithReuseIdentifier: ProductListSummeryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let item = self.productListSummery, item.count > 0  else {
            return 0
        }
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProductListSummeryCell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductListSummeryCell.identifier, for: indexPath) as! ProductListSummeryCell
        guard let item = self.productListSummery, item.count > 0 else{
            return cell
        }

        cell.summeryTitle.text = item[indexPath.row % item.count].title
        cell.summeryValue.text = item[indexPath.row % item.count].value
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.productListSummery, list.count > 0 else{
            return
        }
        let listCount = list.count
        
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//Mark: TableViewDelegate and DataSource
extension ModuleCategoryListViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ModuleProductCategoryCell.nib, forCellReuseIdentifier: ModuleProductCategoryCell.identifier)
        self.tableView.estimatedRowHeight = 60.0
        self.tableView.separatorStyle = .none
        self.emptyMessage.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.moduleProductCategory.count > 0 {
            self.emptyMessage.isHidden = true
            return self.moduleProductCategory.count
        }else{
            self.emptyMessage.isHidden = false
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ModuleProductCategoryCell = tableView.dequeueReusableCell(withIdentifier: ModuleProductCategoryCell.identifier, for: indexPath) as! ModuleProductCategoryCell
        
        cell.selectionStyle = .none
        
        let item = self.moduleProductCategory[indexPath.row]
        
        cell.categoryNameLbl.text = item.name
        cell.categoryPathLbl.text = item.path
        
        if let id = item.id{
            cell.popUpBtn.tag = id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped(sender:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.moduleProductCategory.count > 0{
            self.categoryId = sender.tag
            for category in self.moduleProductCategory{
                if self.categoryId == category.id{
                    guard let id = self.categoryId, let categoryName = category.name else{
                        return
                    }
                    
                    if let deletable = category.deletable, deletable == true{
                        let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                            self.updateAlertWithTextField(id: id, catgoryName: categoryName)
                        }
                        
                        let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                            self.deleteMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        // add action buttons to action sheet
                        myActionSheet.addAction(updateAction)
                        myActionSheet.addAction(deleteAction)
                        myActionSheet.addAction(cancelAction)
                    }else{
                        let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                            self.updateAlertWithTextField(id: id, catgoryName: categoryName)
                        }
                        
//                        let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
//                            self.deleteMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
//                        }
//
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        // add action buttons to action sheet
                        myActionSheet.addAction(updateAction)
                        //myActionSheet.addAction(deleteAction)
                        myActionSheet.addAction(cancelAction)
                    }
                    
                    
                }
            }
        }
        // present the action sheet
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func deleteMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Delete, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            self.presenter.postCategoryDeleteDataToServer(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func updateAlertWithTextField(id: Int, catgoryName: String){
        let alertController = UIAlertController(title: LanguageManager.CategoryUpdate, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.text = catgoryName
            textField.textAlignment = .center
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.NameIsRequired, message: "")
                return
            }
            
            if let id = self.categoryId{
                let param : [String : Any] = ["id": id, "name": textField.text!]

                self.presenter.postCategoryUpdateDataToServer(param: param)
            }
            
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}

extension ModuleCategoryListViewController: ModuleProductCategoryListViewDelegate{
    func setProductCategoryData(products: ModuleProductCategoryDataMapper) {
        self.productListSummery = products.summary
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        guard let category = products.moduleProductCategoryList, category.count > 0 else{
            return
        }
        self.moduleProductCategory = category
    }
    
    func onCategoryUpdate(data: AddDataMapper){
        guard let message = data.message else{
            return
        }
        self.toastMessage(userMessage: message)
    }
    
    func onCategoryDelete(data: AddDataMapper){
        guard let message = data.message else{
            return
        }
        self.toastMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.emptyMessage.isHidden = false
    }
    
    func showLoading() {
        self.emptyMessage.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.emptyMessage.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getModuleProductCategoryFromServer(page: self.currentPage)
    }
}

//Navigation
extension ModuleCategoryListViewController{
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func toastMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.getModuleProductCategoryFromServer(page: self.currentPage)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

extension ModuleCategoryListViewController: UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}

