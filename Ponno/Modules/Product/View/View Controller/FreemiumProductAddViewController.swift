//
//  FreemiumProductAddViewController.swift
//  Ponno
//
//  Created by a k azad on 20/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import iOSDropDown
import SDWebImage
import CoreData

protocol SelectedGlobalProductDelegate: NSObjectProtocol {
    func selectedProduct(selectedProduct: GlobalProducts?)
}

class FreemiumProductAddViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var productNameLbl: UILabel!{
        didSet{
            productNameLbl.text = LanguageManager.ProductName
        }
    }
    @IBOutlet weak var productNameTextField: UITextField!{
        didSet{
            productNameTextField.placeholder = LanguageManager.ProductName
        }
    }
    @IBOutlet weak var productCategoryLbl: UILabel!{
        didSet{
            productCategoryLbl.text = LanguageManager.Category
        }
    }
    @IBOutlet weak var productCategoryTextField: UITextField!{
        didSet{
            productCategoryTextField.placeholder = LanguageManager.SelectOne
        }
    }
    @IBOutlet weak var companyLbl: UILabel!{
        didSet{
            companyLbl.text = LanguageManager.Company
        }
    }
    @IBOutlet weak var companyNameTextField: UITextField!{
        didSet{
            companyNameTextField.placeholder = LanguageManager.Company
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var categoryAddBtn: UIButton!{
        didSet{
            categoryAddBtn.setTitle(LanguageManager.NewCategory, for: .normal)
        }
    }
    @IBOutlet weak var varientLbl: UILabel!{
        didSet{
            varientLbl.text = LanguageManager.Varient
        }
    }
    @IBOutlet weak var varientTextField: UITextField!{
        didSet{
            varientTextField.placeholder = LanguageManager.Varient
        }
    }
    @IBOutlet weak var unitLbl: UILabel!{
        didSet{
            unitLbl.text = LanguageManager.Unit
        }
    }
    @IBOutlet weak var unitTextField: UITextField!{
        didSet{
            unitTextField.placeholder = LanguageManager.SelectOne
        }
    }
    @IBOutlet weak var productCodeLbl: UILabel!{
        didSet{
            productCodeLbl.text = LanguageManager.ProductCode
        }
    }
    @IBOutlet weak var productCodeTextField: UITextField!{
        didSet{
            productCodeTextField.placeholder = LanguageManager.ProductCode
        }
    }
    @IBOutlet weak var buyingPriceLbl: UILabel!{
        didSet{
            buyingPriceLbl.text = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var sellingPriceTextField: UITextField!{
        didSet{
            sellingPriceTextField.placeholder = LanguageManager.SellingPrice
        }
    }
    
    fileprivate var presenter = FreemiumProductAddPresenter(service: ProductService())
    
    //OfflineProduct
       var newProductList: [OfflineProduct]?{
           didSet{
            resetAllRecords(in : "FreemiumProducts")
               if let products = newProductList{
                   for item in products{
                       if let inventoryId = item.inventoryId, let unit = item.unit, let sellingPrice = item.sellingPrice?.toDouble(), let productId = item.productId, let name = item.name, let categoryId = item.categoryId, let categoryName = item.categoryName{
                           
                           insertFreemiumProduct(inventoryId: inventoryId, unit: unit, sellingPrice: sellingPrice, productId: productId, name: name, company: item.company ?? "", varient: item.variant ?? "", sku: item.sku ?? "", image: item.image ?? "", categoryId: categoryId, categoryName: categoryName)
                       }
                       
                   }
               }
           }
       }
    var updatedProductList: [OfflineProduct]?{
        didSet{
            if let products = updatedProductList{
                for item in products{
                    if let inventoryId = item.inventoryId, let unit = item.unit, let sellingPrice = item.sellingPrice?.toDouble(), let productId = item.productId, let name = item.name, let categoryId = item.categoryId, let categoryName = item.categoryName{
                        Update(inventoryId: Int64(inventoryId), unit: unit, sellingPrice: sellingPrice, productId: Int64(productId), name: name, company: item.company ?? "", varient: item.variant ?? "", sku: item.sku ?? "", image: item.image ?? "", categoryId: Int64(categoryId), categoryName: categoryName)
                    }
                }
            }
        }
    }
    var deletedProductList: [OfflineProduct]?{
        didSet{
            if let products = deletedProductList{
                for item in products{
                    if let inventoryId = item.inventoryId{
                        deleteData(id: inventoryId)
                    }
                }
            }
        }
    }
    
    var freemiumProducts : [FreemiumProducts] = []
    
    var categories : [ProductCategories] = []
    var categoryPicker = UIPickerView()
    var productCategoryId : Int?
    var categoryParentId: Int = 0
    
    var globalProducts : [GlobalProducts]?
    var product : GlobalProducts?
        
    var unitPicker = UIPickerView()
    var unitId: Int = 0
    var productUnit : [ProductUnit] = []
    
    let imagePicker = UIImagePickerController()
    var selectedImage : UIImage?
    var categoryName : String?
    
    var message : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpViews()
        self.setUpPickerView()
        self.configureTextFields()
        self.setImageView()
        self.attachPresenter()
    }
    
    func setUpViews(){
        self.title = LanguageManager.NewProduct
        
        ProductObject.companyName = ""
        
        self.navigationController?.navigationBar.topItem?.title = " "
        
        imagePicker.delegate = self
        self.imageIcon.addTapGestureRecognizer(action: {self.onTapOnImageView()})
        
        self.categoryAddBtn.addTarget(self, action: #selector(onCategoryAddBtnPressed), for: .touchUpInside)
        self.productNameTextField.addTarget(self, action: #selector(onProductNameTap), for: .editingDidBegin)
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
    }
    
    @objc func onProductNameTap(sender: UITextField){
        self.productNameTextField.resignFirstResponder()
        self.navigateToGlobalProductListVC(sender: sender)
        //navigatetoGlobal()
    }
    
    func navigateToGlobalProductListVC(sender: UITextField){
        
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "GlobalProductListViewController") as? GlobalProductListViewController
        popoverContentController?.modalPresentationStyle = .popover
        
        if let controller = popoverContentController{
            guard let products = self.globalProducts, products.count > 0 else{
                return
            }
            controller.globalProducts = products
            //controller.dataFromOther = "Hola data is passed and reached"
            controller.selectedProductDelegate = self
            controller.filteredList = self.globalProducts
            
            showPopup(controller, sourceView: sender)
            
//            if let popoverPresentationController = controller.popoverPresentationController {
//                //popoverPresentationController.permittedArrowDirections = .up
//                popoverPresentationController.sourceView = self.view
//                popoverPresentationController.sourceRect = self.productNameTextField.frame
//                popoverPresentationController.delegate = self
//                if let popoverController = popoverContentController {
//                    //popoverController.globalProducts = self.globalProducts
//                    present(popoverController, animated: true, completion: nil)
//                    //self.navigationController?.pushViewController(popoverController, animated: true)
//                }
//            }
        }
        
        
    }
    
    @objc func onCategoryAddBtnPressed(sender: UIButton){
        self.alertWithTextField()
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if self.isValidated(){
            guard let name = self.productNameTextField.text, let category = self.productCategoryId, let sellingPrice = self.sellingPriceTextField.text else{
                return
            }
            let varient = self.varientTextField.text ?? ""
            let sku = self.productCodeTextField.text ?? ""
            let companyName = self.companyNameTextField.text ?? ""
            let params : [String : Any] = ["name" :name, "category": category,
                "company": companyName,
                "variant": varient,
                "unit": self.unitId,
                "sku": sku,
                "serial_no_status": "",
                "serials" : "",
                "buying_price": "",
                "selling_price": sellingPrice,
                "vendor_id": "",
                "quantity": "",
                "stock_alert": "",
                "warranty": "",
                "expire_date": ""]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            self.presenter.postNewProductDataToServer(productData: params)
        }
    }

}

//Mark: TextField Delegate
extension FreemiumProductAddViewController: UITextFieldDelegate{
    func configureTextFields(){
        self.productNameTextField.delegate = self
        self.productCategoryTextField.delegate = self
        self.companyNameTextField.delegate = self
        self.varientTextField.delegate = self
        self.unitTextField.delegate = self
        self.productCodeTextField.delegate = self
        
        self.productNameTextField.underlined()
        self.productCategoryTextField.underlined()
        self.companyNameTextField.underlined()
        self.varientTextField.underlined()
        self.unitTextField.underlined()
        self.productCodeTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.productNameTextField{
            self.productCategoryTextField.becomeFirstResponder()
        }else if textField == self.productCategoryTextField{
            self.companyNameTextField.becomeFirstResponder()
        }else if textField == self.companyNameTextField{
            self.varientTextField.becomeFirstResponder()
        }else if textField == self.varientTextField{
            self.unitTextField.becomeFirstResponder()
        }else if textField == self.unitTextField{
            self.productCodeTextField.becomeFirstResponder()
        }else if textField == self.productCodeTextField{
            self.productCodeTextField.resignFirstResponder()
        }else if textField == self.sellingPriceTextField{
            self.submitBtn.becomeFirstResponder()
        }
        return false
    }
    
}

extension FreemiumProductAddViewController: UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}

extension FreemiumProductAddViewController{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewCategoryAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.CategoryName
            textField.textAlignment = .center
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showMessage(userMessage: LanguageManager.CategoryIsRequired)
                return
            }
            
            self.categoryName = textField.text?.lowercased()

            for category in self.categories{
                let existedCategory = category.name.lowercased()
                if self.categoryName == existedCategory{
                    self.showAlert(title: LanguageManager.CategoryAlreadyExists, message: "")
                    return
                }
            }
            
            let param : [String : Any] = ["name": textField.text!, "parent": self.categoryParentId]
            
            self.presenter.postProductCategoryAddDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.productCategoryTextField.resignFirstResponder()
            self.companyNameTextField.becomeFirstResponder()
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.alertWithTextField()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: PickerViewDelegate
extension FreemiumProductAddViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == categoryPicker {
            guard self.categories.count > 0 else {
                return 0
            }
            return categories.count
        }else{
            guard self.productUnit.count > 0 else {
                return 0
            }
            return self.productUnit.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == categoryPicker{
            if self.categories.count > 0 {
                let item = categories[row]
                self.productCategoryId = item.id
                self.productCategoryTextField.text = item.name
                return item.name
            }
            return ""
        }else{
            guard self.productUnit.count > 0 else {
                return ""
            }
            let unitItem = self.productUnit[row]
            self.unitTextField.text = unitItem.name
            return unitItem.name
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == categoryPicker{
            if self.categories.count > 0 {
                let item =  self.categories[row]
                self.productCategoryTextField.text =  item.name
                self.productCategoryId = item.id
            }
        }else{
            if self.productUnit.count > 0 {
                let unitItem = self.productUnit[row]
                self.unitTextField.text = unitItem.name
                self.unitId = unitItem.id
            }
        }
    }
    
}

//Mark: PickerViewSetup
extension FreemiumProductAddViewController{
    func setUpPickerView(){
        categoryPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCategory(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.productCategoryTextField.inputView = categoryPicker
        self.productCategoryTextField.inputAccessoryView = toolBar
        
        unitPicker.delegate = self
        
        let unitToolBar = UIToolbar()
        unitToolBar.barStyle = UIBarStyle.default
        unitToolBar.isTranslucent = true
        unitToolBar.tintColor = UIColor.black
        unitToolBar.sizeToFit()
        
        
        let doneButtonOnUnit = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnUnit(sender:)))
        
        unitToolBar.setItems([cancelButton,spaceButton, doneButtonOnUnit], animated: false)
        unitToolBar.isUserInteractionEnabled = true
        
        self.unitTextField.inputView = unitPicker
        self.unitTextField.inputAccessoryView = unitToolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnCategory(sender : UIBarButtonItem){
        self.companyNameTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnUnit(sender : UIBarButtonItem){
        self.productCodeTextField.becomeFirstResponder()
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigateToFreemiumProductVC()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func navigateToFreemiumProductVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumProductListViewController") as! FreemiumProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
}

//Mark: ImagePicker
extension FreemiumProductAddViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    private func setImageView(){
        guard let imageUrl = self.product?.image else{
            self.setImageInImageView(imageUrl: "")
            return
        }
        
        self.setImageInImageView(imageUrl: ImageUrl.sharedImageInstance.productImage + imageUrl)
    }
    
    func setImageInImageView(imageUrl : String){
        self.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageIcon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
    }
    
    func onTapOnImageView(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageIcon.contentMode = .scaleAspectFill
            imageIcon.image = pickedImage
            self.selectedImage = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension FreemiumProductAddViewController: FreemiumProductAddViewDelegate{
    func setProductCategoriesList(productCategoryData: ProductCategoryListDataMapper) {
        if let productCategory = productCategoryData.categories, productCategory.count > 0{
            self.categories = productCategory
            
        }
        
    }
    
    func setCategoryAddData(data: CategoryAddDataMapper) {
        guard let categoryData = data.category else {
            return
        }
        self.productCategoryTextField.text =  categoryData.name
        self.productCategoryTextField.resignFirstResponder()
        self.productCategoryId = categoryData.id
        self.categoryParentId = 0
    }
    
    func setProductUnit(unit: ProductUnitDataMapper) {
        if let unitItem = unit.productUnit, unitItem.count > 0 {
            self.productUnit = unitItem
        }
    }
    
    func setGlobalProducts(products: GlobalProductDataMapper) {
        if let globalProducts = products.globalProductList, globalProducts.count > 0{
            self.globalProducts = globalProducts
        }
    }
    
    func onSuccessfulProductAdd(newProductData: ProductAddDataMapper) {
        guard let message = newProductData.message, let product = newProductData.product else {
            return
        }
        self.message = message
        self.successMessage(userMessage: message)
        
        self.getFreemiumOfflineData()
        
        if let id = product.id{
            guard let image = ProductObject.productImage else{
                self.successMessage(userMessage: message)
                return
            }
            
            self.presenter.uploadImage(productId: id, image: image)
        }
        
        
    }
    
    func onImageUpload(data: AddDataMapper) {
        self.successMessage(userMessage: self.message ?? "")
    }
    
    func setFreemiumProductData(data: OfflineProductDataMapper) {
        self.newProductList = data.newProductList
        self.updatedProductList = data.updatedProductList
        self.deletedProductList = data.deletedProductList
        //fetchData()
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.getDataFromServer()
    }
    
    func getDataFromServer(){
        self.presenter.getGlobalProductsFromServer()
        self.presenter.getProductUnitDataFromServer()
        self.presenter.getAllProductCategoryListFromServer(getAll: true)
    }
    
}

extension FreemiumProductAddViewController{
    func isValidated()->Bool{
        if self.productNameTextField.text == ""{
            showAlert(title: LanguageManager.ProductNameIsRequired, message: "")
            return false
        }else if self.productCategoryTextField.text == ""{
            showAlert(title: LanguageManager.ProductCategoryIsRequired, message: "")
            return false
        }else if self.unitTextField.text == ""{
            showAlert(title: LanguageManager.ProductUnitIsRequired, message: "")
            return false
        }else if self.sellingPriceTextField.text == ""{
            showAlert(title: LanguageManager.SellingPriceIsRequired, message: "")
            return false
        }
        return true
    }
}

extension FreemiumProductAddViewController : SelectedGlobalProductDelegate{
    func selectedProduct(selectedProduct: GlobalProducts?) {
        guard let item = selectedProduct else{
            return
        }
        if let products = self.globalProducts{
            for product in products{
                if product.id == item.id{
                    self.product = item
                    self.productNameTextField.text = product.name
                    if product.categoryName != ""{
                        self.productCategoryTextField.isEnabled = false
                    }
                    self.productCategoryTextField.text = product.categoryName
                    self.productCategoryId = product.categoryId
                    self.varientTextField.text = product.variant
                    self.companyNameTextField.text = product.company
                    self.unitTextField.text = product.unitName
                    self.unitId = product.unit
                    self.productCodeTextField.text = product.sku
                    self.setImageInImageView(imageUrl: product.image ?? "")
                }else{
                    self.productNameTextField.text = item.name
                }
            }
        }
        
    }
    
    
}

extension FreemiumProductAddViewController{
    func getFreemiumOfflineData(){
        if let lUpdatedTime = getTimeStamp(){
            if lUpdatedTime.timeStamp == "default"{
                deleteAllData(entity: "FreemiumProducts")
                getOfflineProduct(timesStamp: lUpdatedTime.timeStamp ?? "")
            }else{
                getOfflineProduct(timesStamp: lUpdatedTime.timeStamp ?? "")
            }
            
        }
    }
    
    func getOfflineProduct(timesStamp: String){
        self.presenter.getFreemiumOfflineProductFromServer(timeStamp: timesStamp)
    }
    
    func fetchData(){

        freemiumProducts.removeAll()
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FreemiumProducts")

        do {
            let results = try context.fetch(fetchRequest)
            let  products = results as! [FreemiumProducts]

            for product in products {
                freemiumProducts.append(product)
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }

    }
    
}


