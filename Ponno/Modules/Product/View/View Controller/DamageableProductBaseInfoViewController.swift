//
//  DamageableProductBaseInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 25/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DamageableProductBaseInfoViewController: UIViewController {
    
    @IBOutlet weak var quantityLbl: UILabel!{
        didSet{
            self.quantityLbl.text = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var quantityTextField: UITextField!{
        didSet{
            self.quantityTextField.placeholder = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var buyingPriceLbl: UILabel!{
        didSet{
            buyingPriceLbl.text = LanguageManager.BuyingPrice
        }
    }
    @IBOutlet weak var buyingPriceTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!{
        didSet{
            descriptionLbl.text = LanguageManager.Description

        }
    }
    @IBOutlet weak var descriptionTextField: UITextField!{
        didSet{
            descriptionTextField.placeholder = LanguageManager.Description
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    
    var delegate : DamageableProductAddDelegate?
    var product : ProductForDamage?
    var quantity : Double?
    var buyingPrice : Double?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpViews()
        self.toolBarSetUp()
        self.initialSetup()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext), for: .touchUpInside)
        self.quantityTextField.underlined()
        self.buyingPriceTextField.underlined()
        self.buyingPriceTextField.isEnabled = false
        self.descriptionTextField.underlined()
        
        
    }
    
    func initialSetup(){
        guard let item = self.product else{
            return
        }
        self.buyingPriceTextField.text = item.buyingPrice
        
        self.quantity = Double(item.quantity)
        self.buyingPrice = Double(item.buyingPrice)
        
    }
    
    func toolBarSetUp(){
        //QuantityToolBar
        let quantityToolBar = UIToolbar()
        quantityToolBar.barStyle = UIBarStyle.default
        quantityToolBar.isTranslucent = true
        quantityToolBar.tintColor = UIColor.black
        quantityToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
        
        quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        quantityToolBar.isUserInteractionEnabled = true
        
        self.quantityTextField.inputAccessoryView = quantityToolBar
        
        //RefundPriceToolBar
        let buyingPriceToolBar = UIToolbar()
        buyingPriceToolBar.barStyle = UIBarStyle.default
        buyingPriceToolBar.isTranslucent = true
        buyingPriceToolBar.tintColor = UIColor.black
        buyingPriceToolBar.sizeToFit()
        
        let refundPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnRefundPrice(sender:)))
        
        buyingPriceToolBar.setItems([cancelButton, spaceButton, refundPriceDoneButton], animated: false)
        buyingPriceToolBar.isUserInteractionEnabled = true
        
        self.buyingPriceTextField.inputAccessoryView = buyingPriceToolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
        self.quantityTextField.resignFirstResponder()
        self.buyingPriceTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnRefundPrice(sender: UIBarButtonItem){
        self.buyingPriceTextField.resignFirstResponder()
    }
    
    @objc func onNext(sender : UIButton){
        if self.isValidated(){
            guard let quantity = self.quantityTextField.text, let quantityDouble = Double(quantity), let buyingPrice = self.buyingPriceTextField.text, let buyingPriceDouble = Double(buyingPrice)  else{
                return
            }
            let descriptionText = self.descriptionTextField.text ?? ""
            self.delegate?.setProductData(amount : quantityDouble, buyingPrice : buyingPriceDouble, description: descriptionText)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func isValidated()->Bool{
        if (self.quantityTextField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.QuantityIsRequired)
            return false
        }
        else if (self.buyingPriceTextField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.BuyingPriceIsRequired)
            return false
        }
        if let buyingPriceText = self.buyingPriceTextField.text, let buyingPrice = Double(buyingPriceText), let quantityText = self.quantityTextField.text, let quantity = Double(quantityText), let buyingPriceCheck = self.buyingPrice, let quantityCheck = self.quantity  {
            if quantity > quantityCheck {
                self.showAlert(title: LanguageManager.QuantityCantBeGreaterThan + " \(quantityCheck)", message: "")
                return false
            }else if buyingPrice > buyingPriceCheck {
                self.showAlert(title: LanguageManager.BuyingpriceCantBeGreaterThan + " \(buyingPriceCheck)", message: "")
                return false
            }
        }
        return true
    }

}
