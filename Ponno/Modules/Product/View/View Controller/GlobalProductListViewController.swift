//
//  GlobalProductListViewController.swift
//  Ponno
//
//  Created by a k azad on 21/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class GlobalProductListViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessage: UILabel!
    @IBOutlet weak var nameAddBtn: UIButton!
    
    fileprivate var presenter = GlobalProductListPresenter(service: ProductService())
    
    var globalProducts : [GlobalProducts]?
//    {
//        didSet{
//            self.refreshTableView()
//        }
//    }
        
    var filteredList : [GlobalProducts]?
    
    var isSearchActive = false
    {
        didSet{
            self.refreshTableView()
        }
    }
    
    var searchText = ""
    var product : GlobalProducts?
    var timer : Timer?
    
    var selectedProductDelegate: SelectedGlobalProductDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupAttributeTitle()
        self.setUpSearchBar()
        self.configureTableView()
    }
    
    func initialSetup(){
        guard let products = self.globalProducts, products.count > 0 else {
            return
        }
        self.refreshTableView()
    }
    
    func setupAttributeTitle(){
        self.setUpInitialLanguage()
        self.searchBar.resignFirstResponder()
        searchBar.placeholder = LanguageManager.ProductSearch
        emptyMessage.text = LanguageManager.NoInformationFound
        self.nameAddBtn.addTarget(self, action: #selector(onNameAddBtnTapped), for: .touchUpInside)
        //self.searchBar.setImage(UIImage(named: "addIcon"), for: .bookmark, state: .normal)
    }
    
    @objc func onNameAddBtnTapped(sender: UIButton){
        
        if let text = self.searchBar.text, text.isEmpty == false{
            setGlobalProductData(productName: text)
        }else{
            self.showAlert(title: LanguageManager.ProductNameIsRequired, message: "")
        }
        
    }
    
}

//Mark: Search Delegate
extension GlobalProductListViewController: UISearchBarDelegate{
    fileprivate func setUpSearchBar(){
        self.searchBar.delegate = self
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearchActive = false
        
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = true
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let products = self.globalProducts, products.count > 0 else {
            return
        }
        if products.count > 0 {
            if searchText == "" {
                isSearchActive = false
                return
            }
        }
        guard let textToSearch = searchBar.text else {
            return
        }
                
        if textToSearch.count > 2{
            filteredList = products.filter { (product : GlobalProducts) -> Bool in
                return product.name.lowercased().contains(textToSearch.lowercased())
                
            }
        }
        guard let filterd = self.filteredList, filterd.count > 0 else {
            return
        }
        if filterd.count == 0 {
            isSearchActive = false
        }
        else {
            isSearchActive = true
        }
        
    }
    
    
    func setGlobalProductData(productName: String){
        let product = GlobalProducts(name: productName)
        self.selectedProductDelegate?.selectedProduct(selectedProduct: product)
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension GlobalProductListViewController : UITableViewDataSource, UITableViewDelegate{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(GlobalProductCell.nib, forCellReuseIdentifier: GlobalProductCell.identifier)
        self.tableView.separatorStyle = .none
        self.emptyMessage.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive{
            guard let products = self.filteredList, products.count > 0 else {
                self.emptyMessage.isHidden = false
                return 0
            }
            
            self.emptyMessage.isHidden = true
            return products.count
        }else{
            guard let products = self.globalProducts, products.count > 0 else {
                self.emptyMessage.isHidden = false
                return 0
            }
            self.emptyMessage.isHidden = true
            return products.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GlobalProductCell = tableView.dequeueReusableCell(withIdentifier: "GlobalProductCell", for: indexPath) as! GlobalProductCell
        if isSearchActive{
            guard let products = self.filteredList, products.count > 0 else {
                return cell
            }
            
            let item = products[indexPath.row]
            cell.product = item
            return cell
        }else{
            guard let products = self.globalProducts, products.count > 0 else {
                return cell
            }
            let item = products[indexPath.row]
            cell.product = item
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearchActive{
            guard let products = self.filteredList, products.count > 0 else {
                return
            }
            let item = products[indexPath.row]
            self.selectedProductDelegate?.selectedProduct(selectedProduct: item)
            self.dismiss(animated: true, completion: nil)
        }else{
            guard let products = self.globalProducts, products.count > 0 else {
                return
            }
            let item = products[indexPath.row]
            self.selectedProductDelegate?.selectedProduct(selectedProduct: item)
//            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
}


//extension GlobalProductListViewController: GlobalProductListViewDelegate{
//    func setGlobalProducts(products: GlobalProductDataMapper) {
//        guard let item = products.globalProductList, item.count > 0 else{
//            return
//        }
//        self.globalProducts = item
//    }
//
//    func onFailed(data: String) {
//        self.emptyMessage.isHidden = false
//    }
//
//    func showLoading() {
//        self.emptyMessage.isHidden = true
//        self.showLoader()
//    }
//
//    func hideLoading() {
//        self.emptyMessage.isHidden = true
//        self.hideLoader()
//    }
//
//    func attachPresenter(){
//        self.presenter.attachView(viewDelegate: self)
//        self.presenter.getGlobalProductsFromServer()
//    }
//}
