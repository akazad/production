//
//  FreemiumRecentSaleVC.swift
//  Ponno
//
//  Created by a k azad on 8/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class FreemiumRecentSaleVC: UIViewController {

    @IBOutlet weak var recentSaleView: UIView!{
        didSet{
            self.setUpCardView(uiview: recentSaleView)
        }
    }
    @IBOutlet weak var recentSaleLbl: UILabel!{
        didSet{
            recentSaleLbl.text = LanguageManager.RecentSale
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessageLbl: UILabel!{
        didSet{
            emptyMessageLbl.text = LanguageManager.NoInformationFound
        }
    }
    
    private var presenter = FreemiumPanViewPresenter(service: ProductService())
    
    var productId : Int?
    var product: Product?
    
    var recentSale : [ProductRecentSales] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var productName: String?
    
    //  Pagination Propertise
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
//    var inventoryId : Int?
//    var vendorName : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.emptyMessageLbl.isHidden = true
        self.configureTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
}

//Mark: TableView Delegate and DataSource
extension FreemiumRecentSaleVC: UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = .clear
        self.tableView.register(ProductDetailsRecentSellCell.nib, forCellReuseIdentifier: ProductDetailsRecentSellCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.emptyMessageLbl.isHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.recentSale.count > 0 {
            self.emptyMessageLbl.isHidden = true
            return self.recentSale.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductDetailsRecentSellCell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsRecentSellCell.identifier, for: indexPath) as! ProductDetailsRecentSellCell
        cell.selectionStyle = .none
        if self.recentSale.count > 0 {
            let item = self.recentSale[indexPath.row]
            
            cell.recentSale = item
            
            if isLoading == false && indexPath.row == self.recentSale.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                guard let id = self.productId else{
                    return cell
                }
                
                self.presenter.getProductRecentSaleHistoryFromServer(page: self.currentPage, id: id)
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelect indexPath: IndexPath) {
        if self.recentSale.count > 0{
            let item = self.recentSale[indexPath.row]
            let id = item.invoiceId
            self.navigateToSaleDetailsVC(iD : id)
        }
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {

        return false
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.recentSale.count > 0{
            let item = self.recentSale[indexPath.row]
            let id = item.invoiceId
            self.navigateToSaleDetailsVC(iD : id)
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let saleDetailsVC = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        saleDetailsVC.iD = iD
        self.navigationController?.pushViewController(saleDetailsVC, animated: true)
    }
    
}

extension FreemiumRecentSaleVC: FreemiumPanViewDelegate{
    func setProductRecentSaleHistory(data: ProductRecentSaleDataMapper) {
        guard let list = data.sales, list.count > 0 else{
            return
        }
        self.isLoading = false
        self.recentSale += list
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.emptyMessageLbl.isHidden = false
        self.emptyMessageLbl.text = LanguageManager.NoInformationFound
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.productId else{
            return
        }
        self.isLoading = true
        self.presenter.getProductRecentSaleHistoryFromServer(page: self.currentPage, id: id)
    }
    
}

