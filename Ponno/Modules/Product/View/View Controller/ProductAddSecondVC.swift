//
//  ProductAddSecondVC.swift
//  Ponno
//
//  Created by a k azad on 16/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductAddSecondVC: UIViewController {

    @IBOutlet weak var varient: UITextField!
    @IBOutlet weak var unit: UITextField!
    @IBOutlet weak var productCode: UITextField!
    @IBOutlet weak var nextBtn: UIButton!
    
    private var presenter = ProductAddSecondPresenter(service: ProductService())
    
    var unitId : Int?
    var productUnit : [ProductUnit] = []{
        didSet{
            self.unitPicker.reloadAllComponents()
        }
    }
    var unitPicker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViews()
        self.attachPresenter()
        self.setUpPickerView()
        self.configureTextFields()
    }
    
    func setUpViews(){
        self.title = "New Product"
        self.nextBtn.addTarget(self, action: #selector(onNextBtn(sender:)), for: .touchUpInside)
        self.navigationController?.navigationBar.topItem?.title = " "
        self.unit.text = "pc"
        self.unitId = 1
    }
    
    
    @objc func onNextBtn(sender : UIButton){
        if self.isValidated(){
            ProductObject.varient = self.varient.text
            ProductObject.unitId = self.unitId
            ProductObject.sku = self.productCode.text
            
            self.navigateToAddProductThirdViewController()
        }
    }
    
    func isValidated()->Bool{
        if self.unit.text == ""{
            showAlert(title: "unit is empty", message: "")
            return false
        }
        
        return true
    }
    
    
    
    func navigateToAddProductThirdViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductAddThirdVC") as! ProductAddThirdVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //PickerView
    func setUpPickerView(){
//        self.varient.underlined()
        unitPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        self.unit.inputView = unitPicker
        self.unit.inputAccessoryView = toolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.varient.resignFirstResponder()
        self.productCode.becomeFirstResponder()
    }
    
}


//Mark: PickerViewDelegate
extension ProductAddSecondVC : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        guard self.productUnit.count > 0 else {
            return 0
        }
        return self.productUnit.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard self.productUnit.count > 0 else {
            return ""
        }
        let unitItem = self.productUnit[row]
        self.unit.text = unitItem.name
        return unitItem.name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard self.productUnit.count > 0 else {
            return
        }
        let unitItem = self.productUnit[row]
        let selectedRow = [self.unitPicker.selectedRow(inComponent: 0)]
        if selectedRow == [0]{
            self.unit.text = self.productUnit[0].name
            self.unitId = self.productUnit[0].id
        }else{
            self.unit.text = unitItem.name
            self.unitId = unitItem.id
        }
    }
    
}

//Mark: TextField Delegate
extension ProductAddSecondVC : UITextFieldDelegate{
    func configureTextFields(){
        self.varient.delegate = self
        self.productCode.delegate = self
        self.varient.underlined()
        self.productCode.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.varient{
            self.unit.becomeFirstResponder()
        }
        else if textField == self.productCode{
            self.nextBtn.becomeFirstResponder()
        }
        return false
    }
}

//Mark: Api Delegate
extension ProductAddSecondVC : ProductAddSecondViewDelegate{
    func setProductUnit(unit: ProductUnitDataMapper) {
        guard let item = unit.productUnit, item.count > 0 else {
            return
        }
        self.productUnit = item
    }
    
    func onFailed(data: String) {
        
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getProductUnitDataFromServer()
    }
    
}
