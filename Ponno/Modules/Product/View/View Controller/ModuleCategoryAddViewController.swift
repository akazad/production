//
//  ModuleCategoryAddViewController.swift
//  Ponno
//
//  Created by a k azad on 23/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ModuleCategoryAddViewController: UIViewController {
    
    @IBOutlet weak var parentCategoryLbl: UILabel!
    @IBOutlet weak var parentCategoryTextField: UITextField!
    @IBOutlet weak var newCategoryLbl: UILabel!
    @IBOutlet weak var newCategoryTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = ProductCategoryPresenter(service: ProductService())
    
    var categories : [ProductCategories] = []{
        didSet{
            self.categoryPicker.reloadAllComponents()
        }
    }
    
    var noneCategory = ProductCategories(id: 0, name: "None", gen: 0, parent: 0)
    var currentPage : Int = 1
    
    var categoryPicker = UIPickerView()
    var categoryParentId: Int = 0
    
    var instanceOfModuleCategoryListVC: ModuleCategoryListViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSet()
        self.setUpPickerView()
        self.attachPresenter()
    }
    
    func initialSet(){
        self.setUpInitialLanguage()
        self.parentCategoryLbl.text = LanguageManager.Category
        self.parentCategoryTextField.placeholder = LanguageManager.SelectOne
        self.newCategoryLbl.text = LanguageManager.SubCategory
        self.newCategoryTextField.placeholder = LanguageManager.CategoryName
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
    }
    
    //Mark: PickerView
    func setUpPickerView(){
        categoryPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCategory(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.parentCategoryTextField.inputView = categoryPicker
        self.parentCategoryTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnCategory(sender : UIBarButtonItem){
        self.newCategoryTextField.becomeFirstResponder()
    }
    
    func isValidated()->Bool{
        if self.newCategoryTextField.text == ""{
            showAlert(title: LanguageManager.CategoryIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            if let newCategory = self.newCategoryTextField.text{
                let param : [String: Any] = ["name": newCategory, "parent": self.categoryParentId]
                self.presenter.postProductCategoryAddDataToServer(param: param)
            }
        }
    }
    
}

//Mark: PickerViewDelegate
extension ModuleCategoryAddViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        guard self.categories.count > 0 else {
            return 0
        }
        return categories.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if self.categories.count > 0 {
            let item = categories[row]
            self.categoryParentId = item.id
            self.parentCategoryTextField.text = item.name
            return item.name
        }
        return ""
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.categories.count > 0 {
            let item =  self.categories[row]
            self.parentCategoryTextField.text =  item.name
            self.categoryParentId = item.id
        }
    }
    
}

//Mark: TextField Delegate
extension ModuleCategoryAddViewController: UITextFieldDelegate{
    func configureTextFields(){
        self.newCategoryTextField.delegate = self
        self.newCategoryTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.newCategoryTextField{
            self.submitBtn.becomeFirstResponder()
        }
        return false
    }

}

//Api Delegate
extension ModuleCategoryAddViewController: ProductCategoryViewDelegate{
    func setProductCategoriesList(productCategoryData: ProductCategoryListDataMapper) {
        guard var categoryItems = productCategoryData.categories else{
            return
        }
        
        categoryItems.insert(noneCategory, at: 0)
        self.categories = categoryItems
        
    }
    
    func setCategoryAddData(data: CategoryAddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
        
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getProductCategoryListFromServer(page: self.currentPage)
    }
}

extension ModuleCategoryAddViewController{
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.instanceOfModuleCategoryListVC.attachPresenter()
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
