//
//  ProductAdvanceSearchVC.swift
//  Ponno
//
//  Created by a k azad on 24/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductAdvanceSearchVC: UIViewController {
    
    @IBOutlet weak var searchFilterLbl: UILabel!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productNameTextField: UITextField!
    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var varientLbl: UILabel!
    @IBOutlet weak var varientTextField: UITextField!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var dealerLbl: UILabel!
    @IBOutlet weak var dealerTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = ProductAdvanceSearchPresenter(service: ProductService())
    
    var categoryPicker = UIPickerView()
    var categories : [ProductCategories] = []{
        didSet{
            self.categoryPicker.reloadAllComponents()
        }
    }
    var categoryId: Int?
    
    var vendorPicker = UIPickerView()
    var vendorsList : [VendorsList] = []{
        didSet{
            self.vendorPicker.reloadAllComponents()
        }
    }
    var vendorId: Int?
    
    var productAdvanceSearchDelegate : ProductAdvanceSearchDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.setUpPickerView()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.setUpInitialLanguage()
        self.searchFilterLbl.text = LanguageManager.SearchFilter
        self.productNameLbl.text = LanguageManager.ProductName
        self.productNameTextField.placeholder = LanguageManager.ProductName
        self.companyLbl.text = LanguageManager.Company
        self.companyTextField.placeholder = LanguageManager.Company
        self.varientLbl.text = LanguageManager.Varient
        self.varientTextField.placeholder = LanguageManager.Varient
        self.categoryLbl.text = LanguageManager.CategoryName
        self.categoryTextField.placeholder = LanguageManager.SelectOne
        self.dealerLbl.text = LanguageManager.VendorName
        self.dealerTextField.placeholder = LanguageManager.SelectOne
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
    }
    
    //Mark: PickerView
    func setUpPickerView(){
        categoryPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCategory(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.categoryTextField.inputView = categoryPicker
        self.categoryTextField.inputAccessoryView = toolBar
        
        vendorPicker.delegate = self
        
        let vendorToolBar = UIToolbar()
        vendorToolBar.barStyle = UIBarStyle.default
        vendorToolBar.isTranslucent = true
        vendorToolBar.tintColor = UIColor.black
        vendorToolBar.sizeToFit()
        
        let vendorDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnVendor(sender:)))
        
        vendorToolBar.setItems([cancelButton,spaceButton, vendorDoneButton], animated: false)
        vendorToolBar.isUserInteractionEnabled = true
        
        
        self.dealerTextField.inputView = vendorPicker
        self.dealerTextField.inputAccessoryView = vendorToolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnCategory(sender : UIBarButtonItem){
        self.dealerTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnVendor(sender : UIBarButtonItem){
        self.dealerTextField.resignFirstResponder()
        self.submitBtn.becomeFirstResponder()
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        let productName = self.productNameTextField.text ?? "default"
        let company = self.companyTextField.text ?? "default"
        let varient = self.varientTextField.text ?? "default"
        let category = self.categoryId ?? 0
        let vendor = self.vendorId ?? 0
        productAdvanceSearchDelegate?.setProductAdvanceSearchParam(name: productName, company: company, varient: varient, categoryId: category, dealerId: vendor)
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

//Mark: PickerViewDelegate
extension ProductAdvanceSearchVC : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == categoryPicker {
            guard self.categories.count > 0 else {
                return 0
            }
            return categories.count
        }else{
            guard self.vendorsList.count > 0 else {
                return 0
            }
            return self.vendorsList.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == categoryPicker{
            if self.categories.count > 0 {
                let item = categories[row]
                self.categoryId = item.id
                self.categoryTextField.text = item.name
                return item.name
            }
            return ""
        }else{
            guard self.vendorsList.count > 0 else {
                return ""
            }
            let item = self.vendorsList[row]
            self.dealerTextField.text = item.name
            self.vendorId = item.id
            return item.name
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == categoryPicker{
            if self.categories.count > 0 {
                let item =  self.categories[row]
                self.categoryTextField.text =  item.name
                self.categoryId = item.id
            }
        }else{
            if self.vendorsList.count > 0 {
                let item = self.vendorsList[row]
                self.dealerTextField.text = item.name
                self.vendorId = item.id
            }
        }
    }
    
}

extension ProductAdvanceSearchVC : ProductAdvanceSearchViewDelegate{
    func setProductCategoryData(products: ProductCategoryListDataMapper) {
        guard let item = products.categories, item.count > 0 else{
            return
        }
        self.categories = item
    }
    
    func setDealerData(data: VendorsDataMapper) {
        guard let item = data.vendors, item.count > 0 else{
            return
        }
        self.vendorsList = item
    }
    
    func onFailed(data: String) {
        //
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getDealerDataFromServer(getAll: true)
        self.presenter.getProductCategoryListFromServer(getAll: true)
    }
}
