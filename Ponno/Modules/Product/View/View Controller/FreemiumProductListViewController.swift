//
//  FreemiumProductListViewController.swift
//  Ponno
//
//  Created by a k azad on 8/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import CoreData

class FreemiumProductListViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = FreemiumProductListPresenter(service: ProductService())
    
    var timer : Timer?
    var productList : [ProductList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var productListSummery : [ProductListSummery]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var currentPage : Int = 1
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    var showNavDrawer : Bool = true
    let manager = CollectionViewScrollManager()
    
    var freemiumProducts : [FreemiumProducts] = []{
        didSet{
            self.tableView.reloadData()
        }
    }
    
    //OfflineProduct
       var newProductList: [OfflineProduct]?{
           didSet{
            resetAllRecords(in : "FreemiumProducts")
               if let products = newProductList{
                   for item in products{
                       if let inventoryId = item.inventoryId, let unit = item.unit, let sellingPrice = item.sellingPrice?.toDouble(), let productId = item.productId, let name = item.name, let categoryId = item.categoryId, let categoryName = item.categoryName{
                           
                           insertFreemiumProduct(inventoryId: inventoryId, unit: unit, sellingPrice: sellingPrice, productId: productId, name: name, company: item.company ?? "", varient: item.variant ?? "", sku: item.sku ?? "", image: item.image ?? "", categoryId: categoryId, categoryName: categoryName)
                       }
                       
                   }
               }
           }
       }
    var updatedProductList: [OfflineProduct]?{
        didSet{
            if let products = updatedProductList{
                for item in products{
                    if let inventoryId = item.inventoryId, let unit = item.unit, let sellingPrice = item.sellingPrice?.toDouble(), let productId = item.productId, let name = item.name, let categoryId = item.categoryId, let categoryName = item.categoryName{
                        Update(inventoryId: Int64(inventoryId), unit: unit, sellingPrice: sellingPrice, productId: Int64(productId), name: name, company: item.company ?? "", varient: item.variant ?? "", sku: item.sku ?? "", image: item.image ?? "", categoryId: Int64(categoryId), categoryName: categoryName)
                    }
                }
            }
        }
    }
    var deletedProductList: [OfflineProduct]?{
        didSet{
            if let products = deletedProductList{
                for item in products{
                    if let inventoryId = item.inventoryId{
                        deleteData(id: inventoryId)
                    }
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.showNavDrawer{
            addSlideMenuButton()
        }
        self.setNavigationBarTitle()
        self.configureTableVIew()
        self.configureCollectionView()
        self.addFloaty()
        self.setBarButton()
        //self.setUpSearchBar()
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.Inventory
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0))]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.fetchData()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setBarButton(){
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let productAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        productAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        productAddBtn.addTarget(self, action: #selector(onProductAdd(sender:)), for: UIControl.Event.touchUpInside)
        productAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        productAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        productAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: productAddBtn)
        
//        let popUpBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
//        popUpBtn.setImage(UIImage(named: "popupmenudark"), for: UIControl.State.normal)
//        popUpBtn.addTarget(self, action: #selector(onBarPopUpBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
//        popUpBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
//        popUpBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
//        popUpBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
//        let barBtn3 = UIBarButtonItem(customView: popUpBtn)
        
        navigationItem.setRightBarButtonItems([barButton2,barButton1], animated: true)
    }
    
    @objc func onSearch(sender: UIButton){
        self.navigateToFreemiumProductSearchVC()
    }
    
    @objc func onProductAdd(sender: UIButton){
        self.navigateToFreemiumProductAddViewController()
    }
    
//    @objc func onBarPopUpBtnTapped(sender : UIBarButtonItem){
//        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
//
//        myActionSheet.view.tintColor = UIColor.black
//
//        let damagedProductAction = UIAlertAction(title: LanguageManager.DamagedProduct, style: UIAlertAction.Style.default) { (action) in
//            self.navigateToDamagedProductListVC()
//        }
//
//        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
//
//            self.view.endEditing(true)
//        }
//
//        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
//
//        myActionSheet.addAction(damagedProductAction)
//        myActionSheet.addAction(cancelAction)
//
//        popOverForIpad(action: myActionSheet)
//
//        self.present(myActionSheet, animated: true, completion: nil)
//    }
    
    
    
//    func navigateToDamagedProductListVC(){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamagedProductListViewController") as! DamagedProductListViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
//        
//    }
    
    func navigateToFreemiumProductSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumProductSearchViewController") as! FreemiumProductSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToFreemiumSaleableViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumSaleableProductsVC") as! FreemiumSaleableProductsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToFreemiumProductAddViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumProductAddViewController") as! FreemiumProductAddViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ProductListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        getFreemiumOfflineData()
        refreshControl.endRefreshing()
    }
    
}

//Mark: Floaty Delegate
extension FreemiumProductListViewController: FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToFreemiumSaleableViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}


//MARK: CollectionView Delegate And Data Source
extension FreemiumProductListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(ProductListSummeryCell.nib, forCellWithReuseIdentifier: ProductListSummeryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let item = self.productListSummery, item.count > 0  else {
            return 0
        }
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProductListSummeryCell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductListSummeryCell.identifier, for: indexPath) as! ProductListSummeryCell
        guard let item = self.productListSummery, item.count > 0 else{
            return cell
        }
        
        cell.summeryTitle.text = item[indexPath.row % item.count].title
        cell.summeryValue.text = item[indexPath.row % item.count].value
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70.0)
    }
    
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.productListSummery, list.count > 0 else{
            return
        }
        let listCount = list.count
        
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//MARK: TableView Delegate and Data Source
extension FreemiumProductListViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableVIew(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductListCell.nib, forCellReuseIdentifier: ProductListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if self.productList.count > 0  {
//            return self.productList.count
//        }
//        return 0
        if self.freemiumProducts.count > 0 {
            return self.freemiumProducts.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductListCell = tableView.dequeueReusableCell(withIdentifier: ProductListCell.identifier, for: indexPath) as! ProductListCell
        cell.selectionStyle = .none
        if self.freemiumProducts.count > 0 {
            let product = self.freemiumProducts[indexPath.row]
            
            cell.freemiumProducts = product
            
            cell.selectBtn.isHidden = true
        }
        return cell
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.freemiumProducts.count > 0 {
            let listItem = self.freemiumProducts[indexPath.row]
            self.navigateToFreemiumProductDetailsBaseVC(product : listItem)
        }
    }
    
    func navigateToFreemiumProductDetailsBaseVC(product : FreemiumProducts){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumProductDetailsVC") as! FreemiumProductDetailsVC
        
        viewController.inventoryId = Int(product.inventoryId)
        viewController.productId = Int(product.productId)
        viewController.productName = product.name
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

// MARK: API Delegate
extension FreemiumProductListViewController : FreemiumProductListViewDelegate {
    
    func setFreemiumProductData(data: OfflineProductDataMapper) {
        self.newProductList = data.newProductList
        self.updatedProductList = data.updatedProductList
        self.deletedProductList = data.deletedProductList
        fetchData()
    }
    
    func setProductData(data: ProductListDataMapper) {
        guard let summeryList = data.summary else {
            return
        }
        self.productListSummery = summeryList
        self.setUpAutoScroll()
    }
    
    
    func onFailed(data: String) {
        showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getProductListDataFromServer(page: self.currentPage)
        self.getFreemiumOfflineData()
    }
    
    func getFreemiumOfflineData(){
        if let lUpdatedTime = getTimeStamp(){
            if lUpdatedTime.timeStamp == "default"{
                deleteAllData(entity: "FreemiumProducts")
                getOfflineProduct(timesStamp: lUpdatedTime.timeStamp ?? "")
            }else{
                getOfflineProduct(timesStamp: lUpdatedTime.timeStamp ?? "")
            }
            
        }
    }
    
    func getOfflineProduct(timesStamp: String){
        self.presenter.getFreemiumOfflineProductFromServer(timeStamp: timesStamp)
    }
}

extension FreemiumProductListViewController{
    func fetchData(){

        freemiumProducts.removeAll()
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FreemiumProducts")

        do {
            let results = try context.fetch(fetchRequest)
            let  products = results as! [FreemiumProducts]

            for product in products {
                freemiumProducts.append(product)
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }

    }
}



