//
//  ProductUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 7/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class ProductUpdateViewController: UIViewController {
    
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var nameLbl: UILabel!{
        didSet{
            nameLbl.text = LanguageManager.ProductName
        }
    }
    @IBOutlet weak var nameText: UITextField!{
        didSet{
            nameText.placeholder = LanguageManager.ProductName
        }
    }
    @IBOutlet weak var categoryLbl: UILabel!{
        didSet{
            categoryLbl.text = LanguageManager.Category
        }
    }
    @IBOutlet weak var categoryText: UITextField!{
        didSet{
            categoryText.placeholder = LanguageManager.Category
        }
    }
    @IBOutlet weak var companyLbl: UILabel!{
        didSet{
            companyLbl.text = LanguageManager.Company
        }
    }
    @IBOutlet weak var companyText: UITextField!{
        didSet{
            companyText.placeholder =  LanguageManager.Company
        }
    }
    @IBOutlet weak var varientLbl: UILabel!{
        didSet{
            varientLbl.text = LanguageManager.Varient
        }
    }
    @IBOutlet weak var varientText: UITextField!{
        didSet{
            varientText.placeholder =  LanguageManager.Varient
        }
    }
    @IBOutlet weak var unitLbl: UILabel!{
        didSet{
            unitLbl.text = LanguageManager.Unit
        }
    }
    @IBOutlet weak var unitText: UITextField!{
        didSet{
            unitText.placeholder = LanguageManager.Unit
        }
    }
    @IBOutlet weak var adviseLbl: UILabel!{
        didSet{
            adviseLbl.text = LanguageManager.QuantityAlert
        }
    }
    @IBOutlet weak var adviseText: UITextField!
    @IBOutlet weak var unitTitleLbl: UILabel!{
        didSet{
            unitTitleLbl.text = LanguageManager.Unit
        }
    }
    @IBOutlet weak var productCodeLbl: UILabel!{
        didSet{
            productCodeLbl.text = LanguageManager.ProductCode
        }
    }
    @IBOutlet weak var skuText: UITextField!{
        didSet{
            skuText.placeholder = LanguageManager.ProductCode
        }
    }
    
    @IBOutlet weak var categoryAddBtn: UIButton!{
        didSet{
            categoryAddBtn.setTitle(LanguageManager.NewCategoryAdd, for: .normal)
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = ProductUpdatePresenter(service: ProductService())
    
    var productInfo : Product? 
    var productId : Int?
   // var newCategory = ProductCategories(id: 0, name: "নতুন ক্যাটেগরি যোগ করুন")
    var categories : [ProductCategories] = []{
        didSet{
            self.categoryPicker.reloadAllComponents()
        }
    }
    
    var category : CategoryAdd?
    var categoryId: Int?
    var productUnit : [ProductUnit] = []{
        didSet{
            self.unitPicker.reloadAllComponents()
        }
    }
    var unitId: Int?
    var varient : String = ""
    var company : String = ""
    var productSku : String = ""
    
    var unitPicker = UIPickerView()
    var categoryPicker = UIPickerView()
    
    var currentPage: Int = 1
    
    let imagePicker = UIImagePickerController()
    var selectedImage : UIImage?
    var message : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetUp()
        self.attachPresenter()
        self.toolBarSetUp()
        self.setUpPickerView()
        self.setUpViews()
        self.setImageView()
    }
    
    func initialSetUp(){
        
        imagePicker.delegate = self
        self.imageIcon.addTapGestureRecognizer(action: {self.onTapOnImageView()})
        
        self.categoryAddBtn.addTarget(self, action: #selector(onCategoryAdd), for: .touchUpInside)
        
        self.nameText.underlined()
        self.categoryText.underlined()
        self.companyText.underlined()
        self.varientText.underlined()
        self.unitText.underlined()
        self.adviseText.underlined()
        self.skuText.underlined()
        self.navigationController?.navigationBar.topItem?.title = " "
        //self.newCategoryViewHeight.constant = 0.0
//        self.newCategoryLbl.isHidden = true
//        self.newCategoryText.isHidden = true
        
        guard let info = self.productInfo else{
            return
        }
        imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + info.image), placeholderImage: UIImage(named: "placeholder"))
        
        self.nameText.text = info.name
        self.categoryText.text = info.categoryName
        self.companyText.text = info.company
        self.company = info.company
        self.varient = info.variant
        self.varientText.text = info.variant
        self.unitText.text = info.unit
        self.skuText.text = info.sku
        self.productSku = info.sku
        self.productId = info.productId
        if let sAlert = info.stockAlert{
            self.adviseText.text = "\(sAlert)"
        }
        
        self.categoryId = info.categoryId
        self.unitId = info.unitId
        //self.unitId = s
        
        
    }
    
    //PickerView
    func setUpPickerView(){
        categoryPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        self.categoryText.inputView = categoryPicker
        self.categoryText.inputAccessoryView = toolBar
        
        //UnitPicker
        unitPicker.delegate = self
        
        let unitToolBar = UIToolbar()
        unitToolBar.barStyle = UIBarStyle.default
        unitToolBar.isTranslucent = true
        unitToolBar.tintColor = UIColor.black
        unitToolBar.sizeToFit()
        
        let unitDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnUnit(sender:)))
        
        unitToolBar.setItems([cancelButton,spaceButton, unitDoneButton], animated: false)
        unitToolBar.isUserInteractionEnabled = true
        
        
        self.unitText.inputView = unitPicker
        self.unitText.inputAccessoryView = unitToolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnUnit(sender : UIBarButtonItem){
        self.unitText.resignFirstResponder()
        self.adviseText.becomeFirstResponder()
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.categoryText.resignFirstResponder()
        self.companyText.becomeFirstResponder()
    }
    
    @objc func onCategoryAdd(sender: UIButton){
        self.setUpNewCategoryAddAlert()
    }
    
    func toolBarSetUp(){
        //amount toolBar
        let adviseAmountToolBar = UIToolbar()
        adviseAmountToolBar.barStyle = UIBarStyle.default
        adviseAmountToolBar.isTranslucent = true
        adviseAmountToolBar.tintColor = UIColor.black
        adviseAmountToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let amountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnAdviseAmount(sender:)))
        
        adviseAmountToolBar.setItems([cancelButton,spaceButton, amountDoneButton], animated: false)
        adviseAmountToolBar.isUserInteractionEnabled = true
        
        self.adviseText.inputAccessoryView = adviseAmountToolBar
        
    }
    
    @objc func onPressingDoneOnAdviseAmount(sender: UIBarButtonItem){
        self.adviseText.resignFirstResponder()
        self.skuText.becomeFirstResponder()
    }
    
    func setUpViews(){
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtn(sender:)), for: .touchUpInside)
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    @objc func onSubmitBtn(sender : UIButton){
        if self.isValidated(){
            
            self.company = companyText.text ?? ""
            self.varient = varientText.text ?? ""
            self.productSku = skuText.text ?? ""
            guard let id = self.productId, let name = nameText.text, let categoryId = self.categoryId, let unitId = self.unitId, let stockAlert = adviseText.text else{
                return
            }
            
            let params : [String : Any] = ["id": id , "name" :name, "category": categoryId,
                                           "company": company,
                                           "variant": varient,
                                           "unit": unitId,
                                           "stock_alert": stockAlert,
                                           "sku": productSku]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            self.presenter.postProductUpdateDataToServer(params: params)
        }
        
    }
    
    func isValidated()->Bool{
        if self.nameText.text == ""{
            showAlert(title: LanguageManager.ProductNameIsRequired, message: "")
            return false
        }
        else if self.categoryText.text == ""{
            showAlert(title: LanguageManager.CategoryIsRequired, message: "")
            return false
        }
//        else if self.companyText.text == ""{
//            showAlert(title: "Company can't be empty", message: "")
//            return false
//        }
//        else if self.varientText.text == ""{
//            showAlert(title: "Varient can't be empty", message: "")
//            return false
//        }
        else if self.unitText.text == ""{
            showAlert(title: LanguageManager.ProductUnitIsRequired, message: "")
            return false
        }
//        else if self.skuText.text == ""{
//            showAlert(title: "SKU can't be empty", message: "")
//            return false
//        }
        return true
    }
    
    
//    //Alert Message
//    func displayMessage(userMessage:String) -> Void {
//        DispatchQueue.main.async {
//            let alertController = UIAlertController(title: "Alert", message: userMessage, preferredStyle: .alert)
//            
//            let OKAction = UIAlertAction(title: "OK", style: .default){
//                (action:UIAlertAction!) in
//                DispatchQueue.main.async {
//                    self.dismiss(animated: true, completion: nil)
//                }
//            }
//            alertController.addAction(OKAction)
//            self.present(alertController, animated: true, completion: nil)
//            
//        }
//    }
    
    func setUpNewCategoryAddAlert(){
        let alertController = UIAlertController(title: LanguageManager.NewCategoryAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField(configurationHandler: {(_ textField: UITextField) -> Void in
            textField.placeholder = LanguageManager.CategoryName
            
        })
        
        let confirmAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            guard let name = alertController.textFields?[0].text else{
                return
            }
            self.categoryText.text = name
            //self.categoryId.
            //self.categoryText.resignFirstResponder()
            
            
            let param : [String : Any] = ["name": name , "parent" : 0]
            
            self.presenter.postProductCategoryAddDataToServer(data: param)
        })
        alertController.addAction(confirmAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
        })
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToProductDetailsVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsBaseViewController") as! ProductDetailsBaseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }

}

//Mark: PickerViewDelegate
extension ProductUpdateViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.categoryPicker{
            guard self.categories.count > 0 else {
                return 0
            }
            return self.categories.count
        }else if pickerView == self.unitPicker{
            guard self.productUnit.count > 0 else{
                return 0
            }
            return self.productUnit.count
        }else{
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.categoryPicker{
            guard self.categories.count > 0 else {
                return ""
            }
            let categoryItem = self.categories[row]
            //self.categoryText.text = categoryItem.name
            self.categoryId = categoryItem.id
            return categoryItem.name
        }else if pickerView == self.unitPicker{
            guard self.productUnit.count > 0 else{
                return ""
            }
            let unitItem = self.productUnit[row]
            //self.unitText.text = unitItem.name
            self.unitId = unitItem.id
            return unitItem.name
        }else{
            return ""
        }
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.categoryPicker{
            guard self.categories.count > 0 else {
                return
            }
            let categoryItem = self.categories[row]
            self.categoryText.text = categoryItem.name
            self.categoryId = categoryItem.id
        }else if pickerView == self.unitPicker{
            guard self.productUnit.count > 0 else{
                return
            }
            let unitItem = self.productUnit[row]
            self.unitText.text = unitItem.name
            self.unitId = unitItem.id
        }else{
            return
        }
        
    }
    
}


//Mark: Api Delegate
extension ProductUpdateViewController: ProductUpdateViewDelegate{
    func onProductCategoryAdd(data: CategoryAddDataMapper) {
        guard let cate = data.category else {
            return
        }
        self.category = cate
        self.categoryId = self.category?.id
    }
    
    func setProductCategoryData(data: ProductCategoryListDataMapper) {
        guard let category = data.categories, category.count > 0 else {
            return
        }
        self.categories += category
    }
    
    func setProductUnitData(data: ProductUnitDataMapper) {
        guard let unit = data.productUnit, unit.count > 0 else {
            return
        }
        self.productUnit = unit
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.message = message
        guard let id = self.productId, let image = self.selectedImage else{
            self.successMessage(userMessage: message)
            return
        }
        self.presenter.uploadImage(productId : id, image: image)
    }
    
    func onImageUpload(data: AddDataMapper) {
        self.successMessage(userMessage: self.message ?? "")
    }
    
    func setFreemiumProductData(data: OfflineProductDataMapper) {
        //This one is empty
    }
    
    func onFailed(data: String) {
        self.displayMessage(userMessage: data)
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getProductCategoryDataFromServer(page: self.currentPage)
        self.presenter.getProductUnitDataFromServer()
    }
}

extension ProductUpdateViewController{
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: userMessage, message: "", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

extension ProductUpdateViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    private func setImageView(){
        guard let imageUrl = self.productInfo?.image else{
            self.setImageInImageView(imageUrl: "")
            return
        }
        
        self.setImageInImageView(imageUrl: ImageUrl.sharedImageInstance.productImage + imageUrl)
    }
    
    func setImageInImageView(imageUrl : String){
        self.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageIcon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
    }
    
    func onTapOnImageView(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageIcon.contentMode = .scaleAspectFill
            imageIcon.image = pickedImage
            self.selectedImage = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


