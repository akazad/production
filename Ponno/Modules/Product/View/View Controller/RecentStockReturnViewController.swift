//
//  RecentStockReturnViewController.swift
//  Ponno
//
//  Created by a k azad on 3/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class RecentStockReturnViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var productNameLbl: UILabel!{
        didSet{
            productNameLbl.text = LanguageManager.ProductName
        }
    }
    @IBOutlet weak var nameTextField: UITextField!{
        didSet{
            nameTextField.placeholder = LanguageManager.ProductName
        }
    }
    @IBOutlet weak var quantityLbl: UILabel!{
        didSet{
            quantityLbl.text = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var quantityTextField: UITextField!{
        didSet{
            quantityTextField.placeholder = LanguageManager.Quantity
        }
    }
    @IBOutlet weak var serialNumberLbl: UILabel!{
        didSet{
            serialNumberLbl.text = LanguageManager.SelectSerialNumber
        }
    }
    @IBOutlet weak var serialLblHeight: NSLayoutConstraint!
    @IBOutlet weak var serialTableHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cashBackLbl: UILabel!{
        didSet{
            cashBackLbl.text = LanguageManager.CashBack
        }
    }
    @IBOutlet weak var cashBackTextField: UITextField!{
        didSet{
            cashBackTextField.placeholder = LanguageManager.CashBack
        }
    }
    @IBOutlet weak var totalLbl: UILabel!{
        didSet{
            totalLbl.text = LanguageManager.Total
        }
    }
    @IBOutlet weak var totalTextField: UITextField!{
        didSet{
            totalTextField.text = LanguageManager.Total
        }
    }
    @IBOutlet weak var vendorLbl: UILabel!{
        didSet{
            vendorLbl.text = LanguageManager.Vendor
        }
    }
    @IBOutlet weak var vendorsTextField: UITextField!{
        didSet{
            vendorsTextField.placeholder = LanguageManager.SelectOne
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = RecentStockReturnPresenter(service: ProductService())
    
    var recentStock : ProductRecentPurchase?{
        didSet{
            guard let stock = recentStock else {
                return
            }
            if  stock.serials == ""{
                self.hasSerial = 0
            }
            else{
                self.hasSerial = 1
            }
        }
    }
    
    var vendorPicker = UIPickerView()
    var vendorsList : [VendorsList] = []{
        didSet{
            self.vendorPicker.reloadAllComponents()
        }
    }
    var vendorId : Int?
    var vendorName : String?
    var productId: Int?
    var inventoryId: Int?
    var productName: String?
    var serialList : [String]?
    var selectedSerialList : [String]?
    var productSerials : [String] = []
    var hasSerial : Int?
    
    let screenHeight = UIScreen.main.bounds.height
    let scrollViewContentHeight = 780 as CGFloat

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetUp()
        self.setUpPickerView()
        self.configureTextFields()
        self.setUpToolBar()
        self.attachPresenter()
        //self.configureTableView()
    }
    
    func initialSetUp(){
        
        self.title = LanguageManager.ProductInfo
        
        self.nameTextField.isEnabled = false
        self.totalTextField.isEnabled = false
        guard let recentPurchase = self.recentStock, let name = self.vendorName else { 
            return
        }
        self.nameTextField.text = self.productName
        self.quantityTextField.text = recentPurchase.currentQuantity
        self.inventoryId = recentPurchase.id
        self.vendorsTextField.text = name
        self.vendorId = recentPurchase.vendorId
//        self.inventoryId = recentPurchase.
      
        if hasSerial == 1 {
            self.quantityTextField.isEnabled = false
            if self.prepareSerialNumbers(serials: recentPurchase.serials).count > 0{
                self.serialList = self.prepareSerialNumbers(serials: recentPurchase.serials)
                self.selectedSerialList = self.serialList
                self.configureTableView()
            }
        }
        else{
            self.serialLblHeight.constant = 0.0
            self.serialTableHeight.constant = 0.0
        }
        self.cashBackTextField.text = recentPurchase.buyingPrice
        
        let buyingPrice = Double(recentPurchase.buyingPrice) ?? 0.0
        let quantity = Double(recentPurchase.quantity) ?? 0.0
        
        let totalAmouont = buyingPrice * quantity
        
        self.totalTextField.text = "\(totalAmouont)"

        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        self.quantityTextField.addTarget(self, action: #selector(quantityTextFieldDidChange), for: .editingChanged)
        self.cashBackTextField.addTarget(self, action: #selector(cashBackTextFieldDidChange), for: .editingChanged)
    }
    
    fileprivate func refreshSerialPriceView(){
        if self.productSerials.count >= 0 {
            self.quantityTextField.text = "\(self.productSerials.count)"
            
            guard let recentPurchase = self.recentStock else {
                return
            }
            
            let quantity = Double(self.productSerials.count)
            
            let buyingPrice = Double(recentPurchase.buyingPrice) ?? 0.0
            
            self.totalTextField.text = "\(quantity * buyingPrice)"
        }
    }
    
//    fileprivate func prepareSerialNumbers(serials : String)->[String]{
//        guard serials != "" else {
//            return []
//        }
//        
//        let serialList = serials.components(separatedBy: ",")
//        
//        guard serialList.count > 0 else {
//            return []
//        }
//        self.productSerials = serialList
//        return serialList
//        
//    }
    
    fileprivate func getString(array : [String]) -> String{
        let stringArray = array.map{ String ($0)}
        return stringArray.joined(separator: ",")
    }
    
    
    @objc func quantityTextFieldDidChange(sender: UITextField){
        if self.quantityTextField.text != "" {
            self.refreshView()
        }
    }
    
    @objc func cashBackTextFieldDidChange(sender: UITextField){
        if self.cashBackTextField.text != "" {
            self.refreshView()
        }
    }
    
    func setUpToolBar(){
        
        //quantityToolBaar
        let quantityToolBar = UIToolbar()
        quantityToolBar.barStyle = UIBarStyle.default
        quantityToolBar.isTranslucent = true
        quantityToolBar.tintColor = UIColor.black
        quantityToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnQuantity(sender:)))
        
        quantityToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        quantityToolBar.isUserInteractionEnabled = true
        
        self.quantityTextField.inputAccessoryView = quantityToolBar
        
        //DeliveryDueAmountToolBaar
        let cashBackToolBar = UIToolbar()
        cashBackToolBar.barStyle = UIBarStyle.default
        cashBackToolBar.isTranslucent = true
        cashBackToolBar.tintColor = UIColor.black
        cashBackToolBar.sizeToFit()
        
        let cashBackDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCashBack(sender:)))
        
        cashBackToolBar.setItems([cancelButton, spaceButton, cashBackDoneButton], animated: false)
        cashBackToolBar.isUserInteractionEnabled = true
        
        self.cashBackTextField.inputAccessoryView = cashBackToolBar
    }
    
    @objc func onPressingDoneOnQuantity(sender: UIBarButtonItem){
        self.cashBackTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnCashBack(sender: UIBarButtonItem){
        self.vendorsTextField.becomeFirstResponder()
    }
    
    func scrollViewSetup(){
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: scrollViewContentHeight)
        scrollView.delegate = self
        scrollView.bounces = false
        tableView.bounces = false
        tableView.isScrollEnabled = false
    }
    
    //Mark: PickerView
    func setUpPickerView(){
        vendorPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        self.vendorsTextField.inputView = vendorPicker
        self.vendorsTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.vendorsTextField.resignFirstResponder()
        self.submitBtn.becomeFirstResponder()
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let quantityText = self.quantityTextField.text, let quantity = Double(quantityText), let buyingPrice = self.cashBackTextField.text, let totalPrice = self.totalTextField.text, let inventoryId = self.inventoryId, let productId = self.productId, let vendorId = self.vendorId else{
                return
            }
            
            let serials = getString(array: self.productSerials)
            
            guard let recentPurchase = self.recentStock else {
                return
            }
            let currentQuantity = recentPurchase.currentQuantity
            let quan = Double(currentQuantity)
            
            guard let cquan = quan else{
                return
            }
            
            if cquan <= 0.0 {
                self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
            }else{
                let param : [String: Any] = ["inventory_history_id": inventoryId, "product_id" : productId, "quantity": quantity, "buying_price": buyingPrice, "total": totalPrice, "serial": serials, "vendor_id": vendorId]
                
                if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
                    print(json)
                }
                self.presenter.postProductRecentStockReturnDataToServer(param: param)
            }
        }
    }
    
    func isValidated()->Bool{
        
        if let quantityText = self.quantityTextField.text, let quantity = Double(quantityText), let quantityToCheck = recentStock?.quantity, quantity > Double(quantityToCheck)!{
            self.displayMessage(userMessage: LanguageManager.YouCantRetrunMoreThan + " \(quantityToCheck) ")
            return false
        }
        else if let buyingPriceText = self.cashBackTextField.text, let buyingPrice = Double(buyingPriceText), buyingPrice <= 0 {
            self.displayMessage(userMessage: LanguageManager.PricecannotBe0OrLess)
            return false
        }
        else if (self.vendorsTextField.text?.isEmpty)! {
            self.displayMessage(userMessage: LanguageManager.SelectAVendorToReturnProduct)
            return false
        }else{
            return true
        }
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.popToSpecificViewController()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is ProductDetailsBaseViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }

}

//Mark: TableView Delegate And DataSource
extension RecentStockReturnViewController : UITableViewDelegate, UITableViewDataSource {
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductSerialCell.nib, forCellReuseIdentifier: ProductSerialCell.identifier)
        self.tableView.separatorColor = .none
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 30
        self.refreshTableView()
        self.tableView.allowsMultipleSelection = true
        self.tableView.allowsMultipleSelectionDuringEditing = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let list = self.serialList, list.count > 0 else {
            return 0
        }
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductSerialCell = tableView.dequeueReusableCell(withIdentifier: ProductSerialCell.identifier, for: indexPath) as! ProductSerialCell
        guard let list = self.serialList, list.count > 0 else{
            return cell
        }
        let serial = list[indexPath.row]
        cell.serialNumber.text = serial
        
        
        if self.productSerials.contains(serial){
            cell.checkIcon.image = UIImage(named : "check_icon")
        }
        else{
            cell.checkIcon.image = UIImage(named: "uncheck_icon")
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let list = self.serialList, list.count > 0 else{
            return
        }
        let serial = list[indexPath.row]
        
        if self.productSerials.contains(serial){
           self.productSerials = self.productSerials.filter({ $0 != serial})
        }
        
        else {
            self.productSerials.append(serial)
        }
        self.refreshSerialPriceView()
        self.refreshTableView()
        
    }
    
    func refreshTableView(){
        self.tableView.reloadData {
            if self.tableView.contentSize.height >= 200{
                self.serialTableHeight.constant = 200
            }
            else {
                self.serialTableHeight.constant = self.tableView.contentSize.height
            }
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        
        if scrollView == self.scrollView {
            if yOffset >= scrollViewContentHeight - screenHeight {
                scrollView.isScrollEnabled = false
                tableView.isScrollEnabled = true
            }
        }
        
        if scrollView == self.tableView {
            if yOffset <= 0 {
                self.scrollView.isScrollEnabled = true
                self.tableView.isScrollEnabled = false
            }
        }
    }
    
    
}

extension RecentStockReturnViewController : UITextFieldDelegate{
    func configureTextFields(){
        self.nameTextField.delegate = self
        self.quantityTextField.delegate = self
        self.cashBackTextField.delegate = self
        self.totalTextField.delegate = self
        self.vendorsTextField.delegate = self
        self.nameTextField.underlined()
        self.quantityTextField.underlined()
        self.totalTextField.underlined()
        self.cashBackTextField.underlined()
        self.vendorsTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            self.quantityTextField.becomeFirstResponder()
        }else if textField == vendorsTextField{
            self.submitBtn.becomeFirstResponder()
        }else{
            self.view.endEditing(true)
        }
        return false
    }
}

//Mark: PickerViewDelegate
extension RecentStockReturnViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        guard self.vendorsList.count > 0 else {
            return 0
        }
        return self.vendorsList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard self.vendorsList.count > 0 else {
            return ""
        }
        let vendors = self.vendorsList[row]
        self.vendorsTextField.text = vendors.name
        self.vendorId = vendors.id
        return vendors.name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard self.vendorsList.count > 0 else {
            return
        }
        let vendors = self.vendorsList[row]
        self.vendorsTextField.text = vendors.name
        self.vendorId = vendors.id
    }
}

//Mark: Api Delegate
extension RecentStockReturnViewController: RecentStockReturnViewDelegate{
    func setVendorsList(data: VendorsDataMapper) {
        guard let list = data.vendors, list.count > 0 else {
            return
        }
        self.vendorsList = list
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.displayMessage(userMessage: data)
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getVendorsListDataFromServer(getAll: true)
    }
}

extension RecentStockReturnViewController{
    func refreshView(){
        guard let quantityText = self.quantityTextField.text, let quantity = Double(quantityText) else {
            return
        }
        guard let buyingPriceText = self.cashBackTextField.text, let buyingPrice = Double(buyingPriceText) else {
            return
        }
        
        let totalPrice = quantity * buyingPrice
        self.totalTextField.text = "\(totalPrice)"
    }
}
