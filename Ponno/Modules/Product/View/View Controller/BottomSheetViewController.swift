//
//  BottomSheetViewController.swift
//  NestedScrollView
//
//  Created by ugur on 12.08.2018.
//  Copyright © 2018 me. All rights reserved.
//

import UIKit
import SVProgressHUD

enum SheetLevel{
    case top, bottom, middle
}

enum SegmentIndex : Int {
    case sell, stock, serial
}

protocol BottomSheetDelegate {
    func updateBottomSheet(frame: CGRect)
}

class BottomSheetViewController: UIViewController{
    
    @IBOutlet weak var emptyMessage: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet var panView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = ProductPanViewPresenter(service: ProductService())

    var productId : Int?
    var product: Product?
    var recentPurchase : [ProductRecentPurchase] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var recentSale : [ProductRecentSales] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var serial : [ProductSerial] = [] {
        didSet{
            self.refreshTableView()
        }
    }
    //  Segment Propertise
    var isFirst = true
    var selectedSegment : Int?
    var hasSerial : Int?
    var productName: String?
    
    //  Pagination Propertise
    var isLoading : Bool = false
    var currentPage : Int = 1
    var saleCurrentPage : Int = 1
    var serialCurrentPage : Int = 1
    var lastPageNo: Int = 0
    
    var recentPurchaseId: Int?
    var serialId: Int?
    var serialNumber: String?
    
    var inventoryId : Int?
    
    var lastY: CGFloat = 0
    var pan: UIPanGestureRecognizer!
    
    var bottomSheetDelegate: BottomSheetDelegate?
    var parentView: UIView!
    
    var initalFrame: CGRect!
    var topY: CGFloat = 80 //change this in viewWillAppear for top position
    var middleY: CGFloat = 400 //change this in viewWillAppear to decide if animate to top or bottom
    var bottomY: CGFloat = 600 //no need to change this
    let bottomOffset: CGFloat = 80 //sheet height on bottom position
    var lastLevel: SheetLevel = .bottom //choose inital position of the sheet
    
    var disableTableScroll = false
    
    //hack panOffset To prevent jump when goes from top to down
    var panOffset: CGFloat = 0
    var applyPanOffset = false
    
    //tableview variables
    var listItems: [Any] = []
    var headerItems: [Any] = []
    
//    var delegate : ProductDataUpdate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        pan.delegate = self
        self.panView.addGestureRecognizer(pan)

        self.tableView.panGestureRecognizer.addTarget(self, action: #selector(handlePan(_:)))
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap(_:)))
        tap.delegate = self
        
        tap.cancelsTouchesInView = false
        
        tableView.addGestureRecognizer(tap)
        self.configureTableView()
        self.initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initalFrame = UIScreen.main.bounds
        self.topY = round(initalFrame.height * 0.05)
        self.middleY = initalFrame.height * 0.6
        self.bottomY = initalFrame.height - bottomOffset
        self.lastY = self.middleY
        
        bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.middleY))
    }
    
    func initialSetUp(){
        //self.navigationController?.navigationBar.topItem?.title = " "
//        if self.hasSerial == 0 {
//            segmentedControl.removeSegment(at: 2, animated: false)
//        }
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    
    @IBAction func segmentedControllAction(_ sender: UISegmentedControl) {
        guard let id = self.productId else{
            return
        }
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            //self.selectedSegment = segmentedControl.selectedSegmentIndex
            //self.isFirst = true
            //self.selectedSegment = 0
            self.recentPurchase = []
            self.currentPage = 1
            self.isLoading = true
        self.presenter.getProductRecentPurchaseHistoryFromServer(page: self.currentPage, id: id)
            self.refreshTableView()
            
        case 1:
            //self.selectedSegment = segmentedControl.selectedSegmentIndex
            //self.isFirst = false
            //self.selectedSegment = 1
            self.currentPage = 1
            self.isLoading = true
            self.recentSale = []
            self.presenter.getProductRecentSaleHistoryFromServer(page: self.currentPage, id: id)
            self.refreshTableView()
            
        case 2:
            //self.selectedSegment = segmentedControl.selectedSegmentIndex
            self.isLoading = true
            self.serial = []
            self.presenter.getProductSerialNumberFromServer(page: self.currentPage, id: id)
            self.refreshTableView()
        default:
            break;
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == tableView else {return}
        
        if (self.parentView.frame.minY > topY){
            self.tableView.contentOffset.y = 0
        }
    }
    
    
    
    
    //this stops unintended tableview scrolling while animating to top
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard scrollView == tableView else {return}
        
        if disableTableScroll{
            targetContentOffset.pointee = scrollView.contentOffset
            disableTableScroll = false
        }
    }
    
    
    @objc func handleTap(_ recognizer: UITapGestureRecognizer){
        let p = recognizer.location(in: self.tableView)
        let index = tableView.indexPathForRow(at: p)
        if segmentedControl.selectedSegmentIndex == 1{
         if self.recentSale.count > 0{
            guard let salesIndex = index else {
                return
            }
            let saleItem = self.recentSale[salesIndex.row]
            
            self.navigateToSaleDetailsVC(iD: saleItem.invoiceId)
            }
        }

        tableView.selectRow(at: index, animated: false, scrollPosition: .none)
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let saleDetailsVC = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        saleDetailsVC.iD = iD  
        self.navigationController?.pushViewController(saleDetailsVC, animated: true)
    }
    
    
    @objc func handlePan(_ recognizer: UIPanGestureRecognizer){
        
        let dy = recognizer.translation(in: self.parentView).y
        switch recognizer.state {
        case .began:
            applyPanOffset = (self.tableView.contentOffset.y > 0)
        case .changed:
            if self.tableView.contentOffset.y > 0{
                panOffset = dy
                return
            }
            
            if self.tableView.contentOffset.y <= 0{
                if !applyPanOffset{panOffset = 0}
                let maxY = max(topY, lastY + dy - panOffset)
                let y = min(bottomY, maxY)
                //                self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: y)
                bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: y))
            }
            
            if self.parentView.frame.minY > topY{
                self.tableView.contentOffset.y = 0
            }
        case .failed, .ended, .cancelled:
            panOffset = 0
            
            if (self.tableView.contentOffset.y > 0){
                return
            }
            
            self.panView.isUserInteractionEnabled = false
            
            self.disableTableScroll = self.lastLevel != .top
            
            self.lastY = self.parentView.frame.minY
            self.lastLevel = self.nextLevel(recognizer: recognizer)
            
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.9, options: .curveEaseOut, animations: {
                
                switch self.lastLevel{
                case .top:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.topY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.topY))
                    self.tableView.contentInset.bottom = 50
                case .middle:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.middleY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.middleY))
                case .bottom:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.bottomY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.bottomY))
                }
                
            }) { (_) in
                self.panView.isUserInteractionEnabled = true
                self.lastY = self.parentView.frame.minY
            }
        default:
            break
        }
    }
    
    func nextLevel(recognizer: UIPanGestureRecognizer) -> SheetLevel{
        let y = self.lastY
        let velY = recognizer.velocity(in: self.view).y
        if velY < -200{
            return y > middleY ? .middle : .top
        }else if velY > 200{
            return y < (middleY + 1) ? .middle : .bottom
        }else{
            if y > middleY {
                return (y - middleY) < (bottomY - y) ? .middle : .bottom
            }else{
                return (y - topY) < (middleY - y) ? .top : .middle
            }
        }
    }
    
}

//Mark: TableView Delegate And DataSource
extension BottomSheetViewController: UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = .clear
        self.tableView.register(ProductDetailsRecentSellCell.nib, forCellReuseIdentifier: ProductDetailsRecentSellCell.identifier)
        self.tableView.register(ProductDetailsRecentStockCell.nib, forCellReuseIdentifier: ProductDetailsRecentStockCell.identifier)
        self.tableView.register(ProductDetailsSerialCell.nib, forCellReuseIdentifier: ProductDetailsSerialCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.emptyMessage.isHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl.selectedSegmentIndex == 0 {
            if self.recentPurchase.count > 0 {
                self.emptyMessage.isHidden = true
                return self.recentPurchase.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        else if segmentedControl.selectedSegmentIndex == 1 {
            if self.recentSale.count > 0 {
                self.emptyMessage.isHidden = true
                return self.recentSale.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        else if segmentedControl.selectedSegmentIndex == 2 {
            if self.serial.count > 0 {
                self.emptyMessage.isHidden = true
                return self.serial.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if segmentedControl.selectedSegmentIndex == 0 {
            let cell : ProductDetailsRecentStockCell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsRecentStockCell.identifier, for: indexPath) as! ProductDetailsRecentStockCell
            cell.selectionStyle = .none
            if self.recentPurchase.count > 0 {
                let item = self.recentPurchase[indexPath.row]
                
                cell.createdAtLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_h_mm_a.rawValue)
                cell.createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.currentQuantityLbl.text = "স্টকের পরিমাণ: " + "\(item.quantity)" + " " + Constants.pcsSymbol
                cell.stockPriceLabel.text = "ক্রয়মূল্য: " +  "\(item.buyingPrice)"
                cell.sellingPriceLabel.text = "বিক্রয়মূল্য: " + "\(item.sellingPrice)"
                cell.dealerNameLabel.text = " ডিলার/কোম্পানি নাম: " + item.name
                //                Pagination
                if isLoading == false && indexPath.row == self.recentPurchase.count - 1 && self.currentPage < self.lastPageNo{
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.productId else{
                        return cell
                    }
                self.presenter.getProductRecentPurchaseHistoryFromServer(page: self.currentPage, id: id)
                }
                return cell
            }
        }
        else if segmentedControl.selectedSegmentIndex == 1 {
            let cell : ProductDetailsRecentSellCell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsRecentSellCell.identifier, for: indexPath) as! ProductDetailsRecentSellCell
            cell.selectionStyle = .none
            if self.recentSale.count > 0 {
                let item = self.recentSale[indexPath.row]
                cell.createdAtLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_h_mm_a.rawValue)
                cell.createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.quantityLabel.text = "পরিমাণ:" + "\(item.quantity)" + " " + Constants.pcsSymbol
                cell.sellingPriceLabel.text = "বিক্রয়মূল্য:" + "\(item.totalPrice)" + " " + Constants.currencyPerUnitSymbol
                cell.invoiceLabel.text =  "রসিদ#: " + " " + item.invoice
                cell.invoiceLabel.textColor =  UIColor(red: 106/255, green: 150/255, blue: 44/255, alpha: 1/0)
                cell.customerLabel.text = "কাস্টমার: " + item.customerName
                if isLoading == false && indexPath.row == self.recentSale.count - 1 && self.saleCurrentPage < self.lastPageNo{
                    self.isLoading = true
                    self.saleCurrentPage += 1
                    guard let id = self.productId else{
                        return cell
                    }
                  
                    self.presenter.getProductRecentSaleHistoryFromServer(page: self.saleCurrentPage, id: id)
                }
                return cell
            }
        }
        else if segmentedControl.selectedSegmentIndex == 2 {
            let cell : ProductDetailsSerialCell = tableView.dequeueReusableCell(withIdentifier: ProductDetailsSerialCell.identifier, for: indexPath) as! ProductDetailsSerialCell
            cell.selectionStyle = .none
            if self.serial.count > 0 {
                let item = self.serial[indexPath.row]
                cell.dateLbl.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.dateLbl.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.serialLbl.text = item.serialNo
                if item.isSold == 0 {
                    cell.isSoldLbl.text = "বিক্রি হয়নি"
                }else{
                    cell.isSoldLbl.text = "বিক্রি হয়েছে"
                }
                
                if isLoading == false && indexPath.row == self.serial.count - 1 && self.serialCurrentPage < self.lastPageNo {
                    self.isLoading = true
                    self.serialCurrentPage += 1
                    guard let id = self.productId else{
                        return cell
                    }
                    self.presenter.getProductSerialNumberFromServer(page: self.serialCurrentPage, id: id)
                }
                
            }
            return cell
        }
        else{
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if segmentedControl.selectedSegmentIndex == 0 {
            let returnPurchase = UITableViewRowAction(style: .normal, title: "ফেরত") { action, index in
                if self.recentPurchase.count > 0 {
                    let recentPurchase = self.recentPurchase[indexPath.row]
                    self.recentPurchaseId = recentPurchase.id
                    self.navigateToRecentStockReturnVC(recentStock: recentPurchase)
                }
            }
            
            returnPurchase.backgroundColor = UIColor.orange
            
            let edit = UITableViewRowAction(style: .normal, title: "পরিবর্তন") { action, index in
                if self.recentPurchase.count > 0 {
                    let recentPurchase = self.recentPurchase[indexPath.row]
                    
                    self.navigateToRecentStockUpdateVC(recentStock: recentPurchase)
                }
            }
            
            edit.backgroundColor = UIColor.green
            
            let delete = UITableViewRowAction(style: .normal, title: "বাতিল") { action, index in
                
                if self.recentPurchase.count > 0{
                    let recentPurchase = self.recentPurchase[indexPath.row]
                    self.recentPurchaseId = recentPurchase.id
                    
                    guard let id = self.recentPurchaseId else{
                        return
                    }
                    self.confirmationMessage(userMessage: "আপনি কি নিশ্চিত?", deleteId: id)
                }
                
            }
            delete.backgroundColor = UIColor.red
            
            return [edit, returnPurchase, delete]
        }
        
        return []
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if segmentedControl.selectedSegmentIndex == 2{
            if self.serial.count > 0 {
                let serials = self.serial[indexPath.row]
                self.serialId = serials.id
                self.serialNumber = serials.serialNo
                self.alertWithTextField(serialId: self.serialId ?? 0, value: self.serialNumber ?? "")
            }
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToRecentStockReturnVC(recentStock: ProductRecentPurchase){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RecentStockReturnViewController") as! RecentStockReturnViewController
        //viewController.productInfo = product
        viewController.hasSerial = self.hasSerial
        viewController.productId = self.productId
        viewController.inventoryId = self.recentPurchaseId
        viewController.productName = self.productName
        viewController.recentStock = recentStock
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToRecentStockUpdateVC(recentStock: ProductRecentPurchase){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RecentStockUpdateViewController") as! RecentStockUpdateViewController
        //viewController.productInfo = product
//        guard let product = self.product else{
//            return
//        }
        
//        viewController.inventoryId = product.inventoryId
        guard let hasSerial = self.hasSerial else {
            return
        }
        viewController.hasSerial = hasSerial
        viewController.inventoryId = recentStock.id
        viewController.recentStock = recentStock
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: "Warning", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "Yes Delete!", style: .default){
                (action:UIAlertAction!) in
                
            self.presenter.postProductRecentStockDeleteDataFromServer(id: deleteId)
            self.navigationController?.popViewController(animated: true)
            }
            let cancelAction = UIAlertAction(title: "No", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: "Successful", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    //self.recentPurchase = []
                    self.refreshTableView()
                    self.currentPage = 1
                    guard let id = self.productId else{
                        return
                    }
                    self.presenter.getProductRecentPurchaseHistoryFromServer(page: self.currentPage, id: id)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func alertWithTextField(serialId: Int, value: String){
        let alertController = UIAlertController(title: "Serial Update", message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = "Enter New Serial"
            textField.textAlignment = .center
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: "Can't be empty", message: "")
                return
            }
            
            let param : [String : Any] = ["id": serialId, "value": text]
            self.presenter.postSerialUpdateDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}

extension BottomSheetViewController: UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

extension BottomSheetViewController : ProductPanViewDelegate{
    func setProductRecentSaleHistory(data: ProductRecentSaleDataMapper) {
        guard let list = data.sales, list.count > 0 else{
            return
        }
        self.isLoading = false
        self.recentSale += list
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setProductRecentPurchaseHistory(data: ProductRecentPurchaseDataMapper) {
        guard let list = data.purchases, list.count > 0 else{
            return
        }
        self.isLoading = false
        self.recentPurchase += list
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setProductSerialNumber(data: ProductSerialDataMapper) {
        guard let productSerial = data.serials else{
            return
        }
        self.isLoading = false
        self.serial += productSerial
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
        
    }
    
    func deleteRecentStockData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func serialUpdateData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
   
    func onFailed(data: String) {
        //self.showAlert(title: "কোন তথ্য পাওয়া যায়নি", message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self) 
        
        guard let id = self.productId else{
            return
        }
        self.isLoading = true
        self.presenter.getProductRecentPurchaseHistoryFromServer(page: self.currentPage, id: id)
    }
}

//extension BottomSheetViewController : ProductViewDelegate{
//    
//    func setProductRecentSaleHistory(data: ProductRecentSaleDataMapper) {
//        guard let list = data.sales, list.count > 0 else{
//            return
//        }
//        self.isLoading = false
//        self.recentSale += list
//        guard let pagination = data.pagination else {
//            return
//        }
//        self.lastPageNo = pagination.lastPageNo
//    }
//    
//    func setProductRecentPurchaseHistory(data: ProductRecentPurchaseDataMapper) {
//        guard let list = data.purchases, list.count > 0 else{
//            return
//        }
//        self.isLoading = false
//        self.recentPurchase += list
//        guard let pagination = data.pagination else {
//            return
//        }
//        self.lastPageNo = pagination.lastPageNo
//    }
//    
//    func setProductSerialNumber(data: ProductSerialDataMapper) {
//        guard let productSerial = data.serials else{
//            return
//        }
//        self.isLoading = false
//        self.serial += productSerial
//        guard let pagination = data.pagination else {
//            return
//        }
//        self.lastPageNo = pagination.lastPageNo
//        
//    }
//    
//    func deleteRecentStockData(data: AddDataMapper) {
//        guard let message = data.message else {
//            return
//        }
//        self.displayMessage(userMessage: message)
//    }
//    
//    func serialUpdateData(data: AddDataMapper) {
//        guard let message = data.message else { 
//            return
//        }
//        self.successMessage(userMessage: message)
//    }
//    
//    func setProductViewData(data: ProductDetailsDataMapper) {
//        
//    }
//    
//    func onFailed(data: String) {
//        self.showAlert(title: "কোন তথ্য পাওয়া যায়নি", message: "")
//    }
//    
//    func showLoading() {
//        self.showLoader()
//    }
//    
//    func hideLoading() {
//        self.hideLoader()
//    }
//    
//    func attachPresenter(){
//        self.presenter.attachView(viewDelegate: self)
//        
//        guard let id = self.productId else{
//            return
//        }
//        self.isLoading = true
//        self.presenter.getProductRecentPurchaseHistoryFromServer(page: self.currentPage, id: id)
//    }
//}

extension BottomSheetViewController{
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: "Successful", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in

            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
    }
}
