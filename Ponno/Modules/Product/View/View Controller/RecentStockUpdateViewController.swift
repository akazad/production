//
//  RecentStockUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 4/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class RecentStockUpdateViewController: UIViewController {
    
    @IBOutlet weak var buyingPriceLbl: UILabel!{
        didSet{
            buyingPriceLbl.text = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var sellingPriceTextField: UITextField!{
        didSet{
            sellingPriceTextField.placeholder = LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var warrentyLbl: UILabel!
    @IBOutlet weak var warrentyTextField: UITextField!
    @IBOutlet weak var warrentyLblHeight: NSLayoutConstraint!
    @IBOutlet weak var monthHeight: NSLayoutConstraint!
    @IBOutlet weak var warrentyTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var expiryDateLbl: UILabel!
    @IBOutlet weak var expiryDateLblHeight: NSLayoutConstraint!
    @IBOutlet weak var expiryDateTextField: UITextField!
    @IBOutlet weak var expiryDateTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = ProductRecentStockUpdatePresenter(service: ProductService())
    
    var recentStock : ProductRecentPurchase?
    var inventoryId: Int?
    var sellingPrice : String?
    
    var datePicker = UIDatePicker()
    var hasSerial : Int?
    
    var bottomSheetDelegate: BottomSheetDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetUp()
        self.showStartDatePicker()
        self.attachPresenter()
    }
    
    func initialSetUp(){
        
        self.title = LanguageManager.StockUpdate
        
        self.sellingPriceTextField.underlined()
        
        if self.hasSerial == 0{
            self.warrentyLblHeight.constant = 0.0
            self.warrentyTextFieldHeight.constant = 0.0
            self.monthHeight.constant = 0.0
            self.expiryDateLbl.text = LanguageManager.ExpireDate
            self.expiryDateTextField.underlined()
            self.expiryDateTextField.placeholder = "dd/mm/yyyy"
        }else{
            self.expiryDateLblHeight.constant = 0.0
            self.expiryDateTextFieldHeight.constant = 0.0
            self.warrentyLbl.text = LanguageManager.Warranty
            self.warrentyTextField.underlined()
        }
        
        
        
        guard let recentPurchase = self.recentStock else{
            return
        }
        self.sellingPriceTextField.text = recentPurchase.sellingPrice
        
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let sellingPrice = self.sellingPriceTextField.text, let id = self.inventoryId else{
                return
            }
            self.sellingPrice = sellingPrice
            
            guard let warrenty = self.warrentyTextField.text else{
                return
            }
            guard let expiryDate = self.expiryDateTextField.text else{
                return
            }
            
            let param : [String : Any] = ["inventory_history_id": id, "selling_price": sellingPrice, "warranty": warrenty, "expire_date": expiryDate]
            
        self.presenter.postProductRecentStockUpdateDataToServer(param: param)
        }
    }
    
    func isValidated()->Bool {
        if self.sellingPriceTextField.text == ""{
            self.displayMessage(userMessage: LanguageManager.SellingPriceIsRequired)
            return false
        }
//        if hasSerial == 0 {
//            if self.expiryDateTextField.text == "" {
//                self.displayMessage(userMessage: "মেয়াদ উত্তীর্ণ পূরণ করুণ")
//                return false
//            }
//        }else{
//            if self.warrentyTextField.text == "" {
//                self.displayMessage(userMessage:"ওয়ারেন্টি পূরণ করুণ")
//                return false
//            }
//        }
        return true
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
        
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    //self.navigateToProductVC()
                    //self.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}

extension RecentStockUpdateViewController {
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        //datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        expiryDateTextField.inputAccessoryView = toolbar
        expiryDateTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        expiryDateTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}

//Api Delegate
extension RecentStockUpdateViewController : ProductRecentStockUpdateViewDelegate{
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.displayMessage(userMessage: data)
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}
