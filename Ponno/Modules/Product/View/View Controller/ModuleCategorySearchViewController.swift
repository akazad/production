//
//  ModuleCategorySearchViewController.swift
//  Ponno
//
//  Created by a k azad on 23/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ModuleCategorySearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessage: UILabel!
    
    fileprivate var presenter = ModuleProductCategoryPresenter(service: ProductService())
    
    var moduleProductCategory: [ModuleProductCategoryList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))

    var timer : Timer?
    var searchText = ""
    
    var currentPage : Int = 1
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    
    var categoryId: Int?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
        self.setUpSearchBar()
        self.configureTableView()
        self.attachPresenter()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.setNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    func initialSetup(){
        self.emptyMessage.isHidden = true
        self.emptyMessage.text = LanguageManager.NoInformationFound
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.getModuleProductCategorySearchDataFromServer(page: self.currentPage, searchText: "")
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ModuleCategoryListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.moduleProductCategory = []
        self.currentPage = 1
        self.presenter.getModuleProductCategoryFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }
    

}

//Mark: Search Delegate
extension ModuleCategorySearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.Search
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        if self.searchText.count > 2 || self.searchText.count == 0{
            timer?.invalidate()
            
            timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
        }
        
        
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.moduleProductCategory = []
        self.isLoading = true
        self.isSearchActive = true
        self.presenter.getModuleProductCategorySearchDataFromServer(page: self.currentPage, searchText: self.searchText.lowercased())
        
    }
}

//Mark: TableViewDelegate and DataSource
extension ModuleCategorySearchViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ModuleProductCategoryCell.nib, forCellReuseIdentifier: ModuleProductCategoryCell.identifier)
        self.tableView.estimatedRowHeight = 60.0
        self.tableView.separatorStyle = .none
        self.emptyMessage.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.moduleProductCategory.count > 0 {
            self.emptyMessage.isHidden = true
            return self.moduleProductCategory.count
        }else{
            self.emptyMessage.isHidden = false
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ModuleProductCategoryCell = tableView.dequeueReusableCell(withIdentifier: ModuleProductCategoryCell.identifier, for: indexPath) as! ModuleProductCategoryCell
        
        cell.selectionStyle = .none
        
        let item = self.moduleProductCategory[indexPath.row]
        
        cell.categoryNameLbl.text = item.name
        cell.categoryPathLbl.text = item.path
        
        if let id = item.id{
            cell.popUpBtn.tag = id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped(sender:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.moduleProductCategory.count > 0{
            self.categoryId = sender.tag
            for category in self.moduleProductCategory{
                if self.categoryId == category.id{
                    guard let id = self.categoryId, let categoryName = category.name else{
                        return
                    }
                    
                    let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.updateAlertWithTextField(id: id, catgoryName: categoryName)
                    }
                    
                    let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        self.deleteMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                    }
                    
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    // add action buttons to action sheet
                    myActionSheet.addAction(updateAction)
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        // present the action sheet
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func deleteMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Delete, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            self.presenter.postCategoryDeleteDataToServer(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func updateAlertWithTextField(id: Int, catgoryName: String){
        let alertController = UIAlertController(title: LanguageManager.CategoryUpdate, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.text = catgoryName
            textField.textAlignment = .center
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.NameIsRequired, message: "")
                return
            }
            
            if let id = self.categoryId{
                let param : [String : Any] = ["id": id, "name": textField.text!]

                self.presenter.postCategoryUpdateDataToServer(param: param)
            }
            
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}


extension ModuleCategorySearchViewController: ModuleProductCategoryListViewDelegate{
    func setProductCategoryData(products: ModuleProductCategoryDataMapper) {
        guard let category = products.moduleProductCategoryList, category.count > 0 else{
            return
        }
        self.moduleProductCategory = category
    }
    
    func onCategoryUpdate(data: AddDataMapper){
        guard let message = data.message else{
            return
        }
        self.toastMessage(userMessage: message)
    }
    
    func onCategoryDelete(data: AddDataMapper){
        guard let message = data.message else{
            return
        }
        self.toastMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.emptyMessage.isHidden = false
    }
    
    func showLoading() {
        self.emptyMessage.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.emptyMessage.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getModuleProductCategoryFromServer(page: self.currentPage)
    }
}

extension ModuleCategorySearchViewController{
    func toastMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.getModuleProductCategoryFromServer(page: self.currentPage)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}
