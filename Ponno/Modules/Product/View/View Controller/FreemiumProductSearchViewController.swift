//
//  FreemiumProductSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 19/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import CoreData

class FreemiumProductSearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyMessage: UILabel!{
        didSet{
            emptyMessage.text = LanguageManager.NoInformationFound
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))

    var timer : Timer?
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var searchText = ""
    var productId : Int?
    
    var freemiumProducts : [FreemiumProducts] = []{
        didSet{
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setUpSearchBar()
        self.configureTableVIew()
        fetchData(searchText: "")
        //self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.setNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    //self.presenter.getProductSearchDataFromServer(page: self.currentPage, searchString: "")
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    

}

//Mark: Search Delegate
extension FreemiumProductSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.ProductSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
//    func navigateToProductSearchVC(){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductSearchViewController") as! ProductSearchViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
//
//    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        if self.searchText.count > 2 || self.searchText.count == 0{
            timer?.invalidate()
            
            timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
        }
        
        
    }
    
    @objc func onSearch(){
        self.isSearchActive = true
        fetchData(searchText: self.searchText.lowercased())
    }
}

//MARK: TableView Delegate and Data Source
extension FreemiumProductSearchViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableVIew(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductListCell.nib, forCellReuseIdentifier: ProductListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
        self.emptyMessage.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.freemiumProducts.count > 0 {
            self.emptyMessage.isHidden = true
            return self.freemiumProducts.count
        }
        self.emptyMessage.isHidden = false
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductListCell = tableView.dequeueReusableCell(withIdentifier: ProductListCell.identifier, for: indexPath) as! ProductListCell
        cell.selectionStyle = .none
        if self.freemiumProducts.count > 0 {
            let product = self.freemiumProducts[indexPath.row]
            
            cell.freemiumProducts = product
            
            cell.selectBtn.isHidden = true
        }
        return cell
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.freemiumProducts.count > 0 {
            let listItem = self.freemiumProducts[indexPath.row]
            self.navigateToFreemiumProductDetailsBaseVC(product : listItem)
        }
    }
    
    func navigateToFreemiumProductDetailsBaseVC(product : FreemiumProducts){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumProductDetailsVC") as! FreemiumProductDetailsVC
        
        viewController.inventoryId = Int(product.inventoryId)
        viewController.productId = Int(product.productId)
        viewController.productName = product.name
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

extension FreemiumProductSearchViewController{
    func fetchData(searchText: String){

        freemiumProducts.removeAll()
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FreemiumProducts")
        
        if searchText != ""{
            //fetchRequest.predicate = NSPredicate(format: "name contains[c] %@", searchText)
            let productNamePredicate = NSPredicate(format: "name contains[c] %@", searchText)
            let categoryNamePredicate = NSPredicate(format: "categoryName contains[c] %@", searchText)
            //fetchRequest.predicate = NSPredicate(format: "categoryName contains[c] %@", searchText)
            //fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [productNamePredicate, categoryNamePredicate])
            //let compoundPredicate : NSCompoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [productNamePredicate, categoryNamePredicate])
            //fetchRequest.predicate = compoundPredicate
            //fetchRequest.predicate = NSPredicate(format: "name contains[c] %@ OR categoryName contains[c] %@", searchText)
            let compoundPredicate = NSCompoundPredicate(type: .or, subpredicates: [productNamePredicate,categoryNamePredicate])
            fetchRequest.predicate = compoundPredicate
        }

        do {
            let results = try context.fetch(fetchRequest)
            let  products = results as! [FreemiumProducts]

            for product in products {
                freemiumProducts.append(product)
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }

    }
}
