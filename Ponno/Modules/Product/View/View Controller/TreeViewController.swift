//
//  ViewController.swift
//  TreeView1
//
//  Created by Cindy Oakes on 5/19/16.
//  Copyright © 2016 Cindy Oakes. All rights reserved.
//

import UIKit

class TreeViewController: UIViewController{
    
    fileprivate var presenter = ModuleProductCategoryPresenter(service: ProductService())
    
    var moduleProductCategory: [ModuleProductCategoryList]?{
        didSet{
         resetAllRecords(in : "ProductCategory")
            if let productCategory = moduleProductCategory{
                for item in productCategory{
                    if let gen = item.gen, let id = item.id, let name = item.name, let parent = item.parent{
                        insertProductCategory(id: id, name: name, gen: gen, parent: parent)
                    }
                    
                }
            }
        }
    }
    
    //MARK: Properties & Variables
    
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var displayArray = [TreeViewNode]()
    var indentation: Int = 0
    var nodes: [TreeViewNode] = []
    var data: [TreeViewData] = []
        
    //MARK:  Init & Load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.configureTableView()
        attachPresenter()
        
        NotificationCenter.default.addObserver(self, selector: #selector(TreeViewController.ExpandCollapseNode(notification:)), name: NSNotification.Name(rawValue: "TreeNodeButtonClicked"), object: nil)
        
        data = TreeViewLists.LoadInitialData()
        
        nodes = TreeViewLists.LoadInitialNodes(dataList: data)
        
        self.LoadDisplayArray()
        self.tableView.reloadData()
        
    }
    
    func initialSetup(){
        self.setUpInitialLanguage()
        self.title = LanguageManager.Category
        self.categoryLbl.text = LanguageManager.CategoryAndSubCategory
    }
    
    //MARK:  Node/Data Functions
    
    @objc func ExpandCollapseNode(notification: NSNotification)
    {
        self.LoadDisplayArray()
        self.tableView.reloadData()
    }
    
    
    func LoadDisplayArray()
    {
        self.displayArray = [TreeViewNode]()
        for node: TreeViewNode in nodes
        {
            self.displayArray.append(node)
            if (node.isExpanded == GlobalVariables.TRUE)
            {
                if let nChildren = node.nodeChildren{
                    self.AddChildrenArray(childrenArray: nChildren as! [TreeViewNode])
                }
                
            }
        }
    }
    
    func AddChildrenArray(childrenArray: [TreeViewNode])
    {
        for node: TreeViewNode in childrenArray
        {
            self.displayArray.append(node)
            if (node.isExpanded == GlobalVariables.TRUE )
            {
                if (node.nodeChildren != nil)
                {
                    self.AddChildrenArray(childrenArray: node.nodeChildren as! [TreeViewNode])
                }
            }
        }
    }
    
}

extension TreeViewController :  UITableViewDelegate, UITableViewDataSource{
    //MARK:  Table View Methods
       
       func configureTableView(){
           self.tableView.delegate = self
           self.tableView.dataSource = self
           self.tableView.register(TreeCell.nib, forCellReuseIdentifier: TreeCell.identifier)
        self.tableView.separatorStyle = .none
       }
       
       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return section == 0 ? 1.0 : 60
       }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return displayArray.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let node: TreeViewNode = self.displayArray[indexPath.row]
           
           let cell : TreeCell = tableView.dequeueReusableCell(withIdentifier: TreeCell.identifier, for: indexPath) as! TreeCell
           cell.selectionStyle = .none
           
           cell.treeNode = node
           cell.treeLabel.text = node.nodeObject as! String?
           
           if (node.isExpanded == GlobalVariables.TRUE)
           {
               cell.setTheButtonBackgroundImage(backgroundImage: UIImage(named: "whiteOpen")!)
           }
           else
           {
               cell.setTheButtonBackgroundImage(backgroundImage: UIImage(named: "whiteClose")!)
           }
           
           cell.setNeedsDisplay()
           
           return cell
       }
}

extension TreeViewController: ModuleProductCategoryListViewDelegate{
    func setProductCategoryData(products: ModuleProductCategoryDataMapper) {
        guard let category = products.moduleProductCategoryList, category.count > 0 else{
            return
        }
        self.moduleProductCategory = category
        
    }
    
    func onCategoryUpdate(data: AddDataMapper){
        //
    }
    
    func onCategoryDelete(data: AddDataMapper){
        //
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getAllModuleProductCategoryFromServer(getAll: true)
    }
}
