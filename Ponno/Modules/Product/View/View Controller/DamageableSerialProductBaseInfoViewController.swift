//
//  DamageableSerialProductBaseInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 25/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DamageableSerialProductBaseInfoViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var serialLbl: UILabel!{
        didSet{
            self.serialLbl.text = LanguageManager.SerialNo
        }
    }
    @IBOutlet weak var buyingPriceLbl: UILabel!{
        didSet{
            self.buyingPriceLbl.text = LanguageManager.BuyingPrice
        }
    }
    @IBOutlet weak var buyingPriceTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!{
        didSet{
            self.descriptionLbl.text = LanguageManager.Description
        }
    }
    @IBOutlet weak var descriptionTextField: UITextField!{
        didSet{
            self.descriptionTextField.placeholder = LanguageManager.Description
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var tableViewHeightConstrant: NSLayoutConstraint!
    
    var serialList : [ProductForDamageSerials]?
    var selectedSerial : [String] = []
    
    var product : ProductForDamage?
    var delegate : DamageableProductAddDelegate?
    var buyingPrice : Double?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpViews()
        self.configureTableView()
        self.initialSetup()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNextBtn(sender:)), for: .touchUpInside)
        
    }
    
    func initialSetup(){
        
        
        self.buyingPriceTextField.underlined()
        self.descriptionTextField.underlined()
        
        guard let item = self.product else{
            return
        }
        self.buyingPriceTextField.text = item.buyingPrice
        self.buyingPrice = Double(item.buyingPrice)
        //self.descriptionTextField.placeholder = "Description"
    }
    
    
    @objc func onNextBtn(sender : UIButton){
        if isValidated(){
            guard let price = buyingPriceTextField.text,let priceDouble = Double(price) else{
                return
            }
            let descriptionText = self.descriptionTextField.text ?? ""
            let serials = self.selectedSerial.joined(separator: ",")
            self.delegate?.setSerialData(serial: serials, buyingPrice: priceDouble, description: descriptionText)
            self.navigationController?.popViewController(animated: true)
        }
    }
    func isValidated()->Bool{
        if self.selectedSerial == [""] {
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.SerialNumberIsRequired)
            return false
        }
        else if (self.buyingPriceTextField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.ReturnPriceIsRequired)
            return false
        }
        guard let buyingPriceText = self.buyingPriceTextField.text, let buyingPrice = Double(buyingPriceText), let buyingPriceCheck = self.buyingPrice else {
            return false
        }
        if buyingPrice > buyingPriceCheck{
            self.showAlert(title: LanguageManager.BuyingpriceCantBeGreaterThan + " \(buyingPriceCheck)", message: "")
            return false
        }
        return true
    }

}

//Mark: TableView Delegate DataSource
extension DamageableSerialProductBaseInfoViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductSerialCell.nib, forCellReuseIdentifier: ProductSerialCell.identifier)
        self.tableView.estimatedRowHeight = 30
        self.refreshTableView()
        self.tableView.allowsMultipleSelection = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let list = self.serialList, list.count > 0 else{
            return 0
        }
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductSerialCell = tableView.dequeueReusableCell(withIdentifier: ProductSerialCell.identifier, for: indexPath) as! ProductSerialCell
        guard let list = self.serialList, list.count > 0 else{
            return cell
        }
        let serial = list[indexPath.row]
        let serialId = serial.id
        cell.serialNumber.text = serial.serialNo
        
        if self.selectedSerial.contains("\(serialId)"){
            cell.checkIcon.image = UIImage(named : "check_icon")
        }
        else{
            cell.checkIcon.image = UIImage(named: "uncheck_icon")
        }
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let list = self.serialList, list.count > 0 else{
            return
        }
        let serial = list[indexPath.row]
        
        let serialId = serial.id
        
        if !self.selectedSerial.contains("\(serialId)"){
            self.selectedSerial.append("\(serialId)")
        }
        else {
            self.selectedSerial = self.selectedSerial.filter({ $0 != "\(serialId)"})
        }
        self.refreshTableView()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let list = self.serialList, list.count > 0 else{
            return
        }
        let serial = list[indexPath.row]
        let serialId = serial.id
        if self.selectedSerial.contains("\(serialId)"){
            self.selectedSerial.append("\(serialId)")
        }
        self.refreshTableView()
    }
    
    func refreshTableView(){
        self.tableView.reloadData {
            if self.tableView.contentSize.height > 200{
                self.tableViewHeightConstrant.constant = 200
            }
            else {
                self.tableViewHeightConstrant.constant = self.tableView.contentSize.height
            }
        }
    }
    
}
