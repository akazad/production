//
//  DamagedProductSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 25/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DamagedProductSearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = DamagedProductListPresenter(service: ProductService())
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    
    private var damagedProductList : [DamagedProductGroupInfo] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var searchText = ""
    var damagedProductName : String?
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configureTableView()
        self.setUpSearchBar()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpInitialLanguage()
        self.setNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.presenter.getDamagedProductSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(DamagedProductSearchViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.damagedProductList = []
        self.currentPage = 1
        self.presenter.getDamagedProductListDataFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }
    
}

//Mark: Search Delegate
extension DamagedProductSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.DamagedProductSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.damagedProductList = []
        self.isLoading = true
        self.isSearchActive = true
        self.presenter.getDamagedProductSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
    }
}

//Mark TableView Delegate and Data Source
extension DamagedProductSearchViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DamagedProductListCell.nib, forCellReuseIdentifier: DamagedProductListCell.identifier)
        self.tableView.rowHeight = 80
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.damagedProductList.count > 0 {
            return self.damagedProductList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DamagedProductListCell = tableView.dequeueReusableCell(withIdentifier: DamagedProductListCell.identifier, for: indexPath) as! DamagedProductListCell
        cell.selectionStyle = .none
        if self.damagedProductList.count > 0 {
            let data = self.damagedProductList[indexPath.row]
            
            cell.damagedProduct.text = data.damagedProductName
            cell.numberOfDamages.text = LanguageManager.Damaged + " : " + String(describing: data.damagedLot) + " " + LanguageManager.Times
            cell.totalDamages.text = LanguageManager.TotalDamages + " : " + "\(data.totalQuantity)" + data.unit
            cell.totalLoss.text = LanguageManager.TotalDamageAmount + " : " + String(describing: data.totalLoss) + " " + Constants.currencySymbol
            
            cell.selectBtn.isHidden = true
            
            if isLoading == false && indexPath.row == self.damagedProductList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getDamagedProductListDataFromServer(page: self.currentPage)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.damagedProductList.count > 0 {
            let damagedItem = self.damagedProductList[indexPath.row]
            self.damagedProductName = damagedItem.damagedProductName
            self.navigateToDamagePerProductVC(id: damagedItem.damagedProductId)
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToDamagePerProductVC(id : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DamagedPerProductViewController") as! DamagedPerProductViewController
        viewController.damagedProductId = id
        viewController.damagedMaterialName = self.damagedProductName
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Api Delegate
extension DamagedProductSearchViewController : DamagedProductListViewDelegate{
    
    func setDamagedProductData(data: DamagedProductListDataMapper) {
        guard let list = data.damagedProductGroupInfo, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.damagedProductList += list
        
        
        guard let pagination = data.pagination else{
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.currentPage = 1
        self.damagedProductList = []
        self.presenter.getDamagedProductListDataFromServer(page: self.currentPage)
    }
}

