//
//  ProductSubCategoryVC.swift
//  Ponno
//
//  Created by a k azad on 31/7/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ProductSubCategoryVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var id : Int = -1
    var filteredCategory : [ProductCategories] = []
    var productCategories : [ProductCategories] = []
    var categoryNameString : String = ""
    var currentPage : Int = 1
    var parentId : Int?
    var categoryId : Int?
    var nameString : [Int : String] = [:]
    var categoryName : String?
    
    var productCategoryInfo : ProductCategoryInfo?
    
    private var presenter = ProductCategoryPresenter(service: ProductService())
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBarSetup()
        self.configureTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.initialSetup()
        self.attachPresenter()
    }

    func initialSetup(){
        self.filteredCategory = self.productCategories.filter({$0.parent == self.id})
        self.refreshTableView()
    }
    
    func navigationBarSetup(){
        let productAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        productAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        productAddBtn.addTarget(self, action: #selector(onSubCategoryAdd(sender:)), for: UIControl.Event.touchUpInside)
        productAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        productAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        productAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: productAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2], animated: true)
    }
    
    @objc func onSubCategoryAdd(sender: UIBarButtonItem){
        //self.categoryParentId = 0
        alertWithTextField()
    }
    
    
}

//Mark: TableView Delegate
extension ProductSubCategoryVC: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductCategoryCell.nib, forCellReuseIdentifier: ProductCategoryCell.identifier)
        self.tableView.separatorStyle = .none
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductCategoryCell = tableView.dequeueReusableCell(withIdentifier: ProductCategoryCell.identifier, for: indexPath) as! ProductCategoryCell
        
        cell.selectionStyle = .none
        
        let item = self.filteredCategory[indexPath.row]
        
        cell.categoryItemLbl.text = item.name
        
        cell.subCategoryAddBtn.tag = item.id
        cell.subCategoryAddBtn.addTarget(self, action: #selector(onSubcategoryAdd), for: .touchUpInside)
        
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
    @objc func onSubcategoryAdd(sender: UIButton){
        self.id = sender.tag
        for item in self.productCategories {
            if id == item.id {
                self.parentId = item.parent
                self.nameString [item.gen] = item.name
            }
        }
        self.alertWithTextField()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = self.filteredCategory[indexPath.row]
        
        let categoryName = item.name
        self.categoryId = item.id
        self.categoryNameString += " / " + "\(categoryName)"
        
        let gen = item.gen
        let name = item.name
        
        
        for (key , _) in self.nameString {
            if key == gen {
                self.nameString.removeValue(forKey: key)
                self.nameString [gen] = name
            }else{
                self.nameString [gen] = name
            }
        }
        
        self.parentId = item.parent
        
        self.didSelect(categories: self.productCategories, categoryId: self.categoryId ?? -1)
    }
    
    func didSelect(categories: [ProductCategories], categoryId: Int){
        var flag = 0
        
        for item in categories{
            if item.parent == categoryId{
                flag = 1
                break
            }
        }
        
        if flag == 0{
            if let id = self.categoryId{
                self.productCategoryInfo?.setCategoryInfo(name: self.nameString, id: id)
                self.popToSpecificViewController()
                
            }
            //self.navigateToAddNewProductViewController()
            
        }
        else {
            self.navigateToProductSubCategoryVC(id : categoryId)
        }
    }
    
    func navigateToProductSubCategoryVC(id : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductSubCategoryVC") as! ProductSubCategoryVC
        viewController.productCategories = self.productCategories
        viewController.nameString = self.nameString
        viewController.productCategoryInfo = self.productCategoryInfo
//        viewController.categoryNameString = self.categoryNameString
        viewController.id = id
        self.navigationController?.pushViewController(viewController, animated: true)
        //viewController.modalPresentationStyle = .popover
//        self.present(viewController, animated: true, completion: nil)
//        let navController = UINavigationController(rootViewController: viewController)
//        self.present(navController, animated: false, completion: nil)
    }
    
    func navigateToAddNewProductViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewProductViewController") as! AddNewProductViewController
        //viewController.categoryName = self.categoryNameString
        viewController.nameString = self.nameString
        guard let categoryId = self.categoryId else{
            return
        }
        viewController.productCategoryId = categoryId
        self.navigationController?.popViewController(animated: true)
    }
    
//    func popToSpecificViewController(){
//        let presentingVC = self.presentingViewController
//        self.dismiss(animated: true) {
//            if let  destinationVC =  presentingVC?.navigationController?.viewControllers.filter({$0 is AddNewProductViewController}).first {
//                presentingVC?.navigationController?.popToViewController(destinationVC, animated: false)
//            }
//        }
//
//    }
    func popToSpecificViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is AddNewProductViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
}

//Mark: Api Delegate
extension ProductSubCategoryVC : ProductCategoryViewDelegate{
    func setProductCategoriesList(productCategoryData: ProductCategoryListDataMapper) {
        guard let categories = productCategoryData.categories, categories.count > 0 else{
            return
        }
        
        let subCategoryItem = categories
        
        let subCategories = subCategoryItem.filter({$0.parent == id})
        self.productCategories = subCategories
        
    }
    
    func setCategoryAddData(data: CategoryAddDataMapper) {
        guard let categoryData = data.category, let message = data.message else {
            return
        }

        self.id = categoryData.parent
        //self.categoryNameString += "/ " + categoryData.name
        self.categoryId = categoryData.id
        self.nameString [categoryData.gen] = categoryData.name
        
        let newCategory = ProductCategories.init(id: categoryData.id, name: categoryData.name, gen: categoryData.gen, parent: categoryData.parent)
        
        self.productCategories.append(newCategory)
        self.refreshTableView()
        
        self.successMessage(userMessage: message)
        
        
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        //self.presenter.getProductCategoryListFromServer(page: self.currentPage)
    }
}

extension ProductSubCategoryVC{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewSubCategoryAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.SubCategoryName
            textField.textAlignment = .center
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showMessage(userMessage: LanguageManager.SubCategoryNameIsRequired)
                return
            }
            
            let categoryName = textField.text?.lowercased()
            
            for category in self.productCategories{
                let existedCategory = category.name.lowercased()
                if categoryName == existedCategory{
                    self.showAlert(title: LanguageManager.SubCategoryIsAlreadyExists, message: "")
                    return
                }
            }
            
            let param : [String : Any] = ["name": textField.text!, "parent": self.id]
            
            self.presenter.postProductCategoryAddDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    //    //Alert Message
    //    func displayMessage(userMessage:String) -> Void {
    //        let alertController = UIAlertController(title: "সফল", message: userMessage, preferredStyle: .alert)
    //
    //        let OKAction = UIAlertAction(title: "OK", style: .default){
    //            (action:UIAlertAction!) in
    //            self.productCategoryTextField.resignFirstResponder()
    //            self.companyNameTextField.becomeFirstResponder()
    //
    //        }
    //        alertController.addAction(OKAction)
    //        self.present(alertController, animated: true, completion: nil)
    //    }
    //
    func showMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.alertWithTextField()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            if let id = self.categoryId{
                //self.navigationController?.popViewController(animated: true)
                self.productCategoryInfo?.setCategoryInfo(name: self.nameString, id: id)
            }
            self.popToSpecificViewController()
            //self.navigateToAddNewProductViewController()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}


