//
//  AddNewProductViewController.swift
//  Ponno
//
//  Created by a k azad on 7/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

protocol ProductCategoryInfo {
    func setCategoryInfo(name: [Int : String], id: Int)
}

class AddNewProductViewController: UIViewController, UIScrollViewDelegate, UIPopoverPresentationControllerDelegate {

    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var productNameLbl: UILabel!{
        didSet{
            productNameLbl.text = LanguageManager.ProductName
        }
    }
    @IBOutlet weak var productNameTextField: UITextField!{
        didSet{
            productNameTextField.placeholder = LanguageManager.ProductName
        }
    }
    @IBOutlet weak var productCategoryLbl: UILabel!{
        didSet{
            productCategoryLbl.text = LanguageManager.Category
        }
    }
    @IBOutlet weak var productCategoryTextField: UITextField!
    @IBOutlet weak var companyLbl: UILabel!{
        didSet{
            companyLbl.text = LanguageManager.Company
        }
    }
    @IBOutlet weak var companyNameTextField: UITextField!{
        didSet{
            companyNameTextField.placeholder = LanguageManager.Company
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var categoryAddBtn: UIButton!
    @IBOutlet weak var varientLbl: UILabel!{
        didSet{
            varientLbl.text = LanguageManager.Varient
        }
    }
    @IBOutlet weak var varientTextField: UITextField!{
        didSet{
            varientTextField.placeholder = LanguageManager.Varient
        }
    }
    @IBOutlet weak var unitLbl: UILabel!{
        didSet{
            unitLbl.text = LanguageManager.Unit
        }
    }
    @IBOutlet weak var unitTextField: UITextField!
    @IBOutlet weak var productCodeLbl: UILabel!{
        didSet{
            productCodeLbl.text = LanguageManager.ProductCode
        }
    }
    @IBOutlet weak var productCodeTextField: UITextField!{
        didSet{
            productCodeTextField.placeholder = LanguageManager.ProductCode
            
//            let button = UIButton(type: .custom)
//            button.setImage(UIImage(named: "scan"), for: .normal)
//            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
//             button.frame = CGRect(x: CGFloat(productCodeTextField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
//            button.addTarget(self, action: #selector(self.productScan), for: .touchUpInside)
//            productCodeTextField.rightView = button
//            productCodeTextField.rightViewMode = .always
        }
    }
    
    private var presenter = AddNewProductPresenter(service: ProductService())
    
    var productCategoryId : Int?
    var categoryParentId: Int = 0
    var product : Product?
    
    var categoryPicker = UIPickerView()
    var categories : [ProductCategories] = []{
        didSet{
            self.categoryPicker.reloadAllComponents()
        }
    }
    var categoryName : String?
    
    var nameString : [Int : String] = [:]
    var name : String = ""
    
    var currentPage : Int = 1
    var isLoading : Bool = false
    var getAll: Bool = true
    
    let imagePicker = UIImagePickerController()
    var selectedImage : UIImage?
    
    var unitId : Int?
    var productUnit : [ProductUnit] = []{
        didSet{
            self.unitPicker.reloadAllComponents()
        }
    }
    var unitPicker = UIPickerView()
    
    //Scan
    var scanningCode: String?{
        didSet{
            self.productCodeTextField.text = scanningCode
        }
    }
    var textFieldSerials : String?
    
    var globalProductList : [GlobalProducts]?
    var globalProduct : GlobalProducts?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        //self.appendCategoryName()
        self.setUpViews()
        self.setUpPickerView()
        self.configureTextFields()
        self.setImageView()
        //self.appendCategoryName()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated);
        if self.isMovingFromParent
        {
            self.categoryName = ""
            self.productCategoryId = nil
        }
        if self.isBeingDismissed
        {
            //Dismissed
        }
    }
    
    func setUpViews(){
        self.title = LanguageManager.NewProduct
        
        self.productNameTextField.addTarget(self, action: #selector(onProductNameTextFieldTap), for: .touchUpInside)
        
        ProductObject.companyName = ""
        
        self.nextBtn.addTarget(self, action: #selector(onNextBtn(sender:)), for: .touchUpInside)
        self.navigationController?.navigationBar.topItem?.title = " "
        
        imagePicker.delegate = self
        self.imageIcon.addTapGestureRecognizer(action: {self.onTapOnImageView()})
        
        //self.categoryAddBtn.addTarget(self, action: #selector(onCategoryAddBtnPressed), for: .touchUpInside)
        //self.categoryAddBtn.isHidden = true
        //self.productCategoryTextField.isEnabled = false
        
        self.unitTextField.text = "pc"
        self.unitId = 1
        
        
    }
    
    @objc func onProductNameTextFieldTap(sender: UITextField){
        self.productNameTextField.resignFirstResponder()
        self.navigateToGlobalProductListVC(sender: sender)
    }
    
    func appendCategoryName(){

        let sortedDictionary = self.nameString.sorted{$0.key < $1.key}

        
        for (_, value) in sortedDictionary{
            
            self.name +=  " / " + "\(value)"
        }
        self.productCategoryTextField.text = self.name
    }
    
    @objc func onCategoryAddBtnPressed(sender: UIButton){
        //self.alertWithTextField()
        self.alertWithTableView()
    }
    
    @objc func productScan(sender: UIButton){
        navigateToScannerViewController()
    }
    
    @objc func onNextBtn(sender : UIButton){
        if self.isValidated(){
            ProductObject.productImage = self.selectedImage
            ProductObject.productName = self.productNameTextField.text
            ProductObject.productCategoryId = self.productCategoryId
            ProductObject.companyName = self.companyNameTextField.text
            ProductObject.varient = self.varientTextField.text
            ProductObject.unitId = self.unitId
            ProductObject.sku = self.productCodeTextField.text
//            self.navigateToAddProductSecondViewController()
            self.navigateToAddProductThirdViewController()
        }
    }
    
    func isValidated()->Bool{
        if self.productNameTextField.text == ""{
            showAlert(title: LanguageManager.ProductNameIsRequired, message: "")
            return false
        }else if self.productCategoryTextField.text == ""{
            showAlert(title: LanguageManager.ProductCategoryIsRequired, message: "")
            return false
        }else if self.unitTextField.text == ""{
            showAlert(title: LanguageManager.ProductUnitIsRequired, message: "")
            return false
        }
        return true
    }

    func navigateToAddProductThirdViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductAddThirdVC") as! ProductAddThirdVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddProductSecondViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductAddSecondVC") as! ProductAddSecondVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToScannerViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Scanner", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ScannerViewController") as! ScannerViewController
        viewController.navigateTo = Navigate.productCode
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //Mark: PickerView
    func setUpPickerView(){
        categoryPicker.delegate = self
    
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCategory(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
       
        
//        self.productCategoryTextField.inputView = categoryPicker
//        self.productCategoryTextField.inputAccessoryView = toolBar
        
        unitPicker.delegate = self
        
        let unitToolBar = UIToolbar()
        unitToolBar.barStyle = UIBarStyle.default
        unitToolBar.isTranslucent = true
        unitToolBar.tintColor = UIColor.black
        unitToolBar.sizeToFit()
        
        
        let doneButtonOnUnit = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnUnit(sender:)))
        
        unitToolBar.setItems([cancelButton,spaceButton, doneButtonOnUnit], animated: false)
        unitToolBar.isUserInteractionEnabled = true
        
        self.unitTextField.inputView = unitPicker
        self.unitTextField.inputAccessoryView = unitToolBar
  
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnCategory(sender : UIBarButtonItem){
        //self.productCategoryTextField.resignFirstResponder()
        self.companyNameTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnUnit(sender : UIBarButtonItem){
        //self.unitTextField.resignFirstResponder()
        self.productCodeTextField.becomeFirstResponder()
    }
    
    //GlobalProducts
    func navigateToGlobalProductListVC(sender: UITextField){
        
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "GlobalProductListViewController") as? GlobalProductListViewController
        popoverContentController?.modalPresentationStyle = .popover
        
        if let controller = popoverContentController{
            guard let products = self.globalProductList, products.count > 0 else{
                return
            }
            controller.globalProducts = products
            //controller.dataFromOther = "Hola data is passed and reached"
            controller.selectedProductDelegate = self
            controller.filteredList = self.globalProductList
            
            showPopup(controller, sourceView: sender)
            
            //            if let popoverPresentationController = controller.popoverPresentationController {
            //                //popoverPresentationController.permittedArrowDirections = .up
            //                popoverPresentationController.sourceView = self.view
            //                popoverPresentationController.sourceRect = self.productNameTextField.frame
            //                popoverPresentationController.delegate = self
            //                if let popoverController = popoverContentController {
            //                    //popoverController.globalProducts = self.globalProducts
            //                    present(popoverController, animated: true, completion: nil)
            //                    //self.navigationController?.pushViewController(popoverController, animated: true)
            //                }
            //            }
        }
        
        
    }
    
}

//
extension AddNewProductViewController : SelectedGlobalProductDelegate{
    func selectedProduct(selectedProduct: GlobalProducts?) {
        guard let item = selectedProduct else{
            return
        }
        if let products = self.globalProductList{
            for product in products{
                if product.id == item.id{
                    self.globalProduct = item
                    self.productNameTextField.text = product.name
                    if product.categoryName != ""{
                        self.productCategoryTextField.isEnabled = false
                    }
                    self.productCategoryTextField.text = product.categoryName
                    self.productCategoryId = product.categoryId
                    self.varientTextField.text = product.variant
                    self.companyNameTextField.text = product.company
                    self.unitTextField.text = product.unitName
                    self.unitId = product.unit
                    self.productCodeTextField.text = product.sku
                    self.setImageInImageView(imageUrl: product.image ?? "")
                }else{
                    self.productNameTextField.text = item.name
                }
            }
        }
        
    }
    
    
}

//Mark: PickerViewDelegate
extension AddNewProductViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == categoryPicker {
            guard self.categories.count > 0 else {
                return 0
            }
            return categories.count
        }else{
            guard self.productUnit.count > 0 else {
                return 0
            }
            return self.productUnit.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == categoryPicker{
            if self.categories.count > 0 {
                let item = categories[row]
                self.productCategoryId = item.id
                self.productCategoryTextField.text = item.name
                return item.name
            }
            return ""
        }else{
            guard self.productUnit.count > 0 else {
                return ""
            }
            let unitItem = self.productUnit[row]
            self.unitTextField.text = unitItem.name
            return unitItem.name
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == categoryPicker{
            if self.categories.count > 0 {
                let item =  self.categories[row]
                self.productCategoryTextField.text =  item.name
                self.productCategoryId = item.id
            }
        }else{
            if self.productUnit.count > 0 {
                let unitItem = self.productUnit[row]
                self.unitTextField.text = unitItem.name
                self.unitId = unitItem.id
                //let selectedRow = [self.unitPicker.selectedRow(inComponent: 0)]
                //                if selectedRow == [0]{
                //                    self.unit.text = self.productUnit[0].name
                //                    self.unitId = self.productUnit[0].id
                //                }else{
                //                    self.unit.text = unitItem.name
                //                    self.unitId = unitItem.id
                //                }
            }
        }
    }
    
}

//Mark: TextField Delegate
extension AddNewProductViewController: UITextFieldDelegate{
    func configureTextFields(){
        //self.productName.delegate = self
        //self.companyName.delegate = self
        self.productNameTextField.delegate = self
        self.productCategoryTextField.delegate = self
        self.companyNameTextField.delegate = self
        self.varientTextField.delegate = self
        self.unitTextField.delegate = self
        self.productCodeTextField.delegate = self
        
        self.productNameTextField.underlined()
        self.productCategoryTextField.underlined()
        self.companyNameTextField.underlined()
        self.varientTextField.underlined()
        self.unitTextField.underlined()
        self.productCodeTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.productNameTextField{
            self.productCategoryTextField.becomeFirstResponder()
        }else if textField == self.productCategoryTextField{
            self.companyNameTextField.becomeFirstResponder()
        }else if textField == self.companyNameTextField{
            self.varientTextField.becomeFirstResponder()
        }else if textField == self.varientTextField{
            self.unitTextField.becomeFirstResponder()
        }else if textField == self.unitTextField{
            self.productCodeTextField.becomeFirstResponder()
        }else if textField == self.productCodeTextField{
            self.productCodeTextField.resignFirstResponder()
            self.nextBtn.becomeFirstResponder()
        }
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.productCategoryTextField{
            self.navigateToProductCategoryViewController()
            return false
        }
        return true
    }
    
    func navigateToProductCategoryViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductCategoryViewController") as! ProductCategoryViewController
//        viewController.modalPresentationStyle = .popover
        viewController.productCategoryInfo = self
        self.navigationController?.pushViewController(viewController, animated: true)
//        let navController = UINavigationController(rootViewController: viewController)
//        self.present(navController, animated: false, completion: nil)
        //self.navigationController?.present(viewController, animated: true, completion: nil)
    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if textField == self.varient{
//            self.unit.becomeFirstResponder()
//        }
//        else if textField == self.productCode{
//            self.nextBtn.becomeFirstResponder()
//        }
//        return false
//    }
}

//Mark: Api Delegate
extension AddNewProductViewController : AddNewProductViewDelegate{
    func setProductUnit(unit: ProductUnitDataMapper) {
        guard let item = unit.productUnit, item.count > 0 else {
            return
        }
        self.productUnit = item
    }
    
    
    func setProductCategoriesList(productCategoryData: ProductCategoryListDataMapper) {
        guard  let list = productCategoryData.categories, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.categories = list
    }
    
    func setCategoryAddData(data: CategoryAddDataMapper) {
        guard let categoryData = data.category else {
            return
        }
        self.productCategoryTextField.text =  categoryData.name
        self.productCategoryTextField.resignFirstResponder()
        self.productCategoryId = categoryData.id
        self.categoryParentId = categoryData.parent
    }
    
    
    func onFailed(data: String) {
        //self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.presenter.getProductUnitDataFromServer()
    }
}

extension AddNewProductViewController{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewCategoryAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.CategoryName
            textField.textAlignment = .center
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showMessage(userMessage: LanguageManager.CategoryIsRequired)
                return
            }
            
            self.categoryName = textField.text?.lowercased()

            for category in self.categories{
                let existedCategory = category.name.lowercased()
                if self.categoryName == existedCategory{
                    self.showAlert(title: LanguageManager.CategoryAlreadyExists, message: "")
                    return
                }
            }
            
            let param : [String : Any] = ["name": textField.text!, "parent": self.categoryParentId]
            
            self.presenter.postProductCategoryAddDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.productCategoryTextField.resignFirstResponder()
                self.companyNameTextField.becomeFirstResponder()
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.alertWithTextField()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: ImagePicker
extension AddNewProductViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    private func setImageView(){
        guard let imageUrl = self.product?.image else{
            self.setImageInImageView(imageUrl: "")
            return
        }
        
        self.setImageInImageView(imageUrl: ImageUrl.sharedImageInstance.productImage + imageUrl)
    }
    
    func setImageInImageView(imageUrl : String){
        self.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageIcon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
    }
    
    func onTapOnImageView(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageIcon.contentMode = .scaleAspectFill
            imageIcon.image = pickedImage
            self.selectedImage = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension AddNewProductViewController {
//    func alertWithTableView(){
//        var alrController = UIAlertController(title: "\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
//
//        let margin:CGFloat = 8.0
//        let rect = CGRect(x: margin, y: margin, width: alrController.view.bounds.size.width - margin * 4.0, height: 100.0)
//        //let rect = CGRect(margin, margin, alrController.view.bounds.size.width - margin * 4.0, 100.0)
//        var tableView = UITableView(frame: rect)
//        tableView.delegate = self
//        tableView.dataSource = self
//        tableView.backgroundColor = UIColor.green
//        alrController.view.addSubview(tableView)
//
//        let somethingAction = UIAlertAction(title: "Something", style: UIAlertAction.Style.Default, handler: {(UIAlertAction?)})
//
//        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in println("cancel")})
//
//        alrController.addAction(somethingAction)
//        alrController.addAction(cancelAction)
//
//        self.presentViewController(alrController, animated: true, completion:{})
//    }
    func alertWithTableView(){
//        let alertController : UIAlertController = UIAlertController(title: "Select email ID", message: nil, preferredStyle: .alert)
//        alertController.view.backgroundColor = UIColor.white
//        alertController.view.layer.cornerRadius = 8.0
//
//        let contactNumVC = ProductCategoryViewController()
//
//        //contactNumVC.count = 5
//
//        //contactNumVC.numbersArray = emailIDs
//
//        //contactNumVC.preferredContentSize = CGSizeMake(alertController.view.frame.width, (44 * CGFloat(5)))
//        contactNumVC.preferredContentSize = CGSize(width: alertController.view.frame.width, height: (44 * CGFloat(5)))
//
//        alertController.setValue(contactNumVC, forKeyPath: "ProductCategoryViewController")
        //Configure the presentation controller
        //let button = sender as? UIButton
        //let buttonFrame = categoryAddBtn?.frame ?? CGRect.zero
        
        let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "ProductCategoryViewController") as? ProductCategoryViewController
        popoverContentController?.modalPresentationStyle = .popover
       // present(ProductCategoryViewController, animated: true, completion:  nil)
        popoverContentController?.preferredContentSize = CGSize(width: 220,height:250)

        
        /* 3 */
        if let popoverPresentationController = popoverContentController?.popoverPresentationController
            {
            popoverPresentationController.permittedArrowDirections = .up
            popoverPresentationController.sourceView = self.view
            //popoverPresentationController.sourceRect = CGRect(x: 40, y: 50, width: 315, height: 230)
            //popoverPresentationController.preferredContentSize = CGSize.init(width: 200, height : 200)
            popoverPresentationController.sourceRect = CGRect(x: 0, y: 0, width: 220, height: 320)
            popoverPresentationController.delegate = self
            if let popoverController = popoverContentController {
                present(popoverController, animated: true, completion: nil)
            }
        }
        //UIPopoverPresentationControllerDelegate inherits from UIAdaptivePresentationControllerDelegate, we will use this method to define the presentation style for popover presentation controller
        func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
            return .none
        }
        
        //UIPopoverPresentationControllerDelegate
        func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
            
        }
        
        func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
            return true
        }
    }
}

extension AddNewProductViewController : ProductCategoryInfo{
    func setCategoryInfo(name: [Int : String], id: Int) {
        self.nameString.removeAll()
        self.name = ""
        self.nameString = name
        self.productCategoryId = id
        self.appendCategoryName()
    }
    
    
}
