//
//  ModuleProductCategoryPresenter.swift
//  Ponno
//
//  Created by a k azad on 23/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol ModuleProductCategoryListViewDelegate : NSObjectProtocol {
    func setProductCategoryData(products: ModuleProductCategoryDataMapper)
    func onCategoryUpdate(data: AddDataMapper)
    func onCategoryDelete(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class ModuleProductCategoryPresenter: NSObject {
    private let service : ProductService
    weak private var viewDelegate : ModuleProductCategoryListViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ModuleProductCategoryListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getModuleProductCategoryFromServer(page : Int){
        self.viewDelegate?.showLoading()
        self.service.getModuleProductCategoryData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductCategoryData(products: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getModuleProductCategorySearchDataFromServer(page: Int, searchText: String){
        self.viewDelegate?.showLoading()
        self.service.getModuleProductCategorySearchData(page: page, searchText: searchText, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductCategoryData(products: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postCategoryUpdateDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.PostCategoryUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCategoryUpdate(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postCategoryDeleteDataToServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostCategoryDeleteData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCategoryDelete(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getAllModuleProductCategoryFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllModuleProductCategoryData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductCategoryData(products: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }

}

