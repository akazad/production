//
//  DamagedPerProductPresenter.swift
//  Ponno
//
//  Created by a k azad on 25/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol DamagedPerProductViewDelegate : NSObjectProtocol {
    func setDamagedPerProductData(data : DamagedPerProductDataMapper)
    func onDeleteDamagedData(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}
class DamagedPerProductPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : DamagedPerProductViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DamagedPerProductViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func  getDamagedPerProductDataFromServer(page: Int, id : Int){
        self.viewDelegate?.showLoading()
        self.service.getDamagePerProductData(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDamagedPerProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
        
    }
    func deleteDamagedPerProductDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.deleteDamagedPerProductData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDeleteDamagedData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
