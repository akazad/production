//
//  ProductSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 13/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ProductSearchListViewDelegate : NSObjectProtocol {
    func setProductSearchData(data: ProductListDataMapper)
    //func setSaleableProductData(data : ProductListDataMapper)
    func onFailed(failure : String)
    func showLoading()
    func hideLoading()
}

class ProductSearchListPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : ProductSearchListViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ProductSearchListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductSearchDataFromServer(page: Int, searchString: String){
        self.viewDelegate?.showLoading()
        self.service.getProductSearchData(page: page, searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductSearchData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
}
