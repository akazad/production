//
//  ProductUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 7/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ProductUpdateViewDelegate : NSObjectProtocol {
    func setProductCategoryData(data : ProductCategoryListDataMapper)
    func setFreemiumProductData(data : OfflineProductDataMapper)
    func onProductCategoryAdd(data: CategoryAddDataMapper)
    func setProductUnitData(data: ProductUnitDataMapper)
    func onImageUpload(data: AddDataMapper)
    func onSuccess(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class ProductUpdatePresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : ProductUpdateViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ProductUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductCategoryDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getProductCategoryListData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductCategoryData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postProductUpdateDataToServer(params: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postProductUpdateData(params: params, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getProductUnitDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getProductUnitData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductUnitData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postProductCategoryAddDataToServer(data: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postProductCategoryAddData(params: data, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onProductCategoryAdd(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func uploadImage(productId : Int, image : UIImage?){
        self.viewDelegate?.showLoading()
        self.service.postUserImage(productId: productId, image: image, succeed: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onImageUpload(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getFreemiumOfflineProductFromServer(timeStamp: String){
        self.viewDelegate?.showLoading()
        self.service.getOfflineProductData(timesStamp: timeStamp, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setFreemiumProductData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}

