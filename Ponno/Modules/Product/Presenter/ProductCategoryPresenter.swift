//
//  ProductCategoryPresenter.swift
//  Ponno
//
//  Created by a k azad on 22/7/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ProductCategoryViewDelegate : NSObjectProtocol {
    func setProductCategoriesList(productCategoryData: ProductCategoryListDataMapper)
    func setCategoryAddData(data: CategoryAddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class ProductCategoryPresenter: NSObject {
    private let service : ProductService
    weak private var viewDelegate : ProductCategoryViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ProductCategoryViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductCategoryListFromServer(page : Int){
        self.viewDelegate?.showLoading()
        self.service.getProductCategoryListData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductCategoriesList(productCategoryData: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postProductCategoryAddDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postProductCategoryAddData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCategoryAddData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
