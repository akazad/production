//
//  ProductViewPresenter.swift
//  Ponno
//
//  Created by a k azad on 5/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ProductViewDelegate : NSObjectProtocol {
    func setProductViewData(data : ProductDetailsDataMapper)
    func setFreemiumProductData(data : OfflineProductDataMapper)
    func deleteProductData(data: AddDataMapper)
    func onProductUpdate(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class ProductViewPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : ProductViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ProductViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductViewDataFromServer(id : Int) {
        self.viewDelegate?.showLoading()
        self.service.getProductViewData(id: id, success: { (data)  in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductViewData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func deleteProductDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostProductDeleteData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.deleteProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postProductUpdateDataToServer(params: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postProductUpdateData(params: params, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onProductUpdate(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getFreemiumOfflineProductFromServer(timeStamp: String){
        self.viewDelegate?.showLoading()
        self.service.getOfflineProductData(timesStamp: timeStamp, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setFreemiumProductData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
