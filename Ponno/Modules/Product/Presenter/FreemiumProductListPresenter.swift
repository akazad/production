//
//  FreemiumProductListPresenter.swift
//  Ponno
//
//  Created by a k azad on 19/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol FreemiumProductListViewDelegate : NSObjectProtocol {
    func setFreemiumProductData(data : OfflineProductDataMapper)
    func setProductData(data : ProductListDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class FreemiumProductListPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : FreemiumProductListViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : FreemiumProductListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getFreemiumOfflineProductFromServer(timeStamp: String){
        self.viewDelegate?.showLoading()
        self.service.getOfflineProductData(timesStamp: timeStamp, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setFreemiumProductData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getProductListDataFromServer(page: Int) {
        self.viewDelegate?.showLoading()
        self.service.getProductListData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductData(data: data)
        }, failure: {(message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}


