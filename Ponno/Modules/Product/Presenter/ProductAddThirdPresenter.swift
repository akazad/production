//
//  ProductAddThirdPresenter.swift
//  Ponno
//
//  Created by a k azad on 16/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol ProductAddThirdViewDelegate : NSObjectProtocol {
    func getNewProductData(newProductData: ProductAddDataMapper)
    func setVendorListData(data: VendorsDataMapper)
    func postNewVendorData(data: VendorAddDataMapper)
    func onImageUpload(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class ProductAddThirdPresenter: NSObject {
    private let service : ProductService
    weak private var viewDelegate : ProductAddThirdViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ProductAddThirdViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsListFromServer(page: Int){
        let service = VendorsService()
        self.viewDelegate?.showLoading()
        service.getVendorsData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorListData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewVendorDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.postAddNewVendorData(vendorData: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.postNewVendorData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewProductDataToServer(productData : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewProductData(params: productData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.getNewProductData(newProductData: data) 
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func uploadImage(productId : Int,image : UIImage?){
        self.viewDelegate?.showLoading()
        self.service.postUserImage(productId: productId, image: image, succeed: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onImageUpload(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
