//
//  ProductPresenter.swift
//  Ponno
//
//  Created by a k azad on 26/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol ProductListViewDelegate : NSObjectProtocol {
    func setProductData(data : ProductListDataMapper)
    func setProductSearchData(data: ProductListDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class ProductListPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : ProductListViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ProductListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductListDataFromServer(page: Int) {
        self.viewDelegate?.showLoading()
        self.service.getProductListData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductData(data: data)
        }, failure: {(message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getProductSearchDataFromServer(page: Int, searchString: String){
        self.viewDelegate?.showLoading()
        self.service.getProductSearchData(page: page, searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductSearchData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getProductAdvanceSearchDateFromServer(name: String, company: String, variant: String, category: String, dealer: String){
        self.viewDelegate?.showLoading()
        self.service.getProductAdvanceSearchData(name: name, company: company, variant: variant, category: category, dealer: dealer, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
