//
//  FreemiumPanViewPresenter.swift
//  Ponno
//
//  Created by a k azad on 19/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol FreemiumPanViewDelegate : NSObjectProtocol {
    func setProductRecentSaleHistory(data : ProductRecentSaleDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class FreemiumPanViewPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : FreemiumPanViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : FreemiumPanViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductRecentSaleHistoryFromServer(page: Int, id : Int){
        self.viewDelegate?.showLoading()
        self.service.getProductRecentSale(page: page, id: id,  success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductRecentSaleHistory(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}

