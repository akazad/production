//
//  FreemiumSaleableProductsPresenter.swift
//  Ponno
//
//  Created by a k azad on 20/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol FreemiumSaleableProductsViewDelegate : NSObjectProtocol {
    func setFreemiumCategoryData(data : ProductCategoryListDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class FreemiumSaleableProductsPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : FreemiumSaleableProductsViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : FreemiumSaleableProductsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductCategoryFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllProductCategoryListData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setFreemiumCategoryData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
