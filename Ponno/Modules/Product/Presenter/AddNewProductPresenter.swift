//
//  AddNewProductPresenter.swift
//  Ponno
//
//  Created by a k azad on 13/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol AddNewProductViewDelegate : NSObjectProtocol {
    func setProductCategoriesList(productCategoryData: ProductCategoryListDataMapper)
    func setCategoryAddData(data: CategoryAddDataMapper)
    func setProductUnit(unit: ProductUnitDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class AddNewProductPresenter: NSObject {
    private let service : ProductService
    weak private var viewDelegate : AddNewProductViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AddNewProductViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getAllProductCategoryListFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllProductCategoryListData(getAll : getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductCategoriesList(productCategoryData: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postProductCategoryAddDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postProductCategoryAddData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCategoryAddData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getProductUnitDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getProductUnitData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductUnit(unit: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
//    func getProductUnitDataFromServer(){
//        self.viewDelegate?.showLoading()
//        self.service.getProductUnitData(success: { (data) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.setProductUnit(productUnit: data)
//        }, failure: { (message) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onFailed(data: message)
//        })
//    }

    

}
