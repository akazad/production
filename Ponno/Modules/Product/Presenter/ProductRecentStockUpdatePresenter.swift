//
//  ProductRecentStockUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 4/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol ProductRecentStockUpdateViewDelegate : NSObjectProtocol {
    func onSuccess(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class ProductRecentStockUpdatePresenter: NSObject {
    private let service : ProductService
    weak private var viewDelegate : ProductRecentStockUpdateViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ProductRecentStockUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postProductRecentStockUpdateDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postProductRecentStockUpdateData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
