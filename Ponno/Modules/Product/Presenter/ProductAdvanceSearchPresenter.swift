//
//  ProductAdvanceSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 24/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol ProductAdvanceSearchViewDelegate : NSObjectProtocol {
    func setProductCategoryData(products: ProductCategoryListDataMapper)
    func setDealerData(data: VendorsDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class ProductAdvanceSearchPresenter: NSObject {
    private let service : ProductService
    weak private var viewDelegate : ProductAdvanceSearchViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ProductAdvanceSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductCategoryListFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllProductCategoryListData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductCategoryData(products: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getDealerDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.getAllVendorsData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDealerData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }

}
