//
//  GlobalProductListPresenter.swift
//  Ponno
//
//  Created by a k azad on 21/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol GlobalProductListViewDelegate : NSObjectProtocol {
    func setGlobalProducts(products: GlobalProductDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class GlobalProductListPresenter: NSObject {
    private let service : ProductService
    weak private var viewDelegate : GlobalProductListViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : GlobalProductListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getGlobalProductsFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getGlobalProductsData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setGlobalProducts(products: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }

}
