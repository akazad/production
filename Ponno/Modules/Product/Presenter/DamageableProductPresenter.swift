//
//  DamageableProductPresenter.swift
//  Ponno
//
//  Created by a k azad on 25/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol DamageableProductViewDelegate : NSObjectProtocol {
    func setDamageableProductData(data : ProductForDamageDataMapper)
    func onSuccess(data: AddDataMapper)
    func onFailed(message : String)
    func showLoading()
    func hideLoading()
}

class DamageableProductPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : DamageableProductViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DamageableProductViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductForDamageDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllProductForDamageData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDamageableProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postDamagedProductStoreDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.PostDamagedProductStoreData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}
