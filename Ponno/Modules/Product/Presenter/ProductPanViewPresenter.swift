//
//  ProductPanViewPresenter.swift
//  Ponno
//
//  Created by a k azad on 6/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ProductPanViewDelegate : NSObjectProtocol {
    func setProductRecentSaleHistory(data : ProductRecentSaleDataMapper)
    func setProductRecentPurchaseHistory(data : ProductRecentPurchaseDataMapper)
    func setProductSerialNumber(data: ProductSerialDataMapper)
    func deleteRecentStockData(data: AddDataMapper)
    func serialUpdateData(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class ProductPanViewPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : ProductPanViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ProductPanViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductRecentSaleHistoryFromServer(page: Int, id : Int){
        self.viewDelegate?.showLoading()
        self.service.getProductRecentSale(page: page, id: id,  success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductRecentSaleHistory(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    func getProductRecentPurchaseHistoryFromServer(page: Int, id : Int){
        self.viewDelegate?.showLoading()
        self.service.getProductRecentPurchase(page: page, id: id,  success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductRecentPurchaseHistory(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getProductSerialNumberFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getProductSerial(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductSerialNumber(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postProductRecentStockDeleteDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostProductRecentStockDeleteData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.deleteRecentStockData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postSerialUpdateDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.PostProductSerialUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.serialUpdateData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
