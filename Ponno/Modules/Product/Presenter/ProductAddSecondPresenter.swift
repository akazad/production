//
//  ProductAddSecondPresenter.swift
//  Ponno
//
//  Created by a k azad on 16/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol ProductAddSecondViewDelegate : NSObjectProtocol {
    func setProductUnit(unit: ProductUnitDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class ProductAddSecondPresenter: NSObject {
    private let service : ProductService
    weak private var viewDelegate : ProductAddSecondViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ProductAddSecondViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getProductUnitDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getProductUnitData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductUnit(unit: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
