//
//  RecentStockReturnPresenter.swift
//  Ponno
//
//  Created by a k azad on 4/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol RecentStockReturnViewDelegate : NSObjectProtocol {
    func setVendorsList (data: VendorsDataMapper)
    func onSuccess(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class RecentStockReturnPresenter: NSObject {
    private let service : ProductService
    weak private var viewDelegate : RecentStockReturnViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : RecentStockReturnViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsListDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.getAllVendorsData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postProductRecentStockReturnDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postRecentReturnStockData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
