//
//  DamagedProductListPresenter.swift
//  Ponno
//
//  Created by a k azad on 24/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol DamagedProductListViewDelegate : NSObjectProtocol {
    func setDamagedProductData(data : DamagedProductListDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class DamagedProductListPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : DamagedProductListViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DamagedProductListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getDamagedProductListDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getDamageProductData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDamagedProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getDamagedProductSearchDataFromServer(page: Int, searchText: String){
        self.viewDelegate?.showLoading()
        self.service.getDamageProductSearchData(page: page, searchText: searchText, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDamagedProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
