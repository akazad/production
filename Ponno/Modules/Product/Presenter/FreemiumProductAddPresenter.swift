//
//  FreemiumProductAddPresenter.swift
//  Ponno
//
//  Created by a k azad on 20/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol FreemiumProductAddViewDelegate : NSObjectProtocol {
    func setProductCategoriesList(productCategoryData: ProductCategoryListDataMapper)
    func setCategoryAddData(data: CategoryAddDataMapper)
    func setProductUnit(unit: ProductUnitDataMapper)
    func setGlobalProducts(products: GlobalProductDataMapper)
    func onSuccessfulProductAdd(newProductData: ProductAddDataMapper)
    func onImageUpload(data: AddDataMapper)
    func setFreemiumProductData(data : OfflineProductDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}

class FreemiumProductAddPresenter: NSObject {
    private let service : ProductService
    weak private var viewDelegate : FreemiumProductAddViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : FreemiumProductAddViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getAllProductCategoryListFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllProductCategoryListData(getAll : getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductCategoriesList(productCategoryData: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postProductCategoryAddDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postProductCategoryAddData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCategoryAddData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getProductUnitDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getProductUnitData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductUnit(unit: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }

    func getGlobalProductsFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getGlobalProductsData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setGlobalProducts(products: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewProductDataToServer(productData : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewProductData(params: productData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccessfulProductAdd(newProductData: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getFreemiumOfflineProductFromServer(timeStamp: String){
        self.viewDelegate?.showLoading()
        self.service.getOfflineProductData(timesStamp: timeStamp, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setFreemiumProductData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func uploadImage(productId : Int,image : UIImage?){
        self.viewDelegate?.showLoading()
        self.service.postUserImage(productId: productId, image: image, succeed: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onImageUpload(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }

}

