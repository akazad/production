//
//  DamagedPerProductUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 25/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol DamagedPerProductUpdateViewDelegate : NSObjectProtocol {
    func updatePerProductData(data : AddDataMapper)
    func setProductSerialData(data: DamagedPerProductSerialDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class DamagedPerProductUpdatePresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : DamagedPerProductUpdateViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DamagedPerProductUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postDamagedPerProductUpdateDataToServer(param : [String : Any]) {
        self.viewDelegate?.showLoading()
        self.service.PostDamagedPerProductUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updatePerProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getDamagedPerProductSerialDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getDamagePerProductSerialData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setProductSerialData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
