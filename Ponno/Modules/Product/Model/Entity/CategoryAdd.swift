//
//  CategoryAdd.swift
//  Ponno
//
//  Created by a k azad on 10/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class CategoryAdd : Mappable {
    var name : String = ""
    var parent : Int = 0
    var gen : Int = 0
    var pharmacyId : String = ""
    var updatedAt : String = ""
    var createdAt : String = ""
    var id : Int = 0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        name <- map["name"]
        parent <- map["parent"]
        gen <- map["gen"]
        pharmacyId <- map["pharmacy_id"]
        updatedAt <- map["updated_at"]
        createdAt <- map["created_at"]
        id <- map["id"]
    }
    
}

