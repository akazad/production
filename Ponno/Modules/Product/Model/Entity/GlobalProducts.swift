//
//  GlobalProducts.swift
//  Ponno
//
//  Created by a k azad on 20/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class GlobalProducts : Mappable {
    var id : Int = -1
    var name : String = ""
    var categoryId : Int = -1
    var categoryName : String = ""
    var company : String?
    var variant : String?
    var unit : Int = -1
    var unitName : String = ""
    var sku : String?
    var image : String?
    var pharmacyId : Int?
    var createdAt : String?
    var updatedAt : String?
    var deletedAt : String?
    var shopCategory : Int?

    required init(map: Map) {

    }
    
    init(name: String) {
        self.name = name
    }

    func mapping(map: Map) {

        id <- map["id"]
        name <- map["name"]
        categoryId <- map["category_id"]
        categoryName <- map["category_name"]
        company <- map["company"]
        variant <- map["variant"]
        unit <- map["unit"]
        unitName <- map["unit_name"]
        sku <- map["sku"]
        image <- map["image"]
        pharmacyId <- map["pharmacy_id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
        shopCategory <- map["shop_category"]
    }

}

