//
//  ProductCategoryListDataMapper.swift
//  Ponno
//
//  Created by a k azad on 13/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ProductCategoryListDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var categories : [ProductCategories]? 
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        categories <- map["categories"]
    }
    
}
