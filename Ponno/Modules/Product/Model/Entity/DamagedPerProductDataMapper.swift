//
//  DamagedPerProductDataMapper.swift
//  Ponno
//
//  Created by a k azad on 25/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DamagedPerProductDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var productName : String = ""
    var damagedPerProductList : [DamagedPerProductList]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        productName <- map["product_name"]
        damagedPerProductList <- map["damaged_product_list"]
        pagination <- map["pagination"]
    }
    
}
