//
//  ProductCategories.swift
//  Ponno
//
//  Created by a k azad on 13/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ProductCategories : Mappable {
    var id : Int = 0
    var name : String = ""
    var parent : Int = 0
    var gen : Int = 0
    
    required init(map: Map) {
        
    }
    
    init(id: Int, name: String, gen: Int, parent: Int) {
        self.id = id
        self.name = name
        self.parent = parent
        self.gen = gen
    }
    
//    func initWithAllCategory(name: String){
//        self.name = name
//    }
    
     func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        parent <- map["parent"]
        gen <- map["gen"]
    }
    
}

