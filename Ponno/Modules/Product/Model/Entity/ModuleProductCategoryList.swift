//
//  ModuleProductCategoryList.swift
//  Ponno
//
//  Created by a k azad on 23/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class ModuleProductCategoryList : Mappable {
    var id : Int?
    var name : String?
    var gen: Int?
    var parent: Int?
    var path : String?
    var deletable: Bool?
    var createdAt : String?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        id <- map["id"]
        name <- map["name"]
        gen <- map["gen"]
        parent <- map["parent"]
        path <- map["path"]
        deletable <- map["deletable"]
        createdAt <- map["created_at"]
    }

}

