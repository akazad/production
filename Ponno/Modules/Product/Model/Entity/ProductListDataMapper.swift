//
//  ProductListDataMapper.swift
//  Ponno
//
//  Created by a k azad on 26/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
//import Foundation
//import ObjectMapper
//
//class ProductListDataMapper : Mappable {
//    var success : Bool?
//    var message : String?
//    var totalProducts : Int?
//    var totalPrice : Int?
//    var totalBuyingPrice : Int?
//    var totalCategory : Int?
//    var productList : [ProductList]?
//    var currentPage : Int?
//    var currentPageItem : Int?
//    var to : Int?
//    var from : Int?
//    var perPage : Int?
//    var total : Int?
//    var hasMorePage : Bool?
//    var lastPageNo : Int?
//    var firstPageUrl : String?
//    var nextPageUrl : String?
//    var prevPageUrl : String?
//    var lastPageUrl : String?
//
//    required init(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//
//        success <- map["success"]
//        message <- map["message"]
//        totalProducts <- map["total_products"]
//        totalPrice <- map["total_price"]
//        totalBuyingPrice <- map["total_buying_price"]
//        totalCategory <- map["total_category"]
//        productList <- map["product_list"]
//        currentPage <- map["current_page"]
//        currentPageItem <- map["current_page_item"]
//        to <- map["to"]
//        from <- map["from"]
//        perPage <- map["per_page"]
//        total <- map["total"]
//        hasMorePage <- map["has_more_page"]
//        lastPageNo <- map["last_page_no"]
//        firstPageUrl <- map["first_page_url"]
//        nextPageUrl <- map["next_page_url"]
//        prevPageUrl <- map["prev_page_url"]
//        lastPageUrl <- map["last_page_url"]
//    }
//
//}

//struct ProductResponse: Codable {
//    let success: Bool
//    let message: String
//    let totalProducts: Int
//    let totalPrice: Int
//    let totalBuyingPrice: Int
//    let totalCategory: Int
//    let productList: [ProductList]
//    let currentPage, currentPageItem, to, from: Int
//    let perPage, total: Int
//    let hasMorePage: Bool
//    let lastPageNo: Int
//    let firstPageUrl, nextPageUrl: String
//    let prevPageUrl: String?
//    let lastPageUrl: String
//    
////    enum CodingKeys: String, CodingKey {
////        case success, message
////        case totalProducts
////        case totalPrice
////        case totalBuyingPrice
////        case totalCategory
////        case productList
////        case currentPage
////        case currentPageItem
////        case to, from
////        case perPage
////        case total
////        case hasMorePage
////        case lastPageNo
////        case firstPageUrl
////        case nextPageUrl
////        case prevPageUrl
////        case lastPageUrl
////    }
//
//}
//
//

import Foundation
import ObjectMapper

class ProductListDataMapper : Mappable { 
    var success : Bool?
    var message : String?
    var summary : [ProductListSummery]?
    var productList : [ProductList]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        productList <- map["product_list"]
        pagination <- map["pagination"]
    }
    
}
