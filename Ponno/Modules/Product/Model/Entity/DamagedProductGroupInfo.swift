//
//  DamagedProductGroupInfo.swift
//  Ponno
//
//  Created by a k azad on 24/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DamagedProductGroupInfo : Mappable {
    var id : Int = -1
    var damagedProductId : Int = -1
    var damagedProductName : String = ""
    var unit : String = ""
    var damagedLot : Int = 0
    var totalLoss : Int = 0
    var totalQuantity : Int = 0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        damagedProductId <- map["damaged_product_id"]
        damagedProductName <- map["damaged_product_name"]
        unit <- map["unit"]
        damagedLot <- map["damaged_lot"]
        totalLoss <- map["total_loss"]
        totalQuantity <- map["total_quantity"]
    }
    
}
