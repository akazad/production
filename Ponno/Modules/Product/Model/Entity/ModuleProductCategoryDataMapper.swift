//
//  ModuleProductCategoryDataMapper.swift
//  Ponno
//
//  Created by a k azad on 23/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class ModuleProductCategoryDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var moduleProductCategoryList : [ModuleProductCategoryList]?
    var pagination : Pagination?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        moduleProductCategoryList <- map["product_category_list"]
        pagination <- map["pagination"]
    }

}
