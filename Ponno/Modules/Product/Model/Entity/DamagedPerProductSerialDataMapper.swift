//
//  DamagedPerProductSerialDataMapper.swift
//  Ponno
//
//  Created by a k azad on 5/10/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class DamagedPerProductSerialDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var productDamagedSerials : [Int]?
    var productsSerials : [ProductsSerials]? 
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        productDamagedSerials <- map["product_serials"]
        productsSerials <- map["all_products"]
    }
    
}
