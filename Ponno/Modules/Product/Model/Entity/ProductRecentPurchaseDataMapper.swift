//
//  ProductRecentPurchaseDataMapper.swift
//  Ponno
//
//  Created by a k azad on 1/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ProductRecentPurchaseDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var purchases : [ProductRecentPurchase]? 
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        purchases <- map["purchases"]
        pagination <- map["pagination"]
    }
    
}

