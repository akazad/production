//
//  Sales.swift
//  Ponno
//
//  Created by a k azad on 14/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ProductRecentSales : Mappable {
    var createdAt : String = ""
    var quantity : String = "" 
    var totalPrice : String = ""
    var invoiceId : Int = 0
    var invoice : String = ""
    var returnCash : String = ""
    var customerName : String = ""
    var unit : String = ""
    
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        createdAt <- map["created_at"]
        quantity <- map["quantity"]
        totalPrice <- map["total_price"]
        invoice <- map["invoice"]
        invoiceId <- map["invoice_id"]
        returnCash <- map["return_cash"]
        customerName <- map["customer_name"]
        unit <- map["unit"]
    }
    
}

