//
//  ProductForDamageDataMapper.swift
//  Ponno
//
//  Created by a k azad on 5/10/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class ProductForDamageDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var inventories : [ProductForDamage]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        inventories <- map["inventories"]
        pagination <- map["pagination"]
    }
    
}
