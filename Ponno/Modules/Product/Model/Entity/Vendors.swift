//
//  Vendors.swift
//  Ponno
//
//  Created by a k azad on 14/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class Vendors : Mappable {
    var id : Int = 0
    var name : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
    }
    
}

