//
//  DamagedPerProductList.swift
//  Ponno
//
//  Created by a k azad on 25/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DamagedPerProductList : Mappable {
    var id : Int = 0
    var quantity : String = ""
    var createdAt : String = ""
    var description : String = ""
    var serials : [String] = [""]
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        quantity <- map["quantity"]
        createdAt <- map["created_at"]
        description <- map["description"]
        serials <- map["serials"]
    }
    
}
