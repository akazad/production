//
//  Purchases.swift
//  Ponno
//
//  Created by a k azad on 14/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ProductRecentPurchase : Mappable {
    var id : Int = 0
    var createdAt : String = ""
    var quantity : String = ""
    var unit : String = ""
    var currentQuantity : String = ""
    var buyingPrice : String = ""
    var sellingPrice : String = ""
    var expireDate : String = ""
    var warranty : String = ""
    var serials : String = ""
    var vendorId: Int = 0
    var name : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        createdAt <- map["created_at"]
        quantity <- map["quantity"]
        unit <- map["unit"]
        currentQuantity <- map["current_quantity"]
        buyingPrice <- map["buying_price"]
        sellingPrice <- map["selling_price"]
        expireDate <- map["expire_date"]
        warranty <- map["warranty"]
        serials <- map["serials"]
        vendorId <- map["vendor_id"]
        name <- map["name"]
    }
    
}

