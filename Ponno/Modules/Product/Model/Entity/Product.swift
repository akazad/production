//
//  Product.swift
//  Ponno
//
//  Created by a k azad on 5/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class Product : Mappable {
    var productId : Int = 0
    var name : String = ""
    var image : String = ""
    var company : String = ""
    var variant : String = ""
    var sku : String = ""
    var pharmacyId: Int = 0
    var categoryId : Int = 0
    var categoryName : String = ""
    var inventoryId : Int = 0
    var sellingPrice : String = ""
    var buyingPrice : String = ""
    var quantity : String = ""
    var unitId: Int = 0
    var unit : String = ""
    var hasSerial : Int = 0
    var stockAlert : Int?
    var sellableStockPrice : String = ""
    var totalStockPrice : String = ""
    var totalSell : Double = 0.0
    var totalSoldAmount: Int = 0
    
    
    required init(map: Map) {
        
    }
    
    init(sellingPrice : String) {
        self.sellingPrice = sellingPrice
    }
    
     func mapping(map: Map) {
        
        productId <- map["product_id"]
        name <- map["name"]
        image <- map["image"]
        company <- map["company"]
        variant <- map["variant"]
        sku <- map["sku"]
        pharmacyId <- map["pharmacy_id"]
        categoryId <- map["category_id"]
        categoryName <- map["category_name"]
        inventoryId <- map["inventory_id"]
        sellingPrice <- map["selling_price"]
        buyingPrice <- map["buying_price"]
        quantity <- map["quantity"]
        unitId <- map["unit_id"]
        unit <- map ["unit"]
        hasSerial <- map ["has_serial"]
        stockAlert <- map["stock_alert"]
        sellableStockPrice <- map["sellable_stock_price"]
        totalStockPrice <- map["total_stock_price"]
        totalSell <- map["total_sell"]
        totalSoldAmount <- map["total_sold_amount"]
    }
    
}
