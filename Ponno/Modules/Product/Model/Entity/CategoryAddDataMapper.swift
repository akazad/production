//
//  CategoryAddDataMapper.swift
//  Ponno
//
//  Created by a k azad on 10/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class CategoryAddDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var category : CategoryAdd?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        category <- map["category"]
    }
    
}

