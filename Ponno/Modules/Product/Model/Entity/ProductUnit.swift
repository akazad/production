//
//  ProductUnit.swift
//  Ponno
//
//  Created by a k azad on 13/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ProductUnit : Mappable {
    var id : Int = 0
    var name : String  = ""
    var createdAt : String = ""
    var updatedAt : String = ""
    
    required init(map: Map) {
        
    }
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
    }
    
}

