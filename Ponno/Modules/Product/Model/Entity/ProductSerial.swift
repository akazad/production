//
//  ProductSerial.swift
//  Ponno
//
//  Created by a k azad on 14/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ProductSerial : Mappable {
    var id : Int = 0
    var inventoryId : Int = 0
    var inventoryHistoryId : Int = 0
    var productId : Int = 0
    var serialNo : String = ""
    var isSold : Int = 0
    var createdAt : String = ""
    var updatedAt : String = ""
    var pharmacyId : Int = 0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        inventoryId <- map["inventory_id"]
        inventoryHistoryId <- map["inventory_history_id"]
        productId <- map["product_id"]
        serialNo <- map["serial_no"]
        isSold <- map["is_sold"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        pharmacyId <- map["pharmacy_id"]
    }
    
}

