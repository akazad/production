//
//  GlobalProductDataMapper.swift
//  Ponno
//
//  Created by a k azad on 20/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class GlobalProductDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var globalProductList : [GlobalProducts]?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        globalProductList <- map["global_product_list"]
    }

}
