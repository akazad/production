//
//  ProductsSerials.swift
//  Ponno
//
//  Created by a k azad on 5/10/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class ProductsSerials : Mappable {
    var id : Int = 0
    var serialNo : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        serialNo <- map["serial_no"]
    }
    
}
