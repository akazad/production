//
//  SerialSearchDataMapper.swift
//  Ponno
//
//  Created by a k azad on 30/11/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SerialSearchDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var serials : [String]?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        serials <- map["serials"]
    }

}

