//
//  ProductForDamage.swift
//  Ponno
//
//  Created by a k azad on 5/10/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class ProductForDamage : Mappable {
    var id : Int = 0
    var productId : Int = 0
    var productName : String = ""
    var unit : String = ""
    var quantity : String = ""
    var buyingPrice : String = ""
    var sellingPrice : String = ""
    var variant : String = ""
    var categoryName : String = ""
    var image : String = ""
    var serials : [ProductForDamageSerials]? 
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        productId <- map["product_id"]
        productName <- map["product_name"]
        unit <- map["unit"]
        quantity <- map["quantity"]
        buyingPrice <- map["buying_price"]
        sellingPrice <- map["selling_price"]
        variant <- map["variant"]
        categoryName <- map["category_name"]
        image <- map["image"]
        serials <- map["serials"]
    }
    
}
