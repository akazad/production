//
//  ResponseProduct.swift
//  Ponno
//
//  Created by a k azad on 22/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseProduct : Mappable {
    var name : String?
    var category : Int?
    var company : String?
    var shopCategory : Int?
    var variant : String?
    var unit : Int?
    var sku : String?
    var image : String?
    var pharmacyId : Int?
    var updatedAt : String?
    var createdAt : String?
    var id : Int?
    var inventoryId : Int?
    var sellingPrice : String?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        name <- map["name"]
        category <- map["category"]
        company <- map["company"]
        shopCategory <- map["shop_category"]
        variant <- map["variant"]
        unit <- map["unit"]
        sku <- map["sku"]
        image <- map["image"]
        pharmacyId <- map["pharmacy_id"]
        updatedAt <- map["updated_at"]
        createdAt <- map["created_at"]
        id <- map["id"]
        inventoryId <- map["inventory_id"]
        sellingPrice <- map["selling_price"]
    }

}

