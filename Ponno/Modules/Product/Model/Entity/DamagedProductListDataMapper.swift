//
//  DamagedProductListDataMapper.swift
//  Ponno
//
//  Created by a k azad on 24/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class DamagedProductListDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var damagedProductGroupInfo : [DamagedProductGroupInfo]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        damagedProductGroupInfo <- map["damaged_product_group_info"]
        pagination <- map["pagination"]
    }
    
}
