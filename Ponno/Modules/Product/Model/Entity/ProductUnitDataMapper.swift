//
//  ProductUnitDataMapper.swift
//  Ponno
//
//  Created by a k azad on 13/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ProductUnitDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var productUnit : [ProductUnit]?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        productUnit <- map["product_unit"]
    }
    
}

