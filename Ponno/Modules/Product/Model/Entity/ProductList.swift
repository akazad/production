//
//  ProductList.swift
//  Ponno
//
//  Created by a k azad on 26/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
//import Foundation
//import ObjectMapper
//
//class ProductList : Mappable {
//    var id : String?
//    var quantity : String?
//    var sellingPrice : String?
//    var buyingPrice : String?
//    var name : String?
//    var company : String?
//    var variant : String?
//    var categoryName : String?
//
//    required init(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//
//        id <- map["id"]
//        quantity <- map["quantity"]
//        sellingPrice <- map["selling_price"]
//        buyingPrice <- map["buying_price"]
//        name <- map["name"]
//        company <- map["company"]
//        variant <- map["variant"]
//        categoryName <- map["category_name"]
//    }
//
//}

import Foundation
import ObjectMapper

class ProductList : Mappable { 
    var inventoryId : Int = 0
    var quantity : String = ""
    var preOrderQuantity : String = ""
    var unit : String = ""
    var sellingPrice : String = ""
    var buyingPrice : String = ""
    var stockAlert : Int = 0
    var productId : Int = 0
    var name : String = ""
    var company : String = ""
    var variant : String = ""
    var sku : String = ""
    var image : String = ""
    var categoryId : Int = 0
    var categoryName : String = ""
    var hasSerial : Int = 0
    var serials : String = ""
    var sellableStockPrice : String = ""
    var totalStockPrice : String = ""
    var soldQuantity : String = ""
    var soldSellingPrice : String = ""
    var soldSerialId : [String] = [""]
    var soldSerialNo : [String] = [""]
    
    required init(map: Map) {
        
    }
    
    init(productId: Int, quantity: String, sellingPrice: String, hasSerial: Int, serials: String, name: String, image: String, categoryName: String, varient: String, unit: String){
        self.productId = productId
        self.quantity = quantity
        self.sellingPrice = sellingPrice
        self.serials = serials
        self.hasSerial = hasSerial
        self.name = name
        self.image = image
        self.categoryName = categoryName
        self.variant = varient
        self.unit = unit
    }
    
    init(inventoryId: Int, productId: Int, quantity: String, sellingPrice: String, hasSerial: Int, serials: String, name: String, image: String, categoryName: String, categoryId: Int, variant: String, unitName: String) {
        self.inventoryId = inventoryId
        self.productId = productId
        self.quantity = quantity
        self.sellingPrice = sellingPrice
        self.hasSerial = hasSerial
        self.serials = serials
        self.name = name
        self.image = image
        self.categoryName = categoryName
        self.categoryId = categoryId
        self.variant = variant
        self.unit = unitName
    }
    
    init(inventoryId: Int,productId: Int, quantity: String, sellingPrice: String, soldQuantity: String, soldSellingPrice: String, hasSerial: Int, serials: String, soldSerials: [String], name: String, image: String, categoryName: String, categoryId: Int, variant: String, unitName: String) {
        self.inventoryId = inventoryId
        self.productId = productId
        self.quantity = quantity
        self.soldQuantity = soldQuantity
        self.soldSellingPrice = soldSellingPrice
        self.sellingPrice = sellingPrice
        self.hasSerial = hasSerial
        self.serials = serials
        self.soldSerialNo = soldSerials
        self.name = name
        self.image = image
        self.categoryName = categoryName
        self.categoryId = categoryId
        self.variant = variant
        self.unit = unitName
    }
    
    init(inventoryId: Int,productId: Int, quantity: String, buyingPrice: String, sellingPrice: String, soldQuantity: String, soldSellingPrice: String, hasSerial: Int, serials: String, soldSerials: [String], name: String, image: String, categoryName: String, categoryId: Int, variant: String, unitName: String) {
        self.inventoryId = inventoryId
        self.productId = productId
        self.quantity = quantity
        self.buyingPrice = buyingPrice
        self.soldQuantity = soldQuantity
        self.soldSellingPrice = soldSellingPrice
        self.sellingPrice = sellingPrice
        self.hasSerial = hasSerial
        self.serials = serials
        self.soldSerialNo = soldSerials
        self.name = name
        self.image = image
        self.categoryName = categoryName
        self.categoryId = categoryId
        self.variant = variant
        self.unit = unitName
    }
    
     func mapping(map: Map) {
        
        inventoryId <- map["inventory_id"]
        quantity <- map["quantity"]
        preOrderQuantity <- map["pre_order_quantity"]
        unit <- map["unit"]
        sellingPrice <- map["selling_price"]
        buyingPrice <- map["buying_price"]
        stockAlert <- map["stock_alert"]
        productId <- map["product_id"]
        name <- map["name"]
        company <- map["company"]
        variant <- map["variant"]
        sku <- map["sku"]
        image <- map["image"]
        categoryId <- map["category_id"]
        categoryName <- map["category_name"]
        hasSerial <- map["has_serial"]
        serials <- map["serials"]
        sellableStockPrice <- map["sellable_stock_price"]
        totalStockPrice <- map["total_stock_price"]
    }
    
}
