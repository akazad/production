//
//  ProductViewDataMapper.swift
//  Ponno
//
//  Created by a k azad on 5/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ProductDetailsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var product : Product?
    var vendors : [Vendors]?
    var deletable: Bool?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        product <- map["product"]
        vendors <- map["vendors"]
        deletable <- map["deletable"]
    }
    
}
