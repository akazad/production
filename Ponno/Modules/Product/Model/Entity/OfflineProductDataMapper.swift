//
//  OfflineProductDataMapper.swift
//  Ponno
//
//  Created by a k azad on 19/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class OfflineProductDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var lastUpdatedTime : String?
    var newProductList : [OfflineProduct]?
    var updatedProductList : [OfflineProduct]?
    var deletedProductList : [OfflineProduct]?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        lastUpdatedTime <- map["last_updated_time"]
        newProductList <- map["new_product_list"]
        updatedProductList <- map["updated_product_list"]
        deletedProductList <- map["deleted_product_list"]
    }

}

