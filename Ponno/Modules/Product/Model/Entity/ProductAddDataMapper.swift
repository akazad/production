//
//  ProductAddDataMapper.swift
//  Ponno
//
//  Created by a k azad on 20/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ProductAddDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var product : ResponseProduct? 
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        product <- map["product"]
    }
    
}
