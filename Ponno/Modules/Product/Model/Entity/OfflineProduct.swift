//
//  OfflineProduct.swift
//  Ponno
//
//  Created by a k azad on 19/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class OfflineProduct : Mappable {
    var inventoryId : Int?
    //var quantity : String?
    var unit : String?
    var sellingPrice : String?
    //var buying_price : String?
    //var stock_alert : String?
    var productId : Int?
    var name : String?
    var company : String?
    var variant : String?
    var sku : String?
    var image : String?
    var categoryId : Int?
    var categoryName : String?
    //var has_serial : Int?
    var createdAt : String?
    var updatedAt : String?
    var deletedAt : String?
    //var serials : String?

    required init(map: Map) {

    }
    
//    init(inventoryId: Int, unit: String, sellingPrice: Dou){
//        
//    }

    func mapping(map: Map) {

        inventoryId <- map["inventory_id"]
        //quantity <- map["quantity"]
        unit <- map["unit"]
        sellingPrice <- map["selling_price"]
        //buyingPrice <- map["buying_price"]
        //stockAlert <- map["stock_alert"]
        productId <- map["product_id"]
        name <- map["name"]
        company <- map["company"]
        variant <- map["variant"]
        sku <- map["sku"]
        image <- map["image"]
        categoryId <- map["category_id"]
        categoryName <- map["category_name"]
        //has_serial <- map["has_serial"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
        //serials <- map["serials"]
    }

}

