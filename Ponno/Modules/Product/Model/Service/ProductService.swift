//
//  ProductService.swift
//  Ponno
//
//  Created by a k azad on 26/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper

public class ProductService : NSObject{
    
    func getProductListData(page: Int, success: @escaping (ProductListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let productURL = RestURL.sharedInstance.Product + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        let productHeaders = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(productURL, headers: productHeaders)
            .responseObject { (response: DataResponse<ProductListDataMapper>) in
                if let productResponse = response.result.value{
                    if let response = productResponse.productList, response.count > 0 {
                        success(productResponse)
                    }
                    else if let failureMessage = productResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getAllProductData(getAll: Bool, success: @escaping (ProductListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Product + RestURL.sharedInstance.getAllData(text: getAll)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ProductListDataMapper>) in
                if let productResponse = response.result.value{
                    if let response = productResponse.productList, response.count > 0 {
                        success(productResponse)
                    }
                    else if let failureMessage = productResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getProductViewData(id: Int, success: @escaping (ProductDetailsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let productViewURL = RestURL.sharedInstance.ProductView + RestURL.sharedInstance.getProductDetailsViewUrl(id: id)
        print(productViewURL)
        
        let productViewHeaders = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(productViewURL, headers: productViewHeaders)
            .responseObject { (response: DataResponse<ProductDetailsDataMapper>) in
                if let productResponse = response.result.value{
                    if productResponse.success == true{
                        success(productResponse)
                    }else if let failureMessage = productResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getProductRecentSale(page: Int, id: Int, success: @escaping (ProductRecentSaleDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductRecentSale + RestURL.sharedInstance.getProductDetailsViewUrl(id: id) + RestURL.sharedInstance.getPageNumber(page: page)
        print(url)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ProductRecentSaleDataMapper>) in
                if let productRecentSaleResponse = response.result.value{
                    if let recentSale = productRecentSaleResponse.sales, recentSale.count > 0 {
                        success(productRecentSaleResponse)
                    }else if let failureMessage = productRecentSaleResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getProductRecentPurchase(page: Int, id: Int, success: @escaping (ProductRecentPurchaseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductRecentPurchase + RestURL.sharedInstance.getProductDetailsViewUrl(id: id) + RestURL.sharedInstance.getProductRecentPurchaseParams(page: page)
        
        print(url)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ProductRecentPurchaseDataMapper>) in
                if let productRecentPurchaseResponse = response.result.value{
                    if let recentPurchase = productRecentPurchaseResponse.purchases, recentPurchase.count > 0 {
                        success(productRecentPurchaseResponse)
                    }else if let failureMessage = productRecentPurchaseResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getProductSerial(page : Int, id: Int, success: @escaping (ProductSerialDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductSerial  + RestURL.sharedInstance.getProductDetailsViewUrl(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(url)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ProductSerialDataMapper>) in
                if let productSerialResponse = response.result.value{
                    if let serial = productSerialResponse.serials, serial.count > 0 {
                        success(productSerialResponse)
                    }else if let failureMessage = productSerialResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getProductAllSerials(saleId: Int, productId: Int, success: @escaping (ProductSerialDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductSerial  + RestURL.sharedInstance.getProductDetailsViewUrl(id: productId) + RestURL.sharedInstance.getSaleId(id: saleId)
        print(url)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ProductSerialDataMapper>) in
                if let productSerialResponse = response.result.value{
                    if let serial = productSerialResponse.serials, serial.count > 0 {
                        success(productSerialResponse)
                    }else if let failureMessage = productSerialResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getProductCategoryListData(page : Int, success: @escaping (ProductCategoryListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductCategories
        print(url)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ProductCategoryListDataMapper>) in
                if let productCategoriesListResponse = response.result.value{
                    if let productCategory = productCategoriesListResponse.categories, productCategory.count > 0 {
                        success(productCategoriesListResponse)
                    }else if let failureMessage = productCategoriesListResponse.message {
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getAllProductCategoryListData(getAll : Bool, success: @escaping (ProductCategoryListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductCategories + RestURL.sharedInstance.getAllData(text: getAll)
        print(url)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ProductCategoryListDataMapper>) in
                if let productCategoriesListResponse = response.result.value{
                    if let productCategory = productCategoriesListResponse.categories, productCategory.count > 0 {
                        success(productCategoriesListResponse)
                    }else if let failureMessage = productCategoriesListResponse.message {
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getProductUnitData(success: @escaping (ProductUnitDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.ProductUnit
        print(URL)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(URL, headers: headers)
            .responseObject { (response: DataResponse<ProductUnitDataMapper>) in
                if let productUnitResponse = response.result.value{
                    if let productUnit = productUnitResponse.productUnit, productUnit.count > 0 {
                        success(productUnitResponse)
                    }else if let failureMessage =  productUnitResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getAllProductForDamageData(getAll: Bool, success: @escaping (ProductForDamageDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DamagedProduct + RestURL.sharedInstance.getAllData(text: getAll)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ProductForDamageDataMapper>) in
                if let productResponse = response.result.value{
                    if let response = productResponse.inventories, response.count > 0 {
                        success(productResponse)
                    }
                    else if let failureMessage = productResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getDamageProductData(page: Int, success: @escaping (DamagedProductListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.DamagedProduct + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(URL)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(URL, headers: headers)
            .responseObject { (response: DataResponse<DamagedProductListDataMapper>) in
                if let damagedProductResponse = response.result.value{
                    if let damageProduct = damagedProductResponse.damagedProductGroupInfo, damageProduct.count > 0 {
                        success(damagedProductResponse)
                    }else if let failureMessage =  damagedProductResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getDamagePerProductSerialData(id: Int, success: @escaping (DamagedPerProductSerialDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.DamagedProducts +  RestURL.sharedInstance.getCommonId(id: id)
        print(URL)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(URL, headers: headers)
            .responseObject { (response: DataResponse<DamagedPerProductSerialDataMapper>) in
                if let productSerialResponse = response.result.value{
                    if let damageProduct = productSerialResponse.productsSerials, damageProduct.count > 0 {
                        success(productSerialResponse)
                    }else if let failureMessage =  productSerialResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getDamageProductSearchData(page: Int, searchText: String, success: @escaping (DamagedProductListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.DamagedProduct + RestURL.sharedInstance.getSearchText(text: searchText) + RestURL.sharedInstance.getPageNumber(page: page)
        print(URL)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(URL, headers: headers)
            .responseObject { (response: DataResponse<DamagedProductListDataMapper>) in
                if let damagedProductResponse = response.result.value{
                    if let damageProduct = damagedProductResponse.damagedProductGroupInfo, damageProduct.count > 0 {
                        success(damagedProductResponse)
                    }else if let failureMessage =  damagedProductResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getDamagePerProductData(page: Int, id: Int, success: @escaping (DamagedPerProductDataMapper) -> (), failure : @escaping (String) -> ()){

        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }

        let URL = RestURL.sharedInstance.DamagedPerProduct + RestURL.sharedInstance.getCommonId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(URL)

        let headers = RestURL.sharedInstance.getCommonHeader()

        let request = Alamofire.request(URL, headers: headers)
            .responseObject { (response: DataResponse<DamagedPerProductDataMapper>) in
                if let damagedPerProductResponse = response.result.value{
                    if let damagePerProduct = damagedPerProductResponse.damagedPerProductList, damagePerProduct.count > 0 {
                        success(damagedPerProductResponse)
                    }else if let failureMessage =  damagedPerProductResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    //Mark: PostRequest
    func postNewProductData(params: [String: Any] , success: @escaping (ProductAddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }

        let URL = RestURL.sharedInstance.ProductAdd
        print(URL)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let request = Alamofire.request(URL, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<ProductAddDataMapper>) in
            if let productAddResponse = response.result.value {
                if productAddResponse.success == true{
                    success(productAddResponse)
                }else if let failureMessage = productAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    //ProductDelete
    func PostProductDeleteData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductDelete
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["inventory_id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let deleteProcductResponse = response.result.value {
                if deleteProcductResponse.success == true{
                    success(deleteProcductResponse)
                }else if let failureResponse = deleteProcductResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    //ProductUpdate
    func postProductUpdateData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let productUpdateResponse = response.result.value {
                if productUpdateResponse.success == true{
                    success(productUpdateResponse)
                }else if let failureMessage = productUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    //NewProductCategoryAdd
    func postProductCategoryAddData(params: [String: Any] , success: @escaping (CategoryAddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductCategoriesAdd
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<CategoryAddDataMapper>) in
            if let productcategoryAddResponse = response.result.value {
                if productcategoryAddResponse.success == true{
                    success(productcategoryAddResponse)
                }else if let failureMessage = productcategoryAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    //SearchGet
    
    func getProductSearchData(page: Int, searchString: String, success: @escaping (ProductListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let productURL = RestURL.sharedInstance.Product
         + RestURL.sharedInstance.getSearchText(text: searchString) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = productURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let productHeaders = RestURL.sharedInstance.getCommonHeader()

        let request = Alamofire.request(url, headers: productHeaders)
            .responseObject { (response: DataResponse<ProductListDataMapper>) in
                if let productResponse = response.result.value{
                    if productResponse.success == true{
                        if let response = productResponse.productList, response.count > 0 {
                            success(productResponse)
                        }
                        else if let failureMessage = productResponse.message{
                            failure(failureMessage)
                        }
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)

    }
    
    func getProductAdvanceSearchData(name: String, company: String, variant: String, category: String, dealer: String, success: @escaping (ProductListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let productURL = RestURL.sharedInstance.Product
            + RestURL.sharedInstance.advanceProductSearch(name: name, company: company, variant: variant, category: category, dealer: dealer)
        
        guard let url = productURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        print(url)
        
        let productHeaders = RestURL.sharedInstance.getCommonHeader()

        let request = Alamofire.request(url, headers: productHeaders)
            .responseObject { (response: DataResponse<ProductListDataMapper>) in
                if let productResponse = response.result.value{
                    if productResponse.success == true{
                        if let response = productResponse.productList, response.count > 0 {
                            success(productResponse)
                        }
                        else if let failureMessage = productResponse.message{
                            failure(failureMessage)
                        }
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)

    }
    
    func getProductSerialSearchData(searchString: String, success: @escaping (SerialSearchDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let productURL = RestURL.sharedInstance.ProductGellAllSerials
         + RestURL.sharedInstance.getSearchSerial(text: searchString)
        
        guard let url = productURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let productHeaders = RestURL.sharedInstance.getCommonHeader()

        let request = Alamofire.request(url, headers: productHeaders)
            .responseObject { (response: DataResponse<SerialSearchDataMapper>) in
                if let productResponse = response.result.value{
                    if productResponse.success == true{
                        if let response = productResponse.serials, response.count > 0 {
                            success(productResponse)
                        }
                        else if let failureMessage = productResponse.message{
                            failure(failureMessage)
                        }
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)

    }
    
    func cancelRunningRequests(){
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }
       // Alamofire.SessionManager.default.session.invalidateAndCancel()
    }
    

//        let request = Alamofire.request(productURL, method: .get, parameters: param as Parameters, encoding: JSONEncoding.default, headers: productHeaders).responseObject { (response: DataResponse<ProductListDataMapper>) in
//            if let productResponse = response.result.value{
//                if let response = productResponse.productList, response.count > 0 {
//                    success(productResponse)
//                }
//                else{
//                    failure("failure")
//                }
//            }
//            else {
//                failure("failure")
//            }
//        }
//        print(request)
//    }
    
    
    func postRecentReturnStockData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductReturnStock
        print(url)
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let productReturnResponse = response.result.value {
                if productReturnResponse.success == true{
                    success(productReturnResponse)
                }else if let failureMessage = productReturnResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postProductRecentStockUpdateData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductRecentStockUpdate
        print(url)
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let productRecentStockUpdateResponse = response.result.value {
                if productRecentStockUpdateResponse.success == true{
                    success(productRecentStockUpdateResponse)
                }else if let failureMessage = productRecentStockUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func PostProductRecentStockDeleteData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductRecentStockDelete
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["inventory_history_id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let stockDeleteResponse = response.result.value {
                if stockDeleteResponse.success == true{
                    success(stockDeleteResponse)
                }else if let failureResponse = stockDeleteResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func PostProductSerialUpdateData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ProductSerialUpdate
        let header = RestURL.sharedInstance.postCommonHeader()
                
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let serialUpdateResponse = response.result.value {
                if serialUpdateResponse.success == true{
                    success(serialUpdateResponse)
                }else if let failureResponse = serialUpdateResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func PostDamagedPerProductUpdateData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DamagedProductUpdate
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let updateResponse = response.result.value {
                if updateResponse.success == true{
                    success(updateResponse)
                }else if let failureResponse = updateResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func PostDamagedProductStoreData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DamagedProductStore
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let storeResponse = response.result.value {
                if storeResponse.success == true{
                    success(storeResponse)
                }else if let failureResponse = storeResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func deleteDamagedPerProductData(id: Int, success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DamagedProductDelete
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let damageProductDeleteResponse = response.result.value {
                if damageProductDeleteResponse.success == true{
                    success(damageProductDeleteResponse)
                }else if let failureMessage = damageProductDeleteResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    //Mark: ImageUpload
    func postUserImage(productId : Int,image : UIImage? ,  succeed: @escaping((AddDataMapper)->Void), failure : @escaping((String) -> Void)){
        
        let imageUploadUrl = RestURL.sharedInstance.ProductImageUpload
        let params = ["product_id" : "\(productId)"]
        
        let service = PostImageService()
        
        service.postImage(params: params, image: image, imageUrl: imageUploadUrl, succeed: { response in
            if let success = response.success, success == true{
                print(response.message ?? "")
                succeed(response)
            }
            else{
                failure(response.message ?? "")
            }
        }, failure: {message in
            failure(message)
        })
    }
    
    func getOfflineProductData(timesStamp: String, success: @escaping (OfflineProductDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let productURL = RestURL.sharedInstance.Product
         + RestURL.sharedInstance.getProductListOfflineWithTimeStamp(timeStamp: timesStamp)
        
        guard let url = productURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let productHeaders = RestURL.sharedInstance.getCommonHeader()

        let request = Alamofire.request(url, headers: productHeaders)
            .responseObject { (response: DataResponse<OfflineProductDataMapper>) in
                if let productResponse = response.result.value{
                    if productResponse.success == true{
                        if let timeStamp = productResponse.lastUpdatedTime{ 
                            let lastUpdatedTime = TimeStamp(timeStamp: timeStamp)
                            setTimeStamp(timeStamp: lastUpdatedTime)
                        }
                        success(productResponse)
                    }else if let failureMessage = productResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)

    }
    
    func getGlobalProductsData(success: @escaping (GlobalProductDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.Product + RestURL.sharedInstance.GlobalProducts
        print(URL)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(URL, headers: headers)
            .responseObject { (response: DataResponse<GlobalProductDataMapper>) in
                if let productResponse = response.result.value{
                    if let globalProduct = productResponse.globalProductList, globalProduct.count > 0 {
                        success(productResponse)
                    }else if let failureMessage =  productResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getModuleProductCategoryData(page: Int, success: @escaping (ModuleProductCategoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.ProductCategories + RestURL.sharedInstance.getModuleList() + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(URL)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(URL, headers: headers)
            .responseObject { (response: DataResponse<ModuleProductCategoryDataMapper>) in
                if let response = response.result.value{
                    if let moduleCategory = response.moduleProductCategoryList, moduleCategory.count > 0 {
                        success(response)
                    }else if let failureMessage =  response.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getAllModuleProductCategoryData(getAll: Bool, success: @escaping (ModuleProductCategoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.ProductCategories + RestURL.sharedInstance.getModuleList() + RestURL.sharedInstance.getAllData(text: getAll)
        print(URL)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(URL, headers: headers)
            .responseObject { (response: DataResponse<ModuleProductCategoryDataMapper>) in
                if let response = response.result.value{
                    if let moduleCategory = response.moduleProductCategoryList, moduleCategory.count > 0 {
                        success(response)
                    }else if let failureMessage =  response.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getModuleProductCategorySearchData(page: Int, searchText: String, success: @escaping (ModuleProductCategoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.ProductCategories + RestURL.sharedInstance.getModuleList() +  RestURL.sharedInstance.getSearchText(text: searchText) + RestURL.sharedInstance.getPageNumber(page: page)
        print(URL)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(URL, headers: headers)
            .responseObject { (response: DataResponse<ModuleProductCategoryDataMapper>) in
                if let response = response.result.value{
                    if let moduleCategory = response.moduleProductCategoryList, moduleCategory.count > 0 {
                        success(response)
                    }else if let failureMessage =  response.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func PostCategoryUpdateData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.CategoryUpdate
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let updateResponse = response.result.value {
                if updateResponse.success == true{
                    success(updateResponse)
                }else if let failureResponse = updateResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func PostCategoryDeleteData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.CategoryDelete
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let deleteResponse = response.result.value {
                if deleteResponse.success == true{
                    success(deleteResponse)
                }else if let failureResponse = deleteResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
}
