//
//  DueService.swift
//  Ponno
//
//  Created by a k azad on 24/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

public class DueService : NSObject{
    
    func getDueData(page: Int, success: @escaping (DueDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let dueURL = RestURL.sharedInstance.DueList + RestURL.sharedInstance.getDueParams(page: page)
        print(dueURL)
        
        let dueHeader = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(dueURL, headers: dueHeader)
            .responseObject { (response: DataResponse<DueDataMapper>) in
                if let dueResponse = response.result.value {
                    if dueResponse.success == true{
                        success(dueResponse)
                    }
                    else if let failureMessage = dueResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getDueLogSearchData(page: Int, startDate: String, endDate: String, success: @escaping (DueDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let dueURL = RestURL.sharedInstance.DueList + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getPageNumber(page: page)
        
        print(dueURL)
        
        let dueHeader = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(dueURL, headers: dueHeader)
            .responseObject { (response: DataResponse<DueDataMapper>) in
                if let dueResponse = response.result.value {
                    if dueResponse.success == true{
                        success(dueResponse)
                    }
                    else if let failureMessage = dueResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getDueCustomerData(page: Int, success: @escaping (DueCustomerDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DueCustomerList + RestURL.sharedInstance.getDueParams(page: page)
            //print("Hello",dueCustomerURL)
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<DueCustomerDataMapper>) in
                if let dueCustomerResponse = response.result.value{
                    if dueCustomerResponse.success == true {
                        if let customerResponse = dueCustomerResponse.dueCustomerList, customerResponse.count > 0{
                            success(dueCustomerResponse)
                        }else if let failureMessage = dueCustomerResponse.message{
                            failure(failureMessage)
                        }
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getAllDueCustomerData(getAll: Bool, success: @escaping (DueCustomerDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DueCustomerList + RestURL.sharedInstance.getAllData(text: getAll)
        //print("Hello",dueCustomerURL)
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<DueCustomerDataMapper>) in
                if let dueCustomerResponse = response.result.value{
                    if let customerResponse = dueCustomerResponse.dueCustomerList, customerResponse.count > 0{
                        if dueCustomerResponse.success == true {
                            success(dueCustomerResponse)
                        }else if let failureMessage = dueCustomerResponse.message{
                            failure(failureMessage)
                        }
                    }else{
                        failure(CommonMessages.ApiFailure)
                    }
                }
        }
    }
    
    func getDueCustomerSearchData(page: Int, searchText: String, success: @escaping (DueCustomerDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let dueCustomerURL = RestURL.sharedInstance.DueCustomerList + RestURL.sharedInstance.getSearchText(text: searchText) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = dueCustomerURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<DueCustomerDataMapper>) in
                if let dueCustomerResponse = response.result.value{
                    if let customerResponse = dueCustomerResponse.dueCustomerList, customerResponse.count > 0{
                        if dueCustomerResponse.success == true{
                            success(dueCustomerResponse)
                        }
                    }else if let failureResponse = dueCustomerResponse.message{
                        failure(failureResponse)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
            }
    }
    
    //Mark: PostRequest
    func postNewDueData(dueData: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DueAdd
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = dueData
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let dueAddResponse = response.result.value{
                if dueAddResponse.success == true{
                    success(dueAddResponse)  
                }else if let failureMessage = dueAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    //SwipeToDeleteRequest
    func deleteDueData(id : Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DueDelete
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id" : id]
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let dueDeleteResponse = response.result.value {
                if dueDeleteResponse.success == true{
                    success(dueDeleteResponse)
                }else if let failureMessage = dueDeleteResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postDueUpdate(updateData : [String : Any], success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DueUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = updateData
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let dueUpdateResponse = response.result.value{
                if dueUpdateResponse.success == true{
                    success(dueUpdateResponse)
                }else if let failureMessage = dueUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    //Mark: PostRequest
    func postCustomerPaidData(paidData: [String: Any] , success: @escaping (ResponseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DueAdd
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = paidData
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<ResponseDataMapper>) in
            if let dueAddResponse = response.result.value{
                if dueAddResponse.success == true{
                    success(dueAddResponse)
                }else if let failureMessage = dueAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
}
