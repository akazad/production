//
//  DueCustomerList.swift
//  Ponno
//
//  Created by a k azad on 2/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DueCustomerList : Mappable {
    var id : Int = 0
    var name : String = ""
    var phone : String = ""
    var address : String = ""
    var currentDue : String = ""
    var image : String = ""
    
    required init(map: Map) {
        
    }
    
    init(id: Int, name: String, phone: String) {
        self.id = id
        self.name = name
        self.phone = phone
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        address <- map["address"]
        currentDue <- map["current_due"]
        image <- map["image"]
    }
    
}

