//
//  DueCustomerDataMapper.swift
//  Ponno
//
//  Created by a k azad on 2/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DueCustomerDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [DueSummary]?
    var dueCustomerList : [DueCustomerList]? 
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        dueCustomerList <- map["due_customer_list"]
        pagination <- map["pagination"]
    }
    
}

