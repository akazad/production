//
//  DueList.swift
//  Ponno
//
//  Created by a k azad on 24/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class DueList : Mappable { 
    var id : Int = 0
    var type : Int = 0
    var amount : String = ""
    var description : String = ""
    var createdAt : String = ""
    var customerId : Int = 0
    var customerName : String = ""
    var currentDue : String = ""
    var saleId : Int = 0
    var invoice : String = ""
    var withdrawAmount: String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        type <- map["type"]
        amount <- map["amount"]
        description <- map["description"]
        createdAt <- map["created_at"]
        customerId <- map["customer_id"]
        customerName <- map["customer_name"]
        currentDue <- map["current_due"]
        saleId <- map["sale_id"]
        invoice <- map["invoice"]
        withdrawAmount <- map["withdraw_amount"]
    }
    
}
