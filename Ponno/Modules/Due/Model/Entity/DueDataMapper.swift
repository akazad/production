//
//  DueDataMapper.swift
//  Ponno
//
//  Created by a k azad on 24/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class DueDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [DueSummary]?
    var dueList : [DueList]?
    var pagination: Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        dueList <- map["due_list"]
        pagination <- map["pagination"]
    }
    
}

