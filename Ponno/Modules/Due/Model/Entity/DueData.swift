//
//  DueData.swift
//  Ponno
//
//  Created by a k azad on 24/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class DueData : Mappable {
    var id : Int = 0
    var type : String = ""
    var customerId : String = ""
    var amount : String = ""
    var description : String = ""
    var saleId : String = ""
    var deliverySystem : String = ""
    var pharmacyId : String = ""
    var addedBy : String = ""
    var createdAt : String = ""
    var updatedAt : String = ""
    var customerData : DueCustomerData?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        type <- map["type"]
        customerId <- map["customer_id"]
        amount <- map["amount"]
        description <- map["description"]
        saleId <- map["sale_id"]
        deliverySystem <- map["delivery_system"]
        pharmacyId <- map["pharmacy_id"]
        addedBy <- map["added_by"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        customerData <- map["customer_data"]
    }
    
}

