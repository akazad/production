//
//  DueListCell.swift
//  Ponno
//
//  Created by a k azad on 28/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class DueCustomerListCell: UITableViewCell {
    
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var firstAlphabetView: UIView!
    @IBOutlet var imageIcon: CircularImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var currentDueLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dueCustomerPopUpBtn: UIButton!
    
    var dueCustomerList: DueCustomerList?{
        didSet{
            if let item = dueCustomerList{
                imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.customerImage + item.image), placeholderImage: UIImage(named: "placeholder"))
                nameLabel.text = item.name
                
                let currentDue = item.currentDue
                if currentDue.isEmpty {
                    currentDueLabel.text = LanguageManager.CurrentDue + " : " + "\(0.00)" + " " + Constants.currencySymbol
                }else{
                    currentDueLabel.text = LanguageManager.CurrentDue + " : " + "\(item.currentDue)" + " " + Constants.currencySymbol
                }
                
                let phone = item.phone
                if phone.isEmpty{
                    phoneLabel.text = LanguageManager.Phone + " : " + "---"
                }else{
                    phoneLabel.text = LanguageManager.Phone + " : " + item.phone
                }
                
                let address = item.address
                if address.isEmpty {
                    addressLabel.text = LanguageManager.Address + " : " + "---"
                }else{
                    addressLabel.text = LanguageManager.Address + " : " + item.address
                }
                
            }
        }
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: DueCustomerListCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: backgroundCardView)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
}
