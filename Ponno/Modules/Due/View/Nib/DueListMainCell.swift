//
//  DueListMainCell.swift
//  Ponno
//
//  Created by a k azad on 2/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DueListMainCell: UITableViewCell {
    
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dueListPopUpBtn: UIButton!
    @IBOutlet weak var descriptionImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: backgroundCardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: DueListMainCell.self)
    }
    
//    func setUpCardView(uiview : UIView){
//        backgroundCardView.backgroundColor = UIColor.white
//        contentView.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
//        backgroundCardView.layer.cornerRadius = 5.0
//        backgroundCardView.layer.masksToBounds = false
//        
//        backgroundCardView.layer.shadowColor = (UIColor.black).withAlphaComponent(0.1).cgColor
//        backgroundCardView.layer.shadowOffset = CGSize(width: 0, height: 0)
//        backgroundCardView.layer.shadowOpacity = 0.8
//    }
}
