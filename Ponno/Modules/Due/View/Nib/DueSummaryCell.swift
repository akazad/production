//
//  DueSummaryCell.swift
//  Ponno
//
//  Created by a k azad on 15/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DueSummaryCell: UICollectionViewCell {

    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: DueSummaryCell.self)
    }
}
