//
//  DueCustomerViewController.swift
//  Ponno
//
//  Created by a k azad on 7/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

class DueCustomerViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = DuePresenter(service: DueService())
    
    var dueCustomerList: [DueCustomerList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var dueSummary : [DueSummary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPage : Int = 1
    
    var type : Int = 0
    var customerId : Int?
    var saleId: Int?
    var id : Int?
    var withdrawAmount : String?
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSlideMenuButton()
        self.setNavigationBarTitle()
        self.configureTableView()
        self.configureCollectionView()
        self.initialSetup()
        self.setBarButton()
        self.addFloaty()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.DueBook
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
    }
    
    func initialSetup(){
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarButton(){
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let dueAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        dueAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        dueAddBtn.addTarget(self, action: #selector(onDueAdd(sender:)), for: UIControl.Event.touchUpInside)
        dueAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        dueAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        dueAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: dueAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2,barButton1], animated: true)
    }
    
    @objc func onSearch(sender: UIButton){
        self.navigateToDueListSearchVC()
    }
    
    @objc func onDueAdd(sender: UIBarButtonItem){
        self.navigateToAddNewDueViewController()
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(DueViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.currentPage = 1
        self.dueCustomerList = []
        self.refreshTableView()
        self.presenter.getDueCustomerDataFromServer(page: self.currentPage)
        
        refreshControl.endRefreshing()
    }
    
}

extension DueCustomerViewController{
    func navigateToDueListSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DueListSearchViewController") as! DueListSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
//    func navigateToAddNewSaleViewController(){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
    
    func navigateToFreemiumSaleableViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumSaleableProductsVC") as! FreemiumSaleableProductsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewDueViewController(){
            let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDueViewController") as! AddNewDueViewController
    //        viewController.dueCustomerList = self.dueCustomerList
            self.type = 0
            viewController.postType = type
            viewController.dueState = DueState.dueAdd.rawValue
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
}

//Mark: Floaty Delegate
extension DueCustomerViewController: FloatyDelegate{
    func navigateToAddSaleVC() {
        //self.navigateToAddNewSaleViewController()
        self.navigateToFreemiumSaleableViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}

//MARK: CollectionView Delegate And DataSource
extension DueCustomerViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(DueSummaryCell.nib, forCellWithReuseIdentifier: DueSummaryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let dueItem = self.dueSummary, dueItem.count > 0 else {
            return 0
        }
        return dueItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : DueSummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: DueSummaryCell.identifier, for: indexPath) as! DueSummaryCell
        guard let dueItem = self.dueSummary, dueItem.count > 0 else{
            return cell
        }
        let dueSummaryList = dueItem[indexPath.row]
        
        cell.value.text = dueSummaryList.value
        cell.title.text = dueSummaryList.title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 60.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.dueSummary, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}


//MARK: TableView Delegate And DataSoruce
extension DueCustomerViewController : UITableViewDelegate, UITableViewDataSource{
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DueListMainCell.nib, forCellReuseIdentifier: DueListMainCell.identifier)
        self.tableView.register(DueCustomerListCell.nib, forCellReuseIdentifier: DueCustomerListCell.identifier)
        self.tableView.estimatedRowHeight = 112
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.separatorColor = UIColor.clear
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dueCustomerList.count > 0 {
            return self.dueCustomerList.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell : DueCustomerListCell = tableView.dequeueReusableCell(withIdentifier: DueCustomerListCell.identifier, for: indexPath) as! DueCustomerListCell
            cell.selectionStyle = .none
            if self.dueCustomerList.count > 0 {
                let item = self.dueCustomerList[indexPath.row]
                
                cell.dueCustomerList = item
                
                cell.dueCustomerPopUpBtn.tag = item.id
                cell.dueCustomerPopUpBtn.addTarget(self, action: #selector(onDueCustomerPopUpBtnTapped), for: .touchUpInside)
                
                if isLoading == false && indexPath.row == self.dueCustomerList
                    .count - 1 && self.currentPage < self.lastPage {
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getDueCustomerDataFromServer(page: self.currentPage)
                }
            }
            return cell
    }
    
    fileprivate func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //if !isDueList{
            //return 120
        //}
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.dueCustomerList.count > 0 {
            let customer = self.dueCustomerList[indexPath.row]
            self.navigateToCustomerDetailsVC(id: customer.id)
        }
    }
    
    @objc func onDueCustomerPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.dueCustomerList.count > 0 {
            self.customerId = sender.tag
            for dueCustomerItem in self.dueCustomerList{
                if self.customerId == dueCustomerItem.id{
                    let duePaidAction = UIAlertAction(title: LanguageManager.DueWithdraw, style: UIAlertAction.Style.default) { (action) in
                        self.customerId = dueCustomerItem.id
                        self.navigateToDuePaidVC(data: dueCustomerItem)
                    }
                    
                    let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToUpdateCustomerVC(data: dueCustomerItem)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(duePaidAction)
                    myActionSheet.addAction(updateAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
//    func displayMessage(userMessage:String) -> Void {
//        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
//
//            let OKAction = UIAlertAction(title: "OK", style: .default){
//                (action:UIAlertAction!) in
//                    //self.dueList = []
//                    self.getDueListData()
//            }
//            alertController.addAction(OKAction)
//            self.present(alertController, animated: true, completion: nil)
//    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.deleteDueLogDataFromServer(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }

}

//MARK: Api Delegate
extension DueCustomerViewController : DueViewDelegate {
    func updateDueData(data: AddDataMapper) {
//        guard let message = data.message else{
//            return
//        }
//        self.displayMessage(userMessage: message)
    }
    
    func setCustomerData(data: DueCustomerDataMapper) {
        guard let customer = data.dueCustomerList, customer.count > 0 else{
            return
        }
        self.isLoading = false
        self.dueCustomerList += customer
        
        guard let dueItem = data.summary else {
            return
        }
        self.dueSummary = dueItem
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        guard let pagination = data.pagination else{
            return
        }
        self.lastPage = pagination.lastPageNo
    }
    
    func setDueData(data: DueDataMapper) {
        //
        
    }
    
    func deleteDueLogData(data: AddDataMapper) {
//        guard let message = data.message else {
//            return
//        }
//        self.displayMessage(userMessage: message)
    }
    
    func onDueDeleteFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter() {
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.dueCustomerList = []
        self.currentPage = 1
        self.refreshTableView()
        self.presenter.getDueCustomerDataFromServer(page: self.currentPage)
        
    }
    
}

//Navigation
extension DueCustomerViewController{
    func navigateToCustomerDetailsVC(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerDBaseViewController") as! CustomerDBaseViewController
        viewController.customerId = id
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToEditDueVC(data : DueList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDueViewController") as! AddNewDueViewController
        viewController.postType = self.type
        viewController.customerId = customerId
        viewController.dueData = data
        viewController.dueState = DueState.dueEdit.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToSaleDetailsVC(id : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        viewController.iD = id
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToDuePaidVC(data: DueCustomerList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDueViewController") as! AddNewDueViewController
        self.type = 1
        viewController.postType = type
        viewController.customerId = customerId
        viewController.dueState = DueState.duePaid.rawValue
        viewController.dueCustomer = data
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToUpdateCustomerVC(data: DueCustomerList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerAddViewController") as! CustomerAddViewController
        viewController.customerState = CustomerState.DueCustomerUpdate.rawValue
        viewController.customerId = Int(data.id)
        viewController.dueCustomer = data
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
