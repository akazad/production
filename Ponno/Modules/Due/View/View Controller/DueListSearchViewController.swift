//
//  DueListSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 20/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class DueListSearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = DueListSearchPresenter(service: DueService())
    
    var dueCustomerList: [DueCustomerList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var searchText : String = ""
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    
    var type : Int = 0
    var customerId : Int?
    
    var lastPageNo: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        self.setUpSearchBar()
        self.configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
        self.setUpInitialLanguage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
                self.presenter.getDueCustomerSearchDataFromServer(page: self.currentPage, searchText: "")
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        
    }
    
}

//Mark: Search Delegate
extension DueListSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.SearchCustomer
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.dueCustomerList = []
        self.isLoading = true
        self.isSearchActive = true
        self.presenter.getDueCustomerSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
    }
}

//MARK: TableView Delegate And DataSoruce
extension DueListSearchViewController : UITableViewDelegate, UITableViewDataSource{
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DueCustomerListCell.nib, forCellReuseIdentifier: DueCustomerListCell.identifier)
        self.tableView.estimatedRowHeight = 112
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.separatorColor = UIColor.clear
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dueCustomerList.count > 0 {
            return self.dueCustomerList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DueCustomerListCell = tableView.dequeueReusableCell(withIdentifier: DueCustomerListCell.identifier, for: indexPath) as! DueCustomerListCell
        cell.selectionStyle = .none
        if self.dueCustomerList.count > 0 {
            let item = self.dueCustomerList[indexPath.row]
            
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.customerImage + item.image), placeholderImage: UIImage(named: "placeholder"))
            cell.nameLabel.text = item.name
            
            let currentDue = item.currentDue
            if currentDue.isEmpty {
                cell.currentDueLabel.text = LanguageManager.CurrentDue + " : " + "\(0.00)" + " " + Constants.currencySymbol
            }else{
                cell.currentDueLabel.text = LanguageManager.CurrentDue + " : " + "\(item.currentDue)" + " " + Constants.currencySymbol
            }
            
            let phone = item.phone
            if phone.isEmpty{
                cell.phoneLabel.text = LanguageManager.Phone + " : " + "---"
            }else{
                cell.phoneLabel.text = LanguageManager.Phone + " : " + item.phone
            }
            
            let address = item.address
            if address.isEmpty {
                cell.addressLabel.text = LanguageManager.Address + " : " + "---"
            }else{
                cell.addressLabel.text = LanguageManager.Address + " : " + item.address
            }
            
            cell.dueCustomerPopUpBtn.tag = item.id
            cell.dueCustomerPopUpBtn.addTarget(self, action: #selector(onDueCustomerPopUpBtnTapped), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.dueCustomerList
                .count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getDueCustomerSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
            }
        }
        return cell
    }

    fileprivate func refreshTableView(){
        self.tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.dueCustomerList.count > 0 {
            let customer = self.dueCustomerList[indexPath.row]
            self.navigateToCustomerDetailsVC(id: customer.id)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        

        let pay = UITableViewRowAction(style: .normal, title: "Due Withdraw") { action, index in
            
            if self.dueCustomerList.count > 0 {
                let list = self.dueCustomerList[indexPath.row]
                self.customerId = list.id
                self.navigateToDuePaidVC(data: list)
            }
        }
        pay.backgroundColor = UIColor.orange
        
        let update = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            if self.dueCustomerList.count > 0 {
                let list = self.dueCustomerList[indexPath.row]
                self.navigateToUpdateCustomerVC(data: list)
            }
        }
        update.backgroundColor = UIColor.red
        
        return [pay,update]
        
    }
    
    @objc func onDueCustomerPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.dueCustomerList.count > 0 {
            self.customerId = sender.tag
            for dueCustomerItem in self.dueCustomerList{
                if self.customerId == dueCustomerItem.id{
                    let duePaidAction = UIAlertAction(title: LanguageManager.DueWithdraw, style: UIAlertAction.Style.default) { (action) in
                        self.customerId = dueCustomerItem.id
                        self.navigateToDuePaidVC(data: dueCustomerItem)
                    }
                    
                    let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToUpdateCustomerVC(data: dueCustomerItem)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(duePaidAction)
                    myActionSheet.addAction(updateAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToCustomerDetailsVC(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerDBaseViewController") as! CustomerDBaseViewController
        viewController.customerId = id
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToEditDueVC(data : DueList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDueViewController") as! AddNewDueViewController
        self.type = 1
        viewController.postType = type
        viewController.customerId = customerId
        viewController.dueData = data
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToDuePaidVC(data: DueCustomerList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDueViewController") as! AddNewDueViewController
        self.type = 1
        viewController.postType = type
        viewController.customerId = customerId
        viewController.dueState = DueState.duePaid.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToUpdateCustomerVC(data: DueCustomerList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerAddViewController") as! CustomerAddViewController
        viewController.customerState = CustomerState.DueCustomerUpdate.rawValue
        viewController.customerId = Int(data.id)
        viewController.dueCustomer = data
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
//    func displayMessage(userMessage:String) -> Void {
//        DispatchQueue.main.async {
//            let alertController = UIAlertController(title: "Alert", message: userMessage, preferredStyle: .alert)
//
//            let OKAction = UIAlertAction(title: "OK", style: .default){
//                (action:UIAlertAction!) in
//                DispatchQueue.main.async {
//                    self.dismiss(animated: true, completion: nil)
//                    self.dueList = []
//                    self.refreshTableView()
//                    self.getDueListData()
//                }
//            }
//            alertController.addAction(OKAction)
//            self.present(alertController, animated: true, completion: nil)
//
//        }
//    }
    
//    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
//        DispatchQueue.main.async {
//            let alertController = UIAlertController(title: "সতর্কতা", message: userMessage, preferredStyle: .alert)
//            
//            let OKAction = UIAlertAction(title: "OK", style: .default){
//                (action:UIAlertAction!) in
//                self.dismiss(animated: true, completion: nil)
//                self.presenter.deleteDueLogDataFromServer(id: deleteId)
//            }
//            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
//                (action:UIAlertAction!) in
//                self.dismiss(animated: true, completion: nil)
//            }
//            alertController.addAction(OKAction)
//            alertController.addAction(cancelAction)
//            self.present(alertController, animated: true, completion: nil)
//            
//        }
//    }
    
}

//Mark: Api Delegate
extension DueListSearchViewController: DueListSearchViewDelegate{
    func setDueCustomerSearchData(data: DueCustomerDataMapper) {
        guard let list = data.dueCustomerList, list.count > 0 else {
            return
        }
        self.dueCustomerList += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.searchAlert(title: LanguageManager.NoInformationFound, message: data)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getDueCustomerSearchDataFromServer(page: self.currentPage, searchText: self.searchText)
    }
}
