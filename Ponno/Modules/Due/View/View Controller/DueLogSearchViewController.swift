//
//  DueLogSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 20/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DueLogSearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    
    var datePicker = UIDatePicker()
    
    private var presenter = DueLogSearchPresenter(service: DueService())
    
    var dueList : [DueList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    //var searchText : String = ""
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    
    var type : Int = 0
    var customerId : Int?
    var saleId: Int?
    var withdrawAmount : String?
    var id : Int?
    
    var lastPageNo: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetup()
        self.attachPresenter()
    }
    
    
    func initialSetup(){
        self.configureTableView()
        self.showStartDatePicker()
        self.showEndDatePicker()
        self.searchBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
                self.currentPage = 1
                self.presenter.getDueDataFromServer(page: self.currentPage)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        
    }
  
}

extension DueLogSearchViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            
            self.dueList = []
            self.currentPage = 1
            //self.searchIsActive = true
            
            self.presenter.getDueLogSearchDataFromServer(startDate: startDate, endDate: endDate, page: currentPage)
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

//MARK: TableView Delegate And DataSoruce
extension DueLogSearchViewController : UITableViewDelegate, UITableViewDataSource{
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DueListMainCell.nib, forCellReuseIdentifier: DueListMainCell.identifier)
        //self.tableView.register(DueCustomerListCell.nib, forCellReuseIdentifier: DueCustomerListCell.identifier)
        self.tableView.estimatedRowHeight = 112
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.separatorColor = UIColor.clear
        //self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dueList.count > 0 {
            return self.dueList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DueListMainCell = tableView.dequeueReusableCell(withIdentifier: DueListMainCell.identifier, for: indexPath) as! DueListMainCell
        cell.selectionStyle = .none
        if self.dueList.count > 0{
            let due = self.dueList[indexPath.row]
            
            cell.dateLabel.text = due.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_yy_h_mm_a.rawValue)
            cell.amountLabel.text = LanguageManager.Amount + " : " + "\(due.amount)" + " " + Constants.currencySymbol
            cell.nameLabel.text = LanguageManager.Customer + " : " + due.customerName
            
            let description = due.description
            if description.isEmpty{
                cell.descriptionImage.isHidden = true
            }else{
                cell.descriptionImage.isHidden = false
            }
            
            cell.descriptionImage.addTapGestureRecognizer {
                self.showAlert(title: LanguageManager.Description, message: description)
            }
            
            cell.dueListPopUpBtn.tag = due.id
            cell.dueListPopUpBtn.addTarget(self, action: #selector(onDueListPopUpBtnTapped), for: .touchUpInside)
            
            if due.type == 0 {
                cell.typeLabel.text = LanguageManager.Due
                cell.typeLabel.textColor = UIColor(red: CGFloat(220/255.0), green: CGFloat(53/255.0), blue: CGFloat(69/255.0), alpha: CGFloat(1.0))
                
            }
            if due.type == 1 {
                cell.typeLabel.text = LanguageManager.Withdraw
                cell.typeLabel.textColor = UIColor(red: CGFloat(91/255.0), green: CGFloat(160/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1.0))
                
            }
            if isLoading == false && indexPath.row == self.dueList
                .count - 1 {
                if self.currentPage < self.lastPageNo {
                    guard let startDate = self.datePickerTextField.text else{
                        return cell
                    }
                    guard let endDate = self.toDatePickerTextField.text else{
                        return cell
                    }
                    self.currentPage += 1
                    //self.salesList = []
                    self.presenter.getDueLogSearchDataFromServer(startDate: startDate, endDate: endDate, page: currentPage)
                }
            }
        }
        return cell
        
    }
    
    fileprivate func refreshTableView(){
        self.tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.dueList.count > 0{
            return
        }
        let due = self.dueList[indexPath.row]
        self.navigateToCustomerDetailsVC(id: due.customerId)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if self.dueList.count > 0 {
            let due = self.dueList[indexPath.row]
            
            self.saleId = due.saleId
            
            if saleId == 0{
                //edit = true
                let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
                    
                    self.customerId = due.customerId
                    self.navigateToEditDueVC(data: due)
                }
                edit.backgroundColor = UIColor.orange
                
                
                
                let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
                    
                    let dueId = due.id
                    self.confirmationMessage(userMessage: "Are you sure?", deleteId: dueId)
                }
                delete.backgroundColor = UIColor.lightRed
                
                return [delete, edit]
            }else{
                //edit = false
                
                let invoice = UITableViewRowAction(style: .normal, title: due.invoice) { action, index in
                    guard let id = self.saleId else{ 
                        return
                    }
                    self.navigateToSaleDetailsVC(id : id)
                    
                }
                invoice.backgroundColor = UIColor(red: 0, green: 100, blue: 0)
                
                return [invoice]
            }
            
            
        }
        
//        let edit = UITableViewRowAction(style: .normal, title: "পরিবর্তন") { action, index in
//            if self.dueList.count > 0 {
//                let due = self.dueList[indexPath.row]
//                self.customerId = due.customerId
//                self.navigateToEditDueVC(data: due)
//                //self.displayMessage(userMessage: "কাজটি অসম্পন্ন")
//            }
//        }
//        edit.backgroundColor = UIColor.orange
//        
//        let delete = UITableViewRowAction(style: .normal, title: "বাতিল") { action, index in
//            if self.dueList.count > 0 {
//                let due = self.dueList[indexPath.row]
//                let dueId = due.id
//                self.confirmationMessage(userMessage: "আপনি কি নিশ্চিত?", deleteId: dueId)
//            }
//        }
//        delete.backgroundColor = UIColor.red
//        
//        return [delete, edit]
        return [UITableViewRowAction()]
    }
    
    @objc func onDueListPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
            if self.dueList.count > 0{
                self.id = sender.tag
                for dueItem in self.dueList{
                    if self.id == dueItem.id{
                        self.saleId = dueItem.saleId
                        if self.saleId != 0{
                            let invoiceAction = UIAlertAction(title: dueItem.invoice, style: UIAlertAction.Style.default) { (action) in
                                guard let id = self.saleId else{
                                    return
                                }
                                self.navigateToSaleDetailsVC(id : id)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                
                                self.view.endEditing(true)
                            }
                            
                            cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                            
                            myActionSheet.addAction(invoiceAction)
                            myActionSheet.addAction(cancelAction)
                        } else{
                            self.withdrawAmount = dueItem.withdrawAmount
                            if withdrawAmount != "0.00"{
                                let withdrawAmountAction = UIAlertAction(title: LanguageManager.Paid + " : " + "\(self.withdrawAmount ?? "0.00")" + Constants.currencySymbol, style: UIAlertAction.Style.default) { (action) in
                                    
                                }
                                
                                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                    
                                    self.view.endEditing(true)
                                }
                                
                                cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                                
                                myActionSheet.addAction(withdrawAmountAction)
                                myActionSheet.addAction(cancelAction)
                            }else{
                                
                                let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                                    self.customerId = dueItem.customerId
                                    self.type = dueItem.type
                                    self.navigateToEditDueVC(data: dueItem)
                                }
                                
                                let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                                    let dueId = dueItem.id
                                    self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: dueId)
                                }
                                
                                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                    
                                    self.view.endEditing(true)
                                }
                                
                                cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                                
                                myActionSheet.addAction(editAction)
                                myActionSheet.addAction(deleteAction)
                                myActionSheet.addAction(cancelAction)
                            }
                        }
                        
                    }
                }
            }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
//    {
//        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
//
//        myActionSheet.view.tintColor = UIColor.black
//
//        if self.dueList.count > 0{
//            self.id = sender.tag
//            for dueItem in self.dueList{
//                if self.id == dueItem.id{
//                    self.saleId = dueItem.saleId
//                    if self.saleId != 0{
//                        let invoiceAction = UIAlertAction(title: dueItem.invoice, style: UIAlertAction.Style.default) { (action) in
//                            guard let id = self.saleId else{
//                                return
//                            }
//                            self.navigateToSaleDetailsVC(id : id)
//                        }
//
//                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
//
//                            self.view.endEditing(true)
//                        }
//
//                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
//
//                        myActionSheet.addAction(invoiceAction)
//                        myActionSheet.addAction(cancelAction)
//                    } else{
//                        self.withdrawAmount = dueItem.withdrawAmount
//                        if withdrawAmount != "0.00"{
//                            let withdrawAmountAction = UIAlertAction(title: "Paid : " + "\(self.withdrawAmount ?? "0.00")" + Constants.currencySymbol, style: UIAlertAction.Style.default) { (action) in
//
//                            }
//
//                            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
//
//                                self.view.endEditing(true)
//                            }
//
//                            cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
//
//                            myActionSheet.addAction(withdrawAmountAction)
//                            myActionSheet.addAction(cancelAction)
//                        }else{
//
//                            let editAction = UIAlertAction(title: "Edit", style: UIAlertAction.Style.default) { (action) in
//                                self.customerId = dueItem.customerId
//                                self.navigateToEditDueVC(data: dueItem)
//                            }
//
//                            let deleteAction = UIAlertAction(title: "Delete", style: UIAlertAction.Style.default) { (action) in
//                                let dueId = dueItem.id
//                                self.confirmationMessage(userMessage: "Are you sure?", deleteId: dueId)
//                            }
//
//                            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
//
//                                self.view.endEditing(true)
//                            }
//
//                            cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
//
//                            myActionSheet.addAction(editAction)
//                            myActionSheet.addAction(deleteAction)
//                            myActionSheet.addAction(cancelAction)
//                        }
//                    }
//
//                }
//            }
//        }
//        self.present(myActionSheet, animated: true, completion: nil)
//    }
    
    func navigateToCustomerDetailsVC(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerDetailsBaseViewController") as! CustomerDetailsBaseViewController
        viewController.customerId = id
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToEditDueVC(data : DueList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDueViewController") as! AddNewDueViewController
        self.type = 0
        viewController.postType = type
        viewController.customerId = customerId
        viewController.dueData = data
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToDuePaidVC(data: DueCustomerList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDueViewController") as! AddNewDueViewController
        self.type = 1
        viewController.postType = type
        viewController.customerId = customerId
        viewController.dueState = DueState.duePaid.rawValue
        viewController.dueCustomer = data
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToUpdateCustomerVC(data: DueCustomerList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerAddViewController") as! CustomerAddViewController
        viewController.customerState = CustomerState.DueCustomerUpdate.rawValue
        viewController.customerId = Int(data.id)
        viewController.dueCustomer = data
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleDetailsVC(id : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        viewController.iD = id
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.dueList = []
                    self.refreshTableView()
                    self.getDueListData()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.deleteDueLogDataFromServer(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        
    }
    
}


//MARK: Api Delegate
extension DueLogSearchViewController : DueLogSearchViewDelegate {
    func updateDueData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func setDueData(data: DueDataMapper) {
        
        guard let list = data.dueList , list.count > 0 else {
            return
        }
        self.isLoading = false
        self.dueList += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func deleteDueLogData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter() {
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.dueList = []
        self.presenter.getDueDataFromServer(page: currentPage)
    }
    
    func getDueListData(){
        self.isLoading = true
        self.currentPage = 1
        self.refreshTableView()
        self.presenter.getDueDataFromServer(page: self.currentPage)
    }
}
