//
//  EditDueViewController.swift
//  Ponno
//
//  Created by a k azad on 14/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class EditDueViewController: UIViewController {
    
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var customerNameTextField: UITextField!
    @IBOutlet weak var customerPhoneLabel: UILabel!
    @IBOutlet weak var customerPhoneTextField: UITextField!
    @IBOutlet weak var customerAddressLabel: UILabel!
    @IBOutlet weak var customerAddressTextField: UITextField!
    @IBOutlet weak var submitButtonLabel: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpFormLabel()
    }
    
    func setUpFormLabel(){
        self.title = "তথ্য হালনাগাদ"
        self.customerNameLabel.text = "কাস্টমারের নাম"
        self.customerPhoneLabel.text = "কাস্টমারের মোবাইল নাম্বার"
        self.customerAddressLabel.text = "কাস্টমারের ঠিকানা"
    }
}
