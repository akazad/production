//
//  AddNewDueViewController.swift
//  Ponno
//
//  Created by a k azad on 9/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

enum DueState: Int{
    case dueAdd
    case duePaid
    case dueEdit
}

class AddNewDueViewController: UIViewController { 
    
    @IBOutlet weak var customerLabel: UILabel!{
        didSet{
            customerLabel.text = LanguageManager.CustomerName
        }
    }
    @IBOutlet weak var customerListTextField: UITextField!{
        didSet{
            customerListTextField.placeholder = LanguageManager.CustomerName
        }
    }
    @IBOutlet weak var addCustomerBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var customerAddBtn: UIButton!{
        didSet{
            customerAddBtn.setTitle(LanguageManager.CustomerAdd, for: .normal)
        }
    }
    @IBOutlet weak var customerCurrentDueAmountLabel: UILabel!{
        didSet{
            customerCurrentDueAmountLabel.text = LanguageManager.CurrentDue
        }
    }
    @IBOutlet weak var customerCurrentDueAmountTextField: UITextField!{
        didSet{
            customerCurrentDueAmountTextField.placeholder = LanguageManager.CurrentDue
        }
    }
    @IBOutlet weak var currentDueTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var customerDueAmountLabel: UILabel!{
        didSet{
            customerDueAmountLabel.text = LanguageManager.DueAmount
        }
    }
    @IBOutlet weak var customerDueAmountTextField: UITextField!{
        didSet{
            customerDueAmountTextField.placeholder = LanguageManager.DueAmount
        }
    }
    @IBOutlet weak var dueDetailsLabel: UILabel!{
        didSet{
            dueDetailsLabel.text = LanguageManager.Description
        }
    }
    @IBOutlet weak var dueDetailsView: UITextView!
    @IBOutlet weak var submitButtonLabel: UIButton!
    
    private var presenter = AddNewDuePresenter(service: DueService())
    
    var dueCustomerList : [DueCustomerList] = []{
        didSet{
            self.categoryPicker.reloadAllComponents()
        }
    }
    var dueCustomer : DueCustomerList?
    var customer : [Customers] = []{
        didSet{
            self.categoryPicker.reloadAllComponents()
        }
    }
    var postType: Int?
    var dueData : DueList?
    var customerId : Int?
    var dueId : Int?
    var dueState = DueState.dueAdd.rawValue
    var currentDue : Double?
    var dueAmount : Double?
    var type : Int?
    
    let categoryPicker = UIPickerView()
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    var getAll: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpPickerView()
        self.initialSetup()
        self.attachPresenter()
    }
    func initialSetup(){
        
        self.customerAddBtn.addTarget(self, action: #selector(onCustomerAdd), for: .touchUpInside)
        
        if dueState == DueState.dueAdd.rawValue{
            self.title = LanguageManager.NewDue
            self.customerDueAmountLabel.text = LanguageManager.DueAmount
            self.customerCurrentDueAmountTextField.isHidden = true
            self.customerCurrentDueAmountLabel.isHidden = true
            guard let data = self.dueData else{
                return
            }
            self.customerListTextField.text = data.customerName
            
            self.customerDueAmountTextField.text = ""
        }else if dueState == DueState.dueEdit.rawValue{
            if let type = self.postType, type == 0 {
                self.title = LanguageManager.DueUpdate
                self.customerDueAmountLabel.text = LanguageManager.DueAmount
            }else{
                self.title = LanguageManager.WithdrawUpdate
                self.customerDueAmountLabel.text = LanguageManager.WithdrawnUpdate
            }
            
            self.customerListTextField.isEnabled = false
            self.customerCurrentDueAmountLabel.text = LanguageManager.CurrentDue
            self.customerCurrentDueAmountTextField.isEnabled = false
            
            self.addCustomerBtnHeight.constant = 0.0
            self.customerAddBtn.isHidden = true
            guard let data = self.dueData else{
                return
            }
            self.customerListTextField.text = data.customerName
            self.customerCurrentDueAmountTextField.text = data.currentDue
            self.customerCurrentDueAmountTextField.isEnabled = false
            self.customerDueAmountTextField.text = data.amount
            self.dueDetailsView.text = data.description
            self.dueId = data.id
            self.currentDue = Double(data.currentDue)
            self.dueAmount = Double(data.amount)
            self.type = data.type
            
        }
        else{
            self.title = LanguageManager.DueWithdraw
            self.customerAddBtn.isHidden = true
            self.customerLabel.text = LanguageManager.CustomerName
            self.customerDueAmountLabel.text = LanguageManager.CollectionAmount
            self.customerCurrentDueAmountLabel.text = LanguageManager.CurrentDue
            self.customerCurrentDueAmountTextField.isEnabled = false
            guard let data = self.dueCustomer else{
                return
            }
            self.customerListTextField.text = data.name
            self.customerListTextField.isEnabled = false
            self.customerCurrentDueAmountTextField.text = "\(data.currentDue)"
            self.customerCurrentDueAmountTextField.isEnabled = false
            self.customerDueAmountTextField.text = ""
            self.currentDue = Double(data.currentDue)
        }
        
        
    }
    
    @objc func onCustomerAdd(sender: UIButton){
        self.alertWithTextField()
    }
    
    func setUpPickerView(){
        self.customerListTextField.underlined()
        self.customerCurrentDueAmountTextField.underlined()
        self.customerDueAmountTextField.underlined()
        
        categoryPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.customerListTextField.inputView = categoryPicker
        self.customerListTextField.inputAccessoryView = toolBar
        
        let customerDueAmountToolbar = UIToolbar()
        customerDueAmountToolbar.barStyle = UIBarStyle.default
        customerDueAmountToolbar.isTranslucent = true
        customerDueAmountToolbar.tintColor = UIColor.black
        customerDueAmountToolbar.sizeToFit()
        
        let customerDueAmountToolbarDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCustomerDueAmount(sender:)))
        
        customerDueAmountToolbar.setItems([cancelButton,spaceButton, customerDueAmountToolbarDoneButton], animated: false)
        customerDueAmountToolbar.isUserInteractionEnabled = true
        
        self.customerDueAmountTextField.inputAccessoryView = customerDueAmountToolbar
        
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.customerDueAmountTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnCustomerDueAmount(sender: UIBarButtonItem){
        if let dueAmountText = self.customerDueAmountTextField.text, let dueAmount = Double(dueAmountText), let currentDue = self.currentDue{
            if dueState == DueState.dueEdit.rawValue{
                if self.type == 1 {
                    if let initialDueAmount = self.dueAmount{
                        if dueAmount > currentDue + initialDueAmount{
                            self.customerDueAmountTextField.text = "\(currentDue + initialDueAmount)"
                            
                        }
                    }
                }
            }else if dueState == DueState.duePaid.rawValue{
                if dueAmount == 0 {
                    self.showAlert(title: LanguageManager.AmountIsRequired, message: "")
                }else if dueAmount > currentDue{
                    self.customerDueAmountTextField.text = "\(currentDue )"
                }
            }
        }
        self.view.endEditing(true)
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @IBAction func dueSubmitButton(_ sender: UIButton) {
        if isValidated(){
            guard let id = self.customerId, let amount = self.customerDueAmountTextField.text, let description = self.dueDetailsView.text, let type = self.postType else {
                return
            }
            if dueState == DueState.dueAdd.rawValue{
                let dueDataArray: [String: Any] = ["customer_id": id, "type": type, "amount" : amount, "description": description]
                self.presenter.postNewDueDataToServer(dueData: dueDataArray)
            }else if dueState == DueState.dueEdit.rawValue{
                if let dueId = self.dueId, let dueType = self.type{
                    let param : [String : Any] = ["id": dueId,"customer_id": id, "type": dueType, "amount" : amount, "description": description]
                    self.presenter.postDueUpdateDataSource(param: param)
                }
                
            }else{
                let dueDataArray: [String: Any] = ["customer_id": id, "type": type, "amount" : amount, "description": description]
                self.presenter.postNewDueDataToServer(dueData: dueDataArray)
            }
        }
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitButtonLabel.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
        if (self.customerListTextField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.CustomerIsRequired, message: "")
            return false
        }else if let dueAmountText = self.customerDueAmountTextField.text, let dueAmount = Double(dueAmountText), let currentDue = self.currentDue{
            if dueState == DueState.dueEdit.rawValue{
                if self.type == 1 {
                    if let initialDueAmount = self.dueAmount{
                        if dueAmount > currentDue + initialDueAmount{
                            self.customerDueAmountTextField.text = "\(currentDue + initialDueAmount)"
                            self.showAlert(title: LanguageManager.WithdrawAmountIsTooMuch, message: "")
                            return false
                        }
                    }
                }
            }else if dueState == DueState.duePaid.rawValue{
                if dueAmount > currentDue{
                    self.customerDueAmountTextField.text = "\(currentDue )"
                    self.showAlert(title: LanguageManager.WithdrawAmountIsTooMuch, message: "")
                    return false
                }
            }
        }
        
        guard let dueAmountText = self.customerDueAmountTextField.text, let dueAmount = Double(dueAmountText) else{
            self.showAlert(title: LanguageManager.AmountIsRequired, message: "")
            return false
        }
        
        guard let currentDue = self.currentDue else{
            return false
        }
        
        if dueState == DueState.duePaid.rawValue{
            if dueAmount > currentDue{
                self.customerDueAmountTextField.text = "\(currentDue )"
                self.showAlert(title: LanguageManager.WithdrawAmountIsTooMuch, message: "")
                return false
            }
        }
        
        return true

    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: "", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.customerListTextField.resignFirstResponder()
                    self.customerDueAmountTextField.becomeFirstResponder()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
//    func showMessage(userMessage:String) -> Void {
//        DispatchQueue.main.async {
//            let alertController = UIAlertController(title: "Alert", message: userMessage, preferredStyle: .alert)
//
//            let OKAction = UIAlertAction(title: "OK", style: .default){
//                (action:UIAlertAction!) in
//                DispatchQueue.main.async {
//
//                   // self.presenter.getAllDueCustomerDataFromServer(self.getAll)
//                }
//            }
//            alertController.addAction(OKAction)
//            self.present(alertController, animated: true, completion: nil)
//
//        }
//    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    //self.navigationController?.popViewController(animated: true)
                    self.navigateToDueVC()
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
        
    }
    
    func navigateToDueVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DueViewController") as! DueViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewCustomer, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.CustomerName
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.PhoneNumber
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                //self.showAlert(title: , message: "")
                self.showMessage(userMessage: LanguageManager.CustomerIsRequired)
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                //self.showAlert(title: "কাস্টমারের ফোন নাম্বার অসম্পূর্ণ", message: "")
                self.showMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
                return
            }
            
//            if (!self.validatePhoneNumber(value: phone)){
//                self.showMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
//            }
            
            let param : [String : String] = ["name": textField.text!, "phone": textField2.text!, "address": ""]
            
            self.presenter.postAddNewCustomerDataFromServer(customerDataArray: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
  
}

//Mark: PickerViewDelegate
extension AddNewDueViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.customer.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
//        if isLoading == false && row == self.dueCustomerList.count - 1 && self.currentPage < self.lastPageNo {
//            self.currentPage += 1
//            self.isLoading = true
//            self.presenter.getDueCustomerDataFromServer(page: self.currentPage)
//        }
        self.customerId = self.customer[row].id
        self.customerListTextField.text = self.customer[row].name
        self.customerCurrentDueAmountTextField.text = "\(self.customer[row].currentDue)"
        self.currentDue = self.customer[row].currentDue.toDouble()
        return self.customer[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.customerListTextField.text = self.customer[row].name
        self.customerCurrentDueAmountTextField.text = "\(self.customer[row].currentDue)"
        self.currentDue = self.customer[row].currentDue.toDouble()
        self.customerId = self.customer[row].id
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.customerListTextField {
            textField.becomeFirstResponder()
        }
    }
}

//Mark: API Delegate
extension AddNewDueViewController : AddNewDueViewDelegate{
    func setCustomerData(data: CustomerDataMapper) {
        guard var customer = data.customers, customer.count > 0 else {
            return
        }
        self.isLoading = false
        if dueState == DueState.duePaid.rawValue{
            customer = customer.filter{$0.currentDue > "0.00"}
        }
        self.customer = customer
    }
    
    func setAddNewCustomerData(data: CustomerAddDataMapper) {
        guard let customer = data.customer else {
            return
        }
        self.customerId = customer.id
        self.customerListTextField.text = customer.name
        self.customerCurrentDueAmountTextField.text = "0.0"
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
        
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func updateDueData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func setDueCustomerData(data: DueCustomerDataMapper) {
        guard let customerList = data.dueCustomerList, customerList.count > 0 else {
            return
        }
        self.isLoading = false
//        let newCustomer = DueCustomerList(id: 0, name: "নতুন কাস্টমার যোগ", phone: "")
//        customerList.insert(newCustomer, at: 0)
        self.dueCustomerList = customerList
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.presenter.getAllCustomerDataFromServer(getAll: getAll)
    }
}

extension AddNewDueViewController{
    func showMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.alertWithTextField()
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
        
    }
}
