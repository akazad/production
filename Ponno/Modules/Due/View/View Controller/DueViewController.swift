//
//  DueViewController.swift
//  Ponno
//
//  Created by a k azad on 24/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

class DueViewController: BaseViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dueToggle: UIBarButtonItem!
    @IBOutlet weak var segmentedControl: UISegmentedControl!{
        didSet{
            segmentedControl.setTitle(LanguageManager.DueBook, forSegmentAt: 0)
            segmentedControl.setTitle(LanguageManager.DueCustomer, forSegmentAt: 1)
        }
    }
    
    private var presenter = DuePresenter(service: DueService())
    
    var dueCustomerList: [DueCustomerList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var dueList : [DueList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isDueList = true{
        didSet{
            self.refreshTableView()
        }
    }
    
    var dueSummary : [DueSummary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var checked = false
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var filteredDueCustomerList : [DueCustomerList] = []
    var filteredDueList : [DueList] = []
    
    var isLoading : Bool = false
    //var currentPage : Int = 1
    var dueCustomerPage : Int = 1
    var dueListPage : Int = 1
    var dueCustomerLastPage : Int = 1
    var dueListLastPage : Int = 0
    
    var type : Int = 0
    var customerId : Int?
    var saleId: Int?
    var id : Int?
    var withdrawAmount : String?
    
    var toggleStatus : Bool = true
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.setNavigationBarTitle()
        self.configureTableView()
        self.configureCollectionView()
        self.initialSetup()
        self.setBarButton()
        self.addFloaty()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func initialSetup(){
        self.segmentedControl.addTarget(self, action: #selector(onSegmentChange), for: .valueChanged)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarButton(){
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let dueAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        dueAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        dueAddBtn.addTarget(self, action: #selector(onDueAdd(sender:)), for: UIControl.Event.touchUpInside)
        dueAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        dueAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        dueAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: dueAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2,barButton1], animated: true)
    }
    
    @objc func onSearch(sender: UIButton){
        if toggleStatus == true {
            self.navigateToDueLogSearchVC()
        }else{
            self.navigateToDueListSearchVC()
        }
    }
    
    @objc func onDueAdd(sender: UIBarButtonItem){
        self.navigateToAddNewDueViewController()
    }
    
//    @objc func onDuePaid(sender : UIBarButtonItem){
//        //self.navigateToAddNewDueViewController()
//    }
    
    func setUpBarBtn(){
        let image = UIImage(named: "next")
        dueToggle.setBackgroundImage(image, for: .normal, barMetrics: .default)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func onSegmentChange(sender: UISegmentedControl){
        isDueList = !isDueList
        if (!checked){
            checked = true
            self.isLoading = true
            self.dueCustomerPage = 1
            self.toggleStatus = false
            self.dueCustomerList = []
            self.presenter.getDueCustomerDataFromServer(page: self.dueCustomerPage)
        }
        else{
            checked = false
            self.isLoading = true
            self.dueListPage = 1
            self.dueList = []
            self.toggleStatus = true
            self.presenter.getDueDataFromServer(page: self.dueListPage)
        }
    }
    
    @IBAction func dueToggleButton(_ sender: UIBarButtonItem) {
        isDueList = !isDueList
        if (!checked){
            let image = UIImage(named: "previous")
            dueToggle.setBackgroundImage(image, for: .normal, barMetrics: .default)
            checked = true
            self.isLoading = true
            self.dueCustomerPage = 1
            self.toggleStatus = false
            self.dueCustomerList = []
            self.presenter.getDueCustomerDataFromServer(page: self.dueCustomerPage)
        }
        else{
            let image = UIImage(named: "next")
            dueToggle.setBackgroundImage(image, for: .normal, barMetrics: .default)
            checked = false
            self.isLoading = true
            self.dueListPage = 1
            self.toggleStatus = true
            self.dueList = []
            self.presenter.getDueDataFromServer(page: self.dueListPage)
        }
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.DueBook
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
    }
    
    func navigateToDueListSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DueListSearchViewController") as! DueListSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToDueLogSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DueLogSearchViewController") as! DueLogSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewDueViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDueViewController") as! AddNewDueViewController
//        viewController.dueCustomerList = self.dueCustomerList
        self.type = 0
        viewController.postType = type
        viewController.dueState = DueState.dueAdd.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(DueViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        if(checked){
            self.dueCustomerPage = 1
            self.dueCustomerList = []
            self.refreshTableView()
            self.presenter.getDueCustomerDataFromServer(page: self.dueCustomerPage)
        }else{
            self.dueListPage = 1
            self.dueList = []
            self.refreshTableView()
            self.presenter.getDueDataFromServer(page: dueListPage)
        }
        refreshControl.endRefreshing()
    }
    
    func navigateToFreemiumSaleableViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumSaleableProductsVC") as! FreemiumSaleableProductsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Floaty Delegate
extension DueViewController: FloatyDelegate{
    func navigateToAddSaleVC() {
        if let viewSettings = getViewSettings(){
            if viewSettings.salePanel == "FreemiumSale"{
                self.navigateToFreemiumSaleableViewController()
            }else{
                self.navigateToAddNewSaleViewController()
            }
        }
        //self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}


//MARK: CollectionView Delegate And DataSource
extension DueViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(DueSummaryCell.nib, forCellWithReuseIdentifier: DueSummaryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let dueItem = self.dueSummary, dueItem.count > 0 else {
            return 0
        }
        return dueItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : DueSummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: DueSummaryCell.identifier, for: indexPath) as! DueSummaryCell
        guard let dueItem = self.dueSummary, dueItem.count > 0 else{
            return cell
        }
        let dueSummaryList = dueItem[indexPath.row]
        
        cell.value.text = dueSummaryList.value
        cell.title.text = dueSummaryList.title
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 60.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.dueSummary, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//MARK: TableView Delegate And DataSoruce
extension DueViewController : UITableViewDelegate, UITableViewDataSource{
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DueListMainCell.nib, forCellReuseIdentifier: DueListMainCell.identifier)
        self.tableView.register(DueCustomerListCell.nib, forCellReuseIdentifier: DueCustomerListCell.identifier)
        self.tableView.estimatedRowHeight = 112
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.separatorColor = UIColor.clear
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isDueList {
            if self.dueList.count > 0 {
                return self.dueList.count
            }
            return 0
        } else{
            if self.dueCustomerList.count > 0 {
                return self.dueCustomerList.count
            }
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isDueList{
            let cell : DueListMainCell = tableView.dequeueReusableCell(withIdentifier: DueListMainCell.identifier, for: indexPath) as! DueListMainCell
            cell.selectionStyle = .none
            if self.dueList.count > 0{
                let due = self.dueList[indexPath.row]
                
                cell.dateLabel.text = due.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_yy_h_mm_a.rawValue)
                cell.amountLabel.text = LanguageManager.Amount + " : " + "\(due.amount)" + " " + Constants.currencySymbol
                cell.nameLabel.text = LanguageManager.Customer + " : " + due.customerName
                
                let description = due.description
                if description.isEmpty {
                    cell.descriptionImage.isHidden = true
                }else{
                    cell.descriptionImage.isHidden = false
                }
                
                cell.descriptionImage.addTapGestureRecognizer {
                    self.showAlert(title: LanguageManager.Description, message: description)
                }
                
                cell.dueListPopUpBtn.tag = due.id
                cell.dueListPopUpBtn.addTarget(self, action: #selector(onDueListPopUpBtnTapped), for: .touchUpInside)
                
                if due.type == 0 {
                    cell.typeLabel.text = LanguageManager.Due
                    cell.typeLabel.textColor = UIColor(red: CGFloat(220/255.0), green: CGFloat(53/255.0), blue: CGFloat(69/255.0), alpha: CGFloat(1.0))
                }
                if due.type == 1 {
                    cell.typeLabel.text = LanguageManager.Withdraw
                    cell.typeLabel.textColor = UIColor(red: CGFloat(91/255.0), green: CGFloat(160/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1.0))
                }
                if isLoading == false && indexPath.row == self.dueList
                    .count - 1 && self.dueListPage < self.dueListLastPage{
                    self.isLoading = true
                    self.dueListPage += 1
                    self.presenter.getDueDataFromServer(page: self.dueListPage)
                }
            }
            return cell
        } else{
            let cell : DueCustomerListCell = tableView.dequeueReusableCell(withIdentifier: DueCustomerListCell.identifier, for: indexPath) as! DueCustomerListCell
            cell.selectionStyle = .none
            if self.dueCustomerList.count > 0 {
                let item = self.dueCustomerList[indexPath.row]
                
                cell.dueCustomerList = item
                
                cell.dueCustomerPopUpBtn.tag = item.id
                cell.dueCustomerPopUpBtn.addTarget(self, action: #selector(onDueCustomerPopUpBtnTapped), for: .touchUpInside)
                
                if isLoading == false && indexPath.row == self.dueCustomerList
                    .count - 1 && self.dueCustomerPage < self.dueCustomerLastPage {
                    self.isLoading = true
                    self.dueCustomerPage += 1
                    self.presenter.getDueCustomerDataFromServer(page: self.dueCustomerPage)
                }
            }
            return cell
        }
    }
    
    fileprivate func refreshTableView(){
        self.tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !isDueList{
            return 120
        }
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isDueList{
            if self.dueCustomerList.count > 0 {
                let customer = self.dueCustomerList[indexPath.row]
                self.navigateToCustomerDetailsVC(id: customer.id)
            }
        }else{
            if self.dueList.count > 0{
                return
            }
            let due = self.dueList[indexPath.row]
            self.navigateToCustomerDetailsVC(id: due.customerId)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        //var edit : Bool = true
        
//        if(!checked){
//            if self.dueList.count > 0 {
//                let due = self.dueList[indexPath.row]
//                self.saleId = due.saleId
//
//                if saleId == 0{
//                    //edit = true
//                }else{
//                    //edit = false
//                }
//            }
//        }
//        return edit
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        //        if self.dueList.count > 0 {
//        //            let due = self.dueList[indexPath.row]{
//        //                let edit = UITableViewRowAction(style: .normal, title: "পরিবর্তন") { action, index in
//        //                    self.customerId = due.customerId
//        //                    self.navigateToEditDueVC(data: due)
//        //                    //self.displayMessage(userMessage: "কাজটি অসম্পন্ন")
//        //                }
//        //                edit.backgroundColor = UIColor.orange
//        //
//        //                let delete = UITableViewRowAction(style: .normal, title: "বাতিল") { action, index in
//        //                    let dueId = due.id
//        //                    self.confirmationMessage(userMessage: "আপনি কি নিশ্চিত?", deleteId: dueId)
//        //                }
//        //                delete.backgroundColor = UIColor.red
//        //
//        //                return [delete, edit]
//        //            }
//        //        }
//        
//        if(!checked){
//            if self.dueList.count > 0 {
//                let due = self.dueList[indexPath.row]
//                
//                self.saleId = due.saleId
//                
//                if saleId == 0{
//                    //edit = true
//                    let edit = UITableViewRowAction(style: .normal, title: "Update") { action, index in
//                        
//                        self.customerId = due.customerId
//                        self.navigateToEditDueVC(data: due)
//                        //self.displayMessage(userMessage: "কাজটি অসম্পন্ন")
//                    }
//                    edit.backgroundColor = UIColor.orange
//                    
//                    let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
//                        
//                        let dueId = due.id
//                        self.confirmationMessage(userMessage: "Are you sure?", deleteId: dueId)
//                    }
//                    delete.backgroundColor = UIColor.red
//                    
//                    return [delete, edit]
//                }else{
//                    //edit = false
//                    
//                    let invoice = UITableViewRowAction(style: .normal, title: due.invoice) { action, index in
//                        
//                        //self.customerId = due.customerId
////                        self.saleId = due.saleId
//                        guard let id = self.saleId else{
//                            return
//                        }
//                        self.navigateToSaleDetailsVC(id : id)
//                        
//                    }
//                    invoice.backgroundColor = UIColor(red: 0, green: 100, blue: 0)
//                    
//                    return [invoice]
//                }
//                
//
//            }
//            
//        }else{
//            if self.dueCustomerList.count > 0 {
//                let list = self.dueCustomerList[indexPath.row]
//                let pay = UITableViewRowAction(style: .normal, title: "Due Withdraw") { action, index in
//                    
//                    
//                    self.customerId = list.id
//                    self.navigateToDuePaidVC(data: list)
//                }
//                pay.backgroundColor = UIColor.orange
//                let update = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
//                    
//                    self.navigateToUpdateCustomerVC(data: list)
//                }
//                update.backgroundColor = UIColor.lightRed
//                
//                return [update, pay]
//            }
//        }
//        return [UITableViewRowAction()]
//    }
    
    @objc func onDueListPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if(!checked){
            if self.dueList.count > 0{
                self.id = sender.tag
                for dueItem in self.dueList{
                    if self.id == dueItem.id{
                        self.saleId = dueItem.saleId
                        if self.saleId != 0{
                            let invoiceAction = UIAlertAction(title: dueItem.invoice, style: UIAlertAction.Style.default) { (action) in
                                guard let id = self.saleId else{
                                    return
                                }
                                self.navigateToSaleDetailsVC(id : id)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                
                                self.view.endEditing(true)
                            }
                            
                            cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                            
                            myActionSheet.addAction(invoiceAction)
                            myActionSheet.addAction(cancelAction)
                        } else{
                            self.withdrawAmount = dueItem.withdrawAmount
                            if withdrawAmount != "0.00"{
                                let withdrawAmountAction = UIAlertAction(title: LanguageManager.Paid +  "  : " + "\(self.withdrawAmount ?? "0.00")" + Constants.currencySymbol, style: UIAlertAction.Style.default) { (action) in
                                    
                                }
                                
                                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                    
                                    self.view.endEditing(true)
                                }
                                
                                cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                                
                                myActionSheet.addAction(withdrawAmountAction)
                                myActionSheet.addAction(cancelAction)
                            }else{
                                
                                let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                                    self.customerId = dueItem.customerId
                                    self.type = dueItem.type
                                    self.navigateToEditDueVC(data: dueItem)
                                }
                                
                                let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                                    let dueId = dueItem.id
                                    self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: dueId)
                                }
                                
                                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                                    
                                    self.view.endEditing(true)
                                }
                                
                                cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                                
                                myActionSheet.addAction(editAction)
                                myActionSheet.addAction(deleteAction)
                                myActionSheet.addAction(cancelAction)
                            }
                        }
                        
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    @objc func onDueCustomerPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if(checked){
            if self.dueCustomerList.count > 0 {
                self.customerId = sender.tag
                for dueCustomerItem in self.dueCustomerList{
                    if self.customerId == dueCustomerItem.id{
                        let duePaidAction = UIAlertAction(title: LanguageManager.DueWithdraw, style: UIAlertAction.Style.default) { (action) in
                            self.customerId = dueCustomerItem.id
                            self.navigateToDuePaidVC(data: dueCustomerItem)
                        }
                        
                        let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                            self.navigateToUpdateCustomerVC(data: dueCustomerItem)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(duePaidAction)
                        myActionSheet.addAction(updateAction)
                        myActionSheet.addAction(cancelAction)
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    //self.dueList = []
                    self.getDueListData()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.deleteDueLogDataFromServer(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }

}

//MARK: Api Delegate
extension DueViewController : DueViewDelegate {
    func updateDueData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func setCustomerData(data: DueCustomerDataMapper) {
        guard let customer = data.dueCustomerList, customer.count > 0 else{
            return
        }
        self.isLoading = false
        self.dueCustomerList += customer
        
        guard let pagination = data.pagination else{
            return
        }
        self.dueCustomerLastPage = pagination.lastPageNo
    }
    
    func setDueData(data: DueDataMapper) {
        guard let dueItem = data.summary else {
            return
        }
        self.dueSummary = dueItem
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        guard let list = data.dueList , list.count > 0 else {
            return
        }
        self.isLoading = false
        self.dueList += list
        
        guard let pagination = data.pagination else{
            return
        }
        self.dueListLastPage = pagination.lastPageNo
        
    }
    
    func deleteDueLogData(data: AddDataMapper) {
        guard let message = data.message else { 
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onDueDeleteFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter() {
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        
        if(checked){
            self.isLoading = true
            self.dueCustomerList = []
            self.dueCustomerPage = 1
            self.refreshTableView()
            self.presenter.getDueCustomerDataFromServer(page: self.dueCustomerPage)
        }else{
            self.isLoading = true
            self.dueList = []
            self.dueListPage = 1
            self.presenter.getDueDataFromServer(page: self.dueListPage)
        }
    }
    
    func getDueListData(){
        self.isLoading = true
        self.dueListPage = 1
        self.dueList = []
        //self.refreshTableView()
        self.presenter.getDueDataFromServer(page: self.dueListPage)
    }
}

//Navigation
extension DueViewController{
    func navigateToCustomerDetailsVC(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerDBaseViewController") as! CustomerDBaseViewController
        viewController.customerId = id
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToEditDueVC(data : DueList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDueViewController") as! AddNewDueViewController
        viewController.postType = self.type
        viewController.customerId = customerId
        viewController.dueData = data
        viewController.dueState = DueState.dueEdit.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToSaleDetailsVC(id : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        viewController.iD = id
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToDuePaidVC(data: DueCustomerList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDueViewController") as! AddNewDueViewController
        self.type = 1
        viewController.postType = type
        viewController.customerId = customerId
        viewController.dueState = DueState.duePaid.rawValue
        viewController.dueCustomer = data
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToUpdateCustomerVC(data: DueCustomerList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerAddViewController") as! CustomerAddViewController
        viewController.customerState = CustomerState.DueCustomerUpdate.rawValue
        viewController.customerId = Int(data.id)
        viewController.dueCustomer = data
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

