//
//  DuePaidViewController.swift
//  Ponno
//
//  Created by a k azad on 14/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DuePaidViewController: UIViewController {
    
    @IBOutlet weak var customerLabel: UILabel!{
        didSet{
            customerLabel.text = LanguageManager.CustomerName
        }
    }
    @IBOutlet weak var customerListTextField: UITextField!{
        didSet{
            customerListTextField.placeholder = LanguageManager.CustomerName
        }
    }
    @IBOutlet weak var customerCurrentDueAmountLabel: UILabel!{
        didSet{
            customerCurrentDueAmountLabel.text = LanguageManager.CurrentDue
        }
    }
    @IBOutlet weak var customerCurrentDueAmountTextField: UITextField!{
        didSet{
            customerCurrentDueAmountTextField.placeholder = LanguageManager.CurrentDue
        }
    }
    @IBOutlet weak var customerDueAmountLabel: UILabel!{
        didSet{
            customerDueAmountLabel.text = LanguageManager.DueAmount
        }
    }
    @IBOutlet weak var customerDueAmountTextField: UITextField!{
        didSet{
            customerDueAmountTextField.placeholder = LanguageManager.DueAmount
        }
    }
    @IBOutlet weak var dueDetailsLabel: UILabel!{
        didSet{
            dueDetailsLabel.text = LanguageManager.Description
        }
    }
    @IBOutlet weak var dueDetailsView: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    
    var dueCustomer : DueCustomerList?
    var customerId : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetUp()
    }
    
    func initialSetUp(){
        self.title = LanguageManager.DueWithdraw
        guard let data = dueCustomer else{
            return
        }
        self.customerListTextField.text = data.name
        self.customerCurrentDueAmountTextField.text = "\(data.currentDue)"
    }
    
    
    @IBAction func submitBtn(_ sender: UIButton) {
    }
    
//    @IBAction func dueSubmitButton(_ sender: UIButton) {
//        if isValidated(){
//            self.submitBtnControlWith(isEnabled: false)
//            guard let id = self.customerId, let amount = self.customerDueAmountTextField.text, let description = self.dueDetailsView.text else {
//                return
////            }
////            let dueDataArray: [String: Any] = ["customer_id": id, "type": self.postType, "amount" : amount, "description": description]
////            self.presenter.postDueDataToServer(dueData: dueDataArray)
//        }
//    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitButton.isEnabled = false
    }
    
    func isValidated()->Bool{
        if (self.customerListTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.CustomerIsRequired)
            return false
        }
        else if (self.customerDueAmountTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.AmountIsRequired)
            return false
        }else{
            return true
        }
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}
