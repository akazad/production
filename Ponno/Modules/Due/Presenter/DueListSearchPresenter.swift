//
//  DueListSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 20/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
protocol DueListSearchViewDelegate : NSObjectProtocol {
    func setDueCustomerSearchData(data : DueCustomerDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class DueListSearchPresenter: NSObject {
    
    private let service : DueService
    weak private var viewDelegate : DueListSearchViewDelegate?
    
    init(service : DueService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DueListSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getDueCustomerSearchDataFromServer(page: Int, searchText: String){
        self.viewDelegate?.showLoading()
        self.service.getDueCustomerSearchData(page: page, searchText: searchText, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDueCustomerSearchData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
