//
//  DuePresenter.swift
//  Ponno
//
//  Created by a k azad on 24/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol DueViewDelegate : NSObjectProtocol {
    func setDueData(data : DueDataMapper)
    func setCustomerData(data : DueCustomerDataMapper)
    func deleteDueLogData(data : AddDataMapper)
    func updateDueData(data: AddDataMapper)
    func onDueDeleteFailed(data: String)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class DuePresenter: NSObject {
    
    private let service : DueService
    weak private var viewDelegate : DueViewDelegate?
    
    init(service : DueService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DueViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func  getDueDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getDueData(page: page,success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDueData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getDueCustomerDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getDueCustomerData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCustomerData(data: data)
        }, failure: {(mesaage) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: mesaage)
        })
    }
    
    func deleteDueLogDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.deleteDueData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.deleteDueLogData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDueDeleteFailed(data: message)
        })
    }
    
    func updateDueData(updateData: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postDueUpdate(updateData: updateData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateDueData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}

