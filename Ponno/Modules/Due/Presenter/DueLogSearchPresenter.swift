//
//  DueLogSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 20/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol DueLogSearchViewDelegate : NSObjectProtocol {
    func setDueData(data : DueDataMapper)
    func deleteDueLogData(data : AddDataMapper)
    func updateDueData(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class DueLogSearchPresenter: NSObject {
    
    private let service : DueService
    weak private var viewDelegate : DueLogSearchViewDelegate?
    
    init(service : DueService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DueLogSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func  getDueDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getDueData(page: page,success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDueData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func deleteDueLogDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.deleteDueData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.deleteDueLogData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func updateDueData(updateData: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postDueUpdate(updateData: updateData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateDueData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getDueLogSearchDataFromServer(startDate: String, endDate: String, page: Int){
        self.viewDelegate?.showLoading()
        self.service.getDueLogSearchData(page: page, startDate: startDate, endDate: endDate, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDueData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
