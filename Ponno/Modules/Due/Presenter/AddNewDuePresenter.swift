//
//  AddNewDuePresenter.swift
//  Ponno
//
//  Created by a k azad on 10/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol AddNewDueViewDelegate : NSObjectProtocol {
    func setDueCustomerData(data: DueCustomerDataMapper)
    func setCustomerData (data: CustomerDataMapper)
    func onSuccess(data : AddDataMapper)
    func updateDueData(data: AddDataMapper)
    func setAddNewCustomerData(data: CustomerAddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class AddNewDuePresenter: NSObject {
    
    private let service : DueService
    weak private var viewDelegate : AddNewDueViewDelegate?
    
    init(service : DueService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AddNewDueViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getAllDueCustomerDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllDueCustomerData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDueCustomerData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getAllCustomerDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        let service = CustomerService()
        service.getAllCustomerData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCustomerData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }

    func postNewDueDataToServer(dueData: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewDueData(dueData: dueData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data) 
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postDueUpdateDataSource(param :[String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postDueUpdate(updateData: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateDueData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postAddNewCustomerDataFromServer(customerDataArray: [String: String]){
        self.viewDelegate?.showLoading()
        let service = CustomerService()
        service.postNewCustomerData(customerDataArray: customerDataArray, success:{ (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setAddNewCustomerData(data: data) 
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data : message)
        })
    }
}
