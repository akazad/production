//
//  OtherGraphContents.swift
//  Ponno
//
//  Created by a k azad on 5/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class OtherGraphContents : Mappable {
    var barEntries : [OtherBarEntry]? 
    var labels : [String]?
    var colors : [String]?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        barEntries <- map["bar_entries"]
        labels <- map["labels"]
        colors <- map["colors"]
    }
    
}

