//
//  DashboardDataMapper.swift
//  Ponno
//
//  Created by a k azad on 19/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
import Foundation
import ObjectMapper

class DashboardDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var todaySaleEntries : SaleTodayEntries? 
    var grossProfitEntries : GrossProfitEntries?
    var otherGraphContents : OtherGraphContents? 
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        success <- map["success"]
        message <- map["message"]
        todaySaleEntries <- map["today_sale_entries"]
        grossProfitEntries <- map["gross_profit_entries"]
        otherGraphContents <- map["other_graph_contents"]
    }
    
}

