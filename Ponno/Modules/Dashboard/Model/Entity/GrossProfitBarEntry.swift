//
//  GrossProfitBarEntry.swift
//  Ponno
//
//  Created by a k azad on 22/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class GrossProfitBarEntry : Mappable {
    var index : Double?
    var stepValue : [Double]?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        index <- map["index"]
        stepValue <- map["step_values"]
    }
    
}

