//
//  saleTodayEntries.swift
//  Ponno
//
//  Created by a k azad on 5/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class SaleTodayEntries : Mappable {
    var barEntries : [BarEntries]?
    var colors : [String]?
    var labels : [String]?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        barEntries <- map["bar_entries"]
        colors <- map["colors"]
        labels <- map["labels"]
    }
    
}

