//
//  DashboardService.swift
//  Ponno
//
//  Created by a k azad on 19/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

public class DashboardService : NSObject{
    
    func getDashboardData(success: @escaping (DashboardDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Dashboard
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<DashboardDataMapper>) in
                if let dashboardResponse = response.result.value{
                    if dashboardResponse.success == true{
                        success(dashboardResponse)
                    }
                    else if let failureMessage = dashboardResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
   
    
}

