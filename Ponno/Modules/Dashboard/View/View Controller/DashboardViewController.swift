//
//  DashboardViewController.swift
//  Ponno
//
//  Created by a k azad on 19/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SVProgressHUD
import Charts
import Floaty

class DashboardViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var timeLabel: UILabel!
    
    private var presenter = DashboardPresenter(service: DashboardService())
    
    var saleToday : SaleTodayEntries?
    var grossProfit : GrossProfitEntries?
    var dashboardOthers : OtherGraphContents?
    var timeslots : [String] = []
    var timer = Timer()
    let dateFormatter = DateFormatter()
    
   // var grossProfit : [BarEntries]?
    //var others : [DashboardOthers]?
    
    
    enum Sections: Int {
        case todaysSale
        case grossProfit
        case others
    }
    let sectionHeaders = [LanguageManager.TodaysSale, LanguageManager.GrossProfit, LanguageManager.OtherCalculations]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.attachPresenter()
        self.setNavigationBarTitle()
        self.setUpTime()
        self.configureTableView()
        self.addFloaty()
    }
    
    func setUpTime(){
        dateFormatter.dateFormat = "dd-MMM-yyyy \n HH:mm:ss"
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeLabel), userInfo: nil, repeats: true);
    }
    
    @objc func updateTimeLabel() {
        //timeLabel.text = dateFormatter.string(from: Date())
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.attachPresenter()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func setNavigationBarTitle(){
        self.setUpInitialLanguage()
        self.title = LanguageManager.TodaysSummary
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(SalesListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        //self.salesList = []
        //currentPage = 1
        self.presenter.getDashboardDataFromServer()
        
        refreshControl.endRefreshing()
    }
}

//Mark: Floaty Delegate
extension DashboardViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}

//Mark: TableView Delegate And DataSource
extension DashboardViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
//        self.tableView.bounces = false
        self.tableView.register(TodaysSaleCell.nib, forCellReuseIdentifier: TodaysSaleCell.identifier)
        self.tableView.register(GrossProfitCell.nib, forCellReuseIdentifier: GrossProfitCell.identifier)
        self.tableView.register(DashboardOthersCell.nib, forCellReuseIdentifier: DashboardOthersCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        
        self.tableView.addSubview(self.refreshControl)
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 0, y:0, width: self.tableView.frame.width, height: 40))
        let label = UILabel(frame: CGRect(x: 0, y: 10, width: header.frame.width, height: 20))
        label.text = self.sectionHeaders[section]
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        header.backgroundColor = UIColor.white
        header.addSubview(label)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case Sections.todaysSale.rawValue:
            let cell: TodaysSaleCell = tableView.dequeueReusableCell(withIdentifier: TodaysSaleCell.identifier, for: indexPath) as! TodaysSaleCell
            cell.selectionStyle = .none
            cell.saleTodayEntry = self.saleToday
            return cell
            
        case Sections.grossProfit.rawValue:
            let cell: GrossProfitCell = tableView.dequeueReusableCell(withIdentifier: GrossProfitCell.identifier, for: indexPath) as! GrossProfitCell
            cell.selectionStyle = .none
            cell.grossProfit = self.grossProfit
            return cell
        case Sections.others.rawValue:
            let cell: DashboardOthersCell = tableView.dequeueReusableCell(withIdentifier: DashboardOthersCell.identifier, for: indexPath) as! DashboardOthersCell
            if let others = self.dashboardOthers{
                cell.selectionStyle = .none
                cell.dashboardOthersEntry = others
            }
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 40))
        footer.backgroundColor = UIColor.white
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
}

// MARK: API Delegate
extension DashboardViewController : DashboardViewDelegate{
    func setDashboardData(data: DashboardDataMapper) {
        guard let dashboardSaleToday = data.todaySaleEntries, let grossProfit = data.grossProfitEntries, let dashboardOthers = data.otherGraphContents else{
            return
        }
        self.saleToday = dashboardSaleToday
        self.grossProfit = grossProfit
        self.dashboardOthers = dashboardOthers
        self.refreshTableView()
        
//        guard let dashboardGrossProfit = data.grossProfitEntries?.barEntries else{
//            return
//        }
       // self.grossProfit = dashboardGrossProfit
      //  self.refreshTableView()
    }
    
    func onFailed() {
        
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getDashboardDataFromServer()
    }
}
