//
//  TodaysSaleCell.swift
//  Ponno
//
//  Created by a k azad on 3/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Charts

class TodaysSaleCell: UITableViewCell, ChartViewDelegate , IAxisValueFormatter {
    
    @IBOutlet weak var chartView: BarChartView!
    
    var saleTodayEntry : SaleTodayEntries?{
        didSet{
            guard let data = self.saleTodayEntry, let barEntries = data.barEntries, barEntries.count > 0, let colors = data.colors,colors.count > 0 , let labels = data.labels, labels.count > 0  else{
                return
            }
            self.colors = colors
            self.timeslots = labels
            self.saleToday = barEntries
            
        }
    }
    
    var saleToday : [BarEntries]? {
        didSet {
            DispatchQueue.main.async {
                self.setUpChart()
            }
        }
    }
    
    var colors : [String] = []
    
    var timeslots : [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier : String{
        return String(describing: TodaysSaleCell.self)
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let index = Int(value)
        if self.timeslots.indices.contains(index){
            print(timeslots[Int(value)])
            return timeslots[Int(value)]
        }
        else{
            return ""
        }
    }
    
    
    func setUpChart() {
        var dataEntries : [BarChartDataEntry] = []
        guard let list = self.saleToday, list.count > 0 else{
            return
        }
        var counter = 0.0
        
        for i in 0..<list.count{
            
            if let sale = list[i].stepValue{
                let data = BarChartDataEntry(x: counter, y: Double(sale))
                dataEntries.append(data)
                counter += 1.0
            }
//            guard let saleItem = sale else{
//                return
//            }
            
        }
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "")
        let chartData = BarChartData(dataSet: chartDataSet)
        //chartView.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        chartDataSet.colors = [UIColor(red:0.89, green:0.98, blue:0.76, alpha:1.0)]
        
        chartView.data = chartData
        chartData.highlightEnabled = false
        chartView.rightAxis.enabled = false
        chartView.xAxis.valueFormatter = self
        chartView.xAxis.labelCount = self.timeslots.count
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.labelRotationAngle = 0
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.xAxis.granularityEnabled = true
        chartView.xAxis.granularity = 1.0
        chartView.legend.enabled = false
        chartView.setVisibleXRangeMaximum(3.0)
        
        
        let leftAxis = chartView.leftAxis
        leftAxis.drawLabelsEnabled = false
        leftAxis.drawGridLinesEnabled = false
        
        chartView.drawGridBackgroundEnabled = false
        chartView.delegate = self
    }
}
