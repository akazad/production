//
//  GrossProfitCell.swift
//  Ponno
//
//  Created by a k azad on 4/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Charts

class GrossProfitCell: UITableViewCell , ChartViewDelegate , IAxisValueFormatter {

    @IBOutlet weak var barChartView: BarChartView!
    
 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier : String{
        return String(describing: GrossProfitCell.self)
    }
    
    var grossProfit : GrossProfitEntries? {
        didSet {
            
            guard let data = grossProfit , let barEntries = data.barEntries, barEntries.count > 0 , let labels = data.labels, labels.count > 0 , let colors = data.colors, colors.count > 0 else {return}
             
            self.barEntries = barEntries
            self.labels = labels
            self.colors = colors
            
            
        }
    }
    
    var barEntries : [GrossProfitBarEntry]? {
        didSet{
            DispatchQueue.main.async {
                self.setUpChart()
            }
        }
    }
    var labels : [String] = []
    var colors : [String] = []
    
    func setUpChart() {
        var dataEntries : [BarChartDataEntry] = []
        
        guard let entries = self.barEntries, entries.count > 0 else{
            return
        }

        for i in 0..<entries.count{
            let barEntry = entries[i]
            
            guard let index = barEntry.index else {return}
            guard var stepValue = barEntry.stepValue else {return}
            if index == 1{
                stepValue[0] = 0
            }
            let data = BarChartDataEntry(x: index , yValues: stepValue)
            dataEntries.append(data)
        }
        
        guard self.colors.count > 0 else {return}
        var chartColors : [UIColor] = [UIColor.clear]
        for color in colors{
            let color = self.hexStringToUIColor(hex: color)
            chartColors.append(color)
        }
        chartColors.append(chartColors[2])
        
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "")
        
        let chartData = BarChartData(dataSet: chartDataSet)
        //barChartView.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        chartDataSet.colors = chartColors
//        chartDataSet
        
        barChartView.data = chartData
        //        chartView.highlightFullBarEnabled = false
        chartData.highlightEnabled = false
        barChartView.rightAxis.enabled = false
        
        let bottomAxis = barChartView.xAxis
        
        bottomAxis.valueFormatter = self
        bottomAxis.labelCount = self.labels.count
        bottomAxis.labelPosition = .bottom
//        bottomAxis.labelRotationAngle = 45
        bottomAxis.drawGridLinesEnabled = false
        bottomAxis.granularityEnabled = true
        bottomAxis.drawAxisLineEnabled = false
        bottomAxis.drawLabelsEnabled = false
        bottomAxis.granularity = 1.0
        barChartView.legend.enabled = false
        barChartView.setVisibleXRangeMaximum(3.0)
//        barChartView
//        barChartView.drawValueAboveBarEnabled = false
        barChartView.drawBarShadowEnabled = false
//        barChartView.
        
        
        let leftAxis = barChartView.leftAxis
        leftAxis.drawAxisLineEnabled = false
        leftAxis.drawLabelsEnabled = false
        leftAxis.drawGridLinesEnabled = false
        //        leftAxis.granularity = 1
        leftAxis.drawZeroLineEnabled = true
        barChartView.drawGridBackgroundEnabled = false
        barChartView.delegate = self
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let index = Int(value)
        if self.labels.indices.contains(index){
            return labels[index]
        }
        else{
            return ""
        }
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
