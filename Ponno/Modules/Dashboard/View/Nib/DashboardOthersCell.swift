//
//  DashboardOthersCell.swift
//  Ponno
//
//  Created by a k azad on 4/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Charts

class DashboardOthersCell: UITableViewCell, ChartViewDelegate , IAxisValueFormatter {
    
    @IBOutlet weak var chartView: BarChartView!
    
    var dashboardOthersEntry : OtherGraphContents?{
        didSet{
            guard let data = self.dashboardOthersEntry, let barEntries = data.barEntries, barEntries.count > 0, let colors = data.colors,colors.count > 0 , let labels = data.labels, labels.count > 0  else{
                return
            }
            self.colors = colors
            self.timeslots = labels
            self.otherContent = barEntries
            
            DispatchQueue.main.async {
                self.setUpChart()
            }
        }
    }
    
    var otherContent : [OtherBarEntry]?
    
    var colors : [String] = []
    
    var timeslots : [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier : String{
        return String(describing: DashboardOthersCell.self)
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        //        print("-" + "\(Int(value))")
        let index = Int(value)
        if self.timeslots.indices.contains(index){
            return timeslots[Int(value)]
        }
        else{
            return ""
        }
    }
    
    
    func setUpChart() {
        var dataEntries : [BarChartDataEntry] = []
        guard let list = self.otherContent, list.count > 0 else{
            return
        }
        var counter = 0.0
        for i in 0..<list.count{
            let dashboardOther = list[i].stepValue
            guard let listItem = dashboardOther else{
                return
            }
            let data = BarChartDataEntry(x: counter, y: Double(listItem))
            dataEntries.append(data)
            counter += 1.0
        }
        
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "")
        let chartData = BarChartData(dataSet: chartDataSet)
       // chartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        chartDataSet.colors = [UIColor(red:0.89, green:0.98, blue:0.76, alpha:1.0)]
        
        chartView.data = chartData
        //        chartView.highlightFullBarEnabled = false
        chartData.highlightEnabled = false
        chartView.rightAxis.enabled = false
        chartView.xAxis.valueFormatter = self
        chartView.xAxis.labelCount = self.timeslots.count
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.labelRotationAngle = 0
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.xAxis.granularityEnabled = true
        chartView.xAxis.granularity = 1.0
        chartView.legend.enabled = false
        chartView.setVisibleXRangeMaximum(3.0)
        
        
        let leftAxis = chartView.leftAxis
        //leftAxis.drawAxisLineEnabled = false
        leftAxis.drawLabelsEnabled = false
        leftAxis.drawGridLinesEnabled = false
        //        leftAxis.granularity = 1
        
        chartView.drawGridBackgroundEnabled = false
        chartView.delegate = self
    }
}
