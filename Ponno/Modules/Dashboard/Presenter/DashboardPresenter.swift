//
//  DashboardPresenter.swift
//  Ponno
//
//  Created by a k azad on 19/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol DashboardViewDelegate : NSObjectProtocol {
    func setDashboardData(data : DashboardDataMapper)
    func onFailed()
    func showLoading()
    func hideLoading()
}
class DashboardPresenter: NSObject {
    
    private let service : DashboardService
    weak private var viewDelegate : DashboardViewDelegate?
    
    init(service : DashboardService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DashboardViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getDashboardDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getDashboardData(success: {(data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDashboardData(data: data)
        }, failure: {(message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
}

