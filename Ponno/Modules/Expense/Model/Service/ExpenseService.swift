//
//  ExpenseService.swift
//  Ponno
//
//  Created by a k azad on 21/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

public class ExpenseService : NSObject{
    
    //Get
    func getExpenseData(page: Int, startDt: String, endDt: String, addedBy: String, success: @escaping (ExpenseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Expense + RestURL.sharedInstance.getDateRange(startDate: startDt, endDate: endDt) + RestURL.sharedInstance.addedBy(addedBy: addedBy) + RestURL.sharedInstance.getPageNumber(page: page)
        
        print(url)
        
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<ExpenseDataMapper>) in
                if let expenseResponse = response.result.value{
                    if expenseResponse.success == true{
                        success(expenseResponse)
                    }else if let failureMessage = expenseResponse.message{
                        failure(failureMessage)
                    }
                }else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getExpensePerCategoryData(page: Int, id: Int, startDt: String, endDt: String, addedBy: String, success: @escaping (PerExpenseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PerExpenseList + RestURL.sharedInstance.getCommonId(id: id) + RestURL.sharedInstance.getDateRange(startDate: startDt, endDate: endDt) + RestURL.sharedInstance.addedBy(addedBy: addedBy) + RestURL.sharedInstance.getPageNumber(page: page)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PerExpenseDataMapper>) in
                if let perExpenseListResponse = response.result.value{
                    if perExpenseListResponse.success == true{
                        success(perExpenseListResponse)
                    }else if let failureMessage = perExpenseListResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    
    //Get ExpenseCategoryList
    func getExpenseCategoryListData(page: Int, success: @escaping (ExpenseCategoriesDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ExpenseCategoryList + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<ExpenseCategoriesDataMapper>) in
                if let expenseCategoryListResponse = response.result.value{
                    if let categoryList = expenseCategoryListResponse.expenseCategoryList, categoryList.count > 0 {
                        success(expenseCategoryListResponse)
                    }else if let failureMessage = expenseCategoryListResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    //Mark: PostRequest
    func getAddNewExpenseData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ExpenseAdd
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let expenseAddResponse = response.result.value {
                if expenseAddResponse.success == true{
                    success(expenseAddResponse)
                }else if let failureMessage = expenseAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func getExpenseUpadateData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ExpenseUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let expenseUpdateResponse = response.result.value {
                if expenseUpdateResponse.success == true{
                    success(expenseUpdateResponse)
                }else if let failureMessage = expenseUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func deleteExpenseData(id: Int, success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ExpenseDelete
        print(url +  "\(id)")
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let expenseDeleteResponse = response.result.value {
                if expenseDeleteResponse.success == true{
                    success(expenseDeleteResponse)
                }else if let failureMessage = expenseDeleteResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
}


