//
//  ExpenseCategories.swift
//  Ponno
//
//  Created by a k azad on 13/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ExpenseCategoriesDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var expenseCategoryList : [ExpenseCategoryList]?
    var pagination: Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        expenseCategoryList <- map["expense_category_list"]
        pagination <- map["pagination"]
    }
    
}

