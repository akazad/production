//
//  PerExpenseDataList.swift
//  Ponno
//
//  Created by a k azad on 30/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PerExpenseDataList : Mappable {
    var name : String = ""
    var amount : String = ""
    var createdAt : String = ""
    var expenseId : Int = 0
    var userId : Int = 0
    var description : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        name <- map["name"]
        amount <- map["amount"]
        createdAt <- map["created_at"]
        expenseId <- map["expense_id"]
        userId <- map["user_id"]
        description <- map["description"]
    }
    
}

