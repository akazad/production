//
//  PerExpenseDataMapper.swift
//  Ponno
//
//  Created by a k azad on 30/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class PerExpenseDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [PerExpenseSummary]?
    var expenseCategory : String = ""
    var expenseList : [PerExpenseDataList]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        expenseCategory <- map["expense_category"]
        expenseList <- map["expense_list"]
        pagination <- map["pagination"]
    }
    
}

