//
//  ExpenseCategoryList.swift
//  Ponno
//
//  Created by a k azad on 13/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ExpenseCategoryList : Mappable {
    var id : Int = 0
    var name : String = ""
    
    required init(map: Map) {
        
    }
    
    init(name: String) {
        self.name = name
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
    }
    
}

