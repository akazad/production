//
//  ExpenseDataMapper.swift
//  Ponno
//
//  Created by a k azad on 21/1/19.
//  Copyright © 2019 Ponno. All rights reserved.

import Foundation
import ObjectMapper

class ExpenseDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [ExpenseSummary]?
    var expenseList : [ExpenseList]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        expenseList <- map["expense_list"] 
        pagination <- map["pagination"]
    }
    
}
