//
//  ExpenseList.swift
//  Ponno
//
//  Created by a k azad on 21/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ExpenseList : Mappable {
    var categoryId : Int = 0
    var name : String = ""
    var total : Int = 0
    var totalAmount : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        categoryId <- map["category_id"]
        name <- map["name"]
        total <- map["total"]
        totalAmount <- map["total_amount"]
    }
    
}


