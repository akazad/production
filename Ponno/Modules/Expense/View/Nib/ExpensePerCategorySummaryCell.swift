//
//  ExpensePerCategorySummaryCell.swift
//  Ponno
//
//  Created by a k azad on 14/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ExpensePerCategorySummaryCell: UICollectionViewCell {

    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: ExpensePerCategorySummaryCell.self)
    }
}
