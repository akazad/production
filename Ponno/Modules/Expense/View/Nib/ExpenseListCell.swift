//
//  ExpenseListCell.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ExpenseListCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var expenseType: UILabel!
    @IBOutlet weak var numberOfExpenses: UILabel!
    @IBOutlet weak var totalExpenses: UILabel!
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: ExpenseListCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
