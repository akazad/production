//
//  ExpenseSalleryCell.swift
//  Ponno
//
//  Created by a k azad on 1/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ExpenseSalleryCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var popUpBtn: UIButton!
    @IBOutlet weak var nameViewWidth: NSLayoutConstraint!
    @IBOutlet weak var infoImage: UIImageView!
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: ExpenseSalleryCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
