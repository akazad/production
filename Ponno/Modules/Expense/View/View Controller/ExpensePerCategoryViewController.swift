//
//  ExpensePerCategoryViewController.swift
//  Ponno
//
//  Created by a k azad on 30/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class ExpensePerCategoryViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateSearchHeight: NSLayoutConstraint!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    
    private var presenter = ExpensePerCategoryPresenter(service: ExpenseService())
    
    var datePicker = UIDatePicker()
    var searchEnable : Bool = false
    
    
    var selectedType : Int?
    var expenseCategory : String?
    var expenseName : String?
    
    var expenseList :[PerExpenseDataList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var expenseOthers : [PerExpenseDataMapper]?
    
    var expensePerCategorySummary : [PerExpenseSummary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var searchIsActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    var expenseData : PerExpenseDataList?
    
    let manager = CollectionViewScrollManager()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.configureTableView()
        self.configureCollectionView()
        self.addFloaty()
        self.initialSetUp()
        self.setBarBtn()
    }
    
    func initialSetUp(){
        guard let name = self.expenseName else{
            return
        }
        self.title = name
        
        self.searchBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        self.dateSearchHeight.constant = 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    //FloatingButton
    func addFloaty(){

        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    func navigateToUpdateExpenseVC(data: PerExpenseDataList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        viewController.expenseCategory = self.expenseCategory
        viewController.expenseData = data
        viewController.expenseCategoryId = self.selectedType ?? -1
        viewController.expenseState = ExpenseState.Update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        viewController.expenseState = ExpenseState.Add.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController 
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToFreemiumSaleableViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumSaleableProductsVC") as! FreemiumSaleableProductsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ExpensePerCategoryViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.expenseList = []
        currentPage = 1
        guard let id = self.selectedType else{
            return
        }
        self.presenter.getExpensePerCategoryDataFromServer(page: self.currentPage, id: id, startDt: "default", endDt: "default", addedBy: "default") 
        
        refreshControl.endRefreshing()
    }
    
    func popOverToExpenseAdvanceSearchVC(sender: UIButton){
        if let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "ExpenseAdvanceSearchVC") as? ExpenseAdvanceSearchVC{
            popoverContentController.modalPresentationStyle = .popover
            popoverContentController.preferredContentSize = CGSize(width:560,height:310)
            popoverContentController.advanceSearchDelegate = self
            showPopup(popoverContentController, sourceView: sender)
        }
    }
    
    func setBarBtn(){
        
        let dateSearchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        dateSearchBtn.setImage(UIImage(named: "dateSearch"), for: UIControl.State.normal)
        dateSearchBtn.addTarget(self, action: #selector(onDateSearch(sender:)), for: UIControl.Event.touchUpInside)
        dateSearchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        dateSearchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        dateSearchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn2 = UIBarButtonItem(customView: dateSearchBtn)
        
        let advanceSearchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        advanceSearchBtn.setImage(UIImage(named: "filterSearch"), for: UIControl.State.normal)
        advanceSearchBtn.addTarget(self, action: #selector(onAdvanceSearchTapped(sender:)), for: UIControl.Event.touchUpInside)
        advanceSearchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        advanceSearchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        advanceSearchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn3 = UIBarButtonItem(customView: advanceSearchBtn)
        
        
        if let category = getBusinessCategory(){
            if category.businessCategory == 1{
                navigationItem.setRightBarButtonItems([barBtn2], animated: true)
            }else{
                navigationItem.setRightBarButtonItems([barBtn3,barBtn2], animated: true)
            }
        }
    }
    
    @objc func onDateSearch(sender: UIBarButtonItem){
        if searchEnable == false{
            self.dateSearchHeight.constant = 30.0
            self.searchEnable = !searchEnable
        }else{
            self.dateSearchHeight.constant = 0.01
            searchEnable = !searchEnable
        }
    }
    
    @objc func onAdvanceSearchTapped(sender: UIButton){
        self.popOverToExpenseAdvanceSearchVC(sender: sender)
    }
    
}

//MARK: CollectionView Delegate And DataSource
extension ExpensePerCategoryViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(ExpensePerCategorySummaryCell.nib, forCellWithReuseIdentifier: ExpensePerCategorySummaryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let perExpenseCategory = self.expensePerCategorySummary, perExpenseCategory.count > 0 else{
            return 0
        }
        return perExpenseCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ExpensePerCategorySummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: ExpensePerCategorySummaryCell.identifier, for: indexPath) as! ExpensePerCategorySummaryCell
        guard let perExpenseCategory = self.expensePerCategorySummary, perExpenseCategory.count > 0 else {
            return cell
        }
        let list = perExpenseCategory[indexPath.row]
        
        cell.amount.text = list.value
        cell.title.text = list.title
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70.0)
    }
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.expensePerCategorySummary, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//MARK: TableView Delegate And DataSource
extension ExpensePerCategoryViewController: UITableViewDelegate, UITableViewDataSource {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
//        self.tableView.register(ExpenseListPerCategoryCell.nib, forCellReuseIdentifier: ExpenseListPerCategoryCell.identifier)
        self.tableView.register(ExpenseSalleryCell.nib, forCellReuseIdentifier: ExpenseSalleryCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.expenseList.count > 0 {
            return self.expenseList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell : ExpenseSalleryCell = tableView.dequeueReusableCell(withIdentifier: ExpenseSalleryCell.identifier, for: indexPath) as! ExpenseSalleryCell
            if self.expenseList.count > 0 {
                let data = self.expenseList[indexPath.row]
                if data.name == ""{
                    cell.nameView.isHidden = true
                    cell.nameViewWidth.constant = 0.0
                }else{
                    cell.nameView.isHidden = false
                    cell.nameViewWidth.constant = 160.0
                }
                cell.nameLabel.text = data.name
                cell.createdAtLabel.text = data.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.createdAtLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.amountLabel.text = LanguageManager.Expense + " : " + "\(data.amount)" + " " + Constants.currencySymbol
                
                let desc = data.description
                if desc == "" {
                    cell.infoImage.isHidden = true
                }else{
                    cell.infoImage.addTapGestureRecognizer{
                        self.showAlert(title: LanguageManager.Description, message: desc)
                    }
                }
                
                cell.popUpBtn.tag = data.expenseId
                cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
                
                if isLoading == false && indexPath.row == self.expenseList.count - 1 && self.currentPage < self.lastPageNo{
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.selectedType else{
                        return cell
                    }
                    self.presenter.getExpensePerCategoryDataFromServer(page: self.currentPage, id: id, startDt: "default", endDt: "default", addedBy: "default")
                }
                cell.selectionStyle = .none
            }
            return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {

        return false
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            
            if self.expenseList.count > 0 {
                let data = self.expenseList[indexPath.row]
                self.navigateToUpdateExpenseVC(data: data)
            }
            
            
        }
        edit.backgroundColor = UIColor.orange

        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in

            if self.expenseList.count > 0{
                let expense = self.expenseList[indexPath.row]
                let expenseId = expense.expenseId
                self.confirmationMessage(userMessage: "Are you sure?", deleteId: expenseId)
            }

        }
        delete.backgroundColor = UIColor.red

        return [delete, edit]
    }
    
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.expenseList.count > 0{
            let expenseId = sender.tag
            for expense in self.expenseList{
                if expenseId == expense.expenseId{
                    let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToUpdateExpenseVC(data: expense)
                    }
                    
                    let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: expenseId)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(editAction)
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
}

//Mark: Floaty Delegate
extension ExpensePerCategoryViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        if let viewSettings = getViewSettings(){
            if viewSettings.salePanel == "FreemiumSale"{
                self.navigateToFreemiumSaleableViewController() 
            }else{
                self.navigateToAddNewSaleViewController()
            }
        }
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC(){
        self.navigateToAddNewExpenseViewController()
    }
}

//MARK: Api Delegate
extension ExpensePerCategoryViewController : ExpensePerCategoryViewDelegate {
    func deleteExpenseData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    
    
    func setExpensePerCategoryData(data: PerExpenseDataMapper) {
        
        guard let perExpenseSummary = data.summary else {
            return
        }
        self.expensePerCategorySummary = perExpenseSummary
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        guard let list = data.expenseList, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.expenseCategory = data.expenseCategory
        self.expenseList += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.getExpensePerCategoryData()
    }
    
    func getExpensePerCategoryData(){
        guard let id = self.selectedType else{
            return
        }
        self.isLoading = true
        self.currentPage = 1
        self.expenseList = []
        self.presenter.getExpensePerCategoryDataFromServer(page: self.currentPage, id: id, startDt: "default", endDt: "default", addedBy: "default")
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.expenseList = []
                    self.refreshTableView()
                    self.getExpensePerCategoryData()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.deleteExpenseDataFromServer(id: deleteId) 
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }
}

extension ExpensePerCategoryViewController : ExpenseAdvanceSearchDelegate{
    func setSearchParam(startDt: String, endDt: String, addedBy: String) {
        var startDate: String = "default"
        var endDate: String = "default"
        var added: String = "default"
        if startDt != ""{
            startDate = startDt
        }
        if endDt != ""{
            endDate = endDt
        }
        if addedBy != ""{
            added = addedBy
        }
        
        guard let id = self.selectedType else{
            return
        }
        
        self.expenseList = []
        self.currentPage = 1
        
        self.presenter.getExpensePerCategoryDataFromServer(page: self.currentPage, id: id, startDt: startDate, endDt: endDate, addedBy: added)
    }
}

extension ExpensePerCategoryViewController: UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}

extension ExpensePerCategoryViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            
            self.expenseList = []
            self.currentPage = 1
            self.searchIsActive = true
            
            guard let id = self.selectedType else{
                return
            }
            
            self.presenter.getExpensePerCategoryDataFromServer(page: self.currentPage, id: id, startDt: startDate, endDt: endDate, addedBy: "default")
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}



 
