//
//  AddNewExpenseViewController.swift
//  Ponno
//
//  Created by a k azad on 8/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

enum ExpenseState : Int {
    case Add
    case Update
}

class AddNewExpenseViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newExpenseBtn: UIButton!{
        didSet{
            newExpenseBtn.setTitle(LanguageManager.NewExpenseAdd, for: .normal)
        }
    }
    @IBOutlet weak var categoryTextField: UITextField!{
        didSet{
            categoryTextField.placeholder = LanguageManager.ExpenseType
        }
    }
    @IBOutlet weak var categoryLabel: UILabel!{
        didSet{
            categoryLabel.text = LanguageManager.ExpenseType
        }
    }
    @IBOutlet weak var employeeTextField: UITextField!{
        didSet{
            if let category = getBusinessCategory(){
            if category.businessCategory == 1{
                     employeeTextField.isHidden = true
                }
            }else{
               employeeTextField.placeholder = LanguageManager.Employee
            }
        }
    }
    @IBOutlet weak var employeeLabel: UILabel!{
        didSet{
            employeeLabel.text = LanguageManager.Employee
//            if let category = getBusinessCategory(){
//                if category.businessCategory == 1{
//                    employeeLabel.isHidden = true
//
//                }else{
//                    employeeLabel.text = LanguageManager.Employee
//                }
//            }else{
//                employeeLabel.isHidden = false
//                employeeLabel.text = LanguageManager.Employee
//            }
        }
    }
    @IBOutlet weak var employeeViewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var expenseAmountLabel: UILabel!{
        didSet{
            expenseAmountLabel.text = LanguageManager.ExpenseAmount
        }
    }
  
    @IBOutlet weak var expenseTextField: UITextField!{
        didSet{
            expenseTextField.placeholder = LanguageManager.ExpenseAmount
        }
    }
    
    @IBOutlet weak var expenseDetailsLabel: UILabel!{
        didSet{
            expenseDetailsLabel.text = LanguageManager.Description
        }
    }
    @IBOutlet weak var expenseDateLbl: UILabel!{
        didSet{
            expenseDateLbl.text = LanguageManager.ExpenseDate
        }
    }
    @IBOutlet weak var expenseDateLblHeight: NSLayoutConstraint!
    @IBOutlet weak var expenseDateTextField: UITextField!{
        didSet{
            expenseDateTextField.placeholder = "dd/mm/yyyy"
        }
    }
    @IBOutlet weak var expenseDateTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var submitButtonLabel: UIButton!
    @IBOutlet weak var detailsTextField: UITextField!{
        didSet{
            detailsTextField.placeholder = LanguageManager.Description
        }
    }
    
    private var presenter = AddNewExpensePresenter(service: ExpenseService())

    let categoryPicker = UIPickerView()
    let employeePicker = UIPickerView()
    
    var expenseList: [ExpenseCategoryList] = []
    var employeeList : [EmployeeList] = []
    var expenseData : PerExpenseDataList?
    var employeeData: Employee?
    var expenseCategory : String?
    
    var expenseCategoryId: Int?
    var employeeId : Int?
    var expenseId: Int?
    
    var isLoading : Bool = false
    var categoryCurrentPage : Int = 1
    var employeeCurrentPage : Int = 1
    var lastPageNo: Int = 0
    
    enum Sections : Int {
        case expenseList
        case employeeList
    }
    
    var expenseState = ExpenseState.Add.rawValue
    
    let dateFormatter = DateFormatter()
    var datePicker = UIDatePicker()
    var expenseDate : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.attachPresenter()
        self.initialFormSetUp()
        self.setUpPickerView()
        self.showExpenseDatePicker()
        self.configureTextFields()
    }
    
    func initialFormSetUp(){
        
        self.newExpenseBtn.addTarget(self, action: #selector(onExpenseBtnPressed), for: .touchUpInside)
        self.employeeViewheightConstraint.constant = 0
        self.employeeTextField.isHidden = true
        self.employeeLabel.isHidden = true
        
        if self.expenseState == ExpenseState.Update.rawValue{
            self.title = LanguageManager.UpdateInfo
            guard let data = self.expenseData else{
                return
            }
            self.expenseTextField.text = data.amount
            self.expenseId = data.expenseId
            
            guard let categoryName = self.expenseCategory else{
                return
            }
            self.categoryTextField.text = categoryName
            self.detailsTextField.text = data.description
            
            self.expenseDateTextField.isHidden = true
            self.expenseDateLbl.isHidden = true
            self.expenseDateLblHeight.constant = 0.0
            self.expenseDateTextFieldHeight.constant = 0.0
            
            if categoryName == "Salary"{
                if let category = getBusinessCategory(){
                    if category.businessCategory == 1{
                        employeeTextField.isHidden = true
                        self.employeeLabel.isHidden = true
                        self.employeeViewheightConstraint.constant = 0.01
                    }else{
                        self.employeeTextField.isHidden = false
                        self.employeeLabel.isHidden = false
                        
                        self.employeeViewheightConstraint.constant = 75
                    }
                }
                self.employeeTextField.text = data.name
                self.employeeId = data.userId
            }else{
                self.employeeTextField.isHidden = true
                self.employeeLabel.isHidden = true
                self.employeeViewheightConstraint.constant = 0
                self.employeeId = nil
            }
        }else{
            self.title = LanguageManager.NewExpense
            self.expenseDateTextField.isHidden = false
            self.expenseDateLbl.isHidden = false
            self.expenseDateLblHeight.constant = 21.0
            self.expenseDateTextFieldHeight.constant = 40.0
            self.expenseDateLbl.text = LanguageManager.Date
            self.expenseDateTextField.text = currentDateFormatter(format: "dd MMM, yyyy", date: Date())
            self.expenseDate = currentDateFormatter(format: "yyyy-MM-dd", date: Date())
        }
       
    }
    
    @objc func onExpenseBtnPressed(sender: UIButton){
        self.alertWithTextField()
    }
    
    func setUpPickerView(){
        categoryPicker.delegate = self
        employeePicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCategory(sender:)))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.categoryTextField.inputView = categoryPicker
        self.categoryTextField.inputAccessoryView = toolBar
        
        let employeeToolBar = UIToolbar()
        employeeToolBar.barStyle = UIBarStyle.default
        employeeToolBar.isTranslucent = true
        employeeToolBar.tintColor = UIColor.black
        employeeToolBar.sizeToFit()
        
        let cancelEmployeeButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneEmployeeButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnEmployee(sender:)))
        
        employeeToolBar.setItems([cancelEmployeeButton,spaceButton, doneEmployeeButton], animated: false)
        employeeToolBar.isUserInteractionEnabled = true
        
        self.employeeTextField.inputView = employeePicker
        self.employeeTextField.inputAccessoryView = employeeToolBar
    }
    
    func addDoneButtonOnExpenseAmount(){
        let doneToolBar = UIToolbar()
        doneToolBar.barStyle = UIBarStyle.default
        doneToolBar.isTranslucent = true
        doneToolBar.tintColor = UIColor.black
        doneToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnExpenseAmount(sender:)))
        
        doneToolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        doneToolBar.isUserInteractionEnabled = true
        
       // self.expenseAmountTextField.inputAccessoryView = doneToolBar
    }
    
    @objc func onPressingDoneOnEmployee (sender : UIBarButtonItem){
        self.employeeTextField.resignFirstResponder()
        self.expenseTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnCategory(sender : UIBarButtonItem){
        self.categoryTextField.resignFirstResponder()
        if self.categoryTextField.text != "Salary" {
            self.employeeViewheightConstraint.constant = 0
            self.expenseTextField.becomeFirstResponder()
            self.employeeTextField.isHidden = true
            self.employeeLabel.isHidden = true
        }else{
            if let category = getBusinessCategory(){
                if category.businessCategory == 1{
                     employeeTextField.isHidden = true
                    self.employeeLabel.isHidden = true
                    self.employeeViewheightConstraint.constant = 0.01
                }else{
                    self.employeeTextField.isHidden = false
                    self.employeeLabel.isHidden = false
                    self.employeeViewheightConstraint.constant = 75
                    self.employeeTextField.becomeFirstResponder()
                }
            }
        }
    }
    
    @objc func onPressingDoneOnExpenseAmount(sender : UIBarButtonItem){
        self.expenseTextField.resignFirstResponder()
        self.detailsTextField.becomeFirstResponder()
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    
    @IBAction func submitButton(_ sender: UIButton) {
        if isValidated(){
            if self.expenseState == ExpenseState.Update.rawValue{
                guard let categoryId = self.expenseCategoryId, let amountText = self.expenseTextField.text, let amount = Double(amountText), let description = detailsTextField.text, let id = self.expenseId else {
                    return
                }
                
                let employeeId = self.employeeId ?? nil
                
                let param: [String: Any] = ["id": id, "category": categoryId, "employee_id": employeeId, "amount": amount, "description": description]
                
                if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
                    print(json)
                }
                
                self.presenter.postExpenseUpdateDataToServer(expenseUpdateData: param)
                
            }else{
                let employeeId = self.employeeId ?? nil
                var category = ""
                if self.expenseCategoryId == nil{
                    category = self.categoryTextField.text ?? ""
                }else{
                    category = self.expenseCategoryId?.toString() ?? ""
                }
//                let category = self.expenseCategoryId ?? 0
                if let date = self.expenseDate, let amountText = self.expenseTextField.text, let amount = Double(amountText), let description = detailsTextField.text {
                    let param : [String : Any] = ["category" : category, "employee_id": employeeId, "amount" : amount, "description" : description, "date": date]
                    
                    if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
                        print(json)
                    }
                    
                    self.presenter.postAddExpenseDataToServer(expenseDataArray: param)
                }
            }
        }
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitButtonLabel.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
        if (self.categoryTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.ExpenseTypeIsRequired)
            return false
        }
        else if (self.expenseTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.AmountIsRequired)
            return false
        }
        return true
    }
    
    //Alert Message
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func deleteDisplayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.popToExpensePerCategoryVC()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func popToExpensePerCategoryVC(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is ExpensePerCategoryViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigateToExpenseViewController()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ExpenseViewController") as! ExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func popToSpecificController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is ExpenseViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
}


// TextField Delegate
extension AddNewExpenseViewController : UITextFieldDelegate{
    
    fileprivate func configureTextFields(){
        self.detailsTextField.delegate = self
        self.categoryTextField.underlined()
        self.employeeTextField.underlined()
        self.expenseTextField.underlined()
        self.detailsTextField.underlined()
        self.expenseDateTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.detailsTextField{
            self.view.endEditing(true)
            return true
        }
        return false
    }
}

//Mark: PickerViewDelegate
extension AddNewExpenseViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.categoryPicker{
            return self.expenseList.count
        }
        else {
            return self.employeeList.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == self.categoryPicker{
            let item = self.expenseList[row]
            self.expenseCategoryId = item.id
            self.categoryTextField.text = item.name
            if isLoading == false && row == self.expenseList.count - 1 && self.categoryCurrentPage < self.lastPageNo {
                self.isLoading = true
                self.categoryCurrentPage += 1
                self.presenter.getExpenseCategoryListFromServer(page: self.categoryCurrentPage)
            }
            return self.expenseList[row].name
        }
        else{
            let employee = self.employeeList[row]
            self.employeeTextField.text = employee.name
            self.employeeId = employee.id
            if isLoading == false && row == self.employeeList.count - 1 && self.employeeCurrentPage < self.lastPageNo{
                self.isLoading = true
                self.employeeCurrentPage += 1
                self.presenter.getEmployeeListFromServer(page: self.employeeCurrentPage)
            }
            return self.employeeList[row].name
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.categoryPicker{
            let item = self.expenseList[row]
            self.categoryTextField.text = item.name
            self.expenseCategory = item.name
            self.expenseCategoryId = item.id
            if expenseCategory != "Salary" {
                self.employeeId = nil
            }
        }
        else{
            
            let employee = self.employeeList[row]
            self.employeeTextField.text = employee.name
            self.employeeId = employee.id
        }
    }

}

//Mark: API Delegate
extension AddNewExpenseViewController: AddNewExpenseViewDelegate{
    func updateExpenseList(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
        
    }
    
//    func deleteExpenseData(data: AddDataMapper) {
//        guard let message = data.message else {
//            return
//        }
//        self.deleteDisplayMessage(userMessage: message)
//    }
    
    func setEmployeeListData(employeeListData: EmployeeDataMapper) {
        guard let employee = employeeListData.employee, employee.count > 0 else {
            return
        }
        self.isLoading = false

        self.employeeList += employee
        guard let pagination = employeeListData.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setCategoryListData(categoryData: ExpenseCategoriesDataMapper) {
        guard let categoryList = categoryData.expenseCategoryList, categoryList.count > 0 else {
            return
        }
        self.isLoading = false
        self.expenseList += categoryList
        //self.expenseCategory = categoryData.
//        let newExpense = ExpenseCategoryList(name: "নতুন খরচ যোগ")
//        categoryList.insert(newExpense, at: 0)
//        if let freemium = getViewSettings(){
//            if freemium.dueCustomer != nil{
//                self.expenseList += categoryList.filter{($0.name != "Salary")}
//            }
//        }else{
//
//        }
        
        guard let pagination = categoryData.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func setAddExpenseData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.displayMessage(userMessage: data)
    }
    
    func onCustomerFailed(data: String) {
        //
    }
    
    func showLoading() {
        self.showLoader()
        self.submitBtnControlWith(isEnabled: false)
    }
    
    func hideLoading() {
        self.hideLoader()
        self.submitBtnControlWith(isEnabled: true)
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.presenter.getExpenseCategoryListFromServer(page: self.categoryCurrentPage)
        self.presenter.getEmployeeListFromServer(page: self.employeeCurrentPage)
    }
}

extension AddNewExpenseViewController{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewExpenseAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.NewExpense
            textField.textAlignment = .center
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //self.dismiss(animated: true, completion: nil)
            self.categoryTextField.text = ""
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                //self.showAlert(title: "খরচের নাম অসম্পূর্ণ", message: "")
//                alertController.textFields![0]?isEm
                self.showMessage(userMessage: LanguageManager.ExpenseTypeIsRequired)
                return
            }
            
            self.categoryTextField.text = text
            self.employeeId = nil
            self.expenseCategoryId = Int(text)
            
            self.employeeTextField.isHidden = true
            self.employeeLabel.isHidden = true
            self.employeeViewheightConstraint.constant = 0
            
            self.categoryTextField.resignFirstResponder()
            self.expenseTextField.becomeFirstResponder()
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
}

extension AddNewExpenseViewController{
    func showMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.alertWithTextField()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}

//Mark: ExpenseDate
extension AddNewExpenseViewController{
    func showExpenseDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        expenseDateTextField.inputAccessoryView = toolbar
        expenseDateTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        expenseDateTextField.text = currentDateFormatter(format: "dd MMM, yyyy", date: datePicker.date)
        self.expenseDate = currentDateFormatter(format: "yyyy-MM-dd", date: datePicker.date)
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func currentDateFormatter(format: String, date: Date) -> String{
        dateFormatter.dateFormat = format
        let currentDate = dateFormatter.string(from: date)
        return currentDate
    }
    
}


