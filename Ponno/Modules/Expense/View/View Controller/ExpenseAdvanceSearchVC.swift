//
//  ExpenseAdvanceSearchVC.swift
//  Ponno
//
//  Created by a k azad on 24/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class ExpenseAdvanceSearchVC: UIViewController {

    @IBOutlet weak var searchFilterLbl: UILabel!
    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var toDateTextField: UITextField!
    @IBOutlet weak var addedByTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = ExpenseAdvanceSearchPresenter(service: ExpenseService())
    
    var advanceSearchDelegate : ExpenseAdvanceSearchDelegate?
    
    let employeePicker = UIPickerView()
    private var employeeList : [EmployeeList] = []
    
    var employeeId : String?
    var datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
        self.setUpPickerView()
        self.showStartDatePicker()
        self.showEndDatePicker()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.setUpInitialLanguage()
        self.fromDateTextField.placeholder = LanguageManager.StartDate
        self.toDateTextField.placeholder = LanguageManager.EndDate
        self.addedByTextField.placeholder = LanguageManager.EmployeeName
        self.searchFilterLbl.text = LanguageManager.SearchFilter
        self.submitBtn.addTarget(self, action: #selector(onSubmit(sender:)), for: .touchUpInside)
    }
    
    func setUpPickerView(){
        employeePicker.delegate = self
        
        let employeeToolBar = UIToolbar()
        employeeToolBar.barStyle = UIBarStyle.default
        employeeToolBar.isTranslucent = true
        employeeToolBar.tintColor = UIColor.black
        employeeToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelEmployeeButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneEmployeeButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnEmployee(sender:)))
        
        employeeToolBar.setItems([cancelEmployeeButton,spaceButton, doneEmployeeButton], animated: false)
        employeeToolBar.isUserInteractionEnabled = true
        
        self.addedByTextField.inputView = employeePicker
        self.addedByTextField.inputAccessoryView = employeeToolBar
    }
    
    @objc func onPressingDoneOnEmployee(sender : UIBarButtonItem){
        self.addedByTextField.resignFirstResponder()
        self.submitBtn.becomeFirstResponder()
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if fromDateTextField.text == "" && toDateTextField.text == "" && addedByTextField.text == ""{
            showAlert(title: LanguageManager.SelectAnItemToSearch, message: "")
            return false
        }
        if fromDateTextField.text != "" || toDateTextField.text != ""{
            if toDateTextField.text == ""{
                showAlert(title: LanguageManager.EndDateIsRequired, message: "")
                return false
            }
            if fromDateTextField.text == ""{
                showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
                return false
            }
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            let startDate = self.fromDateTextField.text ?? "default"
            let endDate = self.toDateTextField.text ?? "default"
            
            let addedBy = self.employeeId ?? "default"
            advanceSearchDelegate?.setSearchParam(startDt: startDate, endDt: endDate, addedBy: "\(addedBy)")
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
}

//DatePicker
extension ExpenseAdvanceSearchVC {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        fromDateTextField.inputAccessoryView = toolbar
        fromDateTextField.inputView = datePicker        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if fromDateTextField.isFirstResponder{
            fromDateTextField.text = formatter.string(from: datePicker.date)
            toDateTextField.becomeFirstResponder()
        }
        else {
            toDateTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDateTextField.inputAccessoryView = toolbar
        toDateTextField.inputView = datePicker
    }
}


//Mark: PickerViewDelegate
extension ExpenseAdvanceSearchVC : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.employeeList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let employee = self.employeeList[row]
        self.addedByTextField.text = employee.name
        self.employeeId = "\(employee.id)"
       
        return self.employeeList[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            let employee = self.employeeList[row]
            self.addedByTextField.text = employee.name
            self.employeeId = "\(employee.id)"
    }

}

//Api Delegate
extension ExpenseAdvanceSearchVC : ExpenseAdvanceSearchViewDelegate{
    func setEmployeeListData(employeeListData: EmployeeDataMapper) {
        guard let employee = employeeListData.employee, employee.count > 0 else{
            return
        }
        self.employeeList = employee
    }
    
    func onFailed(data: String) {
        
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getEmployeeListFromServer(getAll: true)
    }
}
