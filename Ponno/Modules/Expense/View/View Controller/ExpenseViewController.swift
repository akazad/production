//
//  ExpenseViewController.swift
//  Ponno
//
//  Created by a k azad on 21/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

protocol ExpenseAdvanceSearchDelegate: NSObjectProtocol {
    func setSearchParam(startDt: String, endDt: String, addedBy: String)
}

class ExpenseViewController: BaseViewController {
    
    private var presenter = ExpensePresenter(service: ExpenseService())

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var dateSearchHeight: NSLayoutConstraint!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    
    var datePicker = UIDatePicker()

    var searchIsActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var searchEnable : Bool = false
     
    private var expenseList : [ExpenseList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var selectedType : Int?
    var expenseName : String?
   
    var expenseSummary : [ExpenseSummary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var filteredExpenseList : [ExpenseList] = []
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    let manager = CollectionViewScrollManager()
    
    var employeeId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.addFloaty()
        self.configureTableView()
        self.setNavigationBarTitle()
        self.configureCollectionView()
        self.setBarBtn()
        self.initialSetUp()
        self.showStartDatePicker()
        self.showEndDatePicker()
        //self.setUpSearchBar()
        
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.ExpenseBook
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func initialSetUp(){
        self.searchBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        self.dateSearchHeight.constant = 0.0
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
//        viewController.expenseList = self.expenseList
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToFreemiumSaleableViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumSaleableProductsVC") as! FreemiumSaleableProductsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //FloatingButton
    func addFloaty(){
        
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ExpenseViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.expenseList = []
        self.currentPage = 1
        self.presenter.getExpenseDataFromServer(page: self.currentPage, startDt: "default", endDt: "default", addedBy: "default")
        
        refreshControl.endRefreshing()
    }
    
    func popOverToExpenseAdvanceSearchVC(sender: UIButton){
        
        if let popoverContentController = self.storyboard?.instantiateViewController(withIdentifier: "ExpenseAdvanceSearchVC") as? ExpenseAdvanceSearchVC{
            popoverContentController.modalPresentationStyle = .popover
            popoverContentController.preferredContentSize = CGSize(width:560,height:310)
            popoverContentController.advanceSearchDelegate = self
            
            showPopup(popoverContentController, sourceView: sender)
        }
        
        //self.showPopup(popoverContentController, sourceView: <#T##UIView#>)
        
//        if let controller = popoverContentController{
//            controller.advanceSearchDelegate = self
//            if let popoverPresentationController = controller.popoverPresentationController {
//                popoverPresentationController.sourceView = self.view
//                popoverPresentationController.sourceRect = CGRect(x: 335, y: 54, width: 0, height: 0)
//                popoverPresentationController.delegate = self
//                if let popoverController = popoverContentController {
//                    present(popoverController, animated: true, completion: nil)
//                }
//            }
//        }
    }
    
    func setBarBtn(){
        
        let dateSearchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        dateSearchBtn.setImage(UIImage(named: "dateSearch"), for: UIControl.State.normal)
        dateSearchBtn.addTarget(self, action: #selector(onDateSearch(sender:)), for: UIControl.Event.touchUpInside)
        dateSearchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        dateSearchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        dateSearchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn2 = UIBarButtonItem(customView: dateSearchBtn)
        
        let advanceSearchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        advanceSearchBtn.setImage(UIImage(named: "filterSearch"), for: UIControl.State.normal)
        advanceSearchBtn.addTarget(self, action: #selector(onAdvanceSearchTapped(sender:)), for: UIControl.Event.touchUpInside)
        advanceSearchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        advanceSearchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        advanceSearchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn3 = UIBarButtonItem(customView: advanceSearchBtn)
        
        
        if let category = getBusinessCategory(){
            if category.businessCategory == 1{
                navigationItem.setRightBarButtonItems([barBtn2], animated: true)
            }else{
                navigationItem.setRightBarButtonItems([barBtn3,barBtn2], animated: true)
            }
        }
        
        
    }
    
    @objc func onDateSearch(sender: UIBarButtonItem){
        if searchEnable == false{
            self.dateSearchHeight.constant = 30.0
            self.searchEnable = !searchEnable
        }else{
            self.dateSearchHeight.constant = 0.01
            searchEnable = !searchEnable
        }
    }
    
    @objc func onAdvanceSearchTapped(sender: UIButton){
        self.popOverToExpenseAdvanceSearchVC(sender: sender)
    }
    
}

extension ExpenseViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        if let viewSettings = getViewSettings(){
            if viewSettings.salePanel == "FreemiumSale"{
                self.navigateToFreemiumSaleableViewController()
            }else{
                self.navigateToAddNewSaleViewController()
            }
        }
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController() 
    }
    
    func navigateToAddExpenseVC(){
        self.navigateToAddNewExpenseViewController()
    }
}

////MARK: SearchBar Delegate
//extension ExpenseViewController: UISearchBarDelegate{
//    fileprivate func setUpSearchBar(){
//        self.searchBar.delegate = self
//    }
//
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        isSearchActive = false
//    }
//
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        isSearchActive = false
//
//        self.view.endEditing(true)
//    }
//
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        isSearchActive = false
//        self.view.endEditing(true)
//    }
//
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        isSearchActive = false
//        self.view.endEditing(true)
//    }
//
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//
//
//        if self.expenseList.count > 0 {
//            if searchText == "" {
//                isSearchActive = false
//                return
//            }
//        }
//
//        guard let textToSearch = searchBar.text else {
//            return
//        }
//
//        filteredExpenseList = self.expenseList.filter { (expense : ExpenseList) -> Bool in
//            return expense.name.lowercased().contains(textToSearch.lowercased())
//
//        }
//
//        if filteredExpenseList.count == 0 {
//            isSearchActive = false
//        }
//        else {
//            isSearchActive = true
//        }
//
//    }
//}

//MARK: CollectionView Delegate And Data Source
extension ExpenseViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(ExpenseSummaryCell.nib, forCellWithReuseIdentifier: ExpenseSummaryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let expenseSummaryItem = self.expenseSummary, expenseSummaryItem.count > 0 else{
            return 0
        }
        return expenseSummaryItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ExpenseSummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: ExpenseSummaryCell.identifier, for: indexPath) as! ExpenseSummaryCell
        guard let expenseSummaryItem = self.expenseSummary, expenseSummaryItem.count > 0 else {
            return cell
        }
        let list = expenseSummaryItem[indexPath.row]
        
        cell.summaryTitle.text = list.title
        cell.summeryValue.text = list.value
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70.0)
    }
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.expenseSummary, list.count > 0 else{
            return
        }
        let listCount = list.count
        
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//Mark TableView Delegate and Data Source
extension ExpenseViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ExpenseListCell.nib, forCellReuseIdentifier: ExpenseListCell.identifier)
        self.tableView.estimatedRowHeight = 60.0
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.expenseList.count > 0 {
            return self.expenseList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ExpenseListCell = tableView.dequeueReusableCell(withIdentifier: ExpenseListCell.identifier, for: indexPath) as! ExpenseListCell
        cell.selectionStyle = .none
        if self.expenseList.count > 0 {
            let data = self.expenseList[indexPath.row]
            
            cell.expenseType.text = data.name
            cell.numberOfExpenses.text = LanguageManager.Total + " : " + String(describing: data.total) + " " + LanguageManager.Times
            cell.totalExpenses.text = LanguageManager.Expense + " : " + String(describing: data.totalAmount) + " " + Constants.currencySymbol
            if isLoading == false && indexPath.row == self.expenseList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getExpenseDataFromServer(page: self.currentPage, startDt: "default", endDt: "default", addedBy: "default")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.expenseList.count > 0 {
            let expenseItem = self.expenseList[indexPath.row]
            self.expenseName = expenseItem.name
            self.navigateToExpensePerCategoryVC(selectedType: expenseItem.categoryId)
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func navigateToExpensePerCategoryVC(selectedType : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ExpensePerCategoryViewController") as! ExpensePerCategoryViewController
        viewController.selectedType = selectedType
        viewController.expenseName = expenseName
        self.navigationController?.pushViewController(viewController, animated: true)
    }
  
}

// MARK: API Delegate
extension ExpenseViewController : ExpenseViewDelegate{
    func setExpenseData(data: ExpenseDataMapper) {
        guard let expenseSummaryItem = data.summary, expenseSummaryItem.count > 0 else {
            return
        }
        self.expenseSummary = expenseSummaryItem
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        guard let list = data.expenseList, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.expenseList += list
                
        guard let pagination = data.pagination else{
            return
        }
        self.lastPageNo = pagination.lastPageNo
        
    }
    
    func onFailed(message: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.expenseList = []
        self.presenter.getExpenseDataFromServer(page: self.currentPage, startDt: "default", endDt: "default", addedBy: "default")
    }
    
}

extension ExpenseViewController : ExpenseAdvanceSearchDelegate{
    func setSearchParam(startDt: String, endDt: String, addedBy: String) {
        var startDate: String = "default"
        var endDate: String = "default"
        var added: String = "default"
        if startDt != ""{
            startDate = startDt
        }
        if endDt != ""{
            endDate = endDt
        }
        if addedBy != ""{
            added = addedBy
        }
        
        self.expenseList = []
        self.presenter.getExpenseDataFromServer(page: self.currentPage, startDt: startDate, endDt: endDate, addedBy: added)
    }    
}

extension ExpenseViewController: UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}

extension ExpenseViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            
            self.expenseList = []
            self.currentPage = 1
            self.searchIsActive = true
            
            self.presenter.getExpenseDataFromServer(page: self.currentPage, startDt: startDate, endDt: endDate, addedBy: "default")
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

