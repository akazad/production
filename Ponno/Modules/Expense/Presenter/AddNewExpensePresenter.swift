//
//  AddNewExpensePresenter.swift
//  Ponno
//
//  Created by a k azad on 10/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol AddNewExpenseViewDelegate : NSObjectProtocol {
    func setAddExpenseData(data : AddDataMapper)
    func setCategoryListData(categoryData: ExpenseCategoriesDataMapper)
    func setEmployeeListData(employeeListData: EmployeeDataMapper)
    func updateExpenseList(data: AddDataMapper)
    func onFailed(data: String)
    func onCustomerFailed(data : String)
    func showLoading()
    func hideLoading()
}
class AddNewExpensePresenter: NSObject {
    
    private let service : ExpenseService
    weak private var viewDelegate : AddNewExpenseViewDelegate?
    
    init(service : ExpenseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AddNewExpenseViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getExpenseCategoryListFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getExpenseCategoryListData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCategoryListData(categoryData: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message) 
        })
    }
    
    func getEmployeeListFromServer(page: Int){
        self.viewDelegate?.showLoading()
        let service = EmployeeService()
        service.getEmployeeData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setEmployeeListData(employeeListData: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCustomerFailed(data: message)
        })
    }

    func postAddExpenseDataToServer(expenseDataArray: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.getAddNewExpenseData(param: expenseDataArray, success: { (expenseData) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setAddExpenseData(data: expenseData)
        }, failure: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: data)
        })
    }
    
    func postExpenseUpdateDataToServer(expenseUpdateData: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.getExpenseUpadateData(param: expenseUpdateData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateExpenseList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    
    
}

