//
//  ExpenseAdvanceSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 24/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol ExpenseAdvanceSearchViewDelegate : NSObjectProtocol {
    func setEmployeeListData(employeeListData: EmployeeDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class ExpenseAdvanceSearchPresenter: NSObject {
    
    private let service : ExpenseService
    weak private var viewDelegate : ExpenseAdvanceSearchViewDelegate?
    
    init(service : ExpenseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ExpenseAdvanceSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getEmployeeListFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        let service = EmployeeService()
        service.getAllEmployeeData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setEmployeeListData(employeeListData: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}

