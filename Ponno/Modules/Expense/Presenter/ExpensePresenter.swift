//
//  ExpensePresenter.swift
//  Ponno
//
//  Created by a k azad on 21/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol ExpenseViewDelegate : NSObjectProtocol {
    func setExpenseData(data : ExpenseDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}
class ExpensePresenter: NSObject {
    
    private let service : ExpenseService
    weak private var viewDelegate : ExpenseViewDelegate?
    
    init(service : ExpenseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ExpenseViewDelegate){
        self.viewDelegate = viewDelegate
    }
   
    func  getExpenseDataFromServer(page: Int, startDt: String, endDt: String, addedBy: String){
        self.viewDelegate?.showLoading()
        self.service.getExpenseData(page: page, startDt: startDt, endDt: endDt, addedBy: addedBy, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setExpenseData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
}
