//
//  ExpenseListPerCategoryPresenter.swift
//  Ponno
//
//  Created by a k azad on 30/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol ExpensePerCategoryViewDelegate : NSObjectProtocol {
    func setExpensePerCategoryData(data : PerExpenseDataMapper)
    func deleteExpenseData(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}
class ExpensePerCategoryPresenter: NSObject {
    
    private let service : ExpenseService
    weak private var viewDelegate : ExpensePerCategoryViewDelegate?
    
    init(service : ExpenseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : ExpensePerCategoryViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getExpensePerCategoryDataFromServer(page: Int, id : Int, startDt: String, endDt: String, addedBy: String){
        self.viewDelegate?.showLoading()
        self.service.getExpensePerCategoryData(page: page, id: id, startDt: startDt, endDt: endDt, addedBy: addedBy, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setExpensePerCategoryData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    func deleteExpenseDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.deleteExpenseData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.deleteExpenseData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
