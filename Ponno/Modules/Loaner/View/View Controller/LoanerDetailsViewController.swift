//
//  LoanerDetailsBaseViewController.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import FloatingPanel
import SDWebImage

class LoanerDetailsViewController: UIViewController, FloatingPanelControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = LoanerDetailsPresenter(service: LoanerService())
    
    var fpc: FloatingPanelController!
    var bottomShetVC : LoanerTransactionHistoryViewController!
    
    var loanerDetails : LoanerInfo?{
        didSet{
            self.refreshTableView()
        }
    }
    var loanInfo : LoanInfo?
    var loanerId : Int?
    var loanerName: String?
    var currentLoan : String?
    
    var interestAmount : String?
    var deadline: String?
    
    enum Sections : Int{
        case LoanerInfo
        case LoanerSummary
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addBottomSheet()
        self.setUpInitialLanguage()
        self.initialSetup()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.title = LanguageManager.LoanerDetails
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func addBottomSheet(){
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = UIColor.lightGreen.withAlphaComponent(0.5)
        fpc.surfaceView.layer.cornerRadius = 9.0
        fpc.surfaceView.clipsToBounds = true
        
        fpc.surfaceView.shadowHidden = false
        bottomShetVC = storyboard?.instantiateViewController(withIdentifier: "LoanerTransactionHistoryViewController") as? LoanerTransactionHistoryViewController
        bottomShetVC.loanerId = self.loanerId
        
        if let interestAmount = self.interestAmount{
            bottomShetVC.interestAmount = interestAmount
        }
        
        if let deadline = self.deadline{
            bottomShetVC.deadline = deadline
        }
        
        fpc.set(contentViewController: bottomShetVC)
        fpc.track(scrollView: bottomShetVC.tableView)
        fpc.addPanel(toParent: self)
    }
    
    func removeBottomSheet(){
        fpc.removePanelFromParent(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeBottomSheet()
    }
    
}

//Mark: TableView Delegate and DataSource
extension LoanerDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(LoanerDetailsCell.nib, forCellReuseIdentifier: LoanerDetailsCell.identifier)
        self.tableView.register(LoanerDetailsInfoCell.nib, forCellReuseIdentifier: LoanerDetailsInfoCell.identifier)
        //self.tableView.estimatedRowHeight = 250.0
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = true
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorStyle = .none
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.LoanerInfo.rawValue:
            return 1
        case Sections.LoanerSummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case Sections.LoanerInfo.rawValue:
            let cell : LoanerDetailsCell = tableView.dequeueReusableCell(withIdentifier: LoanerDetailsCell.identifier, for: indexPath) as! LoanerDetailsCell
            cell.selectionStyle = .none
            
            guard let item = self.loanerDetails else{
                return cell
            }
            
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.loanerImage + item.image), placeholderImage: UIImage(named: "placeholder"))
            
            cell.nameLabel.text = item.name
            cell.phoneLabel.text = item.mobile
            cell.addressLabel.text = item.address
            self.loanerName = item.name
            
            
            cell.loanPayBtn.addTarget(self, action: #selector(onLoanPayBtnTapped), for: .touchUpInside)
            cell.loanEditBtn.addTarget(self, action: #selector(onLoandEditBtnTapped), for: .touchUpInside)
            cell.loanDeleteBtn.addTarget(self, action: #selector(onLoanDeleteBtnTapped), for: .touchUpInside)
            
            return cell
        
        case Sections.LoanerSummary.rawValue:
            let cell : LoanerDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: LoanerDetailsInfoCell.identifier, for: indexPath) as! LoanerDetailsInfoCell
            cell.selectionStyle = .none
            
            if let loanInfo = self.loanInfo {
                
                let currentLoan = loanInfo.currentLoan
                if currentLoan.isEmpty && currentLoan != ""{
                    cell.currentLoanLbl.text = "0.00" + Constants.currencySymbol
                }else{
                    cell.currentLoanLbl.text = loanInfo.currentLoan + Constants.currencySymbol
                }
                
                let interest = loanInfo.interest
                if interest.isEmpty && interest != ""{
                    cell.interestAmount.text = "0.00" + Constants.currencySymbol
                }else{
                    cell.interestAmount.text = loanInfo.interest + Constants.currencySymbol
                }
                
                let loanAmount = Double(loanInfo.amount) ?? 0.00
                if loanAmount <= 0.00 {
                    cell.loanLbl.text = "0.00" + Constants.currencySymbol
                }else{
                    cell.loanLbl.text = loanInfo.amount + Constants.currencySymbol
                }
                
                
                let amount = Double(loanInfo.amount)
                let interset = Double(loanInfo.interest)
                if let amountValue = amount, let interestValue = interset{
                    let loan = amountValue + interestValue
                    cell.totalLoanLbl.text = "\(loan)" + Constants.currencySymbol
                }
            }
            
            
            
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.LoanerInfo.rawValue:
            return UITableView.automaticDimension
        case Sections.LoanerSummary.rawValue:
            return 140.0
        default:
            return 0
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onLoanPayBtnTapped(sender: UIButton){
        self.navigateToLoanPaidVC()
    }
    
    @objc func onLoandEditBtnTapped(sender: UIButton){
        
        self.navigateToLoanerUpdateVC()
    }
    
    @objc func onLoanDeleteBtnTapped(sender: UIButton){
        guard let loanerId = self.loanerId else {
            return
        }
        self.confirmationMessage(userMessage: "", deleteId: loanerId)
    }
    
    
}

//Mark: Api Delegate
extension LoanerDetailsViewController : LoanerDetailsViewDelegate{
    func setLoanerDetailsData(data: LoanViewDataMapper) {
        guard let info = data.loanInfo else{
            return
        }
        self.loanInfo = info
        self.currentLoan = info.currentLoan
        self.interestAmount = info.interest
        
        
        guard let details = data.loanInfo?.loanerInfo else {
            return
        }
        self.loanerDetails = details
    }
    
    func onLoanerDelete(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        
        self.showDeleteAlert(title: message, message: "")
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.loanerId else {
            return
        }
        self.presenter.getLoanerDetailsDataFromServer(id: id)
    }
}

extension LoanerDetailsViewController {
    func navigateToLoanPaidVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoanPaidViewController") as! LoanPaidViewController
        viewController.loanerName = self.loanerName
        viewController.loanInfo = self.loanInfo
        viewController.loanerId = self.loanerId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToLoanerUpdateVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoanerUpdateViewController") as! LoanerUpdateViewController
        viewController.loanerDetails = self.loanerDetails
        //viewController.loanerId = self.loanerId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.AreYouSure, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            self.presenter.postLoanerDeleteDataToServer(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showDeleteAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
//            self.navigateToLoanListVC()
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToLoanListVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoanerListViewController") as! LoanerListViewController
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
