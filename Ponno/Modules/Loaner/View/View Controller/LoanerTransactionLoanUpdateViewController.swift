//
//  LoanerTransactionLoanUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 27/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class LoanerTransactionLoanUpdateViewController: UIViewController {

    @IBOutlet weak var editedLoanAmountLbl: UILabel!
    @IBOutlet weak var editedLoanAmountTextField: UITextField!
    @IBOutlet weak var editedInterestLbl: UILabel!
    @IBOutlet weak var editedInterestTextField: UITextField!
    @IBOutlet weak var loanDeadlineLbl: UILabel!
    @IBOutlet weak var loanDeadlineTextfield: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = LoanTransactionLoanUpdatePresenter(service: LoanerService())
    
    var datePicker = UIDatePicker()
    var interestAmount : String?
    var loanTransaction : LoanTransactions?
    var loanTransactionId : Int?
    var deadline: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initalSetup()
        self.toolbarSetup()
        self.showDeadlineDatePicker()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initalSetup(){
        self.editedLoanAmountLbl.text = LanguageManager.EditedLoanAmount
        self.editedInterestLbl.text = LanguageManager.EditedInterestAmount
        self.loanDeadlineLbl.text = LanguageManager.Deadline
        self.descriptionLbl.text = LanguageManager.Description
        self.editedLoanAmountTextField.underlined()
        self.editedInterestTextField.underlined()
        self.loanDeadlineTextfield.underlined()
        
        if let item = self.loanTransaction{
            self.editedLoanAmountTextField.text = item.amount
            self.loanTransactionId = item.id
            self.descriptionTextView.text = item.description
            if let interest = self.interestAmount{
                self.editedInterestTextField.text = interest
            }
            if let deadline = self.deadline{
                self.loanDeadlineTextfield.text = deadline
            }
        }
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            if let amount = self.editedLoanAmountTextField.text, let interest = self.editedInterestTextField.text, let deadline = self.loanDeadlineTextfield.text, let id = self.loanTransactionId {
                let description = self.descriptionTextView.text ?? ""
                
                let param : [String : Any] = ["id": id, "amount": amount, "interest": interest, "deadline": deadline, "description": description]
                
                self.presenter.postLoanerTransactionLoanUpdateDataToServer(param: param)
            }
        }
    }
    
    func isValidated()->Bool{
        
        if (self.editedLoanAmountTextField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.EditedLoanAmountIsRequired, message: "")
            return false
        }else if(self.editedInterestTextField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.EditedInterestAmountRequired, message: "")
            return false
        }else if(self.loanDeadlineTextfield.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.LoanDeadlineIsRequired, message: "")
            return false
        }
        return true
    }
    
    func toolbarSetup(){
        let amounttoolbar = UIToolbar()
        amounttoolbar.barStyle = UIBarStyle.default
        amounttoolbar.isTranslucent = true
        amounttoolbar.tintColor = UIColor.black
        amounttoolbar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        let amountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnEditedLoanAmount(sender:)))
        
        amounttoolbar.setItems([cancelButton, spaceButton, amountDoneButton], animated: false)
        amounttoolbar.isUserInteractionEnabled = true
        
        editedLoanAmountTextField.inputAccessoryView = amounttoolbar
        
        let interestAmounttoolbar = UIToolbar()
        interestAmounttoolbar.barStyle = UIBarStyle.default
        interestAmounttoolbar.isTranslucent = true
        interestAmounttoolbar.tintColor = UIColor.black
        interestAmounttoolbar.sizeToFit()
        
        
        let interestAmountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnEditedInterestAmount(sender:)))
        
        interestAmounttoolbar.setItems([cancelButton, spaceButton, interestAmountDoneButton], animated: false)
        interestAmounttoolbar.isUserInteractionEnabled = true
        
        editedInterestTextField.inputAccessoryView = interestAmounttoolbar
        
    }
    
    @objc func onPressingDoneOnEditedLoanAmount(sender: UIBarButtonItem){
        self.editedInterestTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnEditedInterestAmount(sender: UIBarButtonItem){
        self.loanDeadlineTextfield.becomeFirstResponder()
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    func showDeadlineDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.minimumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        loanDeadlineTextfield.inputAccessoryView = toolbar
        loanDeadlineTextfield.inputView = datePicker
        
    }
    
    @objc func cancelDatePicker(sender: UIDatePicker){
        self.view.endEditing(true)
    }
    
    @objc func donedatePicker(sender: UIDatePicker){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyy-MM-dd"
        self.loanDeadlineTextfield.text = formatter.string(from: datePicker.date)
        
        self.descriptionTextView.becomeFirstResponder()
        
    }
    
    func submitBtnControllWith(isEnabled: Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension LoanerTransactionLoanUpdateViewController : LoanerTransactionLoanUpdateViewDelegate{
    func updateLoanTransactionLoanData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.submitBtnControllWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControllWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}
