//
//  NewLoanAddViewController.swift
//  Ponno
//
//  Created by a k azad on 25/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class NewLoanAddViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var loanerSelectionLbl: UILabel!
    @IBOutlet weak var loanerSelectionTextField: UITextField!
    @IBOutlet weak var addNewLoanerBtn: UIButton!
    @IBOutlet weak var loanerNameLbl: UILabel!
    @IBOutlet weak var loanerNameTextField: UITextField!
    @IBOutlet weak var loanerPhoneLbl: UILabel!
    @IBOutlet weak var loanerPhoneTextField: UITextField!
    @IBOutlet weak var loanerAddressLbl: UILabel!
    @IBOutlet weak var loanerAddressTextField: UITextField!
    @IBOutlet weak var loanAmountLbl: UILabel!
    @IBOutlet weak var loanAmountTextField: UITextField!
    @IBOutlet weak var interestAmountLbl: UILabel!
    @IBOutlet weak var interestAmountTextField: UITextField!
    @IBOutlet weak var loanDeadLineLbl: UILabel!
    @IBOutlet weak var loanDeadLineTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    
    private var presenter = NewLoanAddPresenter(service: LoanerService())
    
    var loanerList : [LoanList] = []{
        didSet{
            //self.refreshTableView()
        }
    }
    var loanerId : Int = -1
    
    var newLoanerSetup: Bool = false
    var loanerPicker = UIPickerView()
    var datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.cofigureTextField()
        self.setUpPickerView()
        self.showDeadlineDatePicker()
        self.setUpInitialLanguage()
        self.attachPresenter()

    }
    
    
    func initialSetup(){
        
        self.title = LanguageManager.NewLoan
        self.loanerSelectionLbl.text = LanguageManager.LoanerSelection
        self.loanAmountLbl.text = LanguageManager.LoanAmount
        self.interestAmountLbl.text = LanguageManager.InterestAmount
        self.loanDeadLineLbl.text = LanguageManager.LoanDeadline
        self.descriptionLbl.text = LanguageManager.Description
        self.loanerNameLbl.isHidden = true
        self.loanerNameTextField.isHidden = true
        self.loanerPhoneLbl.isHidden = true
        self.loanerPhoneTextField.isHidden = true
        self.loanerAddressLbl.isHidden = true
        self.loanerAddressTextField.isHidden = true
        self.stackViewHeight.constant = 0.0
        
        self.addNewLoanerBtn.addTarget(self, action: #selector(onAddNewLoanerBtnTapped), for: .touchUpInside)
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
    }
    
    @objc func onAddNewLoanerBtnTapped(sender: UIButton){
        self.newLoanerSetup = !self.newLoanerSetup
        
        if self.newLoanerSetup == false{
            self.loanerNameLbl.isHidden = true
            self.loanerNameTextField.isHidden = true
            self.loanerPhoneLbl.isHidden = true
            self.loanerPhoneTextField.isHidden = true
            self.loanerAddressLbl.isHidden = true
            self.loanerAddressTextField.isHidden = true
            self.stackViewHeight.constant = 0.0
            self.loanerSelectionTextField.text = ""
        }else{
            self.loanerNameLbl.isHidden = false
            self.loanerNameTextField.isHidden = false
            self.loanerPhoneLbl.isHidden = false
            self.loanerPhoneTextField.isHidden = false
            self.loanerAddressLbl.isHidden = false
            self.loanerAddressTextField.isHidden = false
            self.stackViewHeight.constant = 223.0
            
            self.loanerSelectionTextField.text = LanguageManager.NewLoanerAdd
            self.loanerNameLbl.text = LanguageManager.LoanerName
            self.loanerPhoneLbl.text = LanguageManager.LoanerPhone
            self.loanerAddressLbl.text = LanguageManager.LoanerAddress
        }
        
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            
            let param : [String : Any]
            
            let deadline = self.loanDeadLineTextField.text ?? ""
            let description = self.descriptionTextView.text ?? ""
            guard let amount = self.interestAmountTextField.text, let interest = self.interestAmountTextField.text else {
                return
            }
            if newLoanerSetup == true {
                let loaner = "add_new"
                guard let name = self.loanerNameTextField.text, let phone = self.loanerPhoneTextField.text, let address = self.loanerAddressTextField.text, let amount = self.loanAmountTextField.text, let interest = self.interestAmountTextField.text else {
                    return
                }
                
                param = ["loaner": loaner, "name": name, "mobile": phone, "address": address, "amount": amount, "interest": interest, "deadline": deadline, "description" : description]
                
            }else{
                param = ["loaner" : self.loanerId, "amount": amount, "interest": interest, "deadline": deadline, "description": description]
            }
            
            self.presenter.postNewLoanAddDataToServer(param: param)
        }
    }
    
    func isValidated() -> Bool {
        if (loanerSelectionTextField.text == "") {
            showAlert(title: LanguageManager.LoanerSelectionIsRequired, message: "")
            return false
        }else if loanAmountTextField.text == "" {
            showAlert(title: LanguageManager.LoanerAmountIsRequired, message: "")
            return false
        }else if interestAmountTextField.text == "" {
            showAlert(title: LanguageManager.InterestAmountIsRequired, message: "")
            return false
        }
//        else if loanDeadLineTextField.text == "" {
//            showAlert(title: "Interest amount is required", message: "")
//            return false
//        }
        if self.newLoanerSetup == true {
            if (loanerNameTextField.text == "") {
                showAlert(title: LanguageManager.LoanerNameIsRequired, message: "")
                return false
            }else if loanerPhoneTextField.text == "" {
                showAlert(title: LanguageManager.PhoneNumberIsRequired, message: "")
                return false
            }else if loanerAddressTextField.text == "" {
                showAlert(title: LanguageManager.AddressIsRequired, message: "")
                return false
            }
            
            let phone = self.loanerPhoneTextField.text ?? ""
//            if (!self.validatePhoneNumber(value: phone)){
//                showAlert(title: LanguageManager.PhoneNumberIsIncorrect, message: "")
//                return false
//            }
        }
        return true
    }
    
    func submitBtnControllWith(isEnabled: Bool){
        self.submitBtn.isEnabled = isEnabled
    }

    
    //PickerView
    func setUpPickerView(){
        
        loanerPicker.delegate = self
        
        //LoanerSelection ToolBar
        let loanerToolBar = UIToolbar()
        loanerToolBar.barStyle = UIBarStyle.default
        loanerToolBar.isTranslucent = true
        loanerToolBar.tintColor = UIColor.black
        loanerToolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let paymentDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnLoanerSelection(sender:)))
        loanerToolBar.setItems([cancelButton, spaceButton, paymentDoneButton], animated: false)
        loanerToolBar.isUserInteractionEnabled = true
        
        self.loanerSelectionTextField.inputView = loanerPicker
        self.loanerSelectionTextField.inputAccessoryView = loanerToolBar
        
    }
    
    @objc func onPressingDoneOnLoanerSelection(sender: UIBarButtonItem){
        self.loanAmountTextField.becomeFirstResponder()
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    func showDeadlineDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.minimumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        loanDeadLineTextField.inputAccessoryView = toolbar
        loanDeadLineTextField.inputView = datePicker
        
        
        let amounttoolbar = UIToolbar()
        amounttoolbar.barStyle = UIBarStyle.default
        amounttoolbar.isTranslucent = true
        amounttoolbar.tintColor = UIColor.black
        amounttoolbar.sizeToFit()
        
        
        let amountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnLoanAmount(sender:)))
        
        amounttoolbar.setItems([cancelButton, spaceButton, amountDoneButton], animated: false)
        amounttoolbar.isUserInteractionEnabled = true
        
        loanAmountTextField.inputAccessoryView = amounttoolbar
        
        let interesttoolbar = UIToolbar()
        interesttoolbar.barStyle = UIBarStyle.default
        interesttoolbar.isTranslucent = true
        interesttoolbar.tintColor = UIColor.black
        interesttoolbar.sizeToFit()
        
        
        let interestAmountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnInterestAmount(sender:)))
        
        interesttoolbar.setItems([cancelButton, spaceButton, interestAmountDoneButton], animated: false)
        interesttoolbar.isUserInteractionEnabled = true
        
        interestAmountTextField.inputAccessoryView = interesttoolbar
        
    }
    
    @objc func onPressingDoneOnLoanAmount(sender: UIBarButtonItem){
        self.interestAmountTextField.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnInterestAmount(sender: UIBarButtonItem){
        self.loanDeadLineTextField.becomeFirstResponder()
    }
    
    @objc func donedatePicker(sender: UIDatePicker){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyy-MM-dd"
        self.loanDeadLineTextField.text = formatter.string(from: datePicker.date)
        
        self.descriptionTextView.becomeFirstResponder()
        
    }
    
    @objc func cancelDatePicker(sender: UIDatePicker){
        self.view.endEditing(true)
    }

}

extension NewLoanAddViewController : UITextFieldDelegate{
    func cofigureTextField(){
        self.loanerSelectionTextField.delegate = self
        self.loanerNameTextField.delegate = self
        self.loanerPhoneTextField.delegate = self
        self.loanerAddressTextField.delegate = self
        self.loanAmountTextField.delegate  = self
        self.interestAmountTextField.delegate = self
        self.loanDeadLineTextField.delegate = self
        
        self.loanerSelectionTextField.underlined()
        self.loanerNameTextField.underlined()
        self.loanerPhoneTextField.underlined()
        self.loanerAddressTextField.underlined()
        self.loanAmountTextField.underlined()
        self.interestAmountTextField.underlined()
        self.loanDeadLineTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == loanerNameTextField{
            self.loanerPhoneTextField.becomeFirstResponder()
        }else if textField == loanerPhoneTextField {
            self.loanerAddressTextField.becomeFirstResponder()
        }else if textField == loanerAddressTextField {
            self.loanAmountTextField.becomeFirstResponder()
        }else if textField == loanAmountTextField {
            self.interestAmountTextField.becomeFirstResponder()
        }else if textField == interestAmountTextField{
            self.loanDeadLineTextField.becomeFirstResponder()
        }else{
            self.view.endEditing(true)
        }
        return false
    }
    
}

//Mark: Picker Delegate and DataSource
extension NewLoanAddViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if self.loanerList.count > 0 {
            return self.loanerList.count
        }else{
            return 0
        }
            
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if self.loanerList.count > 0 {
            let item = self.loanerList[row]
            let loaner = item.name
            self.loanerId = item.id
            self.loanerSelectionTextField.text = loaner
            return loaner
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.loanerList.count > 0 {
            let item = self.loanerList[row]
            let loaner = item.name
            self.loanerId = item.id
            self.loanerSelectionTextField.text = loaner
        }else{
            return
        }
    }
}

// Mark: Api Delegate
extension NewLoanAddViewController : NewLoanAddViewDelegate{
    func onNewLoanAddSuccess(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.submitBtnControllWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControllWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

extension NewLoanAddViewController {
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
