//
//  LoanerTransactionUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class LoanerTransactionUpdateViewController: UIViewController {

    @IBOutlet weak var loanWithdrawAmountLbl: UILabel!
    @IBOutlet weak var loanWithdrawAmountTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    private var presenter = LoanTransactionUpdatePresenter(service: LoanerService())
    
    @IBOutlet weak var submitBtn: UIButton!
    
    var loanTransaction : LoanTransactions?
    var transactionId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initalSetup()
        self.toolbarSetup()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initalSetup(){
        self.title = LanguageManager.WithdrawUpdate
        self.loanWithdrawAmountLbl.text = LanguageManager.LoanWithdrawAmount
        self.descriptionLbl.text = LanguageManager.Description
        
        self.loanWithdrawAmountTextField.underlined()
        
        if let transaction = self.loanTransaction {
            self.loanWithdrawAmountTextField.text = transaction.amount
            self.transactionId = transaction.id
        }
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            
            if let withdrawAmount = self.loanWithdrawAmountTextField.text, let amount = Double(withdrawAmount),
                let id = self.transactionId {
                let param : [String :Any] = ["id": id, "amount" : amount]
                
                self.presenter.postLoanerTransactionUpdateDataToServer(param: param)
            }
        }
    }
    
    func toolbarSetup(){
        let amounttoolbar = UIToolbar()
        amounttoolbar.barStyle = UIBarStyle.default
        amounttoolbar.isTranslucent = true
        amounttoolbar.tintColor = UIColor.black
        amounttoolbar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        let amountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnLoanWithdrawAmount(sender:)))
        
        amounttoolbar.setItems([cancelButton, spaceButton, amountDoneButton], animated: false)
        amounttoolbar.isUserInteractionEnabled = true
        
        loanWithdrawAmountTextField.inputAccessoryView = amounttoolbar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnLoanWithdrawAmount(sender: UIBarButtonItem){
        self.descriptionTextView.becomeFirstResponder()
    }
    
    func isValidated() -> Bool {
        if (loanWithdrawAmountTextField.text == "") {
            showAlert(title: LanguageManager.LoanWithdrawAmountIsRequired, message: "")
            return false
        }
        return true
    }
    
    func submitBtnControllWith(isEnabled: Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension LoanerTransactionUpdateViewController : LoanerTransactionUpdateViewDelegate{
    func updateLoanTransactionData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.submitBtnControllWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControllWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        
    }
}
