//
//  LoanerSerachViewController.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class LoanerSerachViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = LoanerListPresenter(service: LoanerService())
    
    var loanerList : [LoanList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isLoading : Bool = false
    var currentPage: Int = 1
    var lastPage : Int = 1
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    var searchText = ""
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpSearchBar()
        self.configureTableView()
        self.setUpInitialLanguage()
        self.attachPresenter()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.presenter.getLoanerSearchDataFromServer(page: self.currentPage, searchString: "")
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}

//Mark: Search Delegate
extension LoanerSerachViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.Search
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.loanerList = []
        self.isLoading = true
        self.isSearchActive = true
        self.presenter.getLoanerSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
    }
}

//Mark: TableView Delegate and DataSource
extension LoanerSerachViewController : UITableViewDelegate, UITableViewDataSource {
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(LoanerListCell.nib, forCellReuseIdentifier: LoanerListCell.identifier)
        self.tableView.estimatedRowHeight = 120.0
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = true
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorStyle = .none
        //self.tableView.addSubview(refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.loanerList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : LoanerListCell = tableView.dequeueReusableCell(withIdentifier: LoanerListCell.identifier, for: indexPath) as! LoanerListCell
        cell.selectionStyle = .none
        
        let item = self.loanerList[indexPath.row]
        
        cell.nameLabel.text = LanguageManager.LoanerName + " : " + item.name
        cell.loanAmount.text = LanguageManager.InitialLoan + " : " + item.amount + Constants.currencySymbol
        cell.interestAmountLbl.text = LanguageManager.InterestAmount + " : " + item.interest + Constants.currencySymbol
        cell.deadlineLbl.text = LanguageManager.Deadline + " : " + item.deadline
        
        //cell.imageIcon.image = UIImage(named: "loan")
        cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.loanerImage + item.image), placeholderImage: UIImage(named: "placeholder"))
        
        if isLoading == false && indexPath.row == self.loanerList.count - 1 && self.currentPage < self.lastPage{
            self.isLoading = true
            self.currentPage += 1
            self.presenter.getLoanerListDataFromServer(page: self.currentPage)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = self.loanerList[indexPath.row]
        
        let id = item.id
        
        self.navigateToLoanDetailsViewController(id: id)
        
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
}

//Mark : Api Delegate
extension LoanerSerachViewController : LoanerListViewDelegate {
    func setLoanerListData(data: LoanerDataMapper) {
        guard let list = data.loans, list.count > 0 else {
            return
        }
        self.loanerList += list
        
        guard let pagination = data.pagination else{
            return
        }
        self.lastPage = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.currentPage = 1
        self.loanerList = []
        self.presenter.getLoanerSearchDataFromServer(page: self.currentPage, searchString: "")
    }
}

extension LoanerSerachViewController{
    
    func navigateToLoanDetailsViewController(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoanerDetailsViewController") as! LoanerDetailsViewController
        viewController.loanerId = id
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewLoanViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "NewLoanAddViewController") as! NewLoanAddViewController
        
        viewController.loanerList = self.loanerList
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToLoanerSerachViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoanerSerachViewController") as! LoanerSerachViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
