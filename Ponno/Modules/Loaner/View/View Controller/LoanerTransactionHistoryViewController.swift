//
//  LoanerTransactionHistoryViewController.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class LoanerTransactionHistoryViewController: UIViewController {

    @IBOutlet weak var loanTransactionBtn: UIButton!{
        didSet{
            loanTransactionBtn.setTitle(LanguageManager.LoanTransactions, for: .normal)
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = LoanerTransactionPresenter(service: LoanerService())
    
    var loanTransactions : [LoanTransactions] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var loanerId : Int?
    var loanWithdrawAmount : Double?
    
    var interestAmount : String?
    var deadline : String?
    //var loanData : LoanData?
    
    var currentPage : Int = 1
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.initialSetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.loanTransactionBtn.contentEdgeInsets = UIEdgeInsets(top: 8.0, left: 8.0, bottom: 8.0, right: 8.0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.loanTransactions = []
    }
    

}

//Mark: TableView Delegate and DataSource
extension LoanerTransactionHistoryViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(LoanTransactionHistoryCell.nib, forCellReuseIdentifier: LoanTransactionHistoryCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = true
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.loanTransactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : LoanTransactionHistoryCell = tableView.dequeueReusableCell(withIdentifier: LoanTransactionHistoryCell.identifier, for: indexPath) as! LoanTransactionHistoryCell
        
        cell.selectionStyle = .none
        
        let item = self.loanTransactions[indexPath.row]
        
        cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_h_mm_a.rawValue)
        cell.amountLabel.text = LanguageManager.Amount + " : " + item.amount
        
        let description = item.description
        if description == "" {
            cell.descriptionImage.isHidden = true
        }else{
            cell.descriptionImage.isHidden = false
        }
        
        cell.descriptionImage.addTapGestureRecognizer {
            self.showAlert(title: LanguageManager.Description, message: description)
        }
        
        cell.loanTransactionsPopUpBtn.tag = item.id
        cell.loanTransactionsPopUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
        
        if item.type == 1 {
            cell.typeLabel.text = LanguageManager.Loan
            cell.colorView.backgroundColor = UIColor(red: CGFloat(232/255.0), green: CGFloat(94/255.0), blue: CGFloat(88/255.0), alpha: CGFloat(0.8))
        }
        if item.type == 0 {
            cell.typeLabel.text = LanguageManager.Withdraw
            cell.colorView.backgroundColor = UIColor(red: CGFloat(90/255.0), green: CGFloat(162/255.0), blue: CGFloat(99/255.0), alpha: CGFloat(0.8))
        }
        if isLoading == false && indexPath.row == self.loanTransactions.count - 1 && self.currentPage < self.lastPageNo{
            self.isLoading = true
            self.currentPage += 1
            guard let id = self.loanerId else{
                return cell
            }
            self.presenter.getLoanerTransactionDataFromServer(page: self.currentPage, id: id)
        }

        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62.0
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        let id = sender.tag
        
        for item in self.loanTransactions{
            if id == item.id{
                let type = item.type
                let withdrawAmount = self.loanWithdrawAmount ?? 0.0
                if type == 1 && withdrawAmount == 0.0 {
                    let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToLoanTransactionLoanUpdateVC(item: item)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(editAction)
                    myActionSheet.addAction(cancelAction)
                }
                else if type == 1 && withdrawAmount > 0.0 {
                    let withdrawAmountAction = UIAlertAction(title: LanguageManager.Paid + " : " + "\(withdrawAmount) " + Constants.currencySymbol, style: UIAlertAction.Style.default) { (action) in
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(withdrawAmountAction)
                    myActionSheet.addAction(cancelAction)
                }
                else{
                    let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToLoanTransactionUpdateVC(item: item)
                    }
                    
                    let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(editAction)
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToLoanTransactionUpdateVC(item : LoanTransactions){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoanerTransactionUpdateViewController") as! LoanerTransactionUpdateViewController
        
        viewController.loanTransaction = item
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToLoanTransactionLoanUpdateVC(item : LoanTransactions){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoanerTransactionLoanUpdateViewController") as! LoanerTransactionLoanUpdateViewController
        
        viewController.interestAmount = self.interestAmount
        viewController.loanTransaction = item
        viewController.deadline = self.deadline
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Delete, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            self.presenter.postOnLoanTransactionDeleteDataToServer(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension LoanerTransactionHistoryViewController : LoanerTransactionHistoryViewDelegate{
    func onLoanTransactionDelete(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func setLoanerTransactionData(data: LoanTransactionHistoryDataMapper) {
        guard let list = data.loanTransactions, list.count > 0 else{
            return
        }
        self.loanTransactions += list
        
        self.loanWithdrawAmount = data.loan_withdraw_amount
    }
    
    func onFailed(data: String) {
        
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        guard let id = self.loanerId else{
            return
        }
        self.presenter.getLoanerTransactionDataFromServer(page: self.currentPage, id: id)
    }
}

extension LoanerTransactionHistoryViewController {
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.loanTransactions = []
            self.refreshTableView()

            if let id = self.loanerId {
                self.presenter.getLoanerTransactionDataFromServer(page: self.currentPage, id: id)
            }
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
