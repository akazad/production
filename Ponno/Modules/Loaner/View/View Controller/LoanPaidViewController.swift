//
//  LoanPaidViewController.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class LoanPaidViewController: UIViewController {

    @IBOutlet weak var loanerNameLbl: UILabel!
    @IBOutlet weak var loanerNameTextField: UITextField!{
        didSet{
            loanerNameTextField.placeholder = LanguageManager.LoanerName
        }
    }
    @IBOutlet weak var currentLoanLbl: UILabel!
    @IBOutlet weak var currentLoanTextField: UITextField!{
        didSet{
            currentLoanTextField.placeholder = LanguageManager.CurrentLoan
        }
    }
    @IBOutlet weak var loanWithdrawAmountLbl: UILabel!
    @IBOutlet weak var loanWithdrawAmountTextField: UITextField!{
        didSet{
            loanWithdrawAmountTextField.placeholder = LanguageManager.LoanWithdrawAmount
        }
    }
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = LoanWithdrawPresenter(service: LoanerService())
    
    var loanInfo : LoanInfo?
    var loanerId : Int?
    var loanerName : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.toolBarSetUp()
        self.configureTextFields()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.loanerNameLbl.text = LanguageManager.LoanerName
        self.currentLoanLbl.text = LanguageManager.CurrentLoan
        self.loanWithdrawAmountLbl.text = LanguageManager.LoanWithdrawAmount
        self.descriptionLbl.text = LanguageManager.Description
        
        guard let name = self.loanerName, let currentLoan = loanInfo?.currentLoan else{
            return
        }
        self.loanerNameTextField.text = name
        self.currentLoanTextField.text = currentLoan
        self.loanerNameTextField.isEnabled = false
        self.currentLoanTextField.isEnabled = false
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
    }
    
    
    func toolBarSetUp(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.loanWithdrawAmountTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        self.loanWithdrawAmountTextField.resignFirstResponder()
        self.descriptionTextView.becomeFirstResponder()
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if self.isValidated(){
            
            guard let paid = loanWithdrawAmountTextField.text, let id = self.loanerId else{
                return
            }
            
            let details = descriptionTextView.text ?? ""
            
            let params : [String : Any] = ["loan": id,
                                           "amount" : paid,
                                           "description" : details]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            self.presenter.postLoanerWithdrawDataToServer(param: params)
        }
    }
    
    func isValidated()->Bool{
        if self.loanWithdrawAmountTextField.text == "" {
            showAlert(title: LanguageManager.LoanWithdrawAmountIsRequired, message: "")
            return false
        }
        return true
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }

}

//Mark: Api Delegate
extension LoanPaidViewController : LoanWithdrawViewDelegate{
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

extension LoanPaidViewController :  UITextFieldDelegate{
    
    fileprivate func configureTextFields(){
        self.loanerNameTextField.delegate = self
        self.currentLoanTextField.delegate = self
        self.loanWithdrawAmountTextField.delegate = self
        self.loanerNameTextField.underlined()
        self.currentLoanTextField.underlined()
        self.loanWithdrawAmountTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.loanWithdrawAmountTextField{
            self.descriptionTextView.becomeFirstResponder()
        }
        else{
            self.view.endEditing(true)
        }
        return false
    }
}

