//
//  LoanerUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class LoanerUpdateViewController: UIViewController {

    @IBOutlet weak var imageIcon: CircularImageView!
    @IBOutlet weak var loanerNameLbl: UILabel!
    @IBOutlet weak var loanerNameTextField: UITextField!{
        didSet{
            loanerNameTextField.placeholder = LanguageManager.LoanerName
        }
    }
    @IBOutlet weak var loanerPhoneLbl: UILabel!
    @IBOutlet weak var loanerPhoneTextField: UITextField!{
        didSet{
            loanerPhoneTextField.placeholder = LanguageManager.LoanerPhone
        }
    }
    @IBOutlet weak var loanerAddressLbl: UILabel!
    @IBOutlet weak var loanerAddressTextField: UITextField!{
        didSet{
            loanerAddressTextField.placeholder = LanguageManager.LoanerAddress
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = LoanerUpdatePresenter(service: LoanerService())
    
    var loanerDetails : LoanerInfo?
    var loanerId : Int?
    
    let imagePicker = UIImagePickerController()
    var selectedImage : UIImage?
    var message : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.configureTextFields()
        self.setUpToolBar()
        self.setImageView()
        self.setUpInitialLanguage()
        self.attachPresenter()
        
    }
    
    func initialSetup(){
        self.title = LanguageManager.LoanerUpdate
        self.loanerNameLbl.text = LanguageManager.LoanerName
        self.loanerPhoneLbl.text = LanguageManager.PhoneNumber
        self.loanerAddressLbl.text = LanguageManager.Address
        
        if let info = self.loanerDetails {
            self.loanerNameTextField.text = info.name
            self.loanerPhoneTextField.text = info.mobile
            self.loanerAddressTextField.text = info.address
            
            self.loanerId = info.id
            
            imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.loanerImage + info.image), placeholderImage: UIImage(named: "placeholder"))
        }
        self.submitBtn.addTarget(self, action: #selector(onSubmiBtnTapped), for: .touchUpInside)
        
        imagePicker.delegate = self
        self.imageIcon.addTapGestureRecognizer(action: {self.onTapOnImageView()})
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
        
        if (self.loanerNameTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.NameIsRequired)
            return false
        }
        guard let phone = loanerPhoneTextField.text else {
            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
            return false
        }
        if (!validatePhoneNumber(value: phone)){
            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
            return false
        }
        else if(self.loanerAddressTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.AddressIsRequired)
            return false
        }
        return true
    }
    
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigateToLoanerListViewController()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToLoanerListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoanerListViewController") as! LoanerListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func setUpToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.loanerPhoneTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        self.loanerPhoneTextField.resignFirstResponder()
        self.loanerAddressTextField.becomeFirstResponder()
    }
    
    @objc func onSubmiBtnTapped(sender: UIButton){
        if self.isValidated(){
            guard let name = loanerNameTextField.text, let phone = loanerPhoneTextField.text, let address = loanerAddressTextField.text, let id = self.loanerId else {
                return
            }
            let params : [String: Any] = ["id": id, "name": name, "mobile": phone, "address": address]
            
            
            self.presenter.postLoanerUpdateDataToServer(param: params)
            
//            if self.customerState == CustomerState.Add.rawValue{
//                self.presenter.getAddNewCustomerDataFromServer(customerDataArray: params)
//            }
//            else if self.customerState == CustomerState.Update.rawValue{
//                if let id = self.customerId{
//                    params["id"] = "\(id)"
//                }
//                self.presenter.updateCustomerData(customerData: params)
//            }
//            else{
//                if let id = self.customerId{
//                    params["id"] = "\(id)"
//                }
//                self.presenter.updateCustomerData(customerData: params)
//            }
        }
    }
    
}

//Mark: Image Picker
extension LoanerUpdateViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    private func setImageView(){
        guard let imageUrl = self.loanerDetails?.image else{
            self.setImageInImageView(imageUrl: "")
            return
        }
        
        self.setImageInImageView(imageUrl: ImageUrl.sharedImageInstance.loanerImage + imageUrl)
    }
    
    func setImageInImageView(imageUrl : String){
        self.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageIcon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
    }
    
    func onTapOnImageView(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageIcon.contentMode = .scaleAspectFill
            imageIcon.image = pickedImage
            self.selectedImage = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}




//Mark: TextField Delegate
extension LoanerUpdateViewController : UITextFieldDelegate{
    
    fileprivate func configureTextFields(){
        self.loanerNameTextField.delegate = self
        self.loanerPhoneTextField.delegate = self
        self.loanerAddressTextField.delegate = self
        self.loanerNameTextField.underlined()
        self.loanerPhoneTextField.underlined()
        self.loanerAddressTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.loanerNameTextField{
            self.loanerPhoneTextField.becomeFirstResponder()
        }
        else{
            self.view.endEditing(true)
        }
        return false
    }
}

extension LoanerUpdateViewController : LoanerUpdateViewDelegate{
    func updateLoanerData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
        self.message = message
//        guard let id = self.customerId, let image = self.selectedImage else{
//            self.successMessage(userMessage: message)
//            return
//        }
//        self.presenter.uploadImage(customerID : id, image: image)
    }
    
    func setAddNewLoanerData(loanerData: AddDataMapper) {
//        guard let message = customerData.message, let customerID = customerData.customer?.id else{
//            return
//        }
//        self.message = message
        
//        guard let image = self.selectedImage else{
//            self.successMessage(userMessage: message)
//            return
//        }
//        self.presenter.uploadImage(customerID : customerID, image: image)
    }
    
    func onImageUpload(data : AddDataMapper){
        self.successMessage(userMessage: self.message ?? "")
    }
    
    func onFailed(data: String) {
        self.displayMessage(userMessage: data)
    }
    
    func showLoading() {
        self.showLoader()
        self.submitBtnControlWith(isEnabled: false)
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}
