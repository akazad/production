//
//  LoanerDetailsInfoCell.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class LoanerDetailsInfoCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var totalLoanLbl: UILabel!
    @IBOutlet weak var totalLoanTitleLbl: UILabel!{
        didSet{
            totalLoanTitleLbl.text = LanguageManager.TotalLoan
        }
    }
    @IBOutlet weak var currentLoanLbl: UILabel!
    @IBOutlet weak var currentLoanTitleLbl: UILabel!{
        didSet{
            currentLoanTitleLbl.text = LanguageManager.CurrentLoan
        }
    }
    @IBOutlet weak var interestAmount: UILabel!
    @IBOutlet weak var interestAmountTitleLbl: UILabel!{
        didSet{
            interestAmountTitleLbl.text = LanguageManager.InterestAmount
        }
    }
    @IBOutlet weak var loanLbl: UILabel!
    @IBOutlet weak var loanTitleLbl: UILabel!{
        didSet{
            loanTitleLbl.text = LanguageManager.Loan
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: LoanerDetailsInfoCell.self)
    }
    
}
