//
//  LoanerListPresenter.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol LoanerListViewDelegate : NSObjectProtocol {
    func setLoanerListData(data: LoanerDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class LoanerListPresenter : NSObject {
    
    private var service : LoanerService
    weak private var viewDelegate : LoanerListViewDelegate?
    
    init(service : LoanerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : LoanerListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getLoanerListDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getLoanerData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setLoanerListData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getLoanerSearchDataFromServer(page: Int, searchString : String){
        self.viewDelegate?.showLoading()
        self.service.getLoanerSearchData(page: page, searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setLoanerListData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
