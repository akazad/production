//
//  LoanerUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol LoanerUpdateViewDelegate : NSObjectProtocol {
    func setAddNewLoanerData(loanerData : AddDataMapper)
    func updateLoanerData(data: AddDataMapper)
    func onImageUpload(data : AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}
class LoanerUpdatePresenter: NSObject {
    
    private let service : LoanerService
    weak private var viewDelegate : LoanerUpdateViewDelegate?
    
    init(service : LoanerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : LoanerUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postLoanerUpdateDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postLoanUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateLoanerData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
        
    }
    
//    func updateCustomerData(customerData : [String: Any]){
//        self.viewDelegate?.showLoading()
//
//    }
//
//    func uploadImage(customerID : Int,image : UIImage?){
//        self.viewDelegate?.showLoading()
//
//    }
}


