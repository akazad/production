//
//  LoanerTransactionPresenter.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol LoanerTransactionHistoryViewDelegate : NSObjectProtocol {
    func setLoanerTransactionData(data: LoanTransactionHistoryDataMapper)
    func onLoanTransactionDelete(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class LoanerTransactionPresenter : NSObject {
    
    private var service : LoanerService
    weak private var viewDelegate : LoanerTransactionHistoryViewDelegate?
    
    init(service : LoanerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : LoanerTransactionHistoryViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getLoanerTransactionDataFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getLoanTransactionHistoryData(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setLoanerTransactionData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postOnLoanTransactionDeleteDataToServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostLoanTransactionDeleteData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onLoanTransactionDelete(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
//    func getLoanerSearchDataFromServer(page: Int, searchString : String){
//        self.viewDelegate?.showLoading()
//        self.service.getLoanerSearchData(page: page, searchString: searchString, success: { (data) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.setLoanerListData(data: data)
//        }, failure: { (message) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onFailed(data: message)
//        })
//    }
    
}
