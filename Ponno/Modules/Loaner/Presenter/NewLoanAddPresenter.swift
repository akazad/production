//
//  NewLoanAddPresenter.swift
//  Ponno
//
//  Created by a k azad on 25/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol NewLoanAddViewDelegate : NSObjectProtocol {
    func onNewLoanAddSuccess(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class NewLoanAddPresenter : NSObject {
    
    private var service : LoanerService
    weak private var viewDelegate : NewLoanAddViewDelegate?
    
    init(service : LoanerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : NewLoanAddViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postNewLoanAddDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postAddNewLoanData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onNewLoanAddSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    
}

