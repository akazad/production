//
//  LoanerDetailsPresenter.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol LoanerDetailsViewDelegate : NSObjectProtocol {
    func setLoanerDetailsData(data: LoanViewDataMapper)
    func onLoanerDelete(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class LoanerDetailsPresenter : NSObject {
    
    private var service : LoanerService
    weak private var viewDelegate : LoanerDetailsViewDelegate?
    
    init(service : LoanerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : LoanerDetailsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getLoanerDetailsDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getLoanViewData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setLoanerDetailsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postLoanerDeleteDataToServer(id: Int){
        self.viewDelegate?.hideLoading()
        self.service.PostLoanerDeleteData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onLoanerDelete(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
//    func getLoanerSearchDataFromServer(page: Int, searchString : String){
//        self.viewDelegate?.showLoading()
//
//    }
    
}
