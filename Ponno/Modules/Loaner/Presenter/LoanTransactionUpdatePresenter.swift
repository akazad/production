//
//  LoanTransactionUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 27/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol LoanerTransactionUpdateViewDelegate : NSObjectProtocol {
    func updateLoanTransactionData(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}
class LoanTransactionUpdatePresenter: NSObject {
    
    private let service : LoanerService
    weak private var viewDelegate : LoanerTransactionUpdateViewDelegate?
    
    init(service : LoanerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : LoanerTransactionUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postLoanerTransactionUpdateDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postLoanTransactionUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateLoanTransactionData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    
}
