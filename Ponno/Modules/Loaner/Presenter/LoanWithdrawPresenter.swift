//
//  LoanWithdrawPresenter.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol LoanWithdrawViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class LoanWithdrawPresenter: NSObject {
    
    private let service : LoanerService
    weak private var viewDelegate : LoanWithdrawViewDelegate?
    
    init(service : LoanerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : LoanWithdrawViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postLoanerWithdrawDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postLoanWithdrawData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}

