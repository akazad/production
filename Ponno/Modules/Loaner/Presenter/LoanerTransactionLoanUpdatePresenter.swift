//
//  LoanerTransactionLoanUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 27/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol LoanerTransactionLoanUpdateViewDelegate : NSObjectProtocol {
    func updateLoanTransactionLoanData(data: AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}
class LoanTransactionLoanUpdatePresenter: NSObject {
    
    private let service : LoanerService
    weak private var viewDelegate : LoanerTransactionLoanUpdateViewDelegate?
    
    init(service : LoanerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : LoanerTransactionLoanUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postLoanerTransactionLoanUpdateDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postLoanTransactionLoanUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateLoanTransactionLoanData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    
}

