//
//  LoanData.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class LoanData : Mappable {
    var id : Int = 0
    var loanerId : Int = 0
    var amount : String = ""
    var interest : String = ""
    var currentLoan : String = ""
    var deadline : String = ""
    var pharmacyId : Int = 0
    var createdAt : String = ""
    var updatedAt : String = ""
    var deletedAt : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        loanerId <- map["loaner_id"]
        amount <- map["amount"]
        interest <- map["interest"]
        currentLoan <- map["current_loan"]
        deadline <- map["deadline"]
        pharmacyId <- map["pharmacy_id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
    }
    
}

