//
//  LoanTransactionHistoryDataMapper.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class LoanTransactionHistoryDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var loan_withdraw_amount : Double?
    var loanTransactions : [LoanTransactions]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        loan_withdraw_amount <- map["loan_withdraw_amount"]
        loanTransactions <- map["loan_transactions"]
        pagination <- map["pagination"]
    }
    
}

