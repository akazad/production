//
//  LoanViewDataMapper.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class LoanViewDataMapper : Mappable {
    var success : Bool?
    var message : String?
    //var loaner : Loaner?
    var loanInfo : LoanInfo?
    //var loanInvestor : LoanInvestorDetails?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        //loaner <- map["loaner"]
        loanInfo <- map["loan"]
        //loanInvestor <- map["loan_investor"]
    }
    
}

