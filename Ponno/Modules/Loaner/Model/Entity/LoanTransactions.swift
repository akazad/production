//
//  LoanTransactions.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class LoanTransactions : Mappable {
    var id : Int = 0
    var loanId : Int = 0
    var amount : String = ""
    var type : Int = 0
    var description : String = ""
    var pharmacyId : Int = 0
    var createdAt : String = ""
    var deletedAt : String = ""
    var updateAt : String = ""
    //var loanData : LoanData?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        loanId <- map["loan_id"]
        amount <- map["amount"]
        type <- map["type"]
        description <- map["description"]
        pharmacyId <- map["pharmacy_id"]
        createdAt <- map["created_at"]
        deletedAt <- map["deleted_at"]
        //loanData <- map["loan_data"]
        updateAt <- map["updated_at"]
    }
    
}


