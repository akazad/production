//
//  LoanList.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class LoanList : Mappable {
    var id : Int = 0
    //var type : Int = 0
    var name : String = ""
    var mobile : String = ""
    var address : String = ""
    var image : String = ""
    var amount : String = ""
    var interest : String = ""
    var currentLoan : String = ""
    var deadline : String = ""
    //var deletedAt : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        //type <- map["type"]
        name <- map["name"]
        mobile <- map["mobile"]
        address <- map["address"]
        image <- map["image"]
        amount <- map["amount"]
        interest <- map["interest"]
        currentLoan <- map["current_loan"]
        deadline <- map["deadline"]
        //deletedAt <- map["deleted_at"]
    }
    
}
