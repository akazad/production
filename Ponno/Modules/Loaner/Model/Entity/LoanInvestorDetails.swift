//
//  LoanInvestorDetails.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class LoanInvestorDetails : Mappable {
    var id : Int = 0
    var type : Int = 0
    var name : String = ""
    var mobile : String = ""
    var address : String = ""
    var currentAmount : String = ""
    var pharmacyId : Int = 0
    var createdAt : String = ""
    var updatedAt : String = ""
    var deletedAt : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        type <- map["type"]
        name <- map["name"]
        mobile <- map["mobile"]
        address <- map["address"]
        currentAmount <- map["current_amount"]
        pharmacyId <- map["pharmacy_id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
    }
    
}
