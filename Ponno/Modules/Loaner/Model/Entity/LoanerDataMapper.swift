//
//  LoanerDataMapper.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class LoanerDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [CustomerSummary]?
    var loans : [LoanList]? 
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        loans <- map["loans"]
        pagination <- map["pagination"]
    }
    
}
