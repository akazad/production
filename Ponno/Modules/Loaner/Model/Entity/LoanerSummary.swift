//
//  LoanerSummary.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class LoanerSummary : Mappable {
    var totalLoaners : Int = 0
    var totalInvestors : Int  = 0
    var currentLoan : Int = 0
    var currentInvestment : Int = 0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        totalLoaners <- map["total_loaners"]
        totalInvestors <- map["total_investors"]
        currentLoan <- map["current_loan"]
        currentInvestment <- map["current_investment"]
    }
    
}

