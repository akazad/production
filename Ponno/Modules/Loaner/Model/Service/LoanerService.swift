//
//  LoanerService.swift
//  Ponno
//
//  Created by a k azad on 24/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper

public class LoanerService : NSObject{
    
    func getLoanerData(page: Int, success: @escaping (LoanerDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Loans + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<LoanerDataMapper>) in
                if let loanerResponse = response.result.value{
                    if let loaners = loanerResponse.loans, loaners.count > 0 {
                        success(loanerResponse)
                    }else if let failureResponse = loanerResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    //SearchData
    func getLoanerSearchData(page: Int, searchString: String, success: @escaping (LoanerDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let loanerURL = RestURL.sharedInstance.Loans + RestURL.sharedInstance.getSearchText(text: searchString) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = loanerURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<LoanerDataMapper>) in
                if let loanerResponse = response.result.value{
                    if let loaners = loanerResponse.loans, loaners.count > 0 {
                        success(loanerResponse)
                    }else if let failureResponse = loanerResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getLoanViewData(id: Int, success: @escaping (LoanViewDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.LoansView + RestURL.sharedInstance.getCommonId(id: id)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<LoanViewDataMapper>) in
                if let loanViewResponse = response.result.value{
                    if loanViewResponse.success == true{
                        success(loanViewResponse)
                    }else if let failureMessage = loanViewResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    //Mark: LoanDelete
    func PostLoanerDeleteData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.LoanDelete
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let loanDeleteResponse = response.result.value {
                if loanDeleteResponse.success == true{
                    success(loanDeleteResponse)
                }else if let failureResponse = loanDeleteResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    
    
    func getLoanTransactionHistoryData(page: Int, id: Int, success: @escaping (LoanTransactionHistoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.LoanTransactionsHistory + RestURL.sharedInstance.getCommonId(id: id)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<LoanTransactionHistoryDataMapper>) in
                if let loanTransactionHistoryResponse = response.result.value{
                    if let loaners = loanTransactionHistoryResponse.loanTransactions, loaners.count > 0 {
                        success(loanTransactionHistoryResponse)
                    }else if let failureResponse = loanTransactionHistoryResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    //Mark: PostRequest
    func postAddNewLoanData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.LoanStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let loanAddResponse = response.result.value {
                if loanAddResponse.success == true{
                    success(loanAddResponse)
                }else if let failureMessage = loanAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postLoanUpdateData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.LoanUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let loanUpdateResponse = response.result.value {
                if loanUpdateResponse.success == true{
                    success(loanUpdateResponse)
                }else if let failureMessage = loanUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postLoanTransactionUpdateData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.LoanTransactionUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let loanTransactionUpdateResponse = response.result.value {
                if loanTransactionUpdateResponse.success == true{
                    success(loanTransactionUpdateResponse)
                }else if let failureMessage = loanTransactionUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postLoanTransactionLoanUpdateData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.LoanTransactionLoanUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let loanTransactionLoanUpdateResponse = response.result.value {
                if loanTransactionLoanUpdateResponse.success == true{
                    success(loanTransactionLoanUpdateResponse)
                }else if let failureMessage = loanTransactionLoanUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func PostLoanTransactionDeleteData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.LoanTransactionDelete
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let loanTransactionDeleteResponse = response.result.value {
                if loanTransactionDeleteResponse.success == true{
                    success(loanTransactionDeleteResponse)
                }else if let failureResponse = loanTransactionDeleteResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postLoanWithdrawData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.LoanWithdraw
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let loanWithdrawResponse = response.result.value {
                if loanWithdrawResponse.success == true{
                    success(loanWithdrawResponse)
                }else if let failureMessage = loanWithdrawResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    //Mark: ImageUpload
    func postUserImage(LoandId : Int, image : UIImage? ,  succeed: @escaping((AddDataMapper)->Void), failure : @escaping((String) -> Void)){
        
        let imageUploadUrl = RestURL.sharedInstance.LoanImageUpload
        let params = ["loan_id" : "\(LoandId)"]
        
        let service = PostImageService()
        
        service.postImage(params: params, image: image, imageUrl: imageUploadUrl, succeed: { response in
            if let success = response.success, success == true{
                succeed(response)
            }
            else{
                failure(response.message ?? "")
            }
        }, failure: {message in
            failure(message)
        })
    }
    
    
    //    func getAccountsSearchData(startDate: String, endDate: String, success: @escaping (AccountsDataMapper) -> (), failure : @escaping (String) -> ()){
    //
    //        if !Connectivity.isConnectedToInternet {
    //            failure(CommonMessages.NoInternet)
    //        }
    //
    //        let accountsURL = RestURL.sharedInstance.AccountsTemp + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate)
    //
    //        guard let url = accountsURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
    //            return
    //        }
    //
    //        let header = RestURL.sharedInstance.getCommonHeader()
    //
    //        let request = Alamofire.request(url, headers: header)
    //            .responseObject { (response: DataResponse<AccountsDataMapper>) in
    //                if let accountsResponse = response.result.value{
    //                    if accountsResponse.success == true{
    //                        success(accountsResponse)
    //                    }else if let failureMessage = accountsResponse.message{
    //                        failure(failureMessage)
    //                    }
    //                }
    //                else {
    //                    failure(CommonMessages.ApiFailure)
    //                }
    //        }
    //        print(request)
    //    }
}


