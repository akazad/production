//
//  CashDrawerTransactions.swift
//  Ponno
//
//  Created by a k azad on 19/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class CashDrawerTransactions : Mappable {
    var id : Int = 0
    var type : Int = 0
    var amount : String = ""
    var description : String = ""
    //var currentStatus : String = ""
    //var pharmacyId : Int = 0
    var modelType : String = ""
    var modelId : Int = 0
    var createdAt : String = ""
    var linkName : String = ""
    var linkId : Int = 0
    //var updatedAt : String = ""
    //var deletedAt : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        type <- map["type"]
        amount <- map["amount"]
        description <- map ["description"]
        //currentStatus <- map["current_status"]
        //pharmacyId <- map["pharmacy_id"]
        modelType <- map["model_type"]
        modelId <- map["model_id"]
        createdAt <- map["created_at"]
        //updatedAt <- map["updated_at"]
        //deletedAt <- map["deleted_at"]
        linkName <- map["link_name"]
        linkId <- map["link_id"]
    }
    
}
