//
//  CashDrawerDataMapper.swift
//  Ponno
//
//  Created by a k azad on 19/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class CashDrawerDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var cashDrawer : CashDrawer?
    var transactions : [CashDrawerTransactions]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        cashDrawer <- map["cash_drawer"]
        transactions <- map["transactions"]
        pagination <- map["pagination"]
    }
    
}

