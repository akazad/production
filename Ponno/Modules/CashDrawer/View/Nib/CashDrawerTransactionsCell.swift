//
//  CashDrawerTransactionsCell.swift
//  Ponno
//
//  Created by a k azad on 19/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class CashDrawerTransactionsCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var moduleLbl: UILabel!
    @IBOutlet weak var modelBtn: UIButton!
    
    @IBOutlet weak var descriptionImageView: CircularImageView!
    @IBOutlet weak var invoiceLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var popUpMenuBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: CashDrawerTransactionsCell.self)
    }
    
}
