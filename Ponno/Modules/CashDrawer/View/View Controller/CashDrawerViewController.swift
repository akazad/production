//
//  CashDrawerViewController.swift
//  Ponno
//
//  Created by a k azad on 19/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class CashDrawerViewController: UIViewController {
    
    @IBOutlet weak var cashInitialInputTextField: UITextField!{
        didSet{
            cashInitialInputTextField.placeholder = LanguageManager.EnterCashAmountOfYourDrawer
        }
    }
    @IBOutlet weak var cashInitialInputHeight: NSLayoutConstraint!
    @IBOutlet weak var cashInitialSubmitBtn: UIButton!
    @IBOutlet weak var cashInitialSubmitBtnHeight: NSLayoutConstraint!
    
    @IBOutlet weak var cashLbl: UILabel!
    @IBOutlet weak var cashLblHeight: NSLayoutConstraint!
    @IBOutlet weak var cashInBtn: UIButton!{
        didSet{
            cashInBtn.setTitle("+ " + LanguageManager.CashIn, for: .normal)
        }
    }
    @IBOutlet weak var cashInBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var cashOutBtn: UIButton!{
        didSet{
            cashOutBtn.setTitle("- " + LanguageManager.CashOut, for: .normal)
        }
    }
    @IBOutlet weak var cashOutBtnHeight: NSLayoutConstraint!
    
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    
    @IBOutlet weak var noInfoMessageLbl: UILabel!{
        didSet{
            noInfoMessageLbl.text = LanguageManager.NoInformationFound
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = CashDrawerPresenter(service: CashDrawerService())
    
    var cashDrawer : CashDrawer?
    var cashDrawerTransactions : [CashDrawerTransactions] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var datePicker = UIDatePicker()
    var currentPage : Int = 1
    var lastPageNo : Int = 0
    var isLoading : Bool = false
    
    var modelType : String = ""
    var linkId : Int = 0
    
    enum ModelType : String {
        case sale = "Sale"
        case purchase = "Purchase"
        case deliverySystem = "DeliverySystem"
        case due = "Due"
        case payable = "Payable"
        case saleReturn = "SaleReturn"
        case purchaseReturn = "PurchaseReturn"
        case expense = "Expense"
        case purchasePreOrder = "PurchasePreOrder"
        case materialPurchase = "MaterialPurchase"
        case materialPurchaseReturn = "MaterialPurchaseReturn"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        //addSlideMenuButton()
        self.attributeSetUp()
        self.setUpInitialLanguage()
        self.showStartDatePicker()
        self.showEndDatePicker()
        self.configureTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.attachPresenter()
    }
    
    func attributeSetUp(){
        self.tableView.isHidden = true
        self.cashLbl.isHidden = true
        self.cashLblHeight.constant = 0.0
        self.cashInBtn.isHidden = true
        self.cashInBtnHeight.constant = 0.0
        self.cashOutBtn.isHidden = true
        self.cashOutBtnHeight.constant = 0.0
        self.noInfoMessageLbl.isHidden = true
        
        self.cashInitialInputTextField.isHidden = true
        self.cashInitialInputHeight.constant = 0.0
        self.cashInitialSubmitBtn.isHidden = true
        self.cashInitialSubmitBtnHeight.constant = 0.0
        
        self.cashInBtn.addTarget(self, action: #selector(onCashInBtnTapped), for: .touchUpInside)
        self.cashOutBtn.addTarget(self, action: #selector(onCashOutBtnTapped), for: .touchUpInside)
        
        self.searchBtn.addTarget(self, action: #selector(onSearchBtnTapped), for: .touchUpInside)
    }
    
    func initialSetup(){
        
        self.title = LanguageManager.CashDrawer
        
        if self.cashDrawer == nil{
            self.cashLbl.isHidden = true
            self.cashLblHeight.constant = 0.0
            self.cashInBtn.isHidden = true
            self.cashInBtnHeight.constant = 0.0
            self.cashOutBtn.isHidden = true
            self.cashOutBtnHeight.constant = 0.0
            
            self.cashInitialInputTextField.isHidden = false
            self.cashInitialInputHeight.constant = 40.0
            self.cashInitialSubmitBtn.isHidden = false
            self.cashInitialSubmitBtnHeight.constant = 40.0
            
            self.cashInitialSubmitBtn.addTarget(self, action: #selector(onInitialSubmitBtnPressed), for: .touchUpInside)
            
        }else{
            self.cashInitialInputTextField.isHidden = true
            self.cashInitialInputHeight.constant = 0.0
            self.cashInitialSubmitBtn.isHidden = true
            self.cashInitialSubmitBtnHeight.constant = 0.0
            
            self.cashLbl.isHidden = false
            self.cashLblHeight.constant = 40.0
            self.cashInBtn.isHidden = false
            self.cashInBtnHeight.constant = 30.0
            self.cashOutBtn.isHidden = false
            self.cashOutBtnHeight.constant = 30.0
            
            if let cash = self.cashDrawer  {
                let amount = cash.amount
                if amount.isEmpty{
                    self.cashLbl.text = "0.00" + Constants.currencySymbol
                }else{
                    self.cashLbl.text = LanguageManager.Cash + " : " +  amount + " " + Constants.currencySymbol
                }
            }
        }
        
    }
    
    @objc func onInitialSubmitBtnPressed(sender: UIButton){
        if isValidated(){
            if let cashInText = self.cashInitialInputTextField.text, let cashIn = Double(cashInText) {
                
                let param : [String : Any] = ["amount": cashIn]
                
                self.presenter.postAddNewCashInDrawer(param: param)
            }
            
        }
        
    }
    
    func isValidated()->Bool{
        if (self.cashInitialInputTextField.text?.isEmpty)! {
            self.showAlert(title: LanguageManager.CashIsRequired, message: "")
            return false
        }
        return true
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.cashInitialSubmitBtn.isEnabled = isEnabled
    }
    
    @objc func onCashInBtnTapped(sender: UIButton){
        let type = 1
        let title = LanguageManager.CashIn
        
        self.alertWithTextField(title: title, type: type)
    }
    
    @objc func onCashOutBtnTapped(sender: UIButton){
        let type = 0 
        let title = LanguageManager.CashOut
        
        self.alertWithTextField(title: title, type: type)
    }

}

//Mark : TableView Delegate and Data Source
extension CashDrawerViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(CashDrawerTransactionsCell.nib, forCellReuseIdentifier: CashDrawerTransactionsCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.tableView.estimatedRowHeight = 100.0
        self.automaticallyAdjustsScrollViewInsets = false
        self.noInfoMessageLbl.isHidden = true
        //self.noInfoMessageLbl.text = "No information found"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.cashDrawerTransactions.count > 0{
            self.noInfoMessageLbl.isHidden = true
            return self.cashDrawerTransactions.count
        }else{
            self.noInfoMessageLbl.isHidden = false
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CashDrawerTransactionsCell = tableView.dequeueReusableCell(withIdentifier: CashDrawerTransactionsCell.identifier, for: indexPath) as! CashDrawerTransactionsCell
        cell.selectionStyle = .none
        
        if self.cashDrawerTransactions.count > 0 {
            let item = self.cashDrawerTransactions[indexPath.row]
            
            cell.dateLbl.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.dd_MMM_yy_c_HH_mm_a.rawValue)
            cell.moduleLbl.text = item.modelType
            cell.modelBtn.setTitle(item.linkName, for: .normal)
            
            cell.modelBtn.tag = item.linkId
            cell.modelBtn.addTarget(self, action: #selector(onLinkBtnPressed), for: .touchUpInside)
            
            let type = item.type

            if type == 0 {
                cell.colorView.backgroundColor = UIColor.red
            }else{
                cell.colorView.backgroundColor = UIColor(red: 59, green: 149, blue: 31)
            }
            cell.amountLbl.textColor = UIColor.white
            cell.amountLbl.text = item.amount
            
            let modelType = item.modelType
            if modelType == "CashDrawerTransaction"{
                cell.popUpMenuBtn.isHidden = false
                cell.popUpMenuBtn.tag = item.id
                cell.popUpMenuBtn.addTarget(self, action: #selector(onPopUpMenuPressed), for: .touchUpInside)
            }else{
                cell.popUpMenuBtn.isHidden = true
            }
            
            let description = item.description
            if description.isEmpty{
                cell.descriptionImageView.isHidden = true
            }else{
                cell.descriptionImageView.addTapGestureRecognizer{
                    self.showAlert(title: LanguageManager.Description, message: description)
                }
            }
            
            if isLoading == false && indexPath.row == self.cashDrawerTransactions
                .count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                self.presenter.getCashDrawerDataFromServer(page: self.currentPage)
            }
            
        }
        return cell
    }
    
    @objc func onDescriptionImageViewTapped(sender: UIImageView){
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    @objc func onLinkBtnPressed(sender: UIButton){
        self.linkId = sender.tag
        if self.cashDrawerTransactions.count > 0{
            for transaction in self.cashDrawerTransactions{
                if self.linkId == transaction.linkId{
                    self.modelType = transaction.modelType
                    self.navigate(to: self.modelType, id: self.linkId)
                }
            }
        }
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if self.cashDrawerTransactions.count > 0{
//            let item = self.cashDrawerTransactions[indexPath.row]
//            
//            self.modelType = item.modelType
//            self.linkId = item.linkId
//            
//            self.navigate(to: self.modelType, id: self.linkId)
//        }
//    }
    
    func navigate(to : String, id: Int){
        switch to {
            case ModelType.sale.rawValue:
                self.navigateToSaleDetailsVC(iD : id)
            case ModelType.purchase.rawValue:
                self.navigateToPurchaseDetailsVC(selectedId : id)
            case ModelType.deliverySystem.rawValue:
                self.navigateToDeliverySystemDetailsVC(id: id)
            case ModelType.due.rawValue:
                self.navigateToCustomerDBaseViewController(id: id)
            case ModelType.saleReturn.rawValue:
                self.navigateToSaleDetailsVC(iD : id)
            case ModelType.payable.rawValue:
                self.navigateToVendorDBaseViewController(vendorId : id)
            case ModelType.purchaseReturn.rawValue:
                self.navigateToPurchaseReturnDetailsVC(selectedId : id)
            case ModelType.expense.rawValue:
                self.navigateToExpensePerCategoryVC(id: id)
            case ModelType.materialPurchase.rawValue:
                self.navigateToMaterialPurchaseDetailsVC(selectedId : id)
            case ModelType.materialPurchaseReturn.rawValue:
                self.navigateToMaterialPurchaseReturnDetailsVC(selectedId : id)
            case ModelType.purchasePreOrder.rawValue:
                self.navigateToPurchaseDetailsVC(selectedId : id)
            default:
            break
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopUpMenuPressed(sender: UIButton){
        
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        let id = sender.tag
        if self.cashDrawerTransactions.count > 0 {
            for item in self.cashDrawerTransactions{
                if id == item.id{
                    let modelType = item.modelType
                    if modelType == "CashDrawerTransaction"{
                        let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                            self.navigateToCashTransactionUpdateVC(item: item)
                        }
                        
                        let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                            self.transactionDeleteAlert(id: id)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(updateAction)
                        myActionSheet.addAction(deleteAction)
                        myActionSheet.addAction(cancelAction)
                    }
                }
            }
            
            popOverForIpad(action: myActionSheet)
            
            self.present(myActionSheet, animated: true, completion: nil)
        }
        
    }
    
    
    func navigateToCashTransactionUpdateVC(item : CashDrawerTransactions){
        let storyBoard:UIStoryboard = UIStoryboard(name: "CashDrawer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CashTransactionUpdateViewController") as! CashTransactionUpdateViewController
        
        viewController.cashDrawerTransactions = item
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//DatePicker
extension CashDrawerViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isSearchValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSearchBtnTapped(sender: UIButton){
        if isSearchValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            self.cashDrawerTransactions = []
            self.presenter.getCashDrawerSearchData(startDate: startDate, endDate: endDate)
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

//Mark: Api Delegate
extension CashDrawerViewController : CashDrawerViewDelegate{
    
    func setCashDrawerData(data: CashDrawerDataMapper) {
        guard let cash = data.cashDrawer else {
            self.initialSetup()
            return
        }
        self.cashDrawer = cash
        self.initialSetup()
        
        guard let transactions = data.transactions, transactions.count > 0 else {
//            self.cashDrawerTransactions = []
//            self.tableView.isHidden = true
            self.datePickerTextField.text = ""
            self.toDatePickerTextField.text = ""
            self.dateSearchFailedMessage(userMessage: LanguageManager.NoInformationFound)
            return
        }
        self.isLoading = false
        self.cashDrawerTransactions += transactions
        self.noInfoMessageLbl.isHidden = true
        self.tableView.isHidden = false
        
        guard let pagination = data.pagination else{
            return
        }
        
        self.lastPageNo = pagination.lastPageNo
        
    }
    
    func onCashAdd(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onCashInOut(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onTransactionDelete(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onCashAddFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.noInfoMessageLbl.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.noInfoMessageLbl.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
       self.presenter.attachView(viewDelegate: self)
       self.currentPage = 1
       self.isLoading = true
       self.cashDrawerTransactions = []
       self.presenter.getCashDrawerDataFromServer(page: self.currentPage)
    }
}

extension CashDrawerViewController{
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.cashDrawerTransactions = []
            self.presenter.getCashDrawerDataFromServer(page: self.currentPage)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func transactionDeleteAlert(id: Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: LanguageManager.AreYouSure, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            let param : [String : Any] = ["id": id]
            
            self.presenter.postCashTransactionDeleteDataToServer(param: param)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func dateSearchFailedMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title:  userMessage, message: "", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.cashDrawerTransactions = []
            self.presenter.getCashDrawerDataFromServer(page: self.currentPage)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alertWithTextField(title: String, type: Int){
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField0 in
            textField0.placeholder = LanguageManager.Cash
            textField0.textAlignment = .center
            textField0.keyboardType = .decimalPad
        }
        
       
        
        alertController.addTextField { textField1 in
            textField1.placeholder = LanguageManager.Description
            textField1.textAlignment = .left
            textField1.keyboardType = .default
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.CashIsRequired, message: "")
                return
            }
            
            let textField1 = alertController.textFields![1]
            let description = textField1.text ?? ""
            
            let param : [String : Any] = ["amount": textField.text!, "type": type, "description": description]
            
            self.presenter.postCashInOutDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}

extension CashDrawerViewController {
    
    fileprivate func navigateToVendorDBaseViewController(vendorId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorDBaseViewController") as! VendorDBaseViewController
        viewController.vendorId = vendorId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchaseDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseDetailsViewController") as! PurchaseDetailsViewController
        viewController.selectedId = selectedId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchaseReturnDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseReturnDetailsViewController") as! PurchaseReturnDetailsViewController
        viewController.selectedId = selectedId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        viewController.iD = iD
        viewController.state = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToDeliverySystemDetailsVC(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DBaseViewController") as! DBaseViewController
        viewController.deliverySystem = id
        //viewController.deliverySystemName = data.name
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToCustomerDBaseViewController(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerDBaseViewController") as! CustomerDBaseViewController
        viewController.customerId = id
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    
    func navigateToExpensePerCategoryVC(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ExpensePerCategoryViewController") as! ExpensePerCategoryViewController
        viewController.selectedType = id
        //viewController.expenseName = expenseName
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToMaterialPurchaseReturnDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseReturnDetailsViewController") as! PurchaseReturnDetailsViewController
        viewController.selectedId = selectedId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToMaterialPurchaseDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseDetailsViewController") as! PurchaseDetailsViewController
        viewController.selectedId = selectedId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
