//
//  CashTransactionUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 20/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class CashTransactionUpdateViewController: UIViewController {

    @IBOutlet weak var previousCashLbl: UILabel!
    @IBOutlet weak var previousCashTextField: UITextField!
    @IBOutlet weak var newCashLbl: UILabel!
    @IBOutlet weak var newCashTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = CashTransactionsUpdatePresenter(service: CashDrawerService())
    
    var cashDrawerTransactions : CashDrawerTransactions?
    
    var transactionId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.setUpInitialLanguage()
        self.attachPresenter()
        
    }
    
    func initialSetup(){
        
        self.title = LanguageManager.TransactionUpdate
        
        guard let transaction = self.cashDrawerTransactions else{
            return
        }
        self.previousCashLbl.text = LanguageManager.PreviousCashAmount
        self.previousCashTextField.text = transaction.amount
        self.newCashLbl.text = LanguageManager.NewCashAmount
        self.newCashTextField.placeholder = LanguageManager.EnterCashAmount
        
        self.transactionId = transaction.id
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            guard let amount = self.newCashTextField.text, let id = self.transactionId else{
                return
            }
            
            let param : [String : Any] = ["id" : id, "amount" : amount]
            
            self.presenter.postCashTransactionUpdateDataToServer(param: param)
        }
    }
    
    func isValidated()->Bool{
        if (self.newCashTextField.text?.isEmpty)! {
            self.showAlert(title: LanguageManager.CashIsRequired, message: "")
            return false
        }
        return true
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    

}

//Mark: Api Delegate
extension CashTransactionUpdateViewController : CashTransactionsUpdateViewDelegate{
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

extension CashTransactionUpdateViewController {
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigateToCashDrawerVC()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToCashDrawerVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "CashDrawer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CashDrawerViewController") as! CashDrawerViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
