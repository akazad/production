//
//  CashTransactionsUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 20/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol CashTransactionsUpdateViewDelegate : NSObjectProtocol {
    func onSuccess(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class CashTransactionsUpdatePresenter : NSObject {
    
    private var service : CashDrawerService
    weak private var viewDelegate : CashTransactionsUpdateViewDelegate?
    
    init(service : CashDrawerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : CashTransactionsUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postCashTransactionUpdateDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postCashTransactionUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postCustomerWalletTransactionUpdateDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        let service = CustomerService()
        service.postCustomerWalletUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
