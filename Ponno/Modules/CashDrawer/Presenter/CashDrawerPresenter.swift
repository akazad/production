//
//  CashDrawerPresenter.swift
//  Ponno
//
//  Created by a k azad on 19/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol CashDrawerViewDelegate : NSObjectProtocol {
    func setCashDrawerData(data: CashDrawerDataMapper)
    func onCashAdd(data: AddDataMapper)
    func onCashInOut(data: AddDataMapper)
    func onTransactionDelete(data: AddDataMapper)
    func onCashAddFailed(data: String)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class CashDrawerPresenter : NSObject {
    
    private var service : CashDrawerService
    weak private var viewDelegate : CashDrawerViewDelegate?
    
    init(service : CashDrawerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : CashDrawerViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getCashDrawerDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getCashDrawerData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCashDrawerData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postAddNewCashInDrawer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postAddNewCash(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCashAdd(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCashAddFailed(data: message)
        })
    }
    
    func postCashInOutDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postCashInOut(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCashInOut(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postCashTransactionDeleteDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postCashTransactionDeleteData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onTransactionDelete(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getCashDrawerSearchData(startDate: String, endDate: String){
        self.viewDelegate?.showLoading()
        self.service.getCashDrawerSearchData(startDate: startDate, endDate: endDate, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCashDrawerData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}

