//
//  PropertyOrEquipmentUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 4/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PropertyOrEquipmentUpdateViewDelegate : NSObjectProtocol {
    func updatePropertyOrEquipmentData(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class PropertyOrEquipmentUpdatePresenter : NSObject {
    
    private var service : AssestsService
    weak private var viewDelegate : PropertyOrEquipmentUpdateViewDelegate?
    
    init(service : AssestsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PropertyOrEquipmentUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postPropertyOrEquipmentUpdateDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postAssetPropertyUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updatePropertyOrEquipmentData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    
    
}
