//
//  GoodWillListPresenter.swift
//  Ponno
//
//  Created by a k azad on 29/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol GoodWillListViewDelegate : NSObjectProtocol {
    func setGoodWillData(data: GoodWillDataMapper)
    func updateGoodwillData(data: AddDataMapper)
    func onDelete(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class GoodWillListPresenter : NSObject {
    
    private var service : AssestsService
    weak private var viewDelegate : GoodWillListViewDelegate?
    
    init(service : AssestsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : GoodWillListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getGoodWillDataFormServer(page: Int){
        self.viewDelegate?.hideLoading()
        self.service.getGoodWillListData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setGoodWillData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postGoodwillUpdateDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postGoodWillUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateGoodwillData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postGoodWillDeleteDataToServer(id: Int){
        self.viewDelegate?.hideLoading()
        self.service.PostGoodWillDeleteData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDelete(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}

