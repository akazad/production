//
//  PropertyNequipmentsAddPresenter.swift
//  Ponno
//
//  Created by a k azad on 29/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PropertyNequipmentsAddViewDelegate : NSObjectProtocol {
    func setAssetsCategoryData(data: AssetsCategoryDataMapper)
    func onSuccess(data: AddDataMapper)
    func onCategoryFailed(data: String)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class PropertyNequipmentsAddPresenter : NSObject {
    
    private var service : AssestsService
    weak private var viewDelegate : PropertyNequipmentsAddViewDelegate?
    
    init(service : AssestsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PropertyNequipmentsAddViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getAssetsCategoryDataFormServer(type: Int){
        self.viewDelegate?.hideLoading()
        self.service.getAssetsCategoryData(type: type, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setAssetsCategoryData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCategoryFailed(data: message)
        })
    }
    
    func postPropertyOrEquipmentsAddDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postAddNewAssetPropertyData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
