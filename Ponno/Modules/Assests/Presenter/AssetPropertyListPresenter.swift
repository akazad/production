//
//  AssetPropertyListPresenter.swift
//  Ponno
//
//  Created by a k azad on 28/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol AssetPropertyListViewDelegate : NSObjectProtocol {
    func setAssetPropertyListData(data: AssetListDataMapper)
    func onGoodwillAmountAdd(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class AssetPropertyListPresenter : NSObject {
    
    private var service : AssestsService
    weak private var viewDelegate : AssetPropertyListViewDelegate?
    
    init(service : AssestsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AssetPropertyListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getAssetPropertyListDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getAssetPropertyListData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setAssetPropertyListData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postGoodwillAddDataToServer(param : [String : Any]){
        self.viewDelegate?.hideLoading()
        self.service.postAddNewGoodWillData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onGoodwillAmountAdd(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
