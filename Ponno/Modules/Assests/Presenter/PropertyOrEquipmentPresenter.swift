//
//  PropertyOrEquipmentPresenter.swift
//  Ponno
//
//  Created by a k azad on 29/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PropertyOrEquipmentListViewDelegate : NSObjectProtocol {
    func setPropertyOrEquipmentData(data: PropertyOrEquipmentDataMapper)
    func onDelete(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class PropertyOrEquipmentPresenter : NSObject {
    
    private var service : AssestsService
    weak private var viewDelegate : PropertyOrEquipmentListViewDelegate?
    
    init(service : AssestsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PropertyOrEquipmentListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getPropertyOrEquipmentDataFormServer(type: Int, page: Int){
        self.viewDelegate?.showLoading()
        self.service.getPropertyOrEquipmentListData(type: type, page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPropertyOrEquipmentData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postAssetDeleteDateToServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostAssetPropertyDeleteData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDelete(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}

