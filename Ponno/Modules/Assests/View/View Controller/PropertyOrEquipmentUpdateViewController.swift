//
//  PropertyOrEquipmentUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 4/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PropertyOrEquipmentUpdateViewController: UIViewController {

    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextField: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = PropertyOrEquipmentUpdatePresenter(service: AssestsService())
    
    var propertyOrEquipment : PropertyOrEquipments?
    var propertyOrEquipmentId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.toolbarSetup()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    @IBAction func crashBtn(_ sender: Any) {
        
    }
    func initialSetup(){
        
        self.title = LanguageManager.Update
        self.amountLbl.text = LanguageManager.Amount
        self.descriptionLbl.text = LanguageManager.Description
        self.amountTextField.underlined()
        
        if let propertyOrEquipment = self.propertyOrEquipment{
            self.amountTextField.text = propertyOrEquipment.cost
            self.descriptionTextField.text = propertyOrEquipment.description
            self.propertyOrEquipmentId = propertyOrEquipment.id
        }
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped(sender:)), for: .touchUpInside)
    }
    
    func toolbarSetup(){
        let amounttoolbar = UIToolbar()
        amounttoolbar.barStyle = UIBarStyle.default
        amounttoolbar.isTranslucent = true
        amounttoolbar.tintColor = UIColor.black
        amounttoolbar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        let amountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnAmount(sender:)))
        
        amounttoolbar.setItems([cancelButton, spaceButton, amountDoneButton], animated: false)
        amounttoolbar.isUserInteractionEnabled = true
        
        amountTextField.inputAccessoryView = amounttoolbar
        
    }
    
    @objc func onPressingDoneOnAmount(sender: UIBarButtonItem){
        self.descriptionTextField.becomeFirstResponder()
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            let description = self.descriptionTextField.text ?? ""
            if let cost = self.amountTextField.text, let id = self.propertyOrEquipmentId{
                let param : [String : Any] = ["id": id, "cost": cost, "description": description]
                self.presenter.postPropertyOrEquipmentUpdateDataToServer(param: param)
            }
            
        }
    }
    
    func isValidated() -> Bool {
        if (self.amountTextField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.AmountIsRequired, message: "")
            return false
        }
        return true
    }
    
    func submitBtnControllWith(isEnabled: Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
}

//Mark: Api Delegate
extension PropertyOrEquipmentUpdateViewController : PropertyOrEquipmentUpdateViewDelegate{
    func updatePropertyOrEquipmentData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.submitBtnControllWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControllWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
    
}

extension PropertyOrEquipmentUpdateViewController {
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
