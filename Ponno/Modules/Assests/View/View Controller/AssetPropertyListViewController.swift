//
//  AssetPropertyListViewController.swift
//  Ponno
//
//  Created by a k azad on 28/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class AssetPropertyListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = AssetPropertyListPresenter(service: AssestsService())
    
    var assetPropertyList : [AssetList]?{
        didSet{
            self.refreshTableView()
        }
    }
    
    enum AssetProperty : String {
        case saleAbleStock = "inventory"
        case properties = "properties"
        case equipments = "equipments"
        case goodWill = "goodwil"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.configureTableView()
        //self.attachPresenter()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.title = LanguageManager.Assets
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
}

//Mark: TableView Delegate and DataSource
extension AssetPropertyListViewController : UITableViewDelegate, UITableViewDataSource {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(AssetsItemCell.nib, forCellReuseIdentifier: AssetsItemCell.identifier)
        self.tableView.estimatedRowHeight = 70.0
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = true
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let item = self.assetPropertyList {
            return item.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : AssetsItemCell = tableView.dequeueReusableCell(withIdentifier: AssetsItemCell.identifier, for: indexPath) as! AssetsItemCell
        
        if let item = self.assetPropertyList {
            let data = item[indexPath.row]
            if data.name == "inventory" {
                cell.assetPropertyNameLbl.text = LanguageManager.SaleableStockAmount
            }else if data.name == "properties"{
                cell.assetPropertyNameLbl.text = LanguageManager.Properties
            }else if data.name == "equipments"{
                cell.assetPropertyNameLbl.text = LanguageManager.Equipments
            }else if data.name == "goodwill"{
                cell.assetPropertyNameLbl.text = LanguageManager.GoodWill
            }
            
            cell.assetPropertyAmountLbl.text = "\(data.amount) " + Constants.currencySymbol
            
            cell.assetPropertyPopUpBtn.tag = indexPath.row
            cell.assetPropertyPopUpBtn.addTarget(self, action: #selector(onAssetPropertyPopUpBtnTapped), for: .touchUpInside)
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let item = self.assetPropertyList {
            let data = item[indexPath.row]
            if data.name == "properties"{
                self.navigateToPropertytVC()
            }else if data.name == "equipments"{
                self.navigateToEquipmentVC()
            }else if data.name == "goodwill"{
                self.navigateToGoodWillDataVC()
            }
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onAssetPropertyPopUpBtnTapped(sender : UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if let assetItem = self.assetPropertyList, assetItem.count > 0 {
            
            for item in assetItem{
                let position = sender.tag
                if position == 0 && item.name == "inventory" {
                    let inventoryAction = UIAlertAction(title:LanguageManager.Inventory, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToProductListVC()
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(inventoryAction)
                    myActionSheet.addAction(cancelAction)
                }else if position == 1 && item.name == "properties" {
                    let propertyAddAction = UIAlertAction(title: LanguageManager.AddNewProperty, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToPropertyAddVC()
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in

                        self.view.endEditing(true)
                    }

                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(propertyAddAction)
                    myActionSheet.addAction(cancelAction)
                }else if position == 2 && item.name == "equipments" {
                    let equipmentAddAction = UIAlertAction(title: LanguageManager.AddNewEquipments, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToEquipmentAddVC()
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(equipmentAddAction)
                    myActionSheet.addAction(cancelAction)
                }else if position == 3 && item.name == "goodwill" {
                    let goodWillEditAction = UIAlertAction(title:LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.alertWithTextField()
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(goodWillEditAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    
}

//Mark: Api Delegate
extension AssetPropertyListViewController : AssetPropertyListViewDelegate{
    
    func setAssetPropertyListData(data: AssetListDataMapper) {
        guard let data = data.assetList, data.count > 0 else{
            return
        }
        self.assetPropertyList = data
    }
    
    func onGoodwillAmountAdd(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.goodwillAddAlert(title: message, message: "")
    }
    
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.assetPropertyList = []
        self.presenter.getAssetPropertyListDataFromServer()
    }
    
}

extension AssetPropertyListViewController {
    func navigateToProductListVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        viewController.showNavDrawer = false
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPropertyAddVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Assets", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PropertyNequipmentsAddViewController") as! PropertyNequipmentsAddViewController
        
        viewController.type = 0
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToEquipmentAddVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Assets", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PropertyNequipmentsAddViewController") as! PropertyNequipmentsAddViewController
        
        viewController.type = 1
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToPropertytVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Assets", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PropertyOrEquipmentListViewController") as! PropertyOrEquipmentListViewController
        
        viewController.type = 0
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToEquipmentVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Assets", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PropertyOrEquipmentListViewController") as! PropertyOrEquipmentListViewController
        
        viewController.type = 1
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func navigateToGoodWillDataVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Assets", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "GoodWillListViewController") as! GoodWillListViewController
        
        //viewController.type = 1
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

extension AssetPropertyListViewController {
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.GoodWill, message: "", preferredStyle: .alert)
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.GoodWillAmount
            textField.textAlignment = .center
            textField.keyboardType = .decimalPad
            textField.borderStyle = .none
            textField.underlined()
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.GoodwillAmountIsRequired, message: "")
                return
            }
            
            let param : [String : String] = ["amount": textField.text!]
            
            self.presenter.postGoodwillAddDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func goodwillAddAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.presenter.getAssetPropertyListDataFromServer()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
