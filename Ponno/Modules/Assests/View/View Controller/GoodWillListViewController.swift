//
//  GoodWillListViewController.swift
//  Ponno
//
//  Created by a k azad on 29/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class GoodWillListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var presenter = GoodWillListPresenter(service: AssestsService())
    
    var goodWill : [GoodWillData] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var goodWillId: Int?
    var currentPage: Int = 1
    var lastPageNo: Int = 0
    var isLoading: Bool = false
    
    var dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.configureTableView()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.title = LanguageManager.GoodWill
    }
    
}

//Mark: TableView Delegate and Data Source
extension GoodWillListViewController : UITableViewDelegate, UITableViewDataSource {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(GoodWillListCell.nib, forCellReuseIdentifier: GoodWillListCell.identifier)
        self.tableView.estimatedRowHeight = 60.0
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = true
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorStyle = .none
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.goodWill.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : GoodWillListCell = tableView.dequeueReusableCell(withIdentifier: GoodWillListCell.identifier, for: indexPath) as! GoodWillListCell
        
        let item = self.goodWill[indexPath.row]
        
        cell.startDateLbl.text = item.startDate.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
        if !(item.endDate.isEmpty){
            cell.endDateLbl.text = item.endDate.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
        }else{
            cell.endDateLbl.text = ""
        }
        
        cell.amountLbl.text = item.amount
        
        cell.popUpBtn.tag = item.id
        cell.popUpBtn.addTarget(self, action: #selector(onPopBtnTapped), for: .touchUpInside)
        
        if isLoading == false && indexPath.row == self.goodWill.count - 1 && self.currentPage < self.lastPageNo{
            self.isLoading = true
            self.currentPage += 1
            self.presenter.getGoodWillDataFormServer(page: self.currentPage)
            
        }
        
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.goodWill.count > 0 {
            
            for item in self.goodWill{
                self.goodWillId = sender.tag
                if goodWillId == item.id {
                    let amount = item.amount
                    let updateAction = UIAlertAction(title:LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.alertWithTextField(amount : amount)
                    }
                    
                    let deleteAction = UIAlertAction(title:LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        guard let id = self.goodWillId else{
                            return
                        }
                        self.confirmationMessage(userMessage: "", deleteId: id)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(updateAction)
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension GoodWillListViewController : GoodWillListViewDelegate {
    func setGoodWillData(data: GoodWillDataMapper) {
        if let goodwill = data.goodWillData, goodwill.count > 0 {
            self.goodWill = goodwill
        }
        
        if let pagination = data.pagination{
            self.lastPageNo = pagination.lastPageNo
        }
    }
    
    func updateGoodwillData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.goodwillUpdateAlert(title: LanguageManager.Successful, message: message)
    }
    
    func onDelete(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.goodwillUpdateAlert(title: LanguageManager.Successful, message: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.currentPage = 1
        self.presenter.getGoodWillDataFormServer(page: self.currentPage)
    }
}

extension GoodWillListViewController {
    func alertWithTextField(amount : String){
        let alertController = UIAlertController(title: LanguageManager.GoodWill, message: "", preferredStyle: .alert)
        
        alertController.addTextField{ textField in
            textField.text = amount
            textField.textAlignment = .center
            textField.keyboardType = .decimalPad
            textField.borderStyle = .none
            textField.underlined()
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0, let id = self.goodWillId else{
                self.showAlert(title: LanguageManager.GoodwillAmountIsRequired, message: "")
                return
            }
            
            let param : [String : Any] = ["id": id,"amount": textField.text!]
            
            self.presenter.postGoodwillUpdateDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func goodwillUpdateAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.presenter.getGoodWillDataFormServer(page: self.currentPage)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.AreYouSure, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: LanguageManager.Yes, style: .default){
            (action:UIAlertAction!) in
            
            self.presenter.postGoodWillDeleteDataToServer(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: LanguageManager.No, style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
//    func currentDateFormatter(format: String, date: String) -> String{
//        dateFormatter.dateFormat = format
//        let dateString = dateFormatter.date(from: date)
//        return currentDate
//    }
    
}
