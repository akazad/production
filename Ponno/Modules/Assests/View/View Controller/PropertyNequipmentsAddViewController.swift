//
//  PropertyNequipmentsAddViewController.swift
//  Ponno
//
//  Created by a k azad on 29/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PropertyNequipmentsAddViewController: UIViewController {

    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var categoryTextField: UITextField!{
        didSet{
            categoryTextField.placeholder = LanguageManager.SelectOne
        }
    }
    @IBOutlet weak var categoryNameLbl: UILabel!
    @IBOutlet weak var categoryNameLblHeight: NSLayoutConstraint!
    @IBOutlet weak var categoryNameTextField: UITextField!{
        didSet{
            categoryNameTextField.placeholder = LanguageManager.CategoryName
        }
    }
    @IBOutlet weak var categoryNameTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var amountTextField: UITextField!{
        didSet{
            amountTextField.placeholder = LanguageManager.Amount
        }
    }
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var newCategoryAddBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = PropertyNequipmentsAddPresenter(service: AssestsService())
    
    var assetCategory : [AssetsCategory]?{
        didSet{
            self.categoryPicker.reloadAllComponents()
        }
    }
    
    var categoryPicker = UIPickerView()
    var newPropertyOrEquipmentSetup: Bool = false
    var categoryId : Int?
    
    var type : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.toolbarSetup()
        self.setUpPickerView()
        self.setUpInitialLanguage()
        self.attachPresenter()
        
    }
    
    
    func initialSetup(){
        self.categoryLbl.text = LanguageManager.Category
        self.amountLbl.text = LanguageManager.Amount
        self.descriptionLbl.text = LanguageManager.Description
        self.categoryTextField.underlined()
        self.amountTextField.underlined()
        self.categoryNameTextField.isHidden = true
        self.categoryNameLbl.isHidden = true
        
        self.newCategoryAddBtn.addTarget(self, action: #selector(onNewCategoryAddBtnTapped), for: .touchUpInside)
        
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
        
    }
    
    @objc func onNewCategoryAddBtnTapped(sender: UIButton){
        self.newPropertyOrEquipmentSetup = !self.newPropertyOrEquipmentSetup
        
        if self.newPropertyOrEquipmentSetup == false{
            self.categoryNameTextField.isHidden = true
            self.categoryNameLbl.isHidden = true
            
        }else{
            self.categoryNameTextField.isHidden = false
            self.categoryNameTextField.underlined()
            self.categoryNameLbl.isHidden = false
            self.categoryNameLbl.text = LanguageManager.CategoryName
            self.categoryTextField.text = LanguageManager.NewCategoryAdd
            self.categoryNameTextField.becomeFirstResponder()
        }
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            let description = self.descriptionTextView.text ?? ""
            if let cost = self.amountTextField.text, let type = self.type{
                if self.newPropertyOrEquipmentSetup == true {
                    if let newCategoryName = self.categoryNameTextField.text{
                        let param :[String : Any] = ["category": "add_new", "new_category_name": newCategoryName, "cost": cost, "type": type, "description": description]
                        self.presenter.postPropertyOrEquipmentsAddDataToServer(param: param)
                    }
                }else{
                    if let id = self.categoryId{
                        let param : [String : Any] = ["category": id, "cost": cost, "type": type, "description": description]
                        self.presenter.postPropertyOrEquipmentsAddDataToServer(param: param)
                    }
                    
                }
            }
            
        }
    }
    
    func isValidated() -> Bool {
        if (self.categoryTextField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.CategoryIsRequired, message: "")
            return false
        }else if (self.amountTextField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.AmountIsRequired, message: "")
            return false
        }
        if self.newPropertyOrEquipmentSetup == true {
            if (self.categoryNameTextField.text?.isEmpty)!{
                self.showAlert(title: LanguageManager.NewCategoryNameIsRequired, message: "")
                return false
            }
        }
        
        return true
    }
    
    func submitBtnControllWith(isEnabled: Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func toolbarSetup(){
        let amounttoolbar = UIToolbar()
        amounttoolbar.barStyle = UIBarStyle.default
        amounttoolbar.isTranslucent = true
        amounttoolbar.tintColor = UIColor.black
        amounttoolbar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        let amountDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnAmount(sender:)))
        
        amounttoolbar.setItems([cancelButton, spaceButton, amountDoneButton], animated: false)
        amounttoolbar.isUserInteractionEnabled = true
        
        amountTextField.inputAccessoryView = amounttoolbar
        
    }
    
    @objc func onPressingDoneOnAmount(sender: UIBarButtonItem){
        self.descriptionTextView.becomeFirstResponder()
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    //Mark: PickerView
    func setUpPickerView(){
        categoryPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnCategory(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        self.categoryTextField.inputView = categoryPicker
        self.categoryTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func onPressingDoneOnCategory(sender : UIBarButtonItem){
        self.amountTextField.becomeFirstResponder()
    }
    
}

//Mark: PickerViewDelegate
extension PropertyNequipmentsAddViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if let categories = self.assetCategory, categories.count > 0 {
            return categories.count
        }else{
            return 0
        }
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let categories = self.assetCategory, categories.count > 0 {
            let item = categories[row]
            self.categoryId = item.id
            self.categoryTextField.text = item.name
            return item.name
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let categories = self.assetCategory, categories.count > 0 {
            let item = categories[row]
            self.categoryId = item.id
            self.categoryTextField.text = item.name
            //self.amountTextField.becomeFirstResponder()
        }
    }
    
}

//Mark: Api Delegate
extension PropertyNequipmentsAddViewController : PropertyNequipmentsAddViewDelegate{
    func setAssetsCategoryData(data: AssetsCategoryDataMapper) {
        if let categories = data.assetsCategory, categories.count > 0 {
            self.assetCategory = categories
            self.categoryPicker.reloadAllComponents()
        }
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onCategoryFailed(data: String) {
        //self.showAlert(title: <#T##String#>, message: <#T##String#>)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        if let type = self.type {
            self.presenter.getAssetsCategoryDataFormServer(type: type)
        }
    }
    
}

extension PropertyNequipmentsAddViewController {
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
