//
//  PropertyOrEquipmentListViewController.swift
//  Ponno
//
//  Created by a k azad on 29/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PropertyOrEquipmentListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = PropertyOrEquipmentPresenter(service: AssestsService())
    
    var propertyOrEquipmentList : [PropertyOrEquipments] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var type : Int?
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    var isLoading: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.configureTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        if type == 1 {
            self.title = LanguageManager.Equipments
        }else{
            self.title = LanguageManager.Properties
        }
    }
    

}

//Mark: TableView Delegate and DataSource
extension PropertyOrEquipmentListViewController : UITableViewDelegate, UITableViewDataSource {
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PropertyNequipmentListCell.nib, forCellReuseIdentifier: PropertyNequipmentListCell.identifier)
        self.tableView.estimatedRowHeight = 70.0
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = true
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorStyle = .none
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.propertyOrEquipmentList.count > 0 {
            return self.propertyOrEquipmentList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PropertyNequipmentListCell = tableView.dequeueReusableCell(withIdentifier: PropertyNequipmentListCell.identifier, for: indexPath) as! PropertyNequipmentListCell
        
        if self.propertyOrEquipmentList.count > 0 {
            let item = self.propertyOrEquipmentList[indexPath.row]
            
            cell.nameLbl.text = item.categoryName
            cell.amountLbl.text = item.cost + Constants.currencySymbol
            
            let description = item.description
            if description.isEmpty{
                cell.descriptionBtn.isHidden = true
            }else{
                cell.descriptionBtn.tag = item.id
                cell.descriptionBtn.addTarget(self, action: #selector(onDescriptionBtnTapped), for: .touchUpInside)
            }
            
            cell.popUpBtn.tag = item.id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.propertyOrEquipmentList.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                if let type = self.type {
                    self.presenter.getPropertyOrEquipmentDataFormServer(type: type, page: self.currentPage)
                }
                
            }
            
        }
        
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    @objc func onPopUpBtnTapped(sender: UIBarButtonItem){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.propertyOrEquipmentList.count > 0 {
            
            for item in self.propertyOrEquipmentList{
                let id = sender.tag
                if id == item.id {
                    let updateAction = UIAlertAction(title:LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToPropertyOrEquipmentUpdateVC(item : item)
                    }
                    
                    let deleteAction = UIAlertAction(title:LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(updateAction)
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    @objc func onDescriptionBtnTapped(sender: UIButton){
        if self.propertyOrEquipmentList.count > 0 {
            for item in self.propertyOrEquipmentList{
                let id = sender.tag
                if id == item.id{
                    self.showAlert(title: item.description, message: "")
                }
            }
        }
    }
    
    func navigateToPropertyOrEquipmentUpdateVC(item : PropertyOrEquipments){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Assets", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PropertyOrEquipmentUpdateViewController") as! PropertyOrEquipmentUpdateViewController
        viewController.propertyOrEquipment = item
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.presenter.postAssetDeleteDateToServer(id: deleteId)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension PropertyOrEquipmentListViewController : PropertyOrEquipmentListViewDelegate{
    func setPropertyOrEquipmentData(data: PropertyOrEquipmentDataMapper) {
        if let list = data.propertyOrEquipments, list.count > 0 {
            self.propertyOrEquipmentList += list
        }
        
        if let pagination = data.pagination{
            self.lastPageNo = pagination.lastPageNo
        }
    }
    
    func onDelete(data: AddDataMapper) {
        if let message = data.message{
            self.displayMessage(userMessage: message)
        }
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        if let type = self.type {
            self.isLoading = true
            self.propertyOrEquipmentList = []
            self.presenter.getPropertyOrEquipmentDataFormServer(type:type, page: self.currentPage)
        }
        
    }
}

extension PropertyOrEquipmentListViewController {
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            if let type = self.type{
                self.propertyOrEquipmentList = []
                self.presenter.getPropertyOrEquipmentDataFormServer(type: type, page: self.currentPage)
            }
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
