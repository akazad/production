//
//  GoodWillListCell.swift
//  Ponno
//
//  Created by a k azad on 28/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class GoodWillListCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var endDateLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var popUpBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: GoodWillListCell.self)
    }
    
}
