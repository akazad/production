//
//  AssetsItemCell.swift
//  Ponno
//
//  Created by a k azad on 28/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class AssetsItemCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var assetPropertyNameLbl: UILabel!
    @IBOutlet weak var assetPropertyAmountLbl: UILabel!
    @IBOutlet weak var assetPropertyPopUpBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        setUpCardView(uiview: cardView)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: AssetsItemCell.self)
    }
    
}
