//
//  AssestsService.swift
//  Ponno
//
//  Created by a k azad on 28/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper

public class AssestsService : NSObject{
    
    func getAssetPropertyListData(success: @escaping (AssetListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.AssetProperties
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<AssetListDataMapper>) in
                if let assetPropertiesResponse = response.result.value{
                    if let investors = assetPropertiesResponse.assetList, investors.count > 0 {
                        success(assetPropertiesResponse)
                    }else if let failureResponse = assetPropertiesResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func postAddNewAssetPropertyData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.AssetPropertiesStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let assetPropertyAddResponse = response.result.value {
                if assetPropertyAddResponse.success == true{
                    success(assetPropertyAddResponse)
                }else if let failureMessage = assetPropertyAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postAddNewGoodWillData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.goodWillStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let goodWillAddResponse = response.result.value {
                if goodWillAddResponse.success == true{
                    success(goodWillAddResponse)
                }else if let failureMessage = goodWillAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postAssetPropertyUpdateData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.AssetPropertiesUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let assetPropertyUpdateResponse = response.result.value {
                if assetPropertyUpdateResponse.success == true{
                    success(assetPropertyUpdateResponse)
                }else if let failureMessage = assetPropertyUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postGoodWillUpdateData(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.goodWillUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let goodWillUpdateResponse = response.result.value {
                if goodWillUpdateResponse.success == true{
                    success(goodWillUpdateResponse)
                }else if let failureMessage = goodWillUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }

    
    func PostAssetPropertyDeleteData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){

        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }

        let url = RestURL.sharedInstance.AssetPropertiesDelete
        let header = RestURL.sharedInstance.postCommonHeader()

        let param = ["id": id]

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }

        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let assetPropertyDeleteResponse = response.result.value {
                if assetPropertyDeleteResponse.success == true{
                    success(assetPropertyDeleteResponse)
                }else if let failureResponse = assetPropertyDeleteResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }

        }
        print(request)
    }
    
    func PostGoodWillDeleteData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.goodWillDelete
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let goodWillDeleteResponse = response.result.value {
                if goodWillDeleteResponse.success == true{
                    success(goodWillDeleteResponse)
                }else if let failureResponse = goodWillDeleteResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func getAssetsCategoryData(type: Int, success: @escaping (AssetsCategoryDataMapper) -> (), failure : @escaping (String) -> ()){

        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }

        let url = RestURL.sharedInstance.AssetProperties + RestURL.sharedInstance.getAllType(text: type)

        let header = RestURL.sharedInstance.getCommonHeader()

        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<AssetsCategoryDataMapper>) in
                if let assetCategoriesResponse = response.result.value{
                    if let assetsCategory = assetCategoriesResponse.assetsCategory, assetsCategory.count > 0 {
                        success(assetCategoriesResponse)
                    }else if let failureResponse = assetCategoriesResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getPropertyOrEquipmentListData(type: Int, page: Int, success: @escaping (PropertyOrEquipmentDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PropertyEquipmentList + RestURL.sharedInstance.getCommonId(id: type) + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(url)
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PropertyOrEquipmentDataMapper>) in
                if let propertyOrEquipmentsResponse = response.result.value{
                    if let propertyOrEquipments = propertyOrEquipmentsResponse.propertyOrEquipments, propertyOrEquipments.count > 0 {
                        success(propertyOrEquipmentsResponse)
                    }else if let failureResponse = propertyOrEquipmentsResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
//    func PostPropertyOrEquipmentDeleteData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
//        
//        if !Connectivity.isConnectedToInternet {
//            failure(CommonMessages.NoInternet)
//        }
//        
//        let url = RestURL.sharedInstance.AssetPropertiesDelete
//        let header = RestURL.sharedInstance.postCommonHeader()
//        
//        let param = ["id": id]
//        
//        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
//            print(json)
//        }
//        
//        let request = Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
//            if let assetDeleteResponse = response.result.value {
//                if assetDeleteResponse.success == true{
//                    success(assetDeleteResponse)
//                }else if let failureResponse = assetDeleteResponse.message{
//                    failure(failureResponse)
//                }
//            }else{
//                failure(CommonMessages.ApiFailure)
//            }
//            
//        }
//        print(request)
//    }
    
    func getGoodWillListData(page: Int, success: @escaping (GoodWillDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.goodWillList + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<GoodWillDataMapper>) in
                if let goodWillResponse = response.result.value{
                    if let goodWill = goodWillResponse.goodWillData, goodWill.count > 0 {
                        success(goodWillResponse)
                    }else if let failureResponse = goodWillResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
}
