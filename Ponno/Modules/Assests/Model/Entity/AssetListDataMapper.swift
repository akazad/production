//
//  AssetListDataMapper.swift
//  Ponno
//
//  Created by a k azad on 28/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class AssetListDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var assetList : [AssetList]?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        assetList <- map["result_data"]
    }
    
}
