//
//  GoodWillData.swift
//  Ponno
//
//  Created by a k azad on 28/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class GoodWillData : Mappable {
    var id : Int = -1
    var amount : String = ""
    var startDate : String = ""
    var endDate : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        amount <- map["amount"]
        startDate <- map["start_date"]
        endDate <- map["end_date"]
    }
    
}

