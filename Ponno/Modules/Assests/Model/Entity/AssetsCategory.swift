//
//  AssetsCategory.swift
//  Ponno
//
//  Created by a k azad on 29/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class AssetsCategory : Mappable {
    var id : Int = 0
    var name : String = ""
    var type : Int = 0
    var pharmacyId : Int = 0
    var createdAt : String = ""
    var updatedAt : String = ""
    var deletedAt : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        type <- map["type"]
        pharmacyId <- map["pharmacy_id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
    }
    
}

