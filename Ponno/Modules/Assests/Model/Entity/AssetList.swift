//
//  AssetList.swift
//  Ponno
//
//  Created by a k azad on 28/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class AssetList : Mappable {
    var name : String = ""
    var amount : Double = 0.0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
        amount <- map["amount"]
    }
    
}
