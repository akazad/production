//
//  PropertyOrEquipments.swift
//  Ponno
//
//  Created by a k azad on 29/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PropertyOrEquipments : Mappable {
    var id : Int = 0
    var categoryId : Int = 0
    var cost : String = ""
    var type : Int = 0
    var description : String = ""
    var pharmacyId : Int = 0
    var createdAt : String = ""
    var updatedAt : String = ""
    var deletedAt : String = ""
    var categoryName : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        categoryId <- map["category_id"]
        cost <- map["cost"]
        type <- map["type"]
        description <- map["description"]
        pharmacyId <- map["pharmacy_id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        deletedAt <- map["deleted_at"]
        categoryName <- map["category_name"]
    }
    
}
