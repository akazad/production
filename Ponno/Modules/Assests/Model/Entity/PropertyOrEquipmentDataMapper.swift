//
//  PropertyOrEquipmentDataMapper.swift
//  Ponno
//
//  Created by a k azad on 29/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PropertyOrEquipmentDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var propertyOrEquipments : [PropertyOrEquipments]? 
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        propertyOrEquipments <- map["assets_data"]
        pagination <- map["pagination"]
    }
    
}
