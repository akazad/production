//
//  GoodWillDataMapper.swift
//  Ponno
//
//  Created by a k azad on 28/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class GoodWillDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var goodWillData : [GoodWillData]? 
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        goodWillData <- map["good_will_data"]
        pagination <- map["pagination"]
    }
    
}

