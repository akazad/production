//
//  PurchaseHistoryCell.swift
//  Ponno
//
//  Created by a k azad on 10/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PurchaseHistoryCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var invoiceLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var paidLabel: UILabel!
    @IBOutlet weak var dueLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: PurchaseHistoryCell.self)
    }
}
