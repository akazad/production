//
//  WalletCell.swift
//  Ponno
//
//  Created by a k azad on 28/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class WalletCell: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!{
        didSet{
            self.setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var descriptionImageView: CircularImageView!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var popUpMenuBtn: UIButton!
    
    var walletTransaction : WalletTransactions?{
        didSet{
            if let wallet = walletTransaction{
                dateLbl.text = wallet.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.dd_MMM_yy_c_HH_mm_a.rawValue)
                let type = wallet.type

                if type == 0 {
                    colorView.backgroundColor = UIColor.red
                }else{
                    colorView.backgroundColor = UIColor(red: 59, green: 149, blue: 31)
                }
                
                amountLbl.textColor = UIColor.white
                amountLbl.text = wallet.amount
            }
        }
    }
    
    var vendorWalletTransaction : VendorWalletTransactions?{
        didSet{
            if let wallet = vendorWalletTransaction{
                dateLbl.text = wallet.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.dd_MMM_yy_c_HH_mm_a.rawValue)
                let type = wallet.type

                if type == 0 {
                    colorView.backgroundColor = UIColor.red
                }else{
                    colorView.backgroundColor = UIColor(red: 59, green: 149, blue: 31)
                }
                
                amountLbl.textColor = UIColor.white
                amountLbl.text = wallet.amount
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: WalletCell.self)
    }
    
}
