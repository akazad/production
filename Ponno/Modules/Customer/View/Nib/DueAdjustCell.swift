//
//  DueAdjustCell.swift
//  Ponno
//
//  Created by a k azad on 27/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DueAdjustCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            self.setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var adjustAmountLbl: UILabel!
    @IBOutlet weak var adjustedBy: UILabel!
    @IBOutlet weak var descriptionImageView: UIImageView!
    @IBOutlet weak var popUpBtn: UIButton!
    
    var dueAdjust : DueAdjustList?{
        didSet{
            if let item = self.dueAdjust{
                dateLbl.text = item.createdAt?.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_yy_h_mm_a.rawValue)
                if let amount = item.amount{
                    adjustAmountLbl.text = LanguageManager.DueAdjustAmount + ": " + amount
                }
                if let addedBy = item.addedBy{
                    adjustedBy.text = LanguageManager.AdjustedBy + ": "  + addedBy
                }
            }
        }
    }
    
    var payableAdjust : PayableAdjustList?{
        didSet{
            if let item = self.payableAdjust{
                dateLbl.text = item.createdAt?.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.MMM_d_yy_h_mm_a.rawValue)
                if let amount = item.amount{
                    adjustAmountLbl.text = LanguageManager.PayableAdjustAmount + ": " + amount
                }
                if let addedBy = item.addedBy{
                    adjustedBy.text = LanguageManager.AdjustedBy + ": "  + addedBy
                }
            }
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: DueAdjustCell.self)
    }
    
}
