//
//  CustomerDetailsSummaryCell.swift
//  Ponno
//
//  Created by a k azad on 20/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class CustomerDetailsSummaryCell: UITableViewCell {

    @IBOutlet weak var totalPurchaseLabel: UILabel!
    @IBOutlet weak var totalPurchaseTitleLbl: UILabel!{
        didSet{
            totalPurchaseTitleLbl.text = LanguageManager.TotalPurchase
        }
    }
    @IBOutlet weak var totalDueLabel: UILabel!
    @IBOutlet weak var totalDueTitleLbl: UILabel!{
        didSet{
            totalDueTitleLbl.text = LanguageManager.TotalDue
        }
    }
    @IBOutlet weak var currentDueLabel: UILabel!
    @IBOutlet weak var currentDueTitleLbl: UILabel!{
        didSet{
            currentDueTitleLbl.text = LanguageManager.CurrentDue
        }
    }
    @IBOutlet weak var totalDuePaidLabel: UILabel!
    @IBOutlet weak var totalDueWithdrawTitleLbl: UILabel!{
        didSet{
            totalDueWithdrawTitleLbl.text = LanguageManager.TotalDuePaid
        }
    }
    @IBOutlet weak var cardView: UIView!
    
    var customerData : CustomerData?{
        didSet{
            if let data = customerData{
                setLabelTextColor(amount: data.currentDue, label: currentDueLabel)
                setLabelTextColor(amount: data.totalDue, label: totalDueLabel)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: CustomerDetailsSummaryCell.self)
    }
}
