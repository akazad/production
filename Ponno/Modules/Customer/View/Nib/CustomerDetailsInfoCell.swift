//
//  CustomerDetailsCell.swift
//  Ponno
//
//  Created by a k azad on 12/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class CustomerDetailsInfoCell: UITableViewCell {

   
    @IBOutlet var imageIcon: UIImageView!
    @IBOutlet weak var alphabetView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var AddressLabel: UILabel!
    @IBOutlet weak var duePaidBtn: UIButton!
    @IBOutlet weak var cardView: UIView!
    
   override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: CustomerDetailsInfoCell.self)
    }
    
}
