//
//  CustomerListCell.swift
//  Ponno
//
//  Created by a k azad on 28/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class CustomerListCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var customerIcon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var popUpBtnTapped: UIButton!
    
    var customerList : Customers?{
        didSet{
            if let customer = customerList{
                customerIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                customerIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.customerImage + customer.image), placeholderImage: UIImage(named: "placeholder"))
                
                nameLabel.text = LanguageManager.Name + " : " +  customer.name
                phoneLabel.text = LanguageManager.Phone + " : " + customer.phone
                
                let address = customer.address
                if address.isEmpty{
                    addressLabel.text = LanguageManager.Address + " : ---"
                }else{
                    addressLabel.text = LanguageManager.Address + " : " + address
                }
            }
        }
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: CustomerListCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }
    
    private func setUpViews (){
        self.customerIcon.layer.cornerRadius = self.customerIcon.frame.height/2
        self.customerIcon.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
