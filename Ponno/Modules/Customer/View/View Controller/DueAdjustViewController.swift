//
//  DueAdjustViewController.swift
//  Ponno
//
//  Created by a k azad on 27/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class DueAdjustViewController: UIViewController {
    
    @IBOutlet weak var dateSearchHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    
    fileprivate var presenter = DueAdjustListPresenter(service: CustomerService())
    
    var dueAdjustList : [DueAdjustList]?{
        didSet{
            self.tableView.reloadData()
        }
    }
    var summary : [Summary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    var customerId: Int?
    var customer : CustomerData?
    var currentDue : String?
    
    var datePicker = UIDatePicker()
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    var searchIsActive = false{
        didSet{
            self.tableView.reloadData()
        }
    }
    
    //var searchPage: Int = 1
    var searchEnable : Bool = false
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.setBarButton()
        self.showStartDatePicker()
        self.showEndDatePicker()
        self.configureCollectionView()
        self.configureTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func initialSetup(){
        self.setUpInitialLanguage()
        self.title = LanguageManager.DueAdjust
        self.dateSearchHeight.constant = 0.0
        self.searchBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarButton(){
        
//        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
//        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
//        searchBtn.addTarget(self, action: #selector(onCustomerSearch(sender:)), for: UIControl.Event.touchUpInside)
//        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
//        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
//        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
//        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let dueAdjustAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        dueAdjustAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        dueAdjustAddBtn.addTarget(self, action: #selector(onDueAdjustAdd(sender:)), for: UIControl.Event.touchUpInside)
        dueAdjustAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        dueAdjustAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        dueAdjustAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: dueAdjustAddBtn)
        
        let dateSearchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        dateSearchBtn.setImage(UIImage(named: "dateSearch"), for: UIControl.State.normal)
        dateSearchBtn.addTarget(self, action: #selector(onDateSearch(sender:)), for: UIControl.Event.touchUpInside)
        dateSearchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        dateSearchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        dateSearchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: dateSearchBtn)
        
        navigationItem.setRightBarButtonItems([barButton2,barButton1], animated: true)
        
    }
    
    @objc func onDateSearch(sender: UIBarButtonItem){
        if searchEnable == false{
            self.dateSearchHeight.constant = 30.0
            self.searchEnable = !searchEnable
        }else{
            self.dateSearchHeight.constant = 0.0
            searchEnable = !searchEnable
        }
    }
    
    @objc func onDueAdjustAdd(sender: UIBarButtonItem){
        guard let data = self.customer else{
            return
        }
        self.navigateToDueAdjustAddVC(item: data)
    }
    
}

//Search Date Picker
extension DueAdjustViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: "", style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            
            self.dueAdjustList = []
            self.currentPage = 1
            self.searchIsActive = true
            
            if let id = self.customerId{
                self.presenter.getDueAdjustDateSearchDataFromServer(startDt: startDate, endDt: endDate, page: currentPage, id: id)
            }
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: "", style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

//MARK: CollectionView Delegate And DataSource
extension DueAdjustViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(SalesSummeryCell.nib, forCellWithReuseIdentifier: SalesSummeryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let item = self.summary, item.count > 0 else {
            return 0
        }
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SalesSummeryCell = collectionView.dequeueReusableCell(withReuseIdentifier: SalesSummeryCell.identifier, for: indexPath) as! SalesSummeryCell
        
        guard let item = self.summary, item.count > 0 else{
            return cell
        }
        let adjustItem = item[indexPath.row % item.count]
        
        cell.SalesSummeryName.text = adjustItem.title
        cell.SalesSummeryNumber.text = adjustItem.value

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 60.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.summary, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
}

extension DueAdjustViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(DueAdjustCell.nib, forCellReuseIdentifier: DueAdjustCell.identifier)
        self.tableView.separatorStyle = .none
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let item = self.dueAdjustList{
            return item.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DueAdjustCell = tableView.dequeueReusableCell(withIdentifier: "DueAdjustCell", for: indexPath) as! DueAdjustCell
        
        guard let adjustList = self.dueAdjustList else{
            return cell
        }
        let item = adjustList[indexPath.row]
        cell.dueAdjust = item
        
        let desc = item.description
        if desc.isEmpty {
            cell.descriptionImageView.isHidden = true
        }else{
            cell.descriptionImageView.addTapGestureRecognizer{
                self.showAlert(title: LanguageManager.Description, message: desc)
            }
        }
        
        if let id = item.id{
            cell.popUpBtn.tag = id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
        }
        
        if isLoading == false && indexPath.row == adjustList.count - 1 && self.currentPage < self.lastPageNo{
            self.isLoading = true
            self.currentPage += 1
            
            if let id = item.customerId{
                self.presenter.getDueAdjustListDataFromServer(page: self.currentPage, id: id)
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        guard let item = self.dueAdjustList else {
            return
        }
        
        if item.count > 0{
            let id = sender.tag
            for adjustItem in item{
                if id == adjustItem.id{
                    let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToDueAdjustUpdateVC(item: adjustItem)
                    }
                    
                    let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    // add action buttons to action sheet
                    myActionSheet.addAction(updateAction)
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        // present the action sheet
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
}

extension DueAdjustViewController : DueAdjustListViewDelegate{
    func setDueAdjustData(data: DueAdjustListDataMapper) {
        
        self.currentDue = data.customerCurrentDue
        
        guard let summary = data.summary else{
            return
        }
        self.summary = summary
        self.setUpAutoScroll()
        
        guard let adjustList = data.dueAdjustList, adjustList.count > 0 else{
            return
        }
        self.dueAdjustList = adjustList
        
        
    }
    
    func onDueAdjustDelete(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        //self.showAlert(title: , message: )
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(failure: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: failure)
    }
    
    func onDateSearchFailed(message: String) {
        self.datePickerTextField.text = ""
        self.toDatePickerTextField.text = ""
        self.dateSearchFailedMessage(userMessage: LanguageManager.NoInformationFound)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        if let id = self.customerId{
            self.presenter.getDueAdjustListDataFromServer(page: self.currentPage, id: id)
        }
        
    }
}

extension DueAdjustViewController{
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Delete, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
                let param : [String: Any] = ["id": deleteId]
                
                self.presenter.postDueAdjustDeleteDataToServer(param: param)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToDueAdjustAddVC(item: CustomerData){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DueAdjustUpdateViewController") as! DueAdjustUpdateViewController
        viewController.type = Type.add
        viewController.customer = item
        viewController.currentDue = self.currentDue?.toDouble()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToDueAdjustUpdateVC(item: DueAdjustList){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DueAdjustUpdateViewController") as! DueAdjustUpdateViewController
        viewController.type = Type.update
        viewController.dueAdjust = item
        viewController.currentDue = self.currentDue?.toDouble()
        if let customer = self.customer{
            viewController.customer = customer
        }
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title:  LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.dueAdjustList = []
                self.tableView.reloadData()
                guard let id = self.customerId else{
                    return
                }
                    
                self.presenter.getDueAdjustListDataFromServer(page: self.currentPage, id: id)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func dateSearchFailedMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title:  userMessage, message: "", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.dueAdjustList = []
            self.tableView.reloadData()
            guard let id = self.customerId else{
                return
            }
            
            self.presenter.getDueAdjustListDataFromServer(page: self.currentPage, id: id)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
