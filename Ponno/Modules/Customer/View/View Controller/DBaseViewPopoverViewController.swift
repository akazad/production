//
//  DBaseViewPopoverViewController.swift
//  Ponno
//
//  Created by a k azad on 27/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

enum PopedOverFrom: String{
    case customer
    case vendor
}

class DBaseViewPopoverViewController: UIViewController {

    @IBOutlet weak var adjustBtn: UIButton!
    @IBOutlet weak var walletBtn: UIButton!
    
    var popedOverFrom = PopedOverFrom.customer.rawValue
    var customerId: Int?
    var vendorId: Int?
    
    var navigate: NavigateToAdjustAmount?
    var navigateTo: NavigateToWallet?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.intialSetup()
    }
    
    func intialSetup(){
        self.setUpInitialLanguage()
        if popedOverFrom == PopedOverFrom.customer.rawValue{
            self.adjustBtn.setTitle(LanguageManager.DueAdjust, for: .normal)
            self.walletBtn.setTitle(LanguageManager.CustomerWallet, for: .normal)
        }else{
            self.adjustBtn.setTitle(LanguageManager.PayableAdjust, for: .normal)
            self.walletBtn.setTitle(LanguageManager.VendorWallet, for: .normal)
        }
        
        self.adjustBtn.addTarget(self, action: #selector(onAdjustBtnTapped), for: .touchUpInside)
        self.walletBtn.addTarget(self, action: #selector(onWalletBtnTapped), for: .touchUpInside)
    }
    
    @objc func onAdjustBtnTapped(sender: UIButton){
        if popedOverFrom == PopedOverFrom.customer.rawValue{
            if let id = self.customerId{
                self.dismiss(animated: true, completion: nil)
                navigate?.navigateTo(id: id)
            }
        }else{
            if let id = self.vendorId{
                self.dismiss(animated: true, completion: nil)
                navigate?.navigateTo(id: id)
            }
        }
    }
    
    @objc func onWalletBtnTapped(sender: UIButton){
        if popedOverFrom == PopedOverFrom.customer.rawValue{
            if let id = self.customerId{
                self.dismiss(animated: true, completion: nil)
                navigateTo?.navigateToWallet(id: id)
            }
        }else{
            if let id = self.vendorId{
                self.dismiss(animated: true, completion: nil)
                navigateTo?.navigateToWallet(id: id)
            }
        }
    }
    
    
    
}
