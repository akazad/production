//
//  ViewController.swift
//  UBottomSheet
//
//  Created by ugur on 13.08.2018.
//  Copyright © 2018 otw. All rights reserved.
//

import UIKit
import SDWebImage

class CustomerDetailsBaseViewController: UIViewController, BottomSheetDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var container: UIView!
    
    enum Sections : Int{
        case CustomerInfo
        case CustomerSummary
    }

    var customerData : CustomerData?
    var customerDetails : CustomerViewDataMapper?
    
    var customerId : Int?
    
    private var presenter = CustomerViewPresenter(service: CustomerService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        container.layer.cornerRadius = 15
        container.layer.masksToBounds = true
        self.configureTableView()
        self.setBarButton()
//        self.tableView.isHidden = true
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? CustomerDetailsViewController{
            viewController.bottomSheetDelegate = self
            viewController.customerId = self.customerId
            viewController.parentView = container
        }
    }
    
    func updateBottomSheet(frame: CGRect) {
        container.frame = frame
    }
    
    func setBarButton(){
        
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "edit"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onEdit(sender:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func onEdit(sender: UIBarButtonItem){
        self.navigateToCustomerDuePaidVC()
    }
    
    func navigateToCustomerDuePaidVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerDuePaidViewController") as! CustomerDuePaidViewController
        viewController.customerData = customerData
        viewController.customerId = self.customerId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//MARK: TableViewDelegate And DataSource
extension CustomerDetailsBaseViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(CustomerDetailsInfoCell.nib, forCellReuseIdentifier: CustomerDetailsInfoCell.identifier)
        self.tableView.register(CustomerDetailsSummaryCell.nib, forCellReuseIdentifier: CustomerDetailsSummaryCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.CustomerInfo.rawValue:
            return 1
        case Sections.CustomerSummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case Sections.CustomerInfo.rawValue:
            let cell : CustomerDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: CustomerDetailsInfoCell.identifier, for: indexPath) as! CustomerDetailsInfoCell
            guard let customer = self.customerData else{
                return cell
            }
            cell.nameLabel.text = customer.name
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.customerImage + customer.image), placeholderImage: UIImage(named: "placeholder"))
            cell.phoneLabel.text = "ফোনঃ " + customer.phone
            cell.AddressLabel.text = "ঠিকানাঃ " +  customer.address
            self.customerId = Int(customer.id)
            return cell
            
        case Sections.CustomerSummary.rawValue:
            let cell : CustomerDetailsSummaryCell = tableView.dequeueReusableCell(withIdentifier: CustomerDetailsSummaryCell.identifier, for: indexPath) as! CustomerDetailsSummaryCell
            guard let customer = self.customerData else{
                return cell
            }
            cell.totalPurchaseLabel.text = String(describing: customer.totalPurchase)
            cell.currentDueLabel.text = String(describing:customer.currentDue) + " " + Constants.currencySymbol
            cell.totalDueLabel.text = String(describing: customer.totalDue) + " " + Constants.currencySymbol
            cell.totalDuePaidLabel.text = String(describing: customer.totalPaid) + " " + Constants.currencySymbol
            
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.CustomerInfo.rawValue:
            return UITableView.automaticDimension
        case Sections.CustomerSummary.rawValue:
            return 140.0
        default:
            return 0
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
}

//Mark: Api Delegate
extension CustomerDetailsBaseViewController : CustomerDetailsViewDelegate{
    func setCustomerViewData(data: CustomerViewDataMapper) {
        //self.customerDetails = data
        self.customerData = data.customer
        self.refreshTableView()
    }
    
    func setCustomerDueHistoryData(data: CustomerDueHistoryDataMapper) {
        
    }
    
    func setCustomerPurchaseHistoryData(data: CustomerPurchaseHistoryDataMapper) {
        
    }
    
    func onFailed() {
        self.showAlert(title: "কোন তথ্য পাওয়া যায়নি", message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.customerId else{
            return
        }
        self.presenter.getCustomerDataFromServer(id: id)
    }
}



