//
//  CustomerDetViewController.swift
//  Ponno
//
//  Created by a k azad on 29/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class CustomerDViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!{
        didSet{
            segmentedControl.setTitle(LanguageManager.DueHistory, forSegmentAt: 0)
            segmentedControl.setTitle(LanguageManager.PurchaseHistory, forSegmentAt: 1)
        }
    }
    @IBOutlet weak var emptyMessage: UILabel!{
        didSet{
            emptyMessage.text = LanguageManager.NoInformationFound
        }
    }
    
    var isFirst = true
    var selectedSegment : Int?
    
    private var presenter = CustomerViewPresenter(service: CustomerService())
    var customerId : Int?
    
    var purchaseHistory : [CustomerPurchaseHistory] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var dueHistory : [CustomerDueHistory] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isLoading : Bool = false
    var dueCurrentPage : Int = 1
    var puchaseCurrentPage : Int = 1
    var dueLastPage: Int = 0
    var purchaseLastPage: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.configureTableView()
        self.initialSetUp()
        self.attachPresenter()
    }
    
    func initialSetUp(){
        self.segmentedControl.addTarget(self, action: #selector(onSegmentChange), for: .valueChanged)
    }
    
    @objc func onSegmentChange(sender: UISegmentedControl){
        
        guard let id = self.customerId else{
            return
        }
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            self.selectedSegment = 0
            self.dueHistory = []
            self.isLoading = true
            self.dueCurrentPage = 1
            self.isFirst = true
            self.tableView.reloadData()
            self.presenter.getCustomerDueHistoryDataFromServer(page: self.dueCurrentPage, id: id)
        case 1:
            self.selectedSegment = 1
            self.purchaseHistory = []
            self.isLoading = true
            self.puchaseCurrentPage = 1
            self.isFirst = false
            self.tableView.reloadData()
            self.presenter.getCustomerPurchaseHistoryDataFromServer(page: self.puchaseCurrentPage, id: id)
        default:
            break;
        }
        
    }
    
}

//Mark: TableView Delegate And Data Source
extension CustomerDViewController: UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = .clear
        self.tableView.register(DueHistoryCell.nib, forCellReuseIdentifier: DueHistoryCell.identifier)
        self.tableView.register(PurchaseHistoryCell.nib, forCellReuseIdentifier: PurchaseHistoryCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.emptyMessage.isHidden = true
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFirst{
            if self.dueHistory.count > 0 {
                self.emptyMessage.isHidden = true
                return self.dueHistory.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        } else{
            if self.purchaseHistory.count > 0 {
                self.emptyMessage.isHidden = true
                return self.purchaseHistory.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFirst{
            let cell : DueHistoryCell = tableView.dequeueReusableCell(withIdentifier: DueHistoryCell.identifier, for: indexPath) as! DueHistoryCell
            cell.selectionStyle = .none
            if self.dueHistory.count > 0 {
                let item = self.dueHistory[indexPath.row]
                cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.dateLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.amountLabel.text = LanguageManager.Amount  + " : " + "\(item.amount )" + " " + Constants.currencySymbol
                cell.sellerNameLabel.text = LanguageManager.By + " : " + item.sellerName
                if item.type == 0 {
                    cell.typeLabel.text = LanguageManager.Due
                    cell.typeLabel.textColor = UIColor(red: CGFloat(220/255.0), green: CGFloat(53/255.0), blue: CGFloat(69/255.0), alpha: CGFloat(1.0))
                    
                }
                if item.type == 1 {
                    cell.typeLabel.text = LanguageManager.Withdraw
                    cell.typeLabel.textColor = UIColor(red: CGFloat(91/255.0), green: CGFloat(160/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1.0))
                    
                }
                if isLoading == false && indexPath.row == self.dueHistory.count - 1 && dueCurrentPage < dueLastPage {
                    self.isLoading = true
                    self.dueCurrentPage += 1
                    guard let id = self.customerId else{
                        return cell
                    }
                    self.presenter.getCustomerDueHistoryDataFromServer(page: self.dueCurrentPage, id: id)
                }
            }
            return cell
        }else{
            let cell : PurchaseHistoryCell = tableView.dequeueReusableCell(withIdentifier: PurchaseHistoryCell.identifier, for: indexPath) as! PurchaseHistoryCell
            cell.selectionStyle = .none
            if self.purchaseHistory.count > 0 {
                let item = self.purchaseHistory[indexPath.row]
                cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.dateLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.invoiceLabel.text = item.invoice
                cell.totalLabel.text = LanguageManager.TotalAmount + " : " + "\(item.total)" + " " + Constants.currencySymbol
                cell.paidLabel.text = LanguageManager.PaidAmount + " : " + "\(item.paid)" + " " + Constants.currencySymbol
                cell.dueLabel.text = LanguageManager.Due + " : " + "\(item.due)" + " " + Constants.currencySymbol
                if isLoading == false && indexPath.row == self.purchaseHistory.count - 1 && self.puchaseCurrentPage < purchaseLastPage {
                    self.isLoading = true
                    self.puchaseCurrentPage += 1
                    guard let id = self.customerId else{
                        return cell
                    }
                    self.presenter.getCustomerPurchaseHistoryDataFromServer(page: self.puchaseCurrentPage, id: id)
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if isFirst{
            return 85.0
        }else{
            return 110.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isFirst{
            if self.purchaseHistory.count > 0 {
                let item = self.purchaseHistory[indexPath.row] 
                let id = item.id
                self.navigateToPurchaseDetailsVC(selectedId : id)
            }
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let saleDetailsVC = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        saleDetailsVC.iD = iD
        self.navigationController?.pushViewController(saleDetailsVC, animated: true)
    }
    
    func navigateToPurchaseDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let saleDetailsVC = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        saleDetailsVC.iD = selectedId
        self.navigationController?.pushViewController(saleDetailsVC, animated: true)
    }
    
}

//Mark: Api Delegate
extension CustomerDViewController : CustomerDetailsViewDelegate{
    func setCustomerDueHistoryData(data: CustomerDueHistoryDataMapper) {
        guard let dueList = data.dues, dueList.count > 0 else{
            return
        }
        self.isLoading = false
        self.dueHistory += dueList
        
        guard let pagination = data.pagination else{
            return
        }
        self.dueLastPage = pagination.lastPageNo
        
    }
    
    func setCustomerPurchaseHistoryData(data: CustomerPurchaseHistoryDataMapper) {
        guard let purchaseList = data.purchases, purchaseList.count > 0 else{
            return
        }
        self.isLoading = false
        self.purchaseHistory += purchaseList
        
        guard let pagination = data.pagination else {
            return
        }
        self.purchaseLastPage = pagination.lastPageNo
        
    }
    
    func setCustomerViewData(data: CustomerViewDataMapper) {
        
        
    }
    
    func onFailed() {
       // self.isLoading = true
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.emptyMessage.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.emptyMessage.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.customerId else{
            return
        }
        self.presenter.getCustomerDueHistoryDataFromServer(page: self.dueCurrentPage, id: id)
    }
}
