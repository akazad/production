//
//  CustomerWalletViewController.swift
//  Ponno
//
//  Created by a k azad on 28/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class CustomerWalletViewController: UIViewController {
    
    @IBOutlet weak var cashInitialInputTextField: UITextField!{
           didSet{
               cashInitialInputTextField.placeholder = LanguageManager.CashInToCustomerWallet
           }
       }
       @IBOutlet weak var cashInitialInputHeight: NSLayoutConstraint!
       @IBOutlet weak var cashInitialSubmitBtn: UIButton!
       @IBOutlet weak var cashInitialSubmitBtnHeight: NSLayoutConstraint!
       
       @IBOutlet weak var cashLbl: UILabel!
       @IBOutlet weak var cashLblHeight: NSLayoutConstraint!
       @IBOutlet weak var cashInBtn: UIButton!{
           didSet{
               cashInBtn.setTitle("+ " + LanguageManager.CashIn, for: .normal)
           }
       }
       @IBOutlet weak var cashInBtnHeight: NSLayoutConstraint!
       @IBOutlet weak var cashOutBtn: UIButton!{
           didSet{
               cashOutBtn.setTitle("- " + LanguageManager.CashOut, for: .normal)
           }
       }
       @IBOutlet weak var cashOutBtnHeight: NSLayoutConstraint!
       
       @IBOutlet weak var datePickerTextField: UITextField!{
           didSet{
               datePickerTextField.placeholder = LanguageManager.From
           }
       }
       @IBOutlet weak var toDatePickerTextField: UITextField!{
           didSet{
               toDatePickerTextField.placeholder = LanguageManager.From
           }
       }
       @IBOutlet weak var searchBtn: UIButton!{
           didSet{
               searchBtn.setTitle(LanguageManager.Search, for: .normal)
           }
       }
       
       @IBOutlet weak var noInfoMessageLbl: UILabel!{
           didSet{
               noInfoMessageLbl.text = LanguageManager.NoInformationFound
           }
       }
       @IBOutlet weak var tableView: UITableView!
    
    fileprivate var presenter = CustomerWalletPresenter(service: CustomerService())
    
    var customerWallet : CustomerWallet?
    var walletTransaction : [WalletTransactions] = []{
        didSet{
            self.refreshTableView()
        }
    }
    var customerId: Int?
    var walltetAmount : Double?
    
    var datePicker = UIDatePicker()
    var currentPage : Int = 1
    var lastPageNo : Int = 0
    var isLoading : Bool = false
    
    var type : Int = 0
    var alertTitle : String = LanguageManager.CashIn
    
    var modelType : String = ""
    var linkId : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.attributeSetUp()
        self.setUpInitialLanguage()
        self.showStartDatePicker()
        self.showEndDatePicker()
        self.configureTableView()
        self.cashInititalIsHidden(hidden: true)
        self.cashIsHidden(is: true)
        self.hideDateSearch(is: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.attachPresenter()
    }
    
    func attributeSetUp(){
        
        self.cashInBtn.addTarget(self, action: #selector(onCashInBtnTapped), for: .touchUpInside)
        self.cashOutBtn.addTarget(self, action: #selector(onCashOutBtnTapped), for: .touchUpInside)
        
        self.searchBtn.addTarget(self, action: #selector(onSearchBtnTapped), for: .touchUpInside)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func initialSetup(){
        
        self.title = LanguageManager.CustomerWallet
        
        if self.customerWallet == nil{
            self.cashInititalIsHidden(hidden: false)
            self.cashIsHidden(is: true)
            self.hideDateSearch(is: true)
            
            self.cashInitialSubmitBtn.addTarget(self, action: #selector(onInitialSubmitBtnPressed), for: .touchUpInside)
            
        }else{
            self.cashInititalIsHidden(hidden: true)
            self.cashIsHidden(is: false)
            self.hideDateSearch(is: false)
            
            if let cash = self.customerWallet  {
                let amount = cash.amount
                if amount.isEmpty{
                    self.cashLbl.text = "0.00" + Constants.currencySymbol
                }else{
                    self.cashLbl.text = LanguageManager.Cash + " : " +  amount + " " + Constants.currencySymbol
                    self.walltetAmount = amount.toDouble()
                }
            }
        }
        
    }
    
    func cashIsHidden(is hidden: Bool){
        self.cashLbl.isHidden = hidden
        self.cashInBtn.isHidden = hidden
        self.cashOutBtn.isHidden = hidden
    }
    
    func cashInititalIsHidden(hidden: Bool){
        self.cashInitialInputTextField.isHidden = hidden
        self.cashInitialSubmitBtn.isHidden = hidden
    }
    
    func hideDateSearch(is hidden: Bool){
        self.datePickerTextField.isHidden = hidden
        self.toDatePickerTextField.isHidden = hidden
        self.searchBtn.isHidden = hidden
    }
    
    @objc func onInitialSubmitBtnPressed(sender: UIButton){
        if isValidated(){
            if let cashInText = self.cashInitialInputTextField.text, let cashIn = Double(cashInText) {
                if let id = self.customerId{
                    let param : [String : Any] = ["amount": cashIn, "customer_id": id]
                    self.presenter.postCustomerWalletCashInDataToServer(param: param)
                }
                
            }
            
        }
        
    }
    
    func isValidated()->Bool{
        if (self.cashInitialInputTextField.text?.isEmpty)! {
            self.showAlert(title: LanguageManager.CashIsRequired, message: "")
            return false
        }
        return true
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.cashInitialSubmitBtn.isEnabled = isEnabled
    }
    
    @objc func onCashInBtnTapped(sender: UIButton){
        self.type = 1
        self.alertTitle = LanguageManager.CashIn
        
        self.alertWithTextField(title: alertTitle, type: type)
    }
    
    @objc func onCashOutBtnTapped(sender: UIButton){
        self.type = 0
        self.alertTitle = LanguageManager.CashOut
        
        self.alertWithTextField(title: alertTitle, type: type)
    }
    

}

//Mark : TableView Delegate and Data Source
extension CustomerWalletViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(WalletCell.nib, forCellReuseIdentifier: WalletCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.tableView.estimatedRowHeight = 60.0
        self.automaticallyAdjustsScrollViewInsets = false
        self.noInfoMessageLbl.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.walletTransaction.count > 0{
            self.noInfoMessageLbl.isHidden = true
            return self.walletTransaction.count
        }else{
            self.noInfoMessageLbl.isHidden = false
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : WalletCell = tableView.dequeueReusableCell(withIdentifier: WalletCell.identifier, for: indexPath) as! WalletCell
        cell.selectionStyle = .none
        
        if self.walletTransaction.count > 0 {
            let item = self.walletTransaction[indexPath.row]
            
            cell.walletTransaction = item
            
            let desc = item.description
            if desc.isEmpty {
                cell.descriptionImageView.isHidden = true
            }else{
                cell.descriptionImageView.addTapGestureRecognizer{
                    self.showAlert(title: LanguageManager.Description, message: desc)
                }
            }
            
            cell.popUpMenuBtn.tag = item.id
            cell.popUpMenuBtn.addTarget(self, action: #selector(onPopUpMenuPressed), for: .touchUpInside)
            
            let description = item.description
            if description.isEmpty{
                cell.descriptionImageView.isHidden = true
            }else{
                cell.descriptionImageView.addTapGestureRecognizer{
                    self.showAlert(title: LanguageManager.Description, message: description)
                }
            }
            
            if isLoading == false && indexPath.row == self.walletTransaction
                .count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                if let id = self.customerId{
                    self.presenter.getCustomerWalletDataFromServer(page: self.currentPage, id: id)
                }
            }
            
        }
        return cell
    }
    
    @objc func onDescriptionImageViewTapped(sender: UIImageView){
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopUpMenuPressed(sender: UIButton){
        
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        let id = sender.tag
        if self.walletTransaction.count > 0 {
            for item in self.walletTransaction{
                if id == item.id{
                    let saleId = item.saleId
                    if saleId == nil{
                        let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                            self.navigateToCashTransactionUpdateVC(item: item)
                        }
                        
                        let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                            self.transactionDeleteAlert(id: id)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(updateAction)
                        myActionSheet.addAction(deleteAction)
                        myActionSheet.addAction(cancelAction)
                    }else{
                        let invoiceAction = UIAlertAction(title: LanguageManager.SalesInvoice + " " + item.saleInvoice, style: UIAlertAction.Style.default) { (action) in
                            if let id = saleId{
                                self.navigateToSaleDetailsVC(iD : id )
                            }
                            
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(invoiceAction)
                        myActionSheet.addAction(cancelAction)
                    }
                    
                }
            }
            
            popOverForIpad(action: myActionSheet)
            
            self.present(myActionSheet, animated: true, completion: nil)
        }
        
    }
    
    
    func navigateToCashTransactionUpdateVC(item : WalletTransactions){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerWalletTransactionUpdateVC") as! CustomerWalletTransactionUpdateVC
        viewController.walletTransactions = item
        viewController.walletAmount = self.walltetAmount
        viewController.customerWalletVCInstance = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        viewController.iD = iD
        viewController.state = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//DatePicker
extension CustomerWalletViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isSearchValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSearchBtnTapped(sender: UIButton){
        if isSearchValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            self.walletTransaction = []
            if let id = self.customerId{
                self.presenter.getCustomerWalletSearchDataFromServer(page: self.currentPage, id: id, startDt: startDate, endDt: endDate)
            }
            
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

//Mark: Api Delegate
extension CustomerWalletViewController : CustomerWalletViewDelegate{
    func setWalletData(data: CustomerWalletDataMapper) {
        guard let wallet = data.customerWallet else {
            self.initialSetup()
            return
        }
        
        self.customerWallet = wallet
        
//        if self.customerWallet == nil{
//            self.hideDateSearch(is: true)
//        }else{
//            self.hideDateSearch(is: false)
//        }
        self.initialSetup()
        
        guard let transactions = data.transactions, transactions.count > 0 else {
            self.datePickerTextField.text = ""
            self.toDatePickerTextField.text = ""
            self.dateSearchFailedMessage(userMessage: LanguageManager.NoInformationFound)
            return
        }
        self.isLoading = false
        self.walletTransaction += transactions
        self.noInfoMessageLbl.isHidden = true
        self.tableView.isHidden = false
        
        guard let pagination = data.pagination else{
            return
        }
        
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onCashAdd(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onCashInOut(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onTransactionDelete(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onTransactionDeleteFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func onCashAddFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func onDateSearchFailed(message: String) {
        self.datePickerTextField.text = ""
        self.toDatePickerTextField.text = ""
        self.dateSearchFailedMessage(userMessage: LanguageManager.NoInformationFound)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.noInfoMessageLbl.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.noInfoMessageLbl.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.currentPage = 1
        self.isLoading = true
        self.walletTransaction = []
        if let id = self.customerId{
            self.presenter.getCustomerWalletDataFromServer(page: self.currentPage, id: id)
        }
        
    }
}

extension CustomerWalletViewController{
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.walletTransaction = []
            if let id = self.customerId{
                self.presenter.getCustomerWalletDataFromServer(page: self.currentPage, id: id)
            }
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func dateSearchFailedMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title:  userMessage, message: "", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.walletTransaction = []
            self.tableView.reloadData()
            if let id = self.customerId{
                self.presenter.getCustomerWalletDataFromServer(page: self.currentPage, id: id)
            }
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func transactionDeleteAlert(id: Int) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: LanguageManager.AreYouSure, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            let param : [String : Any] = ["id": id]
            self.presenter.postCustomerWalletTransactionDeleteDataToServer(param: param)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alertWithTextField(title: String, type: Int){
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                
                return
            }
            
            if type == 0{
                if let data = self.customerWallet{
                    if let cashOutAmount = text.toDouble(), let amount = data.amount.toDouble(){
                        if cashOutAmount > amount{
                            self.showCashInOutAlert(title: Constants.currencySymbol + " \(amount)", message: LanguageManager.YouCanNotCashOutMoreThan)
                            return
                        }
                    }
                }
            }
            
            let textField1 = alertController.textFields![1]
            let description = textField1.text ?? ""
            
            if let id = self.customerId{
                let param : [String : Any] = ["amount": textField.text!, "type": type, "description": description, "customer_id": id]
                self.presenter.postCustomerWalletCashInOutDataToServer(param: param)
            }
             
        }
        
        confirmAction.isEnabled = false
        
        alertController.addTextField { textField0 in
            textField0.placeholder = LanguageManager.Cash
            textField0.textAlignment = .center
            textField0.keyboardType = .decimalPad
            
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField0, queue: OperationQueue.main, using:
                {_ in
                    let textCount = textField0.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0
                    let textIsNotEmpty = textCount > 0
                    
                    confirmAction.isEnabled = textIsNotEmpty
                
            })
            
        }
        
        alertController.addTextField { textField1 in
            textField1.placeholder = LanguageManager.Description
            textField1.textAlignment = .center
            textField1.keyboardType = .default
            
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showCashInOutAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.alertWithTextField(title: self.alertTitle, type: self.type)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

