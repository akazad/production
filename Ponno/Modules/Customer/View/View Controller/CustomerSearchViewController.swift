//
//  CustomerSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 18/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

class CustomerSearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = CustomerSearchPresenter(service: CustomerService())
    
    var customerList : [Customers] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var timer : Timer?
    
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var searchText = ""
    var currentPage : Int = 1
    var lastPage : Int = 0
    var isLoading : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        self.setUpSearchBar()
        self.configureTableView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpInitialLanguage()
        self.setNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    func navigateToUpdateCustomerVC(customer : Customers){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerAddViewController") as! CustomerAddViewController
        viewController.customer = customer
        viewController.customerState = CustomerState.Update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.presenter.getCustomerSearchDataFromServer(page: self.currentPage, searchString: "")
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
}


//Mark: Search Delegate
extension CustomerSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.CustomerSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.customerList = []
        self.isLoading = true
        self.isSearchActive = true
        self.presenter.getCustomerSearchDataFromServer(page: self.currentPage, searchString: searchText)
    }
}

//MARK: TableView Delegate and DataSource
extension CustomerSearchViewController : UITableViewDelegate , UITableViewDataSource{
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(CustomerListCell.nib, forCellReuseIdentifier: CustomerListCell.identifier)
        self.tableView.estimatedRowHeight = 100
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.separatorColor = UIColor.clear
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.customerList.count > 0 {
            return self.customerList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CustomerListCell = tableView.dequeueReusableCell(withIdentifier: CustomerListCell.identifier, for: indexPath) as! CustomerListCell
        cell.selectionStyle = .none

        if self.customerList.count > 0 {
            let customer = self.customerList[indexPath.row]
            
            cell.customerList = customer
            
//            cell.customerIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
//            cell.customerIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.customerImage + customer.image), placeholderImage: UIImage(named: "placeholder"))
//
////            cell.customerIcon.sd_setShowActivityIndicatorView(true)
////            cell.customerIcon.sd_setIndicatorStyle(.gray)
////            cell.customerIcon.sd_setImage(with: URL(string: "https://www.ponno.co/uploads/images/customer/" + customer.image), placeholderImage: UIImage(named: "placeholder"))
//
//           // let firstAlphabet = Array(customer.name)[0]
//          //  cell.nameFirstAlphabetLabel.text = String(describing: firstAlphabet)
//            cell.nameLabel.text = "Name" + " : " +  customer.name
//            cell.phoneLabel.text = "Phone" + " : " +  customer.phone
//
//            let address = customer.address
//            if address.isEmpty{
//                cell.addressLabel.text = "Address" + " : ---"
//            }else{
//                cell.addressLabel.text = "Address" + " : " + address
//            }
            
            cell.popUpBtnTapped.tag = customer.id
            cell.popUpBtnTapped.addTarget(self, action: #selector(ononPopUpBtnTapped), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.customerList
                .count - 1 && self.currentPage < self.lastPage{
                self.isLoading = true
                self.currentPage += 1
                if isSearchActive{
                    self.presenter.getCustomerSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
                }
            }
        }
        
        return cell
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            
            if self.customerList.count > 0{
                let customer = self.customerList[indexPath.row]
                self.navigateToUpdateCustomerVC(customer: customer)
            }
        }
        
        edit.backgroundColor = UIColor.orange
        
        return [edit]
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.customerList.count > 0 {
            let customer = self.customerList[indexPath.row]
            self.navigateToCustomerDetailsVC(id: customer.id)
        }
    }
    
    @objc func ononPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.customerList.count > 0{
            let customerId = sender.tag
            for customer in self.customerList{
                if customerId == customer.id{
                    let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToUpdateCustomerVC(customer: customer)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(editAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToCustomerDetailsVC(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerDBaseViewController") as! CustomerDBaseViewController
        viewController.customerId = id
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

//Mark: Api Delegate
extension CustomerSearchViewController : CustomerSearchViewDelegate{
    func setCustomerSearchData(data: CustomerDataMapper) {
        guard let list = data.customers, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.customerList += list
     
        guard let pagination = data.pagination else {
            return
        }
        self.lastPage = pagination.lastPageNo
        
    }
    
    func onFailed(failure: String) {
        self.searchAlert(title: LanguageManager.NoInformationFound, message: failure)
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getCustomerSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
    }
}
