//
//  CustomerDetailsViewController.swift
//  Ponno
//
//  Created by a k azad on 9/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SVProgressHUD
import Floaty

class CustomerDetailsViewController: UIViewController{
    
    @IBOutlet weak var emptyMessage: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet var panView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    
    var isFirst = true
    var selectedSegment : Int?
    
    private var presenter = CustomerViewPresenter(service: CustomerService())
    var customerId : Int?
    
    var purchaseHistory : [CustomerPurchaseHistory] = []
    var dueHistory : [CustomerDueHistory] = []
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    
    var lastY: CGFloat = 0
    var pan: UIPanGestureRecognizer!
    
    var bottomSheetDelegate: BottomSheetDelegate?
    var parentView: UIView!
    
    var initalFrame: CGRect!
    var topY: CGFloat = 80 //change this in viewWillAppear for top position
    var middleY: CGFloat = 400 //change this in viewWillAppear to decide if animate to top or bottom
    var bottomY: CGFloat = 600 //no need to change this
    let bottomOffset: CGFloat = 80 //sheet height on bottom position
    var lastLevel: SheetLevel = .bottom //choose inital position of the sheet
    
    var disableTableScroll = false
    
    //hack panOffset To prevent jump when goes from top to down
    var panOffset: CGFloat = 0
    var applyPanOffset = false
    
    //tableview variables
    var listItems: [Any] = []
    var headerItems: [Any] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        self.setUpPan()
        self.configureTableView()
    }
    
    func setUpPan(){
        pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        pan.delegate = self
        self.panView.addGestureRecognizer(pan)
        
        self.tableView.panGestureRecognizer.addTarget(self, action: #selector(handlePan(_:)))
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleTap(_:)))
        tap.delegate = self
        tableView.addGestureRecognizer(tap)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initalFrame = UIScreen.main.bounds
        self.topY = round(initalFrame.height * 0.05)
        self.middleY = initalFrame.height * 0.6
        self.bottomY = initalFrame.height - bottomOffset
        self.lastY = self.middleY
        
        bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.middleY))
    }
    
    func navigateToCustomerAddViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerAddViewController") as! CustomerAddViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func segmentedControllAction(_ sender: UISegmentedControl) {
        
        guard let id = self.customerId else{
            return
        }
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            self.selectedSegment = 0
            self.dueHistory = []
            self.isLoading = true
            self.currentPage = 1
            self.isFirst = true
            self.tableView.reloadData()
            self.presenter.getCustomerDueHistoryDataFromServer(page: self.currentPage, id: id)
        case 1:
            self.selectedSegment = 1
            self.purchaseHistory = []
            self.isLoading = true
            self.currentPage = 1
            self.isFirst = false
            self.tableView.reloadData()
            self.presenter.getCustomerPurchaseHistoryDataFromServer(page: self.currentPage, id: id)
        default:
            break;
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == tableView else {return}
        
        if (self.parentView.frame.minY > topY){
            self.tableView.contentOffset.y = 0
        }
    }
    
    
    //this stops unintended tableview scrolling while animating to top
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard scrollView == tableView else {return}
        
        if disableTableScroll{
            targetContentOffset.pointee = scrollView.contentOffset
            disableTableScroll = false
        }
    }
    
    
    @objc func handleTap(_ recognizer: UITapGestureRecognizer){
        let p = recognizer.location(in: self.tableView)
        let index = tableView.indexPathForRow(at: p)
        
        if selectedSegment == 1{
            if self.purchaseHistory.count > 0 {
                guard let purchaseIndex = index else {
                    return
                }
                let purchaseItem = self.purchaseHistory[purchaseIndex.row]
                self.navigateToPurchaseDetailsVC(selectedId: purchaseItem.id)
            }
        }
        tableView.selectRow(at: index, animated: false, scrollPosition: .none)
    }
    
    func navigateToPurchaseDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let saleDetailsVC = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        saleDetailsVC.iD = selectedId 
        self.navigationController?.pushViewController(saleDetailsVC, animated: true)
    }
    
    
    @objc func handlePan(_ recognizer: UIPanGestureRecognizer){
        
        let dy = recognizer.translation(in: self.parentView).y
        switch recognizer.state {
        case .began:
            applyPanOffset = (self.tableView.contentOffset.y > 0)
        case .changed:
            if self.tableView.contentOffset.y > 0{
                panOffset = dy
                return
            }
            
            if self.tableView.contentOffset.y <= 0{
                if !applyPanOffset{panOffset = 0}
                let maxY = max(topY, lastY + dy - panOffset)
                let y = min(bottomY, maxY)
                //                self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: y)
                bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: y))
            }
            
            if self.parentView.frame.minY > topY{
                self.tableView.contentOffset.y = 0
            }
        case .failed, .ended, .cancelled:
            panOffset = 0
            
            if (self.tableView.contentOffset.y > 0){
                return
            }
            
            self.panView.isUserInteractionEnabled = false
            
            self.disableTableScroll = self.lastLevel != .top
            
            self.lastY = self.parentView.frame.minY
            self.lastLevel = self.nextLevel(recognizer: recognizer)
            
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.9, options: .curveEaseOut, animations: {
                
                switch self.lastLevel{
                case .top:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.topY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.topY))
                    self.tableView.contentInset.bottom = 50
                case .middle:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.middleY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.middleY))
                case .bottom:
                    //                    self.panView.frame = self.initalFrame.offsetBy(dx: 0, dy: self.bottomY)
                    self.bottomSheetDelegate?.updateBottomSheet(frame: self.initalFrame.offsetBy(dx: 0, dy: self.bottomY))
                }
                
            }) { (_) in
                self.panView.isUserInteractionEnabled = true
                self.lastY = self.parentView.frame.minY
            }
        default:
            break
        }
    }
    
    func nextLevel(recognizer: UIPanGestureRecognizer) -> SheetLevel{
        let y = self.lastY
        let velY = recognizer.velocity(in: self.view).y
        if velY < -200{
            return y > middleY ? .middle : .top
        }else if velY > 200{
            return y < (middleY + 1) ? .middle : .bottom
        }else{
            if y > middleY {
                return (y - middleY) < (bottomY - y) ? .middle : .bottom
            }else{
                return (y - topY) < (middleY - y) ? .top : .middle
            }
        }
    }
}


//Mark: TableView Delegate And Data Source
extension CustomerDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = .clear
        self.tableView.register(DueHistoryCell.nib, forCellReuseIdentifier: DueHistoryCell.identifier)
        self.tableView.register(PurchaseHistoryCell.nib, forCellReuseIdentifier: PurchaseHistoryCell.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.emptyMessage.isHidden = true
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFirst{
            if self.dueHistory.count > 0 {
                self.emptyMessage.isHidden = true
                return self.dueHistory.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        } else{
            if self.purchaseHistory.count > 0 {
                self.emptyMessage.isHidden = true
                return self.purchaseHistory.count
            }
            else{
                self.emptyMessage.isHidden = false
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFirst{
            let cell : DueHistoryCell = tableView.dequeueReusableCell(withIdentifier: DueHistoryCell.identifier, for: indexPath) as! DueHistoryCell
            cell.selectionStyle = .none
            if self.dueHistory.count > 0 {
                let item = self.dueHistory[indexPath.row]
                cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.dateLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.amountLabel.text = "পরিমাণ: " + "\(item.amount )" + " " + Constants.pcsSymbol
                cell.sellerNameLabel.text = "বিক্রেতা: " + item.sellerName
//                cell.amountLabel.text = item
                if item.type == 0 {
                    cell.typeLabel.text = "দেনা"
                    cell.typeLabel.textColor = UIColor(red: CGFloat(220/255.0), green: CGFloat(53/255.0), blue: CGFloat(69/255.0), alpha: CGFloat(1.0))
                    cell.colorView.backgroundColor = UIColor(red: CGFloat(255/255.0), green: CGFloat(225/255.0), blue: CGFloat(228/255.0), alpha: CGFloat(1.0))
                }
                if item.type == 1 {
                    cell.typeLabel.text = "পূরণ"
                    cell.typeLabel.textColor = UIColor(red: CGFloat(91/255.0), green: CGFloat(160/255.0), blue: CGFloat(62/255.0), alpha: CGFloat(1.0))
                    cell.colorView.backgroundColor = UIColor(red: CGFloat(241/255.0), green: CGFloat(255/255.0), blue: CGFloat(222/255.0), alpha: CGFloat(1.0))
                }
                if isLoading == false && indexPath.row == self.dueHistory.count - 1 {
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.customerId else{
                        return cell
                    }
                    self.presenter.getCustomerDueHistoryDataFromServer(page: self.currentPage, id: id)
                }
            }
             return cell
        }else{
            let cell : PurchaseHistoryCell = tableView.dequeueReusableCell(withIdentifier: PurchaseHistoryCell.identifier, for: indexPath) as! PurchaseHistoryCell
            cell.selectionStyle = .none
            if self.purchaseHistory.count > 0 {
                let item = self.purchaseHistory[indexPath.row]
                cell.dateLabel.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd_c_HH_mm_ss.rawValue)
                cell.dateLabel.textColor = UIColor(red: 56/255, green: 173/255, blue: 82/255, alpha: 1/0)
                cell.invoiceLabel.text = item.invoice
                cell.totalLabel.text = "সর্বমোট মূল্য : " + "\(item.total)" + " " + Constants.currencySymbol
                cell.paidLabel.text = "পরিশোধ মূল্য :" + "\(item.paid)" + " " + Constants.currencySymbol
                cell.dueLabel.text = "বাকি : " + "\(item.due)" + " " + Constants.currencySymbol
                if isLoading == false && indexPath.row == self.purchaseHistory.count - 1 {
                    self.isLoading = true
                    self.currentPage += 1
                    guard let id = self.customerId else{
                        return cell
                    }
                    self.presenter.getCustomerPurchaseHistoryDataFromServer(page: self.currentPage, id: id)
                }
            }
            return cell
        }
//        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let saleDetailsVC = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        saleDetailsVC.iD = iD
        self.navigationController?.pushViewController(saleDetailsVC, animated: true)
    }
}


extension CustomerDetailsViewController: UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

//Mark: Api Delegate
extension CustomerDetailsViewController : CustomerDetailsViewDelegate{
    func setCustomerDueHistoryData(data: CustomerDueHistoryDataMapper) {
        guard let dueList = data.dues, dueList.count > 0 else{
            return
        }
        self.isLoading = false
        self.dueHistory += dueList
        self.refreshTableView()
    }
    
    func setCustomerPurchaseHistoryData(data: CustomerPurchaseHistoryDataMapper) {
        guard let purchaseList = data.purchases, purchaseList.count > 0 else{
            return
        }
        self.isLoading = false
        self.purchaseHistory += purchaseList
        self.refreshTableView()
    }
    
    func setCustomerViewData(data: CustomerViewDataMapper) {
       
        
    }
    
    func onFailed() {
        self.isLoading = true
    }
    
    func showLoading() {
//        self.tableView.isHidden = true
        SVProgressHUD.show()
    }
    
    func hideLoading() {
//        self.tableView.isHidden = false
        SVProgressHUD.dismiss()
    }

    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.customerId else{
            return
        }
        self.presenter.getCustomerDueHistoryDataFromServer(page: self.currentPage, id: id)
    }
}

