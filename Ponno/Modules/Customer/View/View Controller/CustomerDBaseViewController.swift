//
//  CustomerDBaseViewController.swift
//  Ponno
//
//  Created by a k azad on 29/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import FloatingPanel
import SDWebImage

protocol NavigateToAdjustAmount {
    func navigateTo(id: Int)
}

protocol NavigateToWallet {
    func navigateToWallet(id: Int)
}

class CustomerDBaseViewController: UIViewController, FloatingPanelControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var fpc: FloatingPanelController!
    var bottomShetVC : CustomerDViewController!
    
    enum Sections : Int{
        case CustomerInfo
        case CustomerSummary
    }
    
    var customerData : CustomerData?
    var customerDetails : CustomerViewDataMapper?
    
    var customerId : Int?
    
    private var presenter = CustomerViewPresenter(service: CustomerService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addBottomSheet()
        self.initialSetup()
        self.setBarBtn()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func addBottomSheet(){
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = UIColor.lightGreen.withAlphaComponent(0.5)
        fpc.surfaceView.layer.cornerRadius = 9.0
        fpc.surfaceView.clipsToBounds = true
        
        fpc.surfaceView.shadowHidden = false
        bottomShetVC = storyboard?.instantiateViewController(withIdentifier: "CustomerDViewController") as? CustomerDViewController
        bottomShetVC.customerId = self.customerId
        
        
        fpc.set(contentViewController: bottomShetVC)
        fpc.track(scrollView: bottomShetVC.tableView)
        fpc.addPanel(toParent: self)
    }
    
    func removeBottomSheet(){
        fpc.removePanelFromParent(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeBottomSheet()
    }
    
    func initialSetup(){
        self.title = LanguageManager.CustomerDetails
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setBarBtn(){
        let popUpBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        popUpBtn.setImage(UIImage(named: "popupmenudark"), for: UIControl.State.normal)
        popUpBtn.addTarget(self, action: #selector(onBarPopUpBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        popUpBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        popUpBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        popUpBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn3 = UIBarButtonItem(customView: popUpBtn)
        
        if let category = getBusinessCategory(){
            if category.businessCategory == 1{
                navigationItem.setRightBarButtonItems([], animated: true)
            }else{
                navigationItem.setRightBarButtonItems([barBtn3], animated: true)
            }
        }
        
        
    }
    
    @objc func onBarPopUpBtnTapped(sender: UIButton){
        self.popOverToDBasePopoverViewController(sender: sender)
    }
    
    
    func popOverToDBasePopoverViewController(sender: UIButton){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "DBaseViewPopoverViewController") as? DBaseViewPopoverViewController{
            controller.preferredContentSize = CGSize(width: 170, height: 120)
            controller.popedOverFrom = PopedOverFrom.customer.rawValue
            controller.customerId = self.customerId
            controller.navigate = self
            controller.navigateTo = self
            showPopup(controller, sourceView: sender)
        }
        
    }
    
}



//MARK: TableViewDelegate And DataSource
extension CustomerDBaseViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(CustomerDetailsInfoCell.nib, forCellReuseIdentifier: CustomerDetailsInfoCell.identifier)
        self.tableView.register(CustomerDetailsSummaryCell.nib, forCellReuseIdentifier: CustomerDetailsSummaryCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.CustomerInfo.rawValue:
            return 1
        case Sections.CustomerSummary.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case Sections.CustomerInfo.rawValue:
            let cell : CustomerDetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: CustomerDetailsInfoCell.identifier, for: indexPath) as! CustomerDetailsInfoCell
            cell.selectionStyle = .none
            guard let customer = self.customerData else{
                return cell
            }
            cell.nameLabel.text = customer.name
            if customer.image.isEmpty{
                cell.imageIcon.image = UIImage(named: "placeholder")
            }else{
                cell.imageIcon.image = UIImage(named: customer.image)
            }
            
            cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.customerImage + customer.image), placeholderImage: UIImage(named: "placeholder"))
            cell.phoneLabel.text = LanguageManager.Phone + " : " + customer.phone
            
            let address = customer.address
            if address.isEmpty{
                cell.AddressLabel.text = LanguageManager.Address + " : ---"
            }else{
                cell.AddressLabel.text = LanguageManager.Address + " : " +  customer.address
            }
            
            self.customerId = Int(customer.id)
            let currentDue = customer.currentDue
            if currentDue <= 0 {
                cell.duePaidBtn.isHidden = true
            }else{
                cell.duePaidBtn.isHidden = false
                cell.duePaidBtn.addTarget(self, action: #selector(onDuePaidBtnPressed), for: .touchUpInside)
            }
            
            
            
            return cell
            
        case Sections.CustomerSummary.rawValue:
            let cell : CustomerDetailsSummaryCell = tableView.dequeueReusableCell(withIdentifier: CustomerDetailsSummaryCell.identifier, for: indexPath) as! CustomerDetailsSummaryCell
            cell.selectionStyle = .none
            if let customer = self.customerData {
                let totalPurchase = customer.totalPurchase
                if totalPurchase <= 0  {
                    cell.totalPurchaseLabel.text = String(describing: totalPurchase)
                }else{
                    cell.totalPurchaseLabel.text = String(describing: customer.totalPurchase)
                }
                
                let currentDue = customer.currentDue
                if currentDue <= 0.00 {
                    cell.currentDueLabel.text = String(describing:currentDue) + " " + Constants.currencySymbol
                }else{
                    cell.currentDueLabel.text = String(describing:customer.currentDue) + " " + Constants.currencySymbol
                }
                
                let totalDue = customer.totalDue
                if totalDue <= 0.00 {
                    cell.totalDueLabel.text = String(describing: totalDue) + " " + Constants.currencySymbol
                }else{
                    cell.totalDueLabel.text = String(describing: customer.totalDue) + " " + Constants.currencySymbol
                }
                
                let totalPaid = customer.totalPaid
                if totalPaid <= 0.00{
                    cell.totalDuePaidLabel.text = String(describing: totalPaid) + " " + Constants.currencySymbol
                }else{
                    cell.totalDuePaidLabel.text = String(describing: customer.totalPaid) + " " + Constants.currencySymbol
                }
                
            }
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.CustomerInfo.rawValue:
            return UITableView.automaticDimension
        case Sections.CustomerSummary.rawValue:
            return 140.0
        default:
            return 0
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onDuePaidBtnPressed(sender: UIButton){
        self.navigateToCustomerDuePaidVC()
    }
    
    func navigateToCustomerDuePaidVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerDuePaidViewController") as! CustomerDuePaidViewController
        viewController.customerData = customerData
        viewController.customerId = self.customerId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//Mark: Api Delegate
extension CustomerDBaseViewController : CustomerDetailsViewDelegate{
    func setCustomerViewData(data: CustomerViewDataMapper) {
        self.customerData = data.customer
        self.refreshTableView()
    }
    
    func setCustomerDueHistoryData(data: CustomerDueHistoryDataMapper) {
        
    }
    
    func setCustomerPurchaseHistoryData(data: CustomerPurchaseHistoryDataMapper) {
        
    }
    
    func onFailed() {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.customerId else{
            return
        }
        self.presenter.getCustomerDataFromServer(id: id)
    }
}

extension CustomerDBaseViewController : NavigateToAdjustAmount{
    func navigateTo(id: Int) {
        navigateToDueAdjustViewController(id: id)
    }
    
    func navigateToDueAdjustViewController(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DueAdjustViewController") as! DueAdjustViewController
        viewController.customerId = id
        viewController.customer = self.customerData
        viewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension CustomerDBaseViewController : NavigateToWallet{
    func navigateToWallet(id: Int) {
        navigateToCustomerWalletViewController(id: id)
    }

    func navigateToCustomerWalletViewController(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerWalletViewController") as! CustomerWalletViewController
        viewController.customerId = id
        //viewController.customer = self.customerData
        viewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
