//
//  CustomerViewController.swift
//  Ponno
//
//  Created by a k azad on 27/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

class CustomerViewController: BaseViewController {

    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    
    private var presenter = CustomerPresenter(service: CustomerService())
    
    var customerList : [Customers] = []{
        didSet{
            self.refreshTableView()
        }
    }
    private var customerSummary : [CustomerSummary]?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
//    SearchBar
    var isSearchActive = false{
        didSet{
             self.refreshTableView()
        }
    }
    var filteredList : [Customers] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isLoading: Bool = false
    var currentPage: Int = 1
    var lastPageNo: Int = 0
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.setNavigationBarTitle()
        self.configureTableView()
        self.configureCollectionView()
//        self.setUpSearchBar()
        self.addFloaty()
        self.setBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.clearCachedImages()
        self.setUpInitialLanguage()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.resetData()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.CustomerBook
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0))]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func resetData(){
        self.customerList = []
    }
    
    //New CustomerAdd NavBarButton
    @IBAction func newCustomerAdd(_ sender: UIBarButtonItem) {
        
    }
    
    func navigateToUpdateCustomerVC(customer : Customers){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerAddViewController") as! CustomerAddViewController
        viewController.customer = customer
        viewController.customerState = CustomerState.Update.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToCustomerAddViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerAddViewController") as! CustomerAddViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        
//        let extraItem = FloatyItem()
//        extraItem.icon = UIImage(named: "next")
//        extraItem.buttonColor = UIColor.lightGreen
//        extraItem.title = "নতুন কাস্টমার"
//        extraItem.handler = { item in
//            self.navigateToCustomerAddViewController()
//        }
        
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(CustomerViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.currentPage = 1
        self.customerList = []
        self.presenter.getCustomerDataFromServer(page: self.currentPage)
        refreshControl.endRefreshing()
    }
    
    func setBarButton(){
        
        let searchBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        searchBtn.setImage(UIImage(named: "search"), for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        searchBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton1 = UIBarButtonItem(customView: searchBtn)
        
        let customerAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        customerAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        customerAddBtn.addTarget(self, action: #selector(onCustomerAdd(sender:)), for: UIControl.Event.touchUpInside)
        customerAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        customerAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        customerAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: customerAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2,barButton1], animated: true)
    }
    
    @objc func onSearch(sender: UIButton){
        self.navigateToCustomerSearchVC()
    }
    
    @objc func onCustomerAdd(sender: UIButton){
        self.navigateToCustomerAddViewController()
    }
    
    func navigateToCustomerSearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerSearchViewController") as! CustomerSearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

//Mark: Floaty Delegate
extension CustomerViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}

////MARK: SearchBar Delegate
//extension CustomerViewController: UISearchBarDelegate{
//    fileprivate func setUpSearchBar(){
//        self.searchBar.delegate = self
//    }
//
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        self.view.endEditing(true)
//
//        isSearchActive = false
//    }
//
//
//}


//MARK: CollectionView Delegate And DataSource
extension CustomerViewController : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(CustomerSummaryCell.nib, forCellWithReuseIdentifier: CustomerSummaryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let customerItem = self.customerSummary, customerItem.count > 0 else{
            return 0
        }
        return customerItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CustomerSummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomerSummaryCell.identifier, for: indexPath) as! CustomerSummaryCell
        guard let customerSummaryList = self.customerSummary, customerSummaryList.count > 0 else{
            return cell
        }
        let summaryItem = customerSummaryList[indexPath.row]
        
//        for (key, _) in customerSummaryList.enumerated() {
//            if key == 0 {
//                customerSummaryList[0].title = "Total Customer Number"
//            }else if key == 1 {
//                customerSummaryList[1].title = "Current Due"
//            }else if key == 2 {
//                customerSummaryList[2].title = "Total Due Withdraw"
//            }
//        }
        
        cell.titleLabel.text = summaryItem.title
        cell.valueLabel.text = summaryItem.value
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 60.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.customerSummary, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//MARK: TableView Delegate and DataSource
extension CustomerViewController : UITableViewDelegate , UITableViewDataSource{
    private func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(CustomerListCell.nib, forCellReuseIdentifier: CustomerListCell.identifier)
        self.tableView.estimatedRowHeight = 100.0
        self.tableView.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorStyle = .none
        self.tableView.addSubview(refreshControl)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive{
            if self.filteredList.count > 0 {
                return self.filteredList.count
            }
            else{
                return 0
            }
        }
        else{
            if self.customerList.count > 0 {
                return self.customerList.count
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CustomerListCell = tableView.dequeueReusableCell(withIdentifier: CustomerListCell.identifier, for: indexPath) as! CustomerListCell
        cell.selectionStyle = .none
        
        if isSearchActive{
            if self.filteredList.count > 0 {
                let customer = self.filteredList[indexPath.row]
                
                cell.customerList = customer
                
//                cell.customerIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
//                //cell.customerIcon.sd_setImage(with: URL(string: "https://www.beta.ponno.co/uploads/images/customer/" + customer.image), placeholderImage: UIImage(named: "placeholder"))
//
//                cell.customerIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.customerImage + customer.image), placeholderImage: UIImage(named: "placeholder"))
//
//
//                cell.nameLabel.text = LanguageManager.Name + " : " +  customer.name
//                cell.phoneLabel.text = LanguageManager.Phone + " : " + customer.phone
//
//                let address = customer.address
//                if address.isEmpty{
//                    cell.addressLabel.text = LanguageManager.Address + " : ---"
//                }else{
//                    cell.addressLabel.text = LanguageManager.Address + " : " + address
//                }
                
                cell.popUpBtnTapped.tag = customer.id
                cell.popUpBtnTapped.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
                
                if isLoading == false && indexPath.row == self.filteredList
                    .count - 1 && self.currentPage < self.lastPageNo{
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getCustomerDataFromServer(page: self.currentPage)
                }
            }
        }
        else{
            if self.customerList.count > 0 {
                let customer = self.customerList[indexPath.row]
                
                cell.customerList = customer
                
//                cell.customerIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
//                cell.customerIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.customerImage + customer.image), placeholderImage: UIImage(named: "placeholder"))
//
//                cell.nameLabel.text = "Name" + " : " +  customer.name
//                cell.phoneLabel.text = "Phone" + " : " +  customer.phone
//
//                let address = customer.address
//                if address.isEmpty{
//                    cell.addressLabel.text = "Address" + " : ---"
//                }else{
//                    cell.addressLabel.text = "Address" + " : " + address
//                }
                
                cell.popUpBtnTapped.tag = customer.id
                cell.popUpBtnTapped.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
                
                if isLoading == false && indexPath.row == self.customerList
                    .count - 1 && self.currentPage < self.lastPageNo{
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getCustomerDataFromServer(page: self.currentPage)
                }
            }
        }
        return cell
    }
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            
            if self.isSearchActive{
                if self.filteredList.count > 0 {
                    let customer = self.filteredList[indexPath.row]
                    self.navigateToUpdateCustomerVC(customer: customer)
                }
            }
            
            if self.customerList.count > 0{
                let customer = self.customerList[indexPath.row]
                self.navigateToUpdateCustomerVC(customer: customer)
            }
        }
        
        edit.backgroundColor = UIColor.orange
        
        return [edit]
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearchActive{
            if self.filteredList.count > 0 {
                let customer = self.filteredList[indexPath.row]
                
                self.navigateToCustomerDBaseViewController(id: customer.id)
            }
        }
        else{
            if self.customerList.count > 0 {
                let customer = self.customerList[indexPath.row]
                self.navigateToCustomerDBaseViewController(id: customer.id)
            }
        }
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.customerList.count > 0{
            let customerId = sender.tag
            for customer in self.customerList{
                if customerId == customer.id{
                    let editAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.navigateToUpdateCustomerVC(customer: customer)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    myActionSheet.addAction(editAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    //Old Details
    func navigateToCustomerDetailsVC(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerDetailsBaseViewController") as! CustomerDetailsBaseViewController
        viewController.customerId = id
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    //New Details
    func navigateToCustomerDBaseViewController(id: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerDBaseViewController") as! CustomerDBaseViewController
        viewController.customerId = id
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
}

// MARK: API Delegate
extension CustomerViewController : CustomerViewDelegate{
    func setCustomerData(data: CustomerDataMapper) {
        guard let customerSummaryItem = data.summary, customerSummaryItem.count > 0 else {
            return
        }
        self.customerSummary = customerSummaryItem
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        guard let list = data.customers, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.customerList += list
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(message : String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.currentPage = 1
        self.customerList = []
        self.presenter.getCustomerDataFromServer(page: self.currentPage)
    }
}
