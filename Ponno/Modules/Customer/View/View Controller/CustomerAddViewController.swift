//
//  CustomerAddViewController.swift
//  Ponno
//
//  Created by a k azad on 5/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

enum CustomerState : Int {
    case Add
    case Update
    case DueCustomerUpdate
}

class CustomerAddViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imageUiView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var customerName: UITextField!{
        didSet{
            customerName.placeholder = LanguageManager.CustomerName
        }
    }
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!{
        didSet{
            phoneTextField.placeholder = LanguageManager.PhoneNumber
        }
    }
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressTextField: UITextField!{
        didSet{
            addressTextField.placeholder = LanguageManager.Address
        }
    }
    @IBOutlet weak var submitButtonLabel: UIButton!
    
    var customer : Customers?
    var customerId : Int?
    var dueCustomer : DueCustomerList?
    var customerState = CustomerState.Add.rawValue
    
    private var presenter = AddNewCustomerPresenter(service : CustomerService())
    
    let imagePicker = UIImagePickerController()
    
    var selectedImage : UIImage?
    var message : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setAddNewCustomerTitles()
        self.configureTextFields()
        self.initialSetup()
        self.setImageView()
        self.attachPresenter()
    }
    
    func initialSetup(){
        
        if self.customerState == CustomerState.Add.rawValue{
            self.title = LanguageManager.NewCustomerAdd
        }else if self.customerState == CustomerState.Update.rawValue{
            self.title = LanguageManager.UpdateCustomerInfo
            guard let data = customer else{
                return
            }
            self.customerName.text = data.name
            self.phoneTextField.text = data.phone
            self.addressTextField.text = data.address
            self.customerId = data.id
            
        }else{
            self.title = LanguageManager.CustomerDueUpdate
            guard let data = self.dueCustomer else{
                return
            }
            self.customerName.text = data.name
            self.addressTextField.text = data.address
            self.phoneTextField.text = data.phone
        }
        
        imagePicker.delegate = self
        self.imageView.addTapGestureRecognizer(action: {self.onTapOnImageView()})
    }
    
    private func setImageView(){
        guard let imageUrl = self.customer?.image else{
            self.setImageInImageView(imageUrl: "")
            return
        }
        
        self.setImageInImageView(imageUrl: ImageUrl.sharedImageInstance.customerImage + imageUrl)
    }
    
    func setImageInImageView(imageUrl : String){
        self.imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
    }
    
    func onTapOnImageView(){
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }

    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.contentMode = .scaleAspectFill
//            if pickedImage.imageOrientation.rawValue == .l
            imageView.image = pickedImage
            //pickedImage.sd_rotatedImage(withAngle: <#T##CGFloat#>, fitSize: <#T##Bool#>)
//            imageView.image?.sd_rotatedImage(withAngle: 90, fitSize: true)
            
            self.selectedImage = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButton(_ sender: UIButton) {
        if self.isValidated(){
            guard let name = customerName.text, let phone = phoneTextField.text else {
                return
            }
            
            let address = addressTextField.text ?? ""
            
            var params : [String: String] = ["name": name, "phone": phone, "address": address]
            
            if self.customerState == CustomerState.Add.rawValue{
                self.presenter.getAddNewCustomerDataFromServer(customerDataArray: params)
            }
            else if self.customerState == CustomerState.Update.rawValue{
                if let id = self.customerId{
                    params["id"] = "\(id)"
                }
                self.presenter.updateCustomerData(customerData: params)
            }
            else{
                if let id = self.customerId{
                    params["id"] = "\(id)"
                }
                self.presenter.updateCustomerData(customerData: params)
            }
        }
    }
    
    func setAddNewCustomerTitles(){
        self.setUpToolBar()
        self.nameLabel.text = LanguageManager.CustomerName
        self.addressLabel.text = LanguageManager.Address
        self.phoneLabel.text = LanguageManager.PhoneNumber
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitButtonLabel.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
        
        if (self.customerName.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.NameIsRequired)
            return false
        }
        guard let phone = phoneTextField.text else {
            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
            return false
        }
//        if (!validatePhoneNumber(value: phone)){
//            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
//            return false
//        }
//        else if(self.addressTextField.text?.isEmpty)!{
//            self.displayMessage(userMessage: LanguageManager.CustomerAddressIsRequired)
//            return false
//        }
        return true
    }
    
//    func isValidPhone(phone: String) -> Bool {
//
//        let phoneRegex = "^[0-9]{6,14}$";
//        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
//        return valid
//    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.navigateToCustomerViewController()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func navigateToCustomerViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerViewController") as! CustomerViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func setUpToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:))) 
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.phoneTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        self.phoneTextField.resignFirstResponder()
        self.addressTextField.becomeFirstResponder()
    }

}

//Mark: TextField Delegate
extension CustomerAddViewController : UITextFieldDelegate{
    
    fileprivate func configureTextFields(){
        self.customerName.delegate = self
        self.phoneTextField.delegate = self
        self.addressTextField.delegate = self
        self.customerName.underlined()
        self.phoneTextField.underlined()
        self.addressTextField.underlined()
//        self.customerName.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.customerName{
            self.phoneTextField.becomeFirstResponder()
        }
        else{
            self.view.endEditing(true)
        }
        return false
    }
}

//Mark: API Delegate
extension CustomerAddViewController: AddNewCustomerViewDelegate{
    func onFailed(data: String) {
        self.displayMessage(userMessage: data)
    }
    
    func onImageUpload(data : AddDataMapper){
        self.successMessage(userMessage: self.message ?? "")
    }
    
    func updateCustomerData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.message = message
        guard let id = self.customerId, let image = self.selectedImage else{
            self.successMessage(userMessage: message)
            return
        }
        self.presenter.uploadImage(customerID : id, image: image)
    }
    
    func setAddNewCustomerData(customerData: CustomerAddDataMapper) {
        guard let message = customerData.message else{
            //self.showAlert(title: message, message: "")
            return
        }
        self.message = message
        
        guard let image = self.selectedImage, let customerID = customerData.customer?.id else{
            self.successMessage(userMessage: message)
            return
        }
        self.presenter.uploadImage(customerID : customerID, image: image)
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}
