//
//  CustomerDuePaidViewController.swift
//  Ponno
//
//  Created by a k azad on 7/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class CustomerDuePaidViewController: UIViewController {

    @IBOutlet weak var customerNameLbl: UILabel!{
        didSet{
            customerNameLbl.text = LanguageManager.CustomerName
        }
    }
    @IBOutlet weak var customerName: UITextField!
    @IBOutlet weak var currentDueLbl: UILabel!{
        didSet{
            currentDueLbl.text = LanguageManager.CurrentDue
        }
    }
    @IBOutlet weak var currentDue: UITextField!
    @IBOutlet weak var duePaidLbl: UILabel!{
        didSet{
            duePaidLbl.text = LanguageManager.DuePaid
        }
    }
    @IBOutlet weak var duePaid: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!{
        didSet{
            descriptionLbl.text = LanguageManager.Description
        }
    }
    @IBOutlet weak var details: UITextView!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = CustomerDuePaidPresenter(service: DueService())
    
    var customerData : CustomerData?
    var customerDetails : CustomerViewDataMapper?
    var customerId : Int?
    var ducCustomerData : DueCustomerList?
    var responseErrors: ResponseErrors?
    var currentDueAmount : Double?
    var dueAmount : Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetUp()
        self.setUpInitialLanguage()
        self.toolBarSetUp()
        self.setUpViews()
        self.attachPresenter()
    }
    
    func initialSetUp(){
        self.title = LanguageManager.DuePaid
        guard let data = self.customerData else {
            return
        }
        self.customerName.text = data.name
        self.customerName.isEnabled = false
        self.customerId = Int(data.id)
        self.currentDue.text = "\(data.currentDue)"
        self.currentDueAmount = data.currentDue
        self.currentDue.isEnabled = false
    }
    
    func toolBarSetUp(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.duePaid.inputAccessoryView = toolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        if let duePaid = self.duePaid.text, let duePaidAmount = Double(duePaid), let currentDue = self.currentDueAmount {
            if currentDue < duePaidAmount{
                self.duePaid.text = "\(currentDue)"
            }
        }
        self.duePaid.resignFirstResponder()
        self.details.becomeFirstResponder()
    }
    
    func setUpViews(){
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtn(sender:)), for: .touchUpInside)
    }
    
    @objc func onSubmitBtn(sender : UIButton){
        if self.isValidated(){
            
            guard let paid = duePaid.text, let id = self.customerId else{
                return
            }
            
            let details = self.details.text ?? ""
            
            let params : [String : Any] = ["customer_id": id,
                                           "type": 1,
                                           "amount" : paid,
                                           "description" : details]
            
            if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }
            
            self.presenter.postCustomerDuePaidData(paidData: params)
        }
        
    }
    
    func isValidated()->Bool{
        if self.duePaid.text == "" {
            showAlert(title: LanguageManager.DuePaymentIsRequired, message: "")
            return false
        }
        return true
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }

}


extension CustomerDuePaidViewController: CustomerDuePaidViewDelegate{
    func onSuccess(data: ResponseDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
    }
    
    func onFailed(data: String) { 
//        guard let message = data.errors else {
//            return
//        }
//        self.responseErrors = message
        //print(self.responseErrors)
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}
