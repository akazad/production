//
//  DueAdjustUpdateViewController.swift
//  Ponno
//
//  Created by a k azad on 27/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

enum Type{
    case add
    case update
}

class DueAdjustUpdateViewController: UIViewController {
    
    @IBOutlet weak var customerNameLbl: UILabel!
    @IBOutlet weak var customerNameTextField: UITextField!
    @IBOutlet weak var currentDueLbl: UILabel!
    @IBOutlet weak var currentDueTextField: UITextField!
    @IBOutlet weak var dueAdjustAmountLbl: UILabel!
    @IBOutlet weak var dueAdjustAmountTextField: UITextField!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptonTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate var presenter = DueAdjustUpdatePresenter(service: CustomerService())
    
    var type = Type.add
    var dueAdjust : DueAdjustList?
    var customerId: Int?
    var customer: CustomerData?
    var dueAdjustId : Int?
    var currentDue : Double?
    var dueAdjustAmount : Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.configureTextFields()
        self.toolbarSetup()
        self.attachPresenter()
    }
    
    func initialSetup(){
        self.setUpInitialLanguage()
        self.customerNameLbl.text = LanguageManager.CustomerName
        self.currentDueLbl.text = LanguageManager.CurrentDue
        self.dueAdjustAmountLbl.text = LanguageManager.DueAdjustAmount
        self.descriptionLbl.text = LanguageManager.Description
        self.customerNameTextField.isEnabled = false
        self.currentDueTextField.isEnabled = false
        if let data = self.customer{
            self.customerNameTextField.text = data.name
            self.customerId = data.id
        }
        
        if let cDue = self.currentDue{
            self.currentDueTextField.text = "\(cDue)"

        }
        
        if type == Type.add{
            self.title = LanguageManager.NewDueAdjust

        }else{
            self.title = LanguageManager.DueAdjustUpdate
            if let data = dueAdjust{
                self.dueAdjustAmountTextField.text = data.amount
                if let adjustAmount = data.amount?.toDouble(){
                    self.dueAdjustAmount = adjustAmount
                }
                self.descriptonTextField.text = data.description
                self.dueAdjustId = data.id
            }
        }
        
        self.dueAdjustAmountTextField.addTarget(self, action: #selector(onDueAdjustChange), for: .editingChanged)
        self.submitBtn.addTarget(self, action: #selector(onSubmitBtnTapped), for: .touchUpInside)
    }
    
    func toolbarSetup(){
        let dueAdjustTextFieldToolBar = UIToolbar()
        dueAdjustTextFieldToolBar.barStyle = UIBarStyle.default
        dueAdjustTextFieldToolBar.isTranslucent = true
        dueAdjustTextFieldToolBar.tintColor = UIColor.black
        dueAdjustTextFieldToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let quantityDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnDueAdjustAmount(sender:)))
        
        dueAdjustTextFieldToolBar.setItems([cancelButton, spaceButton, quantityDoneButton], animated: false)
        dueAdjustTextFieldToolBar.isUserInteractionEnabled = true
        
        self.dueAdjustAmountTextField.inputAccessoryView = dueAdjustTextFieldToolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnDueAdjustAmount(sender: UIBarButtonItem){
        self.descriptonTextField.becomeFirstResponder()
    }
    
    @objc func onDueAdjustChange(sender: UITextField){
        if let currentDue = self.currentDue, let dueAdjust = self.dueAdjustAmount{
            let adjustedAmount = self.dueAdjustAmountTextField.text?.toDouble()
            if let givenAdjustedAmount = adjustedAmount{
                if givenAdjustedAmount > dueAdjust + currentDue{
                    let total = currentDue + dueAdjust
                    self.dueAdjustAmountTextField.text = total.toString()
                    self.showAlert(title: Constants.currencySymbol + " \(total)" , message: LanguageManager.YouCanNotAdjustMoreThan)
                }
            }
        }
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title:  LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func onSubmitBtnTapped(sender: UIButton){
        if isValidated(){
            guard let amount = dueAdjustAmountTextField.text, let adjustAmount = amount.toDouble() else{
                return
            }
            let description = self.descriptonTextField.text ?? ""
            if type == Type.add{
                guard let id = self.customerId else{
                    return
                }
                
                let param: [String : Any] = ["amount": adjustAmount, "description": description, "customer": id]
                self.presenter.postDueAdjustStoreDataToServer(param: param)
            }else{
                guard let id = self.dueAdjustId else{
                    return
                }
                
                let param : [String: Any] = ["id": id, "amount": adjustAmount, "description": description]
                self.presenter.postDueAdjustUpdateDataToServer(param: param)
            }
            
        }
    }
    
    func isValidated()->Bool{
        if self.dueAdjustAmountTextField.text == "" {
            showAlert(title: LanguageManager.DueAdjustAmountIsRequired, message: "")
            return false
        }
        
        return true
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
}

extension DueAdjustUpdateViewController: UITextFieldDelegate{
    func configureTextFields(){
        self.customerNameTextField.delegate = self
        self.currentDueTextField.delegate = self
        self.dueAdjustAmountTextField.delegate = self
        self.descriptonTextField.delegate = self
        self.customerNameTextField.underlined()
        self.currentDueTextField.underlined()
        self.dueAdjustAmountTextField.underlined()
        self.descriptonTextField.underlined()
    }
}

extension DueAdjustUpdateViewController : DueAdjustUpdateViewDelegate{
    func onDueAdjustSuccessfullyUpdate(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onDueAdjustSuccessfullyAdd(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(failure: String) {
        self.showAlert(title: failure, message: "")
    }
    
    func showLoading() {
        submitBtnControlWith(isEnabled : false)
        self.showLoader()
    }
    
    func hideLoading() {
        submitBtnControlWith(isEnabled : true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
    
}
