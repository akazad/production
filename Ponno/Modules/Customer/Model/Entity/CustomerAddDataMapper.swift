//
//  CustomerAddDataMapper.swift
//  Ponno
//
//  Created by a k azad on 7/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class CustomerAddDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var customer : CustomerAdd? 
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        customer <- map["customer"]
    }
    
}
