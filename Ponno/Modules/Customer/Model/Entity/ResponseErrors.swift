//
//  ResponseErrors.swift
//  Ponno
//
//  Created by a k azad on 8/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class ResponseErrors : Mappable {
    var customerId : [String]?
    var amount : [String]?
    var type : [String]?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        customerId <- map["customer_id"]
        amount <- map["amount"]
        type <- map["type"]
    }
    
}
