//
//  CustomerDataMapper.swift
//  Ponno
//
//  Created by a k azad on 27/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class CustomerDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [CustomerSummary]?
    var customers : [Customers]?
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        customers <- map["customers"]
        pagination <- map["pagination"]
    }
    
}

/*
 import Foundation
 struct Response: Codable {
 let totalCustomers: Int
 let totalDue: String
 let totalPaid: Int
 let customers: [Customer]
 let currentPage, currentPageItem, to, from: Int
 let perPage, total: Int
 let hasMorePage: Bool
 let lastPageNo: Int
 let firstPageURL: String
 let nextPageURL, prevPageURL: String?
 let lastPageURL: String
 
 struct Customer: Codable {
 let id: Int
 let name, phone: String
 let address: String?
 let image: String?
 }
 
 enum CodingKeys: String, CodingKey {
 case totalCustomers = "total_Customers"
 case totalDue = "total_due"
 case totalPaid = "total_paid"
 case customers
 case currentPage = "current_page"
 case currentPageItem = "current_page_item"
 case to, from
 case perPage = "per_page"
 case total
 case hasMorePage = "has_more_page"
 case lastPageNo = "last_page_no"
 case firstPageURL = "first_page_url"
 case nextPageURL = "next_page_url"
 case prevPageURL = "prev_page_url"
 case lastPageURL = "last_page_url"
 }
 }
 
 

 */
