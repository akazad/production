//
//  ResponseDataMapper.swift
//  Ponno
//
//  Created by a k azad on 8/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var errors : ResponseErrors?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        errors <- map["errors"]
    }
    
}
