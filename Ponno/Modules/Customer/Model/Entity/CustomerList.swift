//
//  CustomerList.swift
//  Ponno
//
//  Created by a k azad on 27/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class Customers : Mappable {
    var id : Int = 0
    var name : String = ""
    var phone : String = ""
    var address : String = ""
    var image : String = ""
    var currentDue : String = ""
    var hasWallet : Double = 0
    var amount : Double = 0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        address <- map["address"]
        image <- map["image"]
        currentDue <- map["current_due"]
        hasWallet <- map["has_wallet"]
        amount <- map["amount"]
    }
    
}


