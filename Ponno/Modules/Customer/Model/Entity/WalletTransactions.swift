//
//  WalletTransactions.swift
//  Ponno
//
//  Created by a k azad on 28/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class WalletTransactions : Mappable {
    var id : Int = 0
    var type : Int = 0
    var amount : String = ""
    var description : String = ""
    var saleId: Int?
    var saleInvoice: String = ""
    var createdAt : String = ""

    required init(map: Map) {

    }

    func mapping(map: Map) {

        id <- map["id"]
        type <- map["type"]
        amount <- map["amount"]
        description <- map["description"]
        saleId <- map["sale_id"]
        saleInvoice <- map["sale_invoice"]
        createdAt <- map["created_at"]
    }

}

