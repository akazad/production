//
//  CustomerDueHistoryDataMapper.swift
//  Ponno
//
//  Created by a k azad on 2/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class CustomerDueHistoryDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var dues : [CustomerDueHistory]?  
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        success <- map["success"]
        message <- map["message"]
        dues <- map["dues"]
        pagination <- map["pagination"]
    }
    
}

