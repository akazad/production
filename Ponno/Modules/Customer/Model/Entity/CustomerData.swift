//
//  CustomerData.swift
//  Ponno
//
//  Created by a k azad on 9/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
//import Foundation
//import ObjectMapper
//
//class CustomerData : Mappable {
//    var id : Int = 0
//    var name : String = ""
//    var phone : String = ""
//    var address : String = ""
//    var image : String = ""
//
//    required init(map: Map) {
//
//    }
//
//     func mapping(map: Map) {
//
//        id <- map["id"]
//        name <- map["name"]
//        phone <- map["phone"]
//        address <- map["address"]
//        image <- map["image"]
//    }
//
//}

import Foundation
import ObjectMapper

class CustomerData : Mappable {
    var id : Int = 0
    var name : String = ""
    var phone : String = ""
    var address : String = ""
    var image : String = ""
    var totalPurchase : Int = 0
    var currentDue : Double = 0.0
    var totalDue : Double = 0.0
    var totalPaid : Double = 0.0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        phone <- map["phone"]
        address <- map["address"]
        image <- map["image"]
        totalPurchase <- map["total_purchase"]
        currentDue <- map["current_due"]
        totalDue <- map["total_due"]
        totalPaid <- map["total_paid"]
    }
    
}


