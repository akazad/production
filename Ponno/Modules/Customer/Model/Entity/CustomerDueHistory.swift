//
//  CustomerDueHistory.swift
//  Ponno
//
//  Created by a k azad on 25/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class CustomerDueHistory : Mappable {
    
    var id : Int = 0
    var type : Int = 0
    var amount : String = ""
    var createdAt : String = ""
    var sellerId : Int = 0
    var sellerName : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        type <- map["type"]
        amount <- map["amount"]
        createdAt <- map["created_at"]
        sellerId <- map["seller_id"]
        sellerName <- map["seller_name"]
    }
    
}


