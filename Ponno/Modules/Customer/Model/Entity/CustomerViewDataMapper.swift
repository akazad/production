//
//  CustomerViewDataMapper.swift
//  Ponno
//
//  Created by a k azad on 9/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
//import Foundation
//import ObjectMapper
//
//class CustomerViewDataMapper : Mappable {
//    var success : Bool?
//    var message : String?
//    var customer : CustomerData?
//    var totalPurchase : Int = 0
//    var currentDue : Int = 0
//    var totalDue : Int = 0
//    var totalPaid : Int = 0
//
//    required init(map: Map) {
//
//    }
//
//     func mapping(map: Map) {
//
//        success <- map["success"]
//        message <- map["message"]
//        customer <- map["customer"]
//        totalPurchase <- map["total_purchase"]
//        currentDue <- map["current_due"]
//        totalDue <- map["total_due"]
//        totalPaid <- map["total_paid"]
////        purchaseHistory <- map["purchase_history"]
////        dueHistory <- map["due_history"]
//    }
//
//}


import Foundation
import ObjectMapper

class CustomerViewDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var customer : CustomerData?
    var purchaseHistory : CustomerPurchaseHistoryDataMapper?
    var dueHistory : CustomerDueHistoryDataMapper?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        customer <- map["customer"]
        purchaseHistory <- map["purchase_history"]
        dueHistory <- map["due_history"]
    }
    
}
