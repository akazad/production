//
//  CustomerPurchaseHistoryDataMapper.swift
//  Ponno
//
//  Created by a k azad on 9/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
//
//import Foundation
//import ObjectMapper
//
//class CustomerPurchaseHistory : Mappable {
//    var purchases : [CustomerPurchases]?
//    var pagination : Pagination?
//
//    required init(map: Map) {
//
//    }
//
//     func mapping(map: Map) {
//
//        purchases <- map["purchases"]
//        pagination <- map["pagination"]
//
//    }
//}
import Foundation
import ObjectMapper

class CustomerPurchaseHistoryDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var purchases : [CustomerPurchaseHistory]? 
    var pagination : Pagination?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        success <- map["success"]
        message <- map["message"]
        purchases <- map["purchases"]
        pagination <- map["pagination"]
    }
    
}


