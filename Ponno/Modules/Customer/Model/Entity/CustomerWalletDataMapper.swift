//
//  CustomerWalletDataMapper.swift
//  Ponno
//
//  Created by a k azad on 28/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class CustomerWalletDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var customerWallet : CustomerWallet?
    var transactions : [WalletTransactions]?
    var pagination : Pagination?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        customerWallet <- map["customer_wallet"]
        transactions <- map["transactions"]
        pagination <- map["pagination"]
    }

}

