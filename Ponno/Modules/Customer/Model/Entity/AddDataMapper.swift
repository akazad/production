//
//  AddDataMapper.swift
//  Ponno
//
//  Created by a k azad on 6/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class AddDataMapper : Mappable {
    var success : Bool?
    var message : String?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
    }
    
}
