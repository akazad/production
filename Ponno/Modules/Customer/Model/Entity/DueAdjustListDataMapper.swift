//
//  DueAdjustListDataMapper.swift
//  Ponno
//
//  Created by a k azad on 27/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class DueAdjustListDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var customerCurrentDue: String?
    var dueAdjustList : [DueAdjustList]? 
    var pagination : Pagination?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        customerCurrentDue <- map["customer_current_due"]
        dueAdjustList <- map["due_adjust_list"]
        pagination <- map["pagination"]
    }

}

