//
//  CustomerPurchaseHistory.swift
//  Ponno
//
//  Created by a k azad on 25/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class CustomerPurchaseHistory : Mappable {

        var id : Int = 0
        var invoice : String = ""
        var total : String = ""
        var payable : String = ""
        var due : String = ""
        var paid : String = ""
        var discount : String = ""
        var discountUnit : String = ""
        var createdAt : String = ""
        var paymentMethodName : String = ""
        var customerId : Int = 0
        var customerName : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {

        id <- map["id"]
        invoice <- map["invoice"]
        total <- map["total"]
        payable <- map["payable"]
        due <- map["due"]
        paid <- map["paid"]
        discount <- map["discount"]
        discountUnit <- map["discount_unit"]
        createdAt <- map["created_at"]
        paymentMethodName <- map["payment_method_name"]
        customerId <- map["customer_id"]
        customerName <- map["customer_name"]
    }
    
}


