//
//  CustomerService.swift
//  Ponno
//
//  Created by a k azad on 27/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

public class CustomerService : NSObject{
    
    func getCustomerData(page: Int, success: @escaping (CustomerDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let customerURL = RestURL.sharedInstance.Customer + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        let customerHeader = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(customerURL, headers: customerHeader)
            .responseObject { (response: DataResponse<CustomerDataMapper>) in
                if let customerResponse = response.result.value{
                    if customerResponse.success == true {
                        success(customerResponse)
                    }else if let failureMessage = customerResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getAllCustomerData(getAll: Bool, success: @escaping (CustomerDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let customerURL = RestURL.sharedInstance.Customer + RestURL.sharedInstance.getAllData(text: getAll)
        
        let customerHeader = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(customerURL, headers: customerHeader)
            .responseObject { (response: DataResponse<CustomerDataMapper>) in
                if let customerResponse = response.result.value{
                    if let customers = customerResponse.customers, customers.count > 0 {
                        success(customerResponse)
                    }else if let failureMessage = customerResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    //SearchData
    func getCustomerSearchData(page: Int, searchString: String, success: @escaping (CustomerDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let customerURL = RestURL.sharedInstance.Customer + RestURL.sharedInstance.getSearchText(text: searchString) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = customerURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<CustomerDataMapper>) in
                if let customerResponse = response.result.value{
                    if let customers = customerResponse.customers, customers.count > 0 {
                        success(customerResponse)
                    }else if let failureResponse = customerResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getCustomerViewData(id: Int, success: @escaping (CustomerViewDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let customerViewURL = RestURL.sharedInstance.CustomerView + RestURL.sharedInstance.getCustomerDetailsViewUrl(id: id)
        
        print(customerViewURL)
        
        let customerViewHeader = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(customerViewURL, headers: customerViewHeader)
            .responseObject { (response: DataResponse<CustomerViewDataMapper>) in
                if let customerViewResponse = response.result.value{
                    if customerViewResponse.success == true{
                        success(customerViewResponse)
                    }else if let failureMessage = customerViewResponse.message{
                        failure(failureMessage)
                    }
                }else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getCustomerDueHistoryData(page: Int, id: Int, success: @escaping (CustomerDueHistoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.CustomerDueHistory + RestURL.sharedInstance.getCustomerDetailsViewUrl(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<CustomerDueHistoryDataMapper>) in
                if let customerViewResponse = response.result.value{
                    if let dueOfCustomer = customerViewResponse.dues, dueOfCustomer.count > 0 {
                         success(customerViewResponse)
                    }else if let failureMessage = customerViewResponse.message{
                       failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getCustomerPurchaseHistoryData(page: Int, id: Int, success: @escaping (CustomerPurchaseHistoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.CustomerPurchaseHistory + RestURL.sharedInstance.getCustomerDetailsViewUrl(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<CustomerPurchaseHistoryDataMapper>) in
                if let customerViewResponse = response.result.value{
                    if let purchaseOfCustomer = customerViewResponse.purchases, purchaseOfCustomer.count > 0 {
                        success(customerViewResponse)
                    }else if let failureMessage = customerViewResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getCustomerDueAdjustData(page: Int, id: Int, success: @escaping (DueAdjustListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DueAdjust + RestURL.sharedInstance.getDueAdjustId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<DueAdjustListDataMapper>) in
                if let response = response.result.value{
                    if response.success == true{
                        success(response)
                    }else if let failureMessage = response.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getDueAdjustDateSearchData(page: Int, id : Int, startDate: String, endDate : String, success: @escaping (DueAdjustListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DueAdjust + RestURL.sharedInstance.getDueAdjustId(id: id) + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getDateRangePage(text: page)
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<DueAdjustListDataMapper>) in
                if let adjustResponse = response.result.value{
                    if let response = adjustResponse.dueAdjustList, response.count > 0 {
                        success(adjustResponse)
                    }else if let failureMessage = adjustResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getCustomerWalletData(page: Int, id: Int, success: @escaping (CustomerWalletDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.CustomerWallet + RestURL.sharedInstance.getDueAdjustId(id: id) + RestURL.sharedInstance.getPaginationNumber(page: page)
        
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<CustomerWalletDataMapper>) in
                if let response = response.result.value{
                    if response.success == true{
                        success(response)
                    }else if let failureMessage = response.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getCustomerWalletSearchData(page: Int, id: Int, startDate: String, endDate: String, success: @escaping (CustomerWalletDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let walletURL = RestURL.sharedInstance.CustomerWallet + RestURL.sharedInstance.getDueAdjustId(id: id) + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = walletURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<CustomerWalletDataMapper>) in
                if let walletResponse = response.result.value{
                    if walletResponse.success == true{
                        success(walletResponse)
                    }else if let failureMessage = walletResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    //Mark: PostRequest
    func postNewCustomerData(customerDataArray: [String: Any] , success: @escaping (CustomerAddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.CustomerAdd
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        var param = customerDataArray
        
        if let id = customerDataArray["id"]{
            param["id"] = id
        }
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<CustomerAddDataMapper>) in
            if let customerAddResponse = response.result.value{
                if customerAddResponse.success == true{
                    success(customerAddResponse) 
                }else if let failureMessage = customerAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postDueAdjustDeleteData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DueAdjustDelete
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let response = response.result.value {
                if response.success == true{
                    success(response)
                }else if let failureMessage = response.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postDueAdjustUpdateData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DueAdjustUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let response = response.result.value {
                if response.success == true{
                    success(response)
                }else if let failureMessage = response.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postDueAdjustStoreData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DueAdjustStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let response = response.result.value {
                if response.success == true{
                    success(response)
                }else if let failureMessage = response.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postUpdateCustomerData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.CustomerUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let customerUpdateResponse = response.result.value {
                if customerUpdateResponse.success == true{
                    success(customerUpdateResponse)
                }else if let failureMessage = customerUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postCustomerWalletStoreData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.CustomerWalletStore
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let response = response.result.value {
                if response.success == true{
                    success(response)
                }else if let failureMessage = response.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }

    func postCustomerWalletUpdateData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.CustomerWalletUpdate
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let response = response.result.value {
                if response.success == true{
                    success(response)
                }else if let failureMessage = response.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postCustomerWalletDeleteData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.CustomerWalletDelete
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()

        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let response = response.result.value {
                if response.success == true{
                    success(response)
                }else if let failureMessage = response.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func postCustomerWalletCashInOut(param: [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.CustomerWalletCashInOut
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let cashInOutResponse = response.result.value {
                if cashInOutResponse.success == true{
                    success(cashInOutResponse)
                }else if let failureMessage = cashInOutResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
//    func postUserImage(customerID : Int, image : UIImage?, succeed: @escaping((AddDataMapper)->Void), failure : @escaping((String) -> Void)){
//        if !Connectivity.isConnectedToInternet {
//            failure(CommonMessages.NoInternet)
//            return
//        }
//        guard let imageToUpload = image else{
//            return
//        }
//        let header = RestURL.sharedInstance.getPostCommonHeader()
//        let parameters = ["customer_id": "\(customerID)"]
//        let imagePostURL = RestURL.sharedInstance.CustomerImageUpload
//
//        Alamofire.upload(multipartFormData: { multipartFormData in
//            if let imageData = imageToUpload.pngData() {
//                multipartFormData.append(imageData, withName: "image", fileName: "file.png", mimeType: "image/png")
//            }
//
//            for params in parameters {
//                let value = params.value
//                multipartFormData.append((value.data(using: .utf8))!, withName: params.key)
//            }}, to: imagePostURL, method: .post, headers: header,
//                encodingCompletion: { encodingResult  in
//                    switch encodingResult {
//                    case .success(let upload, _, _):
//                        upload.responseObject { (response: DataResponse<AddDataMapper>) in
//                            switch response.result {
//                            case .success:
//                                if let rest = response.result.value {
//                                    if rest.success == true{
//                                        succeed(rest)
//                                    }else{
//                                        let mes = rest.message
//                                        failure(mes!)
//                                    }
//                                }
//                            case .failure(let error):
//                                print(error)
//                                failure(CommonMessages.ApiFailure)
//                            }
//
//                        }
//                    case .failure(let encodingError):
//                        print("error:\(encodingError)")
//                        failure(CommonMessages.ApiFailure)
//                    }
//        })
//    }
    
    func postUserImage(customerID : Int,image : UIImage? ,  succeed: @escaping((AddDataMapper)->Void), failure : @escaping((String) -> Void)){
        
        let imageUploadUrl = RestURL.sharedInstance.CustomerImageUpload
        let params = ["customer_id" : "\(customerID)"]
        
        let service = PostImageService()
        
        service.postImage(params: params, image: image, imageUrl: imageUploadUrl, succeed: { response in
            if let success = response.success, success == true{
                print(response.message ?? "")
                succeed(response)
            }
            else{
                failure(response.message ?? "")
            }
        }, failure: {message in
            failure(message)
        })
    }
}

