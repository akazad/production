//
//  CustomerPresenter.swift
//  Ponno
//
//  Created by a k azad on 27/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol CustomerViewDelegate : NSObjectProtocol {
    func setCustomerData(data : CustomerDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}

class CustomerPresenter: NSObject {
    
    private let service : CustomerService
    weak private var viewDelegate: CustomerViewDelegate?
    
    init(service : CustomerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : CustomerViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getCustomerDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        self.service.getCustomerData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCustomerData(data: data)
        }, failure: {(mesaage) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: mesaage)
        })
    }
}
