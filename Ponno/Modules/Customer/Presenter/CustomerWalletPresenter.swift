//
//  CustomerWalletPresenter.swift
//  Ponno
//
//  Created by a k azad on 28/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol CustomerWalletViewDelegate : NSObjectProtocol {
    func setWalletData(data: CustomerWalletDataMapper)
    func onCashAdd(data: AddDataMapper)
    func onCashInOut(data: AddDataMapper)
    func onTransactionDelete(data: AddDataMapper)
    func onCashAddFailed(data: String)
    func onTransactionDeleteFailed(data: String)
    func onDateSearchFailed(message: String)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class CustomerWalletPresenter : NSObject {
    
    private var service : CustomerService
    weak private var viewDelegate : CustomerWalletViewDelegate?
    
    init(service : CustomerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : CustomerWalletViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getCustomerWalletDataFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getCustomerWalletData(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setWalletData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postCustomerWalletCashInDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postCustomerWalletStoreData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCashAdd(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCashAddFailed(data: message)
        })
    }
    
    func postCustomerWalletCashInOutDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postCustomerWalletCashInOut(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onCashInOut(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postCustomerWalletTransactionDeleteDataToServer(param: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postCustomerWalletDeleteData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onTransactionDelete(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onTransactionDeleteFailed(data: message)
        })
    }
    
    func getCustomerWalletSearchDataFromServer(page: Int, id: Int, startDt: String, endDt: String){
        self.viewDelegate?.showLoading()
        self.service.getCustomerWalletSearchData(page: page, id: id, startDate: startDt, endDate: endDt, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setWalletData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDateSearchFailed(message: message)
        })
    }
    
}
