//
//  CustomerSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 18/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
protocol CustomerSearchViewDelegate : NSObjectProtocol {
    func setCustomerSearchData(data : CustomerDataMapper)
    func onFailed(failure: String)
    func showLoading()
    func hideLoading()
}

class CustomerSearchPresenter: NSObject {
    
    private let service : CustomerService
    weak private var viewDelegate: CustomerSearchViewDelegate?
    
    init(service : CustomerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : CustomerSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getCustomerSearchDataFromServer(page: Int, searchString: String){
        self.viewDelegate?.hideLoading()
        self.service.getCustomerSearchData(page: page, searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCustomerSearchData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
}
