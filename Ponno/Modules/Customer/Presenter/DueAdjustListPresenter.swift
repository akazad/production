//
//  DueAdjustListPresenter.swift
//  Ponno
//
//  Created by a k azad on 27/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
protocol DueAdjustListViewDelegate : NSObjectProtocol {
    func setDueAdjustData(data : DueAdjustListDataMapper)
    func onDueAdjustDelete(data: AddDataMapper)
    func onDateSearchFailed(message: String)
    func onFailed(failure: String)
    func showLoading()
    func hideLoading()
}

class DueAdjustListPresenter: NSObject {
    
    private let service : CustomerService
    weak private var viewDelegate: DueAdjustListViewDelegate?
    
    init(service : CustomerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DueAdjustListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getDueAdjustListDataFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getCustomerDueAdjustData(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDueAdjustData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
    func getDueAdjustDateSearchDataFromServer(startDt: String, endDt: String, page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getDueAdjustDateSearchData(page: page, id: id, startDate: startDt, endDate: endDt, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDueAdjustData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDateSearchFailed(message: message)
        })
    }
    
    func postDueAdjustDeleteDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postDueAdjustDeleteData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDueAdjustDelete(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
}

