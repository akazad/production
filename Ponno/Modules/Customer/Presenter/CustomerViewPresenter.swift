//
//  CustomerViewPresenter.swift
//  Ponno
//
//  Created by a k azad on 9/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol CustomerDetailsViewDelegate : NSObjectProtocol {
    func setCustomerViewData(data : CustomerViewDataMapper)
    func setCustomerDueHistoryData(data: CustomerDueHistoryDataMapper)
    func setCustomerPurchaseHistoryData(data: CustomerPurchaseHistoryDataMapper)
    func onFailed()
    func showLoading()
    func hideLoading()
}

class CustomerViewPresenter: NSObject {
    
    private let service : CustomerService
    weak private var viewDelegate: CustomerDetailsViewDelegate?
    
    init(service : CustomerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : CustomerDetailsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getCustomerDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getCustomerViewData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCustomerViewData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed() 
        })
    }
    func getCustomerDueHistoryDataFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getCustomerDueHistoryData(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCustomerDueHistoryData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
    func getCustomerPurchaseHistoryDataFromServer(page: Int, id: Int){
        self.viewDelegate?.showLoading()
        self.service.getCustomerPurchaseHistoryData(page: page, id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCustomerPurchaseHistoryData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
}


