//
//  CustomerDuePaidPresenter.swift
//  Ponno
//
//  Created by a k azad on 8/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol CustomerDuePaidViewDelegate : NSObjectProtocol {
    func onSuccess(data : ResponseDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class CustomerDuePaidPresenter: NSObject {
    
    private let service : DueService
    weak private var viewDelegate : CustomerDuePaidViewDelegate?
    
    init(service : DueService) {
        self.service = service
    }
    
    func attachView(viewDelegate : CustomerDuePaidViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postCustomerDuePaidData(paidData: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postCustomerPaidData(paidData: paidData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
