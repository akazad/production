//
//  DueAdjustUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 27/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol DueAdjustUpdateViewDelegate : NSObjectProtocol {
    func onDueAdjustSuccessfullyUpdate(data : AddDataMapper)
    func onDueAdjustSuccessfullyAdd(data: AddDataMapper)
    func onFailed(failure: String)
    func showLoading()
    func hideLoading()
}

class DueAdjustUpdatePresenter: NSObject {
    
    private let service : CustomerService
    weak private var viewDelegate: DueAdjustUpdateViewDelegate?
    
    init(service : CustomerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : DueAdjustUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postDueAdjustStoreDataToServer(param:[String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postDueAdjustStoreData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDueAdjustSuccessfullyAdd(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
    func postDueAdjustUpdateDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postDueAdjustUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDueAdjustSuccessfullyUpdate(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(failure: message)
        })
    }
    
}
