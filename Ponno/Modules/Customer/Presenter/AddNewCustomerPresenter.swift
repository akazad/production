//
//  AddNewCustomerPresenter.swift
//  Ponno
//
//  Created by a k azad on 6/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol AddNewCustomerViewDelegate : NSObjectProtocol {
    func setAddNewCustomerData(customerData : CustomerAddDataMapper)
    func updateCustomerData(data: AddDataMapper)
    func onImageUpload(data : AddDataMapper)
    func onFailed(data : String)
    func showLoading()
    func hideLoading()
}
class AddNewCustomerPresenter: NSObject {
    
    private let service : CustomerService
    weak private var viewDelegate : AddNewCustomerViewDelegate?
    
    init(service : CustomerService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AddNewCustomerViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getAddNewCustomerDataFromServer(customerDataArray: [String: String]){
        self.viewDelegate?.showLoading()
        self.service.postNewCustomerData(customerDataArray: customerDataArray, success:{ (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setAddNewCustomerData(customerData: data)
        }, failure: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data : data)
        })
    }
    
    func updateCustomerData(customerData : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postUpdateCustomerData(param: customerData, success: { (data) in 
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.updateCustomerData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func uploadImage(customerID : Int,image : UIImage?){
        self.viewDelegate?.showLoading()
        self.service.postUserImage(customerID: customerID, image: image, succeed: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onImageUpload(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}

