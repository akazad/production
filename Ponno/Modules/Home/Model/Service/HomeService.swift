//
//  HomeService.swift
//  Ponno
//
//  Created by a k azad on 31/7/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper

public class HomeService : NSObject{
    
    func getDummyData(success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ShopDummyData
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<AddDataMapper>) in
                if let dummyDataResponse = response.result.value {
                    if dummyDataResponse.success == true{
                        success(dummyDataResponse)
                    }else if let failureMessage = dummyDataResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getFreemiumDashboardData(success: @escaping (FreemiumDashboardDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.DashboardFreemium
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<FreemiumDashboardDataMapper>) in
                if let dataResponse = response.result.value {
                    if dataResponse.success == true{
                        success(dataResponse)
                    }else if let failureMessage = dataResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
}
