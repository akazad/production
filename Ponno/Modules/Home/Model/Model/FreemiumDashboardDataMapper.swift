//
//  FreemiumDashboardDataMapper.swift
//  Ponno
//
//  Created by a k azad on 18/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class FreemiumDashboardDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var sale : Double?
    var due : Double?
    var expense : Double?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        sale <- map["sale"]
        due <- map["due"]
        expense <- map["expense"]
    }
    
}

