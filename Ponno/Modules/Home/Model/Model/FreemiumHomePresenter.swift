//
//  FreemiumHomePresenter.swift
//  Ponno
//
//  Created by a k azad on 18/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol FreemiumHomeViewDelegate : NSObjectProtocol {
    func setFreemiumDashboardData(data: FreemiumDashboardDataMapper)
    func offlineProduct(data: OfflineProductDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class FreemiumHomePresenter: NSObject {
    
    private let service : HomeService
    weak private var viewDelegate : FreemiumHomeViewDelegate?
    
    init(service : HomeService) {
        self.service = service
    }
    
    func attachView(viewDelegate : FreemiumHomeViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getFreemiumDashboardDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getFreemiumDashboardData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setFreemiumDashboardData(data: data)
        }, failure: {(message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getOfflineProductDataFromServer(timesStamp: String){
        self.viewDelegate?.showLoading()
        let service = ProductService()
        service.getOfflineProductData(timesStamp: timesStamp, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.offlineProduct(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    
}
