//
//  HomeCollectionViewCell.swift
//  Ponno
//
//  Created by a k azad on 9/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var collectionImage: UIImageView!
    @IBOutlet weak var collectionLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpCollectionCardView(uiview: cardView)
    }

    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: HomeCollectionViewCell.self)
    }
    
}
