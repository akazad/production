//
//  HomeViewController.swift
//  Ponno
//
//  Created by a k azad on 9/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

enum CollectionLbl : String{
    case Sell = "Sell"
    case Inventory = "Inventory"
    case Purchase = "Purchase"
    case Expense = "Expense"
    case Due = "Due"
    case Payable = "Payable"
    case Employee = "Employee"
    case Delivery = "Delivery"
    case Customer = "Customer"
    case Vendor = "Vendor"
}

class HomeViewController: BaseViewController {

    @IBOutlet weak var dummyDataBtnView: UIView!
    @IBOutlet weak var dummyDataBtnViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dummyDataBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var dummyDataBtn: UIButton!{
        didSet{
            dummyDataBtn.setTitle(LanguageManager.DummyDataButton, for: .normal)
        }
    }
    @IBOutlet weak var todaySaleLbl: UILabel!
    @IBOutlet weak var dummyViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate var presenter = HomePresenter(service: HomeService())
    
    let collectionCellImage: [String] = ["Add_stock","Add_due","Add_Payable","add_sale","add_purchase","add_expense","Add_employee","Add_Delivery","Add_customer","Add_dealer"]
    
    //let collectionLbl: [String] = ["নতুন পণ্য", "নতুন বাকি", "নতুন দেনা", "নতুন বিক্রয়", "নতুন ক্রয়", "নতুন খরচ", "নতুন কর্মচারী", "নতুন ডেলিভারি", "নতুন কাস্টমার", "নতুন কোম্পানি"]
    
    let imageView : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named:"CollectionBackground")
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    var parentValue: Int?
    var deliveryStatus : Int?
    var paymentStatus : Int?
    var dummyDataBtnStatus : Int?
    var menuList : [PermissionList] = []
    var inventoryStatus : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        //self.hideDummyDataBtn()
        self.configureCollectionView()
        self.initialSetup()
        self.getShopDataFromUserDefaults()
        //setUpView(uiview: dummyDataBtnView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = LanguageManager.Dashboard
        self.setUpInitialLanguage()
        
        self.attachPresenter()
    }
    
    func initialSetup(){
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        //self.dummyDataBtn.addTarget(self, action: #selector(onDummyDataBtnTapped), for: .touchUpInside)
    }
    
    func setBarButton(){
        
        let phoneCallBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        phoneCallBtn.setImage(UIImage(named: "phone"), for: UIControl.State.normal)
        phoneCallBtn.addTarget(self, action: #selector(onBarPopUpBtnTapped(sender:)), for: UIControl.Event.touchUpInside)
        phoneCallBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        phoneCallBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        phoneCallBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barBtn1 = UIBarButtonItem(customView: phoneCallBtn)
        
        navigationItem.setRightBarButtonItems([barBtn1], animated: true)
    }
    
    func hideDummyDataBtn(){
        self.dummyDataBtn.isHidden = true
        self.dummyDataBtnView.isHidden = true
        self.dummyDataBtnHeight.constant = 0.01
        self.dummyDataBtnViewHeight.constant = 0.01
    }
    
    @objc func onBarPopUpBtnTapped(sender: UIBarButtonItem){
        self.makePhoneCall(phoneNumber: "01999088654")
        //self.makePhoneCall(phoneNumber: "01716095433")
    }
    
    func getShopDataFromUserDefaults(){
        if let shop = getShopData(){
            guard  let deliverySystemStatus = shop.deliverySystem, let payStatus = shop.paymentStatus, let inventorySystem = shop.inventorySystem else {
                return
            }
            
            self.deliveryStatus = deliverySystemStatus
            self.paymentStatus = payStatus
            self.inventoryStatus = inventorySystem
            
        }
        
        if let dummyData = getDummyData(){
            guard let dummyDataStatus = dummyData.status else{
                return
            }
            self.dummyDataBtnStatus = dummyDataStatus
        }
        
//        if dummyDataBtnStatus == 1{
//            self.dummyDataBtn.isHidden = false
//            self.dummyViewHeight.constant = 40.0
//        }else{
//            self.dummyDataBtn.isHidden = true
//            self.dummyViewHeight.constant = 0.0
//
//        }
        
        if let user = getUserData(){
            guard let parent = user.parent else{
                return
            }
            let parentInt = Int(parent)
            self.parentValue = parentInt
        }
        
        self.configureHome()
    }
    
    func configureHome(){
        if self.parentValue == 0 {
            if let list = getPermissionList(),list.count > 0, let assignedPermissions = getPermission(), let permissions = assignedPermissions.id, permissions.count > 0  {
                for item in permissions{
                    for permission in list{
                        if permission.id == item{
                            self.menuList.append(permission)
                        }
                    }
                }
                if deliveryStatus == 0 {
                    self.menuList = self.menuList.filter({$0.name != "Delivery"})
                }
                
                if self.inventoryStatus == 1 {
                    let results = self.menuList.filter{$0.id == 12}
                    
                    if results.isEmpty == false{
                        self.menuList = self.menuList.filter({$0.id != 12})
                    }
                }
                
            }
        }else if let list = getPermissionList(),list.count > 0{
            if deliveryStatus == 0 {
                self.menuList += list
                self.menuList = self.menuList.filter({$0.name != "Delivery"})
                
            }else if self.inventoryStatus == 1 {
                
                self.menuList += list
                self.menuList = self.menuList.filter({$0.id != 12})
            }else{
                self.menuList += list
            }
        }
        self.menuList = self.menuList.filter({$0.name != "Manager"})
        self.menuList = self.menuList.filter{$0.id != 37}
        self.collectionView.reloadData()
    }
        
    @objc func onDummyDataBtnTapped(sender: UIButton){
        self.displayMessage(userMessage: LanguageManager.AreYouSure)
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.ExistingDataWillBeRemoved, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            self.presenter.getDummyDataFromServer()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func assignBackground(){
        let background = UIImage(named: "CollectionBackground")
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    
    
    
}

//Mark: CollectionView Delegate and DataSource
extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(HomeCollectionViewCell.nib, forCellWithReuseIdentifier: HomeCollectionViewCell.identifier)
        self.automaticallyAdjustsScrollViewInsets = false
        //self.collectionView?.backgroundView = imageView
        self.collectionView.bounces = false

        //self.cellLayoutSetup()
        
    }
    
    func cellLayoutSetup(){
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //layout.sectionInset = UIEdgeInsets(top: 1, left: 0, bottom: 1, right: 1)
        
        let widthPerItem = collectionView.frame.width / 2.0
        layout.itemSize = CGSize(width: widthPerItem, height: 118.0)
        //layout.minimumInteritemSpacing = 2
        //layout.minimumLineSpacing = 2
        collectionView.setCollectionViewLayout(layout, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
//        let width  = collectionView.bounds.width/2.0
//        return CGSize(width: width, height: 118.0)
//        let screenWidth = UIScreen.main.bounds.width
//        let scaleFactor = (screenWidth / 2) - 8
//
//        return CGSize(width: scaleFactor, height: 118.0)
        let noOfCellsInRow = 2
        //let widthPerItem = collectionView.frame.width / 2.0

        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

        let totalSpace = flowLayout.minimumInteritemSpacing

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        //let width = collectionView.bounds.width/2.0
        

        return CGSize(width: size, height: 115)
    
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menuList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCollectionViewCell.identifier, for: indexPath) as! HomeCollectionViewCell

        if self.menuList[indexPath.row].name == "Sell"{
            cell.collectionLbl.text = LanguageManager.Sale
            cell.collectionImage.image = UIImage(named: "add_sale")
        }else if self.menuList[indexPath.row].name == "Purchase"{
            cell.collectionLbl.text = LanguageManager.Purchase
            cell.collectionImage.image = UIImage(named: "add_purchase")
        }else if self.menuList[indexPath.row].name == "Expense"{
            cell.collectionLbl.text = LanguageManager.Expense
            cell.collectionImage.image = UIImage(named: "add_expense")
        }else if self.menuList[indexPath.row].name == "Inventory"{
            cell.collectionLbl.text = LanguageManager.NewInventory
            cell.collectionImage.image = UIImage(named: "Add_stock")
        }else if self.menuList[indexPath.row].name == "Due"{
            cell.collectionLbl.text = LanguageManager.DueWithdraw
            cell.collectionImage.image = UIImage(named: "Add_due")
        }else if self.menuList[indexPath.row].name == "Payable"{
            cell.collectionLbl.text = LanguageManager.PayableWithdraw
            cell.collectionImage.image = UIImage(named: "Add_Payable")
        }else if self.menuList[indexPath.row].name == "Employee"{
            cell.collectionLbl.text = LanguageManager.NewEmployee
            cell.collectionImage.image = UIImage(named: "Add_employee")
        }else if self.menuList[indexPath.row].name == "Customer"{
            cell.collectionLbl.text = LanguageManager.NewCustomer
            cell.collectionImage.image = UIImage(named: "Add_customer")
        }else if self.menuList[indexPath.row].name == "Delivery"{
            cell.collectionLbl.text = LanguageManager.NewDelivery
            cell.collectionImage.image = UIImage(named: "Add_Delivery")
        }else if self.menuList[indexPath.row].name == "Vendor"{
            cell.collectionLbl.text = LanguageManager.NewVendor
            cell.collectionImage.image = UIImage(named: "Add_dealer")
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let permission = self.menuList[indexPath.row].name
        navigate(to: permission)
    }
    
    func navigate(to : String) {
        switch to {
        case CollectionLbl.Inventory.rawValue:
            self.navigateToAddNewProductViewController()
            //self.navigateToProductCategoryViewController()
        case CollectionLbl.Due.rawValue:
            self.navigateToAddNewDueViewController()
        case CollectionLbl.Payable.rawValue:
            self.navigateToNewPayableDueVC()
        case CollectionLbl.Sell.rawValue:
            self.navigateToAddNewSaleViewController()
        case CollectionLbl.Purchase.rawValue:
            self.navigateToAddNewPurchaseViewController()
        case CollectionLbl.Expense.rawValue:
            self.navigateToAddNewExpenseViewController()
        case CollectionLbl.Employee.rawValue:
            self.navigateToNewEmployeeAddVC()
        case CollectionLbl.Delivery.rawValue:
            self.navigateToAddNewDeliverySystemViewController()
        case CollectionLbl.Customer.rawValue:
            self.navigateToCustomerAddViewController()
        case CollectionLbl.Vendor.rawValue:
            self.navigateToVendorAddViewController()
        default:
            break
        }
    }
    
}

extension HomeViewController{
    func navigateToAddNewPurchaseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewDueViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDueViewController") as! AddNewDueViewController
        //        viewController.dueCustomerList = self.dueCustomerList
        //let type = 0
        viewController.postType = 1
        viewController.dueState = DueState.duePaid.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToNewPayableDueVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewPayableViewController") as! AddNewPayableViewController
        viewController.type = 1
        viewController.changePayableState = ChangePayableState.new.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToNewEmployeeAddVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "NewEmployeeAddViewController") as! NewEmployeeAddViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToCustomerAddViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerAddViewController") as! CustomerAddViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToVendorAddViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewVendorViewController") as! AddNewVendorViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func navigateToAddNewProductViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewProductViewController") as! AddNewProductViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewDeliverySystemViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDeliverySystemViewController") as! AddNewDeliverySystemViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToProductCategoryViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductCategoryViewController") as! ProductCategoryViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: "", message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension HomeViewController : HomeViewDelegate {
    func setDummyData(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        showMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

