//
//  FreemiumHomeViewController.swift
//  Ponno
//
//  Created by a k azad on 8/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

enum FreemiumCollectionLbl : String{
    case Sell = "Sale"
    case Inventory = "AddProduct"
    case Expense = "AddExpense"
    case Due = "AddDue"
}

class FreemiumHomeViewController: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var summaryView: UIView!{
        didSet{
            self.setUpCardView(uiview: summaryView)
        }
    }
    @IBOutlet weak var saleStackView: UIStackView!{
        didSet{
            self.setUpCardView(uiview: saleStackView)
        }
    }
    @IBOutlet weak var dueStackView: UIStackView!{
        didSet{
            self.setUpCardView(uiview: dueStackView)
        }
    }
    @IBOutlet weak var expenseStackView: UIStackView!{
        didSet{
            self.setUpCardView(uiview: expenseStackView)
        }
    }
    @IBOutlet weak var todaysSummaryLbl: UILabel!{
        didSet{
            self.todaysSummaryLbl.text = LanguageManager.TodaysSummary
        }
    }
    @IBOutlet weak var saleAmountLbl: UILabel!
    @IBOutlet weak var saleTitleLbl: UILabel!{
        didSet{
            saleTitleLbl.text = LanguageManager.Sale
        }
    }
    @IBOutlet weak var dueAmountLbl: UILabel!
    @IBOutlet weak var dueTitleLbl: UILabel!{
        didSet{
            dueTitleLbl.text = LanguageManager.Due
        }
    }
    @IBOutlet weak var expenseAmountLbl: UILabel!
    @IBOutlet weak var expenseTitleLbl: UILabel!{
        didSet{
            expenseTitleLbl.text = LanguageManager.Expense
        }
    }
    
    fileprivate var presenter = FreemiumHomePresenter(service: HomeService())
        
    let freemiumCollectionItem : [String] = ["AddProduct", "Sale", "AddDue", "AddExpense"]
    let collectionCellImage: [String] = ["Add_stock","Add_due","add_sale","add_expense"]
    
    var freemiumDashboard : FreemiumDashboardDataMapper?{
        didSet{
            if let data = freemiumDashboard{
                self.saleAmountLbl.text =  (data.sale?.toString() ?? "0.0") + Constants.currencySymbol
                self.dueAmountLbl.text = (data.due?.toString() ?? "0.0") + Constants.currencySymbol
                self.expenseAmountLbl.text = (data.expense?.toString() ?? "0.0") + Constants.currencySymbol
            }
        }
    }
    
    //OfflineProduct
       var newProductList: [OfflineProduct]?{
           didSet{
            resetAllRecords(in : "FreemiumProducts") 
               if let products = newProductList{
                   for item in products{
                       if let inventoryId = item.inventoryId, let unit = item.unit, let sellingPrice = item.sellingPrice?.toDouble(), let productId = item.productId, let name = item.name, let categoryId = item.categoryId, let categoryName = item.categoryName{
                           
                           insertFreemiumProduct(inventoryId: inventoryId, unit: unit, sellingPrice: sellingPrice, productId: productId, name: name, company: item.company ?? "", varient: item.variant ?? "", sku: item.sku ?? "", image: item.image ?? "", categoryId: categoryId, categoryName: categoryName)
                       }
                       
                   }
               }
           }
       }
    var updatedProductList: [OfflineProduct]?{
        didSet{
            if let products = updatedProductList{
                for item in products{
                    if let inventoryId = item.inventoryId, let unit = item.unit, let sellingPrice = item.sellingPrice?.toDouble(), let productId = item.productId, let name = item.name, let categoryId = item.categoryId, let categoryName = item.categoryName{
                        Update(inventoryId: Int64(inventoryId), unit: unit, sellingPrice: sellingPrice, productId: Int64(productId), name: name, company: item.company ?? "", varient: item.variant ?? "", sku: item.sku ?? "", image: item.image ?? "", categoryId: Int64(categoryId), categoryName: categoryName)
                    }
                }
            }
        }
    }
     var deletedProductList: [OfflineProduct]?{
           didSet{
               if let products = deletedProductList{
                   for item in products{
                       if let inventoryId = item.inventoryId{
                           deleteData(id: inventoryId)
                       }
                   }
               }
           }
       }
    
    var lastUpdatedTime: String?
       
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSlideMenuButton()
        
        self.configureCollectionView()
        self.initialSetup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = LanguageManager.Dashboard
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    func initialSetup(){
        
        if let lUpdatedTime = getTimeStamp(){
            if lUpdatedTime.timeStamp == "default"{
                deleteAllData(entity: "FreemiumProducts")
                getOfflineProduct(timesStamp: lUpdatedTime.timeStamp ?? "")
            }else{
                getOfflineProduct(timesStamp: lUpdatedTime.timeStamp ?? "default")
            }
            
        }
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
}

//Mark: CollectionView Delegate and DataSource
extension FreemiumHomeViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(HomeCollectionViewCell.nib, forCellWithReuseIdentifier: HomeCollectionViewCell.identifier)
        self.automaticallyAdjustsScrollViewInsets = false
        self.collectionView.bounces = false
    }
    
    func cellLayoutSetup(){
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        let widthPerItem = collectionView.frame.width / 2.0
        layout.itemSize = CGSize(width: widthPerItem, height: 118.0)
        collectionView.setCollectionViewLayout(layout, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let noOfCellsInRow = 2

        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

        let totalSpace = flowLayout.minimumInteritemSpacing

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

        return CGSize(width: size, height: 115)
    
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.freemiumCollectionItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCollectionViewCell.identifier, for: indexPath) as! HomeCollectionViewCell

        if self.freemiumCollectionItem[indexPath.row] == "Sale"{
            cell.collectionLbl.text = LanguageManager.Sale
            cell.collectionImage.image = UIImage(named: "add_sale")
        }else if self.freemiumCollectionItem[indexPath.row] == "AddProduct"{
            cell.collectionLbl.text = LanguageManager.NewProduct
            cell.collectionImage.image = UIImage(named: "Add_stock")
        }else if self.freemiumCollectionItem[indexPath.row] == "AddExpense"{
            cell.collectionLbl.text = LanguageManager.Expense
            cell.collectionImage.image = UIImage(named: "add_expense")
        }else if self.freemiumCollectionItem[indexPath.row] == "AddDue"{
            cell.collectionLbl.text = LanguageManager.NewDue
            cell.collectionImage.image = UIImage(named: "Add_due")
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = self.freemiumCollectionItem[indexPath.row]
        navigate(to: item)
    }
    
    func navigate(to : String) {
        switch to {
        case FreemiumCollectionLbl.Inventory.rawValue:
            self.navigateToFreemiumProductAddViewController()
        case FreemiumCollectionLbl.Due.rawValue:
            self.navigateToAddNewDueViewController()
        case FreemiumCollectionLbl.Sell.rawValue:
            self.navigateToFreemiumSaleableViewController()
        case FreemiumCollectionLbl.Expense.rawValue:
            self.navigateToAddNewExpenseViewController()
        default:
            break
        }
    }
    
}

extension FreemiumHomeViewController{
    
    func navigateToAddNewDueViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewDueViewController") as! AddNewDueViewController
        let type = 0
        viewController.postType = type
        viewController.dueState = DueState.dueAdd.rawValue
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToFreemiumSaleableViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumSaleableProductsVC") as! FreemiumSaleableProductsVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateToFreemiumProductAddViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumProductAddViewController") as! FreemiumProductAddViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: "", message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension FreemiumHomeViewController : FreemiumHomeViewDelegate{
    func setFreemiumDashboardData(data: FreemiumDashboardDataMapper) {
        self.freemiumDashboard = data
    }
    
    func offlineProduct(data: OfflineProductDataMapper) {
        //self.lastUpdatedTime = data.lastUpdatedTime
        self.newProductList = data.newProductList
        self.updatedProductList = data.updatedProductList
        self.deletedProductList = data.deletedProductList
    }
    
    func onFailed(data: String) {
        showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getFreemiumDashboardDataFromServer()
    }
    
    func getOfflineProduct(timesStamp: String){
        self.presenter.getOfflineProductDataFromServer(timesStamp: timesStamp)
    }
}
