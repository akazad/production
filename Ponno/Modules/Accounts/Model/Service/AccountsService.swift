//
//  AccountsService.swift
//  Ponno
//
//  Created by a k azad on 19/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper

public class AccountsService : NSObject{
    
    func getAccountsData(success: @escaping (AccountsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.AccountsTemp
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<AccountsDataMapper>) in
                if let accountsResponse = response.result.value{
                    if accountsResponse.success == true{
                        success(accountsResponse)
                    }else if let failureMessage = accountsResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getAccountsSearchData(startDate: String, endDate: String, success: @escaping (AccountsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let accountsURL = RestURL.sharedInstance.AccountsTemp + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate)
        
        guard let url = accountsURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<AccountsDataMapper>) in
                if let accountsResponse = response.result.value{
                    if accountsResponse.success == true{
                        success(accountsResponse)
                    }else if let failureMessage = accountsResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getCashFlowReportData(success: @escaping (CashFlowDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.CashFlowReport
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<CashFlowDataMapper>) in
                if let cashFlowResponse = response.result.value{
                    if cashFlowResponse.success == true{
                        success(cashFlowResponse)
                    }else if let failureMessage = cashFlowResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getCashFlowReportSearchData(startDate: String, endDate: String, success: @escaping (CashFlowDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let cashFlowSearchURL = RestURL.sharedInstance.CashFlowReport + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate)
        
        guard let url = cashFlowSearchURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<CashFlowDataMapper>) in
                if let cashFlowReportResponse = response.result.value{
                    if cashFlowReportResponse.success == true{
                        success(cashFlowReportResponse)
                    }else if let failureMessage = cashFlowReportResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getBalanceSheetData(success: @escaping (BalanceSheetDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.BalanceSheet
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<BalanceSheetDataMapper>) in
                if let balanceSheetResponse = response.result.value{
                    if balanceSheetResponse.success == true{
                        success(balanceSheetResponse)
                    }else if let failureMessage = balanceSheetResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func getBalanceSheetSearchData(date: String, success: @escaping (BalanceSheetDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let balanceSheetUrl = RestURL.sharedInstance.BalanceSheet + RestURL.sharedInstance.getSingleDate(date: date)
        
        guard let url = balanceSheetUrl.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<BalanceSheetDataMapper>) in
                if let balanceSheetSearchResponse = response.result.value{
                    if balanceSheetSearchResponse.success == true{
                        success(balanceSheetSearchResponse)
                    }else if let failureMessage = balanceSheetSearchResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
//    func getMobileBankingListData(success: @escaping (MobileBankingListDataMapper) -> (), failure : @escaping (String) -> ()){
//
//        if !Connectivity.isConnectedToInternet {
//            failure(CommonMessages.NoInternet)
//        }
//
//        let url = RestURL.sharedInstance.MobileBankingList
//        print(url)
//
//        let header = RestURL.sharedInstance.getCommonHeader()
//
//        let request = Alamofire.request(url, headers: header)
//            .responseObject { (response: DataResponse<MobileBankingListDataMapper>) in
//                if let mobileBankingResponse = response.result.value{
//                    if mobileBankingResponse.success == true{
//                        success(mobileBankingResponse)
//                    }else if let failureMessage = mobileBankingResponse.message{
//                        failure(failureMessage)
//                    }
//                }
//                else {
//                    failure(CommonMessages.ApiFailure)
//                }
//        }
//        print(request)
//    }
    
    func getMobileBankingListData(page: Int,startDate: String, endDate: String, success: @escaping (MobileBankingListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let accountsURL = RestURL.sharedInstance.MobileBankingList + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = accountsURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<MobileBankingListDataMapper>) in
                if let mobileBankingResponse = response.result.value{
                    if mobileBankingResponse.success == true{
                        success(mobileBankingResponse)
                    }else if let failureMessage = mobileBankingResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func postNewMobileBankData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.MobileBankStore
        print(url)
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let mobileBankStoreResponse = response.result.value{
                if mobileBankStoreResponse.success == true{
                    success(mobileBankStoreResponse)
                }else if let failureMessage = mobileBankStoreResponse.message{
                    failure(failureMessage)
                }
            }else{
                 failure(CommonMessages.ApiFailure)
            }
        }
        //print(request)
    }
    
    func getPerMobileBankingListData(id: Int, page: Int, startDate: String, endDate: String, success: @escaping (PerMobileBankingListDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let accountsURL = RestURL.sharedInstance.PerMobileBankingList + RestURL.sharedInstance.getCommonId(id: id) + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getPageNumber(page: page)
        
        guard let url = accountsURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {
            return
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PerMobileBankingListDataMapper>) in
                if let perMobileBankingResponse = response.result.value{
                    if perMobileBankingResponse.success == true{
                        success(perMobileBankingResponse)
                    }else if let failureMessage = perMobileBankingResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
        print(request)
    }
    
    func postMobileBankUpdateData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.MobileBankUpdate
        print(url)
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let mobileBankUpdateResponse = response.result.value{
                if mobileBankUpdateResponse.success == true{
                    success(mobileBankUpdateResponse)
                }else if let failureMessage = mobileBankUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                 failure(CommonMessages.ApiFailure)
            }
        }
        //print(request)
    }
    
    func postMobileBankDeleteData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.MobileBankDelete
        print(url)
        
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let mobileBankDeleteResponse = response.result.value{
                if mobileBankDeleteResponse.success == true{
                    success(mobileBankDeleteResponse)
                }else if let failureMessage = mobileBankDeleteResponse.message{
                    failure(failureMessage)
                }
            }else{
                 failure(CommonMessages.ApiFailure)
            }
        }
        //print(request)
    }
    
}
