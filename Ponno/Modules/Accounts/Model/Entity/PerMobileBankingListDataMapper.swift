//
//  PerMobileBankingListDataMapper.swift
//  Ponno
//
//  Created by a k azad on 28/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class PerMobileBankingListDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var summary : [Summary]?
    var mobileBank : String?
    var perMobileBankingList : [PerMobileBankList]? 
    var pagination : Pagination?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        success <- map["success"]
        message <- map["message"]
        summary <- map["summary"]
        mobileBank <- map["payment_method"]
        perMobileBankingList <- map["payment_list"]
        pagination <- map["pagination"]
    }

}

