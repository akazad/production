//
//  SaleOperation.swift
//  Ponno
//
//  Created by a k azad on 30/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class SaleOperation : Mappable {
    var totalSale : Int = 0
    var costOfSale : Int = 0
    var gross : Int = 0
    var deliveryCommision : Int = 0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        totalSale <- map["total_sale"]
        costOfSale <- map["cost_of_sale"]
        gross <- map["gross"]
        deliveryCommision <- map["delivery_commision"]
    }
    
}
