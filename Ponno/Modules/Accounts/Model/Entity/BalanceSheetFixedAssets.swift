//
//  BalanceSheetFixedAssets.swift
//  Ponno
//
//  Created by a k azad on 1/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class BalanceSheetFixedAssets : Mappable {
    var properties : Int = 0
    var equipments : Int = 0
    var goodwill : String = ""
    var total : Int = 0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        properties <- map["properties"]
        equipments <- map["equipments"]
        goodwill <- map["goodwill"]
        total <- map["total"]
    }
    
}

