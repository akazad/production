//
//  TotalExpenseWithCategory.swift
//  Ponno
//
//  Created by a k azad on 30/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class TotalExpenseWithCategory : Mappable {
    var category : String = ""
    var totalNo : Int = 0
    var totalAmount :  Int = 0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        category <- map["category"]
        totalNo <- map["total_no"]
        totalAmount <- map["total_amount"]
    }
    
}
