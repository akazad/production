//
//  GrossProfit.swift
//  Ponno
//
//  Created by a k azad on 19/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//


import Foundation
import ObjectMapper

class GrossProfit : Mappable {
    var date : String?
    var sale : Int?
    var purchase : Int?
    var gross : Int?
    var color : String?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        date <- map["date"]
        sale <- map["sale"]
        purchase <- map["purchase"]
        gross <- map["gross"]
        color <- map["color"]
    }
    
}
