//
//  PerMobileBankList.swift
//  Ponno
//
//  Created by a k azad on 28/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class PerMobileBankList : Mappable {
    var id : Int?
    var invoice : String = ""
    var cash : String = ""
    var createdAt : String = ""
    var paymentNumber : String = ""

    required init(map: Map) {

    }

    func mapping(map: Map) {

        id <- map["id"]
        invoice <- map["invoice"]
        cash <- map["cash"]
        createdAt <- map["created_at"]
        paymentNumber <- map["payment_number"]
    }

}

