//
//  BalanceSheetCurrentAssets.swift
//  Ponno
//
//  Created by a k azad on 1/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class BalanceSheetCurrentAssets : Mappable {
    var cash : String = ""
    var accountReceivable : Double = 0.0
    var inventory : Int = 0
    var total : Double = 0.0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        cash <- map["cash"]
        accountReceivable <- map["account_receivable"]
        inventory <- map["inventory"]
        total <- map["total"]
    }
    
}

