//
//  BalanceSheetAssets.swift
//  Ponno
//
//  Created by a k azad on 1/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class BalanceSheetAssets : Mappable {
    var currentAssets : BalanceSheetCurrentAssets?
    var fixedAssets : BalanceSheetFixedAssets?
    var total : Double = 0.0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        currentAssets <- map["current_assets"]
        fixedAssets <- map["fixed_assets"]
        total <- map["total"]
    }
    
}

