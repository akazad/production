//
//  CashFlowDataMapper.swift
//  Ponno
//
//  Created by a k azad on 30/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class CashFlowDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var cashFlowReport : CashFlowReport? 
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        cashFlowReport <- map["report"]
    }
    
}
