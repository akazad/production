//
//  TotalExpense.swift
//  Ponno
//
//  Created by a k azad on 30/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class TotalExpense : Mappable {
    var scalar : Int = 0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        scalar <- map["scalar"]
    }
    
}
