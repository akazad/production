//
//  InvestorOperation.swift
//  Ponno
//
//  Created by a k azad on 30/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class InvestorOperation : Mappable {
    var totalTake : Int = 0
    var totalPaid : Int = 0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        totalTake <- map["total_take"]
        totalPaid <- map["total_paid"]
    }
    
}
