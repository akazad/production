//
//  DueOperation.swift
//  Ponno
//
//  Created by a k azad on 30/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class DueOperation : Mappable {
    var totalDue : Int = 0
    var totalWithdraw : Int = 0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        totalDue <- map["total_due"]
        totalWithdraw <- map["total_withdraw"]
    }
    
}
