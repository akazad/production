//
//  AccountsNetProfitBarEntry.swift
//  Ponno
//
//  Created by a k azad on 4/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class AccountsNetProfitBarEntry : Mappable {
    var index : Double?
    var stepValues : [Double]?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        index <- map["index"]
        stepValues <- map["step_values"]
    }
    
}
