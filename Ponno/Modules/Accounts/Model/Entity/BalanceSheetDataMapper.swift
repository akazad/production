//
//  BalanceSheetDataMapper.swift
//  Ponno
//
//  Created by a k azad on 1/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class BalanceSheetDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var balanceSheet : BalanceSheet?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        balanceSheet <- map["report"]
    }
    
}

