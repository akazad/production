//
//  AccountsDataMapper.swift
//  Ponno
//
//  Created by a k azad on 19/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class AccountsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var grossProfit : [GrossProfit]?
    var netProfit : [NetProfit]?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        grossProfit <- map["gross_profit"]
        netProfit <- map["net_profit"]
    }
    
}

