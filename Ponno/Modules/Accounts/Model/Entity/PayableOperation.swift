//
//  PayableOperation.swift
//  Ponno
//
//  Created by a k azad on 30/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PayableOperation : Mappable {
    var totalPayable : Int = 0
    var totalPaid : Int = 0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        totalPayable <- map["total_payable"]
        totalPaid <- map["total_paid"]
    }
    
}
