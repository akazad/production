//
//  NetProfit.swift
//  Ponno
//
//  Created by a k azad on 19/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class NetProfit : Mappable {
    var label : String?
    var value : Int?
    var open : Int?
    var stepValue : Int?
    var color : String?
    var displayValue : Double = 0.0
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        label <- map["label"]
        value <- map["value"]
        open <- map["open"]
        stepValue <- map["stepValue"]
        color <- map["color"]
        displayValue <- map["displayValue"]
    }
    
}

