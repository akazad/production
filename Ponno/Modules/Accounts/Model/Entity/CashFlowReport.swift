//
//  CashFlowReport.swift
//  Ponno
//
//  Created by a k azad on 30/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class CashFlowReport : Mappable {
    var saleOperation : SaleOperation? 
    var dueOperation : DueOperation?
    var payableOperation : PayableOperation?
    var totalExpenseWithCategory : [TotalExpenseWithCategory]?
    var totalExpense : TotalExpense?
    var loanOperation : LoanOperation?
    var investorOperation : InvestorOperation? 
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        saleOperation <- map["sale_operation"]
        dueOperation <- map["due_operation"]
        payableOperation <- map["payable_operation"]
        totalExpenseWithCategory <- map["total_expense_with_category"]
        totalExpense <- map["total_expense"]
        loanOperation <- map["loan_operation"]
        investorOperation <- map["investor_operation"]
    }
    
}
