//
//  BalanceSheetLiabilities.swift
//  Ponno
//
//  Created by a k azad on 1/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class BalanceSheetLiabilities : Mappable {
    var loan : Int = 0
    var payable : Int = 0
    var total : Int = 0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        loan <- map["loan"]
        payable <- map["payable"]
        total <- map["total"]
    }
    
}

