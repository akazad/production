//
//  MobileBankingList.swift
//  Ponno
//
//  Created by a k azad on 28/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class MobileBankingList : Mappable {
    var id : Int?
    var name : String?
    var total : Double?
    var totalAmount : Double?
    var pharmacyId : Int?

    required init(map: Map) {

    }

    func mapping(map: Map) {

        id <- map["id"]
        name <- map["name"]
        total <- map["total"]
        totalAmount <- map["total_amount"]
        pharmacyId <- map["pharmacy_id"]
    }

}

