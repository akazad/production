//
//  BalanceSheet.swift
//  Ponno
//
//  Created by a k azad on 1/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class BalanceSheet : Mappable {
    var balanceSheetAssets : BalanceSheetAssets?
    var balanceSheetLiabilities : BalanceSheetLiabilities? 
    var balanceSheetInvestments : Int = 0
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        balanceSheetAssets <- map["assets"]
        balanceSheetLiabilities <- map["liabilities"]
        balanceSheetInvestments <- map["investments"]
    }
    
}
