//
//  AccountsGrossProfitBarEntries.swift
//  Ponno
//
//  Created by a k azad on 31/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class AccountsGrossProfitBarEntries : Mappable {
    var index : Double?
    var stepValue : String?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        index <- map["index"]
        stepValue <- map["step_value"]
    }
    
}
