//
//  PerMobileBankingListPresenter.swift
//  Ponno
//
//  Created by a k azad on 2/2/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

protocol PerMobileBankingListViewDelegate : NSObjectProtocol {
    func setMobileBankData(data: PerMobileBankingListDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class PerMobileBankingListPresenter : NSObject {
    
    private var service : AccountsService
    weak private var viewDelegate : PerMobileBankingListViewDelegate?
    
    init(service : AccountsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PerMobileBankingListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getPerMobileBankingListDataFromServer(id: Int, page: Int,startDt: String, endDt: String){
        self.viewDelegate?.showLoading()
        self.service.getPerMobileBankingListData(id: id, page: page, startDate: startDt, endDate: endDt, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setMobileBankData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    
}


