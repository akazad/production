//
//  BalanceSheetPresenter.swift
//  Ponno
//
//  Created by a k azad on 1/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol BalanceSheetViewDelegate : NSObjectProtocol {
    func setBalanceSheetData(data: BalanceSheetDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class BalanceSheetPresenter : NSObject {
    
    private var service : AccountsService
    weak private var viewDelegate : BalanceSheetViewDelegate?
    
    init(service : AccountsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : BalanceSheetViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getBalanceSheetDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getBalanceSheetData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setBalanceSheetData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
        
    }
    
    func getBalanceSheetSearchDataFromServer(date : String){
        self.viewDelegate?.showLoading()
        self.service.getBalanceSheetSearchData(date: date, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setBalanceSheetData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    
}
