//
//  MobileBankingListPresenter.swift
//  Ponno
//
//  Created by a k azad on 28/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

protocol MobileBankingListViewDelegate : NSObjectProtocol {
    func setMobileBankData(data: MobileBankingListDataMapper)
    func onSuccessfulMobileBankAdd(data: AddDataMapper)
    func onSuccessfulMobileBankDelete(data: AddDataMapper)
    func onSuccessfulMobileBankUpdate(data: AddDataMapper)
    func onMobileBankFailed(data: String)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class MobileBankingListPresenter : NSObject {
    
    private var service : AccountsService
    weak private var viewDelegate : MobileBankingListViewDelegate?
    
    init(service : AccountsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : MobileBankingListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getMobileBankingListDataFromServer(page: Int,startDt: String, endDt: String){
        self.viewDelegate?.showLoading()
        self.service.getMobileBankingListData(page: page, startDate: startDt, endDate: endDt, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setMobileBankData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewMobileBankStoreDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewMobileBankData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccessfulMobileBankAdd(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postMobileBankUpdateDataToServer(param : [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postMobileBankUpdateData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccessfulMobileBankUpdate(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onMobileBankFailed(data: message)
        })
    }
    
    func postMobileBankDeleteDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postMobileBankDeleteData(params: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccessfulMobileBankDelete(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onMobileBankFailed(data: message)
        })
    }
    
}

