//
//  CashFlowReportPresenter.swift
//  Ponno
//
//  Created by a k azad on 30/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol CashFlowReportViewDelegate : NSObjectProtocol {
    func setCashFlowReportData(data: CashFlowDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class CashFlowReportPresenter : NSObject {
    
    private var service : AccountsService
    weak private var viewDelegate : CashFlowReportViewDelegate?
    
    init(service : AccountsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : CashFlowReportViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getCashFlowReportDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getCashFlowReportData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCashFlowReportData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getCashFlowReportSearchDataFormServer(startDate: String, endDate: String){
        self.viewDelegate?.showLoading()
        self.service.getCashFlowReportSearchData(startDate: startDate, endDate: endDate, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setCashFlowReportData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    
}
