//
//  AccountsPresenter.swift
//  Ponno
//
//  Created by a k azad on 19/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol AccountsViewDelegate : NSObjectProtocol {
    func setAccountsData(data: AccountsDataMapper)
    func setAccountsSearchData(data: AccountsDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class AccountsPresenter : NSObject {
    
    private var service : AccountsService
    weak private var viewDelegate : AccountsViewDelegate?
    
    init(service : AccountsService) {
        self.service = service
    }
    
    func attachView(viewDelegate : AccountsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getAccountsDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getAccountsData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setAccountsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getAccountsSearchData(startDate: String, endDate: String){
        self.viewDelegate?.showLoading()
        self.service.getAccountsSearchData(startDate: startDate, endDate: endDate, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setAccountsSearchData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}

