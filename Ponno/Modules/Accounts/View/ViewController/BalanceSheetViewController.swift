//
//  BalanceSheetViewController.swift
//  Ponno
//
//  Created by a k azad on 1/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class BalanceSheetViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchDateTextField: UITextField!
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    
    fileprivate var presenter = BalanceSheetPresenter(service: AccountsService())
    
    var balanceSheet : BalanceSheet?{
        didSet{
            self.refreshTableView()
        }
    }
    
    var datePicker = UIDatePicker()
    var searchDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        self.setUpInitialLanguage()
        self.showSearchDatePicker()
        self.configureTableView()
        self.attachPresenter()
        
    }
   
    func initialSetup(){
        
        self.generateCurrentDate()
        self.searchBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    func setTitle(date : String){
        self.title = LanguageManager.BalanceSheet + " : " + date
        self.searchDateTextField.text = date
    }
    
    func generateCurrentDate(){
        let currentDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        let result = formatter.string(from: currentDate)
        self.searchDate = currentDate
        self.setTitle(date: result)
    }

}

//Mark: SearchDate
extension BalanceSheetViewController{
    func showSearchDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        searchDateTextField.inputAccessoryView = toolbar
        searchDateTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        if searchDateTextField.isFirstResponder{
            searchDateTextField.text = formatter.string(from: datePicker.date)
            searchDateTextField.becomeFirstResponder()
            self.setTitle(date: formatter.string(from: datePicker.date))
            self.searchDate = datePicker.date
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if searchDateTextField.text == "" {
            showAlert(title: LanguageManager.SearchDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.string(from: self.searchDate)
            
            //dateFormatter.string(from: dateFormatter.date(from: dateString)!)
            self.presenter.getBalanceSheetSearchDataFromServer(date: date)
        }
    }
}

//Mark: TableView Delegate and DataSource
extension BalanceSheetViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(BalanceSheetCell.nib, forCellReuseIdentifier: BalanceSheetCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 725
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : BalanceSheetCell = tableView.dequeueReusableCell(withIdentifier: BalanceSheetCell.identifier, for: indexPath) as! BalanceSheetCell
        
        cell.assetsLbl.text = LanguageManager.Assets
        cell.currentAssetsLbl.text = "(" + LanguageManager.CurrentAssets + ")"
        cell.fixedAssetsLbl.text = "(" + LanguageManager.FixedAssets + ")"
        cell.liabilitiesLbl.text = LanguageManager.Liabilities
        cell.investmentLbl.text = LanguageManager.Investment
        
        if let balanceSheet = self.balanceSheet{
            let cash = balanceSheet.balanceSheetAssets?.currentAssets?.cash
            cell.cashLbl.text = LanguageManager.Cash
            cell.cashAmountLbl.text = cash
            
            let receivable = balanceSheet.balanceSheetAssets?.currentAssets?.accountReceivable
            cell.receivableLbl.text = LanguageManager.Receivable
            cell.receivableAmountLbl.text = "\(receivable ?? 0.0)" + Constants.currencySymbol
            
            let inventory = balanceSheet.balanceSheetAssets?.currentAssets?.inventory
            cell.inventoryLbl.text = LanguageManager.Inventory
            cell.inventoryAmountLbl.text = "\(inventory ?? 0)" + Constants.currencySymbol
            
            let properties = balanceSheet.balanceSheetAssets?.fixedAssets?.properties
            cell.propertiesLbl.text = LanguageManager.Properties
            cell.propertiesAmountLbl.text = "\(properties ?? 0)" + Constants.currencySymbol
            
            let equipments = balanceSheet.balanceSheetAssets?.fixedAssets?.equipments
            cell.equipmentsLbl.text = LanguageManager.Equipments
            cell.equipmentsAmountLbl.text = "\(equipments ?? 0)"
            
            let goodwill = balanceSheet.balanceSheetAssets?.fixedAssets?.goodwill
            cell.goodwillLbl.text = LanguageManager.GoodWill
            cell.goodwillAmountLbl.text = goodwill
            
            let totalAssets = balanceSheet.balanceSheetAssets?.total
            cell.totalAssetsLbl.text = LanguageManager.TotalAssets
            cell.totalAssetsAmountLbl.text = "\(round(totalAssets ?? 0.0))" + Constants.currencySymbol
            
            let accountPayable = balanceSheet.balanceSheetLiabilities?.payable
            cell.accountPayableLbl.text = LanguageManager.Payable
            cell.accountPayableAmountLbl.text = "\(accountPayable ?? 0)"
            
            let loan = balanceSheet.balanceSheetLiabilities?.loan
            cell.loanLbl.text = LanguageManager.Loan
            cell.loanAmountLbl.text = "\(loan ?? 0)"
            
            let totalLiablities = balanceSheet.balanceSheetLiabilities?.loan
            cell.totalLiabilitiesLbl.text = LanguageManager.TotalLiabilities
            cell.totalLiabilitiesAmountLbl.text = "\(totalLiablities ?? 0)" + Constants.currencySymbol
            
            let totalInvestment = balanceSheet.balanceSheetInvestments
            cell.totalInvestmentLbl.text = LanguageManager.TotalInvestment
            cell.totalInvestmentAmountLbl.text = "\(totalInvestment )" + Constants.currencySymbol
            
        }
        
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 725
    }
    
    
}

//Mark: Api Delegate
extension BalanceSheetViewController : BalanceSheetViewDelegate{
    func setBalanceSheetData(data: BalanceSheetDataMapper) {
        guard let balanceSheet = data.balanceSheet else{
            return
        }
        self.balanceSheet = balanceSheet
        
    }
    
    func onFailed(data: String) {
        showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getBalanceSheetDataFromServer()
    }
    
}


