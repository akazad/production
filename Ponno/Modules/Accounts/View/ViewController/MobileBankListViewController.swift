//
//  MobileBankListViewController.swift
//  Ponno
//
//  Created by a k azad on 25/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class MobileBankListViewController: UIViewController {
    @IBOutlet weak var dateSearchHeight: NSLayoutConstraint!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate var presenter = MobileBankingListPresenter(service: AccountsService())
    
    var mobileBankingList : [MobileBankingList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var summery : [Summary]?
    
    var datePicker = UIDatePicker()
    
    var searchIsActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    var currentPage : Int = 1
    var searchEnable : Bool = false
    var searchPage: Int = 1
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureCollectionView()
        self.configureTableView()
        
        self.initialSetUp()
        self.showStartDatePicker()
        self.showEndDatePicker()
    }
    
    
    func initialSetUp(){
        self.title = LanguageManager.MobileBanking
        
        self.searchBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        self.setBarButton()
        self.dateSearchHeight.constant = 0.0
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setBarButton(){
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "dateSearch"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        let paymentAddBtn: UIButton = UIButton (type: UIButton.ButtonType.custom)
        paymentAddBtn.setImage(UIImage(named: "plus-2"), for: UIControl.State.normal)
        paymentAddBtn.addTarget(self, action: #selector(onMobileBankAdd(sender:)), for: UIControl.Event.touchUpInside)
        paymentAddBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        paymentAddBtn.widthAnchor.constraint(equalToConstant: 24).isActive = true
        paymentAddBtn.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton2 = UIBarButtonItem(customView: paymentAddBtn)
        
        navigationItem.setRightBarButtonItems([barButton2,barButton], animated: true)
    }
    
    @objc func onSearch(sender: UIButton){
        if searchEnable == false{
            self.dateSearchHeight.constant = 30.0
            self.searchEnable = !searchEnable
        }else{
            self.dateSearchHeight.constant = 0.01
            searchEnable = !searchEnable
        }
    }
    
    @objc func onMobileBankAdd(sender: UIBarButtonItem){
        self.storeAlertWithTextField()
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(MobileBankListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.mobileBankingList = []
        self.currentPage = 1
        self.searchIsActive = false
        self.datePickerTextField.text = ""
        self.toDatePickerTextField.text = ""
        self.presenter.getMobileBankingListDataFromServer(page: self.currentPage, startDt: "default", endDt: "default")
        
        refreshControl.endRefreshing()
    }
    
}

//Mark: Date Search
extension MobileBankListViewController {
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            
            self.mobileBankingList = []
            self.currentPage = 1
            self.searchIsActive = true
            
            self.presenter.getMobileBankingListDataFromServer(page: currentPage, startDt: startDate, endDt: endDate)
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
    
}

//MARK: CollectionView Delegate And DataSource
extension MobileBankListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(SalesSummeryCell.nib, forCellWithReuseIdentifier: SalesSummeryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let item = self.summery, item.count > 0 else {
            return 0
        }
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SalesSummeryCell = collectionView.dequeueReusableCell(withReuseIdentifier: SalesSummeryCell.identifier, for: indexPath) as! SalesSummeryCell
        
        guard let item = self.summery, item.count > 0 else{
            return cell
        }
        let summeryList = item[indexPath.row % item.count]
        
        cell.SalesSummeryName.text = summeryList.title
        cell.SalesSummeryNumber.text = summeryList.value

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 60.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.summery, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
}

// Mark: TableView Delegate and DataSource
extension MobileBankListViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PaymentMethodCell.nib, forCellReuseIdentifier: PaymentMethodCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.tableView.estimatedRowHeight = 60.0
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.tableView.addSubview(self.refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mobileBankingList.count > 0{
            return self.mobileBankingList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : PaymentMethodCell = tableView.dequeueReusableCell(withIdentifier: PaymentMethodCell.identifier, for: indexPath) as! PaymentMethodCell
        cell.selectionStyle = .none
        if self.mobileBankingList.count > 0 {
            let paymentItem = self.mobileBankingList[indexPath.row]
            
            cell.mobileBank = paymentItem
            
            cell.popUpBtn.tag = paymentItem.id ?? 0
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.mobileBankingList
                .count - 1{
                self.isLoading = true
                if searchIsActive == false{
                    if self.currentPage < self.lastPageNo {
                        self.currentPage += 1
                        self.presenter.getMobileBankingListDataFromServer(page: self.currentPage, startDt: "default", endDt: "default")
                    }
                }else{
                    if self.searchPage < self.lastPageNo {
                        guard let startDate = self.datePickerTextField.text else{
                            return cell
                        }
                        guard let endDate = self.toDatePickerTextField.text else{
                            return cell
                        }
                        self.searchPage += 1
                        
                        self.presenter.getMobileBankingListDataFromServer(page: searchPage, startDt: startDate, endDt: endDate)
                    }
                }
                
            }
        }
        return cell
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.mobileBankingList.count > 0{
            let id = sender.tag
            for item in self.mobileBankingList{
                if id == item.id{
                    
                    let updateAction = UIAlertAction(title: LanguageManager.Update, style: UIAlertAction.Style.default) { (action) in
                        self.updateAlertWithTextField(id: id, name: item.name ?? "")
                    }
                    
                    let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                        self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                    }
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                        
                        self.view.endEditing(true)
                    }
                    
                    deleteAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                    
                    // add action buttons to action sheet
                    myActionSheet.addAction(updateAction)
                    myActionSheet.addAction(deleteAction)
                    myActionSheet.addAction(cancelAction)
                }
            }
        }
        // present the action sheet
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.mobileBankingList.count > 0{
            let item = self.mobileBankingList[indexPath.row]
            if let id = item.id{
                self.navigateToPerMobileBankVC(mobilebankId: id)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToPerMobileBankVC(mobilebankId: Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Accounts", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PerMobileBankListViewController") as! PerMobileBankListViewController
        viewController.mobileBankId = mobilebankId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
        let alertController = UIAlertController(title: userMessage, message: "", preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
            let param : [String : Any] = ["id": deleteId ]
            
            self.presenter.postMobileBankDeleteDataToServer(param: param)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){
            (action:UIAlertAction!) in
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title:  LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.mobileBankingList = []
            self.currentPage = 1
            self.presenter.getMobileBankingListDataFromServer(page: self.currentPage, startDt: "default", endDt: "default")
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

// Mark: Api Delegate
extension MobileBankListViewController : MobileBankingListViewDelegate{
    func setMobileBankData(data: MobileBankingListDataMapper) {
        
        self.summery = data.summary
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        guard let list = data.mobileBankingList, list.count > 0 else{
            return
        }
        self.isLoading = false
        self.mobileBankingList += list
        
        guard let pagination = data.pagination else{
            return
        }
        self.lastPageNo = pagination.lastPageNo
        
    }
    
    func onSuccessfulMobileBankAdd(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onSuccessfulMobileBankUpdate(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onSuccessfulMobileBankDelete(data: AddDataMapper) {
        guard let message = data.message else{
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onMobileBankFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.mobileBankingList = []
        self.presenter.getMobileBankingListDataFromServer(page: self.currentPage, startDt: "default", endDt: "default")
    }
}

extension MobileBankListViewController{
//    func successMessage(userMessage:String) -> Void {
//        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
//
//        let OKAction = UIAlertAction(title: "OK", style: .default){
//            (action:UIAlertAction!) in
//            self.walletTransaction = []
//            if let id = self.vendorId{
//                self.presenter.getVendorWalletDataFromServer(page: self.currentPage, id: id)
//            }
//
//        }
//        alertController.addAction(OKAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
//
//    func transactionDeleteAlert(id: Int) -> Void {
//        let alertController = UIAlertController(title: LanguageManager.Warning, message: LanguageManager.AreYouSure, preferredStyle: .alert)
//
//        let OKAction = UIAlertAction(title: "OK", style: .default){
//            (action:UIAlertAction!) in
//
//            let param : [String : Any] = ["id": id]
//            self.presenter.postVendorWalletTransactionDeleteDataToServer(param: param)
//        }
//        alertController.addAction(OKAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
    
    func storeAlertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.MobileBankingName, message: "", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.MobileBankIsRequired, message: "")
                return
            }
            
            let param : [String : Any] = ["name": text]
            self.presenter.postNewMobileBankStoreDataToServer(param: param)
             
        }
        
        confirmAction.isEnabled = false
        
        alertController.addTextField { textField0 in
            textField0.textAlignment = .center
            
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField0, queue: OperationQueue.main, using:
                {_ in
                    let textCount = textField0.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0
                    let textIsNotEmpty = textCount > 0
                    
                    confirmAction.isEnabled = textIsNotEmpty
            })
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func updateAlertWithTextField(id: Int, name: String){
        let alertController = UIAlertController(title: LanguageManager.MobileBankingName, message: "", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showAlert(title: LanguageManager.MobileBankIsRequired, message: "")
                return
            }
            
            let param : [String : Any] = ["id": id, "name": text]
            self.presenter.postMobileBankUpdateDataToServer(param: param)
             
        }
        
        confirmAction.isEnabled = false
        
        alertController.addTextField { textField0 in
            textField0.textAlignment = .center
            textField0.text = name
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField0, queue: OperationQueue.main, using:
                {_ in
                    let textCount = textField0.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0
                    let textIsNotEmpty = textCount > 0
                    
                    confirmAction.isEnabled = textIsNotEmpty
                
            })
            
        }
        
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}
