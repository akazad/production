//
//  CashFlowReportViewController.swift
//  Ponno
//
//  Created by a k azad on 30/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class CashFlowReportViewController: UIViewController {
    
    @IBOutlet weak var searchHeight: NSLayoutConstraint!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var presenter = CashFlowReportPresenter(service: AccountsService())
    
    var saleOperation : SaleOperation?
    var dueOperation : DueOperation?
    var payableOperation : PayableOperation?
    var totalExpenseWithCategory : [TotalExpenseWithCategory]?{
        didSet{
            self.refreshTableView()
        }
    }
    var totalExpense : TotalExpense?
    var loanOperation : LoanOperation?
    var investorOperation : InvestorOperation?
    
    var totalCashReceive : Double = 0
    var totalCashPaid : Double = 0
    
    var sumOfExpenses: Int?
    var searchEnable : Bool = false
    
    var todayDate : String?
    var fifteenDaysBeforeDate : String?
    
    var datePicker = UIDatePicker()
    
    enum CFR : Int {
        case cashReceive
        case expenseCategory
        case cashPaid
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.initialSetUp()
        self.addFloaty()
        self.setNavigationBarTitle()
        self.configureTableView()
        self.showStartDatePicker()
        self.showEndDatePicker()
        self.setBarButton()
        //self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetUp()
        self.setUpInitialLanguage()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.attachPresenter()
    }
    
    
    func initialSetUp(){
        self.title = LanguageManager.CashFlowReport
        self.generatingDate()
        self.searchHeight.constant = 0.01
        self.searchBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    
    func setNavigationBarTitle(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0))]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.searchHeight.constant = 0.0
    }
    
    
    func setBarButton(){
        
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "dateSearch"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func onSearch(sender: UIButton){
        if searchEnable == false{
            self.searchHeight.constant = 30.0
            self.searchEnable = !searchEnable
        }else{
            self.searchHeight.constant = 0.01
            searchEnable = !searchEnable
        }
    }
    
    
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    func generatingDate(){
        
        let today = Date()
        print(today)
        
        let fifteenDaysBeforeToday = Calendar.current.date(byAdding: .day, value: -15, to: today)!
        print(fifteenDaysBeforeToday)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        self.todayDate = dateFormatter.string(from: Date())
        self.fifteenDaysBeforeDate = dateFormatter.string(from: fifteenDaysBeforeToday)
        
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            self.presenter.getCashFlowReportDataFromServer()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }

}

//Mark: Floaty Delegate
extension CashFlowReportViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}

//DatePicker
extension CashFlowReportViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            
            self.presenter.getCashFlowReportSearchDataFormServer(startDate: startDate, endDate: endDate)
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}

//Mark: Tableview Delegate and DataSource
extension CashFlowReportViewController : UITableViewDelegate, UITableViewDataSource {
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(CFRCashRecieveCell.nib, forCellReuseIdentifier: CFRCashRecieveCell.identifier)
        self.tableView.register(CFRCashPaidExpenseCategoryCell.nib, forCellReuseIdentifier: CFRCashPaidExpenseCategoryCell.identifier)
        self.tableView.register(CFRCashPaidOthersCell.nib, forCellReuseIdentifier: CFRCashPaidOthersCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        //self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == CFR.expenseCategory.rawValue{
            return 30.0
        }
        return 3.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case CFR.cashReceive.rawValue:
            return 1
        case CFR.expenseCategory.rawValue:
            if let categories = self.totalExpenseWithCategory, categories.count > 0 {
                return categories.count
            }else{
                return 0
            }
        case CFR.cashPaid.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case CFR.cashReceive.rawValue:
            let cell : CFRCashRecieveCell = tableView.dequeueReusableCell(withIdentifier: CFRCashRecieveCell.identifier, for: indexPath) as! CFRCashRecieveCell
            
            var totalSale = 0
            var totalWithdraw = 0
            var loan = 0
            var investment = 0
            
            
            if let saleOperation = self.saleOperation {
                cell.totalSaleLbl.text = LanguageManager.Sales
                cell.totalSaleAmountLbl.text = "\(saleOperation.totalSale)" + Constants.currencySymbol
                totalSale = saleOperation.totalSale
            }
            
            if let dueOperation = self.dueOperation {
                cell.receivableLbl.text = LanguageManager.Receivable
                cell.receivableAmountLbl.text = "\(dueOperation.totalWithdraw)" + Constants.currencySymbol
                totalWithdraw = dueOperation.totalWithdraw
            }
            
            if let loanOperation = self.loanOperation {
                cell.collectionOfLoanLbl.text = LanguageManager.CollectionOfLoan
                cell.collectionOfLoanAmountLbl.text = "\(loanOperation.totalTake)" + Constants.currencySymbol
                loan = Int(loanOperation.totalTake)
            }
            
            if let investoOperation = self.investorOperation {
                cell.collectionOfInvestLbl.text = LanguageManager.CollectionOfInvest
                cell.collectionOfInvestAmount.text = "\(String(describing: investoOperation.totalTake))" + Constants.currencySymbol
                investment = Int(investoOperation.totalTake)
            }
            
            self.totalCashReceive = Double(totalSale + totalWithdraw + loan + investment)
            
            
            cell.totalCashReceiveLbl.text = LanguageManager.TotalCashReceive
            cell.totalCashReceiveAmountLbl.text = "\(totalCashReceive)" + Constants.currencySymbol
            
            return cell
            
        case CFR.expenseCategory.rawValue:
            let cell : CFRCashPaidExpenseCategoryCell = tableView.dequeueReusableCell(withIdentifier: CFRCashPaidExpenseCategoryCell.identifier, for: indexPath) as! CFRCashPaidExpenseCategoryCell
            
            
            
            if let totalExpenseWithCategory = self.totalExpenseWithCategory, totalExpenseWithCategory.count > 0 {
                let item = totalExpenseWithCategory[indexPath.row]
                cell.expenseCategoryLbl.text = item.category
                cell.expenseCategoryAmountLbl.text = "\(item.totalAmount)" + Constants.currencySymbol
//                self.sumOfExpenses = totalExpenseWithCategory.map({$0.totalAmount}).reduce(0, +)
                
            }
            
            return cell
            
        case CFR.cashPaid.rawValue:
            let cell : CFRCashPaidOthersCell = tableView.dequeueReusableCell(withIdentifier: CFRCashPaidOthersCell.identifier, for: indexPath) as! CFRCashPaidOthersCell
            
            var costOfSale = 0
            var payable = 0
            var deliveryCommission = 0
            var loanPaid = 0
            var payBack = 0
            
            if let totalExpenseWithCategory = self.totalExpenseWithCategory, totalExpenseWithCategory.count > 0 {
                self.sumOfExpenses = totalExpenseWithCategory.map({$0.totalAmount}).reduce(0, +)
            }else{
                self.sumOfExpenses = 0
            }
            
            if let sumOfExpenses = self.sumOfExpenses{
                cell.totalExpenseLbl.text = LanguageManager.TotalExpense
                cell.totalExpenseAmountLbl.text = "\(sumOfExpenses)" + Constants.currencySymbol
            }

            if let saleOperation = self.saleOperation{
                costOfSale = saleOperation.costOfSale
                deliveryCommission = saleOperation.deliveryCommision
                cell.costOfSaleLbl.text = LanguageManager.CostOfSale
                cell.costOfSaleAmountLbl.text = "\(costOfSale)" + Constants.currencySymbol
                cell.deliveryCommissionLbl.text = LanguageManager.DeliveryCommission
                cell.deliveryCommissionAmountLbl.text = "\(deliveryCommission)" + Constants.currencySymbol
            }

            if let payableOperation = self.payableOperation{
                payable = Int(payableOperation.totalPayable)
                cell.payableLbl.text = LanguageManager.Payable
                cell.payableAmountLbl.text = "\(payable)" + Constants.currencySymbol
            }

            if let loanOperation = self.loanOperation{
                loanPaid = Int(loanOperation.totalPaid)
                cell.loanPaidLbl.text = LanguageManager.LoanPaid
                cell.loanPaidAmountLbl.text = "\(loanPaid)" + Constants.currencySymbol
            }

            if let investoryOpration = self.investorOperation{
                payBack = Int(investoryOpration.totalPaid)
                cell.payBackLbl.text = LanguageManager.PayBack
                cell.payBackAmountLbl.text = "\(payBack)" + Constants.currencySymbol
            }

            self.totalCashPaid = Double(costOfSale + payable + deliveryCommission + loanPaid + payBack)

            cell.totalCashPaidLbl.text = LanguageManager.TotalCashPaid
            cell.totalCashPaidAmountLbl.text = "\(totalCashPaid)" + Constants.currencySymbol

            let cashInHand = self.totalCashReceive - self.totalCashPaid

            cell.cashInHandLbl.text = LanguageManager.CashInHand
            cell.cashInHandAmountLbl.text = "\(cashInHand)" + Constants.currencySymbol
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == CFR.expenseCategory.rawValue{
            
            if let item = self.totalExpenseWithCategory, item.count > 0 {
                let header = UIView(frame: CGRect(x: 0, y:0, width: self.tableView.frame.width, height: 20))
                let label = UILabel(frame: CGRect(x: 10, y: 5, width: header.frame.width, height: 20))
                
                label.text = LanguageManager.Expense
                
                label.textAlignment = .left
                label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
                header.addSubview(label)
                return header
            }
            
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case CFR.cashReceive.rawValue:
            return 200.0
        case CFR.expenseCategory.rawValue:
            return 44.0
        case CFR.cashPaid.rawValue:
            return 326.0
        default:
            return 0
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func totalExpenseCalculation(item : [TotalExpenseWithCategory]){
//        let sum = item.flatMap { $0["total_amount"] as? Int }.reduce(0, combine: +)
//        for key in item {
//            let sum = item.map({$0.value}).reduce(0, +)
//            resultArray.append(Item(value:sum, name:key))
//        }
       // item{$0.}
        
    }
    
}

//Mark: Api Delegate
extension CashFlowReportViewController : CashFlowReportViewDelegate{
    func setCashFlowReportData(data: CashFlowDataMapper) {
        guard let cashFlowReport = data.cashFlowReport else {
            return
        }
        self.saleOperation = cashFlowReport.saleOperation
        self.dueOperation = cashFlowReport.dueOperation
        self.payableOperation = cashFlowReport.payableOperation
        self.investorOperation = cashFlowReport.investorOperation
        self.loanOperation = cashFlowReport.loanOperation
        self.totalExpenseWithCategory = cashFlowReport.totalExpenseWithCategory
        self.totalExpense = cashFlowReport.totalExpense
        
        self.refreshTableView()
        
    }
    
    func onFailed(data: String) {
        
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getCashFlowReportDataFromServer()
    }
    
}
