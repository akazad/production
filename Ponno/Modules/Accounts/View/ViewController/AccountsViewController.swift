//
//  AccountsViewController.swift
//  Ponno
//
//  Created by a k azad on 22/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
//import Font_Awesome_Swift

enum AccountsBook : String{
    case CashDrawer = "Cash Drawer"
    case Loan = "Loan"
    case Invest = "Invest"
    case Asset = "Assets"
    case GrossNetProfit = "Gross & NetProfit"
    case CashFlow = "CashFlow Report"
    case BalanceSheet = "Balance Sheet"
    case MobileBanking = "Mobile Banking"
}

class AccountsViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var collectionViewItem : [String] = ["Cash Drawer","Loan", "Invest", "Assets","Gross & NetProfit","CashFlow Report","Balance Sheet","Mobile Banking"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.configureCollectionView()
        //print("Hola")
        self.initialSetup()
        
        
    }
    
    func initialSetup(){
        self.title = LanguageManager.Accounts
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

}

extension AccountsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(AccountsCollectionViewCell.nib, forCellWithReuseIdentifier: AccountsCollectionViewCell.identifier)
        self.automaticallyAdjustsScrollViewInsets = false
        self.collectionView.bounces = false
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return self.collectionViewItem.count
        //return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : AccountsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: AccountsCollectionViewCell.identifier, for: indexPath) as! AccountsCollectionViewCell
       
        
        if self.collectionViewItem[indexPath.row] == "Cash Drawer" {
            cell.titleLbl.text = LanguageManager.CashDrawer
            cell.imageView.image = UIImage(named: "cashDrawer")
        }else if self.collectionViewItem[indexPath.row] == "Loan" {
            cell.titleLbl.text = LanguageManager.Loan
            cell.imageView.image = UIImage(named: "loan")
        }else if self.collectionViewItem[indexPath.row] == "Invest" {
            cell.titleLbl.text = LanguageManager.Invest
            cell.imageView.image = UIImage(named: "Invest")
        }else if self.collectionViewItem[indexPath.row] == "Assets" {
            cell.titleLbl.text = LanguageManager.Assets
            cell.imageView.image = UIImage(named: "assets")
        }else if self.collectionViewItem[indexPath.row] == "Gross & NetProfit" {
            cell.titleLbl.text = LanguageManager.GrossAndNetProfit
            cell.imageView.image = UIImage(named: "gross&NetProfit")
        }else if self.collectionViewItem[indexPath.row] == "CashFlow Report" {
            cell.titleLbl.text = LanguageManager.CashFlowReport
            cell.imageView.image = UIImage(named: "cashFlow")
        }else if self.collectionViewItem[indexPath.row] == "Balance Sheet" {
            cell.titleLbl.text = LanguageManager.BalanceSheet
            cell.imageView.image = UIImage(named: "balanceSheet")
        }else if self.collectionViewItem[indexPath.row] == "Mobile Banking" {
            cell.titleLbl.text = LanguageManager.MobileBanking
            cell.imageView.image = UIImage(named: "mobileBanking")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //        let width  = collectionView.bounds.width/2.0
        //        return CGSize(width: width, height: 118.0)
        //        let screenWidth = UIScreen.main.bounds.width
        //        let scaleFactor = (screenWidth / 2) - 8
        //
        //        return CGSize(width: scaleFactor, height: 118.0)
        let noOfCellsInRow = 2
        //let widthPerItem = collectionView.frame.width / 2.0
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.minimumInteritemSpacing
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        //let width = collectionView.bounds.width/2.0
        
        
        return CGSize(width: size, height: 115)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        navigate(to: self.collectionViewItem[indexPath.row])
        
    }

    func navigate(to : String) {
        switch to {
        case AccountsBook.CashDrawer.rawValue:
            self.navigateToCashDrawerViewController()
        case AccountsBook.Loan.rawValue:
            self.navigateToLoanerListViewController()
        case AccountsBook.Invest.rawValue:
            self.navigateToInvestorListViewController()
        case AccountsBook.Asset.rawValue:
            self.navigateToAssetPropertyListViewController()
        case AccountsBook.GrossNetProfit.rawValue:
            self.navigateToGrossNetProfitViewController()
        case AccountsBook.CashFlow.rawValue:
            self.navigateToCashFlowReportViewController()
        case AccountsBook.BalanceSheet.rawValue:
            self.navigateToBalanceSheetViewController()
        case AccountsBook.MobileBanking.rawValue:
            self.navigateToMobileBankingViewController()
            //self.showAlert(title: LanguageManager.WorkingOnProgress, message: LanguageManager.SorryForTheInterupt)
        default:
            break
        }
    }
    
}

extension AccountsViewController {
    func navigateToCashDrawerViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "CashDrawer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CashDrawerViewController") as! CashDrawerViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToGrossNetProfitViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Accounts", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "GrossNetProfitViewController") as! GrossNetProfitViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToLoanerListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Loan", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoanerListViewController") as! LoanerListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToInvestorListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Investor", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "InvestorsListViewController") as! InvestorsListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAssetPropertyListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Assets", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AssetPropertyListViewController") as! AssetPropertyListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func navigateToCashFlowReportViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Accounts", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CashFlowReportViewController") as! CashFlowReportViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToBalanceSheetViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Accounts", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "BalanceSheetViewController") as! BalanceSheetViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToMobileBankingViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Accounts", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "MobileBankListViewController") as! MobileBankListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
