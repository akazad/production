//
//  PerMobileBankListViewController.swift
//  Ponno
//
//  Created by a k azad on 2/2/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class PerMobileBankListViewController: UIViewController {

    @IBOutlet weak var dateSearchHeight: NSLayoutConstraint!
    @IBOutlet weak var datePickerTextField: UITextField!{
        didSet{
            datePickerTextField.placeholder = LanguageManager.From
        }
    }
    @IBOutlet weak var toDatePickerTextField: UITextField!{
        didSet{
            toDatePickerTextField.placeholder = LanguageManager.To
        }
    }
    @IBOutlet weak var searchBtn: UIButton!{
        didSet{
            searchBtn.setTitle(LanguageManager.Search, for: .normal)
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate var presenter = PerMobileBankingListPresenter(service: AccountsService())
    
    var perMobileBankingList : [PerMobileBankList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var summery : [Summary]?
    
    var mobileBankId: Int?
    //var mobileBankName : String?
    
    var datePicker = UIDatePicker()
    
    var searchIsActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isLoading : Bool = false
    var lastPageNo: Int = 0
    var currentPage : Int = 1
    var searchEnable : Bool = false
    var searchPage: Int = 1
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.configureCollectionView()
       self.configureTableView()
       self.initialSetUp()
       self.showStartDatePicker()
       self.showEndDatePicker()
    }
    
    func initialSetUp(){
        self.searchBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
        self.setBarButton()
        self.dateSearchHeight.constant = 0.0
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.attachPresenter()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setBarButton(){
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "dateSearch"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        navigationItem.setRightBarButtonItems([barButton], animated: true)
    }
    
    @objc func onSearch(sender: UIButton){
        if searchEnable == false{
            self.dateSearchHeight.constant = 30.0
            self.searchEnable = !searchEnable
        }else{
            self.dateSearchHeight.constant = 0.01
            searchEnable = !searchEnable
        }
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(PerMobileBankListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.perMobileBankingList = []
        self.currentPage = 1
        self.searchIsActive = false
        self.datePickerTextField.text = ""
        self.toDatePickerTextField.text = ""
        if let id = self.mobileBankId{
            self.presenter.getPerMobileBankingListDataFromServer(id: id, page: self.currentPage, startDt: "default", endDt: "default")
        }
        
        refreshControl.endRefreshing()
    }

}

//Mark: Date Search
extension PerMobileBankListViewController {
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: LanguageManager.StartingDateIsRequired, message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: LanguageManager.EndDateIsRequired, message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            
            self.perMobileBankingList = []
            self.currentPage = 1
            self.searchIsActive = true
            
            if let id = self.mobileBankId{
                self.presenter.getPerMobileBankingListDataFromServer(id: id, page: self.currentPage, startDt: startDate, endDt: endDate)
            }
            
            
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: #selector(donedatePicker))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
        
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
    
}

//MARK: CollectionView Delegate And DataSource
extension PerMobileBankListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(SalesSummeryCell.nib, forCellWithReuseIdentifier: SalesSummeryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let item = self.summery, item.count > 0 else {
            return 0
        }
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SalesSummeryCell = collectionView.dequeueReusableCell(withReuseIdentifier: SalesSummeryCell.identifier, for: indexPath) as! SalesSummeryCell
        
        guard let item = self.summery, item.count > 0 else{
            return cell
        }
        let summeryList = item[indexPath.row % item.count]
        
        cell.SalesSummeryName.text = summeryList.title
        cell.SalesSummeryNumber.text = summeryList.value

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 60.0)
    }
    
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.summery, list.count > 0 else{
            return
        }
        let listCount = list.count
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
}

// Mark: TableView Delegate and DataSource
extension PerMobileBankListViewController : UITableViewDelegate, UITableViewDataSource{
    
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PerPaymentMethodCell.nib, forCellReuseIdentifier: PerPaymentMethodCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.tableView.estimatedRowHeight = 60.0
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.tableView.addSubview(self.refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.perMobileBankingList.count > 0{
            return self.perMobileBankingList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : PerPaymentMethodCell = tableView.dequeueReusableCell(withIdentifier: PerPaymentMethodCell.identifier, for: indexPath) as! PerPaymentMethodCell
        cell.selectionStyle = .none
        if self.perMobileBankingList.count > 0 {
            let paymentItem = self.perMobileBankingList[indexPath.row]
            
            cell.perMobileBankingList = paymentItem
            
            if isLoading == false && indexPath.row == self.perMobileBankingList
                .count - 1{
                self.isLoading = true
                if searchIsActive == false{
                    if self.currentPage < self.lastPageNo {
                        self.currentPage += 1
                        if let id = self.mobileBankId{
                            self.presenter.getPerMobileBankingListDataFromServer(id: id, page: self.currentPage, startDt: "defualt", endDt: "default")
                        }
                    }
                }else{
                    if self.searchPage < self.lastPageNo {
                        guard let startDate = self.datePickerTextField.text else{
                            return cell
                        }
                        guard let endDate = self.toDatePickerTextField.text else{
                            return cell
                        }
                        self.searchPage += 1
                        if let id = self.mobileBankId{
                            self.presenter.getPerMobileBankingListDataFromServer(id: id, page: searchPage, startDt: startDate, endDt: endDate)
                        }
                        
                    }
                }
                
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.perMobileBankingList.count > 0{
            let item = self.perMobileBankingList[indexPath.row]
            if let id = item.id{
                self.navigateToSaleDetailsVC(iD : id)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func navigateToSaleDetailsVC(iD : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleDetailsViewController") as! SaleDetailsViewController
        viewController.iD = iD
        viewController.state = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension PerMobileBankListViewController : PerMobileBankingListViewDelegate{
    func setMobileBankData(data: PerMobileBankingListDataMapper) {
        
        self.summery = data.summary
        self.collectionView.reloadData()
        self.setUpAutoScroll()
        
        guard let list = data.perMobileBankingList, list.count > 0 else{
            return
        }
        //self.mobileBankName = data.mobileBank
        self.title = data.mobileBank
        self.perMobileBankingList += list
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        if let id = self.mobileBankId{
            self.currentPage = 1
            self.perMobileBankingList = []
            self.presenter.getPerMobileBankingListDataFromServer(id: id, page: self.currentPage, startDt: "default", endDt: "default")
        }
        
    }
}

