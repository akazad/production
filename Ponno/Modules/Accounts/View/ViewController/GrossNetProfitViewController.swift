//
//  AccountsViewController.swift
//  Ponno
//
//  Created by a k azad on 19/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//


import UIKit
import Floaty


class GrossNetProfitViewController: UIViewController {
    
    private var presenter = AccountsPresenter(service: AccountsService())

    @IBOutlet weak var searchHeight: NSLayoutConstraint!
    @IBOutlet weak var segmentControll: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePickerTextField: UITextField!
    @IBOutlet weak var toDatePickerTextField: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    
    var datePicker = UIDatePicker()
    
    var searchEnable : Bool = false
    
    private var accountsGrossProfit : [GrossProfit]?
    private var accountsNetProfit : [NetProfit]?
    
    private var indexNumber: Int?
    
    var todayDate : String?
    var fifteenDaysBeforeDate : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //addSlideMenuButton()
        self.setNavigationBarTitle()
        //self.attachPresenter()
        //self.addFloaty()
        self.configureTableView()
        self.initialSegmentSetUp()
        self.showStartDatePicker()
        self.showEndDatePicker()
        self.setBarButton()
    }
    
   
    func setNavigationBarTitle(){
        self.title = "Accounts"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0))]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.searchHeight.constant = 0.0
    }
    
    
    func setBarButton(){
        
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "dateSearch"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onSearch(sender:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func onSearch(sender: UIButton){
        if searchEnable == false{
            self.searchHeight.constant = 30.0
            self.searchEnable = !searchEnable
        }else{
            self.searchHeight.constant = 0.0
            searchEnable = !searchEnable
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetUp()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.attachPresenter()
    }
    
    func initialSetUp(){
        self.generatingDate()
        self.searchBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
//        self.datePickerTextField.text = self.fifteenDaysBeforeDate
//        self.toDatePickerTextField.text = self.todayDate
        self.datePickerTextField.placeholder = "From"
        self.toDatePickerTextField.placeholder = "To"
    }
    
    func initialSegmentSetUp(){
        self.segmentControll.addTarget(self, action: #selector(onSelectedSegment(segment: )), for: .valueChanged)
    }
    
    @objc func onSelectedSegment(segment: UISegmentedControl){
        if segment.selectedSegmentIndex == 0 {
            self.indexNumber = 0
        }else{
             self.indexNumber = 1
        }
        self.tableView.reloadData()

    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    func generatingDate(){
        
        let today = Date()
        print(today)
        
        let fifteenDaysBeforeToday = Calendar.current.date(byAdding: .day, value: -15, to: today)!
        print(fifteenDaysBeforeToday)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        self.todayDate = dateFormatter.string(from: Date())
        self.fifteenDaysBeforeDate = dateFormatter.string(from: fifteenDaysBeforeToday)
        
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.presenter.getAccountsDataFromServer()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(GrossNetProfitViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
//        self.currentPage = 1
//        self.acc = []
        self.presenter.getAccountsDataFromServer()
        refreshControl.endRefreshing()
    }
    
}

//DatePicker
extension GrossNetProfitViewController {
    
    func showStartDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: "Select Start Date", style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)

        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
        
        
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if datePickerTextField.isFirstResponder{
            datePickerTextField.text = formatter.string(from: datePicker.date)
            toDatePickerTextField.becomeFirstResponder()
        }
        else {
            toDatePickerTextField.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
        }
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidated() -> Bool {
        if datePickerTextField.text == "" {
            showAlert(title: "Starting date is required", message: "")
            return false
        }else if toDatePickerTextField.text == "" {
            showAlert(title: "End date is required", message: "")
            return false
        }
        return true
    }
    
    @objc func onSubmit(sender: UIButton){
        if isValidated(){
            guard let startDate = self.datePickerTextField.text else{
                return
            }
            guard let endDate = self.toDatePickerTextField.text else{
                return
            }
            
            self.presenter.getAccountsSearchData(startDate: startDate, endDate: endDate)
        }
    }
    
    func showEndDatePicker(){
        datePicker.datePickerMode = .date
        
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let endDate = UIBarButtonItem(title: LanguageManager.SelectEndDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,endDate,spaceButton,cancelButton], animated: false)
 
        toDatePickerTextField.inputAccessoryView = toolbar
        toDatePickerTextField.inputView = datePicker
    }
}


//Mark: Floaty Delegate
extension GrossNetProfitViewController : FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToPurchasableProductListViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}

//Mark: TableView Delegate And Data Source
extension GrossNetProfitViewController: UITableViewDelegate, UITableViewDataSource{

    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(GrossProfitChartCell.nib, forCellReuseIdentifier: GrossProfitChartCell.identifier)
        self.tableView.register(NetProfitChartCell.nib, forCellReuseIdentifier: NetProfitChartCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
        self.tableView.addSubview(refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch segmentControll.selectedSegmentIndex {
        case 0:
            let cell : GrossProfitChartCell = tableView.dequeueReusableCell(withIdentifier: GrossProfitChartCell.identifier, for: indexPath) as! GrossProfitChartCell
            cell.selectionStyle = .none
            cell.grossProfit = self.accountsGrossProfit 
            return cell
        case 1:
            let cell: NetProfitChartCell = tableView.dequeueReusableCell(withIdentifier: NetProfitChartCell.identifier, for: indexPath)as! NetProfitChartCell
            cell.selectionStyle = .none
            cell.netProfit = self.accountsNetProfit
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500.0
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
}

//Mark: Api Delegate
extension GrossNetProfitViewController : AccountsViewDelegate{
    func setAccountsData(data: AccountsDataMapper) {
        guard let grossProfit = data.grossProfit, let netProfit = data.netProfit else{
            return
        }
        self.accountsGrossProfit = grossProfit
        self.accountsNetProfit = netProfit
        self.refreshTableView()
    }
    
    func setAccountsSearchData(data: AccountsDataMapper) {
        guard let grossProfit = data.grossProfit, let netProfit = data.netProfit else{
            return
        }
        self.accountsGrossProfit = grossProfit
        self.accountsNetProfit = netProfit
        self.refreshTableView()
    }
    
    func onFailed(data: String) {
        self.searchAlert(title: "No Information Found", message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.presenter.getAccountsDataFromServer()
    }
}


