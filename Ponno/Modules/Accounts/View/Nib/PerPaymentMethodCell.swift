//
//  PerPaymentMethodCell.swift
//  Ponno
//
//  Created by a k azad on 26/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class PerPaymentMethodCell: UITableViewCell {

  
    @IBOutlet weak var cardView: UIView!{
        didSet{
            self.setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var invoiceLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var accountNumberLbl: UILabel!
    
    var perMobileBankingList : PerMobileBankList?{
        didSet{
            if let item = self.perMobileBankingList{
                invoiceLbl.text = item.invoice
                dateLbl.text = item.createdAt.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd_HH_mm_ss.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
                totalLbl.text = LanguageManager.Cash + " : " + item.cash + Constants.currencySymbol
                accountNumberLbl.text = LanguageManager.Account + " : " + item.paymentNumber
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: PerPaymentMethodCell.self)
    }
    
}
