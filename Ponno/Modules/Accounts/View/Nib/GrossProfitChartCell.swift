//
//  GrossProfitChartCell.swift
//  Ponno
//
//  Created by a k azad on 29/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import AnyChartiOS

class GrossProfitChartCell: UITableViewCell {
    
//    @IBOutlet weak var chartView: BarChartView!
    
    @IBOutlet weak var verticalChartView: AnyChartView!
    var grossProfit : [GrossProfit]?{
        didSet{
            DispatchQueue.main.async {
                self.setUpChart()
            }
        }
    }
    
    
    
//    {
//        didSet{
//            guard let data = self.grossProfit, let barEntries = data.date, barEntries.count > 0, let colors = data.color, colors.count > 0,  let labels = data., labels.count > 0 else {
//                return
//            }
//
//            self.colors = colors
//            self.timeslots = labels
//            self.grossProfitToday = barEntries
//
//            DispatchQueue.main.async {
//                self.setUpChart()
//            }
//        }
//    }
//
//    var grossProfitToday : [AccountsGrossProfitBarEntries] = []
//    var labels : [String] = []
//    var colors : [String] = []
//    var timeslots : [String] = []

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: GrossProfitChartCell.self)
    }
    
    func setUpChart(){
        let chartView = AnyChartView()
        verticalChartView.addSubview(chartView)
        chartView.translatesAutoresizingMaskIntoConstraints = false
        chartView.centerXAnchor.constraint(equalTo: verticalChartView.centerXAnchor).isActive = true
        chartView.centerYAnchor.constraint(equalTo: verticalChartView.centerYAnchor).isActive = true
        chartView.widthAnchor.constraint(equalTo: verticalChartView.widthAnchor).isActive = true
        chartView.heightAnchor.constraint(equalTo: verticalChartView.heightAnchor).isActive = true

        let chart = AnyChart.vertical()

        chart.noData().label().enabled(enabled: true)
        
//        chart.animation(settings: true)
//            .title(settings: "Vertical Combination of Bar and Jump Line Chart")

        guard let grossProfit = self.grossProfit , grossProfit.count > 0  else {
            return
        }
        
        //let data: Array<DataEntry>
        for item in grossProfit{
            
            guard let dateString = item.date, let grossProfit = item.gross else{
                return
            }
            
           let data: Array<DataEntry> = [
            CustomDataEntry(x: dateString, value: Double(grossProfit), color: item.color ?? "")
                
            ]
            
            let set = anychart.data.Set().instantiate()
            set.data(data: data)
            let barData = set.mapAs(mapping: "{x: 'x', value: 'value'}")
            //let jumpLineData = set.mapAs(mapping: "{x: 'x', value: 'jumpLine'}")
            
            let bar = chart.bar(data: barData)
            bar.labels().format(token: "{%Value}" + Constants.currencySymbol)
            bar.color(color: "#e2f9c2")
            //bar.labels().background().stroke("#663399")
            
//            bar.labels().background().stroke("#663399")
//            bar.selected().labels().background().stroke("Green")
            
//            let jumpLine = chart.jumpLine(data: jumpLineData)
//            jumpLine.stroke(settings: "2 #60727B")
//            jumpLine.labels().enabled(enabled: false)
            
            chart.yScale().minimum(minimum: 0)
            //chart.labels().background().stroke(color: <#T##String#>)
            
            chart.labels(settings: true)
            
            chart.tooltip()
                .displayMode(value: anychart.enums.TooltipDisplayMode.UNION)
                .positionMode(mode: anychart.enums.TooltipPositionMode.POINT)
            
            chart.tooltip().titleFormat(format: "{%x}")
            
            chart.interactivity().hoverMode(mode: anychart.enums.HoverMode.BY_X)
            
            chart.xAxis(settings: true)
            chart.yAxis(settings: true)
            chart.yAxis(index: 0).labels().format(token: "{%Value} " + Constants.currencySymbol)
            
            chart.animation(settings: true)
            
            chartView.setChart(chart: chart)
            
        }

        
    }
    
    class CustomDataEntry: ValueDataEntry {
        init(x: String, value: Double, color : String) {
            super.init(x: x, value : value)
            
           //setValue(value, forKey: color)
            //setValue(key: "jumpLine", value: jumpLine)
        }
    }
    
//    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
//        let index = Int(value)
//        if self.timeslots.indices.contains(index){
//            return timeslots[Int(value)]
//        }
//        else{
//            return ""
//        }
//    }
    
    
//    func setUpChart() {
//        var dataEntries : [BarChartDataEntry] = []
//        guard self.grossProfitToday.count > 0 else{
//            return
//        }
//
////        for i in 0..<self.grossProfitToday.count{
////            let profit = self.grossProfitToday[i].stepValues
////            guard let grossProfitItem = profit else{
////                return
////            }
////            let data = BarChartDataEntry(x: counter, y: Double(grossProfitItem))
////            dataEntries.append(data)
////            counter += 1.0
////        }
//
//
//        for i in 0..<self.grossProfitToday.count{
//            let barEntry = self.grossProfitToday[i]
//
//            guard let index = barEntry.index else {return}
//            guard let stepValue = barEntry.stepValue, let stepValueDouble = Double(stepValue) else {return}
//            let data = BarChartDataEntry(x: index , yValues: [stepValueDouble])
//            dataEntries.append(data)
//        }
//        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "")
//        let chartData = BarChartData(dataSet: chartDataSet)
//        chartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
//        chartDataSet.colors = [UIColor(red:0.89, green:0.98, blue:0.76, alpha:1.0)]
//
//        chartView.data = chartData
//        chartData.highlightEnabled = false
//        chartView.rightAxis.enabled = false
//        //chartView.xAxis.valueFormatter = self
//        chartView.xAxis.labelCount = self.timeslots.count
//        chartView.xAxis.labelPosition = .bottom
//        chartView.xAxis.labelRotationAngle = 0
//        chartView.xAxis.drawGridLinesEnabled = false
//        chartView.xAxis.granularityEnabled = true
//        chartView.xAxis.granularity = 1.0
//        chartView.legend.enabled = false
//        chartView.setVisibleXRangeMaximum(3.0)
//
//
//        let leftAxis = chartView.leftAxis
//        leftAxis.drawLabelsEnabled = false
//        leftAxis.drawGridLinesEnabled = false
//
//        chartView.drawGridBackgroundEnabled = false
////        chartView.delegate = self
//    }
    
   
}
