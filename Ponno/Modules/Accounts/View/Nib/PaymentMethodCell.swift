//
//  PaymentMethodCell.swift
//  Ponno
//
//  Created by a k azad on 26/1/20.
//  Copyright © 2020 Ponno. All rights reserved.
//

import UIKit

class PaymentMethodCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            self.setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var paymentMethodLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var paymentLbl: UILabel!
    @IBOutlet weak var popUpBtn: UIButton!
    
    var mobileBank : MobileBankingList?{
        didSet{
            if let item = self.mobileBank{
                paymentMethodLbl.text = item.name
                totalLbl.text = LanguageManager.Total + " : " + (item.total?.toString() ?? "0") + Constants.times
                paymentLbl.text = "Payment :" + (item.totalAmount?.toString() ?? "0") + Constants.currencySymbol
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: PaymentMethodCell.self)
    }
    
    
}
