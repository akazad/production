//
//  NetProfitChartCell.swift
//  Ponno
//
//  Created by a k azad on 31/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import AnyChartiOS

class NetProfitChartCell: UITableViewCell  { 
    
    @IBOutlet weak var chartView: AnyChartView!
    
    var netProfit : [NetProfit]?{
        didSet{
            self.setupChart()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: NetProfitChartCell.self)
    }
    
    func setupChart(){
        let anyChartView = AnyChartView()
        chartView.addSubview(anyChartView)
        anyChartView.translatesAutoresizingMaskIntoConstraints = false
        anyChartView.centerXAnchor.constraint(equalTo: chartView.centerXAnchor).isActive = true
        anyChartView.centerYAnchor.constraint(equalTo: chartView.centerYAnchor).isActive = true
        anyChartView.widthAnchor.constraint(equalTo: chartView.widthAnchor).isActive = true
        anyChartView.heightAnchor.constraint(equalTo: chartView.heightAnchor).isActive = true
        
        
        guard let netProfit = self.netProfit, netProfit.count > 0 else{
            return
        }
        
        
        var data: Array<DataEntry>  = []
        
        for item in netProfit {
            switch item.label {
            case "sale":
                data.append(CustomDataEntry(x: "Sale", displayValue: netProfit[0].displayValue))
            //break
            case "purchase":
                data.append(CustomDataEntry(x: "Purchase", displayValue: (-1 * netProfit[1].displayValue)))
            // break
            case "gross_profit":
                let end = DataEntry()
                end.setValue(key: "x", value: "Gross")

                data.append(end)
            //break
            case "delivery_commission":
                data.append(CustomDataEntry(x: "Delivery", displayValue: (-1 * netProfit[3].displayValue)))
            // break
            case "expense":
                data.append(CustomDataEntry(x: "Expense", displayValue: (-1 * netProfit[4].displayValue)))
            //break
            case "net_profit":
                
                let end1 = DataEntry()
                end1.setValue(key: "x", value: "Net")

                data.append(end1)
                
            default:
                break
            }
        }
        
        let chart = AnyChart.waterfall()
        
        let set = anychart.data.Set().instantiate()
        set.data(data: data)
        
        chart.yScale().minimum(minimum: 0)
        //chart.xScale()
        
        chart.labels(settings: true)
        chart.legend(settings: false)
        chart.yAxis(settings: false)
        chart.tooltip(settings: false)
        
        chart.connectorStroke(color: "F1FFDE", thickness: 2.0, dashpattern: "-", lineJoin: .ROUND, lineCap: .ROUND)
        
        
        let series = chart.waterfall(data: set, csvSettings: "")
        
        series.normal().fill(color: "#F1FFDE", opacity: 0.3)
        series.normal().hatchFill(type: "forward-diagonal", color: "#F1FFDE", thickness: 0.5, size: 10)
        
        series.normal().fallingFill(color: "#f99094", opacity: 0.3)
        series.normal().fallingStroke(settings: "#f99094")
        
        series.hovered().fallingFill(color: "#f99094", opacity: 0.1)
        series.hovered().fallingStroke(settings: "#f99094")
        
        series.selected().fallingFill(color: "#f99094", opacity: 0.1)
        series.selected().fallingStroke(settings: "#f99094")
        
        
        series.normal().risingFill(color: "#a7e081", opacity: 0.3)
        series.normal().risingStroke(settings: "#a7e081")
        
        series.hovered().risingFill(color: "#a7e081", opacity: 0.1)
        series.hovered().risingStroke(settings: "#a7e081 2")
        
        series.selected().risingFill(color: "#a7e081", opacity: 0.5)
        series.selected().risingStroke(settings: "#a7e081 4")
        
        chart.data(data: data)
        
        anyChartView.setChart(chart: chart)
        
    }
    
    class CustomDataEntry: ValueDataEntry {
        init(x: String, displayValue: Double) {
            super.init(x: x, value: displayValue)
        }
    }
    
}
