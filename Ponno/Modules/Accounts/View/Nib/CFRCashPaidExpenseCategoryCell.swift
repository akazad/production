//
//  CFRCashPaidExpenseCategoryCell.swift
//  Ponno
//
//  Created by a k azad on 31/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class CFRCashPaidExpenseCategoryCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var expenseCategoryLbl: UILabel!
    @IBOutlet weak var expenseCategoryAmountLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: CFRCashPaidExpenseCategoryCell.self)
    }
    
}
