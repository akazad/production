//
//  AccountsCollectionViewCell.swift
//  Ponno
//
//  Created by a k azad on 22/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class AccountsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCollectionCardView(uiview: cardView)
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: AccountsCollectionViewCell.self)
    }

}
