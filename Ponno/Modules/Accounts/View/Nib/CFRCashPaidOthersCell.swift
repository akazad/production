//
//  CFRCashPaidOthersCell.swift
//  Ponno
//
//  Created by a k azad on 31/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class CFRCashPaidOthersCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var totalExpenseLbl: UILabel!
    @IBOutlet weak var totalExpenseAmountLbl: UILabel!
    @IBOutlet weak var costOfSaleLbl: UILabel!
    @IBOutlet weak var costOfSaleAmountLbl: UILabel!
    @IBOutlet weak var payableLbl: UILabel!
    @IBOutlet weak var payableAmountLbl: UILabel!
    @IBOutlet weak var loanPaidLbl: UILabel!
    @IBOutlet weak var payBackLbl: UILabel!
    @IBOutlet weak var payBackAmountLbl: UILabel!
    @IBOutlet weak var totalCashPaidLbl: UILabel!
    @IBOutlet weak var totalCashPaidAmountLbl: UILabel!
    
    @IBOutlet weak var loanPaidAmountLbl: UILabel!
    @IBOutlet weak var cashInHandLbl: UILabel!
    @IBOutlet weak var cashInHandAmountLbl: UILabel!
    @IBOutlet weak var deliveryCommissionLbl: UILabel!
    @IBOutlet weak var deliveryCommissionAmountLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: CFRCashPaidOthersCell.self)
    }
    
}
