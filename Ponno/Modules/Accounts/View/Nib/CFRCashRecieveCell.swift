//
//  CFRCashRecieveCell.swift
//  Ponno
//
//  Created by a k azad on 31/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class CFRCashRecieveCell: UITableViewCell {
    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var totalSaleLbl: UILabel!
    @IBOutlet weak var totalSaleAmountLbl: UILabel!
    @IBOutlet weak var receivableLbl: UILabel!
    @IBOutlet weak var receivableAmountLbl: UILabel!
    @IBOutlet weak var collectionOfLoanLbl: UILabel!
    @IBOutlet weak var collectionOfLoanAmountLbl: UILabel!
    @IBOutlet weak var collectionOfInvestLbl: UILabel!
    @IBOutlet weak var collectionOfInvestAmount: UILabel!
    @IBOutlet weak var totalCashReceiveLbl: UILabel!
    @IBOutlet weak var totalCashReceiveAmountLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: CFRCashRecieveCell.self)
    }
    
}
