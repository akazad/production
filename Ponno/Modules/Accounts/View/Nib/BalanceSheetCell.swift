//
//  BalanceSheetCell.swift
//  Ponno
//
//  Created by a k azad on 1/9/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class BalanceSheetCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    
    @IBOutlet weak var assetsLbl: UILabel!
    @IBOutlet weak var currentAssetsLbl: UILabel!
    @IBOutlet weak var cashLbl: UILabel!
    @IBOutlet weak var cashAmountLbl: UILabel!
    @IBOutlet weak var receivableLbl: UILabel!
    @IBOutlet weak var receivableAmountLbl: UILabel!
    @IBOutlet weak var inventoryLbl: UILabel!
    @IBOutlet weak var inventoryAmountLbl: UILabel!
    
    @IBOutlet weak var fixedAssetsLbl: UILabel!
    @IBOutlet weak var propertiesLbl: UILabel!
    @IBOutlet weak var propertiesAmountLbl: UILabel!
    @IBOutlet weak var equipmentsLbl: UILabel!
    @IBOutlet weak var equipmentsAmountLbl: UILabel!
    @IBOutlet weak var goodwillLbl: UILabel!
    @IBOutlet weak var goodwillAmountLbl: UILabel!
    @IBOutlet weak var totalAssetsLbl: UILabel!
    @IBOutlet weak var totalAssetsAmountLbl: UILabel!
    @IBOutlet weak var liabilitiesLbl: UILabel!
    @IBOutlet weak var accountPayableLbl: UILabel!
    @IBOutlet weak var accountPayableAmountLbl: UILabel!
    @IBOutlet weak var loanLbl: UILabel!
    @IBOutlet weak var loanAmountLbl: UILabel!
    
    @IBOutlet weak var totalLiabilitiesLbl: UILabel!
    @IBOutlet weak var totalLiabilitiesAmountLbl: UILabel!
    @IBOutlet weak var investmentLbl: UILabel!
    @IBOutlet weak var totalInvestmentLbl: UILabel!
    @IBOutlet weak var totalInvestmentAmountLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       self.selectionStyle = .none
       setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: BalanceSheetCell.self)
    }
    
}
