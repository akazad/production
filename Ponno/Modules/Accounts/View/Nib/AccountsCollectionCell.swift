//
//  AccountsCollectionCell.swift
//  Ponno
//
//  Created by a k azad on 22/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class AccountsCollectionCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: AccountsCollectionCell.self)
    }
    
}
