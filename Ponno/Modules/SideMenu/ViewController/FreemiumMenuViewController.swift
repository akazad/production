//
//  FreemiumMenuViewController.swift
//  Ponno
//
//  Created by a k azad on 7/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SDWebImage

enum FreemiumMenus : Int{
    case Dashboard = 101
    case Sell = 10
    case Inventory = 11
    case Expense = 13
    case Due = 14
    case Contact = 107
    case Logout = 105
}

class FreemiumMenuViewController: UIViewController{
    
    var btnMenu : UIButton!
    var delegate : SlideMenuDelegate?
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            self.setUpTableCardView(uiTableView : tableView)
        }
    }
    @IBOutlet weak var btnMenuCloseOverlay: UIButton!
    
    @IBOutlet weak var shopName: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var shopImage: UIImageView!
    @IBOutlet weak var settingsBtn: UIButton!
    
    @IBOutlet weak var profileView: UIView!{
        didSet{
            setUpCardView(uiview: profileView)
        }
    }
    var parentValue : Int?
    
    var menuList : [PermissionList] = []{
        didSet{
            self.tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.configureTableView()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.initialSetup()
        self.getShopDataFromUserDefaults()
        self.setUpInitialLanguage()
    }
    
    func initialSetup(){
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onSettingsBtnPressed))
        settingsBtn.addGestureRecognizer(tapGesture)
    }
    
    @objc func onSettingsBtnPressed(_ sender: UIGestureRecognizer){
        self.onBtnClose()
        self.navigateToSegmentedVC()
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
           btnMenu.tag = 0
           btnMenu.isHidden = false
           if (self.delegate != nil) {
               var index = Int32(sender.tag)
               if(sender == self.btnMenuCloseOverlay)
               {
                   index = -1
               }
               delegate?.slideMenuItemSelectedAtIndex(index)
           }
    
           UIView.animate(withDuration: 0.3, animations: { () -> Void in
               self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
               self.view.layoutIfNeeded()
               self.view.backgroundColor = UIColor.clear
           }, completion: { (finished) -> Void in
               self.view.removeFromSuperview()
               self.removeFromParent()
           })
       }
    
    
    func onBtnClose(){
        btnMenu.tag = 0
        btnMenu.isHidden = false
        if (self.delegate != nil) {
            let index = Int32(-1)
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }
    

}

extension FreemiumMenuViewController : UITableViewDelegate,UITableViewDataSource {
    func configureTableView(){
        self.tableView.register(NavDrawerCell.nib, forCellReuseIdentifier: NavDrawerCell.identifier)
        self.tableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NavDrawerCell = tableView.dequeueReusableCell(withIdentifier: NavDrawerCell.identifier, for: indexPath) as! NavDrawerCell
        
        if self.menuList[indexPath.row].id == 101{
            cell.drawerText.text = LanguageManager.Dashboard
        }else if self.menuList[indexPath.row].id == 10{
            cell.drawerText.text = LanguageManager.SaleBook
        }else if self.menuList[indexPath.row].id == 14{
            cell.drawerText.text = LanguageManager.DueBook
        }else if self.menuList[indexPath.row].id == 11{
            cell.drawerText.text =  LanguageManager.Inventory
        }else if self.menuList[indexPath.row].id == 13{
            cell.drawerText.text = LanguageManager.ExpenseBook
        }else if self.menuList[indexPath.row].id == 107{
            cell.drawerText.text =  LanguageManager.Contact
        }else if self.menuList[indexPath.row].id == 105{
            cell.drawerText.text = LanguageManager.Logout
        }
        
        cell.icon.image = UIImage(named: self.menuList[indexPath.row].name)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let permission = self.menuList[indexPath.row].id
        navigate(to: permission)
    }
    
    func navigate(to : Int) {
        switch to {
        case Menus.Dashboard.rawValue:
            navigateToHomeVC()
        case Menus.Sell.rawValue:
            navigateToSalesVC()
        case Menus.Inventory.rawValue:
            navigateToProductVC()
        case Menus.Expense.rawValue:
            navigateToExpenseVC()
        case Menus.Due.rawValue:
            navigateToDueCustomerVC()
        case Menus.Contact.rawValue:
            navigateToContactVC()
        case Menus.Logout.rawValue:
            setLoggedIn(status: false)
            setRememberToken(token: "")
            navigateToLoginVC()
        default:
            navigateToHomeVC()
        }
    }
    
    
}

extension FreemiumMenuViewController{
    func getShopDataFromUserDefaults(){
        
        if let user = getUserData(){
            guard let phone = user.phone else{
                return
            }
            self.phone.text = phone
        }
        
        if let shop = getShopData(){
            guard let shopName = shop.name, let shopPhoto = shop.image else {
                return
            }
            self.shopName.text = shopName
            
            shopImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            shopImage.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.shopImage + shopPhoto), placeholderImage: UIImage(named: "placeholder"))
            
        }
        
        self.menuList = []
        
        let dashboard = PermissionList(id: 101, name: "Dashboard")
        self.menuList.append(dashboard)
        
        let saleBook = PermissionList(id: 10, name: "Sell")
        self.menuList.append(saleBook)
        
        let dueBook = PermissionList(id: 14, name: "Due")
        self.menuList.append(dueBook)
        
        let inventoryBook = PermissionList(id: 11, name: "Inventory")
        self.menuList.append(inventoryBook)

        let expenseBook = PermissionList(id: 13, name: "Expense")
        self.menuList.append(expenseBook)

        let contact = PermissionList(id: 107, name: "Contact")
        self.menuList.append(contact)
        
        let logOut = PermissionList(id: 105, name: "LogOut")
        self.menuList.append(logOut)
                        
        self.tableView.reloadData()
        
    }
}

//Navigation
extension FreemiumMenuViewController{
    func navigateToSegmentedVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Shop", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SegementedVC") as! SegementedVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToHomeVC(){
        let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let navController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "FreemiumHomeViewController"))
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: {})
    }
    
    func navigateToSalesVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalesListViewController") as! SalesListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToProductVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "FreemiumProductListViewController") as! FreemiumProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToExpenseVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ExpenseViewController") as! ExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateToDueCustomerVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DueCustomerViewController") as! DueCustomerViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToContactVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Contact", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToLoginVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
