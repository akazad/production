//
//  MenuViewController.swift
//  SideMenuUsingBaseClass
//
//  Created by a k azad on 11/1/19.
//  Copyright © 2019 a k azad. All rights reserved.
//

import UIKit
import SDWebImage

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index: Int32)
}

enum SideMenus : Int{
    case Dashboard
    case TodaysSummary
    case Sales
    case Delivery
    case Due
    case Customer
    case Product
    case Expense
    case Employee
    case Purchase
    case PreOrder
    case Payable
    case Vendors
    case Accounts
    case Contact
    case LogOut
}

//enum Menus : String{
//    case Dashboard = "Dashboard"
//    case TodaysSummary = "TodaysSummary"
//    case Sell = "Sell"
//    case Delivery = "Delivery"
//    case Inventory = "Inventory"
//    case Purchase = "Purchase"
//    case Expense = "Expense"
//    case Due = "Due"
//    case Payable = "Payable"
//    case Employee = "Employee"
//    case RawMaterial = "Raw-Material"
//    case Preorder = "Preorder"
//    case Customer = "Customer"
//    case Vendor = "Vendor"
//    case Accounts = "Accounts"
//    case Contact = "Contact"
//    case Logout = "LogOut"
//}

enum Menus : Int{
    case Dashboard = 101
    case TodaysSummary = 102
    case Sell = 10
    case Delivery = 36
    case Inventory = 11
    case Purchase = 12
    case Expense = 13
    case Due = 14
    case Payable = 15
    case Employee = 16
    case RawMaterial = 37
    case Preorder = 103
    case Customer = 33
    case Vendor = 34
    case Accounts = 106
    case Contact = 107
    case Logout = 105
}

class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var btnMenu : UIButton!
    var delegate : SlideMenuDelegate?
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            self.setUpTableCardView(uiTableView : tableView) 
        }
    }
    @IBOutlet weak var btnMenuCloseOverlay: UIButton!
    
    @IBOutlet weak var shopName: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var shopAddress: UILabel!
    @IBOutlet weak var shopImage: UIImageView!
    @IBOutlet weak var settingsBtn: UIButton!
    @IBOutlet weak var settingsImage: UIImageView!
    
    @IBOutlet weak var profileView: UIView!{
        didSet{
            setUpCardView(uiview: profileView)
        }
    }
    var parentValue : Int?
    
    var menuList : [PermissionList] = []{
        didSet{
            self.tableView.reloadData()
        }
    }
    var sortedMenuList : [PermissionList] = []{
        didSet{
            self.tableView.restore()
        }
    }
    var deliveryStatus : Int?
    var inventoryStatus : Int?
    
    let sideMenuIcons = ["Dashboard","TodaysSummary","Sell","Inventory","Purchase","Preorder","Expense","Due","Delivery","Payable","Employee","Customer","Vendor","Accounts","LogOut"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        //self.getShopDataFromUserDefaults()
        
        //self.setUpViews()
        //setUpCardView(uiview : profileView)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getShopDataFromUserDefaults()
        self.setUpInitialLanguage()
        self.initialSetup()
        //self.getDeliveryStatus()
    }
    
    func initialSetup(){
        
        guard let parent = self.parentValue else{
            return
        }
        if parent == 1 {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onSettingsBtnPressed))
            settingsBtn.addGestureRecognizer(tapGesture)
        }else{
            self.settingsBtn.isHidden = true
        }
    }
    
    @objc func onSettingsBtnPressed(_ sender: UIGestureRecognizer){
        self.onBtnClose()
        self.navigateToSegmentedVC()
    }
    
    func getDeliveryStatus(){
        if let shop = getShopData(){
            guard let deliverySystemStatus = shop.deliverySystem else {
                return
            }
            self.deliveryStatus = deliverySystemStatus
        }
    }
    
    func getShopDataFromUserDefaults(){
        
        if let shop = getShopData(){
            guard let shopName = shop.name, let shopPhoto = shop.image, let deliverySystemStatus = shop.deliverySystem, let inventorySystem = shop.inventorySystem else {
                return
            }
            
            self.shopName.text = shopName
            self.deliveryStatus = deliverySystemStatus
            self.inventoryStatus = inventorySystem
            
            shopImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            shopImage.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.shopImage + shopPhoto), placeholderImage: UIImage(named: "placeholder"))

        }
        if let user = getUserData(){
            guard let phone = user.phone, let parent = user.parent else{
                return
            }
            self.phone.text = phone
            let parentInt = Int(parent)
            self.parentValue = parentInt
        }
        
        self.menuList = []
        
        let dashboard = PermissionList(id: 101, name: "Dashboard")
        self.menuList.append(dashboard)
        
        
        
        if self.parentValue == 0 {
            
            
            if let list = getPermissionList(),list.count > 0, let assignedPermissions = getPermission(), let permissions = assignedPermissions.id, permissions.count > 0  {
                
                var index = 2
                var purchaseIndex = 0
                
                for item in permissions{
                    for permission in list{
                        if permission.id == item{
                            self.menuList.append(permission)
                        }
                    }
                    index += 1
                    if item == 12{
                        purchaseIndex = index
                        
                        let preorder = PermissionList(id: 103, name: "Preorder")
                        self.menuList.insert(preorder, at: purchaseIndex)
                    }
                }
                                
                if self.deliveryStatus == 0 {
                    self.menuList = self.menuList.filter({$0.id != 36})
                }
                
                if self.inventoryStatus == 0 {
                    
                    let results = self.menuList.filter{$0.id == 37}
                    
                    if results.isEmpty == false{
                        self.menuList = self.menuList.filter({$0.id != 37})
                    }
                    
                }else if self.inventoryStatus == 1 {
                    let results = self.menuList.filter{$0.id == 37}
                    
                    if results.isEmpty == false{
                        self.menuList = self.menuList.filter({$0.id != 36})
                        self.menuList = self.menuList.filter({$0.id != 103})
                    }
//                    let rawMaterial = PermissionList(id: 37, name: "Raw Material")
//                    self.menuList.append(rawMaterial)
//
//                    self.menuList = self.menuList.filter({$0.id != 103})
                    
                }
                
            }
            
        }else if let list = getPermissionList(),list.count > 0{
            
            let todaysSummary = PermissionList(id: 102, name: "TodaysSummary")
            self.menuList.append(todaysSummary)
            
            self.menuList += list
            
            var index = 2
            var purchaseIndex = 0
            
            for item in list{
                index += 1
                if item.id == 12{
                    purchaseIndex = index
                }
            }
            
            let preorder = PermissionList(id: 103, name: "Preorder")
            self.menuList.insert(preorder, at: purchaseIndex)
            
            if self.deliveryStatus == 0 {
                self.menuList = self.menuList.filter({$0.id != 36})
            }
            
            if self.inventoryStatus == 0 {
                
                let results = self.menuList.filter{$0.id == 37}
                
                if results.isEmpty == false{
                    self.menuList = self.menuList.filter({$0.id != 37})
                }
                
            }else if self.inventoryStatus == 1 {
                let results = self.menuList.filter{$0.id == 12}
                
                if results.isEmpty == false{
                    self.menuList = self.menuList.filter({$0.id != 12})
                    self.menuList = self.menuList.filter({$0.id != 103})
                }
            }
            
//            let rawMaterial = PermissionList(id: 107, name: "RawMaterial")
//            self.menuList.append(rawMaterial)
            
            let accounts = PermissionList(id: 106, name: "Accounts")
            self.menuList.append(accounts)
        }
        
        let contact = PermissionList(id: 107, name: "Contact")
        self.menuList.append(contact)
        
        let logOut = PermissionList(id: 105, name: "LogOut")
        self.menuList.append(logOut)
        
        self.menuList = self.menuList.filter({$0.id != 17})
        
        //self.menuList = self.sortMenuList(item: self.menuList)
        
        self.tableView.reloadData()
        
    }
    
    func setUpViews(){
        guard let parent = self.parentValue else{
            return
        }
        if parent == 1 {
            _ = UITapGestureRecognizer(target: self, action: #selector(self.touchTapped(_:)))
        }else{
            self.settingsBtn.isHidden = true
        }
        
        //self.profileView.addGestureRecognizer(tap)
        
        //self.profileView.addGestureRecognizer(tap)
        
    }
    
    @objc func touchTapped(_ sender: UITapGestureRecognizer) {
        self.onBtnClose()
        self.navigateToSegmentedVC()
    }
    
//    func navigateToShopVC(){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "Shop", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "ShopViewController") as! ShopViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
    
    func navigateToSegmentedVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Shop", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SegementedVC") as! SegementedVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        btnMenu.tag = 0
        btnMenu.isHidden = false
        if (self.delegate != nil) {
            var index = Int32(sender.tag)
            if(sender == self.btnMenuCloseOverlay)
            {
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
 
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }
    
    func onBtnClose(){
        btnMenu.tag = 0
        btnMenu.isHidden = false
        if (self.delegate != nil) {
            let index = Int32(-1)
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }
    
    @IBAction func btnDashBoardTapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let VC = mainStoryBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func imageWithImage(image:UIImage,scaledToSize newSize:CGSize)->UIImage{
        
        UIGraphicsBeginImageContext( newSize )
        image.draw(in: CGRect(x: 0,y: 0,width: newSize.width,height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!.withRenderingMode(.alwaysTemplate)
    }

    fileprivate func configureTableView(){
        self.tableView.register(NavDrawerCell.nib, forCellReuseIdentifier: NavDrawerCell.identifier)
        self.tableView.separatorStyle = .none
        
       // self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuList.count
        //return sideMenuNames.count
        //return self.assignedPermissions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : NavDrawerCell = tableView.dequeueReusableCell(withIdentifier: NavDrawerCell.identifier, for: indexPath) as! NavDrawerCell
        
        if self.menuList[indexPath.row].id == 101{
            cell.drawerText.text = LanguageManager.Dashboard
        }else if self.menuList[indexPath.row].id == 102{
            cell.drawerText.text = LanguageManager.TodaysSummary
        }else if self.menuList[indexPath.row].id == 10{
            cell.drawerText.text = LanguageManager.SaleBook
        }else if self.menuList[indexPath.row].id == 36{
            cell.drawerText.text = LanguageManager.DeliveryBook
        }else if self.menuList[indexPath.row].id == 14{
            cell.drawerText.text = LanguageManager.DueBook
        }else if self.menuList[indexPath.row].id == 33{
            cell.drawerText.text = LanguageManager.CustomerBook
        }else if self.menuList[indexPath.row].id == 11{
            cell.drawerText.text =  LanguageManager.Inventory
        }else if self.menuList[indexPath.row].id == 13{
            cell.drawerText.text = LanguageManager.ExpenseBook
        }else if self.menuList[indexPath.row].id == 16{
            cell.drawerText.text = LanguageManager.EmployeeBook
        }else if self.menuList[indexPath.row].id == 12{
            cell.drawerText.text = LanguageManager.PurchaseBook
        }else if self.menuList[indexPath.row].id == 37{
            cell.drawerText.text = LanguageManager.RawMaterial
        }else if self.menuList[indexPath.row].id == 103{
            cell.drawerText.text = LanguageManager.PreOrder
        }else if self.menuList[indexPath.row].id == 15{
            cell.drawerText.text = LanguageManager.PayableBook
        }else if self.menuList[indexPath.row].id == 34{
            cell.drawerText.text = LanguageManager.VendorBook
        }else if self.menuList[indexPath.row].id == 106{
            cell.drawerText.text = LanguageManager.AccountsBook
        }else if self.menuList[indexPath.row].id == 107{
            cell.drawerText.text =  LanguageManager.Contact
        }else if self.menuList[indexPath.row].id == 105{
            cell.drawerText.text = LanguageManager.Logout
        }

        cell.icon.image = UIImage(named: self.menuList[indexPath.row].name)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //setNavigation(index: indexPath.row)
        let permission = self.menuList[indexPath.row].id
        navigate(to: permission)
    }
    
    func navigate(to : Int) {
        switch to {
        case Menus.TodaysSummary.rawValue:
            navigateToTodaysSummaryVC()
        case Menus.Dashboard.rawValue:
            navigateToHomeVC()
        case Menus.Sell.rawValue:
            navigateToSalesVC()
        case Menus.Inventory.rawValue:
            navigateToProductVC()
        case Menus.Purchase.rawValue:
            navigateToPurchaseVC()
        case Menus.Expense.rawValue:
            navigateToExpenseVC()
        case Menus.Due.rawValue:
            navigateToDueVC()
        case Menus.Payable.rawValue:
            navigateToPayableVC()
        case Menus.Employee.rawValue:
            navigateToEmployeVC()
        case Menus.RawMaterial.rawValue:
            navigateToRawMaterialListViewController()
        case Menus.Preorder.rawValue:
            navigateToPreOrderListViewController()
        case Menus.Delivery.rawValue:
            navigateToDeliveryVC()
        case Menus.Customer.rawValue:
            navigateToCustomerVC()
        case Menus.Vendor.rawValue:
            navigateToVendorsVC()
        case Menus.Accounts.rawValue:
            navigateToAccountsVC()
        case Menus.Contact.rawValue: 
            navigateToContactVC()
        case Menus.Logout.rawValue:
            setLoggedIn(status: false)
            setRememberToken(token: "")
            navigateToLoginVC()
        default:
            navigateToHomeVC()
        }
    }

//    func setNavigation(index : Int){
//
//        switch index {
//        case SideMenus.Dashboard.rawValue:
//            navigateToHomeVC()
//            break
//        case SideMenus.TodaysSummary.rawValue:
//            navigateToTodaysSummaryVC()
//            break
//        case SideMenus.Sales.rawValue:
//            navigateToSalesVC()
//            break
//        case SideMenus.Product.rawValue:
//            navigateToProductVC()
//            break
//        case SideMenus.Purchase.rawValue:
//             navigateToPurchaseVC()
//            break
//        case SideMenus.PreOrder.rawValue:
//            self.navigateToPreOrderListViewController()
//            break
//        case SideMenus.Expense.rawValue:
//            navigateToExpenseVC()
//            break
//        case SideMenus.Due.rawValue:
//            navigateToDueVC()
//            break
//        case SideMenus.Delivery.rawValue:
//            navigateToDeliveryVC()
//            break
//        case SideMenus.Payable.rawValue:
//            navigateToPayableVC()
//            break
//        case SideMenus.Employee.rawValue:
//            navigateToEmployeVC()
//            break
//        case SideMenus.Customer.rawValue:
//            navigateToCustomerVC()
//            break
//        case SideMenus.Vendors.rawValue:
//            navigateToVendorsVC()
//            break
//        case SideMenus.Accounts.rawValue:
//            self.navigateToAccountsVC()
//            break
//        case SideMenus.LogOut.rawValue:
//            setLoggedIn(status: false)
//            setRememberToken(token: "")
//            navigateToLoginVC()
//            break
//
//        default:
//            navigateToSalesVC()
//        }
//    }
    
    func navigateToTodaysSummaryVC(){
        let storyboard: UIStoryboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "DashboardViewController")
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToHomeVC(){
        let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let navController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "HomeViewController"))
        navController.modalPresentationStyle = .fullScreen
        //self.navigationController?.pushViewController(navController, animated: true)
        self.present(navController, animated: true, completion: {})
    }
    
    func navigateToSalesVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SalesListViewController") as! SalesListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToProductVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchaseVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToRawMaterialListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "RawMaterial", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RawMaterialListViewController") as! RawMaterialListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPreOrderListViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PreOrderListViewController") as! PreOrderListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToExpenseVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ExpenseViewController") as! ExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateToDueVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Due", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DueViewController") as! DueViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToDeliveryVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Delivery", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "DeliveryViewController") as! DeliveryViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToEmployeVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Employee", bundle: nil)
        let employeeViewController = storyBoard.instantiateViewController(withIdentifier: "EmployeeViewController") as! EmployeeViewController
        self.navigationController?.pushViewController(employeeViewController, animated: true)
    }
    
    func navigateToPayableVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Payable", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PayableViewController") as! PayableViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToCustomerVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Customer", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "CustomerViewController") as! CustomerViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToVendorsVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Vendors", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "VendorsViewController") as! VendorsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
//    func navigateToCashDrawerVC(){
//        let storyBoard:UIStoryboard = UIStoryboard(name: "CashDrawer", bundle: nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: "CashDrawerViewController") as! CashDrawerViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
    
    func navigateToContactVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Contact", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAccountsVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Accounts", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AccountsViewController") as! AccountsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToLoginVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension Dictionary{
    func containsValue<T : Equatable>(value : T)->Bool{
        let contains = self.contains { (k, v) -> Bool in
            
            if let v = v as? T, v == value{
                return true
            }
            return false
        }
        return contains
    }
}
