//
//  NavDrawerCell.swift
//  Ponno
//
//  Created by a k azad on 15/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class NavDrawerCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var drawerText: UILabel!
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: NavDrawerCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        //setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
