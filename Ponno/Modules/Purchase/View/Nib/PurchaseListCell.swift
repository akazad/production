//
//  PurchaseListCell.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PurchaseListCell: UITableViewCell {
    
    
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dayPurchase: UILabel!
    @IBOutlet weak var dayPurchaseAmountLabel: UILabel!
    @IBOutlet weak var dayPayableLabel: UILabel!
    
    var purchaseBookList : PurchaseBook?{
        didSet{
            if let purchaseItem = purchaseBookList{

                dateLabel.text = purchaseItem.date.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
                dayPurchase.text = LanguageManager.TotalPurchase + " : " + String(describing: purchaseItem.dayPurchase)
                dayPurchaseAmountLabel.text = LanguageManager.TotalPayable + " : " + String(describing: purchaseItem.dayPayable) + " "  + Constants.currencySymbol
                
                if let payable = Double(purchaseItem.dayPayable) {
                    setViewColor(amount: payable, view: colorView)
                }
                
                dayPayableLabel.text = String(describing: purchaseItem.dayPurchaseAmount) + " "  + Constants.currencySymbol
            }
        }
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: PurchaseListCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: backgroundCardView)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

