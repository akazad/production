//
//  AddProductInPreorderCell.swift
//  Ponno
//
//  Created by a k azad on 4/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class AddProductInPreorderCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var addProductBtn: UIButton!{
        didSet{
            addProductBtn.setTitle(LanguageManager.AddMoreProduct, for: .normal)
        }
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: AddProductInPreorderCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
