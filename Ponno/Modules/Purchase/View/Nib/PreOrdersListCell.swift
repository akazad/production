//
//  PreOrdersListCell.swift
//  Ponno
//
//  Created by a k azad on 28/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PreOrdersListCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var receiveDataLbl: UILabel!
    @IBOutlet weak var advanceAmountLbl: UILabel!
    @IBOutlet weak var AmountToBePaidLbl: UILabel!
    @IBOutlet weak var vendorLbl: UILabel!
    @IBOutlet weak var preOrderPopUpBtn: UIButton!
    @IBOutlet weak var preOrderRemarks: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: PreOrdersListCell.self)
    }
    
}
