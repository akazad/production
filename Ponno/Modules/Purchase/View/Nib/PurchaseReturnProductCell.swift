//
//  PurchaseReturnProductCell.swift
//  Ponno
//
//  Created by a k azad on 27/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PurchaseReturnProductCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            self.setUpCardView(uiview: cardView)
        }
    }
    @IBOutlet weak var productNamelabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var perUnitPriceLabel: UILabel!
    @IBOutlet weak var totalTitleLbl: UILabel!{
        didSet{
            totalTitleLbl.text = LanguageManager.Total
        }
    }
    @IBOutlet weak var totalLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: PurchaseReturnProductCell.self)
    }
}
