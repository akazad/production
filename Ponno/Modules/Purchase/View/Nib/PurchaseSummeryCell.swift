//
//  PurchaseSummeryCell.swift
//  Ponno
//
//  Created by a k azad on 6/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PurchaseSummeryCell: UICollectionViewCell {


    @IBOutlet weak var summeryNumber: UILabel!
    @IBOutlet weak var summeryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: PurchaseSummeryCell.self)
    }
}
