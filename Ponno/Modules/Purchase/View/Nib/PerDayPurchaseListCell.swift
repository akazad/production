//
//  PerDayPurchaseListCell.swift
//  Ponno
//
//  Created by a k azad on 30/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PerDayPurchaseListCell: UITableViewCell {
    
    
    @IBOutlet weak var invoiceLabel: UILabel!
    @IBOutlet weak var totalPurchaseLabel: UILabel!
    @IBOutlet weak var paidLabel: UILabel!
    @IBOutlet weak var totalPayableLabel: UILabel!
    @IBOutlet weak var vendorNameLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var popUpBtn: UIButton!
    
    var perDayPurchaselist : PurchaseList?{
        didSet{
            if let perDayPurchase = perDayPurchaselist{
                setLabelTextColor(amount: perDayPurchase.payable.toDouble(), label: totalPayableLabel)
            }
        }
    }
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: PerDayPurchaseListCell.self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
