//
//  PerDayPurchaseSummeryCell.swift
//  Ponno
//
//  Created by a k azad on 9/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PerDayPurchaseSummeryCell: UICollectionViewCell {

    @IBOutlet weak var perDaySummeryNumber: UILabel!
    @IBOutlet weak var perDaySummery: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: PerDayPurchaseSummeryCell.self)
    }
}
