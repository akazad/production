//
//  PreOrderProductCell.swift
//  Ponno
//
//  Created by a k azad on 2/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PreOrderProductCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productAmount: UILabel!
    @IBOutlet weak var productBuyingPrice: UILabel!
    @IBOutlet weak var productSellingPrice: UILabel!
    @IBOutlet weak var productPopUpBtn: UIButton!
    
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: PreOrderProductCell.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCardView(uiview: cardView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
