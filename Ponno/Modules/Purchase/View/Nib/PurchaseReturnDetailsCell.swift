//
//  PurchaseReturnDetailsCell.swift
//  Ponno
//
//  Created by a k azad on 27/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PurchaseReturnDetailsCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!{
        didSet{
            setUpCardView(uiview: cardView)
        }
    }
    
    @IBOutlet weak var invoiceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier : String{
        return String(describing: PurchaseReturnDetailsCell.self)
    }
    
}
