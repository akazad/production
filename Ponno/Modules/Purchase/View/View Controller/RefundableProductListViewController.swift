//
//  RefundableProductListViewController.swift
//  Ponno
//
//  Created by a k azad on 20/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

class RefundableProductListViewController: UIViewController {
    
    
    //@IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    
    private var presenter = RefundableProductListPresenter(service: ProductService())
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var productList : [ProductList] = []
    var filteredList : [ProductList] = []
    
    var isLoading : Bool = false
    var currentPage : Int = 1

    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var selectedProductName : String?
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedProductsRefund : [Double] = []
    var selectedSerials : [String] = []
    
    var selectedProduct : ProductList?
    
    var crossImage = UIImage(named: "error_red.png")
    
    var searchText = ""
    var timer : Timer?
    var lastPage : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpSearchBar()
        self.setUpViews()
        self.configureTableVIew()
        self.attachPresenter()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext(sender:)), for: .touchUpInside)
        self.title = LanguageManager.SelectProduct
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func onNext(sender : UIButton){
        if self.selectedProductId.count <= 0 {
            self.showAlert(title: LanguageManager.ToReturnSelectAProduct, message: "")
        }else{
            self.navigateToRefundInfoVC()
        }
        
    }
    
    func navigateToRefundInfoVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "RefundInfoViewController") as! RefundInfoViewController
        
        viewController.selectedProductId = self.selectedProductId
        viewController.selectedAmounts = selectedAmounts
        viewController.selectedSerials = selectedSerials
        viewController.selectedRefundPrices = selectedProductsRefund
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//MARK: SearchBar Delegate
extension RefundableProductListViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.ProductSearch
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.productList = []
        self.isLoading = true
        self.isSearchActive = true
        self.presenter.getProductSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
    }
}



//MARK: TableView Delegate and Data Source
extension RefundableProductListViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableVIew(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductListCell.nib, forCellReuseIdentifier: ProductListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if isSearchActive{
//            if self.filteredList.count > 0 {
//                return self.filteredList.count
//            }
//        }else{
            if self.productList.count > 0  {
                return self.productList.count
            }
//        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductListCell = tableView.dequeueReusableCell(withIdentifier: ProductListCell.identifier, for: indexPath) as! ProductListCell
        cell.selectionStyle = .none

            if self.productList.count > 0 {
                let product = self.productList[indexPath.row]
                
                cell.product = product
                
                if self.selectedProductId.contains(product.productId){
                   // cell.selectBtn.setImage(crossImage, for: .normal)
                    cell.selectBtn.isHidden = false
                    cell.selectBtn.tag = product.productId
                    cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
                }
                else{
                    cell.selectBtn.isHidden = true
                }
                
//                cell.imageIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
//                cell.imageIcon.sd_setImage(with: URL(string: ImageUrl.sharedImageInstance.productImage + product.image), placeholderImage: UIImage(named: "placeholder"))
//                cell.productNameLabel.text = String(describing: product.name)
//                cell.productVariantLabel.text = "Variant" + ": " + String(describing: product.variant)
//                cell.productQuantityLabel.text =  "Quantity" + ": " + String(describing: product.quantity) + " " + product.unit
//                cell.productCategoryLabel.text = "Category" + ": " + String(describing: product.categoryName)
//                cell.productSellingPriceLabel.text = "Selling Price" + ": " + String(describing: product.sellingPrice) + " " + Constants.currencySymbol
                self.selectedProductName = product.name
                
                if isLoading == false && indexPath.row == self.productList.count - 1 && self.currentPage < self.lastPage{
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getProductSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
                }
            }
        return cell
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.productList.count > 0 {
            let data = self.productList[indexPath.row]
            
            if self.selectedProductId.contains(data.productId){
                self.selectedProductAlert(userMessage: LanguageManager.TheProductIsAlreadySelectedDeselectTheProductForUpdate)
            }else{
                self.selectedProduct = data
                
                if data.serials == ""{
                    self.navigateToRefundAddVC()
                }
                else{
                    self.navigateToRefundProductWithSerialVC(serials: data.serials)
                }
            }
        }
    }
    
    func navigateToRefundAddVC(){
        let viewController = UIStoryboard(name: "Purchase", bundle: nil).instantiateViewController(withIdentifier: "RefundAddViewController") as! RefundAddViewController
        viewController.delegate = self
        viewController.product = self.selectedProduct
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToRefundProductWithSerialVC(serials : String){
        if self.prepareSerialNumbers(serials: serials).count > 0{  
            let serialList = self.prepareSerialNumbers(serials: serials)
            
            let viewController = UIStoryboard(name: "Purchase", bundle: nil).instantiateViewController(withIdentifier: "RefundAddSerialViewController") as! RefundAddSerialViewController
            viewController.serialList = serialList
            viewController.delegate = self
            viewController.product = self.selectedProduct
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    @objc func onRemove(sender : UIButton){
        let productId = sender.tag
        
        if self.selectedProductId.contains(productId){
            removeDataofProduct(productId: productId)
        }
    }
    
    func removeDataofProduct(productId : Int){
        var productAtIndex = -1
        for index in 0..<self.selectedProductId.count{
            if self.selectedProductId[index] == productId{
                productAtIndex = index
            }
        }
        
        //self.selectedSellingPrices.remove(at: productAtIndex)
        self.selectedProductsRefund.remove(at: productAtIndex)
        self.selectedAmounts.remove(at: productAtIndex)
        self.selectedSerials.remove(at: productAtIndex)
        self.selectedProductId.remove(at: productAtIndex)
        self.refreshTableView()
    }
    
//    fileprivate func prepareSerialNumbers(serials : String)->[String]{
//        guard serials != "" else {
//            return []
//        }
//        let serialList = serials.components(separatedBy: ",")
//
//        guard serialList.count > 0 else {
//            return []
//        }
//        return serialList
//    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func selectedProductAlert(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
//
////ProductDelegate
//extension RefundableProductListViewController : RefundableProductAddDelegate{
//
//    func setSerialData(serial: String, buyingPrice: Double, sellingPrice: Double) {
//        guard let selectedProduct = self.selectedProduct else{
//            return
//        }
//        self.selectedProductId.append(selectedProduct.productId)
//        selectedSerials.append(serial)
//        selectedAmounts.append(1.0)
//        print(serial + "\(buyingPrice) " + " : " + "\(sellingPrice)")
//    }
//
//    func setProductData(amount: Double, buyingPrice: Double, sellingPrice: Double) {
//        guard let selectedProduct = self.selectedProduct else{
//            return
//        }
//        self.selectedProductId.append(selectedProduct.productId)
//        selectedAmounts.append(amount)
//        selectedSerials.append("")
//        self.refreshTableView()
//        print("\(amount)" + "\(buyingPrice) " + " : " + "\(sellingPrice)")
//    }
//
//
//}

//Mark: Api Delegate
extension RefundableProductListViewController: RefundableProductListViewDelegate{
    func setRefundableProductData(data: ProductListDataMapper) {
        guard let list = data.productList, list.count > 0 else{
            return
        }
        self.isLoading = false
        self.productList += list
        self.filteredList += list
        
        self.refreshTableView()
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPage = pagination.lastPageNo
        //self.refreshTableView()
    }
    
    func onFailed(message : String) {
        self.showAlert(title: message, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.presenter.getProductSearchDataFromServer(page: self.currentPage, searchString: "")
    }
}


//Product Delegate
extension RefundableProductListViewController : RefundableProductAddDelegate{
    func setSerialData(serial: String, refundPrice: Double) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        self.selectedProductId.append(selectedProduct.productId)
        selectedSerials.append(serial)
        selectedProductsRefund.append(refundPrice)
        selectedAmounts.append(Double(self.countFrequencyOf(serial: serial)))
        self.refreshTableView()
//        print(serial + " : " + "\(refundPrice)")
    
    }
    
    func convertToString(serials : [String])->String{
        var serialText = ""
        for i in 0..<serials.count{
            serialText += serials[i]
//            if i != serials.count - 1{
//                serialText += ","
//            }
        }
        return serialText
    }
    
    func countFrequencyOf(serial : String)->Int{
        let count = serial.filter { $0 == "," }.count
        if count > 0{
            return count + 1
        }
        return 1
    }
    
    func setProductData(amount: Double, refundPrice: Double) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        self.selectedProductId.append(selectedProduct.productId)
        selectedProductsRefund.append(refundPrice)
        selectedAmounts.append(amount)
        selectedSerials.append("")
        self.refreshTableView()
        //print("\(amount)" + " : " + "\(refundPrice)")
    }
}

