//
//  RefundInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 20/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class RefundInfoViewController: UIViewController {

    @IBOutlet weak var returnRelatedInfoLbl: UILabel!{
        didSet{
            returnRelatedInfoLbl.text = LanguageManager.ReturnRelatedInfo
        }
    }
    @IBOutlet weak var totalPriceLbl: UILabel!{
        didSet{
            totalPriceLbl.text = LanguageManager.TotalPrice
        }
    }
    @IBOutlet weak var totalPriceTextField: UITextField!{
        didSet{
            totalPriceTextField.placeholder = LanguageManager.TotalPrice
        }
    }
    @IBOutlet weak var VendorLbl: UILabel!{
        didSet{
            VendorLbl.text = LanguageManager.Vendor
        }
    }
    @IBOutlet weak var vendorsListTextField: UITextField!{
        didSet{
            vendorsListTextField.placeholder = LanguageManager.SelectOne
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = RefundableInfoPresenter(service: PurchaseService())
    
    //
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedRefundPrices : [Double] = []
    var selectedSerials : [String] = []
    
    //
    var isLoading : Bool = false
    var currentPage : Int = 1
    
    //
    var vendorsList : [VendorsList] = []
    var vendorsListPicker = UIPickerView()
    var vendorsId : Int?
    var vendorLastPage : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.initialSetUp()
        self.addFloaty()
        self.setUpPickerView()
        self.attachPresenter()
    }
    
    func addFloaty(){
        let floaty = Floaty()
        floaty.buttonColor = UIColor.lightGreen
        
        let extraItem = FloatyItem()
        extraItem.icon = UIImage(named: "next")
        extraItem.buttonColor = UIColor.lightGreen
        extraItem.title = LanguageManager.NewVendorAdd
        extraItem.handler = { item in
            self.alertWithTextField()
        }
        
        floaty.addItem(item: extraItem)
        self.view.addSubview(floaty)
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.popToSpecifiedController()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func popToSpecifiedController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is PurchaseViewController {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    
    func initialSetUp(){
        self.totalPriceTextField.isEnabled = false
        guard self.selectedAmounts.count > 0 else {return}
        guard self.selectedRefundPrices.count > 0 else { return }
        
        var totalPrice = 0.0
        var temp : Int = 0
        for price in self.selectedRefundPrices {
            let tempTotal = price * self.selectedAmounts[temp]
            totalPrice += tempTotal
            temp += 1
        }
        totalPriceTextField.text = "\(totalPrice)"
    }
    
    //PickerView
    func setUpPickerView(){
        totalPriceTextField.delegate = self
        vendorsListTextField.delegate = self
        
        vendorsListPicker.delegate = self
        
        let vendorsListToolBar = UIToolbar()
        vendorsListToolBar.barStyle = UIBarStyle.default
        vendorsListToolBar.isTranslucent = true
        vendorsListToolBar.tintColor = UIColor.black
        vendorsListToolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        vendorsListToolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        vendorsListToolBar.isUserInteractionEnabled = true
        
        self.vendorsListTextField.inputView = vendorsListPicker
        self.vendorsListTextField.inputAccessoryView = vendorsListToolBar
        
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    @objc func onSubmit(sender : UIButton){
        self.submitData()
    }
    
    func submitData(){
        
        guard self.selectedProductId.count > 0 else {return}
        guard self.selectedAmounts.count > 0 else {return}
        guard self.selectedRefundPrices.count > 0 else {return}
        guard self.selectedSerials.count > 0 else {return}
        
        guard let totalRefundPrice = self.totalPriceTextField.text else {
            return
        }
        
        guard let vendorsId =  self.vendorsId else {
            return
        }
        
        let param : [String : Any] = ["products": self.selectedProductId,  "quantity" : self.selectedAmounts, "return_price" : self.selectedRefundPrices, "serial_no" : self.selectedSerials, "total" : totalRefundPrice, "vendor_id": vendorsId]
        
        self.presenter.postRefundableDataToServer(refundData: param)
        
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.view.endEditing(true)
    }
 
}

//Mark: PickerViewDelegate
extension RefundInfoViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.vendorsListPicker{
            return vendorsList.count
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.vendorsListPicker{
            if self.vendorsList.count > 0 {
                let list = self.vendorsList[row]
                self.vendorsListTextField.text = list.name
                self.vendorsId = list.id
                
                if isLoading == false && row == self.vendorsList.count - 1  && self.currentPage < self.vendorLastPage{
                    self.isLoading = true
                    self.currentPage += 1
                    self.presenter.getVendorsDataFromServer(page: self.currentPage)
                }
                return self.vendorsList[row].name
            }else{
                return ""
            }
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.vendorsListPicker{
            if self.vendorsList.count > 0 {
                let list = self.vendorsList[row]
                self.vendorsListTextField.text = list.name
                self.vendorsId = list.id
            }else{
                return
            }
        }else{
            return
        }
    }
}

//Mark: TextField Delegate
extension RefundInfoViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       if textField == self.vendorsListTextField{
            self.vendorsListTextField.becomeFirstResponder()
        }else{
            self.submitBtn.becomeFirstResponder()
        }
        return true
    }
    
}

//Mark: Api Delegate
extension RefundInfoViewController: refundableInfoViewDelegate{
    func setVendorsData(data: VendorsDataMapper) {
        guard let list = data.vendors, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.vendorsList += list
        
        guard let pagination = data.pagination else{
            return
        }
        self.vendorLastPage = pagination.lastPageNo
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func postNewVendorData(data: VendorAddDataMapper) {
        guard let vendor = data.vendor else {
            return
        }
        self.vendorsId = vendor.id
        self.vendorsListTextField.text = vendor.name
        guard let message = data.message else{
            return
        }
        self.showAlert(title: message, message: "")
    }
    
    func onFailed(data: String) {
        showAlert(title: data, message: "")
    }
    
    func onVendorsDataFailed(data: String) {
        //showAlert(title: data, message: "")
    }
    
    func showLoading() {
        self.showLoader()
        
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.presenter.getVendorsDataFromServer(page: self.currentPage)
    }
    
}

extension RefundInfoViewController{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewVendorAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.VendorName
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.PhoneNumber
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
           // self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showMessage(userMessage: LanguageManager.VendorNameIsRequired)
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                self.showMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
                return
            }
            
//            if(!self.validatePhoneNumber(value: phone)){
//                self.showMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
//            }
            
            let param : [String : Any] = ["name": textField.text!, "phone": textField2.text!, "address": ""]
            
            self.presenter.postNewVendorDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.alertWithTextField()
                
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
        
    }
    
}
