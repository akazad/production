//
//  PurchaseAddSerialViewController.swift
//  Ponno
//
//  Created by a k azad on 18/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol PurchasableProductDelegate : NSObjectProtocol{
    func setSerialData(serial : String, buyingPrice : Double, sellingPrice : Double, warranty : String)
    func setProductData(amount : Double, buyingPrice : Double, sellingPrice : Double, expireDate : String)
}

class PurchaseAddSerialViewController: UIViewController {
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var titleLbl: UILabel!{
        didSet{
            titleLbl.text = LanguageManager.PurchaseRelatedInfo
        }
    }
    @IBOutlet weak var serialLbl: UILabel!{
        didSet{
            serialLbl.text =  LanguageManager.SerialNo
        }
    }
    @IBOutlet weak var buyingPriceLbl: UILabel!{
        didSet{
            buyingPriceLbl.text =  LanguageManager.BuyingPrice
        }
    }
    @IBOutlet weak var sellingPriceLbl: UILabel!{
        didSet{
            sellingPriceLbl.text =  LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var warrentyLbl: UILabel!{
        didSet{
            warrentyLbl.text =  LanguageManager.Warranty
        }
    }
    @IBOutlet weak var serialText: UITextField!
//        {
//        didSet{
//            let button = UIButton(type: .custom)
//            button.setImage(UIImage(named: "scan"), for: .normal)
//            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
//             button.frame = CGRect(x: CGFloat(serialText.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
//            button.addTarget(self, action: #selector(self.productScan), for: .touchUpInside)
//            serialText.rightView = button
//            serialText.rightViewMode = .always
//        }
//    }
    @IBOutlet weak var buyingPrice: UITextField!{
        didSet{
            buyingPrice.placeholder =  LanguageManager.BuyingPrice
        }
    }
    @IBOutlet weak var sellingPrice: UITextField!{
        didSet{
            sellingPrice.placeholder =  LanguageManager.SellingPrice
        }
    }
    @IBOutlet weak var warrenty: UITextField!{
        didSet{
            warrenty.placeholder =  LanguageManager.Warranty
        }
    }
    @IBOutlet weak var monthTextField: UITextField!{
        didSet{
            monthTextField.text = LanguageManager.Month
        }
    }
    @IBOutlet weak var nextBtn: UIButton!

    var serialList : [String]?
    var selectedSerial : [String] = []
    
    var productname : String?
    var warranty : String = ""
    var product : ProductList?
    var delegate : PurchasableProductDelegate?
    
    //CodeScanner
    var isCodeScannerActive : Bool = true
    var searchSerial : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialFormSetUp()
        self.toolBarSetUp()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setUpViews()
        //self.checkIfScannerActive()
        self.setUpInitialLanguage()
    }
    
    func intialFormSetUp(){
        self.title = LanguageManager.NewPurchase
        self.monthTextField.contentVerticalAlignment = .center
        self.monthTextField.isEnabled = false
        self.serialText.underlined()
        self.sellingPrice.underlined()
        self.buyingPrice.underlined()
        self.warrenty.underlined()
        
        guard let product = self.product else{
            return
        }
        self.serialText.text = product.soldSerialNo.joined(separator: ",")
        self.buyingPrice.text = product.buyingPrice
        self.sellingPrice.text = product.sellingPrice
        
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNextBtn(sender:)), for: .touchUpInside)
    }
    
//    //CodeScan
//    func checkIfScannerActive(){
//        if self.isCodeScannerActive == true{
//            if let serial = self.searchSerial?.lowercased(){
//                self.serialText.text = serial
//            }
//        }
//    }
    
    @objc func productScan(sender: Any){
        print("Hola Scan")
    }
    
    func navigateToScannerViewController(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Scanner", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ScannerViewController") as! ScannerViewController
        viewController.navigateTo = Navigate.purchase
        viewController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func onNextBtn(sender : UIButton){
        if isValidated(){
            guard let sellingPrice = sellingPrice.text,let priceDouble = Double(sellingPrice), let buyingPrice = buyingPrice.text, let buyingPriceDouble = Double(buyingPrice), let serial = serialText.text else{
                return
            }
            
            //Serials into array
            let serials = serial.components(separatedBy: ",")
            
            //Unique array elements
            let uniqueElement = uniqueElementsFrom(array:serials)
            
            //Unique array into string
            let uniqueSerials = (uniqueElement.map{String($0)}).joined(separator: ",")
            
            self.delegate?.setSerialData(serial: uniqueSerials, buyingPrice:  buyingPriceDouble, sellingPrice: priceDouble, warranty: "")
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func isValidated()->Bool{
        if self.serialText.text == ""{
            self.showAlert(title: LanguageManager.SerialNumberIsRequired, message: "")
            return false
        }
        else if self.buyingPrice.text == "" {
            self.showAlert(title: LanguageManager.BuyingPriceIsRequired, message: "")
            return false
        }
        else if self.sellingPrice.text == ""{
            self.showAlert(title: LanguageManager.SellingPriceIsRequired, message: "")
            return false
        }
        return true
    }
    
    
    
    func toolBarSetUp(){
        //QuantityToolBar
        let serailToolBar = UIToolbar()
        serailToolBar.barStyle = UIBarStyle.default
        serailToolBar.isTranslucent = true
        serailToolBar.tintColor = UIColor.black
        serailToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let serailDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnSerial(sender:)))
        
        serailToolBar.setItems([cancelButton, spaceButton, serailDoneButton], animated: false)
        serailToolBar.isUserInteractionEnabled = true
        
        self.serialText.inputAccessoryView = serailToolBar
        
        //BuyingPriceToolBar
        let buyingPriceToolBar = UIToolbar()
        buyingPriceToolBar.barStyle = UIBarStyle.default
        buyingPriceToolBar.isTranslucent = true
        buyingPriceToolBar.tintColor = UIColor.black
        buyingPriceToolBar.sizeToFit()
        
        let buyingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnBuyingPrice(sender:)))
        
        buyingPriceToolBar.setItems([cancelButton, spaceButton, buyingPriceDoneButton], animated: false)
        buyingPriceToolBar.isUserInteractionEnabled = true
        
        self.buyingPrice.inputAccessoryView = buyingPriceToolBar
        
        //SellingPriceToolBar
        let sellingPriceToolBar = UIToolbar()
        sellingPriceToolBar.barStyle = UIBarStyle.default
        sellingPriceToolBar.isTranslucent = true
        sellingPriceToolBar.tintColor = UIColor.black
        sellingPriceToolBar.sizeToFit()
        
        let sellingPriceDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnSellingPrice(sender:)))
        
        sellingPriceToolBar.setItems([cancelButton, spaceButton, sellingPriceDoneButton], animated: false)
        sellingPriceToolBar.isUserInteractionEnabled = true
        
        self.sellingPrice.inputAccessoryView = sellingPriceToolBar
        
        //WarrentyToolBar
        let warrentyToolBar = UIToolbar()
        warrentyToolBar.barStyle = UIBarStyle.default
        warrentyToolBar.isTranslucent = true
        warrentyToolBar.tintColor = UIColor.black
        warrentyToolBar.sizeToFit()
        
        let warrentyDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnWarrenty(sender:)))
        
        warrentyToolBar.setItems([cancelButton, spaceButton, warrentyDoneButton], animated: false)
        warrentyToolBar.isUserInteractionEnabled = true
        
        self.warrenty.inputAccessoryView = warrentyToolBar
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingDoneOnSerial(sender: UIBarButtonItem){
        self.serialText.resignFirstResponder()
        self.buyingPrice.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnBuyingPrice(sender: UIBarButtonItem){
        self.buyingPrice.resignFirstResponder()
        self.sellingPrice.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnSellingPrice(sender: UIBarButtonItem){
        self.sellingPrice.resignFirstResponder()
        self.warrenty.becomeFirstResponder()
    }
    
    @objc func onPressingDoneOnWarrenty(sender: UIBarButtonItem){
        let warrantyTime = self.warrenty.text ?? ""
        self.warranty = warrantyTime
        self.warrenty.resignFirstResponder()
        self.nextBtn.becomeFirstResponder()
    }
    
}


// TextField Delegate
extension PurchaseAddSerialViewController : UITextFieldDelegate{
    
    fileprivate func configureTextFields(){
        self.serialText.delegate = self
        
    }
    
    //    func textFieldDidBeginEditing(_ textField: UITextField) {
    //
    //        if textField == self.categoryTextField {
    //            textField.layer.borderColor = UIColor.lightGreen.cgColor
    //            textField.borderWidth = 1
    //
    //            if self.categoryTextField.text != "Salary" {
    //                self.expenseAmountTextField.becomeFirstResponder()
    //                self.employeeTextField.isHidden = true
    //            }else{
    //                self.employeeTextField.isHidden = false
    //                self.employeeTextField.becomeFirstResponder()
    //            }
    //        }
    //        if textField == self.employeeTextField {
    //            textField.becomeFirstResponder()
    //            textField.layer.borderColor = UIColor.lightGreen.cgColor
    //            textField.borderWidth = 1
    //        }
    //        else if textField == self.expenseAmountTextField{
    //            textField.becomeFirstResponder()
    //            textField.layer.borderColor = UIColor.lightGreen.cgColor
    //            textField.borderWidth = 1
    //        }
    //        else if textField == self.detailsTextField{
    //            textField.becomeFirstResponder()
    //            textField.layer.borderColor = UIColor.lightGreen.cgColor
    //            textField.borderWidth = 1
    //        }
    //    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.serialText{
            self.buyingPrice.becomeFirstResponder()
            self.view.endEditing(true)
            return true
        }
        return false
    }
}

extension PurchaseAddSerialViewController{
    func uniqueElementsFrom(array: [String]) -> [String] {
        var set = Set<String>()
        let result = array.filter {
            guard !set.contains($0) else {
                return false
            }
            set.insert($0)
            return true
        }
        return result
    }
}

//extension PurchaseAddSerialViewController{
//    func uniqueElementFrom(uniqueSerial : String) -> String{
//
//        string[].words = uniqueSerial.Split(",")
//        List<string> list = new List<string>()
//        foreach (string word in words)
//        {
//            if ( ! list.Contains(word)){
//                list.Add(word)
//            }
//
//        }
//    }
//}
