//
//  PurchaseInfoViewController.swift
//  Ponno
//
//  Created by a k azad on 19/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PurchaseInfoViewController: UIViewController {
    
    
    @IBOutlet weak var totalPriceLbl: UILabel!{
        didSet{
            totalPriceLbl.text = LanguageManager.TotalPrice
        }
    }
    @IBOutlet weak var totalPriceTxtField: UITextField!{
        didSet{
            totalPriceTxtField.placeholder = LanguageManager.TotalPrice
        }
    }
    @IBOutlet weak var discountLbl: UILabel!{
        didSet{
            discountLbl.text = LanguageManager.Discount
        }
    }
    @IBOutlet weak var discountTextField: UITextField!
    @IBOutlet weak var discountSegmentedControl: UISegmentedControl!
    @IBOutlet weak var payableAmountLbl: UILabel!{
        didSet{
            payableAmountLbl.text = LanguageManager.PayableAmount
        }
    }
    @IBOutlet weak var payableAmountTextField: UITextField!{
        didSet{
            payableAmountTextField.placeholder = LanguageManager.PayableAmount
        }
    }
    @IBOutlet weak var paidAmountLbl: UILabel!{
        didSet{
            paidAmountLbl.text = LanguageManager.PaidAmount
        }
    }
    @IBOutlet weak var paidTxtField: UITextField!{
        didSet{
            paidTxtField.placeholder = LanguageManager.PaidAmount
        }
    }
    @IBOutlet weak var payableLbl: UILabel!{
        didSet{
            payableLbl.text = LanguageManager.Payable
        }
    }
    @IBOutlet weak var payableTxtField: UITextField!{
        didSet{
            payableTxtField.placeholder = LanguageManager.Payable
        }
    }
    @IBOutlet weak var returnLbl: UILabel!{
        didSet{
            returnLbl.text = LanguageManager.Return
        }
    }
    @IBOutlet weak var returnTxtField: UITextField!{
        didSet{
            returnTxtField.placeholder = LanguageManager.ReturnPrice
        }
    }
    @IBOutlet weak var vendorLbl: UILabel!{
        didSet{
            vendorLbl.text = LanguageManager.Vendor
        }
    }
    @IBOutlet weak var vendorTxtField: UITextField!{
        didSet{
            vendorTxtField.placeholder = LanguageManager.SelectOne
        }
    }
    @IBOutlet weak var vendorAddBtn: UIButton!{
        didSet{
            vendorAddBtn.setTitle(LanguageManager.NewVendorAdd, for: .normal)
        }
    }
    @IBOutlet weak var walletAmountTitleLbl: UILabel!
    @IBOutlet weak var walletAmountTitleLblHeight: NSLayoutConstraint!
    @IBOutlet weak var walletAmountTextField: UITextField!
    @IBOutlet weak var walletAmountTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var walletPayLbl: UILabel!
    @IBOutlet weak var walletPayLblHeight: NSLayoutConstraint!
    @IBOutlet weak var walletPayTextField: UITextField!
    @IBOutlet weak var walletPayTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var purchaseDateLbl: UILabel!{
        didSet{
            purchaseDateLbl.text = LanguageManager.PurchaseDate
        }
    }
    @IBOutlet weak var purchaseDateTextField: UITextField!{
        didSet{
            purchaseDateTextField.placeholder = "dd/mm/yyyy"
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    
    private var presenter = AddNewPurchasePresenter(service: PurchaseService())
    
    //
    var selectedProductId : [Int] = []
    var selectedAmounts : [Double] = []
    var selectedBuyingPrices : [Double] = []
    var selectedSellingPrices : [Double] = []
    var selectedSerials : [String] = []
    var selectedExpireDate : [String] = []
    var selectedProductWarranty : [String] = []
    var multipleSelections : [Int] = []
    
    //
    var isLoading : Bool = false
    var currentPage : Int = 1
    
    //
    var vendorsList : [VendorsList] = []{
        didSet{
            self.vendorsListPicker.reloadAllComponents()
        }
    }
    var vendorsListPicker = UIPickerView()
    var vendorsId : Int?

    var totalPrice : Double?
    var paidAmount : Double?
    var payableAmount : Double?
    
    var walletPay : Double = 0.0
    var walletAmount: Double?
    var hasWallet : Bool = false
    
    var vendorLastPageNo: Int = 0
    
    var discountUnits : [String] = ["%","৳"]
    var discountUnitPicker = UIPickerView()
    //var discountUnitId : Int?
    var discount : Double?
    var discountAmount : Double?
    var discountUnit : String?
    
    let dateFormatter = DateFormatter()
    var datePicker = UIDatePicker()
    var purchaseDate : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpPickerView()
        self.toolBarSetUp()
        self.showPurchaseDatePicker()
        self.configureTextFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.initialSetUp()
        self.setUpInitialLanguage()
        self.attachPresenter()
    }
    
    @objc func backBtnPressed(sender: UIBarButtonItem){
        self.totalPriceTxtField = nil
        self.payableAmountTextField.text = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigatePurchaseViewController()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func navigatePurchaseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func initialSetUp(){
        
        self.returnTxtField.isEnabled = false
        self.payableTxtField.isEnabled = false
        self.totalPriceTxtField.isEnabled = false
        self.walletAmountTextField.isEnabled = false
        self.hideVendorWallet(bool: true)
        self.title = LanguageManager.NewPurchase
        self.discountUnit = "৳"
        //self.discountUnitTextField.text = "%"
        self.discountSegmentedControl.selectedSegmentIndex = 0
        //self.discountUnitTextField.contentVerticalAlignment = .center
        //self.discountUnitTextField.textAlignment = .center
        self.discountTextField.text = "\(0.0)"
        
        self.returnTxtField.text = "\(0.0)"
        
        guard self.selectedAmounts.count > 0 else {return}
        guard self.selectedBuyingPrices.count > 0 else { return }
        guard self.selectedSerials.count > 0 else { return }
        self.discount = 0.0
        var totalPrice = 0.0
        self.paidAmount = 0.0
        self.discountAmount = 0.0
        self.payableAmount = 0.0

        for index in 0..<selectedAmounts.count {
            totalPrice += self.selectedAmounts[index] * self.selectedBuyingPrices[index]
        }
        //paidTxtField.text = "\(0.0)"
        totalPriceTxtField.text = "\(totalPrice)"
        payableAmountTextField.text = "\(totalPrice)"
        payableTxtField.text = "\(totalPrice)"
        
        self.totalPrice = totalPrice
        
        self.purchaseDateTextField.text = currentDateFormatter(format: "dd MMM, yyy", date: Date())
        self.purchaseDate = currentDateFormatter(format: "dd MMM, yyy", date: Date())
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.discountTextField.addTarget(self, action: #selector(onDiscountTextEditChange), for: .editingChanged)
        self.discountSegmentedControl.addTarget(self, action: #selector(onDiscountSegmentChange), for: .valueChanged)
        
        self.paidTxtField.addTarget(self, action: #selector(onPaidTextFieldChange), for: .editingChanged)
        
        self.walletPayTextField.addTarget(self, action: #selector(onWalletPay), for: .editingChanged)
        
        self.vendorAddBtn.addTarget(self, action: #selector(onVendorAddBtnPressed), for: .touchUpInside) 
    }
    
    @objc func onVendorAddBtnPressed(sender: UIButton){
        self.alertWithTextField()
    }
    
    @objc func onWalletPay(sender: UITextField){
        guard let amountText = self.walletPayTextField.text, let amount = amountText.toDouble() else{
            self.walletPay = 0.0
            self.refreshScreen()
            return
        }
        
        if let walletAmount = self.walletAmount{
            if amount > walletAmount{
                self.showAlert(title: Constants.currencySymbol + " \(walletAmount)", message: LanguageManager.YouCanNotPayMoreThan)
                self.walletPayTextField.text = walletAmount.toString()
                self.walletPay = walletAmount
                self.refreshScreen()
                return
            }else{
                self.walletPay = amount
            }
        }
        
        let temp1 = self.payableAmountTextField.text?.toDouble() ?? 0
        let temp2 = self.paidTxtField.text?.toDouble() ?? 0
        
        let tempPayable = temp1 - temp2
        
        if amount > tempPayable{
            self.walletPayTextField.text = tempPayable.toString()
            self.walletPay = tempPayable
            self.showAlert(title: LanguageManager.WalletPayExceedsPayableAmount, message: "")
            self.refreshScreen()
            return
        }
        
        self.refreshScreen()
    }
    
    @objc func onDiscountTextEditChange(_textField: UITextField){
        self.setMaxDiscount()
    }
    
    func setMaxDiscount(){
        if let discount = self.discountTextField.text?.toDouble(), let totalAmount = self.totalPrice{
            if discountSegmentedControl.selectedSegmentIndex == 1{
                if discount >= 100{
                    self.discountTextField.text = "\(100)"
                }
            }else{
                if discount > totalAmount{
                    self.discountTextField.text = "\(totalAmount)"
                }
            }
            self.discount = self.discountTextField.text?.toDouble()
            self.refreshScreen()
        }else{
            self.discount = 0
            self.refreshScreen()
        }
    }
    
    @objc func onDiscountSegmentChange(sender: UISegmentedControl){
        if self.discountSegmentedControl.selectedSegmentIndex == 0 {
            self.discountUnit = "৳"
            setMaxDiscount()
        }else{
            self.discountUnit = "%"
            setMaxDiscount()
        }
        self.refreshScreen()
    }
    
    @objc func onPaidTextFieldChange(textField: UITextField){
        guard let paidText = textField.text, let paid = Double(paidText) else{
            self.paidAmount = 0.0
            self.refreshScreen()
            return

        }
        self.paidAmount = paid
        self.refreshScreen()
    }
    
    //paidToolBarSetUp
    func toolBarSetUp(){
        let paidToolBar = UIToolbar()
        paidToolBar.barStyle = UIBarStyle.default
        paidToolBar.isTranslucent = true
        paidToolBar.tintColor = UIColor.black
        paidToolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let paidDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingPaidDone(sender:)))
        
        paidToolBar.setItems([cancelButton, spaceButton, paidDoneButton], animated: false)
        paidToolBar.isUserInteractionEnabled = true
        
        self.paidTxtField.inputAccessoryView = paidToolBar
        
        let walletPayToolBar = UIToolbar()
        walletPayToolBar.barStyle = UIBarStyle.default
        walletPayToolBar.isTranslucent = true
        walletPayToolBar.tintColor = UIColor.black
        walletPayToolBar.sizeToFit()
        
        let walletPayDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingWalletPayDone(sender:)))
        
        walletPayToolBar.setItems([cancelButton, spaceButton, walletPayDoneButton], animated: false)
        walletPayToolBar.isUserInteractionEnabled = true
        
        self.walletPayTextField.inputAccessoryView = walletPayToolBar
        
        
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onPressingPaidDone(sender: UIBarButtonItem){
        guard let paid = self.paidTxtField.text, paid != "", let paidAmount = Double(paid) else{ return }
        self.paidAmount = paidAmount
        self.paidTxtField.resignFirstResponder()
        self.vendorTxtField.becomeFirstResponder()
        self.refreshScreen()
    }
    
    @objc func onPressingWalletPayDone(sender: UIButton){
        self.walletPayTextField.resignFirstResponder()
        guard let walletPayText = self.walletPayTextField.text, walletPayText != "", let walletPayAmount = walletPayText.toDouble() else{ return }
        if let walletAmount = self.walletAmount{
            if walletPayAmount > walletAmount{
                self.showAlert(title: Constants.currencySymbol + " \(walletAmount)", message: LanguageManager.YouCanNotPayMoreThan)
                self.walletPayTextField.text = walletAmount.toString()
                return
            }
        }
        self.walletPay = walletPayAmount
        self.refreshScreen()
    }
    
    @objc func onPressingDoneOnDiscount(sender: UIBarButtonItem){
        //self.discountUnitTextField.becomeFirstResponder()
        self.refreshScreen()
    }
    
//    @objc func onPressingDoneOnDiscountUnit(sender: UIBarButtonItem){
//        self.paidTxtField.becomeFirstResponder()
//        self.refreshScreen()
//    }
    
    //PickerView
    func setUpPickerView(){
        
        vendorsListPicker.delegate = self
        discountUnitPicker.delegate = self
        
        let vendorsListToolBar = UIToolbar()
        vendorsListToolBar.barStyle = UIBarStyle.default
        vendorsListToolBar.isTranslucent = true
        vendorsListToolBar.tintColor = UIColor.black
        vendorsListToolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        vendorsListToolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        vendorsListToolBar.isUserInteractionEnabled = true
        
        self.vendorTxtField.inputView = vendorsListPicker
        self.vendorTxtField.inputAccessoryView = vendorsListToolBar
        
        self.submitBtn.addTarget(self, action: #selector(onSubmit), for: .touchUpInside)
    }
    
    @objc func onSubmit(sender : UIButton){
        self.submitData()
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.submitBtn.isEnabled = isEnabled
    }
    
    func submitData(){
        
        if isValidated(){
            guard self.selectedProductId.count > 0 else {return}
            guard self.selectedAmounts.count > 0 else {return}
            guard self.selectedBuyingPrices.count > 0 else {return}
            guard self.selectedSellingPrices.count > 0 else {return}
            guard self.selectedSerials.count > 0 else {return}
            
            guard let total = self.totalPriceTxtField.text, total != "", let totalAmount = Double(total), let payableAmountText = self.payableAmountTextField.text, let payableAmount = Double(payableAmountText) else {
                return
            }
            
            if let paidAmount = self.paidTxtField.text?.toDouble(), let discount = self.discount, let discountAmount = self.discountAmount, let discountUnit = self.discountUnit, let date = self.purchaseDate, let payable = self.payableTxtField.text?.toDouble(), let cashBack = self.returnTxtField.text?.toDouble() {
                
                if let vendorsId =  self.vendorsId {
                    let param : [String : Any] = ["products": self.selectedProductId,  "quantity" : self.selectedAmounts, "buying_price" : self.selectedBuyingPrices, "selling_price" : self.selectedSellingPrices,  "serial_no" : self.selectedSerials, "total" : totalAmount, "discount": discount, "discount_amount": discountAmount, "discount_unit": discountUnit, "to_be_paid": payableAmount, "paid" : paidAmount, "payable" : payable, "cash_back": cashBack, "warranty": self.selectedProductWarranty, "expire_date": self.selectedExpireDate, "vendor_id": vendorsId, "date": self.dateFormatWith(date: date), "wallet": self.walletPay]
                    
                    self.presenter.postPurchasableDataToServer(purchasesData: param)
                }
            }
        }
    }
    
    func isValidated() -> Bool{
        if (self.vendorTxtField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.VendorNameIsRequired, message: "")
            return false
        }
        return true
    }
    
    func dateFormatWith(date: String) -> String{
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd MMM, yyyy"
        
        guard let originalDate = dateFormatter.date(from: date) else {return ""}
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.string(from: originalDate)
        return dateString
    }
    
    @objc func onPressingDone(sender : UIBarButtonItem){
        self.view.endEditing(true)
    }
   
}

//Mark: PickerViewDelegate
extension PurchaseInfoViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == self.vendorsListPicker{
            return vendorsList.count
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.vendorsListPicker{
            if self.vendorsList.count > 0 {
                let list = self.vendorsList[row]
                self.vendorTxtField.text = list.name
                self.vendorsId = list.id
                self.hasWallet = list.hasWallet
                if hasWallet == true{
                    self.hideVendorWallet(bool: false)
                    self.walletAmountTextField.text = list.amount
                    self.walletAmount = list.amount.toDouble()
                }else{
                    self.hideVendorWallet(bool: true)
                }
                return self.vendorsList[row].name
            }else{
                return ""
            }
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.vendorsListPicker{
            if self.vendorsList.count > 0 {
                let list = self.vendorsList[row]
                self.vendorTxtField.text = list.name
                self.vendorsId = list.id
                self.hasWallet = list.hasWallet
                if hasWallet == true{
                    self.hideVendorWallet(bool: false)
                    self.walletAmountTextField.text = list.amount
                    self.walletAmount = list.amount.toDouble()
                    self.walletPayTextField.text = "0.00"
                    self.walletPay = 0.0
                }else{
                    self.hideVendorWallet(bool: true)
                }
            }
        }
    }
}

//Mark: TextField Delegate
extension PurchaseInfoViewController : UITextFieldDelegate{
    func configureTextFields(){
        totalPriceTxtField.delegate = self
        discountTextField.delegate = self
        //discountUnitTextField.delegate = self
        payableAmountTextField.delegate = self
        paidTxtField.delegate = self
        payableTxtField.delegate = self
        returnTxtField.delegate = self
        vendorTxtField.delegate = self
        totalPriceTxtField.underlined()
        discountTextField.underlined()
        //discountUnitTextField.underlined()
        payableAmountTextField.underlined()
        paidTxtField.underlined()
        payableTxtField.underlined()
        returnTxtField.underlined()
        vendorTxtField.underlined()
        purchaseDateTextField.underlined()
        walletPayTextField.underlined()
        walletAmountTextField.underlined()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        if textField == self.discountTextField{
//            self.discountUnitTextField.becomeFirstResponder()
//        }
        if textField == self.paidTxtField{
            //self.vendorTxtField.becomeFirstResponder()
            self.refreshScreen()
        }else if textField == self.discountTextField{
            self.paidTxtField.becomeFirstResponder()
            self.refreshScreen()
        }
        return true
    }
    
}


//Mark: Refresh TextField
extension PurchaseInfoViewController{
    
    fileprivate func refreshScreen(){
        
        guard self.selectedAmounts.count > 0 else {return}
        guard self.selectedBuyingPrices.count > 0 else { return }
        guard self.selectedSerials.count > 0 else { return }
        var totalPrice = 0.0
        
        for index in 0..<selectedAmounts.count {
            totalPrice += self.selectedAmounts[index] * self.selectedBuyingPrices[index]
        }
        
        totalPriceTxtField.text = "\(totalPrice)"
        
        guard let paid = self.paidAmount else {
            return
        }
        //self.paidTxtField.text = "\(paid)"
        
        guard let discount = self.discount else{
            return
        }
        
        let discountUnit = discountSegmentedControl.titleForSegment(at: discountSegmentedControl.selectedSegmentIndex)
        
        var payableAmount = totalPrice
        
        if discountUnit == "%"{
            self.discountUnit = "%"
            self.discountAmount = (discount * totalPrice) / 100
            payableAmount = totalPrice - ((discount * totalPrice) / 100)
        }else{
            self.discountUnit = "৳"
            
            payableAmount = totalPrice - discount
        }
        
        self.discountAmount = discount
        
        self.payableAmountTextField.text = "\(payableAmount)"
        self.payableAmount = payableAmount
        
        var wPay : Double = 0.0
        if wPay < 0 {
            wPay = 0
        }
        
        //need to change
        if self.hasWallet == true{
            wPay = self.walletPay
            self.hideVendorWallet(bool: false)
        }else{
            self.hideVendorWallet(bool: true)
        }
        
        //self.walletPayTextField.text = "\(self.walletPay)"
        
        var payable = 0.0
        var cashBack = 0.0
        
        let totalPayable = payableAmount - (paid + wPay)
        if totalPayable < 0{
            payable = 0.0
        }else{
            payable = totalPayable
        }
        
        if paid + wPay > payableAmount{
            cashBack = (paid + wPay) - payableAmount
        }else{
            cashBack = 0.0
        }
        
        self.payableTxtField.text = "\(payable)"
        self.returnTxtField.text = "\(cashBack)"
        
        
    }
}


//Mark: Api Delegate
extension PurchaseInfoViewController: addNewPurchaseViewDelegate{
    func postNewVendorData(data: VendorAddDataMapper) {
        guard let vendor = data.vendor else {
            return
        }
        self.vendorsId = vendor.id  
        self.vendorTxtField.text = vendor.name
        
        let newVendor = VendorsList(id: vendor.id, name: vendor.name, phone: vendor.phone)
        self.vendorsList.append(newVendor)
        
        self.vendorTxtField.resignFirstResponder()
        guard let message = data.message else{
            return
        }
        self.showAlert(title: message, message: "")
    }
    
    func setVendorsData(data: VendorsDataMapper) {
        guard let list = data.vendors, list.count > 0 else {
            return
        }
        self.isLoading = false
        self.vendorsList += list
        
        guard let pagination = data.pagination else{
            return
        }
        self.vendorLastPageNo = pagination.lastPageNo
    }
    
    func onSuccess(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func onFailed(data: String) {
        self.showAlert(title: data, message: "")
    }
    
    func showLoading() {
        submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        //self.presenter.getVendorsDataFromServer(page: self.currentPage)
        self.presenter.getAllVendorsDataFromServer(getAll: true)

    }
  
}

extension PurchaseInfoViewController{
    func alertWithTextField(){
        let alertController = UIAlertController(title: LanguageManager.NewVendorAdd, message: "", preferredStyle: .alert)
        
        alertController.addTextField { textField in
            textField.placeholder = LanguageManager.VendorName
            textField.textAlignment = .center
        }
        
        alertController.addTextField{ textField in
            textField.placeholder = LanguageManager.PhoneNumber
            textField.textAlignment = .center
            textField.keyboardType = .phonePad
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
           // self.dismiss(animated: true, completion: nil)
        }
        
        let confirmAction = UIAlertAction(title: "OK", style: .default) { _ in
            
            let textField = alertController.textFields![0]
            
            guard let text = textField.text , text.count > 0 else{
                self.showMessage(userMessage: LanguageManager.VendorNameIsRequired)
                return
            }
            
            let textField2 = alertController.textFields![1]
            
            guard let phone = textField2.text, phone.count > 0 else{
                self.showMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
                return
            }
            
//            if(!self.validatePhoneNumber(value: phone)){
//                self.showMessage(userMessage: LanguageManager.PhoneNumberIsIncorrect)
//            }
            
            let param : [String : Any] = ["name": textField.text!, "phone": textField2.text!, "address": ""]
            
            self.presenter.postNewVendorDataToServer(param: param)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.alertWithTextField()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: PurchaseDate
extension PurchaseInfoViewController{
    func showPurchaseDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let startDate = UIBarButtonItem(title: LanguageManager.SelectStartDate, style: .plain, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,startDate,spaceButton,cancelButton], animated: false)
        
        purchaseDateTextField.inputAccessoryView = toolbar
        purchaseDateTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        purchaseDateTextField.text = currentDateFormatter(format: "dd MMM, yyyy", date: datePicker.date)
        self.purchaseDate = currentDateFormatter(format: "yyyy-MM-dd", date: datePicker.date)
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func currentDateFormatter(format: String, date: Date) -> String{
        dateFormatter.dateFormat = format
        let currentDate = dateFormatter.string(from: date)
        return currentDate
    }
    
}

extension PurchaseInfoViewController{
    func hideVendorWallet(bool: Bool){
        self.walletPayLbl.isHidden = bool
        self.walletAmountTitleLbl.isHidden = bool
        self.walletAmountTextField.isHidden = bool
        self.walletPayTextField.isHidden = bool
    }
}

