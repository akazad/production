//
//  PerDayPurchaseViewController.swift
//  Ponno
//
//  Created by a k azad on 30/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class PerDayPurchaseViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var presenter = PerDayPurchasePresenter(service: PurchaseService())
    
    var selectedDate : String?
    var perdayPurchaseId: Int?
    
    var perDayPurchaselist : [PurchaseList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var perDayPurchaseSummery : [PerDayPurchaseSummery]?{
        didSet{
            self.collectionView.reloadData()
        }
    }

    var isLoading : Bool = false
    var currentPage: Int = 1
    var lastPageNo: Int = 0
    
    let manager = CollectionViewScrollManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.attachPresenter()
        
        self.configureTableView()
        self.configureCollectionView()
        self.addFloaty()
        self.setBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpInitialLanguage()
        self.setNavigationBarTitle()
        self.attachPresenter()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.invalidateTimer()
    }
    
    func setNavigationBarTitle(){
        guard let date = self.selectedDate else {
            return
        }
        self.title = date.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func navigateToAddNewExpenseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Expense", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as! AddNewExpenseViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewSaleViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToAddNewPurchaseViewController(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchasableProductListViewController") as! PurchasableProductListViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addFloaty(){
        let floatyManager = FloatingActionButton()
        floatyManager.floatyDelegate = self
        let floaty = floatyManager.addFloaty(buttons: [])
        self.view.addSubview(floaty)
    }
    
    func setBarButton(){
        
        let button: UIButton = UIButton (type: UIButton.ButtonType.custom)
        button.setImage(UIImage(named: "search-2"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onPressed(sender:)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func onPressed(sender: UIBarButtonItem){
        self.navigateToSalesPerDaySearchVC()
    }
    
    func navigateToSalesPerDaySearchVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PerDayPurchaseSearchViewController") as! PerDayPurchaseSearchViewController
        viewController.selectedDate = self.selectedDate
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                    self.perDayPurchaselist = []
                    self.refreshTableView()
                    guard let date = self.selectedDate else{
                        return
                    }
                    
                    self.presenter.getPerDayPurchaseDataFromServer(page: self.currentPage, date: date)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    //Pull To Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(PerDayPurchaseViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.green
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.perDayPurchaselist = []
        currentPage = 1
        guard let date = self.selectedDate else{
            return
        }
        self.presenter.getPerDayPurchaseDataFromServer(page: self.currentPage, date: date)
        
        refreshControl.endRefreshing()
    }
    
    
}

//Mark: Floaty Delegate
extension PerDayPurchaseViewController: FloatyDelegate{
    func navigateToAddSaleVC() {
        self.navigateToAddNewSaleViewController()
    }
    
    func navigateToAddPurchaseVC() {
        self.navigateToAddNewPurchaseViewController()
    }
    
    func navigateToAddExpenseVC() {
        self.navigateToAddNewExpenseViewController()
    }
}

//MARK: CollectionView Delegate and DataSource
extension PerDayPurchaseViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func configureCollectionView(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(PerDayPurchaseSummeryCell.nib, forCellWithReuseIdentifier: PerDayPurchaseSummeryCell.identifier)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let item = self.perDayPurchaseSummery, item.count > 0 else {
            return 0
        }
        return item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PerDayPurchaseSummeryCell = collectionView.dequeueReusableCell(withReuseIdentifier: PerDayPurchaseSummeryCell.identifier, for: indexPath) as! PerDayPurchaseSummeryCell
        guard let item = self.perDayPurchaseSummery, item.count > 0 else {
            return cell
        }
        let summeryData = item[indexPath.row]
        
//        for (key, _) in item.enumerated() {
//            if key == 0 {
//                item[0].title = "Total Purchase"
//            }else if key == 1 {
//                item[1].title = "Total Purchase Amount"
//            }else if key == 2 {
//                item[2].title = "Current Payable"
//            }
//        }
        
        cell.perDaySummery.text = summeryData.title
        cell.perDaySummeryNumber.text = summeryData.value
       return cell
    }
    func refreshCollectionView(){
        self.collectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 70.0)
    }
    //MARK: Set Up Auto Scroll
    func setUpAutoScroll(){
        guard let list = self.perDayPurchaseSummery, list.count > 0 else{
            return
        }
        let listCount = list.count
        
        self.manager.setUpManager(listCount: listCount, collectionView: self.collectionView)
    }
    
    func invalidateTimer(){
        self.manager.invalidateTimer()
    }
    
}

//MARK: TableView Delegate And DataSource
extension PerDayPurchaseViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PerDayPurchaseListCell.nib, forCellReuseIdentifier: PerDayPurchaseListCell.identifier)
        self.tableView.tableFooterView = UIView()
//        self.tableView.estimatedRowHeight = 120
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.separatorColor = UIColor.clear
        self.tableView.addSubview(refreshControl)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.perDayPurchaselist.count > 0 {
            return self.perDayPurchaselist.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PerDayPurchaseListCell = tableView.dequeueReusableCell(withIdentifier: PerDayPurchaseListCell.identifier, for: indexPath) as! PerDayPurchaseListCell
        cell.selectionStyle = .none
        if self.perDayPurchaselist.count > 0 {
            let perDayPurchaseItem = self.perDayPurchaselist[indexPath.row]
            
            cell.perDayPurchaselist = perDayPurchaseItem
            
            cell.invoiceLabel.text = perDayPurchaseItem.invoice
            cell.totalPurchaseLabel.text = LanguageManager.TotalPurchase + " : " + "\(perDayPurchaseItem.total)" + " " + Constants.currencySymbol
            cell.paidLabel.text = LanguageManager.PaidAmount + " : " + "\(perDayPurchaseItem.paid)" + " " + Constants.currencySymbol
            cell.totalPayableLabel.text = LanguageManager.TotalPayable + " : " + "\(perDayPurchaseItem.payable)" + " " + Constants.currencySymbol
            cell.vendorNameLabel.text = LanguageManager.Vendor + " : " + perDayPurchaseItem.vendorName
            
            
            
            cell.popUpBtn.tag = perDayPurchaseItem.id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.perDayPurchaselist.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                guard let date = self.selectedDate else{
                    return cell
                }
                self.presenter.getPerDayPurchaseDataFromServer(page: self.currentPage, date: date)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.perDayPurchaselist.count > 0 {
            let purchaseItem = self.perDayPurchaselist[indexPath.row]
            self.navigateToPurchaseDetailsVC(selectedId: purchaseItem.id)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title: LanguageManager.Delete) { action, index in
            
            if self.perDayPurchaselist.count > 0{
                let purchasePerDay = self.perDayPurchaselist[indexPath.row]
                self.perdayPurchaseId = purchasePerDay.id
                
                guard let id = self.perdayPurchaseId else{
                    return
                }
                self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
            }
            
        }
        delete.backgroundColor = UIColor.red
        
        return [delete]
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.perDayPurchaselist.count > 0{
            self.perdayPurchaseId = sender.tag
            for purchaseItem in self.perDayPurchaselist{
                if self.perdayPurchaseId == purchaseItem.id{
                    
                    let payable : Double = purchaseItem.payable.toDouble() ?? 0.00
                    let invoicePayable = Double (purchaseItem.invoicePayable) ?? 0.00
                    
                    if payable != invoicePayable && invoicePayable != 0{
                        let paid = String(describing: payable - invoicePayable)
                        let paidAction = UIAlertAction(title: paid + " " + Constants.currencySymbol + " " + LanguageManager.Paid, style: UIAlertAction.Style.default) { (action) in
                            
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        paidAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(paidAction)
                        myActionSheet.addAction(cancelAction)
                    }else{
                        guard let id = self.perdayPurchaseId else{
                            return
                        }
                        let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                            self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(deleteAction)
                        myActionSheet.addAction(cancelAction)
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToPurchaseDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseDetailsViewController") as! PurchaseDetailsViewController
        viewController.selectedId = selectedId 
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.postPerDayPurchaseCancelDataToServer(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

// MARK: Api Delegate
extension PerDayPurchaseViewController : PerDayPurchaseViewDelegate {
    func setPerDayPurchaseCancelData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func setPerDayPurchaseList(data: PerDayPurchaseDataMapper) {
        guard let summeryItem = data.summary else {
            return
        }
        self.perDayPurchaseSummery = summeryItem
        self.refreshCollectionView()
        self.setUpAutoScroll()
        
        guard let lists = data.purchaseList else {
            return
        }
        self.isLoading = false
        self.perDayPurchaselist += lists
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()

    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.isLoading = true
        self.perDayPurchaselist = []
        guard let date = self.selectedDate else{
            return
        }
        
        self.presenter.getPerDayPurchaseDataFromServer(page: self.currentPage, date: date.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd.rawValue, outputDateFormat: DateFormats.yyyy_MM_dd.rawValue))
    }
    
}
