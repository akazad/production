//
//  RefundAddSerialViewController.swift
//  Ponno
//
//  Created by a k azad on 20/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol RefundableProductAddDelegate : NSObjectProtocol{
    func setSerialData(serial : String, refundPrice : Double)
    func setProductData(amount : Double, refundPrice : Double)
}

class RefundAddSerialViewController: UIViewController {
    
    @IBOutlet weak var returnRelatedInfoLbl: UILabel!{
        didSet{
            returnRelatedInfoLbl.text = LanguageManager.ReturnRelatedInfo
        }
    }
    @IBOutlet weak var selectSerialLbl: UILabel!{
        didSet{
            selectSerialLbl.text = LanguageManager.SelectSerialNumber
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var returnPriceLbl: UILabel!{
        didSet{
            returnPriceLbl.text = LanguageManager.ReturnPrice
        }
    }
    @IBOutlet weak var refundPriceTextField: UITextField!{
        didSet{
            refundPriceTextField.placeholder = LanguageManager.ReturnPrice
        }
    }
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var tableViewHeightConstrant: NSLayoutConstraint!
    
    var serialList : [String]?
    var selectedSerial : [String] = []
    
    var product : ProductList?
    var delegate : RefundableProductAddDelegate?
    var buyingPrice : Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setUpViews()
        self.configureTableView()
        self.initialSetup()
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNextBtn(sender:)), for: .touchUpInside)
        
    }
    
    func initialSetup(){
        self.title = LanguageManager.ProductReturn
        guard let item = self.product else{
            return
        }
        self.refundPriceTextField.text = item.buyingPrice
        self.buyingPrice = Double(item.buyingPrice)
    }
    
    
    @objc func onNextBtn(sender : UIButton){
        if isValidated(){
            guard let price = refundPriceTextField.text,let priceDouble = Double(price) else{
                return
            }
            let serials = self.selectedSerial.joined(separator: ",")
            self.delegate?.setSerialData(serial: serials, refundPrice: priceDouble)
            self.navigationController?.popViewController(animated: true)
        }
    }
    func isValidated()->Bool{
        if self.selectedSerial == [""] {
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.SerialNumberIsRequired)
            return false
        }
        else if (self.refundPriceTextField.text?.isEmpty)!{
            self.showAlert(title: LanguageManager.Warning, message: LanguageManager.ReturnPriceIsRequired)
            return false
        }
        guard let buyingPriceText = self.refundPriceTextField.text, let buyingPrice = Double(buyingPriceText), let buyingPriceCheck = self.buyingPrice else {
            return false
        }
        if buyingPrice > buyingPriceCheck{
            self.showAlert(title: LanguageManager.BuyingpriceCantBeGreaterThan + "  \(buyingPriceCheck)", message: "")
            return false
        }
        return true
    }
}

//Mark: TableView Delegate DataSource
extension RefundAddSerialViewController : UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductSerialCell.nib, forCellReuseIdentifier: ProductSerialCell.identifier)
        self.tableView.estimatedRowHeight = 30
        self.refreshTableView()
        self.tableView.allowsMultipleSelection = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let list = self.serialList, list.count > 0 else{
            return 0
        }
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductSerialCell = tableView.dequeueReusableCell(withIdentifier: ProductSerialCell.identifier, for: indexPath) as! ProductSerialCell
        guard let list = self.serialList, list.count > 0 else{
            return cell
        }
        let serial = list[indexPath.row]
        cell.serialNumber.text = serial
        
        if self.selectedSerial.contains(serial){
            cell.checkIcon.image = UIImage(named : "check_icon")
        }
        else{
            cell.checkIcon.image = UIImage(named: "uncheck_icon")
        }
        
        return cell
        
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let list = self.serialList, list.count > 0 else{
            return
        }
        let serial = list[indexPath.row]
        if !self.selectedSerial.contains(serial){
            self.selectedSerial.append(serial)
        }
        else {
            self.selectedSerial = self.selectedSerial.filter({ $0 != serial})
        }
        self.refreshTableView()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let list = self.serialList, list.count > 0 else{
            return
        }
        let serial = list[indexPath.row]
        if self.selectedSerial.contains(serial){
            self.selectedSerial.append(serial)
        }
        self.refreshTableView()
    }
    
    func refreshTableView(){
        self.tableView.reloadData {
            if self.tableView.contentSize.height > 200{
                self.tableViewHeightConstrant.constant = 200
            }
            else {
                self.tableViewHeightConstrant.constant = self.tableView.contentSize.height
            }
        }
    }
    
}
