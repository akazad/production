//
//  PurchaseReturnDetailsViewController.swift
//  Ponno
//
//  Created by a k azad on 27/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class PurchaseReturnDetailsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = PurchaseReturnDetailsPresenter(service: PurchaseService())
    
    var selectedId : Int?
    enum Sections: Int {
        case ReturnDetails
        case RetrunProducts
    }
    var returnProductDetails: PurchaseReturnDetails?{
        didSet{
            self.refreshTableView()
        }
    }
    var returnProduct: [PurchaseReturnProducts]?{
        didSet{
            self.refreshTableView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        self.setNavigationBarTitle()
        self.configureTableView()
        self.attachPresenter()
    }
    
    func setNavigationBarTitle(){
        self.title = LanguageManager.ReturnInvoice
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor( red: CGFloat(122/255.0), green: CGFloat(190/255.0), blue: CGFloat(57/255.0), alpha: CGFloat(1.0) )]
        self.navigationController?.navigationBar.barTintColor =  UIColor.white
    }
    
}

//MARK: TableView Delegate and DataSource
extension PurchaseReturnDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PurchaseReturnDetailsCell.nib, forCellReuseIdentifier: PurchaseReturnDetailsCell.identifier)
        self.tableView.register(PurchaseReturnProductCell.nib, forCellReuseIdentifier: PurchaseReturnProductCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.clear
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Sections.ReturnDetails.rawValue:
            return 1
        case Sections.RetrunProducts.rawValue:
            guard let returnProducts = self.returnProduct, returnProducts.count > 0 else{
                return 0
            }
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case Sections.ReturnDetails.rawValue:
            let cell : PurchaseReturnDetailsCell = tableView.dequeueReusableCell(withIdentifier: PurchaseReturnDetailsCell.identifier, for: indexPath) as! PurchaseReturnDetailsCell
            cell.selectionStyle = .none
            guard let returnDetails = self.returnProductDetails else{
                return cell
            }
            
            cell.invoiceLabel.text = LanguageManager.Invoice + "#" + " " +  returnDetails.invoice
            cell.dateLabel.text = LanguageManager.Date + " : " +  returnDetails.date.convertDateFormater(inputDateFormat: DateFormats.yyyy_MM_dd.rawValue, outputDateFormat: DateFormats.dd_MMM_yy.rawValue)
            cell.totalLabel.text = LanguageManager.ReturnPrice + " : " + "\(returnDetails.total)" + " " + Constants.currencySymbol
            cell.nameLabel.text = LanguageManager.Vendor + " : " + returnDetails.vendorName
            cell.phoneLabel.text = LanguageManager.Phone + " : " + returnDetails.vendorPhone
            return cell
            
        case Sections.RetrunProducts.rawValue:
            let cell : PurchaseReturnProductCell = tableView.dequeueReusableCell(withIdentifier: PurchaseReturnProductCell.identifier, for: indexPath) as! PurchaseReturnProductCell
            cell.selectionStyle = .none
            guard let productReturn = self.returnProduct, productReturn.count > 0 else{
                return cell
            }
            let productItem = productReturn[indexPath.row]
            cell.productNamelabel.text = productItem.name
            cell.quantityLabel.text = LanguageManager.Quantity + " : " + "\(productItem.quantity)" + productItem.unit
            cell.perUnitPriceLabel.text = LanguageManager.PricePerUnit + " : " +  "\(productItem.returnPrice)" + productItem.unit
            
            let total = productItem.total.toDouble()?.rounded(toPlaces: 2)
            if let totalPrice = total{
                print(totalPrice)
                cell.totalLabel.text = Constants.currencySymbol + " " +  "\(totalPrice)"
            }
            
            //cell.totalLabel.text = "\(productItem.total)" + " " + Constants.currencySymbol
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Sections.ReturnDetails.rawValue:
            return UITableView.automaticDimension
        case Sections.RetrunProducts.rawValue:
            return UITableView.automaticDimension
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == Sections.RetrunProducts.rawValue{
            let footer = UIView(frame: CGRect(x: 20, y: 0, width: self.tableView.frame.width, height: 40))
            let label = UILabel(frame: CGRect(x: 100, y: 0, width: footer.frame.width, height: 20))
            guard let data = self.returnProductDetails else{
                return UIView()
            }
            
            label.text = LanguageManager.ReturnPrice + " : " + "\(data.total)" + " " + Constants.currencySymbol
            
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
            footer.addSubview(label)
            return footer
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == Sections.RetrunProducts.rawValue{
            return 40.0
        }
        return 3.0
    }
}

//MARK: Api Delegate
extension PurchaseReturnDetailsViewController : PurchaseReturnDetailsViewDelegate{
    func setPurchaseReturnDetailsData(data: PurchaseReturnDetailsDataMapper) {
        guard let productDetails = data.purchaseReturnDetails else{
            return
        }
        self.returnProductDetails = productDetails
        self.refreshTableView()
        
        guard let productItem = data.purchaseReturnProducts, productItem.count > 0 else {
            return
        }
        self.returnProduct = productItem
        self.refreshTableView()
    }
    
    func onFailed() {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.tableView.isHidden = true
        self.showLoader()
    }
    
    func hideLoading() {
        self.tableView.isHidden = false
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let id = self.selectedId else{
            return
        }
        self.presenter.getPurchaseReturnDetailsDataFromServer(id: id)
    }
    
}
