//
//  PerDayPurchaseSearchViewController.swift
//  Ponno
//
//  Created by a k azad on 23/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class PerDayPurchaseSearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var presenter = PerDayPurchaseSearchPresenter(service: PurchaseService())
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var list : [PurchaseList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    
    var searchText = ""
    var timer : Timer?
    
    var selectedDate : String?
    var perdayPurchaseId: Int?
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.attachPresenter()
        self.setUpSearchBar()
        self.configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
        self.setUpInitialLanguage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    //Search Alert
    func searchAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                guard let date = self.selectedDate else{
                    return
                }
                
                self.presenter.getPerDayPurchaseSearchDataFromServer(page: self.currentPage, date: date, searchText: "")
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.list = []
                self.refreshTableView()
                guard let date = self.selectedDate else{
                    return
                }
                
                self.presenter.getPerDayPurchaseSearchDataFromServer(page: self.currentPage, date: date, searchText: self.searchText)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Search Delegate
extension PerDayPurchaseSearchViewController: UISearchBarDelegate{
    
    func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.SearchSerial
        self.navigationItem.titleView = searchBar
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let textToSearch = searchBar.text else {
            return
        }
        self.searchText = textToSearch
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.onSearch), userInfo: nil, repeats: false);
    }
    
    @objc func onSearch(){
        self.currentPage = 1
        self.list = []
        self.isLoading = true
        self.isSearchActive = true
        guard let date = self.selectedDate else {
            return
        }
        
        self.presenter.getPerDayPurchaseSearchDataFromServer(page: self.currentPage, date: date, searchText: self.searchText)
    }
}


//MARK: TableView Delegate And DataSource
extension PerDayPurchaseSearchViewController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(PerDayPurchaseListCell.nib, forCellReuseIdentifier: PerDayPurchaseListCell.identifier)
        self.tableView.tableFooterView = UIView()
        //        self.tableView.estimatedRowHeight = 120
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.separatorColor = UIColor.clear
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.list.count > 0 {
            return self.list.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PerDayPurchaseListCell = tableView.dequeueReusableCell(withIdentifier: PerDayPurchaseListCell.identifier, for: indexPath) as! PerDayPurchaseListCell
        cell.selectionStyle = .none
        if self.list.count > 0 {
            let perDayPurchaseItem = self.list[indexPath.row]
            cell.invoiceLabel.text = perDayPurchaseItem.invoice
            cell.totalPurchaseLabel.text = LanguageManager.TotalPurchase + " : " + "\(perDayPurchaseItem.total)" + " " + Constants.currencySymbol
            cell.paidLabel.text = LanguageManager.PaidAmount + " : " + "\(perDayPurchaseItem.paid)" + " " + Constants.currencySymbol
            cell.totalPayableLabel.text = LanguageManager.TotalPayable + " : " + "\(perDayPurchaseItem.payable)" + " " + Constants.currencySymbol
            cell.vendorNameLabel.text = LanguageManager.Vendor + " : " + perDayPurchaseItem.vendorName
            
            cell.popUpBtn.tag = perDayPurchaseItem.id
            cell.popUpBtn.addTarget(self, action: #selector(onPopUpBtnTapped(sender:)), for: .touchUpInside)
            
            if isLoading == false && indexPath.row == self.list.count - 1 && self.currentPage < self.lastPageNo{
                self.isLoading = true
                self.currentPage += 1
                guard let date = self.selectedDate else{
                    return cell
                }
                self.presenter.getPerDayPurchaseSearchDataFromServer(page: self.currentPage, date: date, searchText: self.searchText)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.list.count > 0 {
            let purchaseItem = self.list[indexPath.row]
            self.navigateToPurchaseDetailsVC(selectedId: purchaseItem.id)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            
            if self.list.count > 0{
                let purchasePerDay = self.list[indexPath.row]
                self.perdayPurchaseId = purchasePerDay.id
                
                guard let id = self.perdayPurchaseId else{
                    return
                }
                self.confirmationMessage(userMessage: "Are you sure?", deleteId: id)
            }
            
        }
        delete.backgroundColor = UIColor.red
        
        return [delete]
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    @objc func onPopUpBtnTapped(sender: UIButton){
        let myActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        myActionSheet.view.tintColor = UIColor.black
        
        if self.list.count > 0{
            self.perdayPurchaseId = sender.tag
            for purchaseItem in self.list{
                if self.perdayPurchaseId == purchaseItem.id{
                    
                    let payable : Double = purchaseItem.payable.toDouble() ?? 0.00
                    let invoicePayable = Double (purchaseItem.invoicePayable) ?? 0.00
                    
                    if (payable - invoicePayable == 0.00){
                        guard let id = self.perdayPurchaseId else{
                            return
                        }
                        let deleteAction = UIAlertAction(title: LanguageManager.Delete, style: UIAlertAction.Style.default) { (action) in
                            self.confirmationMessage(userMessage: LanguageManager.AreYouSure, deleteId: id)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(deleteAction)
                        myActionSheet.addAction(cancelAction)
                    }else{
                        let paid = String(describing: payable - invoicePayable)
                        let deleteAction = UIAlertAction(title: paid + " " + Constants.currencySymbol + " " + LanguageManager.Paid, style: UIAlertAction.Style.default) { (action) in
                            
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                            
                            self.view.endEditing(true)
                        }
                        
                        deleteAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        cancelAction.setValue(UIColor.lightRed, forKey: "titleTextColor")
                        
                        myActionSheet.addAction(deleteAction)
                        myActionSheet.addAction(cancelAction)
                    }
                }
            }
        }
        
        popOverForIpad(action: myActionSheet)
        
        self.present(myActionSheet, animated: true, completion: nil)
    }
    
    func navigateToPurchaseDetailsVC(selectedId : Int){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseDetailsViewController") as! PurchaseDetailsViewController
        viewController.selectedId = selectedId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func confirmationMessage(userMessage:String, deleteId  : Int) -> Void {
            let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.presenter.postPerDayPurchaseCancelDataToServer(id: deleteId)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
}

//Mark: Api Delegate
extension PerDayPurchaseSearchViewController: PerDayPurchaseSearchViewDelegate{
    func setPerDayPurchaseCancelData(data: AddDataMapper) {
        guard let message = data.message else {
            return
        }
        self.displayMessage(userMessage: message)
    }
    
    func setPerDayPurchaseSearchList(data: PerDayPurchaseDataMapper) {
        guard let lists = data.purchaseList else {
            return
        }
        self.isLoading = false
        self.list += lists
        
        guard let pagination = data.pagination else {
            return
        }
        self.lastPageNo = pagination.lastPageNo
    }
    
    func onFailed(data: String) {
        self.searchAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        guard let date = self.selectedDate else{
            return
        }
        self.presenter.getPerDayPurchaseSearchDataFromServer(page: self.currentPage, date: date, searchText: self.searchText)
    }
}
