//
//  PurchasableProductListViewController.swift
//  Ponno
//
//  Created by a k azad on 18/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty
import SDWebImage

class PurchasableProductListViewController: UIViewController {
    
    
    //@IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    private var presenter = PurchasableProductListPresenter(service: ProductService())
    
    
    var productList : [ProductList] = []
    
    var selectedProductList : [ProductList] = []{
        didSet{
            if self.selectedProductList.count > 0{
                self.selectedProductList = self.selectedProductList.sorted(by: { $0.soldQuantity > $1.soldQuantity})
            }
        }
    }
    
    var selectableProductList : [ProductList] = []{
        didSet{
            refreshTableView()
        }
    }
    
    var filteredList : [ProductList] = []{
        didSet{
            self.refreshTableView()
        }
    }
    
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 50, height: 44))
    
    var isLoading : Bool = false
    var currentPage : Int = 1
    var lastPageNo: Int = 0
    var getAll: Bool = true
    
    var searchText = ""
    var timer : Timer?
    
    var crossImage = UIImage(named: "error_red.png")
    
    var isSearchActive = false{
        didSet{
            self.refreshTableView()
        }
    }
    var selectedProductName : String?
    var selectedProductId : [Int] = [] 
    var selectedAmounts : [Double] = []
    var selectedBuyingPrices : [Double] = []
    var selectedSellingPrices : [Double] = []
    var selectedSerials : [String] = []
    var selectedProductWarranty : [String] = []
    var selectedExpireDate : [String] = []
    var hasSerial : Int?
    
    var selectedProduct : ProductList?
    
    //CodeScan
//    var codeScannerActive : Bool = false
//    var codeSearchProduct : ProductList?{
//        didSet{
//            if let data = codeSearchProduct, isObjectNotNil(object: data){
//                self.selectedProduct = data
//                //self.sellingPrice = Double(data.sellingPrice)
//
//                if self.selectedProductId.contains(data.productId){
//                    self.selectedProductAlert(userMessage: LanguageManager.TheProductIsAlreadySelectedDeselectTheProductForUpdate)
//                }else{
//                    self.selectedProduct = data
//
//                    if let product = self.selectedProduct{
//                        let quantity = data.quantity
//                        if quantity.isEmpty == false && quantity > "0.00"{
//                            if data.hasSerial == 0 {
//                                self.navigateToPurchaseAddVC(product: product)
//                            }
//                            else{
//                                self.navigateToPurchasableProductWithSerialVC(product: product, serials: data.serials)
//                            }
//                        }else{
//                            self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
//                        }
//                    }
//                }
//            }
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.addFloaty()
        
        self.configureTableVIew()
        self.setUpViews()
        self.setUpSearchBar()
        self.attachPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBar()
        self.setUpInitialLanguage()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //self.searchBar.becomeFirstResponder()
    }
    
    func setNavigationBar(){
        self.navigationController?.navigationBar.topItem?.title = ""
    }
    
    func setUpViews(){
        self.nextBtn.addTarget(self, action: #selector(onNext(sender:)), for: .touchUpInside)
        self.title = LanguageManager.SelectProduct
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func onNext(sender : UIButton){
        if self.selectedProductId.count <= 0 {
            self.showAlert(title: LanguageManager.SelectAProductToPurchase, message: "")
        }else{
            self.navigateToPurchaseInfoVC()
        }
    }
    
    func navigateToPurchaseInfoVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Purchase", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PurchaseInfoViewController") as! PurchaseInfoViewController
        
        viewController.selectedProductId = self.selectedProductId
        viewController.selectedAmounts = selectedAmounts
        viewController.selectedBuyingPrices = selectedBuyingPrices
        viewController.selectedSellingPrices = selectedSellingPrices
        viewController.selectedSerials = selectedSerials
        viewController.selectedExpireDate = selectedExpireDate
        viewController.selectedProductWarranty = selectedProductWarranty
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

//MARK: SearchBar Delegate
extension PurchasableProductListViewController: UISearchBarDelegate{
    fileprivate func setUpSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.placeholder = LanguageManager.ProductSearch
        self.navigationItem.titleView = searchBar
    }

//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        isSearchActive = true
//    }

//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        //self.isSearchActive = false
//
//        self.view.endEditing(true)
//    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        self.view.endEditing(true)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //isSearchActive = true
        self.view.endEditing(true)
    }


    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {


        if self.productList.count > 0 {
            if searchText == "" {
                isSearchActive = false
                return
            }else{
                isSearchActive = true
            }
        }
        guard let textToSearch = searchBar.text else {
            return
        }

        filteredList = self.productList.filter { (product : ProductList) -> Bool in
            return product.name.lowercased().contains(textToSearch.lowercased())  || product.categoryName.lowercased().contains(textToSearch.lowercased()) || product.variant.lowercased().contains(textToSearch.lowercased())

        }

    }
}

//MARK: TableView Delegate and Data Source
extension PurchasableProductListViewController : UITableViewDelegate, UITableViewDataSource {
    private func configureTableVIew(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(ProductListCell.nib, forCellReuseIdentifier: ProductListCell.identifier)
        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 145
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if isSearchActive == true{
                return 0
            }else{
                if self.selectedProductList.count > 0 {
                    return self.selectedProductList.count
                }
                return 0
            }
            
        case 1:
            if isSearchActive{
                if self.filteredList.count > 0 {
                    return self.filteredList.count
                }
                return 0
            }else{
                if self.selectableProductList.count > 0 {
                    return self.selectableProductList.count
                }
                return 0
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductListCell = tableView.dequeueReusableCell(withIdentifier: ProductListCell.identifier, for: indexPath) as! ProductListCell
        cell.selectionStyle = .none
        
        switch indexPath.section {
            
        case 0:
            if self.selectedProductList.count > 0 {
                let product = self.selectedProductList[indexPath.row]
                
                cell.selectedProduct = product
                cell.productQuantityLabel.textColor = .black
                
                cell.selectBtn.tag = product.productId
                cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
            }
            
        case 1:
            if isSearchActive{
                if self.filteredList.count > 0{
                    let product = self.filteredList[indexPath.row]
                    
                    cell.selectableProduct = product
                }
            }else{
                if self.selectableProductList.count > 0 {
                    let product = self.selectableProductList[indexPath.row]
                    
                    cell.selectableProduct = product
                }
            }
        default:
            return cell
        }
        
        return cell
        
    }
    
    
    //    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //        let cell : ProductListCell = tableView.dequeueReusableCell(withIdentifier: ProductListCell.identifier, for: indexPath) as! ProductListCell
    //        cell.selectionStyle = .none
    //
    //        if isSearchActive{
    //            if self.filteredList.count > 0 {
    //                let product = self.filteredList[indexPath.row]
    //
    //                cell.product = product
    //
    //                if self.selectedProductId.contains(product.productId){
    //                    cell.selectBtn.isHidden = false
    //                }
    //                else{
    //                    cell.selectBtn.isHidden = true
    //                }
    //                self.selectedProductName = product.name
    //                cell.selectBtn.tag = product.productId
    //                cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
    //
    //                if isLoading == false && indexPath.row == self.filteredList.count - 1 && self.currentPage < self.lastPageNo{
    //                    self.isLoading = true
    //                    self.currentPage += 1
    //                    self.presenter.getProductSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
    //                }
    //            }
    //        }
    //        else{
    //            if self.productList.count > 0 {
    //                let product = self.productList[indexPath.row]
    //
    //                cell.product = product
    //
    //                if self.selectedProductId.contains(product.productId){
    //                    cell.selectBtn.isHidden = false
    //                }
    //                else{
    //                    cell.selectBtn.isHidden = true
    //                }
    //                self.selectedProductName = product.name
    //                cell.selectBtn.tag = product.productId
    //                cell.selectBtn.addTarget(self, action: #selector(onRemove(sender:)), for: .touchUpInside)
    //
    //                if isLoading == false && indexPath.row == self.productList.count - 1 && self.currentPage < self.lastPageNo{
    //                    self.isLoading = true
    //                    self.currentPage += 1
    //                    self.presenter.getProductSearchDataFromServer(page: self.currentPage, searchString: self.searchText)
    //                }
    //            }
    //        }
    //        return cell
    //    }
    
    //
    //    @objc func onRemove(sender : UIButton){
    //        let productId = sender.tag
    //
    //        if self.selectedProductId.contains(productId){
    //            removeDataofProduct(productId: productId)
    //        }
    //    }
    
    //
    @objc func onRemove(sender : UIButton){
        let productId = sender.tag
        
        if self.selectedProductId.contains(productId){
            for item in self.selectedProductList{
                if item.productId == productId{
                    let soldQuantity = item.soldQuantity
                    removeDataFromSelectedProducts(productId: productId, quantity: "\(soldQuantity)")
                    removeDataofProduct(productId : productId)
                }
            }
        }
    }
    
    func removeDataFromSelectedProducts(productId : Int, quantity: String){
        self.selectedProductList = self.selectedProductList.filter{ $0.productId != productId}
        self.addDataToSelectableProductList(productId: productId, quantity: quantity)
        self.refreshTableView()
    }
    
    func removeDataofProduct(productId : Int){
        var productAtIndex = -1
        for index in 0..<self.selectedProductId.count{
            if self.selectedProductId[index] == productId{
                productAtIndex = index
            }
        }
        
        self.selectedSellingPrices.remove(at: productAtIndex)
        self.selectedAmounts.remove(at: productAtIndex)
        self.selectedSerials.remove(at: productAtIndex)
        self.selectedProductId.remove(at: productAtIndex)
        self.selectedBuyingPrices.remove(at: productAtIndex)
        
        if selectedExpireDate.indices.contains(productAtIndex){
            self.selectedExpireDate.remove(at: productAtIndex)
        }
        if selectedProductWarranty.indices.contains(productAtIndex){
            self.selectedProductWarranty.remove(at: productAtIndex)
        }
        
        self.refreshTableView()
    }
    
    func refreshTableView(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        //        if isSearchActive{
//        //            if self.filteredList.count > 0 {
//        //
//        //                let data = self.filteredList[indexPath.row]
//        //
//        //                if self.selectedProductId.contains(data.productId){
//        //                    self.selectedProductAlert(userMessage: "The Product is already selected! Deselect the product for update")
//        //                }else{
//        //                    self.selectedProduct = data
//        //
//        //                    guard let product = self.selectedProduct else{return}
//        //
//        //                    if data.hasSerial == 0 {
//        //                        self.navigateToPurchaseAddVC(product: product)
//        //                    }
//        //                    else{
//        //                        self.navigateToPurchasableProductWithSerialVC(product: product, serials: data.serials)
//        //                    }
//        //                }
//        //            }
//        //        }
//        //        else{
//        if self.productList.count > 0 {
//            let data = self.productList[indexPath.row]
//
//            if self.selectedProductId.contains(data.productId){
//                self.selectedProductAlert(userMessage: LanguageManager.TheProductIsAlreadySelectedDeselectTheProductForUpdate)
//            }else{
//                self.selectedProduct = data
//
//                if let product = self.selectedProduct{
//                    if data.hasSerial == 0 {
//                        self.navigateToPurchaseAddVC(product: product)
//                    }
//                    else{
//                        self.navigateToPurchasableProductWithSerialVC(product: product, serials: data.serials)
//                    }
//                    //                        let quantity = data.quantity
//                    //                        if quantity.isEmpty == false && quantity != "0.00"{
//                    //
//                    //                        }else{
//                    //                            self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
//                    //                        }
//                }
//            }
//        }
//        //        }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            if self.selectedProductList.count > 0 {
                let data = self.selectedProductList[indexPath.row]
                self.didSelectOnSelectedProductList(data: data)
            }
        case 1:
            if isSearchActive{
                if self.filteredList.count > 0{
                    let data = self.filteredList[indexPath.row]
                    self.didSelectOnSelectedProductList(data: data)
                }
            }else{
                if self.selectableProductList.count > 0 {
                    let data = self.selectableProductList[indexPath.row]
                    self.didSelectOnSelectedProductList(data: data)
                }
            }
        default:
            return
        }
        
    }
    
    func didSelectOnSelectedProductList(data: ProductList){
//        for item in self.selectedProductList{
//            if item.productId == data.productId{
//                self.selectedProduct = item
//            }
//        }
        self.selectedProduct = data
        if data.hasSerial == 0{
            self.navigateToPurchaseAddVC(product: data)
        }else{
            //let serials = data.soldSerialNo.joined(separator: ",")
            self.navigateToPurchasableProductWithSerialVC(product: data, serials: data.soldSerialNo)
        }
        
        
    }
    
//    func didSelectOnSelectableProductList(data: ProductList){
//        self.selectedProductName = data.name
//        self.selectedProduct = data
//
//        if self.isPreOrderEnable == true{
//            self.navigateToSaleAddVC()
//        }else{
//            let quantity = data.quantity
//            if quantity.isEmpty == false && quantity != "0.00"{
//                if data.hasSerial == 0{
//                    self.navigateToSaleAddVC()
//                }
//                else{
//                    self.navigateToSaleAddWithSerialVC(data: data)
//                }
//            }else{
//                self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
//            }
//        }
//    }
    
    func navigateToPurchaseAddVC(product : ProductList){
        let viewController = UIStoryboard(name: "Purchase", bundle: nil).instantiateViewController(withIdentifier: "PurchaseAddViewController") as! PurchaseAddViewController
        
        viewController.delegate = self
        viewController.product = product
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToPurchasableProductWithSerialVC(product: ProductList, serials : [String]){
        let viewController = UIStoryboard(name: "Purchase", bundle: nil).instantiateViewController(withIdentifier: "PurchaseAddSerialViewController") as! PurchaseAddSerialViewController
        
        viewController.delegate = self
        viewController.product = product
        
        //        if serials == "" {
        //            self.navigationController?.pushViewController(viewController, animated: true)
        //        }else if self.prepareSerialNumbers(serials: serials).count > 0{
        //            let serialList = self.prepareSerialNumbers(serials: serials)
        //            viewController.serialList = serialList
        //
        ////            if codeScannerActive == true{
        ////                viewController.isCodeScannerActive = true
        ////                viewController.searchSerial = self.searchText
        ////            }else{
        ////                viewController.isCodeScannerActive = false
        ////            }
        //
        //            self.navigationController?.pushViewController(viewController, animated: true)
        //        }
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
        
    func displayMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Successful, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func selectedProductAlert(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Warning, message: userMessage, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default){
            (action:UIAlertAction!) in
            
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//ProductDelegate
extension PurchasableProductListViewController : PurchasableProductDelegate{
    
    func setSerialData(serial: String, buyingPrice: Double, sellingPrice: Double, warranty : String) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        if self.selectedProductId.contains(selectedProduct.productId){
            self.removeDataofProductObject(productId: selectedProduct.productId)
        }
        self.selectedProductId.append(selectedProduct.productId)
        self.selectedSerials.append(serial)
        self.selectedBuyingPrices.append(buyingPrice)
        self.selectedSellingPrices.append(sellingPrice)
        self.selectedAmounts.append(Double(self.countFrequencyOf(serial: serial)))
        self.selectedExpireDate.append("")
        if warranty == "" {
            selectedProductWarranty.append("")
        }else{
            selectedProductWarranty.append(warranty)
        }
        
        let serialArray = prepareSerialNumbers(serials: serial)
        self.addSerialDataToSelectedProducts(productId: selectedProduct.productId, soldQuantity: "\(serialArray.count)", soldSerials: serialArray, buyingPrice: (buyingPrice.toString()), soldSellingPrice: (sellingPrice.toString()))
        
        self.removeDataFromSelectableProductList(productId: selectedProduct.productId)
        self.resetSearchBar()
        print(serial.description + " : " + "\(buyingPrice) " + " : " + "\(sellingPrice)")
    }
    
    func setProductData(amount: Double, buyingPrice: Double, sellingPrice: Double, expireDate : String) {
        guard let selectedProduct = self.selectedProduct else{
            return
        }
        if self.selectedProductId.contains(selectedProduct.productId){
            self.removeDataofProductObject(productId: selectedProduct.productId)
        }
        self.selectedProductId.append(selectedProduct.productId)
        self.selectedBuyingPrices.append(buyingPrice)
        self.selectedSellingPrices.append(sellingPrice)
        self.selectedAmounts.append(amount)
        self.selectedSerials.append("")
        self.selectedProductWarranty.append("")
        if expireDate == "" {
            self.selectedExpireDate.append("")
        }else{
            self.selectedExpireDate.append(expireDate)
        }
        
        self.addDataToSelectedProducts(productId: selectedProduct.productId, soldQuantity: amount.toString(), buyingPrice: "\(buyingPrice)", soldSellingPrice: sellingPrice.toString())
        self.removeDataFromSelectableProductList(productId: selectedProduct.productId)
        self.resetSearchBar()
        print("\(amount)" + "\(buyingPrice) " + " : " + "\(sellingPrice)")
    }
    
    func countFrequencyOf(serial : String)->Int{
        let count = serial.filter { $0 == "," }.count
        if count > 0{
            return count + 1
        }
        return 1
    }
    
    func resetSearchBar(){
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        self.isSearchActive = false
        self.view.endEditing(true)
    }
    
}

//Mark: Api Delegate
extension PurchasableProductListViewController: PurchasableProductListViewDelegate{
    func setPurchasableProductData(data: ProductListDataMapper) {
        guard let list = data.productList, list.count > 0 else{
            return
        }
        
        self.productList = list
        
        self.selectedProductList = self.productList.filter{selectedProductId.contains($0.productId)}
        self.selectableProductList = self.productList.filter{ !selectedProductId.contains($0.productId)}
        
    }
    
    func onFailed(data: String) {
        self.showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        self.productList = []
        self.presenter.getAllProductFromServer(getAll: true)
    }
}

extension PurchasableProductListViewController{
    func addDataToSelectableProductList(productId: Int, quantity: String){
        if self.productList.count > 0{
            for item in self.productList{
                if item.productId == productId{
                    let inventoryId = item.inventoryId
                    let productId = productId
                    let quantity = item.quantity
                    let sellingPrice = item.sellingPrice
                    let hasSerial = item.hasSerial
                    let serials = item.serials
                    let name = item.name
                    let image = item.image
                    let categoryName = item.categoryName
                    let variant = item.variant
                    let unit = item.unit
                    let categoryId = item.categoryId
                    
                    let selectableProduct = ProductList(inventoryId: inventoryId, productId: productId, quantity: quantity, sellingPrice: sellingPrice, hasSerial: hasSerial, serials: serials, name: name, image: image, categoryName: categoryName, categoryId: categoryId, variant: variant, unitName: unit)
                    
                    self.selectableProductList.append(selectableProduct)
                    self.selectableProductList = self.selectableProductList.sorted(by: { $0.name < $1.name })
                }
            }
        }
    }
    
    func removeDataofProductObject(productId : Int){
        var productAtIndex = -1
        if let index = self.selectedProductId.firstIndex(of: productId){
            productAtIndex = index
        }
        self.selectedProductId.remove(at: productAtIndex)
        self.selectedSellingPrices.remove(at: productAtIndex)
        self.selectedAmounts.remove(at: productAtIndex)
        self.selectedSerials.remove(at: productAtIndex)
        self.selectedBuyingPrices.remove(at: productAtIndex)
        if selectedProductWarranty[productAtIndex] != ""{
            self.selectedProductWarranty.remove(at: productAtIndex)
        }
        if self.selectedExpireDate[productAtIndex] != ""{
            self.selectedExpireDate.remove(at: productAtIndex)
        }
        
    }
    
    func addSerialDataToSelectedProducts(productId: Int, soldQuantity: String, soldSerials: [String], buyingPrice: String, soldSellingPrice: String){
        if let product = findProductInfo(productId: productId){
            let selectedProduct = ProductList(inventoryId: product.inventoryId,productId: productId, quantity: product.quantity, buyingPrice: buyingPrice, sellingPrice: product.sellingPrice, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, soldSerials: soldSerials, name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
            for item in self.selectedProductList{
                if item.productId == productId{
                    self.selectedProductList = self.selectedProductList.filter{($0.productId != productId)}
                }
            }
            self.selectedProductList.append(selectedProduct)
            //self.refreshTableView()
        }
    }
    
    func addDataToSelectedProducts(productId: Int, soldQuantity: String, buyingPrice: String, soldSellingPrice: String){
        if let product = findProductInfo(productId: productId){
            let selectedProduct = ProductList(inventoryId: product.inventoryId, productId: productId, quantity: product.quantity, buyingPrice: buyingPrice, sellingPrice: product.sellingPrice, soldQuantity: soldQuantity, soldSellingPrice: soldSellingPrice, hasSerial: product.hasSerial, serials: product.serials, soldSerials: [""], name: product.name, image: product.image, categoryName: product.categoryName, categoryId: product.categoryId, variant: product.variant, unitName: product.unit)
            for item in self.selectedProductList{
                if item.productId == productId{
                    self.selectedProductList = self.selectedProductList.filter{($0.productId != productId)}
                }
            }
            self.selectedProductList.append(selectedProduct)
            self.refreshTableView()
        }
    }
    
    func findProductInfo(productId: Int) -> ProductList?{
        var productItem : ProductList?
        for item in self.productList{
            if item.productId == productId{
                productItem = item
            }
        }
        return productItem
    }
    
    func removeDataFromSelectableProductList(productId: Int){
        if let product = findProductInfo(productId: productId){
            self.selectableProductList = self.selectableProductList.filter{($0.productId != product.productId)}
            self.refreshTableView()
        }
    }
    
}

