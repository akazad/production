//
//  PurchaseBook.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

//import Foundation
//import ObjectMapper
//
//class PurchaseBook : Mappable {
//    var date : String?
//    var dayPurchase : String?
//    var dayPurchaseAmount : String?
//    var dayPayable : String?
//
//    required init(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//
//        date <- map["date"]
//        dayPurchase <- map["day_purchase"]
//        dayPurchaseAmount <- map["day_purchase_amount"]
//        dayPayable <- map["day_payable"]
//    }
//
//}
import Foundation
import ObjectMapper

class PurchaseBook : Mappable {
    var date : String = ""
    var dayPurchase : Int = 0
    var dayPurchaseAmount : String = ""
    var dayPayable : String = ""
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        date <- map["date"]
        dayPurchase <- map["day_purchase"]
        dayPurchaseAmount <- map["day_purchase_amount"]
        dayPayable <- map["day_payable"]
    }
    
}

