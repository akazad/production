//
//  PerDayPurchaseDataMapper.swift
//  Ponno
//
//  Created by a k azad on 30/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

//import Foundation
//import ObjectMapper
//
//class PerDayPurchaseDataMapper : Mappable {
//    var success : Bool?
//    var message : String?
//    var date : String = ""
//    var totalPurchases : Int = 0
//    var totalPurchaseAmount : Int = 0
//    var currentPayable : Int = 0
//    var purchaseList : [PurchaseList]?
//    var currentPage : Int?
//    var currentPageItem : Int?
//    var to : Int?
//    var from : Int?
//    var perPage : Int?
//    var total : Int?
//    var hasMorePage : Bool?
//    var lastPageNo : Int?
//    var firstPageUrl : String?
//    var nextPageUrl : String?
//    var prevPageUrl : String?
//    var lastPageUrl : String?
//
//    required init(map: Map) {
//
//    }
//
//     func mapping(map: Map) {
//
//        success <- map["success"]
//        message <- map["message"]
//        date <- map["date"]
//        totalPurchases <- map["total_purchases"]
//        totalPurchaseAmount <- map["total_purchase_amount"]
//        currentPayable <- map["current_payable"]
//        purchaseList <- map["purchase_list"]
//        currentPage <- map["current_page"]
//        currentPageItem <- map["current_page_item"]
//        to <- map["to"]
//        from <- map["from"]
//        perPage <- map["per_page"]
//        total <- map["total"]
//        hasMorePage <- map["has_more_page"]
//        lastPageNo <- map["last_page_no"]
//        firstPageUrl <- map["first_page_url"]
//        nextPageUrl <- map["next_page_url"]
//        prevPageUrl <- map["prev_page_url"]
//        lastPageUrl <- map["last_page_url"]
//    }
//
//}

import Foundation
import ObjectMapper

class PerDayPurchaseDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var date : String = ""
    var summary : [PerDayPurchaseSummery]?
    var purchaseList : [PurchaseList]?
    var pagination: Pagination?

    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        date <- map["date"]
        summary <- map["summary"]
        purchaseList <- map["purchase_list"]
        pagination <- map["pagination"]
    }
    
}
