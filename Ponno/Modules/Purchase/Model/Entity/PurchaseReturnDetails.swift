//
//  PurchaseReturnDetails.swift
//  Ponno
//
//  Created by a k azad on 27/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PurchaseReturnDetails : Mappable {
    var invoice : String = ""
    var total : String = ""
    var date : String = ""
    var shopName : String = ""
    var shopAddress : String = ""
    var invoiceNote : String = ""
    var vendorName : String = ""
    var vendorPhone : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        invoice <- map["invoice"]
        total <- map["total"]
        date <- map["date"]
        shopName <- map["shop_name"]
        shopAddress <- map["shop_address"]
        invoiceNote <- map["invoice_note"]
        vendorName <- map["vendor_name"]
        vendorPhone <- map["vendor_phone"]
    }
    
}
