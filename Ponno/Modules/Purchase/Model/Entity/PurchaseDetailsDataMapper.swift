//
//  PurchaseDetailsDataMapper.swift
//  Ponno
//
//  Created by a k azad on 9/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PurchaseDetailsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var purchaseDetails : PurchaseDetails?
    var purchaseProducts : [PurchaseProducts]?
    var payableTransactions : [PayableTransactions]? 
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        purchaseDetails <- map["purchase_details"]
        purchaseProducts <- map["purchase_products"]
        payableTransactions <- map["payable_transactions"]
    }
    
}

