//
//  PurchaseDetails.swift
//  Ponno
//
//  Created by a k azad on 9/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PurchaseDetails : Mappable {
    var invoice : String = ""
    var total : String = ""
    var discount : String = ""
    var discountUnit : String = ""
    var discountAmount : String = ""
    var toBePaid : String = ""
    var paid : String = ""
    var payable : String = ""
    var createdAt : String = ""
    var vendorName : String = ""
    var vendorPhone : String = ""
    var vendorAddress : String = ""
    var currentPayable : String = ""
    var shopName : String = ""
    var shopAddress : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        invoice <- map["invoice"]
        total <- map["total"]
        discount <- map["discount"]
        discountUnit <- map["discount_unit"]
        discountAmount <- map["discount_amount"]
        toBePaid <- map["to_be_paid"]
        paid <- map["paid"]
        payable <- map["payable"]
        createdAt <- map["created_at"]
        vendorName <- map["vendor_name"]
        vendorPhone <- map["vendor_phone"]
        vendorAddress <- map["vendor_address"]
        currentPayable <- map["current_payable"]
        shopName <- map["shop_name"]
        shopAddress <- map["shop_address"]
    }
    
}

