//
//  PayableTransactions.swift
//  Ponno
//
//  Created by a k azad on 9/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PayableTransactions : Mappable {
    var createdAt : String = ""
    var paid : String = ""
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        createdAt <- map["created_at"]
        paid <- map["paid"]
        
    }
    
}

