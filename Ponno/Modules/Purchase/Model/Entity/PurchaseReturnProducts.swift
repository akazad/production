//
//  PurchaseReturnProducts.swift
//  Ponno
//
//  Created by a k azad on 27/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PurchaseReturnProducts : Mappable {
    var name : String = ""
    var variant : String = ""
    var category : String = ""
    var quantity : String = ""
    var unit : String = ""
    var returnPrice : String = ""
    var total : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        name <- map["name"]
        variant <- map["variant"]
        category <- map["category"]
        quantity <- map["quantity"]
        unit <- map["unit"]
        returnPrice <- map["return_price"]
        total <- map["total"]
    }
    
}

