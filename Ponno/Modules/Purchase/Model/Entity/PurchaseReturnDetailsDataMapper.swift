//
//  PurchaseReturnDetailsDataMapper.swift
//  Ponno
//
//  Created by a k azad on 27/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class PurchaseReturnDetailsDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var purchaseReturnDetails : PurchaseReturnDetails?
    var purchaseReturnProducts : [PurchaseReturnProducts]? 
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        purchaseReturnDetails <- map["purchase_return_details"]
        purchaseReturnProducts <- map["purchase_return_products"]
    }
    
}
