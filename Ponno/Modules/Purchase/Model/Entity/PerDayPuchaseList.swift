//
//  PerDayPuchaseList.swift
//  Ponno
//
//  Created by a k azad on 30/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PurchaseList : Mappable {
    var id : Int = 0
    var invoice : String = ""
    var total : String = ""
    var paid : String = ""
    var payable : String = ""
    var invoicePayable : String = ""
    var createdAt : String = ""
    var vendorId : Int = 0
    var vendorName : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        id <- map["id"]
        invoice <- map["invoice"]
        total <- map["total"]
        paid <- map["paid"]
        payable <- map["payable"]
        invoicePayable <- map["invoice_payable"]
        createdAt <- map["created_at"]
        vendorId <- map["vendor_id"]
        vendorName <- map["vendor_name"]
    }
    
}

