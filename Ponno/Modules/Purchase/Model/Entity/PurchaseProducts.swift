//
//  PurchaseProducts.swift
//  Ponno
//
//  Created by a k azad on 9/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PurchaseProducts : Mappable {
    var name : String = ""
    var categoryName : String = ""
    var variant : String = ""
    var quantity : String = ""
    var buyingPrice : String = ""
    var total : String = ""
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        name <- map["name"]
        categoryName <- map["category_name"]
        variant <- map["variant"]
        quantity <- map["quantity"]
        buyingPrice <- map["buying_price"]
        total <- map["total"]
    }
    
}

