//
//  PurchaseService.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireObjectMapper

public class PurchaseService : NSObject{
    
    func getPurchaseData(page: Int, success: @escaping (PurchaseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Purchase + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(url)
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PurchaseDataMapper>) in
                if let purchaseResponse = response.result.value{
                    if let purchase = purchaseResponse.purchaseBook, purchase.count > 0{
                        if purchaseResponse.success == true{
                            success(purchaseResponse)
                        }else if let failureMessage = purchaseResponse.message{
                            failure(failureMessage)
                        }
                    }else{
                        failure(CommonMessages.ApiFailure)
                    }
                }
        }
    }
    
    func getPurchaseSearchData(page: Int, startDate: String, endDate: String, success: @escaping (PurchaseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Purchase + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getDateRangePage(text: page)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PurchaseDataMapper>) in
                if let purchaseResponse = response.result.value{
                    if let purchase = purchaseResponse.purchaseBook, purchase.count > 0{
                        if purchaseResponse.success == true{
                            success(purchaseResponse)
                        }else if let failureMessage = purchaseResponse.message{
                            failure(failureMessage)
                        }
                    }else{
                        failure(CommonMessages.ApiFailure)
                    }
                }
        }
    }
    
    func getPreOrdersData(page: Int, success: @escaping (PreOrdersDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PreOrders + RestURL.sharedInstance.getPaginationNumber(page: page)
        print(url)
        let header = RestURL.sharedInstance.getCommonHeader()
        
//        Alamofire.request(url, headers: header)
//            .responseObject { (response: DataResponse<PreOrdersDataMapper>) in
//                if let preOrdersResponse = response.result.value{
//                    if let preOrders = preOrdersResponse.preorders, preOrders.count > 0{
//                        if preOrdersResponse.success == true{
//                            success(preOrdersResponse)
//                        }else if let failureMessage = preOrdersResponse.message{
//                            failure(failureMessage)
//                        }
//                    }else{
//                        failure(CommonMessages.ApiFailure)
//                    }
//                }
//        }
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PreOrdersDataMapper>) in
                if let preOrdersResponse = response.result.value{
                    if preOrdersResponse.success == true{
                        if let preOrders = preOrdersResponse.preorders, preOrders.count > 0{
                            success(preOrdersResponse)
                        }else if let failureMessage = preOrdersResponse.message{
                            failure(failureMessage)
                        }
                    }else{
                        failure(CommonMessages.ApiFailure)
                    }
                }
        }
    }
    
    //Mark: PreOrdersStore
    func PostPreOrderAddData(param : [String : Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.PreOrdersAdd
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(URL, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let preOrdersAddResponse = response.result.value {
                if preOrdersAddResponse.success == true{
                    success(preOrdersAddResponse)
                }else if let failureResponse = preOrdersAddResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    //Mark: PreOrdersCancel
    func PostPreOrderCancelData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.PreOrdersCancel
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["preorder_id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(URL, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let cancelPreOrdersResponse = response.result.value {
                if cancelPreOrdersResponse.success == true{
                    success(cancelPreOrdersResponse)
                }else if let failureResponse = cancelPreOrdersResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func getPerDayPurchaseData(page: Int, date: String, success: @escaping (PerDayPurchaseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PerDayPurchase + RestURL.sharedInstance.getPerDayPurchaseUrl(date: date) + RestURL.sharedInstance.getPageNumber(page: page)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PerDayPurchaseDataMapper>) in
                if let purchasePerDayResponse = response.result.value{
                    if let perDayResponse = purchasePerDayResponse.purchaseList, perDayResponse.count > 0{
                        success(purchasePerDayResponse)
                    }
                    else if let failureResponse = purchasePerDayResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getPerDayPurchaseSearchData(page: Int, date: String, searchText: String, success: @escaping (PerDayPurchaseDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PerDayPurchase + RestURL.sharedInstance.getPerDayPurchaseUrl(date: date) + RestURL.sharedInstance.getSearchText(text: searchText) + RestURL.sharedInstance.getPageNumber(page: page)
        
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PerDayPurchaseDataMapper>) in
                if let purchasePerDayResponse = response.result.value{
                    if let perDayResponse = purchasePerDayResponse.purchaseList, perDayResponse.count > 0{
                        success(purchasePerDayResponse)
                    }
                    else if let failureResponse = purchasePerDayResponse.message{
                        failure(failureResponse)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //Mark: PurchaseCancel
    func PostPerDayPurchaseCancelData(id: Int , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.PurchaseCancel
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = ["id": id]
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(URL, method: .post, parameters: param, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let cancelPurchaseResponse = response.result.value {
                if cancelPurchaseResponse.success == true{
                    success(cancelPurchaseResponse)
                }else if let failureResponse = cancelPurchaseResponse.message{
                    failure(failureResponse)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
            
        }
        print(request)
    }
    
    func getPurchaseDetailsData(id: Int, success: @escaping (PurchaseDetailsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PurchaseDetails + RestURL.sharedInstance.getPurchaseDetailsUrl(id: id)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PurchaseDetailsDataMapper>) in
                if let purchaseDetailsResponse = response.result.value {
                    if purchaseDetailsResponse.success == true{
                        success(purchaseDetailsResponse)
                    }
                    else if let failureMessage = purchaseDetailsResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    func getPurchaseReturnDetailsData(id: Int, success: @escaping (PurchaseReturnDetailsDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PurchaseReturnDetails + RestURL.sharedInstance.getPurchaseReturnDetailsUrl(id: id)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PurchaseReturnDetailsDataMapper>) in
                if let purchaseReturnDetailsResponse = response.result.value {
                    if purchaseReturnDetailsResponse.success == true{
                        success(purchaseReturnDetailsResponse)
                    }
                    else if let failureMessage = purchaseReturnDetailsResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //Mark: PostRequest
    func postNewPurchaseData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.PurchaseAdd
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(URL, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let purchaseAddResponse = response.result.value {
                if purchaseAddResponse.success == true{
                    success(purchaseAddResponse)
                }else if let failureMessage = purchaseAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    
    //Mark: PostRequest
    func postNewRefundableData(params: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PurchaseReturn
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(url, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let refundAddResponse = response.result.value {
                if refundAddResponse.success == true{
                    success(refundAddResponse)
                }else if let failureMessage = refundAddResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func getPurchaseReturnData(purchaseReturnDataArray: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PurchaseAdd
        print(url)
        let header = RestURL.sharedInstance.postCommonHeader()
        
        let param = purchaseReturnDataArray
        
        let request = Alamofire.request(url, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let purchaseReturnResponse = response.result.value{
                if purchaseReturnResponse.success == true{
                    success(purchaseReturnResponse)
                }else if let failureMessage = purchaseReturnResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    
    func getPreOrderEditData(id: Int, success: @escaping (PreOrderProductEditDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PreOrderEditProducts + RestURL.sharedInstance.getPreOrderEditProductListUrl(id: id)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PreOrderProductEditDataMapper>) in
                if let preOrderProductEditResponse = response.result.value {
                    if preOrderProductEditResponse.success == true{
                        success(preOrderProductEditResponse)
                    }
                    else if let failureMessage = preOrderProductEditResponse.message{
                        failure(failureMessage)
                    }
                }else{
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //PostPreOrderUpdateData
    func postPreOrderUpdateData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.baseUrl + RestURL.sharedInstance.PreOrderUpdate
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(URL, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let preOrderUpdateResponse = response.result.value {
                if preOrderUpdateResponse.success == true{
                    success(preOrderUpdateResponse)
                }else if let failureMessage = preOrderUpdateResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func postPreOrderReceiveData(param: [String: Any] , success: @escaping (AddDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let URL = RestURL.sharedInstance.baseUrl + RestURL.sharedInstance.PreOrderReceive
        let header = RestURL.sharedInstance.postCommonHeader()
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let request = Alamofire.request(URL, method: .post, parameters: param as Parameters, encoding: JSONEncoding.default, headers: header).responseObject{ (response: DataResponse<AddDataMapper>) in
            if let preOrderReceiveResponse = response.result.value {
                if preOrderReceiveResponse.success == true{
                    success(preOrderReceiveResponse)
                }else if let failureMessage = preOrderReceiveResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func getPreOrderSearchData(page: Int, startDate: String, endDate: String, success: @escaping (PreOrdersDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.PreOrders + RestURL.sharedInstance.getDateRange(startDate: startDate, endDate: endDate) + RestURL.sharedInstance.getDateRangePage(text: page)
        print(url)
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: header)
            .responseObject { (response: DataResponse<PreOrdersDataMapper>) in
                if let preOrderResponse = response.result.value{
                    if let preOrder = preOrderResponse.preorders, preOrder.count > 0{
                        if preOrderResponse.success == true{
                            success(preOrderResponse)
                        }else if let failureMessage = preOrderResponse.message{
                            failure(failureMessage)
                        }
                    }else{
                        failure(CommonMessages.ApiFailure)
                    }
                }
        }
    }
    
}

