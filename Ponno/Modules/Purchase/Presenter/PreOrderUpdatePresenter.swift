//
//  PreOrderUpdatePresenter.swift
//  Ponno
//
//  Created by a k azad on 2/6/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol preOrderUpdateViewDelegate : NSObjectProtocol {
    func setPreOrderEditData(data: PreOrderProductEditDataMapper?)
    func postPreOrderUpdateData(data: AddDataMapper)
    func postPreOrderReceiveData(data: AddDataMapper)
    //func postPreOrderCancelData(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class PreOrderUpdatePresenter: NSObject {
    
    private let service : PurchaseService
    weak private var viewDelegate : preOrderUpdateViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : preOrderUpdateViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getPreOrderDataFromServer(id : Int){
        self.viewDelegate?.showLoading()
        service.getPreOrderEditData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPreOrderEditData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postPreOrderUpdateData(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postPreOrderUpdateData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.postPreOrderUpdateData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postPreOrderReceiveDataToServer(param : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postPreOrderReceiveData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.postPreOrderReceiveData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
//    func postPreOrderCancelDataToServer(id: Int){
//        self.viewDelegate?.showLoading()
//        self.service.PostPreOrderCancelData(id: id, success: { (data) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.postPreOrderCancelData(data: data)
//        }, failure: { (message) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onFailed(data: message)
//        })
//    }
    
}

