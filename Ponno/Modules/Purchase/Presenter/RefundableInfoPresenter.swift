//
//  RefundableInfoPresenter.swift
//  Ponno
//
//  Created by a k azad on 20/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol refundableInfoViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func setVendorsData(data: VendorsDataMapper)
    func postNewVendorData(data: VendorAddDataMapper)
    func onVendorsDataFailed(data: String)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class RefundableInfoPresenter: NSObject {
    
    private let service : PurchaseService
    weak private var viewDelegate : refundableInfoViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : refundableInfoViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.getVendorsData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewVendorDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.postAddNewVendorData(vendorData: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.postNewVendorData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onVendorsDataFailed(data: message)
        })
    }
    
    func postRefundableDataToServer(refundData : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewRefundableData(params: refundData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
