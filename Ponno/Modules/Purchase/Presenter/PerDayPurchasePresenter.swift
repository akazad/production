//
//  PerDayPurchasePresenter.swift
//  Ponno
//
//  Created by a k azad on 30/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PerDayPurchaseViewDelegate : NSObjectProtocol {
    func setPerDayPurchaseList(data : PerDayPurchaseDataMapper)
    func setPerDayPurchaseCancelData(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class PerDayPurchasePresenter: NSObject {
    
    private let service : PurchaseService
    weak private var viewDelegate : PerDayPurchaseViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PerDayPurchaseViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func  getPerDayPurchaseDataFromServer(page: Int, date : String){
        self.viewDelegate?.showLoading()
        self.service.getPerDayPurchaseData(page: page, date: date, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPerDayPurchaseList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postPerDayPurchaseCancelDataToServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostPerDayPurchaseCancelData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPerDayPurchaseCancelData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
