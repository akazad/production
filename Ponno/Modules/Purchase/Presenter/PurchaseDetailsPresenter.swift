//
//  PurchaseDetailsPresenter.swift
//  Ponno
//
//  Created by a k azad on 9/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PurchaseDetailsViewDelegate : NSObjectProtocol {
    func setPurchaseDetailsData(data : PurchaseDetailsDataMapper)
    func onFailed()
    func showLoading()
    func hideLoading()
}
class PurchaseDetailsPresenter: NSObject {
    
    private let service : PurchaseService
    weak private var viewDelegate : PurchaseDetailsViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PurchaseDetailsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getPurchaseDetailsDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getPurchaseDetailsData(id: id, success: { (data)   in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPurchaseDetailsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
}
