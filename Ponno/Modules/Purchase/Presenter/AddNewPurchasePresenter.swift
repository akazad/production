//
//  AddNewPurchasePresenter.swift
//  Ponno
//
//  Created by a k azad on 13/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol addNewPurchaseViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func setVendorsData(data: VendorsDataMapper)
    func postNewVendorData(data: VendorAddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class AddNewPurchasePresenter: NSObject {
    
    private let service : PurchaseService
    weak private var viewDelegate : addNewPurchaseViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : addNewPurchaseViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsDataFromServer(page: Int){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.getVendorsData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getAllVendorsDataFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.getAllVendorsData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsData(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewVendorDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.postAddNewVendorData(vendorData: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.postNewVendorData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postPurchasableDataToServer(purchasesData : [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.postNewPurchaseData(params: purchasesData, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
