//
//  PreOrderAddInfoPresenter.swift
//  Ponno
//
//  Created by a k azad on 29/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol preOrderAddInfoViewDelegate : NSObjectProtocol {
    func onSuccess(data : AddDataMapper)
    func setVendorsData(data: VendorsDataMapper)
    func postNewVendorData(data: VendorAddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class PreOrderAddInfoPresenter: NSObject {
    
    private let service : PurchaseService
    weak private var viewDelegate : preOrderAddInfoViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : preOrderAddInfoViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getVendorsDataFromServer(getAll : Bool){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.getAllVendorsData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setVendorsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postNewVendorDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        let service = VendorsService()
        service.postAddNewVendorData(vendorData: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.postNewVendorData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postPreOrdersAddDataToServer(param: [String : Any]){
        self.viewDelegate?.showLoading()
        self.service.PostPreOrderAddData(param: param, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
