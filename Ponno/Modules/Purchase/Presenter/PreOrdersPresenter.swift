//
//  PreOrdersPresenter.swift
//  Ponno
//
//  Created by a k azad on 28/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PreOrdersViewDelegate : NSObjectProtocol {
    func setPreOrdersData(data : PreOrdersDataMapper)
    func preOrdersCancelData(data: AddDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}
class PreOrdersPresenter: NSObject {
    
    private let service : PurchaseService
    weak private var viewDelegate : PreOrdersViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PreOrdersViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getPreOrdersDataFromServer(page: Int){
       self.viewDelegate?.showLoading()
        self.service.getPreOrdersData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPreOrdersData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func postPreOrdersCancelDataToServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostPreOrderCancelData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.preOrdersCancelData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func getPreOrderSearchDataFromServer(page: Int, startDate: String, endDate: String){
        self.viewDelegate?.showLoading()
        self.service.getPreOrderSearchData(page: page, startDate: startDate, endDate: endDate, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPreOrdersData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}
