//
//  PurchaseReturnDetailsPresenter.swift
//  Ponno
//
//  Created by a k azad on 27/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PurchaseReturnDetailsViewDelegate : NSObjectProtocol {
    func setPurchaseReturnDetailsData(data : PurchaseReturnDetailsDataMapper)
    func onFailed()
    func showLoading()
    func hideLoading()
}
class PurchaseReturnDetailsPresenter: NSObject {
    
    private let service : PurchaseService
    weak private var viewDelegate : PurchaseReturnDetailsViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PurchaseReturnDetailsViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getPurchaseReturnDetailsDataFromServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.getPurchaseReturnDetailsData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPurchaseReturnDetailsData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed()
        })
    }
    
}

