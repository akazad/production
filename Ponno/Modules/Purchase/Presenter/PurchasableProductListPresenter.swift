//
//  PurchasableProductListPresenter.swift
//  Ponno
//
//  Created by a k azad on 18/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol PurchasableProductListViewDelegate : NSObjectProtocol {
    func setPurchasableProductData(data : ProductListDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}

class PurchasableProductListPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : PurchasableProductListViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PurchasableProductListViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
//    func getProductListDataFromServer(page: Int) {
//        self.viewDelegate?.showLoading()
//        self.service.getProductListData(page: page, success: { (data) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.setPurchasableProductData(data: data)
//        }, failure: {(message) in
//            self.viewDelegate?.hideLoading()
//            self.viewDelegate?.onFailed(data: message)
//        })
//    }
    
    func getProductSearchDataFromServer(page: Int, searchString: String){
        self.viewDelegate?.showLoading()
        self.service.getProductSearchData(page: page, searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPurchasableProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getAllProductFromServer(getAll: Bool){
        self.viewDelegate?.showLoading()
        self.service.getAllProductData(getAll: getAll, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPurchasableProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
}
