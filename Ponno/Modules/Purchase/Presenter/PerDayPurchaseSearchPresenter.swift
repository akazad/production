//
//  PerDayPurchaseSearchPresenter.swift
//  Ponno
//
//  Created by a k azad on 23/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

protocol PerDayPurchaseSearchViewDelegate : NSObjectProtocol {
    func setPerDayPurchaseSearchList(data : PerDayPurchaseDataMapper)
    func setPerDayPurchaseCancelData(data: AddDataMapper)
    func onFailed(data: String) 
    func showLoading()
    func hideLoading()
}
class PerDayPurchaseSearchPresenter: NSObject {
    
    private let service : PurchaseService
    weak private var viewDelegate : PerDayPurchaseSearchViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PerDayPurchaseSearchViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func  getPerDayPurchaseSearchDataFromServer(page: Int, date : String, searchText: String){
        self.viewDelegate?.showLoading()
        self.service.getPerDayPurchaseSearchData(page: page, date: date, searchText: searchText, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPerDayPurchaseSearchList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func postPerDayPurchaseCancelDataToServer(id: Int){
        self.viewDelegate?.showLoading()
        self.service.PostPerDayPurchaseCancelData(id: id, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPerDayPurchaseCancelData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
