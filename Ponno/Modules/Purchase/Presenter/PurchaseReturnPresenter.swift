//
//  PurchaseReturnPresenter.swift
//  Ponno
//
//  Created by a k azad on 13/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol purchaseReturnViewDelegate : NSObjectProtocol {
    func setReturnPurchaseData(data : AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class PurchaseReturnPresenter: NSObject {
    
    private let service : PurchaseService
    weak private var viewDelegate : purchaseReturnViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : purchaseReturnViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postPurchasesReturnDataToServer(purchaseReturnDataArray: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.getPurchaseReturnData(purchaseReturnDataArray: purchaseReturnDataArray, success: { (data) in
            self.viewDelegate?.showLoading()
            self.viewDelegate?.setReturnPurchaseData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message) 
        })
    }
}
