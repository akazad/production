//
//  PurchasePresenter.swift
//  Ponno
//
//  Created by a k azad on 25/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol PurchaseViewDelegate : NSObjectProtocol {
    func setPurchaseList(data : PurchaseDataMapper)
    func onFailed(message: String)
    func showLoading()
    func hideLoading()
}
class PurchasePresenter: NSObject {
    
    private let service : PurchaseService
    weak private var viewDelegate : PurchaseViewDelegate?
    
    init(service : PurchaseService) {
        self.service = service
    }
    
    func attachView(viewDelegate : PurchaseViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func  getPurchaseDataFromServer(page : Int){
        self.viewDelegate?.showLoading()
        self.service.getPurchaseData(page: page, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPurchaseList(data: data)
        }, failure:  { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
    func getPurchaseSearchDataFromServer(page: Int, startDate: String, endDate: String){
        self.viewDelegate?.showLoading()
        self.service.getPurchaseSearchData(page: page, startDate: startDate, endDate: endDate, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setPurchaseList(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}

