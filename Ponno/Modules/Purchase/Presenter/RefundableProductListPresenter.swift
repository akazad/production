//
//  RefundableProductListPresenter.swift
//  Ponno
//
//  Created by a k azad on 20/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol RefundableProductListViewDelegate : NSObjectProtocol {
    func setRefundableProductData(data : ProductListDataMapper)
    func onFailed(message : String)
    func showLoading()
    func hideLoading()
}

class RefundableProductListPresenter: NSObject {
    
    private let service : ProductService
    weak private var viewDelegate : RefundableProductListViewDelegate?
    
    init(service : ProductService) {
        self.service = service
    }
    
    func attachView(viewDelegate : RefundableProductListViewDelegate){ 
        self.viewDelegate = viewDelegate
    }
    
    func getProductSearchDataFromServer(page: Int, searchString: String){
        self.viewDelegate?.showLoading()
        self.service.getProductSearchData(page: page, searchString: searchString, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setRefundableProductData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(message: message)
        })
    }
    
}
