//
//  RegistrationViewController.swift
//  Ponno
//
//  Created by a k azad on 8/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    
    
    @IBOutlet weak var segmentedController: UISegmentedControl!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var registrationBtn: UIButton!
    @IBOutlet weak var alreadyHaveAnAccountLbl: UILabel!
    @IBOutlet weak var logInBtn: UIButton!
    private var presenter = RegistrationPresenter(service: LoginService())
    
    var businessCategories : [RegistrationShopCategories] = []
    var categoryPicker = UIPickerView()
    var category : String?
    var businessCategory: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.setUpInitialLanguage()
        self.navigationController?.navigationBar.isHidden = true
        self.initialSetUp()
        self.configureTextField()
        self.setupToolbar()
        self.setUpPickerView()
        self.attachPresenter()
    }
    
    func initialSetUp(){
        //self.title = LanguageManager.Registration
        self.category = ""
        self.logInBtn.addTarget(self, action: #selector(onLogin), for: .touchUpInside)
        
        self.segmentedController.addTarget(self, action: #selector(onBusinessCategoryChange), for: .valueChanged)
//        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.red], for: .selected)
        segmentedController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
        
    }
    
//    @objc func onSubmission(){
//        self.setUpRegistrationBtn()
//    }
    
    func setUpPickerView(){
        
        categoryPicker.delegate = self
        //categoryPicker.selectedRow(inComponent: 0)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDone(sender:)))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.categoryTextField.inputView = categoryPicker
        self.categoryTextField.inputAccessoryView = toolBar
    }
    
    @objc func onPressingDone(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @objc func onLogin(sender : UIButton){
        let storyboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let loginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navigationController = UINavigationController(rootViewController: loginViewController)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = navigationController
    }
    
    @objc func onBusinessCategoryChange(sender: UISegmentedControl){
        
        if sender.selectedSegmentIndex == 0{
            self.businessCategory = 2
            if let category = self.businessCategory{
                categoryTextField.text = ""
                getBusinessCategoryFromSegment(category: category)
            }
            
        }else{
            self.businessCategory = 1
            if let category = self.businessCategory{
                categoryTextField.text = ""
                getBusinessCategoryFromSegment(category: category)
            }
        }
    }
    
    func getBusinessCategoryFromSegment(category: Int){
        self.presenter.getShopCategoryDataFromServer(businessCategory: category)
    }
    
//    func setUpRegistrationBtn(){
//        if isValidated(){
//            guard let name = nameText.text, let phone = phoneText.text, let password = passwordText.text else{
//                return
//            }
//            let param : [String: Any] = ["name": name, "phone": phone, "password": password]
//
//            self.presenter.postRegistrationDataToServer(data: param)
//
//        }
//    }
    
    
    @IBAction func registerBtn(_ sender: UIButton) {
        if isValidated(){
            guard let name = nameText.text, let phone = phoneText.text, let password = passwordText.text, let category = self.category else{
                return
            }
            let param : [String: Any] = ["name": name, "phone": phone, "password": password, "category" : category]

            if let json = String(data: try! JSONSerialization.data(withJSONObject: param, options: .prettyPrinted), encoding: .utf8 ){
                print(json)
            }

            self.presenter.postRegistrationDataToServer(data: param)

        }
    }
    
    func submitBtnControlWith(isEnabled : Bool){
        self.registrationBtn.isEnabled = isEnabled
    }
    
    func isValidated()->Bool{
        
        if (self.nameText.text?.isEmpty)!{
            showAlert(title: LanguageManager.Warning, message: LanguageManager.NameIsRequired)
            return false
        }
        else if (self.phoneText.text?.isEmpty)!{
            showAlert(title: LanguageManager.Warning, message: LanguageManager.PhoneNumberIsRequired)
            return false
        }else if (self.passwordText.text?.isEmpty)!{
            showAlert(title: LanguageManager.Warning, message: LanguageManager.PasswordIsRequired)
            return false
        }else if (self.repeatPasswordTextField.text?.isEmpty)!{
            showAlert(title: LanguageManager.Warning, message: LanguageManager.RepeatPasswordIsRequired)
            return false
        }
        else if (self.passwordText.text?.elementsEqual(self.repeatPasswordTextField.text!)) != true{
            showAlert(title: LanguageManager.Warning, message: LanguageManager.PasswordDidNotMatch)
            return false
        }
        else{
            return true
        }
    }
    
    func successMessage(userMessage:String) -> Void {
        let alertController = UIAlertController(title: LanguageManager.Congratulations, message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in

                guard let phone = self.phoneText.text, let password = self.passwordText.text else{
                        return
                }
                self.presenter.getLoginDataFromServer(userPhoneNumber: phone, userPassword: password)
                    
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
    }
    
    func setupToolbar(){
        let phoneToolBar = UIToolbar()
        phoneToolBar.barStyle = UIBarStyle.default
        phoneToolBar.isTranslucent = true
        phoneToolBar.tintColor = UIColor.black
        phoneToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let phoneDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnPhone(sender:)))
        
        phoneToolBar.setItems([cancelButton, spaceButton, phoneDoneButton], animated: false)
        phoneToolBar.isUserInteractionEnabled = true
        
        self.phoneText.inputAccessoryView = phoneToolBar
        
    }
    
    @objc func onPressingDoneOnPhone(sender: UIBarButtonItem){
        self.passwordText.becomeFirstResponder()
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
//    @objc func onPressingDoneOnPassword(sender: UIBarButtonItem){
////        self.passwordText.resignFirstResponder()
////        self.nameText.becomeFirstResponder()
//    }

}

//Mark: PickerViewDelegate
extension RegistrationViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
//        guard let categoryItems = self.categories, categoryItems.count > 0 else {
//            return 0
//        }
        
        return self.businessCategories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        guard let categoryItems = self.categories, categoryItems.count > 0 else {
//            return ""
//        }
        if self.businessCategories.count > 0 {
            let item = self.businessCategories[row]
            self.categoryTextField.text = item.name
            return item.name
        }
        //self.categoryTextField.text = self.businessCategories[row].name
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //if let selectedRow =
        self.categoryTextField.text = self.businessCategories[row].name
        self.category = "\(self.businessCategories[row].id)"
    }
    
}

extension RegistrationViewController : UITextFieldDelegate{
    func configureTextField(){
        self.repeatPasswordTextField.delegate = self
        self.passwordText.delegate = self
        self.phoneText.delegate = self
        self.nameText.delegate = self
        self.passwordText.underlined()
        self.repeatPasswordTextField.underlined()
        self.phoneText.underlined()
        self.nameText.underlined()
        self.categoryTextField.underlined()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.nameText {
            self.phoneText.becomeFirstResponder()
        }else if textField == self.passwordText{
            self.repeatPasswordTextField.becomeFirstResponder()
        }else if textField == self.repeatPasswordTextField{
            self.categoryTextField.becomeFirstResponder()
            self.repeatPasswordTextField.resignFirstResponder()
        }
        return false
    }
}

//Mark: Api Delegate
extension RegistrationViewController: RegistrationViewDelegate{
    func setLoginData(loginData: LoginDataMapper) {
        
        self.navigateToHomeViewController()
    }
    
    func onSuccess(data: RegistrationDataMapper) {
        guard let message = data.message else {
            return
        }
        self.successMessage(userMessage: message)
        
    }
    
    func setShopCategoryData(data: RegistrationShopCategoryDataMapper) {
        guard let categories = data.categories, categories.count > 0 else {
            return
        }
        self.businessCategories = categories
    }
    
    func onFailed(data: String) {
        showAlert(title: data, message: "")
    }
    
    func onShopCategoryFailed(data: String) {
        
    }
    
    func showLoading() {
        self.submitBtnControlWith(isEnabled: false)
        self.showLoader()
    }
    
    func hideLoading() {
        self.submitBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
        
        self.presenter.getShopCategoryDataFromServer(businessCategory: 2)
    }
    
//    fileprivate func navigateToDashboardVC(){
//        let storyboard: UIStoryboard = UIStoryboard(name: "Dashboard", bundle: nil)
//        let navController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "DashboardViewController"))
//        self.present(navController, animated: true, completion: {})
//
//    }
    
    fileprivate func navigateToHomeViewController(){
        let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        if let category = getBusinessCategory(){
            if category.businessCategory == 1{
                let navController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "FreemiumHomeViewController"))
                navController.modalPresentationStyle = .overFullScreen
                self.present(navController, animated: true, completion: {})
            }else{
                let navController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "HomeViewController"))
                navController.modalPresentationStyle = .overFullScreen
                self.present(navController, animated: true, completion: {})
            }
        }
    }
    
}
