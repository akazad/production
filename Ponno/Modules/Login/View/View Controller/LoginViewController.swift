//
//  LoginViewController.swift
//  Ponno
//
//  Created by a k azad on 18/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Crashlytics

class LoginViewController: UIViewController {

    
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var userPhoneTextField: UITextField!
//        {
//        didSet{
//            userPhoneTextField.placeholder = LanguageManager.PhoneNumber
//        }
//    }
    @IBOutlet weak var userPasswordTextField: UITextField!
//        {
//        didSet{
//            userPasswordTextField.placeholder = LanguageManager.Password
//        }
//    }
    @IBOutlet weak var loginBtn: UIButton!
//        {
//        didSet{
//            loginBtn.setTitle(LanguageManager.Login, for: .normal)
//        }
//    }
    @IBOutlet weak var dontHaveAnAccountLbl: UILabel!
//        {
//        didSet{
//            dontHaveAnAccountLbl.text = LanguageManager.DontHaveAnAccount
//        }
//    }
    @IBOutlet weak var registrationBtn: UIButton!
//        {
//        didSet{
//            registrationBtn.setTitle(LanguageManager.Registration, for: .normal)
//        }
//    }
    
    private var presenter = LoginPresenter(service : LoginService())
    
    //OfflineProduct
    var newOfflineProductList: [OfflineProduct]?
    var updatedOfflineProductList: [OfflineProduct]?
    var deletedOfflineProductList: [OfflineProduct]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LanguageManager.setLanguageAs(language: .Bangla)
        self.attachPresenter()
        self.navigationController?.navigationBar.isHidden = true
        self.configureTextFields()
        self.setUpToolBar()
        //self.setUpRegistrationBtn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //self.setUpInitialLanguage()
    }
    
    @IBAction func loginBtnAction(_ sender: UIButton) {
        if self.isValidated(){
            guard let phone = userPhoneTextField.text, let password = userPasswordTextField.text else{
                return
            }
            
            self.presenter.getLoginDataFromServer(userPhoneNumber: phone, userPassword: password)
        }
    }
    
    func isValidated()->Bool{
        if (self.userPhoneTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.PhoneNumberIsRequired)
            return false
        }
        
        let phone = self.userPhoneTextField.text ?? ""
        if (!self.validatePhoneNumber(value: phone)){
            showAlert(title: LanguageManager.PhoneNumberIsIncorrect, message: "")
        }else if (self.userPasswordTextField.text?.isEmpty)!{
            self.displayMessage(userMessage: LanguageManager.PasswordIsRequired)
            return false
        }
        return true
    }
    
    func loginBtnControlWith(isEnabled : Bool){
        self.loginBtn.isEnabled = isEnabled
    }
    
    func setUpToolBar(){
        let phoneToolBar = UIToolbar()
        phoneToolBar.barStyle = UIBarStyle.default
        phoneToolBar.isTranslucent = true
        phoneToolBar.tintColor = UIColor.black
        phoneToolBar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingCancel(sender:)))
        
        let phoneDoneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(onPressingDoneOnPhone(sender:)))
        
        phoneToolBar.setItems([cancelButton, spaceButton, phoneDoneButton], animated: false)
        phoneToolBar.isUserInteractionEnabled = true
        
        self.userPhoneTextField.inputAccessoryView = phoneToolBar
    }
    
    @objc func onPressingDoneOnPhone(sender: UIBarButtonItem){
        self.userPhoneTextField.resignFirstResponder()
        self.userPasswordTextField.becomeFirstResponder()
    }
    
    @objc func onPressingCancel(sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: "", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
            
    }
    
//    func navigateToDashboardVC(){
//        let storyboard: UIStoryboard = UIStoryboard(name: "Dashboard", bundle: nil)
//        let navController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "DashboardViewController"))
//        self.present(navController, animated: true, completion: {})
//
//    }
    
    fileprivate func navigateToHomeViewController(){
        let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        if let viewSettings = getViewSettings(){
            if viewSettings.dashboard == "FreemiumDashboard"{
                
                self.presenter.getOfflineProductFromServer(timeStamp: "default")
                
                let navController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "FreemiumHomeViewController"))
                navController.modalPresentationStyle = .overFullScreen
                self.present(navController, animated: true, completion: {})
            }else{
                let navController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "HomeViewController"))
                navController.modalPresentationStyle = .overFullScreen
                self.present(navController, animated: true, completion: {})
            }
        }        
    }
    
    @IBAction func registerBtn(_ sender: UIButton) {
        self.navigateToRegistrationVC()
    }
    
    //Registration
    func setUpRegistrationBtn(){
        self.registrationBtn.addTarget(self, action: #selector(onPressRegistrationBtn), for: .touchUpInside)
    }
    
    @objc func onPressRegistrationBtn(_ sender: UIButton){
        self.navigateToRegistrationVC()
    }
    
    func navigateToRegistrationVC(){
        let storyboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
        
        let registrationViewController = storyboard.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
        let navigationController = UINavigationController(rootViewController: registrationViewController)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = navigationController
    }
    
}

// TextField Delegate
extension LoginViewController : UITextFieldDelegate{
    
    fileprivate func configureTextFields(){
        self.userPhoneTextField.delegate = self
        self.userPasswordTextField.delegate = self
        self.userPhoneTextField.underlined()
        self.userPasswordTextField.underlined()
        //self.userPhoneTextField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.userPhoneTextField{
            self.userPasswordTextField.becomeFirstResponder()
        }
        else{
            self.view.endEditing(true)
            return true
        }
        return false
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        
//        let currentCharacterCount = textField.text?.count ?? 0
//        if range.length + range.location > currentCharacterCount {
//            return false
//        }
//        let newLength = currentCharacterCount + string.count - range.length
//        return newLength <= 11
//    }
}

//Mark: Api Delegate
extension LoginViewController : LoginViewDelegate{
    func showLoading() {
        self.loginBtnControlWith(isEnabled: false)
       self.showLoader()
    }
    func setLoginData(loginData: LoginDataMapper) {
        self.navigateToHomeViewController()
    }
    
    func setOfflineProduct(data: OfflineProductDataMapper) {
        self.newOfflineProductList = data.newProductList
        self.updatedOfflineProductList = data.updatedProductList
        self.deletedOfflineProductList = data.deletedProductList
    }
    
    func onLoginFailed(data: String) {
        self.loginBtnControlWith(isEnabled: true)
        self.displayMessage(userMessage: data)
    }
    
    func onDataSyncingFailed(data: String) {
        self.showAlert(title: "Data Syncing Fail", message: "") 
    }
    
    func hideLoading() {
        self.loginBtnControlWith(isEnabled: true)
        self.hideLoader()
    }
//    func attachView(presenter : LoginViewDelegate){
//        self.presenter.attachView(viewDelegate: presenter)
//    }
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}

extension UITextField {
    
    func underlined(){
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}


