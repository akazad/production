//
//  LoginService.swift
//  Ponno
//
//  Created by a k azad on 19/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

public class LoginService : NSObject{
    
    func postLoginData(userPhoneNumber : String, userPassword : String, success: @escaping (LoginDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Login
        
        let parameters = RestURL.sharedInstance.getLoginParams(userPhoneNumber: userPhoneNumber, userPassword: userPassword)
        
        if let json = String(data: try! JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted), encoding: .utf8 ){
            print(json)
        }
        
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let request = Alamofire.request(url, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: header).responseObject { (response: DataResponse<LoginDataMapper>) in
            if let loginResponse = response.result.value{
                if loginResponse.success == true{
                    if let token = loginResponse.token{
                        setRememberToken(token: token)
                        setLoggedIn(status: true)
                    }
                    if let shopInfo = loginResponse.shop{
                        let shopData = ShopInfo(name: shopInfo.name, registrationNumber: shopInfo.registrationNumber, address: shopInfo.address, image: shopInfo.image, inventorySystem: shopInfo.inventorySystem, deliverySystem: shopInfo.deliverySystem, invoiceNote: shopInfo.invoiceNote, paymentStatus: shopInfo.paymentStatus, accStatus: shopInfo.accStatus, category: shopInfo.category)
                        setShopData(shopInfo: shopData)
                    }
                    if let userInfo = loginResponse.user{
                        let userData = UserInfo(id : userInfo.id,name: userInfo.name, phone: userInfo.phone, parent : userInfo.parent, shopId: userInfo.shopId)
                        setUserData(userInfo: userData)
                    }
                    if let permission = loginResponse.assignedPermission{
                        let id = AssignedPermission(id: permission)
                        setPermission(permission: id)
                    }
                    let timeStamp = TimeStamp(timeStamp: "default")
                    setTimeStamp(timeStamp: timeStamp)
                    
                    let dummyDataStatus = DummyData(status : loginResponse.btnDummyData)
                    setDummyData(dummyData: dummyDataStatus)
                    
                    if let languageStatus = loginResponse.user?.language{
                        if languageStatus == 0{
                            setLanguage(isBangla: true)
                        }else{
                            setLanguage(isBangla: false)
                        }
                    }
                    
                    if let businessCategory = loginResponse.businessCategory{
                        let category = BusinessCategory(businessCategory: businessCategory)
                        setBusinessCategoryData(category: category)
                    }
                    
                    if let viewSettings = loginResponse.appViewSettings{
                        if let navItem = viewSettings.sideMenu?.navigationItem, let dueCust = viewSettings.dueCustomer?.dueCustomer, let set = viewSettings.shopSettings?.settings, let dash = viewSettings.dashboard?.dashboard, let freem = viewSettings.product?.freemiumProduct, let sPanel = viewSettings.salePanel?.freemiumSalePanel{
                            let settings = ViewSettings(navigationItem: navItem, dueCustomer: dueCust, settings: set, dashboard: dash, freemiumProduct: freem, salePanel: sPanel)
                            setViewSettings(settings: settings)
                        }
                    }else{
                        let settings = ViewSettings(navigationItem: "", dueCustomer: "", settings: "", dashboard: "", freemiumProduct: "", salePanel: "")
                        setViewSettings(settings: settings)
                    }
                    
                    if let list = loginResponse.permissionList{
                        setPermissionList(list: list)
                    }
                    
                    success(loginResponse)
                }else{
                    guard let message = loginResponse.message else{
                        return
                    }
                    failure(message)
                }
            }else {
                failure(CommonMessages.ApiFailure)
            }
        }
        print(request)
    }
    
    func getVersionCheckData(success: @escaping (VersionCheckDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.VersionCheck
        //print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<VersionCheckDataMapper>) in
                if let versionCheckResponse = response.result.value{
                    if versionCheckResponse.success == true {
                        success(versionCheckResponse)
                    }else if let failureMessage = versionCheckResponse.message{
                        failure(failureMessage)
                    }
                }
                else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
    //Mark: RegistrationPost
    func postRegistrationData(data: [String: Any], success: @escaping (RegistrationDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet{
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.Registration
        let header = RestURL.sharedInstance.getCommonHeader()
        
        let parameters = data
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseObject { (response: DataResponse<RegistrationDataMapper>) in
            if let registrationResponse = response.result.value{
                if registrationResponse.success == true{
                    success(registrationResponse)
                }
                else if let failureMessage = registrationResponse.message{
                    failure(failureMessage)
                }
            }else{
                failure(CommonMessages.ApiFailure)
            }  
        }
    }
    
    func getShopCategoryData(businessCategory: Int, success: @escaping (RegistrationShopCategoryDataMapper) -> (), failure : @escaping (String) -> ()){
        
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
        }
        
        let url = RestURL.sharedInstance.ShopCategory + RestURL.sharedInstance.getBusinessCategory(businessCategory: businessCategory)
        print(url)
        let headers = RestURL.sharedInstance.getCommonHeader()
        
        Alamofire.request(url, headers: headers)
            .responseObject { (response: DataResponse<RegistrationShopCategoryDataMapper>) in
                if let shopCategoryResponse = response.result.value{
                    if let response = shopCategoryResponse.categories, response.count > 0 {
                        success(shopCategoryResponse)
                    }else if let failureMessage = shopCategoryResponse.message{
                        failure(failureMessage)
                    }
                }else {
                    failure(CommonMessages.ApiFailure)
                }
        }
    }
    
}
