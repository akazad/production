//
//  HomePresenter.swift
//  Ponno
//
//  Created by a k azad on 31/7/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol HomeViewDelegate : NSObjectProtocol {
    func setDummyData(data: AddDataMapper)
    func onFailed(data: String)
    func showLoading()
    func hideLoading()
}
class HomePresenter: NSObject {
    
    private let service : HomeService
    weak private var viewDelegate : HomeViewDelegate?
    
    init(service : HomeService) {
        self.service = service
    }
    
    func attachView(viewDelegate : HomeViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getDummyDataFromServer(){
        self.viewDelegate?.showLoading()
        self.service.getDummyData(success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setDummyData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
}
