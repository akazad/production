//
//  FreemiumProduct.swift
//  Ponno
//
//  Created by a k azad on 8/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class FreemiumProduct : Mappable {
    var freemiumProduct : String = ""

    required init(map: Map) {

    }

    func mapping(map: Map) {

        freemiumProduct <- map["FreemiumProduct"]
    }

}
