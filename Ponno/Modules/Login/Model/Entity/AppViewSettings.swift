//
//  AppViewSettings.swift
//  Ponno
//
//  Created by a k azad on 7/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class AppViewSettings : Mappable {
    var sideMenu : SideMenu?
    var dueCustomer: FreemiumDueCustomer?
    var shopSettings: FreemiumShopSettings?
    var dashboard: FreemiumDashboard?
    var product: FreemiumProduct?
    var salePanel: FreemiumSalePanel? 

    required init(map: Map) {

    }

    func mapping(map: Map) {

        sideMenu <- map["SideMenu"]
        dueCustomer <- map["DueBook"]
        shopSettings <- map["Shop"]
        dashboard <- map["Home"]
        product <- map["Product"]
        salePanel <- map["SaleBook"]
    }

}

