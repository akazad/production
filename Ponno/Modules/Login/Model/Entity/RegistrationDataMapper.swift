//
//  RegistrationDataMapper.swift
//  Ponno
//
//  Created by a k azad on 9/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class RegistrationDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var errors : RegistrationErrors? 
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        errors <- map["errors"]
    }
    
}

