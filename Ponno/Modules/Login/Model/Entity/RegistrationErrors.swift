//
//  RegistrationErrors.swift
//  Ponno
//
//  Created by a k azad on 9/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class RegistrationErrors : Mappable {
    var name : [String]?
    var phone : [String]?
    var password : [String]?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
        phone <- map["phone"]
        password <- map["password"]
    }
    
}
