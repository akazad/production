//
//  VersionCheckDataMapper.swift
//  Ponno
//
//  Created by a k azad on 11/7/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class VersionCheckDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var latestVersion : String?
    var forceUpdate : Bool?
    var recommendedUpdate : Bool?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        latestVersion <- map["latest_version"]
        forceUpdate <- map["force_update"]
        recommendedUpdate <- map["recommended_update"]
    }
    
}
