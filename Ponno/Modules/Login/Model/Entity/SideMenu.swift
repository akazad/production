//
//  SideMenu.swift
//  Ponno
//
//  Created by a k azad on 7/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class SideMenu : Mappable {
    var navigationItem : String = ""

    required init(map: Map) {

    }

    func mapping(map: Map) {

        navigationItem <- map["NavigationItem"]
    }

}

