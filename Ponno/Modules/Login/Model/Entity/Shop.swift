//
//  Shop.swift
//  Ponno
//
//  Created by a k azad on 19/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import ObjectMapper

class Shop: Mappable {
    var name : String = ""
    var registrationNumber : String = ""
    var address : String = ""
    var image : String = ""
    var deliverySystem : Int = 0
    var paymentStatus : Int = 0
    var invoiceNote : String = ""
    var accStatus : Int = 0
    var inventorySystem : Int = 0
    var category : Int = 0
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        name  <- map["name"]
        registrationNumber <- map["reg_no"]
        address <- map["address"]
        image <- map["image"]
        deliverySystem <- map["delivery_system"]
        paymentStatus <- map["payment_status"]
        invoiceNote <- map["invoice_note"]
        accStatus <- map["acc_status"]
        inventorySystem <- map["inventory_system"]
        category <- map["category"]
    }
}
