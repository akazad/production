//
//  FreemiumSalePanel.swift
//  Ponno
//
//  Created by a k azad on 24/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class FreemiumSalePanel : Mappable {
    var freemiumSalePanel : String = ""

    required init(map: Map) {

    }

    func mapping(map: Map) {

        freemiumSalePanel <- map["SalePanel"]
    }

}
