//
//  FreemiumDueCustomer.swift
//  Ponno
//
//  Created by a k azad on 7/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class FreemiumDueCustomer : Mappable {
    var dueCustomer : String = ""

    required init(map: Map) {

    }

    func mapping(map: Map) {

        dueCustomer <- map["DueCustomer"]
    }

}

