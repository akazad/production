//
//  FreemiumShopSettings.swift
//  Ponno
//
//  Created by a k azad on 8/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class FreemiumShopSettings : Mappable {
    var settings : String = ""

    required init(map: Map) {

    }

    func mapping(map: Map) {

        settings <- map["Settings"]
    }

}
