//
//  RegistrationShopCategoryDataMapper.swift
//  Ponno
//
//  Created by a k azad on 30/7/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class RegistrationShopCategoryDataMapper : Mappable {
    var success : Bool?
    var message : String?
    var categories : [RegistrationShopCategories]?
    
    required init(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        success <- map["success"]
        message <- map["message"]
        categories <- map["categories"]
    }
    
}
