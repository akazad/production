//
//  FreemiumDashboard.swift
//  Ponno
//
//  Created by a k azad on 8/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import ObjectMapper

class FreemiumDashboard : Mappable {
    var dashboard : String = ""

    required init(map: Map) {

    }

    func mapping(map: Map) {

        dashboard <- map["Dashboard"]
    }

}

