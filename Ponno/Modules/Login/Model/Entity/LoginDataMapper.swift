//
//  LoginDataMapper.swift
//  Ponno
//
//  Created by a k azad on 19/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import ObjectMapper

class LoginDataMapper: Mappable {
    var success: Bool?
    var message : String?
    var token : String?
    var user : User?
    var shop : Shop?
    var btnDummyData : Int = -1
    var permissionList : [PermissionList]?
    var assignedPermission : [Int]?
    var appViewSettings : AppViewSettings?
    var businessCategory : Int?
    
    required init(map: Map){
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        message  <- map["message"]
        token <- map["token"]
        user <- map["user"]
        shop <- map["shop"]
        btnDummyData <- map["btn_dummy_data"]
        permissionList <- map["permission_list"]
        assignedPermission <- map["assigned_permission"]
        appViewSettings <- map["app_view_settings"]
        businessCategory <- map["business_category"]
    }
}
