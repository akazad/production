//
//  PermissionList.swift
//  Ponno
//
//  Created by a k azad on 11/7/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import Foundation
import ObjectMapper

class PermissionList : Mappable , Codable {
    var id : Int = 0
    var name : String = ""
    
    init(id : Int, name : String) {
        self.id = id
        self.name = name
    }
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
    }
    
}
