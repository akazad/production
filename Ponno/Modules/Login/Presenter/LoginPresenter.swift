//
//  LoginPresenter.swift
//  Ponno
//
//  Created by a k azad on 19/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

protocol LoginViewDelegate : NSObjectProtocol {
    func setLoginData(loginData : LoginDataMapper)
    func setOfflineProduct(data: OfflineProductDataMapper)
    func onLoginFailed(data: String)
    func onDataSyncingFailed(data: String)
    func showLoading()
    func hideLoading()
}
class LoginPresenter: NSObject {
    
    private let service : LoginService
    weak private var viewDelegate : LoginViewDelegate?
    
    init(service : LoginService) {
        self.service = service
    }
    
    func attachView(viewDelegate : LoginViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func getLoginDataFromServer(userPhoneNumber : String, userPassword : String){
        self.viewDelegate?.showLoading()
        self.service.postLoginData(userPhoneNumber: userPhoneNumber , userPassword: userPassword, success: { (loginData) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setLoginData(loginData : loginData)
        }, failure: {(message) in
            self.viewDelegate?.onLoginFailed(data: message)
            self.viewDelegate?.hideLoading()
        })
    }
    
    func getOfflineProductFromServer(timeStamp: String){
        self.viewDelegate?.showLoading()
        let service = ProductService()
        service.getOfflineProductData(timesStamp: timeStamp, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setOfflineProduct(data: data)
        }, failure: {message in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onDataSyncingFailed(data: message)
        })
    }
}
