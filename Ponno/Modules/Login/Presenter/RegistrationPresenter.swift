//
//  RegistrationPresenter.swift
//  Ponno
//
//  Created by a k azad on 9/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

protocol RegistrationViewDelegate : NSObjectProtocol {
    func onSuccess(data : RegistrationDataMapper)
    func onFailed(data: String)
    func setLoginData(loginData : LoginDataMapper)
    func setShopCategoryData(data: RegistrationShopCategoryDataMapper)
    func onShopCategoryFailed(data: String)
    func showLoading()
    func hideLoading()
}
class RegistrationPresenter: NSObject {
    
    private let service : LoginService
    weak private var viewDelegate : RegistrationViewDelegate?
    
    init(service : LoginService) {
        self.service = service
    }
    
    func attachView(viewDelegate : RegistrationViewDelegate){
        self.viewDelegate = viewDelegate
    }
    
    func postRegistrationDataToServer(data: [String: Any]){
        self.viewDelegate?.showLoading()
        self.service.postRegistrationData(data: data, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onSuccess(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onFailed(data: message)
        })
    }
    
    func getLoginDataFromServer(userPhoneNumber : String, userPassword : String){
        self.viewDelegate?.showLoading()
        LoginService().postLoginData(userPhoneNumber: userPhoneNumber , userPassword: userPassword, success: { (loginData) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setLoginData(loginData : loginData)
        }, failure: {(message) in
            self.viewDelegate?.onFailed(data: message)
            self.viewDelegate?.hideLoading()
        })
    }
    
    func getShopCategoryDataFromServer(businessCategory: Int){
        self.viewDelegate?.showLoading()
        self.service.getShopCategoryData(businessCategory: businessCategory, success: { (data) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.setShopCategoryData(data: data)
        }, failure: { (message) in
            self.viewDelegate?.hideLoading()
            self.viewDelegate?.onShopCategoryFailed(data: message)
        })
    }
    
}
