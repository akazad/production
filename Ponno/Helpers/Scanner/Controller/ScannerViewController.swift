//
//  ScannerViewController.swift
//  Ponno
//
//  Created by a k azad on 1/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

enum Navigate: String{
    case sale = "Sale"
    case saleAddSerial = "SaleSerial"
    case purchase = "Purchase"
    case productCode = "ProductCode"
    case productSerial = "ProductSerial"
}

class ScannerViewController: UIViewController {
    
    @IBOutlet weak var scannerView: ScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }
    @IBOutlet weak var scanButton: UIButton! {
        didSet {
            scanButton.setTitle("STOP", for: .normal)
        }
    }
    
    let presenter = ProductSearchListPresenter(service: ProductService())
    
    //var selectedProduct : ProductList?
    //var sellingPrice : Double?
    var isLoading : Bool = false
    var currentPage : Int = 1
    var delegate : SalesAddProductDelegate?
    var productDelegate : SelectedProduct?
    //var scanStartFromDelegate : ScaningFrom?
    var codeSearchProduct : ProductList?
    {
        didSet{
            if let data = codeSearchProduct, isObjectNotNil(object: data){
                let viewController = AddNewSaleViewController()
                viewController.selectedProduct = data
                
                let quantity = data.quantity
                if quantity.isEmpty == false && quantity > "0.00"{
                    if data.hasSerial == 0{
                        //self.navigateToSaleAddVC()
                    }
                    else{
                        self.navigateToSaleAddWithSerialVC(data: data)
                    }
                }else{
                    self.showAlert(title: LanguageManager.InsufficientQuantity, message: "")
                }
            }
        }
    }
    
    var searchCode : String?{
        didSet{
            //self.currentPage = 1
            //self.codeScannerActive = true
            //self.productList = []
            self.isLoading = true
            //self.isSearchActive = true
            if let serial = searchCode{
                self.presenter.getProductSearchDataFromServer(page: 1, searchString: serial)
            }
        }
    }
    
    var navigateTo : Navigate?
    var scanningCode: String?
    
    var qrData: ScanData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpInitialLanguage()
        //addSlideMenuButton()
    }
    
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if navigateTo?.rawValue == Navigate.sale.rawValue{
            self.attachPresenter()
        }

        if !scannerView.isRunning {
            scannerView.startScanning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }

    @IBAction func scanButtonAction(_ sender: UIButton) {
        scannerView.isRunning ? scannerView.stopScanning() : scannerView.startScanning()
        let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
        sender.setTitle(buttonTitle, for: .normal)
    }
}


extension ScannerViewController: QRScannerViewDelegate {
    func qrScanningDidStop() {
        let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
        scanButton.setTitle(buttonTitle, for: .normal)
    }
    
    func qrScanningDidFail() {
        //presentAlert(withTitle: "Error", message: "Scanning Failed. Please try again")
        self.showAlert(title: LanguageManager.Warning, message: "Scanning Failed. Please try again")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        if navigateTo?.rawValue == Navigate.sale.rawValue{
            if let code = str{
                self.searchCode = code
            }
        }else{
            self.qrData = ScanData(codeString: str)
            self.scanningCode = str
            if let controller = self.navigateTo{
                self.navigateTo(to: controller.rawValue)
            }
        }
        
        
        
//        if let data = qrData{
//            self.scanningCode = data.codeString
//            if let controller = self.navigateTo{
//                self.navigationController?.popViewController(animated: true)
//                self.navigateTo(to: controller.rawValue)
//            }
//        }

    }
    
}


extension ScannerViewController {
    func navigateTo(to: String){
        switch to {
        case Navigate.sale.rawValue:
            //navigateToAddSaleVC()
            popToAddNewSaleVC()
        case Navigate.purchase.rawValue:
            popToAddNewSaleVC()
        case Navigate.productCode.rawValue:
            popToAddNewSaleVC()
        case Navigate.productSerial.rawValue:
            popToProductAddThirdVC()
        case Navigate.saleAddSerial.rawValue:
            navigateToSaleAddSerialVC()
        default:
            break
        }
    }
    
//    func navigateToSaleAddVC(){
//
//    }
    
    func navigateToAddSaleVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewSaleViewController") as! AddNewSaleViewController
//        if let text = self.scanningCode{
//            viewController.searchText = text
//            viewController.codeScannerActive = true
//        }
        //viewController.modalPresentationStyle = .fullScreen
        //self.navigationController?.popViewController(animated: true)
        //self.navigationController?.pushViewController(viewController, animated: true)
        self.present(viewController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "detailSeuge", let viewController = segue.destination as? DetailViewController {
//            viewController.qrData = self.qrData
//        }searchSerial
    }
    
    func navigateToSaleAddSerialVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Sales", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SaleAddSerialViewController") as! SaleAddSerialViewController
        
        viewController.isCodeScannerActive = true
        if let code = self.scanningCode{
            viewController.searchSerial = code
        }
        viewController.scaningStart = "SaleAddSerial"
//        let startFrom = ScanningStratFrom()
//        viewController.scaningStart = startFrom.SaleAddSerial
        //viewController.setStartFrom(from: "SaleAddSerial")
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func popToProductAddThirdVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "ProductAddThirdVC") as! ProductAddThirdVC
        viewController.scanningCode = self.scanningCode
        self.navigationController?.popViewController(animated: true)
    }
    func popToAddNewProductVC(){
        let storyBoard:UIStoryboard = UIStoryboard(name: "Product", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "AddNewProductViewController") as! AddNewProductViewController
        viewController.scanningCode = self.scanningCode 
        self.navigationController?.popViewController(animated: true)
    }
    
    func popToAddNewSaleVC(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is AddNewSaleViewController {
                self.navigationController!.pushViewController(aViewController, animated: true)
            }
        }
    }
    
    
}

extension ScannerViewController{
    func isObjectNotNil(object:AnyObject!) -> Bool
    {
        if let _:AnyObject = object
        {
            return true
        }

        return false
    }
}

extension ScannerViewController{
//    func navigateToSaleAddVC(){
//            let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSecondVC") as! SaleAddSecondVC
//
//        viewController.delegate = self.delegate
//            viewController.saleAblePrice = self.sellingPrice
//            //viewController.selectedProductName = self.selectedProductName
//            //viewController.quantiy = self.selectedAmounts
//    //        viewController.saleAbleQuantity =
//
//            self.navigationController?.pushViewController(viewController, animated: true)
//        }

    func navigateToSaleAddWithSerialVC(data : ProductList){
        let serial = self.prepareSerialNumbers(serials: data.serials)
        
        let viewController = UIStoryboard(name: "Sales", bundle: nil).instantiateViewController(withIdentifier: "SaleAddSerialViewController") as! SaleAddSerialViewController
        
        viewController.serialList = serial
        viewController.saleAblePrice = data.sellingPrice.toDouble()
        viewController.delegate = self.delegate
        viewController.selectedProductDelegate = self.productDelegate
        
        viewController.selectedProductName = data.name
        viewController.isCodeScannerActive = true
        viewController.product = data
        let startFrom = ScanningStratFrom()
        viewController.scaningStart = startFrom.AddNewSale
        //viewController.setStartFrom(from: "FromAddSale")
        viewController.searchSerial = self.searchCode
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension ScannerViewController : ProductSearchListViewDelegate{
    func setProductSearchData(data: ProductListDataMapper) {
        guard let product = data.productList, product.count > 0 else{
            return
        }
        self.codeSearchProduct = product[0]
    }
    
    func onFailed(failure: String) {
        showAlert(title: LanguageManager.NoInformationFound, message: "")
    }
    
    func showLoading() {
        self.showLoader()
    }
    
    func hideLoading() {
        self.hideLoader()
    }
    
    func attachPresenter(){
        self.presenter.attachView(viewDelegate: self)
    }
}


