//
//  EnglishLanguageSupport.swift
//  Ponno
//
//  Created by a k azad on 31/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//


import Foundation

public struct EnglishLang {
    
    //Common
    static var warning = "Warning"
    static var congratulations = "Congratulations!"
    static var areYouSure = "Are you sure?"
    static var cancel = "Cancel"
    static var successful = "Successful"
    static var noInformationFound = "No Information found"
    static var noCategoryFound = "No Category found"
    static var delete = "Delete"
    static var yes = "Yes"
    static var no = "No"
    static var productSearch = "Product Search"
    static var searchFilter = "Search Filter"
    
    //Module -> Login
    static var login = "Login"
    static var logout = "Logout"
    static var register = "Register now"
    static var phoneNumberIsIncorrect = "Phone number is incorrect"
    static var phoneNumberIsRequired = "Phone number is required"
    static var passwordIsRequired = "Password is required"
    static var repeatPasswordIsRequired = "Confirm password is required"
    static var registration = "Registration"
    static var nameIsRequired = "Name is required"
    static var passwordDidNotMatch = "Password did not match, Try again"
    static var mobileNumber = "Mobile number"
    static var password = "Password"
    static var repeatPassword = "Confirm password"
    static var sixToTwenty = "(6-20 characters)"
    static var dontHaveAnAccount = "Don't Have An Account"
    static var alreadyHaveAnAccount = "Already Have An Account"
    static var name = "Name"
    static var phone = "Phone"
    static var shopCategory = "Shop category"
    
    //Home
    static var dashboard = "Dashboard"
    static var existingDataWillBeRemoved = "Existing data will be removed"
    static var sale = "Sale"
    static var purchase = "Purchase"
    static var expense = "Expense"
    static var newInventory = "New Inventory"
    static var newDue = "New Due"
    static var newPayable = "New Payable"
    static var newEmployee = "New Employee"
    static var newCustomer = "New Customer"
    static var newDelivery = "New Delivery"
    static var newVendor = "New Vendor"
    static var dummyDataButton = "Request Dummy Data"
    
    //SideMenu
    static var todaysSummary = "Today's Summary"
    static var saleBook = "Sale Book"
    static var deliveryBook = "Delivery Book"
    static var dueBook = "Due Book"
    static var customerBook = "Customer Book"
    static var inventory = "Inventory"
    static var expenseBook = "Expense Book"
    static var employeeBook = "Employee Book"
    static var purchaseBook = "Purchase Book"
    static var rawMaterial = "Raw Material Book"
    static var preOrder = "Pre-Order"
    static var payableBook = "Payable Book"
    static var vendorBook = "Vendor Book"
    static var accounts = "Accounts"
    static var accountsBook = "Accounts Book"
    static var contact = "Contact"
    
    //Mark: TodaysSummary
    static var todaysSale = "TodaysSale"
    static var otherCalculations = "Other Calculations"
    
    //Sale
    //SalesListViewController
    static var selectStartDate = "Select Start Date"
    static var startingDateIsRequired = "Starting date is required"
    static var endDateIsRequired = "End date is required"
    static var selectEndDate = "Select End Date"
    static var selectDate = "Select Date"
    static var startDate = "Start Date"
    static var endDate = "End Date"
    static var totalDue = "Total Due"
    static var totalSale = "Total Sale"
    static var totalSaleAmount = "Total Sale Amount"
    //SalePerDayListViewController
    static var total = "Total"
    static var due = "Due"
    static var deliverySystem = "Delivery System"
    static var paid = "Paid"
    static var unpaid = "Unpaid"
    //SaleDetailsViewController
    static var salesInvoice = "Sales Invoice"
    static var invoice = "Invoice"
    static var invoiceReceive = "Invoice Receive"
    static var date = "Date"
    static var deliveryDate = "Delivery Date"
    static var time = "Time"
    static var discount = "Discount"
    static var paymentMethod = "Payment Method"
    static var paymentNumber = "Payment Number"
    static var paidAmount = "Paid Amount"
    static var deliveryCharge = "Delivery Charge"
    static var customerName = "Customer name"
    static var address = "Address"
    static var quantity = "Quantity"
    static var pricePerUnit = "Price/Unit"
    static var deliveryStatus = "Delivery Status"
    static var delivered = "Delivered"
    static var deliverySystemDue = "Delivery System Due"
    static var invoiceDue = "Invoice Due"
    static var latestDue = "Latest Due"
    static var duePaid = "Due Paid"
    static var remainingDue = "Remaining Due"
    static var othersDue = "Others Due"
    static var othersDuePaid = "Others Due Paid"
    static var data = "Data"
    static var productReturn = "Product Return"
    static var retturn = "Return"
    //AddNewSaleViewController
    static var theProductIsAlreadySelectedDeselectTheProductForUpdate = "The Product is already selected! Deselect the product for update"
    static var insufficientQuantity = "Insufficient quantity"
    static var saveAsDraft = "Save as Draft"
    static var updateDraft = "Update Draft"
    static var continuee = "Continue"
    static var complete = "Complete"
    static var youCannotSaleThisProduct = "You cannot sale this product"
    static var discard = "Discard"
    static var next = "Next"
    static var submit = "Submit"
    static var refresh = "Refresh"
    static var draft = "Draft"
    //SaleAddSecondViewController
    static var one = "1"
    static var quantityIsRequired = "Quantity is required"
    static var sellingPriceIsRequired = "Selling price is required"
    //SaleAddSerialViewController
    static var productSerialIsRequired = "Product Serial is required"
    //SaleInfoViewController
    static var amount = "Amount"
    static var customer = "Customer"
    static var payableAmount = "Payable Amount"
    static var cash = "Cash"
    static var cashBack = "Cash Back"
    static var bkashPaymentPhoneNumberIsRequired = "Bkash payment phone number is required"
    static var rocketPaymentPhoneNumberIsRequired = "Rocket payment phone number is required"
    static var accountNumberIsRequired = "Account Number is Required"
    static var cashIsRequired = "Cash is required"
    static var customerIsRequired = "Customer is required"
    static var bkashMobileNumber = "Bkash mobile number"
    static var rocketMobileNumber = "Rocket mobile number"
    static var accountNumber = "Account Number"
    static var account = "Account"
    static var delivery = "Delivery"
    static var youCanNotUpdateMoreThan = "You can not update more than"
    static var youCanNotPayMoreThan = "You can not pay more than"
    static var walletPayExceedsPayableAmount = "Wallet pay exceeds payable amount!"
    static var youCanNotUpdateLessThan = "You can not update less than"
    static var walletAmount = "Wallet Amount"
    static var walletPay = "Wallet Pay"
    //SaleProductReturnViewController
    static var searchSaleDate = "Search Sale Date"
    //InvoiceReturnViewController
    static var productName = "Product Name"
    static var returnQuantity = "Return Quantity"
    static var productIsRequired = "Product is required"
    static var youCantReturnMoreThan = "You can't retrun more than"
    static var cashBackIsRequired = "Cash back is required"
    static var returnRelatedInfo = "Return related info"
    //SaleOnDeliveryViewController
    static var deliverySystemAdd = "Delivery System Add"
    static var deliverySystemIsRequired = "Delivery System is required"
    static var deliveryChargeIsRequired = "Delivery Charge is required"
    static var newDeliverySystemAdd = "New Delivery System Add"
    static var phoneNumber = "Phone Number"
    //SaleWithDiscountViewController
    static var discountIsRequired = "Discount is required"
    //SaleWithCustomerViewController
    static var customerAdd = "Customer Add"
    static var customerPhone = "Customer Phone"
    //SalesCustomerSearchViewController
    static var searchCustomer = "Search Customer"
    //SalePerDaySearchViewController
    static var searchSerial = "Search Serial"
    //SaleListPopOverVC
    static var saleDraft = "Sale Draft"
    static var salePreOrder = "Sale PreOrder"
    
    //Product
    //ProductListViewController
    static var damagedProduct = "Damaged Product"
    static var varient = "Variant"
    static var category = "Category"
    static var categoryAndSubCategory = "Category & Sub-Category"
    static var sellingPrice = "Selling Price"
    //ProductDetailsBaseViewController
    static var productDetails = "Product Details"
    static var company = "Company"
    static var productCode = "Product Code"
    static var purchasePrice = "Purchase Price"
    static var quantityAlert = "Quantity Alert"
    //ProductDViewController
    static var recentStock = "Recent Stock"
    static var recentSale = "Recent Sale"
    static var serialNo = "Serial No"
    static var stockAvailable = "Stock available"
    static var vendorName = "Vendor Name"
    static var warranty = "Warranty"
    static var expireDate = "Expire Date"
    static var unSold = "Unsold"
    static var sold = "Sold"
    static var refund = "Refund"
    static var update = "Update"
    static var theStockIsEmpty = "The Stock is empty"
    static var serialUpdate = "Serial Update"
    //ProductCategoryViewController
    static var selectCategory = "Select Category"
    static var newCategoryAdd = "New Category Add"
    static var newCategory = "New Category"
    static var categoryName = "Category Name"
    static var categoryIsRequired = "Category name is required"
    static var categoryAlreadyExists = "Category is already exists"
    //ProductSubCategoryVC
    static var newSubCategoryAdd = "New Sub Category Add"
    static var subCategoryName = "Sub Category Name"
    static var subCategory = "Sub-Category"
    static var subCategoryNameIsRequired = "Sub Category name is required"
    static var subCategoryIsAlreadyExists = "Sub Category is already exists"
    //AddNewProductVC
    static var unit = "Unit"
    static var newProduct = "New Product"
    static var pc = "pc"
    static var productNameIsRequired = "Product name is required"
    static var productCategoryIsRequired = "Product category is required"
    static var productUnitIsRequired = "Product unit is required"
    //ProductAddThirdVC
    static var serialNumber = "Serial Number"
    static var quantityAlertWhenSmallerThan = "Quantity alert! when smaller than,"
    static var useCommaToSeparateSerial = "Use comma(,) to separate serial"
    static var newVendorAdd = "New Vendor Add"
    static var vendorMobileNumber = "Vendor Mobile Number"
    static var vendorNameIsRequired = "Vendor name is required"
    static var vendorMobileNumberIsRequired = "Vendor mobile number is required"
    static var month = "Month"
    static var buyingPrice = "Buying Price"
    static var selectOne = "Select One..."
    static var vendor = "Vendor"
    //RecentStockReturnViewController
    static var productInfo = "Product Info"
    static var selectSerialNumber = "Select Serial Number"
    static var selectAVendorToReturnProduct = "Select a vendor to return product"
    static var buyingPriceIsRequired = "Buying Price is required"
    //RecentStockUpdateViewController
    static var stockUpdate = "Stock Update"
    //DamagedProductListVC
    static var damagedProducts = "Damaged Products"
    static var damaged = "Damaged"
    static var times = "Times"
    static var totalDamages = "Total Damages"
    static var totalDamageAmount = "Total Damage Amount"
    //DamagedProductSearchVC
    static var damagedProductSearch = "Damaged Product Search"
    //DamagedPerProductVC
    static var damagedQuantity = "Damage Quantity"
    static var description = "Description"
    static var close = "Close"
    static var yesDelete = "Yes! Delete"
    //DamageableProductList
    static var toDamageSelectProduct = "To Damage, select product"
    //DamageableProductBaseInfoVC
    static var quantityCantBeGreaterThan = "Quantity can't be greater than"
    static var buyingpriceCantBeGreaterThan = "Buying price can't be greater than"
    static var returnPriceIsRequired = "Return Price is required"
    //ModuleProductCategory
    static var treeView = "Tree View"
    static var categoryUpdate = "Category Update"
    static var parentCategory = "Parent Category"
    
    
    //Module -> Raw Material
    //RawMaterialListVC
    static var production = "Production"
    static var newRawMaterial = "New Raw Material"
    static var damagedRawMaterial = "Damaged Raw Material"
    //RawMaterialSearchVC
    static var rawMaterialSearch = "Raw Material Search"
    //RawMaterialDetailsBaseVC
    static var rawMaterialInfo = "Raw Material Info"
    static var materialSku = "Material SKU"
    static var currentStock = "Current Stock"
    static var currentStockAmount = "Current StockAmount"
    //RawMaterialDetailsVC
    static var recentMaterialStock = "Recent Material Stock"
    static var recentUse = "Recent Use"
    static var available = "Available"
    static var stockAmount = "Stock Amount"
    static var usedQuantity = "Used Quanity"
    //RawMaterialRecentStockReturnVC
    static var rawMaterialName = "Raw Material Name"
    static var pricecannotBe0OrLess = "Price can't be 0 or less "
    //ProductionRawMaterialListVC
    static var selectRawMaterial = "Select Raw Material"
    static var selectARawMaterialForProduction = "Select a raw material for production"
    static var theRawMaterialIsAlreadySelectedDeselectItThenUpdate = "The Raw Material is already selected! Deselect it then update"
    //ProductionRawMaterialAccessoryInfoVC
    static var product = "Product"
    static var selectProduct = "Select Product"
    static var productionCost = "Production Cost"
    static var totalExpense = "Total Expense"
    //PurchaseableRawMaterialListVC
    static var selectARawMaterialToPurchase = "Select a Raw Material to purchase"
    //PurchaseReturnRawMaterialListVC
    static var selectARawMaterialToReturn = "Select a Raw Material to return"
    //PurchaseReturnRawMaterialBaseInfoVC
    static var rawMaterialReturn = "Raw Material Return"
    static var returnPrice = "Return Price"
    static var cashBackPriceIsRequired = "Cashback price is required"
    static var cashbackPriceCantBeGreaterThan = "Cashback price can't be greater than"
    //PurchaseReturnRawMaterialAccessoryInfoVC
    static var totalPrice = "Total Price"
    //RawMaterialPurchaseListVC
    static var rawMaterialPurchaseBook = "Raw Material Purchase Book"
    //RawMaterialPurchaseDetailsVC
    static var materialPurchaseInvoice = "Material Purchase Invoice"
    static var invoicePayable = "Invoice Payable"
    static var latestPayable = "Latest Payable"
    static var payablePaid = "Payable Paid"
    static var remainingPayable = "Remaining Payable"
    static var othersPayable = "Others Payable"
    //DamagedRawMaterialSearchVC
    static var damagedRawMaterialSearch = "Damaged Raw Material Search"
    //DamageableRawMaterialListVC
    static var selectRawMaterialToDamage = "Select Raw Material to damage"
    //RawMaterialUpdateViewController
    static var rawMaterialUnitRequired = "Raw Material Unit is required"
    //AddRawMaterialBaseInfoVC
    static var rawMaterialCode = "Raw Material Code"
    static var rawMaterialNameIsRequired = "Raw Material name is required"
    //PurchaseableRawMaterialAccessoryInfoVC
    static var purchaseDate = "Purchase Date"
    static var saleDate = "Sale Date"
    
    //Module -> Purchase
    //PurchaseDetailsVC
    static var purchaseInvoice = "Purchase Invoice"
    //PurchaseReturnDetailsVC
    static var returnInvoice = "Return Invoice"
    //PurchaseableProductListVC
    static var selectAProductToPurchase = "Select a product to purchase"
    //PurchaseAddVC
    static var newPurchase = "New Purchase"
    static var purchaseRelatedInfo = "Purchase Related Info"
    //RefundableProductListVC
    static var toReturnSelectAProduct = "To Return, Select a product"
    //RefundAddSerialVC
    static var serialNumberIsRequired = "Serial number is required"
    //PreOrderListVC
    static var received = "Received"
    static var orderDate = "Order date"
    static var receiveDate = "Receive date"
    static var advanceAmount = "Advance Amount"
    static var amountToBePaid = "Amount to be paid"
    static var noRemarks = "No remarks"
    //PreOrderableProductVC
    static var toOrderSelectAProduct = "To order select a product"
    //PreOrderAddVC
    static var newPreOrder = "New Pre-Order"
    static var addMoreProduct = "Add More Product"
    //PreOrderAddInfoVC
    static var remarks = "Remarks"
    static var selectReceiveDate = "Select Receive Date"
    //PreOrderPendingVC
    static var receive = "Receive"
    static var changeAdvance = "Change Advance"
    static var advanceAmountIsRequired = "Advance amount is required"
    static var advanceAmountsNeedToBeModified = "Advance amount need to be modified"
    static var paidAmountIsRequired = "Paid amount is required"
    //PerOrderEditProductList
    static var selectProductsForPreOrder = "Select products for Pre-Order"
    
    //Module -> Expense
    //ExpenseViewController
    static var totalExpenseAmount = "Total Expense Amount"
    //AddNewExpenseVC
    static var employee = "Employee"
    static var expenseAmount = "Expense Amount"
    static var updateInfo = "Update Info"
    static var newExpense = "New Expense"
    static var expenseTypeIsRequired = "Expense type is required"
    static var expenseType = "Expense Type"
    static var newExpenseAdd = "New Expense Add"
    static var expenseDate = "Expense Date"
    static var selectAnItemToSearch = "Select an item to search"
    
    //Module -> Due
    //DueViewController
    static var dueCustomer = "Due Customer"
    //AddNewDueVC
    static var dueUpdate = "Due Update"
    static var withdrawUpdate = "Withdraw Update"
    static var withdrawnUpdate = "Withdrawn Amount"
    static var collectionAmount = "Collection Amount"
    
    
    //Module -> Delivery
    //DeliveryViewController
    static var deliveryLog = "Delivery Log"
    static var deliveryHistory = "Delivery History"
    static var withdraw = "Withdraw"
    static var deliveryNameIsRequired = "Delivery name is required"
    //AddNewDeliveryDueVC
    static var dueAmount = "Due Amount"
    static var dueWithdraw = "Due Withdraw"
    static var dueWithdrawAmount = "Due Withdraw Amount"
    static var withdrawAmountIsTooMuch = "Withdraw amount is too much"
    //DeliverySystemSearchVC
    static var deliverySystemSearch = "Delivery System Search"
    //DeliveryLogSearchVC
    static var from = "From"
    static var to = "To"
    static var search = "Search"
    //AddNewDeliverySystemVC
    static var newDeliverySystemInfo = "New Delivery System Info"
    //DeliveryDuePaidVC
    static var commission = "Commission"
    static var newDueIsRequired = "New Due is required"
    static var commissionUnitIsRequired = "Commission unit is required"
    static var oneHundred = "100"
    //DBaseViewController
    static var deliverySystemProfile = "Delivery System Profile"
    static var totalOrder = "Total Order"
    static var currentOrder = "Current Order"
    static var totalDuePaid = "Total Due Paid"
    //DDetailsViewController
    static var by = "By"
    static var totalPaid = "Total Paid"
    static var dueHistory = "Due History"
    
    //Module -> Customer
    //CustomerSearchVC
    static var customerSearch = "Customer Search"
    //CustomerDBaseVC
    static var customerDetails = "Customer Details"
    static var customerWallet = "Customer Wallet"
    static var dueAdjust = "Due Adjust"
    static var dueAdjustUpdate = "Due Adjust Update"
    static var newDueAdjust = "New Due Adjust"
    static var dueAdjustAmount = "Due Adjust Amount"
    static var adjustedBy = "Adjusted By"
    static var youCanNotAdjustMoreThan = "You can not adjust more than"
    //CustomerAddVC
    static var newCustomerAdd = "New Customer Add"
    static var updateCustomerInfo = "Update Customer Info"
    static var customerDueUpdate = "Customer Due Update"
    static var customerAddressIsRequired = "Customer address is required"
    //CustomerDuePaidVC
    static var duePaymentIsRequired = "Due payment is required"
    static var dueAdjustAmountIsRequired = "Due adjust amount is required"
    
    //Module -> Vendors
    //VendorSearchViewController
    static var vendorSearch = "Vendor Search"
    //VendorDBaseVC
    static var vendorProfile = "Vendor Profile"
    static var totalPurchase = "Total Purchase"
    static var currentPayable = "Current Payable"
    static var totalPayable = "Total Payable"
    static var vendorWallet = "Vendor Wallet"
    static var payableAdjust = "Payable Adjust"
    static var payableAdjustUpdate = "Payable Adjust Update"
    static var newPayableAdjust = "New Payable Adjust"
    static var payableAdjustAmount = "Payable Adjust Amount"
    static var payableAdjustAmountIsRequired = "Payable adjust amount is required"

    //VendorDViewController
    static var payableHistory = "Payable History"
    static var purchaseHistory = "Purchase History"
    static var returnHistory = "Return History"
    static var payable = "Payable"
    static var purchaseBy = "Purchased by"
    static var totalAmount = "Total Amount"
    static var returnedBy = "Returned by"
    //AddNewVendorVC
    static var vendorInfo = "Vendor Info"
    static var vendorInfoUpdate = "Vendor Info Update"
    //VendorPaidVC
    static var payableWithdraw = "Payable Withdraw"
    static var paidAmountIsGreaterThanCurrentPayable = "Paid amount is greater than current payable"
    
    //Module -> Payable Book
    //PayableViewController
    static var vendors = "Vendors"
    //AddNewPayableVC
    static var payableUpdate = "Payable Update"
    static var payableWithdrawUpdate = "Payable Withdraw Update"
    static var payableWithdrawAmount = "Payable Withdraw Amount"
    static var payableWithdrawAmountIsEmpty = "Payable withdraw amount is required"
    static var payableAmountIsRequired = "Payable amount is required"
    static var payableWithdrawUpdateAmountIsTooMuch = "Payable withdraw update amount is too much"
    static var payableWithdrawnAmountIsTooMuch = "Payable withdrawn amount is too much"
    //EditPayableVC
    static var addressIsRequired = "Address is required"
    
    //Module -> EmployeeBook
    //EmployeeDBaseVC
    static var employeeDetails = "Employee Details"
    static var employeeName = "Employee Name"
    static var joiningDate = "Joining Date"
    static var totalInvoice = "Total Invoice"
    static var totalsalaryPaid = "Total Salary Paid"
    static var accountStatus = "Account Status"
    static var paidSalary = "Paid Salary"
    static var unpaidSalary = "Unpaid Salary"
    static var active = "Active"
    static var inactive = "Inactive"
    static var paidSalaryIsTooMuch = "Paid Salary is too much"
    //EmployeeDViewController
    static var saleHistory = "Sale History"
    static var salaryHistory = "Salary History"
    //NewEmployeeAddVC
    static var employeePassword = "Employee Password"
    static var employeeMobileNo = "Employee Mobile No"
    static var employeeInfoUpdate = "Employee Info Update"
    static var employeeNameIsRequired = "Employee name is required"
    static var permissionIsRequired = "PermissionIsRequired"
    static var permission = "Permission"
    //EmployeeSearchVC
    static var employeeSearch = "Employee Search"
    //EmployeeDBasePopOverVC
    static var salary = "Salary"
    static var payment = "Payment"
    //EmployeeSalaryAdd
    static var status = "Status"
    static var salaryUpdate = "Salary Update"
    static var unpaidSalaryAmount = "Unpaid Salary Amount"
    
    //Module -> Accounts
    //Asset
    //AssetPropertyListVC
    static var assets = "Assets"
    static var saleableStockAmount = "Saleable StockAmount"
    static var properties = "Properties"
    static var equipments = "Equipments"
    static var goodWill = "Goodwill"
    static var addNewProperty = "Add New Property"
    static var addNewEquipments = "Add New Equipments"
    static var goodWillAmount = "Goodwill amount"
    static var goodwillAmountIsRequired = "Goodwill amount is required"
    //PropertyNequipmentsAddVC
    static var amountIsRequired = "Amount is required"
    static var newCategoryNameIsRequired = "New Category name is required"
    //Gross&NetProfitVC
    static var grossProfit = "Gross Profit"
    static var netProfit = "Net Profit"
    //AccountsViewController
    static var cashDrawer = "Cash Drawer"
    static var loan = "Loan"
    static var invest = "Invest"
    static var grossAndNetProfit = "Gross & NetProfit"
    static var cashFlowReport = "CashFlow Report"
    static var balanceSheet = "Balance Sheet"
    static var commingSoon = "Comming Soon"
    static var workingOnProgress = "Working on progress"
    static var sorryForTheInterupt = "Sorry for the interupt"
    //CashFlowReportVC
    static var sales = "Sales"
    static var receivable = "Receivable"
    static var collectionOfLoan = "Collection of Loan"
    static var collectionOfInvest = "Collection of Invest"
    static var totalCashReceive = "Total Cash Receive"
    static var costOfSale = "Cost of Sale"
    static var deliveryCommission = "Delivery Commission"
    static var loanPaid = "Loan Paid"
    static var payBack = "Pay Back"
    static var totalCashPaid = "Total Cash Paid"
    static var cashInHand = "Cash In Hand"
    //BalanceSheetVC
    static var searchDateIsRequired = "Search date is required"
    static var currentAssets = "Current Assets"
    static var fixedAssets = "Fixed Assets"
    static var liabilities = "Liabilities"
    static var investment = "Investment"
    static var totalAssets = "Total Assets"
    static var totalLiabilities = "Total Liabilities"
    static var totalInvestment = "Total Investment"
    //CashDrawer
    static var enterCashAmountOfYourDrawer = "Enter cash amount of your drawer"
    static var cashIn = "Cash In"
    static var cashOut = "Cash Out"
    static var cashInToVendorWallet = "Cash in to Vendor Wallet"
    static var cashInToCustomerWallet = "Cash in to Customer Wallet"
    static var youCanNotCashOutMoreThan = "You cannot cash out more than"
    //CashTransactionVC
    static var transactionUpdate = "Transaction Update"
    static var previousCashAmount = "Previous Cash Amount"
    static var newCashAmount = "New Cash Amount"
    static var enterCashAmount = "Enter Cash Amount"
    //LoanerListVC
    static var initialLoan = "Initial Loan"
    static var interestAmount = "Interest Amount"
    static var deadline = "Deadline"
    //LoanerDetailsVC
    static var loanerDetails = "Loaner Details"
    static var totalLoan = "Total Loan"
    //LoanerTransactionHistoryVC
    static var loanTransactions = "Loan Transactions"
    //NewLoanAddVC
    static var newLoan = "New Loan"
    static var loanAmount = "Loan Amount"
    static var loanDeadline = "Loan Deadline"
    static var loanerSelection = "Loaner Selection"
    static var newLoanerAdd = "New Loaner Add"
    static var loanerName = "Loaner Name"
    static var loanerPhone = "Loaner Phone"
    static var loanerAddress = "Loaner Address"
    static var loanerSelectionIsRequired = "Loaner selection is required"
    static var loanerAmountIsRequired = "Loaner amount is required"
    static var interestAmountIsRequired = "Interest amount is required"
    static var loanerNameIsRequired = "Loaner name is required"
    //LoanPaidVC
    static var currentLoan = "Current Loan"
    static var loanWithdrawAmount = "Loan Withdraw Amount"
    static var loanWithdrawAmountIsRequired = "Loan Withdraw amount is required"
    //LoanerUpdateVC
    static var loanerUpdate = "Loaner Update"
    //LoanerTransactionUpdateVC
    static var withdrawUpdatedAmountIsRequired = "Withdraw updated amount is required"
    //LoanerSearchVC
    static var loanerSearch = "Loaner Search"
    //LoanerTransactionLoanUpdateVC
    static var editedLoanAmount = "Edited Loan Amount"
    static var editedInterestAmount = "Edited Interest Amount"
    static var editedLoanAmountIsRequired = "Edited loan amount is required"
    static var editedInterestAmountRequired = "Edited interest amount required"
    static var loanDeadlineIsRequired = "Loan deadline is required"
    //InvestorsListVC
    static var investor = "Investor"
    //InvestorDetailsVC
    static var investorDetails = "Investor Details"
    //InvestmentTransactionHistoryVC
    static var investmentTransactions = "Investment Transactions"
    //NewInvestmentAddVC
    static var newInvestment = "New Investment"
    static var investorSelection = "Investor Selection"
    static var investAmount = "Invest Amount"
    static var newInvestorAdd = "New Investor Add"
    static var investorName = "Investor Name"
    static var investorPhone = "Investor Phone"
    static var investorAddress = "Investor Address"
    static var investorSelectionIsRequired = "Investor selection is required"
    static var investorAmountIsRequired = "Investor amount is required"
    static var investorNameIsRequired = "Investor name is required"
    //InvestmentProfitWithdrawVC
    static var investmentWithdrawAmountIsRequired = "Investment Withdraw amount is required"
    static var investmentWithdrawInfo = "Investment Withdraw Info"
    static var currentInvestment = "Current Investment"
    static var investmentWithdrawAmount = "Investment withdraw amount"
    //InvestmentTransactionUpdateVC
    static var editedInvestedAmount = "Edited Invested Amount"
    static var editedInvestmentAmountIsRequired = "Edited investment amount is required"
    //InvestorSearchVC
    static var investorSearch = "Investor Search"
    //InvestorUpdateVC
    static var investorUpdate = "Investor Update"
    //MobileBankingListVC
    static var mobileBankIsRequired = "Mobile Banking name is required"
    static var mobileBankingName = "Mobile Banking Name"
    static var mobileBank = "Mobile Banking"
    
    //Contact
    static var call = "Call"
    static var ponnoTitle = "Ponno"
    static var ponnoAddress = "House : 53(2nd Floor), Road: 20, Nikunja-2, Dhaka-1229"
    static var ponnoMobile = "Mobile: 01999088672, 01999088654"
    
    //Shop
    //SegmentedVC
    static var shopInformation = "Shop Information"
    static var shopSettings = "Shop Settings"
    static var paymentReferrals = "Payment & Referrals"
    static var promotion = "Promotion"
    //FirstViewController
    static var shopName = "Shop name"
    static var ownerName = "Owner name"
    static var registrationNumber = "Registration number"
    static var shopPhoneNumber = "Shop Phone number"
    static var shopAddress = "Shop address"
    static var shopNameIsRequired = "Shop name is required"
    static var ownerNameIsRequired = "Owner name is required"
    //SecondViewController
    static var purchaseBase = "Purchase Base"
    static var productionBase = "ProductionBase"
    static var languageLbl = "Language"
    static var invoiceNote = "Invoice Note"
    static var englishTitle = "English"
    static var banglaTitle = "Bangla"
    static var clearShopData = "Clear Shop Data"
    static var purchaseBaseOrProductionBaseIsRequired = "Purchase base or Production base is required"
    //static var languageTitle = "Language"
    //ShopInfoUpdateVc
    static var generalInfo = "General Info"
    static var logInInfo = "Log-In Info"
    //OwnerLogInInfoUpdate
    static var loginInformation = "Login Information"
    static var oldPassword = "Old password"
    static var newPassword = "New password"
    static var confirmPassword = "Confirm password"
    static var oldPasswordIsRequired = "Old password is required"
    static var newPasswordIsRequired = "New password is required"
    static var confirmPasswordIsRequired = "Confirm password is required"
    //ThirdViewController
    static var referrals = "Referrals"
    static var referralsHistory = "Referrals History"
    //ReferralInfoViewController
    static var referred = "Referred"
    static var pending = "Pending"
    static var expired = "Expired"
    static var totalIncome = "Total Income"
    static var totalReceived = "Total Received"
    static var currentDue = "Current Due"
    //ReferralDetailsViewController
    static var newReferral = "New Referral"
    //ReferralAddViewController
    static var shopCategoryIsRequired = "Shop category is required"
    
}
