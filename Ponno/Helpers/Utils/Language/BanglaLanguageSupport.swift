//
//  BanglaLanguageSupport.swift
//  Ponno
//
//  Created by a k azad on 31/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//


import Foundation

public struct BanglaLang {
   
//    //Module -> Login
//    static var login = "লগইন"
//    static var logout = "লগআউট"
//    static var register = "রেজিস্ট্রেশন করুন"
//    static var phoneNumberIsIncorrect = "ভুল মোবাইল নাম্বার"
//    static var phoneNumberIsRequired = "মোবাইল নাম্বার আবশ্যক"
//    static var passwordIsRequired = "পাসওয়ার্ড আবশ্যক"
//    static var nameIsRequired = "নাম আবশ্যক"
//    static var repeatPasswordIsRequired = "Confirm password is required"
//    static var registration = "Registration"
//    static var passwordDidNotMatch = "Password did not match, Try again"
//    static var mobileNumber = "Mobile number"
//    static var password = "Password"
//    static var repeatPassword = "Confirm password"
//    static var sixToTwenty = "(6-20 characters)"
//    static var dontHaveAnAccount = "Don't Have An Account"
//    static var alreadyHaveAnAccount = "Already Have An Account"
//    static var name = "নাম"
//    static var phone = "মোবাইল"
//    static var shopCategory = "দোকানের ধরণ"
    
    //Common
    static var warning = "সতর্কতা!"
    static var congratulations = "অভিনন্দন!"
    static var areYouSure = "আপনি কি নিশ্চিত?"
    static var cancel = "বাতিল"
    static var successful = "সফল!"
    static var noInformationFound = "কোন তথ্য পাওয়া যায় নি"
    static var noCategoryFound = "কোন ক্যাটেগরি পাওয়া যায় নি"
    //static var noTodaysSummaryFound = "আজকের কোন "
    static var delete = "ডিলিট করুন"
    static var yes = "হ্যা"
    static var no = "না"
    static var productSearch = "পণ্য খুঁজুন"
    static var searchFilter = "ফিল্টার করুন"
    
    //Module -> Login
    static var login = "লগ ইন"
    static var logout = "লগ আউট করুন"
    static var register = "রেজিষ্ট্রেশন করুন"
    static var phoneNumberIsIncorrect = "ফোন নম্বর টি সঠিক নয়"
    static var phoneNumberIsRequired = "ফোন নম্বর আবশ্যক"
    static var passwordIsRequired = "পাসওয়ার্ড আবশ্যক"
    static var repeatPasswordIsRequired = "পাসওয়ার্ড নিশ্চিত করা আবশ্যক"
    static var registration = "রেজিষ্ট্রেশন"
    static var nameIsRequired = "নাম আবশ্যক"
    static var passwordDidNotMatch = "পাসওয়ার্ড এর অমিল, পূণরায় চেষ্টা করুন"
    static var mobileNumber = "মোবাইল নম্বর"
    static var password = "পাসওয়ার্ড"
    static var repeatPassword = "পাসওয়ার্ড নিশ্চিত করুন"
    static var sixToTwenty = "(৬-২০ অক্ষর হতে হবে)"
    static var dontHaveAnAccount = "একাউন্ট নেই"
    static var alreadyHaveAnAccount = "ইতিমধ্যে একটি একাউন্ট আছে"
    static var name = "নাম"
    static var phone = "ফোন"
    static var shopCategory = "দোকানের ক্যাটেগরি"
    
    //Home
    static var dashboard = "ড্যাশবোর্ড"
    static var existingDataWillBeRemoved = "বিদ্যান সকল ডাটা মুছে যাবে!"
    static var sale = "বিক্রয়"
    static var purchase = "ক্রয়"
    static var expense = "খরচ"
    static var newInventory = "নতুন পণ্য"
    static var newDue = "নতুন বাকি"
    static var newPayable = "নতুন দেনা"
    static var newEmployee = "নতুন কর্মচারী"
    static var newCustomer = "নতুন কাস্টমার"
    static var newDelivery = "নতুন ডেলিভারি"
    static var newVendor = "নতুন ডিলার "
    static var dummyDataButton = "ডামি ডাটা "
    
    //SideMenu
    static var todaysSummary = "আজকের সারাদিন"
    static var saleBook = "বিক্রয়ের খাতা"
    static var deliveryBook = "ডেলিভারির খাতা"
    static var dueBook = "বাকির খাতা"
    static var customerBook = "কাস্টমারের খাতা"
    static var inventory = "মালামালের খাতা"
    static var expenseBook = "খরচের খাতা"
    static var employeeBook = "কর্মচারীর খাতা"
    static var purchaseBook = "ক্রয়ের খাতা"
    static var rawMaterial = "কাঁচামালের খাতা"
    static var preOrder = "প্রি-অর্ডার"
    static var payableBook = "দেনার খাতা"
    static var vendorBook = "ডিলারের খাতা"
    static var accounts = "হিসাব"
    static var accountsBook = "হিসাবের খাতা"
    static var contact = "যোগাযোগ"
    
    //Mark: TodaysSummary
    static var todaysSale = "আজকের বিক্রয়"
    static var otherCalculations = "অন্যান্য হিসাব"
    
    //Sale
    //SalesListViewController
    static var selectStartDate = "শুরুর তারিখ বাছাই করুন"
    static var startingDateIsRequired = "শুরুর তারিখ আবশ্যক"
    static var endDateIsRequired = "শেষের তারিখ আবশ্যক"
    static var selectEndDate = "শেষের তারিখ বাছাই করুন"
    static var selectDate = "তারিখ বাছাই করুন"
    static var startDate = "তারিখ থেকে"
    static var endDate = "তারিখ পর্যন্ত"
    static var totalDue = "সর্বমোট বাকি"
    static var totalSale = "সর্বমোট বিক্রয়"
    static var totalSaleAmount = "সর্বমোট বিক্রয়মূল্য"
    //SalePerDayListViewController
    static var total = "সর্বমোট"
    static var due = "বাকি"
    static var deliverySystem = "ডেলিভারি মাধ্যম"
    static var delivery = "ডেলিভারি"
    static var paid = "পরিশোধিত"
    static var unpaid = "অপরিশোধিত"
    //SaleDetailsViewController
    static var salesInvoice = "বিক্রয় রসিদ"
    static var invoice = "রসিদ"
    static var invoiceReceive = "রশিদ গ্রহন"
    static var date = "তারিখ"
    static var deliveryDate = "ডেলিভারি তারিখ"
    static var time = "সময়"
    static var discount = "ডিস্কাউন্ট"
    static var paymentMethod = "পেমেন্টের মাধ্যম"
    static var paymentNumber = "পেমেন্ট নম্বর"
    static var paidAmount = "পরিশোধিত মূল্য"
    static var deliveryCharge = "ডেলিভারি চার্জ"
    static var customerName = "কাস্টমারের নাম"
    static var address = "ঠিকানা"
    static var quantity = "পরিমাণ"
    static var pricePerUnit = "মূল্য/একক"
    static var deliveryStatus = "ডেলিভারি স্ট্যাটাস"
    static var delivered = "ডেলিভারি সম্পন্ন"
    static var deliverySystemDue = "ডেলিভারি পদ্ধতির বাকি"
    static var invoiceDue = "রসিদ বাকি"
    static var latestDue = "সর্বশেষ বাকি"
    static var duePaid = "বাকি পরিশোধিত"
    static var remainingDue = "অবশিষ্ট বাকি"
    static var othersDue = "অন্যান্য বাকি"
    static var othersDuePaid = "অন্যান্য বাকি পূরণ"
    static var data = "ডাটা"
    static var productReturn = "পণ্য ফেরত"
    static var retturn = "ফেরত"
    //AddNewSaleViewController
    static var theProductIsAlreadySelectedDeselectTheProductForUpdate = "পণ্য টি ইতিমদ্ধ্যে নির্বাচন করা হয়েছে, পরিবর্তনের জন্য পুনরায় নির্বাচন করুন"
    static var insufficientQuantity = "অপর্যাপ্ত পরিমাণ"
    static var saveAsDraft = "Save as Draft"
    static var updateDraft = "Update Draft"
    static var continuee = "Continue"
    static var complete = "সম্পন্ন করুন"
    static var youCannotSaleThisProduct = "পণ্যটি বিক্রয়যোগ্য নয়"
    static var discard = "বাতিল"
    static var next = "পরবর্তী"
    static var submit = "সম্পন্ন"
    static var refresh = "রিফ্রেশ"
    static var draft = "ড্রাফট"
    //SaleAddSecondViewController
    static var one = "১"
    static var quantityIsRequired = "পরিমাণ আবশ্যক"
    static var sellingPriceIsRequired = "বিক্রয়মূল্য আবশ্যক"
    //SaleAddSerialViewController
    static var productSerialIsRequired = "পণ্যের সিরিয়াল আবশ্যক"
    //SaleInfoViewController
    static var amount = "পরিমাণ"
    static var customer = "কাস্টমার"
    static var payableAmount = "দেনার পরিমাণ"
    static var cash = "ক্যাশ"
    static var cashBack = "ক্যাশব্যাক"
    static var bkashPaymentPhoneNumberIsRequired = "বিকাশ পেমেন্ট নম্বর আবশ্যক"
    static var rocketPaymentPhoneNumberIsRequired = "রকেট পেমেন্ট ফোন নম্বর আবশ্যক"
    static var accountNumberIsRequired = "একাউন্ট নাম্বার আবশ্যক"
    static var cashIsRequired = "ক্যাশ আবশ্যক"
    static var customerIsRequired = "কাস্টমার আবশ্যক"
    static var bkashMobileNumber = "বিকাশ মোবাইল নম্বর"
    static var rocketMobileNumber = "রকেট মোবাইল নম্বর"
    static var accountNumber = "একাউন্ট নাম্বার"
    static var account = "একাউন্ট"
    static var youCanNotUpdateMoreThan = "আপনি এর চেয়ে বেশি পরিবর্তন করতে পারবেন না"
    static var youCanNotUpdateLessThan = "আপনি এর চেয়ে কম পরিবর্তন করতে পারবেন না"
    static var youCanNotPayMoreThan = "আপনি এর চেয়ে বেশি টাকা দিতে পারবেন না"
    static var walletPayExceedsPayableAmount = "ওয়ালেট পে পরিশোধ মূল্যর চেয়ে বেশি !"
    static var walletAmount = "ওয়ালেট পরিমান"
    static var walletPay = "ওয়ালেট পে"
    //SaleProductReturnViewController
    static var searchSaleDate = "বিক্রয়ের তারিখ খুঁজুন"
    //InvoiceReturnViewController
    static var productName = "পণ্যের নাম"
    static var returnQuantity = "ফেরতের পরিমাণ"
    static var productIsRequired = "পণ্য আবশ্যক"
    static var youCantReturnMoreThan = "আপনি বেশি ফেরত দিতে পারবেন না এর চেয়ে"
    static var CashBackIsRequired = "ক্যাশব্যাক আবশ্যক"
    static var returnRelatedInfo = "ফেরত সম্পর্কিত তথ্য"
    //SaleOnDeliveryViewController
    static var deliverySystemAdd = "ডেলিভারি পদ্ধতি যোগ"
    static var deliverySystemIsRequired = "ডেলিভারি পদ্ধতি আবশ্যক"
    static var deliveryChargeIsRequired = "ডেলিভারি চার্জ আবশ্যক"
    static var newDeliverySystemAdd = "নতুন ডেলিভারি পদ্ধতি যোগ"
    static var phoneNumber = "ফোন নম্বর"
    //SaleWithDiscountViewController
    static var discountIsRequired = "ডিস্কাউন্ট আবশ্যক"
    //SaleWithCustomerViewController
    static var customerAdd = "কাস্টমার যোগ"
    static var customerPhone = "কাস্টমার ফোন"
    //SalesCustomerSearchViewController
    static var searchCustomer = "কাস্টমার খুঁজুন"
    //SalePerDaySearchViewController
    static var searchSerial = "সিরিয়াল খুঁজুন"
    //SaleListPopOverVC
    static var saleDraft = "বিক্রয় খসড়া"
    static var salePreOrder = "বিক্রয় প্রি-অর্ডার"
    
    //Product
    //ProductListViewController
    static var damagedProduct = "ক্ষতিগ্রস্থ কাঁচামাল"
    static var variant = "ভিন্নতা"
    static var category = "ক্যাটেগরি"
    static var parentCategory = "Parent Category"
    static var sellingPrice = "বিক্রয়মূল্য"
    static var categoryAndSubCategory = "ক্যাটেগরি এবং সাব-ক্যাটেগরি"
    //ProductDetailsBaseViewController
    static var productDetails = "পণ্যের বিবরণ"
    static var company = "কোম্পানি"
    static var productCode = "পণ্যের কোড"
    static var purchasePrice = "ক্রয়মূল্য"
    static var quantityAlert = "পরিমাণ সতর্কতা"
    //ProductDViewController
    static var recentStock = "সাম্প্রতিক স্টক"
    static var recentSale = "সাম্প্রতিক বিক্রয়"
    static var serialNo = "সিরিয়াল নম্বর"
    static var stockAvailable = "স্টক বর্তমান"
    static var vendorName = "ডিলারের নাম"
    static var warranty = "ওয়ারেন্টি"
    static var expireDate = "মেয়াদোত্তীর্ণ হবার তারিখ"
    static var unSold = "বিক্রি হয়নি"
    static var sold = "বিক্রি হয়েছে"
    static var refund = "ফেরত মূল্য"
    static var update = "আপডেট"
    static var theStockIsEmpty = "স্টক টি শূন্য"
    static var serialUpdate = "সিরিয়াল আপডেট"
    //ProductCategoryViewController
    static var selectCategory = "ক্যটেগরি বাছাই করুন"
    static var newCategoryAdd = "নতুন ক্যাটেগরি যোগ"
    static var newCategory = "নতুন ক্যাটেগরি"
    static var categoryName = "ক্যটেগরির নাম"
    static var categoryIsRequired = "ক্যটেগরির নাম আবশ্যক"
    static var categoryAlreadyExists = "ক্যটেগরি টি ইতিমধ্যে বর্তমান"
    //ProductSubCategoryVC
    static var newSubCategoryAdd = "নতুন সাব ক্যাটেগরি যোগ"
    static var subCategoryName = "সাব ক্যাটেগরির নাম"
    static var subCategory = "সাব-ক্যাটেগরি"
    static var subCategoryNameIsRequired = "সাব ক্যটেগরির না আবশ্যক"
    static var subCategoryIsAlreadyExists = "সাব ক্যাটেগরি টি ইতিমধ্যে বর্তমান"
    //AddNewProductVC
    static var unit = "একক"
    static var newProduct = "নতুন পণ্য"
    static var pc = "পিস"
    static var productNameIsRequired = "পণের নাম আবশ্যক"
    static var productCategoryIsRequired = "পণ্যের ক্যাটেগরি আবশ্যক"
    static var productUnitIsRequired = "পণের একক আবশ্যক"
    //ProductAddThirdVC
    static var serialNumber = "সিরিয়াল নম্বর"
    static var quantityAlertWhenSmallerThan = "পরিমাণ সতর্কতা, যখন কম এর চেয়ে"
    static var useCommaToSeparateSerial = "সিরিয়াল নম্বর পৃথক করার জন্য কমা (,) চাপুন"
    static var newVendorAdd = "নতুন ডিলার যোগ"
    static var vendorMobileNumber = "ডিলারের মোবাইল নম্বর"
    static var vendorNameIsRequired = "ডিলারের নাম আবশ্যক"
    static var vendorMobileNumberIsRequired = "ডিলারের ফোন নম্বর আবশ্যক"
    static var month = "মাস"
    static var buyingPrice = "ক্রয়মূল্য"
    static var selectOne = "বাছাই করুন"
    static var vendor = "ডিলার"
    //RecentStockReturnViewController
    static var productInfo = "পণ্যের তথ্য"
    static var selectSerialNumber = "সিরিয়াল নম্বর বাছাই করুন"
    static var selectAVendorToReturnProduct = "পণ্য ফেরতের জন্য ডিলার খুঁজুন"
    static var buyingPriceIsRequired = "ক্রয়মূল্য আবশ্যক"
    //RecentStockUpdateViewController
    static var stockUpdate = "স্টক আপডেট"
    //DamagedProductListVC
    static var damagedProducts = "ক্ষতিগ্রস্থ পণ্য"
    static var damaged = "ক্ষতিগ্রস্থ"
    static var times = "সময়"
    static var totalDamages = "সর্বমোট ক্ষতিগ্রস্থ"
    static var totalDamageAmount = "সর্বমোট ক্ষতিগ্রস্থ পণ্য"
    //DamagedProductSearchVC
    static var damagedProductSearch = "ক্ষতিগ্রস্থ পণ্য খুঁজুন"
    //DamagedPerProductVC
    static var damagedQuantity = "ক্ষতিগ্রস্থ পরিমাণ"
    static var description = "বিবরণ"
    static var close = "বন্ধ"
    static var yesDelete = "অবশ্যই, মুছে দিন"
    //DamageableProductList
    static var toDamageSelectProduct = "ক্ষতিগ্রস্থ পণ্য বাছাই করুন"
    //DamageableProductBaseInfoVC
    static var quantityCantBeGreaterThan = "পরিমাণ এর চেয়ে বেশী হতে পারবে না"
    static var buyingpriceCantBeGreaterThan = "ক্রয়মূল্য বেশী হতে পারবে না এর থেকে"
    static var returnPriceIsRequired = "ফেরত মূল্য আবশ্যক"
    //ModuleProductCategory
    static var treeView = "Tree View"
    static var categoryUpdate = "ক্যাটেগরি পরিবর্তন"
    //static var parentCategoryIsRequired = ""
    
    //Module -> Raw Material
    //RawMaterialListVC
    static var production = "উৎপাদন"
    static var newRawMaterial = "নতুন কাঁচামাল"
    static var damagedRawMaterial = "ক্ষতিগ্রস্থ কাঁচামাল"
    //RawMaterialSearchVC
    static var rawMaterialSearch = "কাঁচামাল খুঁজুন"
    //RawMaterialDetailsBaseVC
    static var rawMaterialInfo = "কাঁচামালের তথ্য"
    static var materialSku = "কাঁচামালের এস কে ইউ"
    static var currentStock = "চলতি স্টক"
    static var currentStockAmount = "চলতি স্টক পরিমাণ"
    //RawMaterialDetailsVC
    static var recentMaterialStock = "সাম্প্রতিক কাঁচামাল স্টক"
    static var recentUse = "সাম্প্রতিক  ব্যবহৃত"
    static var available = "বিদ্যমান"
    static var stockAmount = "স্টকের পরিমাণ"
    static var usedQuantity = "ব্যবহৃত পরিমান"
    //RawMaterialRecentStockReturnVC
    static var rawMaterialName = "কাঁচামালের নাম"
    static var pricecannotBe0OrLess = "মূল্য ০ বা এর কম হতে পারবে না"
    //ProductionRawMaterialListVC
    static var selectRawMaterial = "কাঁচামাল বাছাই করুন"
    static var selectARawMaterialForProduction = "উৎপাদনের জন্য কাঁচামাল বাছাই করুন"
    static var theRawMaterialIsAlreadySelectedDeselectItThenUpdate = "কাঁচামাল টি পূর্বেই বাছাইকৃত, পুণরায় বাছাই করে পরিবর্তন করুন"
    //ProductionRawMaterialAccessoryInfoVC
    static var product = "পণ্য"
    static var selectProduct = "পণ্য বাছাই করুন"
    static var productionCost = "উৎপাদন খরচ"
    static var totalExpense = "সর্বমোট খরচ"
    //PurchaseableRawMaterialListVC
    static var selectARawMaterialToPurchase = "ক্রয়ের জন্য কাঁচামাল বাছাই করুন"
    //PurchaseReturnRawMaterialListVC
    static var selectARawMaterialToReturn = "ফেরতের জন্য কাঁচামাল বাছাই করুন"
    //PurchaseReturnRawMaterialBaseInfoVC
    static var rawMaterialReturn = "কাঁচামাল ফেরত"
    static var returnPrice = "ফেরত মূল্য"
    static var cashBackPriceIsRequired = "ক্যাশব্যাকের পরিমাণ আবশ্যক"
    static var cashbackPriceCantBeGreaterThan = "ফেরত মূল্য বেশি হতে পারবে না এর থেকে"
    //PurchaseReturnRawMaterialAccessoryInfoVC
    static var totalPrice = "সর্বমোট মূল্য"
    //RawMaterialPurchaseListVC
    static var rawMaterialPurchaseBook = "কাঁচামাল ক্রয়ের খাতা"
    //RawMaterialPurchaseDetailsVC
    static var materialPurchaseInvoice = "কাঁচামাল ক্রয়ের রসিদ"
    static var invoicePayable = "রসিদের দেনা"
    static var latestPayable = "সর্বশেষ দেনা"
    static var payablePaid = "পরিশোধিত দেনা"
    static var remainingPayable = "অবশিষ্ট দেনা"
    static var othersPayable = "অন্যান্য দেনা"
    //DamagedRawMaterialSearchVC
    static var damagedRawMaterialSearch = "ক্ষতিগ্রস্থ কাঁচামাল খুঁজুন"
    //DamageableRawMaterialListVC
    static var selectRawMaterialToDamage = "কাঁচামাল নির্বাচন করুন ক্ষতিগ্রস্থের জন্য"
    //RawMaterialUpdateViewController
    static var rawMaterialUnitRequired = "কাঁচামাল ইউনিট আবশ্যক"
    //AddRawMaterialBaseInfoVC
    static var rawMaterialCode = "কাঁচামালের কোড"
    static var rawMaterialNameIsRequired = "কাঁচামালের নাম আবশ্যক"
    //PurchaseableRawMaterialAccessoryInfoVC
    static var purchaseDate = "ক্রয়ের তারিখ"
    static var saleDate = "বিক্রয়ের তারিখ"
    
    //Module -> Purchase
    //PurchaseDetailsVC
    static var purchaseInvoice = "ক্রয় রসিদ"
    //PurchaseReturnDetailsVC
    static var returnInvoice = "ফেরত রসিদ"
    //PurchaseableProductListVC
    static var selectAProductToPurchase = "ক্রয় করার জন্য পণ্য বাছাই করুন"
    //PurchaseAddVC
    static var newPurchase = "নতুন ক্রয়"
    static var purchaseRelatedInfo = "ক্রয় নংক্রান্ত তথ্য"
    //RefundableProductListVC
    static var toReturnSelectAProduct = "ফেরত দেবার জন্য পণ্য বাছাই করুন"
    //RefundAddSerialVC
    static var serialNumberIsRequired = "সিরিয়াল নম্বর আবশ্যক"
    //PreOrderListVC
    static var received = "গ্রহন করা হয়েছে"
    static var orderDate = "অর্ডারের তারিখ"
    static var receiveDate = "গ্রহনের তারিখ"
    static var advanceAmount = "অগ্রিমের পরিমাণ"
    static var amountToBePaid = "পরিশোধ যোগ্য মূল্যের পরিমাণ"
    static var noRemarks = "কোন মন্তব্য নেই"
    //PreOrderableProductVC
    static var toOrderSelectAProduct = "অর্ডারের জন্য পণ্য বছাই করুন"
    //PreOrderAddVC
    static var newPreOrder = "নতুহ্ন প্রি-অর্ডার"
    static var addMoreProduct = "নতুন পণ্য যোগ"
    //PreOrderAddInfoVC
    static var remarks = "মন্তব্য"
    static var selectReceiveDate = "গ্রহনের তারিখ বাছাই করুন"
    //PreOrderPendingVC
    static var receive = "গ্রহন"
    static var changeAdvance = "অগ্রিম পরিবর্তন করুন"
    static var advanceAmountIsRequired = "অগ্রিম টাকার পরিমাণ আবশ্যক"
    static var advanceAmountsNeedToBeModified = "অগ্রিম টাকার পরিমাণ পরিবর্তন করা আবশ্যক"
    static var paidAmountIsRequired = "পরিশোধিত মূল্যের পরিমাণ আবশ্যক"
    //PerOrderEditProductList
    static var selectProductsForPreOrder = "প্রি-অর্ডারের জন্য পণ্য বাছাই করুন"
    
    //Module -> Expense
    //ExpenseViewController
    static var totalExpenseAmount = "সর্বমোট খরচের পরিমাণ"
    //AddNewExpenseVC
    static var employee = "কর্মচারী"
    static var expenseAmount = "খরচের পরিমাণ"
    static var updateInfo = "আপডেটের তথ্য"
    static var newExpense = "নতুন খরচ"
    static var expenseTypeIsRequired = "খরচের ধরণ আবশ্যক"
    static var expenseType = "খরচের ধরণ"
    static var newExpenseAdd = "নতুন খরচ যোগ"
    static var expenseDate = "খরচের তারিখ"
    static var selectAnItemToSearch = "খোঁজ করতে আইটেম নির্বাচন করুন"
    
    //Module -> Due
    //DueViewController
    static var dueCustomer = "বাকির গ্রাহক"
    //AddNewDueVC
    static var dueUpdate = "বাকি আপডেট"
    static var withdrawUpdate = "পূরণ আপডেট"
    static var withdrawnUpdate = "পূরণের পরিমাণ"
    static var collectionAmount = "সংগ্রহের পরিমাণ"
    
    
    //Module -> Delivery
    //DeliveryViewController
    static var deliveryLog = "ডেলিভারি লগ"
    static var deliveryHistory = "ডেলিভারি বৃত্তান্ত"
    static var withdraw = "পূরণ"
    static var deliveryNameIsRequired = "ডেলিভারি কোম্পানির নাম আবশ্যক"
    //AddNewDeliveryDueVC
    static var dueAmount = "বাকির পরিমাণ"
    static var dueWithdraw = "বাকি পূরণ"
    static var dueWithdrawAmount = "বাকি পূরণের পরিমাণ"
    static var withdrawAmountIsTooMuch = "পূরণের পরিমাণ অতিরিক্ত"
    //DeliverySystemSearchVC
    static var deliverySystemSearch = "ডেলিভারি কোম্পানি খুঁজুন"
    //DeliveryLogSearchVC
    static var from = "থেকে"
    static var to = "হতে"
    static var search = "খুঁজুন"
    //AddNewDeliverySystemVC
    static var newDeliverySystemInfo = "নতুন ডেলিভারি কোম্পানির তথ্য"
    //DeliveryDuePaidVC
    static var commission = "কমিশন"
    static var newDueIsRequired = "নতুন বাকি আবশ্যক"
    static var commissionUnitIsRequired = "কমিশনের একক আবশ্যক"
    static var oneHundred = "১০০"
    //DBaseViewController
    static var deliverySystemProfile = "ডেলিভারি কোম্পানি প্রোফাইল"
    static var totalOrder = "সর্বমোট অর্ডার"
    static var currentOrder = "চলতি অর্ডার"
    static var totalDuePaid = "সর্বমোট বাকি পূরণ"
    //DDetailsViewController
    static var by = "কর্তৃক"
    static var totalPaid = "সর্বমোট পরিশোধিত"
    static var dueHistory = "বাকি বৃত্তান্ত"
    
    //Module -> Customer
    //CustomerSearchVC
    static var customerSearch = "গ্রাহক খুঁজুন"
    //CustomerDBaseVC
    static var customerDetails = "গ্রাহকের বিবরণ"
    static var customerWallet = "কাস্টমার ওয়ালেট"
    static var dueAdjust = "বাকি সমন্বয়"
    static var dueAdjustUpdate = "বাকি সমন্বয় পরিবর্তন"
    static var newDueAdjust = "নতুন বাকি সমন্বয়"
    static var dueAdjustAmount = "বাকি সমন্বয় পরিমান"
    static var adjustedBy = "সমন্বয়কারী"
    static var dueAdjustAmountIsRequired = "বাকি সমন্বয় পরিমাণ আবশ্যক"
    static var youCanNotAdjustMoreThan = "আপনি এর চেয়ে বেশি সমন্বয় করতে পারবেন না"
    //CustomerAddVC
    static var newCustomerAdd = "নতুন গ্রাহক যোগ করুন"
    static var updateCustomerInfo = "গ্রাহকের তথ্য আপডেট করুন"
    static var customerDueUpdate = "গ্রাহকের বাকি আপডেট করুন"
    static var customerAddressIsRequired = "গ্রাহকের ঠিকানা প্রয়োজন"
    //CustomerDuePaidVC
    static var duePaymentIsRequired = "বাকি পরিশোধ করা আবশ্যক"
    
    //Module -> Vendors
    //VendorSearchViewController
    static var vendorSearch = "ডিলার খুঁজুন"
    //VendorDBaseVC
    static var vendorProfile = "ডিলার প্রোফাইল"
    static var totalPurchase = "সর্বমোট ক্রয়"
    static var currentPayable = "চলতি দেনা"
    static var totalPayable = "সর্বমোট দেনা"
    static var vendorWallet = "ডিলার ওয়ালেট"
    static var payableAdjust = "দেনা সমন্বয়"
    static var payableAdjustUpdate = "দেনা সমন্বয় পরিবর্তন"
    static var newPayableAdjust = "নতুন দেনা সমন্বয়"
    static var payableAdjustAmount = "দেনা সমন্বয় পরিমান"
    static var payableAdjustAmountIsRequired = "দেনা সমন্বয় পরিমাণ আবশ্যক"
    //VendorDViewController
    static var payableHistory = "দেনা বৃত্তান্ত"
    static var purchaseHistory = "ক্রয় বৃত্তান্ত"
    static var returnHistory = "ফেরত বৃত্তান্ত"
    static var payable = "দেনা"
    static var purchaseBy = "ক্রেতা"
    static var totalAmount = "সর্বমোট পরিমাণ"
    static var returnedBy = "কর্তৃক ফেরত দাতা"
    //AddNewVendorVC
    static var vendorInfo = "ডিলারের তথ্য"
    static var vendorInfoUpdate = "ডিলারের তথ্য আপডেট"
    //VendorPaidVC
    static var payableWithdraw = "দেনা পূরণ"
    static var paidAmountIsGreaterThanCurrentPayable = "দেনার থেকে পরিশোধিত মূল্যের পরিমাণ বেশি"
    
    //Module -> Payable Book
    //PayableViewController
    static var vendors = "ডিলার"
    //AddNewPayableVC
    static var payableUpdate = "দেনার আপডেট"
    static var payableWithdrawUpdate = "দেনা পূরণ আপডেট"
    static var payableWithdrawAmount = "দেনা পূরণের পরিমাণ"
    static var payableWithdrawAmountIsEmpty = "দেনা পূরণের পরিমাণ আবশ্যক"
    static var payableAmountIsRequired = "দেনার পরিমাণ আবশ্যক"
    static var payableWithdrawUpdateAmountIsTooMuch = "দেনা পূরণের পরিমাণ অতিরিক্ত"
    static var payableWithdrawnAmountIsTooMuch = "দেনা পূরণের পরিমাণ দেনার থেকে বেশি"
    //EditPayableVC
    static var addressIsRequired = "ঠিকানা আবশ্যক"
    
    //Module -> EmployeeBook
    //EmployeeDBaseVC
    static var employeeDetails = "কর্মচারীর বিবরণ"
    static var employeeName = "কর্মচারীর নাম"
    static var joiningDate = "যোগদানের তারিখ"
    static var totalInvoice = "সর্বমোট রসিদ"
    static var totalsalaryPaid = "সর্বমোট পরিশোধিত বেতন"
    static var accountStatus = "একাউন্ট স্ট্যাটাস"
    static var paidSalary = "পরিশোধিত বেতন"
    static var unpaidSalary = "অপরিশোধিত বেতন"
    static var active = "একটিভ"
    static var inactive = "ইনএকটিভ"
    static var paidSalaryIsTooMuch = "পরিশোধিত বেতন বেশি!"
    //EmployeeDViewController
    static var saleHistory = "বিক্রয় বৃত্তান্ত"
    static var salaryHistory = "বেতন বৃত্তান্ত"
    //NewEmployeeAddVC
    static var employeePassword = "কর্মচারীর পাসওয়ার্ড"
    static var employeeMobileNo = "কর্মচারীর মোবাইল নম্বর"
    static var employeeInfoUpdate = "কর্মচারীর তথ্য আপডেট"
    static var employeeNameIsRequired = "কর্মচারীর নাম আবশ্যক"
    static var permissionIsRequired = "অনুমতি আবশ্যক"
    static var permission = "অনুমতি"
    //EmployeeSearchVC
    static var employeeSearch = "কর্মচারী খুঁজুন"
    //EmployeeDBasePopOverVC
    static var salary = "বেতন"
    static var payment = "পরিশোধ"
    //EmployeeSalaryAdd
    static var status = "Status"
    static var salaryUpdate = "বেতন পরিবর্তন"
    static var unpaidSalaryAmount = "পেন্ডিং বেতনের পরিমান"
    
    //Module -> Accounts
    //Asset
    //AssetPropertyListVC
    static var assets = "সম্পদ"
    static var saleableStockAmount = "বিক্রয়যোগ্য স্টকের পরিমাণ"
    static var properties = "সম্পত্তি"
    static var equipments = "উপকরণ"
    static var goodWill = "গুড উইল"
    static var addNewProperty = "নতুন সম্পত্তি যোগ"
    static var addNewEquipments = "নতুন উপকরণ যোগ"
    static var goodWillAmount = "গুড উইলের পরিমাণ"
    static var goodwillAmountIsRequired = "গুড উইলের পরিমাণ আবশ্যক"
    //PropertyNequipmentsAddVC
    static var amountIsRequired = "টাকার পরিমাণ আবশ্যক"
    static var newCategoryNameIsRequired = "নতুন ক্যাটেগরির নাম আবশ্যক"
    //Gross&NetProfitVC
    static var grossProfit = "গ্রস প্রফিট"
    static var netProfit = "নেট প্রফিট"
    //AccountsViewController
    static var cashDrawer = "ক্যাশ ড্রয়ার"
    static var loan = "ঋণ"
    static var invest = "বিনিয়োগ"
    static var grossAndNetProfit = "গ্রস ও নেট প্রফিট"
    static var cashFlowReport = "ক্যাশ ফ্লো রিপোর্ট"
    static var balanceSheet = "ব্যালেন্স শীট"
    static var commingSoon = "শীঘ্রই আসছে"
    static var workingOnProgress = "কাজ সম্পন্ন হচ্ছে"
    static var sorryForTheInterupt = "বিঘ্নতার জন্য দুঃখিত"
    //CashFlowReportVC
    static var sales = "সেলস"
    static var receivable = "হিসাব থেকে পাওনা"
    static var collectionOfLoan = "সংগৃহীত ঋণের পরিমাণ"
    static var collectionOfInvest = "সংগৃহীত  বিনিয়োগের পরিমাণ"
    static var totalCashReceive = "সর্বমোট গৃহীত টাকার পরিমাণ"
    static var costOfSale = "বিক্রয় ব্যয়"
    static var deliveryCommission = "ডেলিভারি কমিশন"
    static var loanPaid = "পরিশোধিত ঋণের পরিমাণ"
    static var payBack = "পে ব্যাক"
    static var totalCashPaid = "সর্বমোট পরিশোধিত টাকার পরিমাণ"
    static var cashInHand = "হাতে টাকার পরিমাণ"
    //BalanceSheetVC
    static var searchDateIsRequired = "খোঁজার তারিখ আবশ্যক"
    static var currentAssets = "বর্তমান সম্পদ"
    static var fixedAssets = "স্থায়ী সম্পদ"
    static var liabilities = "দায়"
    static var investment = "বিনিয়োগ"
    static var totalAssets = "সর্বমোট সম্পদ"
    static var totalLiabilities = "সর্বমোট দায়"
    static var totalInvestment = "সর্বমোট বিনিয়োগের পরিমাণ"
    //CashDrawer
    static var enterCashAmountOfYourDrawer = "ক্যাশ ড্রয়ারের টাকার পরিমাণ উল্লেখ করুন"
    static var cashIn = "ক্যাশ ইন"
    static var cashOut = "ক্যাশ আউট"
    static var cashInToVendorWallet = "ডিলার ওয়ালেটে ক্যাশ ইন করুন"
    static var cashInToCustomerWallet = "কাস্টমার ওয়ালেটে ক্যাশ ইন করুন"
    static var youCanNotCashOutMoreThan = "আপনি এর চেয়ে বেশি ক্যাশ উত্তোলন করতে পারবেন না"
    //CashTransactionVC
    static var transactionUpdate = "লেনদেন আপডেট"
    static var previousCashAmount = "পূর্বের টাকার পরিমাণ"
    static var newCashAmount = "নতুন টাকার পরিমাণ"
    static var enterCashAmount = "টাকার পরিমাণ উল্লেখ করুন"
    //LoanerListVC
    static var initialLoan = "প্রাথমিক ঋণ"
    static var interestAmount = "মুনাফার পরিমাণ"
    static var deadline = "শেষ তারিখ"
    //LoanerDetailsVC
    static var loanerDetails = "ঋণদাতার বৃত্তান্ত"
    static var totalLoan = "সর্বমোট ঋণ"
    //LoanerTransactionHistoryVC
    static var loanTransactions = "ঋণ লেনদেন"
    //NewLoanAddVC
    static var newLoan = "নতুন ঋণ"
    static var loanAmount = "ঋণের পরিমাণ"
    static var loanDeadline = "ঋণষোধের শেষ তারিখ"
    static var loanerSelection = "ঋন্দাতা নির্বাচন"
    static var newLoanerAdd = "নতুন ঋন্দাতা যোগ"
    static var loanerName = "ঋন্দাতার নাম"
    static var loanerPhone = "ঋণদাতার ফোন"
    static var loanerAddress = "ঋণদাতার ঠিকানা"
    static var loanerSelectionIsRequired = "ঋণদাতা নির্বাচন আবশ্যক"
    static var loanerAmountIsRequired = "ঋণের পরিমাণ আবশ্যক"
    static var interestAmountIsRequired = "মুনাফার পরিমাণ আবশ্যক"
    static var loanerNameIsRequired = "ঋণদাতার নাম আবশ্যক"
    //LoanPaidVC
    static var currentLoan = "চলতি ঋণ"
    static var loanWithdrawAmount = "ঋণ পরিশোধের পরিমাণ"
    static var loanWithdrawAmountIsRequired = "ঋণ পরিশোধের পরিমাণ আবশ্যক"
    //LoanerUpdateVC
    static var loanerUpdate = "ঋণদাতার আপডেট"
    //LoanerTransactionUpdateVC
    static var withdrawUpdatedAmountIsRequired = "উত্তোলন পরিমাণ পরিবর্তন আবশ্যক"
    //LoanerSearchVC
    static var loanerSearch = "ঋণদাতা খুঁজুন"
    //LoanerTransactionLoanUpdateVC
    static var editedLoanAmount = "পরিবর্তিত ঋণের পরিমাণ"
    static var editedInterestAmount = "পরিবর্তিত মুনাফার পরিমাণ"
    static var editedLoanAmountIsRequired = "পরিবর্তিত ঋণের পরিমান আবশ্যক"
    static var editedInterestAmountRequired = "পরিবর্তিত মুনাফার পরিমাণ আবশ্যক"
    static var loanDeadlineIsRequired = "ঋণ পরিশোধের শেষ তারিখ আবশ্যক"
    //InvestorsListVC
    static var investor = "বিনিয়োগকারী"
    //InvestorDetailsVC
    static var investorDetails = "বিনিয়োগকারীর বিবরণ"
    //InvestmentTransactionHistoryVC
    static var investmentTransactions = "বিনিয়োগ লেনদেন"
    //NewInvestmentAddVC
    static var newInvestment = "নতুন বিনিয়োগ"
    static var investorSelection = "বিনিয়োগকারীর বাছাই করুন"
    static var investAmount = "বিনিয়োগের পরিমাণ"
    static var newInvestorAdd = "নতুন বিনিয়োগকারী যোগ"
    static var investorName = "বিনিয়োগকারীর নাম"
    static var investorPhone = "বিনিয়োগকারীর ফোন নম্বর"
    static var investorAddress = "বিনিয়োগকারীর ঠিকানা"
    static var investorSelectionIsRequired = "বিনিয়োগকারী নির্বাচন আবশ্যক"
    static var investorAmountIsRequired = "বিনিয়োগের পরিমাণ আবশ্যক"
    static var investorNameIsRequired = "বিনিয়োগকারীর নাম আবশ্যক"
    //InvestmentProfitWithdrawVC
    static var investmentWithdrawAmountIsRequired = "পূরণকৃত বিনিয়োগের পরিমাণ আবশ্যক"
    static var investmentWithdrawInfo = "বিনিয়োগ উত্তোলনের তথ্য"
    static var currentInvestment = "চলতি বিনিয়োগ"
    static var investmentWithdrawAmount = "বিনিয়োগ উত্তোলনের পরিমান"
    //InvestmentTransactionUpdateVC
    static var editedInvestedAmount = "পরিবর্তিত বিনিয়োগের পরিমাণ"
    static var editedInvestmentAmountIsRequired = "পরিবর্তিত বিনিয়োগের পরিমাণ আবশ্যক"
    //InvestorSearchVC
    static var investorSearch = "বিনিয়োগকারী খুঁজুন"
    //InvestorUpdateVC
    static var investorUpdate = "বিনিয়োগকারী আপডেট"
    //MobileBankingList
    static var mobileBankIsRequired = "মোবাইল ব্যাংকিং নাম আবশ্যক"
    static var mobileBankingName = "মোবাইল ব্যাংকিং নাম"
    static var mobileBanking = "মোবাইল ব্যাংকিং"
    
    //Contact
    static var call = "কল"
    static var ponnoTitle = "পণ্য"
    static var ponnoAddress = "বাসা : ৫৩(৩য় তলা), রোড: ২০, নিকুঞ্জ-২, ঢাকা-১২২৯"
    static var ponnoMobile = "মোবাইল: ০১৯৯৯০৮৮৬৭২, ০১৯৯৯০৮৮৬৫৪"
    
    //Shop
    //SegmentedVC
    static var shopInformation = "দোকানের তথ্য"
    static var shopSettings = "দোকানের সেটিংস"
    static var paymentReferrals = "পেমেন্ট ও রেফারেল"
    static var promotion = "প্রোমোশন"
    //FirstViewController
    static var shopName = "দোকানের নাম"
    static var ownerName = "মালিকের নাম"
    static var registrationNumber = "রেজিষ্ট্রেশন"
    static var shopPhoneNumber = "দোকানের ফোন নম্বর"
    static var shopAddress = "দোকানের ঠিকানা"
    static var shopNameIsRequired = "দোকানের নাম আবশ্যক"
    static var ownerNameIsRequired = "মালিকের নাম আবশ্যক"
    //SecondViewController
    static var purchaseBase = "ক্রয় নির্ভর"
    static var productionBase = "উৎপাদন নির্ভর"
    static var languageLbl = "ভাষা"
    static var invoiceNote = "রসিদ নোট"
    static var englishTitle = "ইংরেজি"
    static var banglaTitle = "বাংলা"
    static var clearShopData = "দোকানের ডাটা মুছুন"
    static var purchaseBaseOrProductionBaseIsRequired = "উৎপাদন অথবা ক্রয় নির্ভর আবশ্যক "
    //ShopInfoUpdateVc
    static var generalInfo = "সাধারণ তথ্যাদি"
    static var logInInfo = "লগ-ইন তথ্য"
    //OwnerLogInInfoUpdate
    static var loginInformation = "লগইন সংক্রান্ত তথ্য"
    static var oldPassword = "পুরোনো পাসওয়ার্ড আবশ্যক"
    static var newPassword = "নতুন পাসওয়ার্ড"
    static var confirmPassword = "পাসওয়ার্ড "
    static var oldPasswordIsRequired = "পুরোনো পাসওয়ার্ড আবশ্যক"
    static var newPasswordIsRequired = "নতুন পাসওয়ার্ড আবশ্যক"
    static var confirmPasswordIsRequired = "পাসওয়ার্ড নিশ্চিত করুন"
    //ThirdViewController
    static var referrals = "রেফারেলস"
    static var referralsHistory = "রেফারেল বৃত্তান্ত"
    //ReferralInfoViewController
    static var referred = "রেফার্ড"
    static var pending = "পেন্ডিং"
    static var expired = "মেয়াদোত্তীর্ণ"
    static var totalIncome = "সর্বমোট আয়"
    static var totalReceived = "সর্বমোট পরিশোধিত"
    static var currentDue = "বর্তমান বাকি"
    //ReferralDetailsViewController
    static var newReferral = "নতুন রেফারেল"
    //ReferralAddViewController
    static var shopCategoryIsRequired = "দোকানের ক্যাটেগরি আবশ্যক"
    
    
//    //Common
//    static var warning = "Warning"
//    static var congratulations = "Congratulations!"
//    static var areYouSure = "Are you sure?"
//    static var cancel = "Cancel"
//    static var successful = "Successful"
//    static var noInformationFound = "No Information found"
//    static var delete = "Delete"
//    static var yes = "Yes"
//    static var no = "No"
//    static var productSearch = "Product Search"
//
//    //Module -> Login
//    static var login = "Login"
//    static var logout = "Logout"
//    static var register = "Register now"
//    static var phoneNumberIsIncorrect = "Phone number is incorrect"
//    static var phoneNumberIsRequired = "Phone number is required"
//    static var passwordIsRequired = "Password is required"
//    static var repeatPasswordIsRequired = "Confirm password is required"
//    static var registration = "Registration"
//    static var nameIsRequired = "Name is required"
//    static var passwordDidNotMatch = "Password did not match, Try again"
//    static var mobileNumber = "Mobile number"
//    static var password = "Password"
//    static var repeatPassword = "Confirm password"
//    static var sixToTwenty = "(6-20 characters)"
//    static var dontHaveAnAccount = "Don't Have An Account"
//    static var alreadyHaveAnAccount = "Already Have An Account"
//    static var name = "Name"
//    static var phone = "Phone"
//    static var shopCategory = "Shop category"
//
//    //Home
//    static var dashboard = "Dashboard"
//    static var existingDataWillBeRemoved = "Existing data will be removed"
//    static var sale = "Sale"
//    static var purchase = "Purchase"
//    static var expense = "Expense"
//    static var newInventory = "New Inventory"
//    static var newDue = "New Due"
//    static var newPayable = "New Payable"
//    static var newEmployee = "New Employee"
//    static var newCustomer = "New Customer"
//    static var newDelivery = "New Delivery"
//    static var newVendor = "New Vendor"
//    static var dummyDataButton = "Request Dummy Data"
//
//    //SideMenu
//    static var todaysSummary = "Today's Summary"
//    static var saleBook = "Sale Book"
//    static var deliveryBook = "Delivery Book"
//    static var dueBook = "Due Book"
//    static var customerBook = "Customer Book"
//    static var inventory = "Inventory"
//    static var expenseBook = "Expense Book"
//    static var employeeBook = "Employee Book"
//    static var purchaseBook = "Purchase Book"
//    static var rawMaterial = "Raw Material"
//    static var preOrder = "Pre-Order"
//    static var payableBook = "Payable Book"
//    static var vendorBook = "Vendor Book"
//    static var accounts = "Accounts"
//    static var contact = "Contact"
//
//    //Sale
//    //SalesListViewController
//    static var selectStartDate = "Select Start Date"
//    static var startingDateIsRequired = "Starting date is required"
//    static var endDateIsRequired = "End date is required"
//    static var selectEndDate = "Select End Date"
//    static var totalDue = "Total Due"
//    static var totalSale = "Total Sale"
//    //SalePerDayListViewController
//    static var total = "Total"
//    static var due = "Due"
//    static var deliverySystem = "Delivery System"
//    static var paid = "Paid"
//    //SaleDetailsViewController
//    static var salesInvoice = "Sales Invoice"
//    static var invoice = "Invoice"
//    static var date = "Date"
//    static var time = "Time"
//    static var discount = "Discount"
//    static var paymentMethod = "Payment Method"
//    static var paymentNumber = "Payment Number"
//    static var paidAmount = "Paid Amount"
//    static var deliveryCharge = "Delivery Charge"
//    static var customerName = "Customer name"
//    static var address = "Address"
//    static var quantity = "Quantity"
//    static var pricePerUnit = "Price/Unit"
//    static var deliveryStatus = "Delivery Status"
//    static var delivered = "Delivered"
//    static var deliverySystemDue = "Delivery System Due"
//    static var invoiceDue = "Invoice Due"
//    static var latestDue = "Latest Due"
//    static var duePaid = "Due Paid"
//    static var remainingDue = "Remaining Due"
//    static var othersDue = "Others Due"
//    static var data = "Data"
//    static var productReturn = "Product Return"
//    static var retturn = "Return"
//    //AddNewSaleViewController
//    static var theProductIsAlreadySelectedDeselectTheProductForUpdate = "The Product is already selected! Deselect the product for update"
//    static var insufficientQuantity = "Insufficient quantity"
//    //SaleAddSecondViewController
//    static var one = "1"
//    static var quantityIsRequired = "Quantity is required"
//    static var sellingPriceIsRequired = "Selling price is required"
//    //SaleAddSerialViewController
//    static var productSerialIsRequired = "Product Serial is required"
//    //SaleInfoViewController
//    static var amount = "Amount"
//    static var customer = "Customer"
//    static var payableAmount = "Payable Amount"
//    static var cash = "Cash"
//    static var cashBack = "Cash Back"
//    static var bkashPaymentPhoneNumberIsRequired = "Bkash payment phone number is required"
//    static var rocketPaymentPhoneNumberIsRequired = "Rocket payment phone number is required"
//    static var cashIsRequired = "Cash is required"
//    static var customerIsRequired = "Customer is required"
//    static var bkashMobileNumber = "Bkash mobile number"
//    static var rocketMobileNumber = "Rocket mobile number"
//    //SaleProductReturnViewController
//    static var searchSaleDate = "Search Sale Date"
//    //InvoiceReturnViewController
//    static var productName = "Product Name"
//    static var returnQuantity = "Return Quantity"
//    static var productIsRequired = "Product is required"
//    static var youCantReturnMoreThan = "You can't retrun more than"
//    static var CashBackIsRequired = "Cash back is required"
//    //SaleOnDeliveryViewController
//    static var deliverySystemAdd = "Delivery System Add"
//    static var deliverySystemIsRequired = "Delivery System is required"
//    static var deliveryChargeIsRequired = "Delivery Charge is required"
//    static var newDeliverySystemAdd = "New Delivery System Add"
//    static var phoneNumber = "Phone Number"
//    //SaleWithDiscountViewController
//    static var discountIsRequired = "Discount is required"
//    //SaleWithCustomerViewController
//    static var customerAdd = "Customer Add"
//    static var customerPhone = "Customer Phone"
//    //SalesCustomerSearchViewController
//    static var searchCustomer = "Search Customer"
//    //SalePerDaySearchViewController
//    static var searchSerial = "Search Serial"
//
//    //Product
//    //ProductListViewController
//    static var damagedProduct = "Damaged Product"
//    static var variant = "Variant"
//    static var category = "Category"
//    static var sellingPrice = "Selling Price"
//    //ProductDetailsBaseViewController
//    static var productDetails = "Product Details"
//    static var company = "Company"
//    static var productCode = "Product Code"
//    static var purchasePrice = "Purchase Price"
//    static var quantityAlert = "Quantity Alert"
//    //ProductDViewController
//    static var recentStock = "Recent Stock"
//    static var recentSale = "Recent Sale"
//    static var serialNo = "Serial No"
//    static var stockAvailable = "Stock available"
//    static var vendorName = "Vendor Name"
//    static var warranty = "Warranty"
//    static var expireDate = "Expire Date"
//    static var unSold = "Unsold"
//    static var sold = "Sold"
//    static var refund = "Refund"
//    static var update = "Update"
//    static var theStockIsEmpty = "The Stock is empty"
//    static var serialUpdate = "Serial Update"
//    //ProductCategoryViewController
//    static var selectCategory = "Select Category"
//    static var newCategoryAdd = "New Category Add"
//    static var categoryName = "Category Name"
//    static var categoryIsRequired = "Category name is required"
//    static var categoryAlreadyExists = "Category is already exists"
//    //ProductSubCategoryVC
//    static var newSubCategoryAdd = "New Sub Category Add"
//    static var subCategoryName = "Sub Category Name"
//    static var subCategoryNameIsRequired = "Sub Category name is required"
//    static var subCategoryIsAlreadyExists = "Sub Category is already exists"
//    //AddNewProductVC
//    static var unit = "Unit"
//    static var newProduct = "New Product"
//    static var pc = "pc"
//    static var productNameIsRequired = "Product name is required"
//    static var productCategoryIsRequired = "Product category is required"
//    static var productUnitIsRequired = "Product unit is required"
//    //ProductAddThirdVC
//    static var serialNumber = "Serial Number"
//    static var quantityAlertWhenSmallerThan = "Quantity alert! when smaller than,"
//    static var useCommaToSeparateSerial = "Use comma(,) to separate serial"
//    static var newVendorAdd = "New Vendor Add"
//    static var vendorMobileNumber = "Vendor Mobile Number"
//    static var vendorNameIsRequired = "Vendor name is required"
//    static var vendorMobileNumberIsRequired = "Vendor mobile number is required"
//    static var month = "Month"
//    static var buyingPrice = "Buying Price"
//    static var selectOne = "Select One..."
//    //RecentStockReturnViewController
//    static var productInfo = "Product Info"
//    static var selectSerialNumber = "Select Serial Number"
//    static var selectAVendorToReturnProduct = "Select a vendor to return product"
//    static var buyingPriceIsRequired = "Select a vendor to return product"
//    //RecentStockUpdateViewController
//    static var stockUpdate = "Stock Update"
//    //DamagedProductListVC
//    static var damagedProducts = "Damaged Products"
//    static var damaged = "Damaged"
//    static var times = "Times"
//    static var totalDamages = "Total Damages"
//    static var totalDamageAmount = "Total Damage Amount"
//    //DamagedProductSearchVC
//    static var damagedProductSearch = "Damaged Product Search"
//    //DamagedPerProductVC
//    static var damagedQuantity = "Damage Quantity"
//    static var description = "Description"
//    static var close = "Close"
//    static var yesDelete = "Yes! Delete"
//    //DamageableProductList
//    static var toDamageSelectProduct = "To Damage, select product"
//    //DamageableProductBaseInfoVC
//    static var quantityCantBeGreaterThan = "Quantity can't be greater than"
//    static var buyingpriceCantBeGreaterThan = "Buying price can't be greater than"
//    static var returnPriceIsRequired = "Return Price is required"
//
//
//    //Module -> Raw Material
//    //RawMaterialListVC
//    static var production = "Production"
//    static var newRawMaterial = "New Raw Material"
//    static var damagedRawMaterial = "Damaged Raw Material"
//    //RawMaterialSearchVC
//    static var rawMaterialSearch = "Raw Material Search"
//    //RawMaterialDetailsBaseVC
//    static var rawMaterialInfo = "Raw Material Info"
//    static var materialSku = "Material SKU"
//    static var currentStock = "Current Stock"
//    static var currentStockAmount = "Current StockAmount"
//    //RawMaterialDetailsVC
//    static var recentMaterialStock = "Recent Material Stock"
//    static var recentUse = "Recent Use"
//    static var available = "Available"
//    static var stockAmount = "Stock Amount"
//    static var usedQuantity = "Used Quanity"
//    //RawMaterialRecentStockReturnVC
//    static var rawMaterialName = "Raw Material Name"
//    static var pricecannotBe0OrLess = "Price can't be 0 or less "
//    //ProductionRawMaterialListVC
//    static var selectRawMaterial = "Select Raw Material"
//    static var selectARawMaterialForProduction = "Select a raw material for production"
//    static var theRawMaterialIsAlreadySelectedDeselectItThenUpdate = "The Raw Material is already selected! Deselect it then update"
//    //ProductionRawMaterialAccessoryInfoVC
//    static var product = "Product"
//    static var selectProduct = "Select Product"
//    static var productionCost = "Production Cost"
//    static var totalExpense = "Total Expense"
//    //PurchaseableRawMaterialListVC
//    static var selectARawMaterialToPurchase = "Select a Raw Material to purchase"
//    //PurchaseReturnRawMaterialListVC
//    static var selectARawMaterialToReturn = "Select a Raw Material to return"
//    //PurchaseReturnRawMaterialBaseInfoVC
//    static var rawMaterialReturn = "Raw Material Return"
//    static var returnPrice = "Return Price"
//    static var cashBackPriceIsRequired = "Cashback price is required"
//    static var cashbackPriceCantBeGreaterThan = "Cashback price can't be greater than"
//    //PurchaseReturnRawMaterialAccessoryInfoVC
//    static var totalPrice = "Total Price"
//    //RawMaterialPurchaseListVC
//    static var rawMaterialPurchaseBook = "Raw Material Purchase Book"
//    //RawMaterialPurchaseDetailsVC
//    static var materialPurchaseInvoice = "Material Purchase Invoice"
//    static var invoicePayable = "Invoice Payable"
//    static var latestPayable = "Latest Payable"
//    static var payablePaid = "Payable Paid"
//    static var remainingPayable = "Remaining Payable"
//    static var othersPayable = "Others Payable"
//    //DamagedRawMaterialSearchVC
//    static var damagedRawMaterialSearch = "Damaged Raw Material Search"
//    //DamageableRawMaterialListVC
//    static var selectRawMaterialToDamage = "Select Raw Material to damage"
//
//    //Module -> Purchase
//    //PurchaseDetailsVC
//    static var purchaseInvoice = "Purchase Invoice"
//    //PurchaseReturnDetailsVC
//    static var returnInvoice = "Return Invoice"
//    //PurchaseableProductListVC
//    static var selectAProductToPurchase = "Select a product to purchase"
//    //PurchaseAddVC
//    static var newPurchase = "New Purchase"
//    static var purchaseRelatedInfo = "Purchase Related Info"
//    //RefundableProductListVC
//    static var toReturnSelectAProduct = "To Return, Select a product"
//    //RefundAddSerialVC
//    static var serialNumberIsRequired = "Serial number is required"
//    //PreOrderListVC
//    static var received = "Received"
//    static var orderDate = "Order date"
//    static var receiveDate = "Receive date"
//    static var advanceAmount = "Advance Amount"
//    static var amountToBePaid = "Amount to be paid"
//    static var noRemarks = "No remarks"
//    //PreOrderableProductVC
//    static var toOrderSelectAProduct = "To order select a product"
//    //PreOrderAddVC
//    static var newPreOrder = "New Pre-Order"
//    //PreOrderAddInfoVC
//    static var remarks = "Remarks"
//    static var selectReceiveDate = "Select Receive Date"
//    //PreOrderPendingVC
//    static var receive = "Receive"
//    static var changeAdvance = "Change Advance"
//    static var advanceAmountIsRequired = "Advance amount is required"
//    static var advanceAmountsNeedToBeModified = "Advance amount need to be modified"
//    static var paidAmountIsRequired = "Paid amount is required"
//    //PerOrderEditProductList
//    static var selectProductsForPreOrder = "Select products for Pre-Order"
//
//    //Module -> Expense
//    //ExpenseViewController
//    static var totalExpenseAmount = "Total Expense Amount"
//    //AddNewExpenseVC
//    static var employee = "Employee"
//    static var expenseAmount = "Expense Amount"
//    static var updateInfo = "Update Info"
//    static var newExpense = "New Expense"
//    static var expenseTypeIsRequired = "Expense type is required"
//    static var expenseType = "Expense Type"
//    static var newExpenseAdd = "New Expense Add"
//
//    //Module -> Due
//    //DueViewController
//    static var dueCustomer = "Due Customer"
//    //AddNewDueVC
//    static var dueUpdate = "Due Update"
//    static var withdrawUpdate = "Withdraw Update"
//    static var withdrawnUpdate = "Withdrawn Amount"
//    static var collectionAmount = "Collection Amount"
//
//
//    //Module -> Delivery
//    //DeliveryViewController
//    static var deliveryLog = "Delivery Log"
//    static var withdraw = "Withdraw"
//    static var deliveryNameIsRequired = "Delivery name is required"
//    //AddNewDeliveryDueVC
//    static var dueAmount = "Due Amount"
//    static var dueWithdraw = "Due Withdraw"
//    static var dueWithdrawAmount = "Due Withdraw Amount"
//    static var withdrawAmountIsTooMuch = "Withdraw amount is too much"
//    //DeliverySystemSearchVC
//    static var deliverySystemSearch = "Delivery System Search"
//    //DeliveryLogSearchVC
//    static var from = "From"
//    static var to = "To"
//    static var search = "Search"
//    //AddNewDeliverySystemVC
//    static var newDeliverySystemInfo = "New Delivery System Info"
//    //DeliveryDuePaidVC
//    static var commission = "Commission"
//    static var newDueIsRequired = "New Due is required"
//    static var commissionUnitIsRequired = "Commission unit is required"
//    static var oneHundred = "100"
//    //DBaseViewController
//    static var deliverySystemProfile = "Delivery System Profile"
//    //DDetailsViewController
//    static var by = "By"
//    static var totalPaid = "Total Paid"
//
//    //Module -> Customer
//    //CustomerSearchVC
//    static var customerSearch = "Customer Search"
//    //CustomerDBaseVC
//    static var customerDetails = "Customer Details"
//    //CustomerAddVC
//    static var newCustomerAdd = "New Customer Add"
//    static var updateCustomerInfo = "Update Customer Info"
//    static var customerDueUpdate = "Customer Due Update"
//    static var customerAddressIsRequired = "Customer address is required"
//    //CustomerDuePaidVC
//    static var duePaymentIsRequired = "Due payment is required"
//
//    //Module -> Vendors
//    //VendorSearchViewController
//    static var vendorSearch = "Vendor Search"
//    //VendorDBaseVC
//    static var vendorProfile = "Vendor Profile"
//    static var totalPurchase = "Total Purchase"
//    static var currentPayable = "Current Payable"
//    static var totalPayable = "Total Payable"
//    //VendorDViewController
//    static var payableHistory = "Payable History"
//    static var purchaseHistory = "Purchase History"
//    static var returnHistory = "Return History"
//    static var payable = "Payable"
//    static var purchaseBy = "Purchased by"
//    static var totalAmount = "Total Amount"
//    static var returnedBy = "Returned by"
//    //AddNewVendorVC
//    static var vendorInfo = "Vendor Info"
//    static var vendorInfoUpdate = "Vendor Info Update"
//    //VendorPaidVC
//    static var payableWithdraw = "Payable Withdraw"
//    static var paidAmountIsGreaterThanCurrentPayable = "Paid amount is greater than current payable"
//
//    //Module -> Payable Book
//    //PayableViewController
//    static var vendors = "Vendors"
//    //AddNewPayableVC
//    static var payableUpdate = "Payable Update"
//    static var payableWithdrawUpdate = "Payable Withdraw Update"
//    static var payableWithdrawAmount = "Payable Withdraw Amount"
//    static var payableWithdrawAmountIsEmpty = "Payable withdraw amount is required"
//    static var payableAmountIsRequired = "Payable amount is required"
//    static var payableWithdrawUpdateAmountIsTooMuch = "Payable withdraw update amount is too much"
//    static var payableWithdrawnAmountIsTooMuch = "Payable withdrawn amount is too much"
//    //EditPayableVC
//    static var addressIsRequired = "Address is required"
//
//    //Module -> EmployeeBook
//    //EmployeeDBaseVC
//    static var employeeDetails = "Employee Details"
//    static var employeeName = "Employee Name"
//    static var joiningDate = "Joining Date"
//    static var totalInvoice = "Total Invoice"
//    static var totalsalaryPaid = "Total Salary Paid"
//    //EmployeeDViewController
//    static var saleHistory = "Sale History"
//    static var salaryHistory = "Salary History"
//    //NewEmployeeAddVC
//    static var employeePassword = "Employee Password"
//    static var employeeMobileNo = "Employee Mobile No"
//    static var employeeInfoUpdate = "Employee Info Update"
//    static var employeeNameIsRequired = "Employee name is required"
//    static var permissionIsRequired = "PermissionIsRequired"
//    //EmployeeSearchVC
//    static var employeeSearch = "Employee Search"
//
//
//    //Module -> Accounts
//    //Asset
//    //AssetPropertyListVC
//    static var assets = "Assets"
//    static var saleableStockAmount = "Saleable StockAmount"
//    static var properties = "Properties"
//    static var equipments = "Equipments"
//    static var goodWill = "Goodwill"
//    static var addNewProperty = "Add New Property"
//    static var addNewEquipments = "Add New Equipments"
//    static var goodWillAmount = "Goodwill amount"
//    static var goodwillAmountIsRequired = "Goodwill amount is required"
//    //PropertyNequipmentsAddVC
//    static var amountIsRequired = "Amount is required"
//    static var newCategoryNameIsRequired = "New Category name is required"
//    //Gross&NetProfitVC
//    static var grossProfit = "Gross Profit"
//    static var netProfit = "Net Profit"
//    //AccountsViewController
//    static var cashDrawer = "Cash Drawer"
//    static var loan = "Loan"
//    static var invest = "Invest"
//    static var grossAndNetProfit = "Gross & NetProfit"
//    static var cashFlowReport = "CashFlow Report"
//    static var balanceSheet = "Balance Sheet"
//    static var commingSoon = "Comming Soon"
//    static var workingOnProgress = "Working on progress"
//    static var sorryForTheInterupt = "Sorry for the interupt"
//    //CashFlowReportVC
//    static var sales = "Sales"
//    static var receivable = "Receivable"
//    static var collectionOfLoan = "Collection of Loan"
//    static var collectionOfInvest = "Collection of Invest"
//    static var totalCashReceive = "Total Cash Receive"
//    static var costOfSale = "Cost of Sale"
//    static var deliveryCommission = "Delivery Commission"
//    static var loanPaid = "Loan Paid"
//    static var payBack = "Pay Back"
//    static var totalCashPaid = "Total Cash Paid"
//    static var cashInHand = "Cash In Hand"
//    //BalanceSheetVC
//    static var searchDateIsRequired = "Search date is required"
//    static var currentAssets = "Current Assets"
//    static var fixedAssets = "Fixed Assets"
//    static var liabilities = "Liabilities"
//    static var investment = "Investment"
//    static var totalAssets = "Total Assets"
//    static var totalLiabilities = "Total Liabilities"
//    static var totalInvestment = "Total Investment"
//    //CashDrawer
//    static var enterCashAmountOfYourDrawer = "Enter cash amount of your drawer"
//    static var cashIn = "Cash In"
//    static var cashOut = "Cash Out"
//    //CashTransactionVC
//    static var transactionUpdate = "Transaction Update"
//    static var previousCashAmount = "Previous Cash Amount"
//    static var newCashAmount = "New Cash Amount"
//    static var enterCashAmount = "Enter Cash Amount"
//    //LoanerListVC
//    static var initialLoan = "Initial Loan"
//    static var interestAmount = "Interest Amount"
//    static var deadline = "Deadline"
//    //LoanerDetailsVC
//    static var loanerDetails = "Loaner Details"
//    //LoanerTransactionHistoryVC
//    static var loanTransactions = "Loan Transactions"
//    //NewLoanAddVC
//    static var newLoan = "New Loan"
//    static var loanAmount = "Loan Amount"
//    static var loanDeadline = "Loan Deadline"
//    static var loanerSelection = "Loaner Selection"
//    static var newLoanerAdd = "New Loaner Add"
//    static var loanerName = "Loaner Name"
//    static var loanerPhone = "Loaner Phone"
//    static var loanerAddress = "Loaner Address"
//    static var loanerSelectionIsRequired = "Loaner selection is required"
//    static var loanerAmountIsRequired = "Loaner amount is required"
//    static var interestAmountIsRequired = "Interest amount is required"
//    static var loanerNameIsRequired = "Loaner name is required"
//    //LoanPaidVC
//    static var currentLoan = "Current Loan"
//    static var loanWithdrawAmount = "Loan Withdraw Amount"
//    static var loanWithdrawAmountIsRequired = "Loan Withdraw amount is required"
//    //LoanerUpdateVC
//    static var loanerUpdate = "Loaner Update"
//    //LoanerTransactionUpdateVC
//    static var withdrawUpdatedAmountIsRequired = "Withdraw updated amount is required"
//    //LoanerSearchVC
//    static var loanerSearch = "Loaner Search"
//    //LoanerTransactionLoanUpdateVC
//    static var editedLoanAmount = "Edited Loan Amount"
//    static var editedInterestAmount = "Edited Interest Amount"
//    static var editedLoanAmountIsRequired = "Edited loan amount is required"
//    static var editedInterestAmountRequired = "Edited interest amount required"
//    static var loanDeadlineIsRequired = "Loan deadline is required"
//    //InvestorsListVC
//    static var investor = "Investor"
//    //InvestorDetailsVC
//    static var investorDetails = "Investor Details"
//    //InvestmentTransactionHistoryVC
//    static var investmentTransactions = "Investment Transactions"
//    //NewInvestmentAddVC
//    static var newInvestment = "New Investment"
//    static var investorSelection = "Investor Selection"
//    static var investAmount = "Invest Amount"
//    static var newInvestorAdd = "New Investor Add"
//    static var investorName = "Investor Name"
//    static var investorPhone = "Investor Phone"
//    static var investorAddress = "Investor Address"
//    static var investorSelectionIsRequired = "Investor selection is required"
//    static var investorAmountIsRequired = "Investor amount is required"
//    static var investorNameIsRequired = "Investor name is required"
//    //InvestmentProfitWithdrawVC
//    static var investmentWithdrawAmountIsRequired = "Investment Withdraw amount is required"
//    //InvestmentTransactionUpdateVC
//    static var editedInvestedAmount = "Edited Invested Amount"
//    static var editedInvestmentAmountIsRequired = "Edited investment amount is required"
//    //InvestorSearchVC
//    static var investorSearch = "Investor Search"
//    //InvestorUpdateVC
//    static var investorUpdate = "Investor Update"
//
//    //Contact
//    static var call = "Call"
//    static var ponnoTitle = "Ponno"
//    static var ponnoAddress = "House : 53(2nd Floor), Road: 20, Nikunja-2, Dhaka-1229"
//    static var ponnoMobile = "Mobile: 01999088672, 01999088654"
//
//    //Shop
//    //SegmentedVC
//    static var shopInformation = "Shop Information"
//    static var shopSettings = "Shop Settings"
//    static var paymentReferrals = "Payment & Referrals"
//    //FirstViewController
//    static var shopName = "Shop name"
//    static var ownerName = "Owner name"
//    static var registrationNumber = "Registration number"
//    static var shopPhoneNumber = "Shop Phone number"
//    static var shopAddress = "Shop address"
//    static var shopNameIsRequired = "Shop name is required"
//    static var ownerNameIsRequired = "Owner name is required"
//    //SecondViewController
//    static var purchaseBase = "Purchase Base"
//    static var productionBase = "ProductionBase"
//    static var languageLbl = "ভাষা"
//    static var invoiceNote = "রসিদ নোট"
//    static var englishTitle = "ইংরেজি"
//    static var banglaTitle = "বাংলা"
//    static var clearShopData = "দোকানের ডাটা মুছুন"
//    //ShopInfoUpdateVc
//    static var generalInfo = "General Info"
//    static var logInInfo = "Log-In Info"
//    //OwnerLogInInfoUpdate
//    static var loginInformation = "Login Information"
//    static var oldPassword = "Old password"
//    static var newPassword = "New password"
//    static var confirmPassword = "Confirm password"
//    static var oldPasswordIsRequired = "Old password is required"
//    static var newPasswordIsRequired = "New password is required"
//    static var confirmPasswordIsRequired = "Confirm password is required"
//    //ThirdViewController
//    static var referrals = "Referrals"
//    static var referralsHistory = "Referrals History"
//    //ReferralInfoViewController
//    static var referred = "Referred"
//    static var pending = "Pending"
//    static var expired = "Expired"
//    static var totalIncome = "Total Income"
//    static var totalReceived = "Total Received"
//    static var currentDue = "Current Due"
//    //ReferralDetailsViewController
//    static var newReferral = "New Referral"
//    //ReferralAddViewController
//    static var shopCategoryIsRequired = "Shop category is required"
//
    
}

