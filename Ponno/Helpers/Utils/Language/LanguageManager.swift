//
//  LanguageManager.swift
//  Ponno
//
//  Created by a k azad on 26/8/19.
//  Copyright © 2019 Ponno. All rights reserved.
//


import Foundation

enum Language : String{
    case English
    case Bangla
}

public class LanguageManager : NSObject{
    static let sharedInstance = LanguageManager()
    
    //Common
    public static var Warning = ""
    public static var Congratulations = ""
    public static var AreYouSure = ""
    public static var Cancel = ""
    public static var Successful = ""
    public static var NoInformationFound = ""
    public static var NoCategoryFound = ""
    //public static var NoTodaysSummaryFound = ""
    public static var Delete = ""
    public static var Yes = ""
    public static var No = ""
    public static var ProductSearch = ""
    public static var SearchFilter = ""
    
    
    //Module -> Login
    public static var Logout = ""
    public static var Login = ""
    public static var Register = ""
    public static var PhoneNumberIsIncorrect = ""
    public static var PhoneNumberIsRequired = ""
    public static var PasswordIsRequired = ""
    public static var RepeatPasswordIsRequired = ""
    public static var Registration = ""
    public static var NameIsRequired = ""
    public static var PasswordDidNotMatch = ""
    public static var MobileNumber = ""
    public static var Password = ""
    public static var RepeatPassword = ""
    public static var SixToTwenty = ""
    public static var DontHaveAnAccount = ""
    public static var AlreadyHaveAnAccount = ""
    public static var Name = ""
    public static var Phone = ""
    public static var ShopCategory = ""
    
    //Home
    public static var Dashboard = ""
    public static var ExistingDataWillBeRemoved = ""
    public static var Sale = ""
    public static var Purchase = ""
    public static var Expense = ""
    public static var NewInventory = ""
    public static var NewDue = ""
    public static var NewPayable = ""
    public static var NewEmployee = ""
    public static var NewCustomer = ""
    public static var NewDelivery = ""
    public static var NewVendor = ""
    public static var DummyDataButton = ""
    
    //SideMenu
    public static var TodaysSummary = ""
    public static var SaleBook = ""
    public static var DeliveryBook = ""
    public static var DueBook = ""
    public static var CustomerBook = ""
    public static var Inventory = ""
    public static var ExpenseBook = ""
    public static var EmployeeBook = ""
    public static var PurchaseBook = ""
    public static var RawMaterial = ""
    public static var PreOrder = ""
    public static var PayableBook = ""
    public static var VendorBook = ""
    public static var Accounts = ""
    public static var AccountsBook = ""
    public static var Contact = ""
    
    //Mark: Module -> TodaysSummary
    public static var TodaysSale = ""
    public static var OtherCalculations = ""
    
    //Module -> Sale
    //SaleListViewController
    public static var SelectStartDate = ""
    public static var StartingDateIsRequired = ""
    public static var EndDateIsRequired = ""
    public static var SelectEndDate = ""
    public static var SelectDate = ""
    public static var StartDate = ""
    public static var EndDate = ""
    public static var TotalDue = ""
    public static var TotalSale = ""
    public static var TotalSaleAmount = ""
    //SalePerDayListViewController
    public static var Total = ""
    public static var Due = ""
    public static var DeliverySystem = ""
    public static var Paid = ""
    public static var Unpaid = ""
    //SaleDetailsViewController
    public static var SalesInvoice = ""
    public static var Invoice = ""
    public static var InvoiceReceive = ""
    public static var Date = ""
    public static var DeliveryDate = ""
    public static var Time = ""
    public static var Discount = ""
    public static var PaymentMethod = ""
    public static var PaymentNumber = ""
    public static var PaidAmount = ""
    public static var DeliveryCharge = ""
    public static var CustomerName = ""
    public static var Address = ""
    public static var Quantity = ""
    public static var PricePerUnit = ""
    public static var DeliveryStatus = ""
    public static var Delivered = ""
    public static var DeliverySystemDue = ""
    public static var InvoiceDue = ""
    public static var LatestDue = ""
    public static var DuePaid = ""
    public static var RemainingDue = ""
    public static var OthersDue = ""
    public static var OthersDuePaid = ""
    public static var Data = ""
    public static var ProductReturn = ""
    public static var Return = ""
    //AddNewSaleViewController
    public static var TheProductIsAlreadySelectedDeselectTheProductForUpdate = ""
    public static var InsufficientQuantity = ""
    public static var SaveAsDraft = ""
    public static var UpdateDraft = ""
    public static var Continue = ""
    public static var Complete = ""
    public static var YouCannotSaleThisProduct = ""
    public static var Discard = ""
    public static var Next = ""
    public static var Submit = ""
    public static var Refresh = ""
    public static var Draft = ""
    //SaleAddSecondViewController
    public static var One = ""
    public static var QuantityIsRequired = ""
    public static var SellingPriceIsRequired = ""
    //SaleAddSerialViewController
    public static var ProductSerialIsRequired = ""
    //SaleInfoViewController
    public static var Amount = ""
    public static var Customer = ""
    public static var PayableAmount = ""
    public static var Cash = ""
    public static var CashBack = ""
    public static var BkashPaymentPhoneNumberIsRequired = ""
    public static var RocketPaymentPhoneNumberIsRequired = ""
    public static var AccountNumberIsRequired = ""
    public static var CashIsRequired = ""
    public static var CustomerIsRequired = ""
    public static var BkashMobileNumber = ""
    public static var AccountNumber = ""
    public static var Account = ""
    public static var RocketMobileNumber = ""
    public static var Delivery = ""
    public static var YouCanNotCashOutMoreThan = ""
    public static var YouCanNotUpdateMoreThan = ""
    public static var YouCanNotUpdateLessThan = ""
    public static var YouCanNotPayMoreThan = ""
    public static var WalletPayExceedsPayableAmount = ""
    public static var WalletAmount = ""
    public static var WalletPay = ""
    //SaleProductReturnViewController
    public static var SearchSaleDate = ""
    //InvoiceReturnViewController
    public static var ProductName = ""
    public static var ReturnQuantity = ""
    public static var ProductIsRequired = ""
    public static var YouCantRetrunMoreThan = ""
    public static var CashBackIsRequired = ""
    public static var ReturnRelatedInfo = ""
    //SaleOnDeliveryViewController
    public static var DeliverySystemAdd = ""
    public static var DeliverySystemIsRequired = ""
    public static var DeliveryChargeIsRequired = ""
    public static var NewDeliverySystemAdd = ""
    public static var PhoneNumber = ""
    //SaleWithDiscountViewController
    public static var DiscountIsRequired = ""
    //SaleWithCustomerViewController
    public static var CustomerAdd = ""
    public static var CustomerPhone = ""
    //SalesCustomerSearchViewController
    public static var SearchCustomer = ""
    //SalePerDaySearchViewController
    public static var SearchSerial = ""
    //SaleListPopOverVC
    public static var SaleDraft = ""
    public static var SalePreOrder = ""
    
    //Product
    //ProductListViewController
    //public static var Inventory = ""
    public static var DamagedProduct = ""
    public static var Varient = ""
    public static var Category = ""
    public static var SellingPrice = ""
    public static var CategoryAndSubCategory = ""
    //ProductDetailsBaseViewController
    public static var ProductDetails = ""
    public static var Company = ""
    public static var ProductCode = ""
    public static var PurchasePrice = ""
    public static var QuantityAlert = ""
    //ProductDViewController
    public static var RecentStock = ""
    public static var RecentSale = ""
    public static var SerialNo = ""
    public static var StockAvailable = ""
    public static var VendorName = ""
    public static var Warranty = ""
    public static var ExpireDate = ""
    public static var UnSold = ""
    public static var Sold = ""
    public static var Refund = ""
    public static var Update = ""
    public static var TheStockIsEmpty = ""
    public static var SerialUpdate = ""
    //ProductCategoryViewController
    public static var SelectCategory = ""
    public static var NewCategoryAdd = ""
    public static var NewCategory = ""
    public static var CategoryName = ""
    public static var CategoryIsRequired = ""
    public static var CategoryAlreadyExists = ""
    public static var ParentCategory = ""
    //ProductSubCategoryVC
    public static var NewSubCategoryAdd = ""
    public static var SubCategoryName = ""
    public static var SubCategory = ""
    public static var SubCategoryNameIsRequired = ""
    public static var SubCategoryIsAlreadyExists = ""
    //AddNewProductVC
    public static var Unit = ""
    public static var NewProduct = ""
    public static var Pc = ""
    public static var ProductNameIsRequired = ""
    public static var ProductCategoryIsRequired = ""
    public static var ProductUnitIsRequired = ""
    //ProductAddThirdVC
    public static var SerialNumber = ""
    public static var QuantityAlertWhenSmallerThan = ""
    public static var UseCommaToSeparateSerial = ""
    public static var NewVendorAdd = ""
    public static var VendorMobileNumber = ""
    public static var VendorNameIsRequired = ""
    public static var VendorMobileNumberIsRequired = ""
    public static var Month = ""
    public static var BuyingPrice = ""
    public static var SelectOne = ""
    public static var Vendor = ""
    //RecentStockReturnViewController
    public static var ProductInfo = ""
    public static var SelectSerialNumber = ""
    public static var SelectAVendorToReturnProduct = ""
    public static var BuyingPriceIsRequired = ""
    //RecentStockUpdateViewController
    public static var StockUpdate = ""
    //DamagedProductListVC
    public static var DamagedProducts = ""
    public static var Damaged = ""
    public static var Times = ""
    public static var TotalDamages = ""
    public static var TotalDamageAmount = ""
    //DamagedProductSearchVC
    public static var DamagedProductSearch = ""
    //DamagedPerProductVC
    public static var DamagedQuantity = ""
    public static var Description = ""
    public static var Close = ""
    public static var YesDelete = ""
    //DamageableProductList
    public static var ToDamageSelectProduct = ""
    //DamageableProductBaseInfoVC
    public static var QuantityCantBeGreaterThan = ""
    public static var BuyingpriceCantBeGreaterThan = ""
    public static var ReturnPriceIsRequired = ""
    //ModuleProductCategory
    public static var TreeView = ""
    public static var CategoryUpdate = ""
    public static var ParentCategoryIsRequired = ""
    
    
    //Module -> Raw Material
    //RawMaterialListVC
    public static var Production = ""
    public static var NewRawMaterial = ""
    public static var DamagedRawMaterial = ""
    //RawMaterialSearchVC
    public static var RawMaterialSearch = ""
    //RawMaterialDetailsBaseVC
    public static var RawMaterialInfo = ""
    public static var MaterialSku = ""
    public static var CurrentStock = ""
    public static var CurrentStockAmount = ""
    //RawMaterialDetailsVC
    public static var RecentMaterialStock = ""
    public static var RecentUse = ""
    public static var Available = ""
    public static var StockAmount = ""
    public static var UsedQuantity = ""
    //RawMaterialRecentStockReturnVC
    public static var RawMaterialName = ""
    public static var PricecannotBe0OrLess = ""
    //ProductionRawMaterialListVC
    public static var SelectRawMaterial = ""
    public static var SelectARawMaterialForProduction = ""
    public static var TheRawMaterialIsAlreadySelectedDeselectItThenUpdate = ""
    //ProductionRawMaterialAccessoryInfoVC
    public static var Product = ""
    public static var SelectProduct = ""
    public static var ProductionCost = ""
    public static var TotalExpense = ""
    //PurchaseableRawMaterialListVC
    public static var SelectARawMaterialToPurchase = ""
    //PurchaseReturnRawMaterialListVC
    public static var SelectARawMaterialToReturn = ""
    //PurchaseReturnRawMaterialBaseInfoVC
    public static var RawMaterialReturn = ""
    public static var ReturnPrice = ""
    public static var CashBackPriceIsRequired = ""
    public static var CashbackPriceCantBeGreaterThan = ""
    //PurchaseReturnRawMaterialAccessoryInfoVC
    public static var TotalPrice = ""
    //RawMaterialPurchaseListVC
    public static var RawMaterialPurchaseBook = ""
    //RawMaterialPurchaseDetailsVC
    public static var MaterialPurchaseInvoice = ""
    public static var InvoicePayable = ""
    public static var LatestPayable = ""
    public static var PayablePaid = ""
    public static var RemainingPayable = ""
    public static var OthersPayable = ""
    //DamagedRawMaterialSearchVC
    public static var DamagedRawMaterialSearch = ""
    //DamageableRawMaterialListVC
    public static var SelectRawMaterialToDamage = ""
    //RawMaterialUpdateViewController
    public static var RawMaterialUnitRequired = ""
    //AddRawMaterialBaseInfoVC
    public static var RawMaterialCode = ""
    public static var RawMaterialNameIsRequired = ""
    //PurchaseableRawMaterialAccessoryInfoVC
    public static var PurchaseDate = ""
    public static var SaleDate = ""
    
    //Module -> Purchase
    //PurchaseDetailsVC
    public static var PurchaseInvoice = ""
    //PurchaseReturnDetailsVC
    public static var ReturnInvoice = ""
    //PurchaseableProductListVC
    public static var SelectAProductToPurchase = ""
    //PurchaseAddVC
    public static var NewPurchase = ""
    public static var PurchaseRelatedInfo = ""
    //RefundableProductListVC
    public static var ToReturnSelectAProduct = ""
    //RefundAddSerialVC
    public static var SerialNumberIsRequired = ""
    //PreOrderListVC
    public static var Received = ""
    public static var OrderDate = ""
    public static var ReceiveDate = ""
    public static var AdvanceAmount = ""
    public static var AmountToBePaid = ""
    public static var NoRemarks = ""
    //PreOrderableProductVC
    public static var ToOrderSelectAProduct = ""
    //PreOrderAddVC
    public static var NewPreOrder = ""
    public static var AddMoreProduct = ""
    //PreOrderAddInfoVC
    public static var Remarks = ""
    public static var SelectReceiveDate = ""
    //PreOrderPendingVC
    public static var Receive = ""
    public static var ChangeAdvance = ""
    public static var AdvanceAmountIsRequired = ""
    public static var AdvanceAmountsNeedToBeModified = ""
    public static var PaidAmountIsRequired = ""
    //PerOrderEditProductList
    public static var SelectProductsForPreOrder = ""
    
    //Module -> Expense
    //ExpenseViewController
    public static var TotalExpenseAmount = ""
    //AddNewExpenseVC
    public static var Employee = ""
    public static var ExpenseAmount = ""
    public static var UpdateInfo = ""
    public static var NewExpense = ""
    public static var ExpenseTypeIsRequired = ""
    public static var ExpenseType = ""
    public static var NewExpenseAdd = ""
    public static var ExpenseDate = ""
    public static var SelectAnItemToSearch = ""
    
    //Module -> Due
    //DueViewController
    public static var DueCustomer = ""
    //AddNewDueVC
    public static var DueUpdate = ""
    public static var WithdrawUpdate = ""
    public static var WithdrawnUpdate = ""
    public static var CollectionAmount = ""
    
    //Module -> Delivery
    //DeliveryViewController
    public static var DeliveryLog = ""
    public static var DeliveryHistory = ""
    public static var Withdraw = ""
    public static var DeliveryNameIsRequired = ""
    //AddNewDeliveryDueVC
    public static var DueAmount = ""
    public static var DueWithdraw = ""
    public static var DueWithdrawAmount = ""
    public static var WithdrawAmountIsTooMuch = ""
    //DeliverySystemSearchVC
    public static var DeliverySystemSearch = ""
    //DeliveryLogSearchVC
    public static var From = ""
    public static var To = ""
    public static var Search = ""
    //AddNewDeliverySystemVC
    public static var NewDeliverySystemInfo = ""
    //DeliveryDuePaidVC
    public static var Commission = ""
    public static var NewDueIsRequired = ""
    public static var CommissionUnitIsRequired = ""
    public static var OneHundred = ""
    //DBaseViewController
    public static var DeliverySystemProfile = ""
    public static var TotalOrder = ""
    public static var CurrentOrder = ""
    public static var TotalDuePaid = ""
    //DDetailsViewController
    public static var By = ""
    public static var TotalPaid = ""
    public static var DueHistory = ""
    
    //Module -> Customer
    //CustomerSearchVC
    public static var CustomerSearch = ""
    //CustomerDBaseVC
    public static var CustomerDetails = ""
    public static var CustomerWallet = ""
    public static var DueAdjust = ""
    public static var DueAdjustUpdate = ""
    public static var NewDueAdjust = ""
    public static var DueAdjustAmount = ""
    public static var AdjustedBy = ""
    public static var DueAdjustAmountIsRequired = ""
    public static var YouCanNotAdjustMoreThan = ""
    //CustomerAddVC
    public static var NewCustomerAdd = ""
    public static var UpdateCustomerInfo = ""
    public static var CustomerDueUpdate = ""
    public static var CustomerAddressIsRequired = ""
    //CustomerDuePaidVC
    public static var DuePaymentIsRequired = ""
    
    //Module -> Vendors
    //VendorSearchViewController
    public static var VendorSearch = ""
    //VendorDBaseVC
    public static var VendorProfile = ""
    public static var TotalPurchase = ""
    public static var CurrentPayable = ""
    public static var TotalPayable = ""
    public static var VendorWallet = ""
    public static var PayableAdjust = ""
    public static var PayableAdjustUpdate = ""
    public static var NewPayableAdjust = ""
    public static var PayableAdjustAmount = ""
    public static var PayableAdjustAmountIsRequired = ""
    //VendorDViewController
    public static var PayableHistory = ""
    public static var PurchaseHistory = ""
    public static var ReturnHistory = ""
    public static var Payable = ""
    public static var PurchaseBy = ""
    public static var TotalAmount = ""
    public static var ReturnedBy = ""
    //AddNewVendorVC
    public static var VendorInfo = ""
    public static var VendorInfoUpdate = ""
    //VendorPaidVC
    public static var PayableWithdraw = ""
    public static var PaidAmountIsGreaterThanCurrentPayable = ""
    
    //Module -> Payable Book
    //PayableViewController
    public static var Vendors = ""
    //AddNewPayableVC
    public static var PayableUpdate = ""
    public static var PayableWithdrawUpdate = ""
    public static var PayableWithdrawAmount = ""
    public static var PayableWithdrawAmountIsEmpty = ""
    public static var PayableAmountIsRequired = ""
    public static var PayableWithdrawUpdateAmountIsTooMuch = ""
    public static var PayableWithdrawnAmountIsTooMuch = ""
    //EditPayableVC
    public static var AddressIsRequired = ""
    
    //Module -> EmployeeBook
    //EmployeeDBaseVC
    public static var EmployeeDetails = ""
    public static var EmployeeName = ""
    public static var JoiningDate = ""
    public static var TotalInvoice = ""
    public static var TotalsalaryPaid = ""
    public static var AccountStatus = ""
    public static var PaidSalary = ""
    public static var UnpaidSalary = ""
    public static var Active = ""
    public static var Inactive = ""
    public static var PaidSalaryIsTooMuch = ""
    //EmployeeDViewController
    public static var SaleHistory = ""
    public static var SalaryHistory = ""
    //NewEmployeeAddVC
    public static var EmployeePassword = ""
    public static var EmployeeMobileNo = ""
    public static var EmployeeInfoUpdate = ""
    public static var EmployeeNameIsRequired = ""
    public static var PermissionIsRequired = ""
    public static var Permission = ""
    //EmployeeSearchVC
    public static var EmployeeSearch = ""
    //EmployeeDBasePopOverVC
    public static var Salary = ""
    public static var Payment = ""
    //EmployeeSalaryAdd
    public static var Status = ""
    public static var SalaryUpdate = ""
    public static var UnpaidSalaryAmount = ""
    
    //Module->Accounts
    //Asset
    //AssetPropertyListVC
    public static var Assets = ""
    public static var SaleableStockAmount = ""
    public static var Properties = ""
    public static var Equipments = ""
    public static var GoodWill = ""
    public static var AddNewProperty = ""
    public static var AddNewEquipments = ""
    public static var GoodWillAmount = ""
    public static var GoodwillAmountIsRequired = ""
    //PropertyNequipmentsAddVC
    public static var AmountIsRequired = ""
    public static var NewCategoryNameIsRequired = ""
    //Gross&NetProfitVC
    public static var GrossProfit = ""
    public static var NetProfit = ""
    //AccountsViewController
    public static var CashDrawer = ""
    public static var Loan = ""
    public static var Invest = ""
    public static var GrossAndNetProfit = ""
    public static var CashFlowReport = ""
    public static var BalanceSheet = ""
    public static var CommingSoon = ""
    public static var WorkingOnProgress = ""
    public static var SorryForTheInterupt = ""
    //CashFlowReportVC
    public static var Sales = ""
    public static var Receivable = ""
    public static var CollectionOfLoan = ""
    public static var CollectionOfInvest = ""
    public static var TotalCashReceive = ""
    public static var CostOfSale = ""
    public static var DeliveryCommission = ""
    public static var LoanPaid = ""
    public static var PayBack = ""
    public static var TotalCashPaid = ""
    public static var CashInHand = ""
    //BalanceSheetVC
    public static var SearchDateIsRequired = ""
    public static var CurrentAssets = ""
    public static var FixedAssets = ""
    public static var Liabilities = ""
    public static var Investment = ""
    public static var TotalAssets = ""
    public static var TotalLiabilities = ""
    public static var TotalInvestment = ""
    //CashDrawer
    public static var EnterCashAmountOfYourDrawer = ""
    public static var CashIn = ""
    public static var CashOut = ""
    public static var CashInToVendorWallet = ""
    public static var CashInToCustomerWallet = ""
    //CashTransactionVC
    public static var TransactionUpdate = ""
    public static var PreviousCashAmount = ""
    public static var NewCashAmount = ""
    public static var EnterCashAmount = ""
    //LoanerListVC
    public static var InitialLoan = ""
    public static var InterestAmount = ""
    public static var Deadline = ""
    //LoanerDetailsVC
    public static var LoanerDetails = ""
    public static var TotalLoan = ""
    //LoanerTransactionHistoryVC
    public static var LoanTransactions = ""
    //NewLoanAddVC
    public static var NewLoan = ""
    public static var LoanAmount = ""
    public static var LoanDeadline = ""
    public static var LoanerSelection = ""
    public static var NewLoanerAdd = ""
    public static var LoanerName = ""
    public static var LoanerPhone = ""
    public static var LoanerAddress = ""
    public static var LoanerSelectionIsRequired = ""
    public static var LoanerAmountIsRequired = ""
    public static var InterestAmountIsRequired = ""
    public static var LoanerNameIsRequired = ""
    //LoanPaidVC
    public static var CurrentLoan = ""
    public static var LoanWithdrawAmount = ""
    public static var LoanWithdrawAmountIsRequired = ""
    //LoanerUpdateVC
    public static var LoanerUpdate = ""
    //LoanerTransactionUpdateVC
    public static var WithdrawUpdatedAmountIsRequired = ""
    //LoanerTransactionLoanUpdateVC
    public static var EditedLoanAmount = ""
    public static var EditedInterestAmount = ""
    public static var EditedLoanAmountIsRequired = ""
    public static var EditedInterestAmountRequired = ""
    public static var LoanDeadlineIsRequired = ""
    //InvestorsListVC
    public static var Investor = ""
    //InvestorDetailsVC
    public static var InvestorDetails = ""
    //InvestmentTransactionHistoryVC
    public static var InvestmentTransactions = ""
    //NewInvestmentAddVC
    public static var NewInvestment = ""
    public static var InvestorSelection = ""
    public static var InvestAmount = ""
    public static var NewInvestorAdd = ""
    public static var InvestorName = ""
    public static var InvestorPhone = ""
    public static var InvestorAddress = ""
    public static var InvestorSelectionIsRequired = ""
    public static var InvestorAmountIsRequired = ""
    public static var InvestorNameIsRequired = ""
    //InvestmentProfitWithdrawVC
    public static var InvestmentWithdrawAmountIsRequired = ""
    public static var InvestmentWithdrawInfo = ""
    public static var CurrentInvestment = ""
    public static var InvestmentWithdrawAmount = ""
    //InvestmentTransactionUpdateVC
    public static var EditedInvestedAmount = ""
    public static var EditedInvestmentAmountIsRequired = ""
    //InvestorSearchVC
    public static var InvestorSearch = ""
    //InvestorUpdateVC
    public static var InvestorUpdate = ""
    //MobileBankingListVC
    public static var MobileBankingName = ""
    public static var MobileBankIsRequired = ""
    public static var MobileBanking = ""
    
    //Contact
    public static var Call = ""
    public static var PonnoTitle = ""
    public static var PonnoAddress = ""
    public static var PonnoMobile = ""
    
    //Shop
    //SegmentedVC
    public static var ShopInformation = ""
    public static var ShopSettings = ""
    public static var PaymentAndReferrals = ""
    public static var Promotion = ""
    //FirstViewController
    public static var ShopName = ""
    public static var OwnerName = ""
    public static var RegistrationNumber = ""
    public static var ShopPhoneNumber = ""
    public static var ShopAddress = ""
    public static var ShopNameIsRequired = ""
    public static var OwnerNameIsRequired = ""
    //SecondViewController
    public static var PurchaseBase = ""
    public static var ProductionBase = ""
    public static var LanguageLbl = ""
    public static var EnglishTitle = ""
    public static var BanglaTitle = ""
    public static var InvoiceNote = ""
    public static var ClearShopData = ""
    public static var LanguageTitle = ""
    public static var PurchaseBaseOrProductionBaseIsRequired = ""
    //ShopInfoUpdateVc
    public static var GeneralInfo = ""
    public static var LogInInfo = ""
    //OwnerLoginInfoUpdateVC
    public static var LoginInformation = ""
    public static var OldPassword = ""
    public static var NewPassword = ""
    public static var ConfirmPassword = ""
    public static var OldPasswordIsRequired = ""
    public static var NewPasswordIsRequired = ""
    public static var ConfirmPasswordIsRequired = ""
    //ThirdViewController
    public static var Referrals = ""
    public static var ReferralsHistory = ""
    //ReferralInfoViewController
    public static var Referred = ""
    public static var Pending = ""
    public static var Expired = ""
    public static var TotalIncome = ""
    public static var TotalReceived = ""
    public static var CurrentDue = ""
    //ReferralDetailsViewController
    public static var NewReferral = ""
    //ReferralAddViewController
    public static var ShopCategoryIsRequired = ""
    
    
    
    static var language : Language = .Bangla
    
    override init() {
        switch LanguageManager.language {
        case .Bangla:
            //Common
            LanguageManager.Warning = BanglaLang.warning
            LanguageManager.Congratulations = BanglaLang.congratulations
            LanguageManager.AreYouSure = BanglaLang.areYouSure
            LanguageManager.Cancel = BanglaLang.cancel
            LanguageManager.Successful = BanglaLang.successful
            LanguageManager.NoInformationFound = BanglaLang.noInformationFound
            LanguageManager.NoCategoryFound = BanglaLang.noCategoryFound
            LanguageManager.Delete = BanglaLang.delete
            LanguageManager.Yes = BanglaLang.yes
            LanguageManager.No = BanglaLang.no
            LanguageManager.ProductSearch = BanglaLang.productSearch
            LanguageManager.SearchFilter = BanglaLang.searchFilter
            
            //Module -> Login
            LanguageManager.Login = BanglaLang.login
            LanguageManager.Logout = BanglaLang.logout
            LanguageManager.Register = BanglaLang.register
            LanguageManager.PhoneNumberIsRequired = BanglaLang.phoneNumberIsRequired
            LanguageManager.PhoneNumberIsIncorrect = BanglaLang.phoneNumberIsIncorrect
            LanguageManager.NameIsRequired = BanglaLang.nameIsRequired
            LanguageManager.PasswordIsRequired = BanglaLang.passwordIsRequired
            LanguageManager.RepeatPasswordIsRequired = BanglaLang.repeatPasswordIsRequired
            LanguageManager.PasswordDidNotMatch = BanglaLang.passwordDidNotMatch
            LanguageManager.MobileNumber = BanglaLang.mobileNumber
            LanguageManager.Password = BanglaLang.password
            LanguageManager.SixToTwenty = BanglaLang.sixToTwenty
            LanguageManager.DontHaveAnAccount = BanglaLang.dontHaveAnAccount
            LanguageManager.AlreadyHaveAnAccount = BanglaLang.alreadyHaveAnAccount
            LanguageManager.Name = BanglaLang.name
            LanguageManager.Phone = BanglaLang.phone
            LanguageManager.RepeatPassword = BanglaLang.repeatPassword
            LanguageManager.ShopCategory = BanglaLang.shopCategory
            LanguageManager.Registration = BanglaLang.registration
            
            //Home
            LanguageManager.Dashboard = BanglaLang.dashboard
            LanguageManager.ExistingDataWillBeRemoved = BanglaLang.existingDataWillBeRemoved
            LanguageManager.Sale = BanglaLang.sale
            LanguageManager.Purchase = BanglaLang.purchase
            LanguageManager.Expense = BanglaLang.expense
            LanguageManager.NewInventory = BanglaLang.newInventory
            LanguageManager.NewDue = BanglaLang.newDue
            LanguageManager.NewPayable = BanglaLang.newPayable
            LanguageManager.NewEmployee = BanglaLang.newEmployee
            LanguageManager.NewCustomer = BanglaLang.newCustomer
            LanguageManager.NewDelivery = BanglaLang.newDelivery
            LanguageManager.NewVendor = BanglaLang.newVendor
            LanguageManager.DummyDataButton = BanglaLang.dummyDataButton
            
            //SideMenu
            LanguageManager.TodaysSummary = BanglaLang.todaysSummary
            LanguageManager.SaleBook = BanglaLang.saleBook
            LanguageManager.DeliveryBook = BanglaLang.deliveryBook
            LanguageManager.DueBook = BanglaLang.dueBook
            LanguageManager.CustomerBook = BanglaLang.customerBook
            LanguageManager.Inventory = BanglaLang.inventory
            LanguageManager.ExpenseBook = BanglaLang.expenseBook
            LanguageManager.EmployeeBook = BanglaLang.employeeBook
            LanguageManager.PurchaseBook = BanglaLang.purchaseBook
            LanguageManager.RawMaterial = BanglaLang.rawMaterial
            LanguageManager.PreOrder = BanglaLang.preOrder
            LanguageManager.PayableBook = BanglaLang.payableBook
            LanguageManager.VendorBook = BanglaLang.vendorBook
            LanguageManager.Accounts = BanglaLang.accounts
            LanguageManager.AccountsBook = BanglaLang.accountsBook
            LanguageManager.Contact = BanglaLang.contact
            
            //Mark: TodaysSummary
            LanguageManager.TodaysSale = BanglaLang.todaysSale
            LanguageManager.OtherCalculations = BanglaLang.otherCalculations
            
            //Sale
            //SalesListViewController
            LanguageManager.SelectStartDate = BanglaLang.selectStartDate
            LanguageManager.SelectEndDate = BanglaLang.selectEndDate
            LanguageManager.SelectDate = BanglaLang.selectDate
            LanguageManager.StartingDateIsRequired = BanglaLang.startingDateIsRequired
            LanguageManager.EndDateIsRequired = BanglaLang.endDateIsRequired
            LanguageManager.StartDate = BanglaLang.startDate
            LanguageManager.EndDate = BanglaLang.endDate
            LanguageManager.TotalSale = BanglaLang.totalSale
            LanguageManager.TotalSaleAmount = BanglaLang.totalSaleAmount
            LanguageManager.TotalDue = BanglaLang.totalDue
            //SalePerDayListViewController
            LanguageManager.Total = BanglaLang.total
            LanguageManager.Due = BanglaLang.due
            LanguageManager.DeliverySystem = BanglaLang.deliverySystem
            LanguageManager.Paid = BanglaLang.paid
            LanguageManager.Unpaid = BanglaLang.unpaid
            //SaleDetailsViewController
            LanguageManager.SalesInvoice = BanglaLang.salesInvoice
            LanguageManager.Invoice = BanglaLang.invoice
            LanguageManager.InvoiceReceive = BanglaLang.invoiceReceive
            LanguageManager.Date = BanglaLang.date
            LanguageManager.DeliveryDate = BanglaLang.deliveryDate
            LanguageManager.Time = BanglaLang.time
            LanguageManager.Discount = BanglaLang.discount
            LanguageManager.PaymentMethod = BanglaLang.paymentMethod
            LanguageManager.PaymentNumber = BanglaLang.paymentNumber
            LanguageManager.PaidAmount = BanglaLang.paidAmount
            LanguageManager.DeliveryCharge = BanglaLang.deliveryCharge
            LanguageManager.CustomerName = BanglaLang.customerName
            LanguageManager.Address = BanglaLang.address
            LanguageManager.Quantity = BanglaLang.quantity
            LanguageManager.PricePerUnit = BanglaLang.pricePerUnit
            LanguageManager.DeliveryStatus = BanglaLang.deliveryStatus
            LanguageManager.Delivered = BanglaLang.delivered
            LanguageManager.DeliverySystemDue = BanglaLang.deliverySystemDue
            LanguageManager.InvoiceDue = BanglaLang.invoiceDue
            LanguageManager.LatestDue = BanglaLang.latestDue
            LanguageManager.DuePaid = BanglaLang.duePaid
            LanguageManager.RemainingDue = BanglaLang.remainingDue
            LanguageManager.OthersDue = BanglaLang.othersDue
            LanguageManager.OthersDuePaid = BanglaLang.othersDuePaid
            LanguageManager.Data = BanglaLang.data
            LanguageManager.ProductReturn = BanglaLang.productReturn
            LanguageManager.Return = BanglaLang.retturn
            //AddNewSaleViewController
            LanguageManager.TheProductIsAlreadySelectedDeselectTheProductForUpdate = BanglaLang.theProductIsAlreadySelectedDeselectTheProductForUpdate
            LanguageManager.InsufficientQuantity = BanglaLang.insufficientQuantity
            LanguageManager.Continue = BanglaLang.continuee
            LanguageManager.Complete = BanglaLang.complete
            LanguageManager.SaveAsDraft = BanglaLang.saveAsDraft
            LanguageManager.UpdateDraft = BanglaLang.updateDraft
            LanguageManager.YouCannotSaleThisProduct = BanglaLang.youCannotSaleThisProduct
            LanguageManager.Discard = BanglaLang.discard
            LanguageManager.Next = BanglaLang.next
            LanguageManager.Submit = BanglaLang.submit
            LanguageManager.Refresh = BanglaLang.refresh
            LanguageManager.Draft = BanglaLang.draft
            //SaleAddSecondViewController
            LanguageManager.One = BanglaLang.one
            LanguageManager.QuantityIsRequired = BanglaLang.quantityIsRequired
            LanguageManager.SellingPriceIsRequired = BanglaLang.sellingPriceIsRequired
            //SaleAddSerialViewController
            LanguageManager.ProductSerialIsRequired = BanglaLang.productSerialIsRequired
            //SaleInfoViewController
            LanguageManager.Amount = BanglaLang.amount
            LanguageManager.Customer = BanglaLang.customer
            LanguageManager.PayableAmount = BanglaLang.payableAmount
            LanguageManager.Cash = BanglaLang.cash
            LanguageManager.CashBack = BanglaLang.cashBack
            LanguageManager.BkashPaymentPhoneNumberIsRequired = BanglaLang.bkashPaymentPhoneNumberIsRequired
            LanguageManager.RocketPaymentPhoneNumberIsRequired = BanglaLang.rocketPaymentPhoneNumberIsRequired
            LanguageManager.AccountNumberIsRequired = BanglaLang.accountNumberIsRequired
            LanguageManager.CashIsRequired = BanglaLang.cashIsRequired
            LanguageManager.CustomerIsRequired = BanglaLang.customerIsRequired
            LanguageManager.BkashMobileNumber = BanglaLang.bkashMobileNumber
            LanguageManager.RocketMobileNumber = BanglaLang.rocketMobileNumber
            LanguageManager.AccountNumber = BanglaLang.accountNumber
            LanguageManager.Account = BanglaLang.account
            LanguageManager.YouCanNotUpdateMoreThan = BanglaLang.youCanNotUpdateMoreThan
            LanguageManager.YouCanNotUpdateLessThan = BanglaLang.youCanNotUpdateLessThan
            LanguageManager.YouCanNotPayMoreThan = BanglaLang.youCanNotPayMoreThan
            LanguageManager.WalletPayExceedsPayableAmount = BanglaLang.walletPayExceedsPayableAmount
            LanguageManager.Delivery = BanglaLang.delivery
            LanguageManager.WalletAmount = BanglaLang.walletAmount
            LanguageManager.WalletPay = BanglaLang.walletPay
            //SaleProductReturnViewController
            LanguageManager.SearchSaleDate = BanglaLang.searchSaleDate
            //InvoiceReturnViewController
            LanguageManager.ProductName = BanglaLang.productName
            LanguageManager.ReturnQuantity = BanglaLang.returnQuantity
            LanguageManager.ProductIsRequired = BanglaLang.productIsRequired
            LanguageManager.YouCantRetrunMoreThan = BanglaLang.youCantReturnMoreThan
            LanguageManager.CashBackIsRequired = BanglaLang.CashBackIsRequired
            //SaleOnDeliveryViewController
            LanguageManager.DeliverySystemAdd = BanglaLang.deliverySystemAdd
            LanguageManager.DeliverySystemIsRequired = BanglaLang.deliverySystemIsRequired
            LanguageManager.DeliveryChargeIsRequired = BanglaLang.deliveryChargeIsRequired
            LanguageManager.NewDeliverySystemAdd = BanglaLang.newDeliverySystemAdd
            LanguageManager.PhoneNumber = BanglaLang.phoneNumber
            LanguageManager.DiscountIsRequired = BanglaLang.discountIsRequired
            //SaleWithCustomerViewController
            LanguageManager.CustomerAdd = BanglaLang.customerAdd
            LanguageManager.CustomerPhone = BanglaLang.customerPhone
            //SalesCustomerSearchViewController
            LanguageManager.SearchCustomer = BanglaLang.searchCustomer
            //SalesPerDaySearchViewController
            LanguageManager.SearchSerial = BanglaLang.searchSerial
            //SaleListPopOverVC
            LanguageManager.SaleDraft = BanglaLang.saleDraft
            LanguageManager.SalePreOrder = BanglaLang.salePreOrder
            
            //DamagedProductListVC
            LanguageManager.DamagedProducts = BanglaLang.damagedProducts
            LanguageManager.Damaged = BanglaLang.damaged
            LanguageManager.Times = BanglaLang.times
            LanguageManager.TotalDamages = BanglaLang.totalDamages
            LanguageManager.TotalDamageAmount = BanglaLang.totalDamageAmount
            //DamagedProductSearchVC
            LanguageManager.DamagedProductSearch = BanglaLang.damagedProductSearch
            
            //Product
            //ProductListViewController
            LanguageManager.DamagedProduct = BanglaLang.damagedProduct
            LanguageManager.Varient = BanglaLang.variant
            LanguageManager.Category = BanglaLang.category
            LanguageManager.CategoryAndSubCategory = BanglaLang.categoryAndSubCategory
            LanguageManager.SellingPrice = BanglaLang.sellingPrice
            //ProductDetailsBaseViewController
            LanguageManager.ProductDetails = BanglaLang.productDetails
            LanguageManager.Company = BanglaLang.company
            LanguageManager.ProductCode = BanglaLang.productCode
            LanguageManager.PurchasePrice = BanglaLang.purchasePrice
            LanguageManager.QuantityAlert = BanglaLang.quantityAlert
            //ProductDViewController
            LanguageManager.RecentStock = BanglaLang.recentStock
            LanguageManager.RecentSale = BanglaLang.recentSale
            LanguageManager.SerialNo = BanglaLang.serialNo
            LanguageManager.StockAvailable = BanglaLang.stockAvailable
            LanguageManager.VendorName = BanglaLang.vendorName
            LanguageManager.Warranty = BanglaLang.warranty
            LanguageManager.ExpireDate = BanglaLang.expireDate
            LanguageManager.UnSold = BanglaLang.unSold
            LanguageManager.Sold = BanglaLang.sold
            LanguageManager.Refund = BanglaLang.refund
            LanguageManager.Update = BanglaLang.update
            LanguageManager.TheStockIsEmpty = BanglaLang.theStockIsEmpty
            LanguageManager.SerialUpdate = BanglaLang.serialUpdate
            //ProductCategoryViewController
            LanguageManager.SelectCategory = BanglaLang.selectCategory
            LanguageManager.NewCategoryAdd = BanglaLang.newCategoryAdd
            LanguageManager.NewCategory = BanglaLang.newCategory
            LanguageManager.CategoryName = BanglaLang.categoryName
            LanguageManager.CategoryIsRequired = BanglaLang.categoryIsRequired
            LanguageManager.CategoryAlreadyExists = BanglaLang.categoryAlreadyExists
            //ProductSubCategoryVC
            LanguageManager.NewSubCategoryAdd = BanglaLang.newSubCategoryAdd
            LanguageManager.SubCategoryName = BanglaLang.subCategoryName
            LanguageManager.SubCategory = BanglaLang.subCategory
            LanguageManager.SubCategoryNameIsRequired = BanglaLang.subCategoryNameIsRequired
            LanguageManager.SubCategoryIsAlreadyExists = BanglaLang.subCategoryIsAlreadyExists
            //AddNewProductVC
            LanguageManager.Unit = BanglaLang.unit
            LanguageManager.NewProduct = BanglaLang.newProduct
            LanguageManager.Pc = BanglaLang.pc
            LanguageManager.ProductNameIsRequired = BanglaLang.productNameIsRequired
            LanguageManager.ProductCategoryIsRequired = BanglaLang.productCategoryIsRequired
            LanguageManager.ProductUnitIsRequired = BanglaLang.productUnitIsRequired
            //ProductAddThirdVC
            LanguageManager.SerialNumber = BanglaLang.serialNumber
            LanguageManager.QuantityAlertWhenSmallerThan = BanglaLang.quantityAlertWhenSmallerThan
            LanguageManager.UseCommaToSeparateSerial = BanglaLang.useCommaToSeparateSerial
            LanguageManager.NewVendorAdd = BanglaLang.newVendorAdd
            LanguageManager.VendorMobileNumber = BanglaLang.vendorMobileNumber
            LanguageManager.VendorNameIsRequired = BanglaLang.vendorNameIsRequired
            LanguageManager.VendorMobileNumberIsRequired = BanglaLang.vendorMobileNumberIsRequired
            LanguageManager.Month = BanglaLang.month
            LanguageManager.BuyingPrice = BanglaLang.buyingPrice
            LanguageManager.Vendor = BanglaLang.vendor
            LanguageManager.SelectOne = BanglaLang.selectOne
            //RecentStockReturnViewController
            LanguageManager.ProductInfo = BanglaLang.productInfo
            LanguageManager.SelectSerialNumber = BanglaLang.selectSerialNumber
            LanguageManager.SelectAVendorToReturnProduct = BanglaLang.selectAVendorToReturnProduct
            LanguageManager.BuyingPriceIsRequired = BanglaLang.buyingPriceIsRequired
            //RecentStockUpdateViewController
            LanguageManager.StockUpdate = BanglaLang.stockUpdate
            //DamagedProductListVC
            LanguageManager.DamagedProducts = BanglaLang.damagedProducts
            LanguageManager.Damaged = BanglaLang.damaged
            LanguageManager.Times = BanglaLang.times
            LanguageManager.TotalDamages = BanglaLang.totalDamages
            LanguageManager.TotalDamageAmount = BanglaLang.totalDamageAmount
            //DamagedProductSearchVC
            LanguageManager.DamagedProductSearch = BanglaLang.damagedProductSearch
            //DamagedPerProductVC
            LanguageManager.DamagedQuantity = BanglaLang.damagedQuantity
            LanguageManager.Description = BanglaLang.description
            LanguageManager.Close = BanglaLang.close
            LanguageManager.YesDelete = BanglaLang.yesDelete
            //DamageableProductList
            LanguageManager.ToDamageSelectProduct = BanglaLang.toDamageSelectProduct
            //DamageableProductBaseInfoVC
            LanguageManager.QuantityCantBeGreaterThan = BanglaLang.quantityCantBeGreaterThan
            LanguageManager.BuyingpriceCantBeGreaterThan = BanglaLang.buyingpriceCantBeGreaterThan
            LanguageManager.ReturnPriceIsRequired = BanglaLang.returnPriceIsRequired
            //ModuleProductCategory
            LanguageManager.TreeView = BanglaLang.treeView
            LanguageManager.CategoryUpdate = BanglaLang.categoryUpdate
            LanguageManager.ParentCategory = BanglaLang.parentCategory
            
            //Module -> Raw Material
            //RawMaterialListVC
            LanguageManager.Production = BanglaLang.production
            LanguageManager.NewRawMaterial = BanglaLang.newRawMaterial
            LanguageManager.DamagedRawMaterial = BanglaLang.damagedRawMaterial
            //RawMaterialSearchVC
            LanguageManager.RawMaterialSearch = BanglaLang.rawMaterialSearch
            //RawMaterialDetailsBaseVC
            LanguageManager.RawMaterialInfo = BanglaLang.rawMaterialInfo
            LanguageManager.MaterialSku = BanglaLang.materialSku
            LanguageManager.CurrentStock = BanglaLang.currentStock
            LanguageManager.CurrentStockAmount = BanglaLang.currentStockAmount
            //RawMaterialDetailsVC
            LanguageManager.RecentMaterialStock = BanglaLang.recentMaterialStock
            LanguageManager.RecentUse = BanglaLang.recentUse
            LanguageManager.Available = BanglaLang.available
            LanguageManager.StockAmount = BanglaLang.stockAmount
            LanguageManager.UsedQuantity = BanglaLang.usedQuantity
            //RawMaterialRecentStockReturnVC
            LanguageManager.RawMaterialName = BanglaLang.rawMaterialName
            LanguageManager.PricecannotBe0OrLess = BanglaLang.pricecannotBe0OrLess
            //ProductionRawMaterialListVC
            LanguageManager.SelectRawMaterial = BanglaLang.selectRawMaterial
            LanguageManager.SelectARawMaterialForProduction = BanglaLang.selectARawMaterialForProduction
            LanguageManager.TheRawMaterialIsAlreadySelectedDeselectItThenUpdate = BanglaLang.theRawMaterialIsAlreadySelectedDeselectItThenUpdate
            //ProductionRawMaterialAccessoryInfoVC
            LanguageManager.Product = BanglaLang.product
            LanguageManager.SelectProduct = BanglaLang.selectProduct
            LanguageManager.ProductionCost = BanglaLang.productionCost
            LanguageManager.TotalExpense = BanglaLang.totalExpense
            //PurchaseableRawMaterialListVC
            LanguageManager.SelectARawMaterialToPurchase = BanglaLang.selectARawMaterialToPurchase
            //PurchaseReturnRawMaterialListVC
            LanguageManager.SelectARawMaterialToReturn = BanglaLang.selectARawMaterialToReturn
            //PurchaseReturnRawMaterialBaseInfoVC
            LanguageManager.RawMaterialReturn = BanglaLang.rawMaterialReturn
            LanguageManager.ReturnPrice = BanglaLang.returnPrice
            LanguageManager.CashBackPriceIsRequired = BanglaLang.cashBackPriceIsRequired
            LanguageManager.CashbackPriceCantBeGreaterThan = BanglaLang.cashbackPriceCantBeGreaterThan
            //PurchaseReturnRawMaterialAccessoryInfoVC
            LanguageManager.TotalPrice = BanglaLang.totalPrice
            //RawMaterialPurchaseListVC
            LanguageManager.RawMaterialPurchaseBook = BanglaLang.rawMaterialPurchaseBook
            //RawMaterialPurchaseDetailsVC
            LanguageManager.MaterialPurchaseInvoice = BanglaLang.materialPurchaseInvoice
            LanguageManager.InvoicePayable = BanglaLang.invoicePayable
            LanguageManager.LatestPayable = BanglaLang.latestPayable
            LanguageManager.PayablePaid = BanglaLang.payablePaid
            LanguageManager.RemainingPayable = BanglaLang.remainingPayable
            LanguageManager.OthersPayable = BanglaLang.othersPayable
            //DamagedRawMaterialSearchVC
            LanguageManager.DamagedRawMaterialSearch = BanglaLang.damagedRawMaterialSearch
            //DamageableRawMaterialListVC
            LanguageManager.SelectRawMaterialToDamage = BanglaLang.selectRawMaterialToDamage
            //RawMaterialUpdateViewController
            LanguageManager.RawMaterialUnitRequired = BanglaLang.rawMaterialUnitRequired
            //AddRawMaterialBaseInfoVC
            LanguageManager.RawMaterialCode = BanglaLang.rawMaterialCode
            LanguageManager.RawMaterialNameIsRequired = BanglaLang.rawMaterialNameIsRequired
            //PurchaseableRawMaterialAccessoryInfoVC
            LanguageManager.PurchaseDate = BanglaLang.purchaseDate
            LanguageManager.SaleDate = BanglaLang.saleDate
            
            //Module -> Purchase
            //PurchaseDetailsVC
            LanguageManager.PurchaseInvoice = BanglaLang.purchaseInvoice
            //PurchaseReturnDetailsVC
            LanguageManager.ReturnInvoice = BanglaLang.returnInvoice
            //PurchaseableProductListVC
            LanguageManager.SelectAProductToPurchase = BanglaLang.selectAProductToPurchase
            //PurchaseAddVC
            LanguageManager.NewPurchase = BanglaLang.newPurchase
            LanguageManager.PurchaseRelatedInfo = BanglaLang.purchaseRelatedInfo
            //RefundableProductListVC
            LanguageManager.ToReturnSelectAProduct = BanglaLang.toReturnSelectAProduct
            //RefundAddSerialVC
            LanguageManager.SerialNumberIsRequired = BanglaLang.serialNumberIsRequired
            //PreOrderListVC
            LanguageManager.Received = BanglaLang.received
            LanguageManager.OrderDate = BanglaLang.orderDate
            LanguageManager.ReceiveDate = BanglaLang.receiveDate
            LanguageManager.AdvanceAmount = BanglaLang.advanceAmount
            LanguageManager.AmountToBePaid = BanglaLang.amountToBePaid
            LanguageManager.NoRemarks = BanglaLang.noRemarks
            //PreOrderableProductVC
            LanguageManager.ToOrderSelectAProduct = BanglaLang.toOrderSelectAProduct
            //PreOrderAddVC
            LanguageManager.NewPreOrder = BanglaLang.newPreOrder
            LanguageManager.AddMoreProduct = BanglaLang.addMoreProduct
            //PreOrderAddInfoVC
            LanguageManager.Remarks = BanglaLang.remarks
            LanguageManager.SelectReceiveDate = BanglaLang.selectReceiveDate
            //PreOrderPendingVC
            LanguageManager.Receive = BanglaLang.receive
            LanguageManager.ChangeAdvance = BanglaLang.changeAdvance
            LanguageManager.AdvanceAmountIsRequired = BanglaLang.advanceAmountIsRequired
            LanguageManager.AdvanceAmountsNeedToBeModified = BanglaLang.advanceAmountsNeedToBeModified
            LanguageManager.PaidAmountIsRequired = BanglaLang.paidAmountIsRequired
            //PerOrderEditProductList
            LanguageManager.SelectProductsForPreOrder = BanglaLang.selectProductsForPreOrder
            
            //Module -> Expense
            //ExpenseViewController
            LanguageManager.TotalExpenseAmount = BanglaLang.totalExpenseAmount
            //AddNewExpenseVC
            LanguageManager.Employee = BanglaLang.employee
            LanguageManager.ExpenseAmount = BanglaLang.expenseAmount
            LanguageManager.UpdateInfo = BanglaLang.updateInfo
            LanguageManager.NewExpense = BanglaLang.newExpense
            LanguageManager.ExpenseTypeIsRequired = BanglaLang.expenseTypeIsRequired
            LanguageManager.ExpenseType = BanglaLang.expenseType
            LanguageManager.NewExpenseAdd = BanglaLang.newExpenseAdd
            LanguageManager.ExpenseDate = BanglaLang.expenseDate
            LanguageManager.SelectAnItemToSearch = BanglaLang.selectAnItemToSearch
            
            //Module -> Due
            //DueViewController
            LanguageManager.DueCustomer = BanglaLang.dueCustomer
            //AddNewDueVC
            LanguageManager.DueUpdate = BanglaLang.dueUpdate
            LanguageManager.WithdrawUpdate = BanglaLang.withdrawUpdate
            LanguageManager.WithdrawnUpdate = BanglaLang.withdrawnUpdate
            LanguageManager.CollectionAmount = BanglaLang.collectionAmount
            
            
            //Module -> Delivery
            //DeliveryViewController
            LanguageManager.DeliveryLog = BanglaLang.deliveryLog
            LanguageManager.DeliveryHistory = BanglaLang.deliveryHistory
            LanguageManager.Withdraw = BanglaLang.withdraw
            LanguageManager.DeliveryNameIsRequired = BanglaLang.deliveryNameIsRequired
            //AddNewDeliveryDueVC
            LanguageManager.DueAmount = BanglaLang.dueAmount
            LanguageManager.DueWithdraw = BanglaLang.dueWithdraw
            LanguageManager.DueWithdrawAmount = BanglaLang.dueWithdrawAmount
            LanguageManager.WithdrawAmountIsTooMuch = BanglaLang.withdrawAmountIsTooMuch
            //DeliveryLogSearchVC
            LanguageManager.From = BanglaLang.from
            LanguageManager.To = BanglaLang.to
            LanguageManager.Search = BanglaLang.search
            //AddNewDeliverySystemVC
            LanguageManager.NewDeliverySystemInfo = BanglaLang.newDeliverySystemInfo
            //DeliveryDuePaidVC
            LanguageManager.Commission = BanglaLang.commission
            LanguageManager.NewDueIsRequired = BanglaLang.newDueIsRequired
            LanguageManager.CommissionUnitIsRequired = BanglaLang.commissionUnitIsRequired
            LanguageManager.OneHundred = BanglaLang.oneHundred
            //DBaseViewController
            LanguageManager.DeliverySystemProfile = BanglaLang.deliverySystemProfile
            LanguageManager.TotalOrder = BanglaLang.totalOrder
            LanguageManager.CurrentOrder = BanglaLang.currentOrder
            LanguageManager.TotalDuePaid = BanglaLang.totalDuePaid
            //DDetailsViewController
            LanguageManager.By = BanglaLang.by
            LanguageManager.TotalPaid = BanglaLang.totalPaid
            LanguageManager.DueHistory = BanglaLang.dueHistory
            
            //Module -> Customer
            //CustomerSearchVC
            LanguageManager.CustomerSearch = BanglaLang.customerSearch
            //CustomerDBaseVC
            LanguageManager.CustomerDetails = BanglaLang.customerDetails
            LanguageManager.CustomerWallet = BanglaLang.customerWallet
            LanguageManager.DueAdjust = BanglaLang.dueAdjust
            LanguageManager.DueAdjustUpdate = BanglaLang.dueAdjustUpdate
            LanguageManager.NewDueAdjust = BanglaLang.newDueAdjust
            LanguageManager.DueAdjustAmount = BanglaLang.dueAdjustAmount
            LanguageManager.AdjustedBy = BanglaLang.adjustedBy
            LanguageManager.DueAdjustAmountIsRequired = BanglaLang.dueAdjustAmountIsRequired
            LanguageManager.YouCanNotAdjustMoreThan =  BanglaLang.youCanNotAdjustMoreThan
            //CustomerAddVC
            LanguageManager.NewCustomerAdd = BanglaLang.newCustomerAdd
            LanguageManager.UpdateCustomerInfo = BanglaLang.updateCustomerInfo
            LanguageManager.CustomerDueUpdate = BanglaLang.customerDueUpdate
            LanguageManager.CustomerAddressIsRequired = BanglaLang.customerAddressIsRequired
            //CustomerDuePaidVC
            LanguageManager.DuePaymentIsRequired = BanglaLang.duePaymentIsRequired
            
            //Module -> Vendors
            //VendorSearchViewController
            LanguageManager.VendorSearch = BanglaLang.vendorSearch
            //VendorDBaseVC
            LanguageManager.VendorProfile = BanglaLang.vendorProfile
            LanguageManager.TotalPurchase = BanglaLang.totalPurchase
            LanguageManager.CurrentPayable = BanglaLang.currentPayable
            LanguageManager.TotalPayable = BanglaLang.totalPayable
            LanguageManager.VendorWallet = BanglaLang.vendorWallet
            LanguageManager.PayableAdjust = BanglaLang.payableAdjust
            LanguageManager.PayableAdjustUpdate = BanglaLang.payableAdjustUpdate
            LanguageManager.NewPayableAdjust = BanglaLang.newPayableAdjust
            LanguageManager.PayableAdjustAmount = BanglaLang.payableAdjustAmount
            LanguageManager.PayableAdjustAmountIsRequired = BanglaLang.payableAdjustAmountIsRequired
            //VendorDViewController
            LanguageManager.PayableHistory = BanglaLang.payableHistory
            LanguageManager.PurchaseHistory = BanglaLang.purchaseHistory
            LanguageManager.ReturnHistory = BanglaLang.returnHistory
            LanguageManager.Payable = BanglaLang.payable
            LanguageManager.PurchaseBy = BanglaLang.purchaseBy
            LanguageManager.TotalAmount = BanglaLang.totalAmount
            LanguageManager.ReturnedBy = BanglaLang.returnedBy
            //AddNewVendorVC
            LanguageManager.VendorInfo = BanglaLang.vendorInfo
            LanguageManager.VendorInfoUpdate = BanglaLang.vendorInfoUpdate
            //VendorPaidVC
            LanguageManager.PayableWithdraw = BanglaLang.payableWithdraw
            LanguageManager.PaidAmountIsGreaterThanCurrentPayable = BanglaLang.paidAmountIsGreaterThanCurrentPayable
            
            //Module -> Payable Book
            //PayableViewController
            LanguageManager.Vendors = BanglaLang.vendors
            //AddNewPayableVC
            LanguageManager.PayableUpdate = BanglaLang.payableUpdate
            LanguageManager.PayableWithdrawUpdate = BanglaLang.payableWithdrawUpdate
            LanguageManager.PayableWithdrawAmount = BanglaLang.payableWithdrawAmount
            LanguageManager.PayableWithdrawAmountIsEmpty = BanglaLang.payableWithdrawAmountIsEmpty
            LanguageManager.PayableAmountIsRequired = BanglaLang.payableAmountIsRequired
            LanguageManager.PayableWithdrawUpdateAmountIsTooMuch = BanglaLang.payableWithdrawUpdateAmountIsTooMuch
            LanguageManager.PayableWithdrawnAmountIsTooMuch = BanglaLang.payableWithdrawnAmountIsTooMuch
            //EditPayableVC
            LanguageManager.AddressIsRequired = BanglaLang.addressIsRequired
            
            //Module -> EmployeeBook
            //EmployeeDBaseVC
            LanguageManager.EmployeeDetails = BanglaLang.employeeDetails
            LanguageManager.EmployeeName = BanglaLang.employeeName
            LanguageManager.JoiningDate = BanglaLang.joiningDate
            LanguageManager.TotalInvoice = BanglaLang.totalInvoice
            LanguageManager.PaidSalaryIsTooMuch = BanglaLang.paidSalaryIsTooMuch
            LanguageManager.TotalsalaryPaid = BanglaLang.totalsalaryPaid
            LanguageManager.PaidSalary = BanglaLang.paidSalary
            LanguageManager.UnpaidSalary = BanglaLang.unpaidSalary
            LanguageManager.Active = BanglaLang.active
            LanguageManager.Inactive = BanglaLang.inactive
            LanguageManager.AccountStatus = BanglaLang.accountStatus
            //EmployeeDViewController
            LanguageManager.SaleHistory = BanglaLang.saleHistory
            LanguageManager.SalaryHistory = BanglaLang.salaryHistory
            //NewEmployeeAddVC
            LanguageManager.EmployeePassword = BanglaLang.employeePassword
            LanguageManager.EmployeeMobileNo = BanglaLang.employeeMobileNo
            LanguageManager.EmployeeInfoUpdate = BanglaLang.employeeInfoUpdate
            LanguageManager.EmployeeNameIsRequired = BanglaLang.employeeNameIsRequired
            LanguageManager.PermissionIsRequired = BanglaLang.permissionIsRequired
            LanguageManager.Permission = BanglaLang.permission
            //EmployeeSearchVC
            LanguageManager.EmployeeSearch = BanglaLang.employeeSearch
            //EmployeeDBasePopOverVC
            LanguageManager.Salary = BanglaLang.salary
            LanguageManager.Payment = BanglaLang.payment
            //EmployeeSalaryAdd
            LanguageManager.Status = BanglaLang.status
            LanguageManager.SalaryUpdate = BanglaLang.salaryUpdate
            LanguageManager.UnpaidSalaryAmount = BanglaLang.unpaidSalaryAmount
            
            //Module->Accounts
            //Asset
            //AssetPropertyListVC
            LanguageManager.Assets = BanglaLang.assets
            LanguageManager.SaleableStockAmount = BanglaLang.saleableStockAmount
            LanguageManager.Properties = BanglaLang.properties
            LanguageManager.Equipments = BanglaLang.equipments
            LanguageManager.GoodWill = BanglaLang.goodWill
            LanguageManager.AddNewProperty = BanglaLang.addNewProperty
            LanguageManager.AddNewEquipments = BanglaLang.addNewEquipments
            LanguageManager.GoodWillAmount = BanglaLang.goodWillAmount
            LanguageManager.GoodwillAmountIsRequired = BanglaLang.goodwillAmountIsRequired
            //PropertyNequipmentsAddVC
            LanguageManager.AmountIsRequired = BanglaLang.amountIsRequired
            LanguageManager.NewCategoryNameIsRequired = BanglaLang.newCategoryNameIsRequired
            //Gross&NetProfitVC
            LanguageManager.GrossProfit = BanglaLang.grossProfit
            LanguageManager.NetProfit = BanglaLang.netProfit
            //AccountsViewController
            LanguageManager.CashDrawer = BanglaLang.cashDrawer
            LanguageManager.Loan = BanglaLang.loan
            LanguageManager.Invest = BanglaLang.invest
            LanguageManager.GrossAndNetProfit = BanglaLang.grossAndNetProfit
            LanguageManager.CashFlowReport = BanglaLang.cashFlowReport
            LanguageManager.BalanceSheet = BanglaLang.balanceSheet
            LanguageManager.CommingSoon = BanglaLang.commingSoon
            LanguageManager.WorkingOnProgress = BanglaLang.workingOnProgress
            LanguageManager.SorryForTheInterupt = BanglaLang.sorryForTheInterupt
            //CashFlowReportVC
            LanguageManager.Sales = BanglaLang.sales
            LanguageManager.Receivable = BanglaLang.receivable
            LanguageManager.CollectionOfLoan = BanglaLang.collectionOfLoan
            LanguageManager.CollectionOfInvest = BanglaLang.collectionOfInvest
            LanguageManager.TotalCashReceive = BanglaLang.totalCashReceive
            LanguageManager.CostOfSale = BanglaLang.costOfSale
            LanguageManager.DeliveryCommission = BanglaLang.deliveryCommission
            LanguageManager.LoanPaid = BanglaLang.loanPaid
            LanguageManager.PayBack = BanglaLang.payBack
            LanguageManager.TotalCashPaid = BanglaLang.totalCashPaid
            LanguageManager.CashInHand = BanglaLang.cashInHand
            //BalanceSheetVC
            LanguageManager.SearchDateIsRequired = BanglaLang.searchDateIsRequired
            LanguageManager.CurrentAssets = BanglaLang.currentAssets
            LanguageManager.FixedAssets = BanglaLang.fixedAssets
            LanguageManager.Liabilities = BanglaLang.liabilities
            LanguageManager.Investment = BanglaLang.investment
            LanguageManager.TotalAssets = BanglaLang.totalAssets
            LanguageManager.TotalLiabilities = BanglaLang.totalLiabilities
            LanguageManager.TotalInvestment = BanglaLang.totalInvestment
            //CashDrawer
            LanguageManager.EnterCashAmountOfYourDrawer = BanglaLang.enterCashAmountOfYourDrawer
            LanguageManager.CashIn = BanglaLang.cashIn
            LanguageManager.CashOut = BanglaLang.cashOut
            LanguageManager.YouCanNotCashOutMoreThan = BanglaLang.youCanNotCashOutMoreThan
            LanguageManager.CashInToVendorWallet = BanglaLang.cashInToVendorWallet
            LanguageManager.CashInToCustomerWallet = BanglaLang.cashInToCustomerWallet
            //CashTransactionVC
            LanguageManager.TransactionUpdate = BanglaLang.transactionUpdate
            LanguageManager.PreviousCashAmount = BanglaLang.previousCashAmount
            LanguageManager.NewCashAmount = BanglaLang.newCashAmount
            LanguageManager.EnterCashAmount = BanglaLang.enterCashAmount
            //LoanerListVC
            LanguageManager.InitialLoan = BanglaLang.initialLoan
            LanguageManager.InterestAmount = BanglaLang.interestAmount
            LanguageManager.Deadline = BanglaLang.deadline
            //LoanerDetailsVC
            LanguageManager.LoanerDetails = BanglaLang.loanerDetails
            LanguageManager.TotalLoan = BanglaLang.totalLoan
            //LoanerTransactionHistoryVC
            LanguageManager.LoanTransactions = BanglaLang.loanTransactions
            //NewLoanAddVC
            LanguageManager.NewLoan = BanglaLang.newLoan
            LanguageManager.LoanAmount = BanglaLang.loanAmount
            LanguageManager.LoanDeadline = BanglaLang.loanDeadline
            LanguageManager.LoanerSelection = BanglaLang.loanerSelection
            LanguageManager.NewLoanerAdd = BanglaLang.newLoanerAdd
            LanguageManager.LoanerName = BanglaLang.loanerName
            LanguageManager.LoanerPhone = BanglaLang.loanerPhone
            LanguageManager.LoanerAddress = BanglaLang.loanerAddress
            LanguageManager.LoanerSelectionIsRequired = BanglaLang.loanerSelectionIsRequired
            LanguageManager.LoanerAmountIsRequired = BanglaLang.loanerAmountIsRequired
            LanguageManager.InterestAmountIsRequired = BanglaLang.interestAmountIsRequired
            LanguageManager.LoanerNameIsRequired = BanglaLang.loanerNameIsRequired
            //LoanPaidVC
            LanguageManager.CurrentLoan = BanglaLang.currentLoan
            LanguageManager.LoanWithdrawAmount = BanglaLang.loanWithdrawAmount
            LanguageManager.LoanWithdrawAmountIsRequired = BanglaLang.loanWithdrawAmountIsRequired
            //LoanerUpdateVC
            LanguageManager.LoanerUpdate = BanglaLang.loanerUpdate
            //LoanerTransactionUpdateVC
            LanguageManager.WithdrawUpdatedAmountIsRequired = BanglaLang.withdrawUpdatedAmountIsRequired
            //LoanerTransactionLoanUpdateVC
            LanguageManager.EditedLoanAmount = BanglaLang.editedLoanAmount
            LanguageManager.EditedInterestAmount = BanglaLang.editedInterestAmount
            LanguageManager.EditedLoanAmountIsRequired = BanglaLang.editedLoanAmountIsRequired
            LanguageManager.EditedInterestAmountRequired = BanglaLang.editedInterestAmountRequired
            LanguageManager.LoanDeadlineIsRequired = BanglaLang.loanDeadlineIsRequired
            //InvestorsListVC
            LanguageManager.Investor = BanglaLang.investor
            //InvestorDetailsVC
            LanguageManager.InvestorDetails = BanglaLang.investorDetails
            //InvestmentTransactionHistoryVC
            LanguageManager.InvestmentTransactions = BanglaLang.investmentTransactions
            //NewInvestmentAddVC
            LanguageManager.NewInvestment = BanglaLang.newInvestment
            LanguageManager.InvestorSelection = BanglaLang.investorSelection
            LanguageManager.InvestAmount = BanglaLang.investAmount
            LanguageManager.NewInvestorAdd = BanglaLang.newInvestorAdd
            LanguageManager.InvestorName = BanglaLang.investorName
            LanguageManager.InvestorPhone = BanglaLang.investorPhone
            LanguageManager.InvestorAddress = BanglaLang.investorAddress
            LanguageManager.InvestorSelectionIsRequired = BanglaLang.investorSelectionIsRequired
            LanguageManager.InvestorAmountIsRequired = BanglaLang.investorAmountIsRequired
            LanguageManager.InvestorNameIsRequired = BanglaLang.investorNameIsRequired
            //InvestmentProfitWithdrawVC
            LanguageManager.InvestmentWithdrawAmountIsRequired = BanglaLang.investmentWithdrawAmountIsRequired
            LanguageManager.InvestmentWithdrawInfo = BanglaLang.investmentWithdrawInfo
            LanguageManager.CurrentInvestment = BanglaLang.currentInvestment
            LanguageManager.InvestmentWithdrawAmount = BanglaLang.investmentWithdrawAmount
            //InvestmentTransactionUpdateVC
            LanguageManager.EditedInvestedAmount = BanglaLang.editedInvestedAmount
            LanguageManager.EditedInvestmentAmountIsRequired = BanglaLang.editedInvestmentAmountIsRequired
            //InvestorSearchVC
            LanguageManager.InvestorSearch = BanglaLang.investorSearch
            //InvestorUpdateVC
            LanguageManager.InvestorUpdate = BanglaLang.investorUpdate
            //MobileBankingListVC
            LanguageManager.MobileBankIsRequired = BanglaLang.mobileBankIsRequired
            LanguageManager.MobileBankingName = BanglaLang.mobileBankingName
            LanguageManager.MobileBanking = BanglaLang.mobileBanking
            
            //Contact
            LanguageManager.Call = BanglaLang.call
            LanguageManager.PonnoTitle = BanglaLang.ponnoTitle
            LanguageManager.PonnoAddress = BanglaLang.ponnoAddress
            LanguageManager.PonnoMobile = BanglaLang.ponnoMobile
            
            //Mark: Shop
            //SegmentedVC
            LanguageManager.ShopInformation = BanglaLang.shopInformation
            LanguageManager.ShopSettings = BanglaLang.shopSettings
            LanguageManager.PaymentAndReferrals = BanglaLang.paymentReferrals
            LanguageManager.Promotion = BanglaLang.promotion
            //FirstViewController
            LanguageManager.ShopName = BanglaLang.shopName
            LanguageManager.OwnerName = BanglaLang.ownerName
            LanguageManager.RegistrationNumber = BanglaLang.registrationNumber
            LanguageManager.ShopPhoneNumber = BanglaLang.shopPhoneNumber
            LanguageManager.ShopAddress = BanglaLang.shopAddress
            LanguageManager.ShopNameIsRequired = BanglaLang.shopNameIsRequired
            LanguageManager.OwnerNameIsRequired = BanglaLang.ownerNameIsRequired
            //SecondViewController
            LanguageManager.PurchaseBase = BanglaLang.purchaseBase
            LanguageManager.ProductionBase = BanglaLang.productionBase
            LanguageManager.LanguageLbl = BanglaLang.languageLbl
            LanguageManager.EnglishTitle = BanglaLang.englishTitle
            LanguageManager.BanglaTitle = BanglaLang.banglaTitle
            LanguageManager.InvoiceNote = BanglaLang.invoiceNote
            LanguageManager.ClearShopData = BanglaLang.clearShopData
            //ShopInfoUpdateVC
            LanguageManager.GeneralInfo = BanglaLang.generalInfo
            LanguageManager.LogInInfo = BanglaLang.logInInfo
            //OwnerLoginInfoUpdate
            LanguageManager.LoginInformation = BanglaLang.loginInformation
            LanguageManager.OldPassword = BanglaLang.oldPassword
            LanguageManager.NewPassword = BanglaLang.newPassword
            LanguageManager.ConfirmPassword = BanglaLang.confirmPassword
            LanguageManager.OldPasswordIsRequired = BanglaLang.oldPasswordIsRequired
            LanguageManager.NewPasswordIsRequired = BanglaLang.newPasswordIsRequired
            LanguageManager.ConfirmPasswordIsRequired = BanglaLang.confirmPasswordIsRequired
            //ThirdViewController
            LanguageManager.Referrals = BanglaLang.referrals
            LanguageManager.ReferralsHistory = BanglaLang.referralsHistory
            //ReferralInfoViewController
            LanguageManager.Referred = BanglaLang.referred
            LanguageManager.Pending = BanglaLang.pending
            LanguageManager.Expired = BanglaLang.expired
            LanguageManager.TotalIncome = BanglaLang.totalIncome
            LanguageManager.TotalReceived = BanglaLang.totalReceived
            LanguageManager.CurrentDue = BanglaLang.currentDue
            //ReferralDetailsViewController
            LanguageManager.NewReferral = BanglaLang.newReferral
            //ReferralAddViewController
            LanguageManager.ShopCategoryIsRequired = BanglaLang.shopCategoryIsRequired
            
            
            
        case .English:
            
            //Common
            LanguageManager.Warning = EnglishLang.warning
            LanguageManager.Congratulations = EnglishLang.congratulations
            LanguageManager.AreYouSure = EnglishLang.areYouSure
            LanguageManager.Cancel = EnglishLang.cancel
            LanguageManager.Successful = EnglishLang.successful
            LanguageManager.NoInformationFound = EnglishLang.noInformationFound
            LanguageManager.NoCategoryFound = EnglishLang.noCategoryFound
            LanguageManager.Delete = EnglishLang.delete
            LanguageManager.Yes = EnglishLang.yes
            LanguageManager.No = EnglishLang.no
            LanguageManager.ProductSearch = EnglishLang.productSearch
            LanguageManager.SearchFilter = EnglishLang.searchFilter
            
            //Module -> Login
            LanguageManager.Login = EnglishLang.login
            LanguageManager.Logout = EnglishLang.logout
            LanguageManager.Register = EnglishLang.register
            LanguageManager.PhoneNumberIsRequired = EnglishLang.phoneNumberIsRequired
            LanguageManager.PhoneNumberIsIncorrect = EnglishLang.phoneNumberIsIncorrect
            LanguageManager.NameIsRequired = EnglishLang.nameIsRequired
            LanguageManager.PasswordIsRequired = EnglishLang.passwordIsRequired
            LanguageManager.RepeatPasswordIsRequired = EnglishLang.repeatPasswordIsRequired
            LanguageManager.PasswordDidNotMatch = EnglishLang.passwordDidNotMatch
            LanguageManager.MobileNumber = EnglishLang.mobileNumber
            LanguageManager.Password = EnglishLang.password
            LanguageManager.SixToTwenty = EnglishLang.sixToTwenty
            LanguageManager.DontHaveAnAccount = EnglishLang.dontHaveAnAccount
            LanguageManager.AlreadyHaveAnAccount = EnglishLang.alreadyHaveAnAccount
            LanguageManager.Name = EnglishLang.name
            LanguageManager.Phone = EnglishLang.phone
            LanguageManager.RepeatPassword = EnglishLang.repeatPassword
            LanguageManager.ShopCategory = EnglishLang.shopCategory
            LanguageManager.Registration = EnglishLang.registration
            
            //Home
            LanguageManager.Dashboard = EnglishLang.dashboard
            LanguageManager.ExistingDataWillBeRemoved = EnglishLang.existingDataWillBeRemoved
            LanguageManager.Sale = EnglishLang.sale
            LanguageManager.Purchase = EnglishLang.purchase
            LanguageManager.Expense = EnglishLang.expense
            LanguageManager.NewInventory = EnglishLang.newInventory
            LanguageManager.NewDue = EnglishLang.newDue
            LanguageManager.NewPayable = EnglishLang.newPayable
            LanguageManager.NewEmployee = EnglishLang.newEmployee
            LanguageManager.NewCustomer = EnglishLang.newCustomer
            LanguageManager.NewDelivery = EnglishLang.newDelivery
            LanguageManager.NewVendor = EnglishLang.newVendor
            LanguageManager.DummyDataButton = EnglishLang.dummyDataButton
            
            //SideMenu
            LanguageManager.TodaysSummary = EnglishLang.todaysSummary
            LanguageManager.SaleBook = EnglishLang.saleBook
            LanguageManager.DeliveryBook = EnglishLang.deliveryBook
            LanguageManager.DueBook = EnglishLang.dueBook
            LanguageManager.CustomerBook = EnglishLang.customerBook
            LanguageManager.Inventory = EnglishLang.inventory
            LanguageManager.ExpenseBook = EnglishLang.expenseBook
            LanguageManager.EmployeeBook = EnglishLang.employeeBook
            LanguageManager.PurchaseBook = EnglishLang.purchaseBook
            LanguageManager.RawMaterial = EnglishLang.rawMaterial
            LanguageManager.PreOrder = EnglishLang.preOrder
            LanguageManager.PayableBook = EnglishLang.payableBook
            LanguageManager.VendorBook = EnglishLang.vendorBook
            LanguageManager.Accounts = EnglishLang.accounts
            LanguageManager.AccountsBook = EnglishLang.accountsBook
            LanguageManager.Contact = EnglishLang.contact
            
            //Mark: TodaysSummary
            LanguageManager.TodaysSale = EnglishLang.todaysSale
            LanguageManager.OtherCalculations = EnglishLang.otherCalculations
            
            //Sale
            //SalesListViewController
            LanguageManager.SelectStartDate = EnglishLang.selectStartDate
            LanguageManager.SelectEndDate = EnglishLang.selectEndDate
            LanguageManager.SelectDate = EnglishLang.selectDate
            LanguageManager.StartingDateIsRequired = EnglishLang.startingDateIsRequired
            LanguageManager.EndDateIsRequired = EnglishLang.endDateIsRequired
            LanguageManager.StartDate = EnglishLang.startDate
            LanguageManager.EndDate = EnglishLang.endDate
            LanguageManager.TotalSale = EnglishLang.totalSale
            LanguageManager.TotalSaleAmount = EnglishLang.totalSaleAmount
            LanguageManager.TotalDue = EnglishLang.totalDue
            //SalePerDayListViewController
            LanguageManager.Total = EnglishLang.total
            LanguageManager.Due = EnglishLang.due
            LanguageManager.DeliverySystem = EnglishLang.deliverySystem
            LanguageManager.Paid = EnglishLang.paid
            LanguageManager.Unpaid = EnglishLang.unpaid
            //SaleDetailsViewController
            LanguageManager.SalesInvoice = EnglishLang.salesInvoice
            LanguageManager.Invoice = EnglishLang.invoice
            LanguageManager.InvoiceReceive = EnglishLang.invoiceReceive
            LanguageManager.Date = EnglishLang.date
            LanguageManager.DeliveryDate = EnglishLang.deliveryDate
            LanguageManager.Time = EnglishLang.time
            LanguageManager.Discount = EnglishLang.discount
            LanguageManager.PaymentMethod = EnglishLang.paymentMethod
            LanguageManager.PaymentNumber = EnglishLang.paymentNumber
            LanguageManager.PaidAmount = EnglishLang.paidAmount
            LanguageManager.DeliveryCharge = EnglishLang.deliveryCharge
            LanguageManager.CustomerName = EnglishLang.customerName
            LanguageManager.Address = EnglishLang.address
            LanguageManager.Quantity = EnglishLang.quantity
            LanguageManager.PricePerUnit = EnglishLang.pricePerUnit
            LanguageManager.DeliveryStatus = EnglishLang.deliveryStatus
            LanguageManager.Delivered = EnglishLang.delivered
            LanguageManager.DeliverySystemDue = EnglishLang.deliverySystemDue
            LanguageManager.InvoiceDue = EnglishLang.invoiceDue
            LanguageManager.LatestDue = EnglishLang.latestDue
            LanguageManager.DuePaid = EnglishLang.duePaid
            LanguageManager.RemainingDue = EnglishLang.remainingDue
            LanguageManager.OthersDue = EnglishLang.othersDue
            LanguageManager.OthersDuePaid = EnglishLang.othersDuePaid
            LanguageManager.Data = EnglishLang.data
            LanguageManager.ProductReturn = EnglishLang.productReturn
            LanguageManager.Return = EnglishLang.retturn
            //AddNewSaleViewController
            LanguageManager.TheProductIsAlreadySelectedDeselectTheProductForUpdate = EnglishLang.theProductIsAlreadySelectedDeselectTheProductForUpdate
            LanguageManager.InsufficientQuantity = EnglishLang.insufficientQuantity
            LanguageManager.Continue = EnglishLang.continuee
            LanguageManager.Complete = BanglaLang.complete
            LanguageManager.SaveAsDraft = EnglishLang.saveAsDraft
            LanguageManager.UpdateDraft = EnglishLang.updateDraft
            LanguageManager.YouCannotSaleThisProduct = EnglishLang.youCannotSaleThisProduct
            LanguageManager.Discard = EnglishLang.discard
            LanguageManager.Next = EnglishLang.next
            LanguageManager.Submit = EnglishLang.submit
            LanguageManager.Refresh = EnglishLang.refresh
            LanguageManager.Draft = EnglishLang.draft
            //SaleAddSecondViewController
            LanguageManager.One = EnglishLang.one
            LanguageManager.QuantityIsRequired = EnglishLang.quantityIsRequired
            LanguageManager.SellingPriceIsRequired = EnglishLang.sellingPriceIsRequired
            //SaleAddSerialViewController
            LanguageManager.ProductSerialIsRequired = EnglishLang.productSerialIsRequired
            //SaleInfoViewController
            LanguageManager.Amount = EnglishLang.amount
            LanguageManager.Customer = EnglishLang.customer
            LanguageManager.PayableAmount = EnglishLang.payableAmount
            LanguageManager.Cash = EnglishLang.cash
            LanguageManager.CashBack = EnglishLang.cashBack
            LanguageManager.BkashPaymentPhoneNumberIsRequired = EnglishLang.bkashPaymentPhoneNumberIsRequired
            LanguageManager.RocketPaymentPhoneNumberIsRequired = EnglishLang.rocketPaymentPhoneNumberIsRequired
            LanguageManager.AccountNumberIsRequired = EnglishLang.accountNumberIsRequired
            LanguageManager.CashIsRequired = EnglishLang.cashIsRequired
            LanguageManager.CustomerIsRequired = EnglishLang.customerIsRequired
            LanguageManager.BkashMobileNumber = EnglishLang.bkashMobileNumber
            LanguageManager.RocketMobileNumber = EnglishLang.rocketMobileNumber
            LanguageManager.AccountNumber = EnglishLang.accountNumber
            LanguageManager.Account = EnglishLang.account
            LanguageManager.Delivery = EnglishLang.delivery
            LanguageManager.YouCanNotUpdateMoreThan = EnglishLang.youCanNotUpdateMoreThan
            LanguageManager.YouCanNotUpdateLessThan = EnglishLang.youCanNotUpdateLessThan
            LanguageManager.YouCanNotPayMoreThan = EnglishLang.youCanNotPayMoreThan
            LanguageManager.WalletPayExceedsPayableAmount = EnglishLang.walletPayExceedsPayableAmount
            LanguageManager.WalletAmount = EnglishLang.walletAmount
            LanguageManager.WalletPay = EnglishLang.walletPay
            //SaleProductReturnViewController
            LanguageManager.SearchSaleDate = EnglishLang.searchSaleDate
            //InvoiceReturnViewController
            LanguageManager.ProductName = EnglishLang.productName
            LanguageManager.ReturnQuantity = EnglishLang.returnQuantity
            LanguageManager.ProductIsRequired = EnglishLang.productIsRequired
            LanguageManager.YouCantRetrunMoreThan = EnglishLang.youCantReturnMoreThan
            LanguageManager.CashBackIsRequired = EnglishLang.cashBackIsRequired
            LanguageManager.ReturnRelatedInfo = EnglishLang.returnRelatedInfo
            //SaleOnDeliveryViewController
            LanguageManager.DeliverySystemAdd = EnglishLang.deliverySystemAdd
            LanguageManager.DeliverySystemIsRequired = EnglishLang.deliverySystemIsRequired
            LanguageManager.DeliveryChargeIsRequired = EnglishLang.deliveryChargeIsRequired
            LanguageManager.NewDeliverySystemAdd = EnglishLang.newDeliverySystemAdd
            LanguageManager.PhoneNumber = EnglishLang.phoneNumber
            LanguageManager.DiscountIsRequired = EnglishLang.discountIsRequired
            //SaleWithCustomerViewController
            LanguageManager.CustomerAdd = EnglishLang.customerAdd
            LanguageManager.CustomerPhone = EnglishLang.customerPhone
            //SalesCustomerSearchViewController
            LanguageManager.SearchCustomer = EnglishLang.searchCustomer
            //SalesPerDaySearchViewController
            LanguageManager.SearchSerial = EnglishLang.searchSerial
            //DamagedProductListVC
            LanguageManager.DamagedProducts = EnglishLang.damagedProducts
            LanguageManager.Damaged = EnglishLang.damaged
            LanguageManager.Times = EnglishLang.times
            LanguageManager.TotalDamages = EnglishLang.totalDamages
            LanguageManager.TotalDamageAmount = EnglishLang.totalDamageAmount
            //DamagedProductSearchVC
            LanguageManager.DamagedProductSearch = EnglishLang.damagedProductSearch
            //SaleListPopOverVC
            LanguageManager.SaleDraft = EnglishLang.saleDraft
            LanguageManager.SalePreOrder = EnglishLang.salePreOrder
            
            //Product
            //ProductListViewController
            LanguageManager.DamagedProduct = EnglishLang.damagedProduct
            LanguageManager.Varient = EnglishLang.varient
            LanguageManager.Category = EnglishLang.category
            LanguageManager.CategoryAndSubCategory = EnglishLang.categoryAndSubCategory
            LanguageManager.SellingPrice = EnglishLang.sellingPrice
            //ProductDetailsBaseViewController
            LanguageManager.ProductDetails = EnglishLang.productDetails
            LanguageManager.Company = EnglishLang.company
            LanguageManager.ProductCode = EnglishLang.productCode
            LanguageManager.PurchasePrice = EnglishLang.purchasePrice
            LanguageManager.QuantityAlert = EnglishLang.quantityAlert
            //ProductDViewController
            LanguageManager.RecentStock = EnglishLang.recentStock
            LanguageManager.RecentSale = EnglishLang.recentSale
            LanguageManager.SerialNo = EnglishLang.serialNo
            LanguageManager.StockAvailable = EnglishLang.stockAvailable
            LanguageManager.VendorName = EnglishLang.vendorName
            LanguageManager.Warranty = EnglishLang.warranty
            LanguageManager.ExpireDate = EnglishLang.expireDate
            LanguageManager.UnSold = EnglishLang.unSold
            LanguageManager.Sold = EnglishLang.sold
            LanguageManager.Refund = EnglishLang.refund
            LanguageManager.Update = EnglishLang.update
            LanguageManager.TheStockIsEmpty = EnglishLang.theStockIsEmpty
            LanguageManager.SerialUpdate = EnglishLang.serialUpdate
            //ProductCategoryViewController
            LanguageManager.SelectCategory = EnglishLang.selectCategory
            LanguageManager.NewCategoryAdd = EnglishLang.newCategoryAdd
            LanguageManager.NewCategory = EnglishLang.newCategory
            LanguageManager.CategoryName = EnglishLang.categoryName
            LanguageManager.CategoryIsRequired = EnglishLang.categoryIsRequired
            LanguageManager.CategoryAlreadyExists = EnglishLang.categoryAlreadyExists
            //ProductSubCategoryVC
            LanguageManager.NewSubCategoryAdd = EnglishLang.newSubCategoryAdd
            LanguageManager.SubCategoryName = EnglishLang.subCategoryName
            LanguageManager.SubCategory = EnglishLang.subCategory
            LanguageManager.SubCategoryNameIsRequired = EnglishLang.subCategoryNameIsRequired
            LanguageManager.SubCategoryIsAlreadyExists = EnglishLang.subCategoryIsAlreadyExists
            //AddNewProductVC
            LanguageManager.Unit = EnglishLang.unit
            LanguageManager.NewProduct = EnglishLang.newProduct
            LanguageManager.Pc = EnglishLang.pc
            LanguageManager.ProductNameIsRequired = EnglishLang.productNameIsRequired
            LanguageManager.ProductCategoryIsRequired = EnglishLang.productCategoryIsRequired
            LanguageManager.ProductUnitIsRequired = EnglishLang.productUnitIsRequired
            //ProductAddThirdVC
            LanguageManager.SerialNumber = EnglishLang.serialNumber
            LanguageManager.QuantityAlertWhenSmallerThan = EnglishLang.quantityAlertWhenSmallerThan
            LanguageManager.UseCommaToSeparateSerial = EnglishLang.useCommaToSeparateSerial
            LanguageManager.NewVendorAdd = EnglishLang.newVendorAdd
            LanguageManager.VendorMobileNumber = EnglishLang.vendorMobileNumber
            LanguageManager.VendorNameIsRequired = EnglishLang.vendorNameIsRequired
            LanguageManager.VendorMobileNumberIsRequired = EnglishLang.vendorMobileNumberIsRequired
            LanguageManager.Month = EnglishLang.month
            LanguageManager.BuyingPrice = EnglishLang.buyingPrice
            LanguageManager.Vendor = EnglishLang.vendor
            LanguageManager.SelectOne = EnglishLang.selectOne
            //RecentStockReturnViewController
            LanguageManager.ProductInfo = EnglishLang.productInfo
            LanguageManager.SelectSerialNumber = EnglishLang.selectSerialNumber
            LanguageManager.SelectAVendorToReturnProduct = EnglishLang.selectAVendorToReturnProduct
            LanguageManager.BuyingPriceIsRequired = EnglishLang.buyingPriceIsRequired
            //RecentStockUpdateViewController
            LanguageManager.StockUpdate = EnglishLang.stockUpdate
            //DamagedProductListVC
            LanguageManager.DamagedProducts = EnglishLang.damagedProducts
            LanguageManager.Damaged = EnglishLang.damaged
            LanguageManager.Times = EnglishLang.times
            LanguageManager.TotalDamages = EnglishLang.totalDamages
            LanguageManager.TotalDamageAmount = EnglishLang.totalDamageAmount
            //DamagedProductSearchVC
            LanguageManager.DamagedProductSearch = EnglishLang.damagedProductSearch
            //DamagedPerProductVC
            LanguageManager.DamagedQuantity = EnglishLang.damagedQuantity
            LanguageManager.Description = EnglishLang.description
            LanguageManager.Close = EnglishLang.close
            LanguageManager.YesDelete = EnglishLang.yesDelete
            //DamageableProductList
            LanguageManager.ToDamageSelectProduct = EnglishLang.toDamageSelectProduct
            //DamageableProductBaseInfoVC
            LanguageManager.QuantityCantBeGreaterThan = EnglishLang.quantityCantBeGreaterThan
            LanguageManager.BuyingpriceCantBeGreaterThan = EnglishLang.buyingpriceCantBeGreaterThan
            LanguageManager.ReturnPriceIsRequired = EnglishLang.returnPriceIsRequired
            //ModuleProductCategory
            LanguageManager.TreeView = EnglishLang.treeView
            LanguageManager.CategoryUpdate = EnglishLang.categoryUpdate
            LanguageManager.ParentCategory = EnglishLang.parentCategory
            
            //Module -> Raw Material
            //RawMaterialListVC
            LanguageManager.Production = EnglishLang.production
            LanguageManager.NewRawMaterial = EnglishLang.newRawMaterial
            LanguageManager.DamagedRawMaterial = EnglishLang.damagedRawMaterial
            //RawMaterialSearchVC
            LanguageManager.RawMaterialSearch = EnglishLang.rawMaterialSearch
            //RawMaterialDetailsBaseVC
            LanguageManager.RawMaterialInfo = EnglishLang.rawMaterialInfo
            LanguageManager.MaterialSku = EnglishLang.materialSku
            LanguageManager.CurrentStock = EnglishLang.currentStock
            LanguageManager.CurrentStockAmount = EnglishLang.currentStockAmount
            //RawMaterialDetailsVC
            LanguageManager.RecentMaterialStock = EnglishLang.recentMaterialStock
            LanguageManager.RecentUse = EnglishLang.recentUse
            LanguageManager.Available = EnglishLang.available
            LanguageManager.StockAmount = EnglishLang.stockAmount
            LanguageManager.UsedQuantity = EnglishLang.usedQuantity
            //RawMaterialRecentStockReturnVC
            LanguageManager.RawMaterialName = EnglishLang.rawMaterialName
            LanguageManager.PricecannotBe0OrLess = EnglishLang.pricecannotBe0OrLess
            //ProductionRawMaterialListVC
            LanguageManager.SelectRawMaterial = EnglishLang.selectRawMaterial
            LanguageManager.SelectARawMaterialForProduction = EnglishLang.selectARawMaterialForProduction
            LanguageManager.TheRawMaterialIsAlreadySelectedDeselectItThenUpdate = EnglishLang.theRawMaterialIsAlreadySelectedDeselectItThenUpdate
            //ProductionRawMaterialAccessoryInfoVC
            LanguageManager.Product = EnglishLang.product
            LanguageManager.SelectProduct = EnglishLang.selectProduct
            LanguageManager.ProductionCost = EnglishLang.productionCost
            LanguageManager.TotalExpense = EnglishLang.totalExpense
            //PurchaseableRawMaterialListVC
            LanguageManager.SelectARawMaterialToPurchase = EnglishLang.selectARawMaterialToPurchase
            //PurchaseReturnRawMaterialListVC
            LanguageManager.SelectARawMaterialToReturn = EnglishLang.selectARawMaterialToReturn
            //PurchaseReturnRawMaterialBaseInfoVC
            LanguageManager.RawMaterialReturn = EnglishLang.rawMaterialReturn
            LanguageManager.ReturnPrice = EnglishLang.returnPrice
            LanguageManager.CashBackPriceIsRequired = EnglishLang.cashBackPriceIsRequired
            LanguageManager.CashbackPriceCantBeGreaterThan = EnglishLang.cashbackPriceCantBeGreaterThan
            //PurchaseReturnRawMaterialAccessoryInfoVC
            LanguageManager.TotalPrice = EnglishLang.totalPrice
            //RawMaterialPurchaseListVC
            LanguageManager.RawMaterialPurchaseBook = EnglishLang.rawMaterialPurchaseBook
            //RawMaterialPurchaseDetailsVC
            LanguageManager.MaterialPurchaseInvoice = EnglishLang.materialPurchaseInvoice
            LanguageManager.InvoicePayable = EnglishLang.invoicePayable
            LanguageManager.LatestPayable = EnglishLang.latestPayable
            LanguageManager.PayablePaid = EnglishLang.payablePaid
            LanguageManager.RemainingPayable = EnglishLang.remainingPayable
            LanguageManager.OthersPayable = EnglishLang.othersPayable
            //DamagedRawMaterialSearchVC
            LanguageManager.DamagedRawMaterialSearch = EnglishLang.damagedRawMaterialSearch
            //DamageableRawMaterialListVC
            LanguageManager.SelectRawMaterialToDamage = EnglishLang.selectRawMaterialToDamage
            //RawMaterialUpdateViewController
            LanguageManager.RawMaterialUnitRequired = EnglishLang.rawMaterialUnitRequired
            //AddRawMaterialBaseInfoVC
            LanguageManager.RawMaterialCode = EnglishLang.rawMaterialCode
            LanguageManager.RawMaterialNameIsRequired = EnglishLang.rawMaterialNameIsRequired
            //PurchaseableRawMaterialAccessoryInfoVC
            LanguageManager.PurchaseDate = EnglishLang.purchaseDate
            LanguageManager.SaleDate = EnglishLang.saleDate
            
            //Module -> Purchase
            //PurchaseDetailsVC
            LanguageManager.PurchaseInvoice = EnglishLang.purchaseInvoice
            //PurchaseReturnDetailsVC
            LanguageManager.ReturnInvoice = EnglishLang.returnInvoice
            //PurchaseableProductListVC
            LanguageManager.SelectAProductToPurchase = EnglishLang.selectAProductToPurchase
            //PurchaseAddVC
            LanguageManager.NewPurchase = EnglishLang.newPurchase
            LanguageManager.PurchaseRelatedInfo = EnglishLang.purchaseRelatedInfo
            //RefundableProductListVC
            LanguageManager.ToReturnSelectAProduct = EnglishLang.toReturnSelectAProduct
            //RefundAddSerialVC
            LanguageManager.SerialNumberIsRequired = EnglishLang.serialNumberIsRequired
            //PreOrderListVC
            LanguageManager.Received = EnglishLang.received
            LanguageManager.OrderDate = EnglishLang.orderDate
            LanguageManager.ReceiveDate = EnglishLang.receiveDate
            LanguageManager.AdvanceAmount = EnglishLang.advanceAmount
            LanguageManager.AmountToBePaid = EnglishLang.amountToBePaid
            LanguageManager.NoRemarks = EnglishLang.noRemarks
            //PreOrderableProductVC
            LanguageManager.ToOrderSelectAProduct = EnglishLang.toOrderSelectAProduct
            //PreOrderAddVC
            LanguageManager.NewPreOrder = EnglishLang.newPreOrder
            LanguageManager.AddMoreProduct = EnglishLang.addMoreProduct
            //PreOrderAddInfoVC
            LanguageManager.Remarks = EnglishLang.remarks
            LanguageManager.SelectReceiveDate = EnglishLang.selectReceiveDate
            //PreOrderPendingVC
            LanguageManager.Receive = EnglishLang.receive
            LanguageManager.ChangeAdvance = EnglishLang.changeAdvance
            LanguageManager.AdvanceAmountIsRequired = EnglishLang.advanceAmountIsRequired
            LanguageManager.AdvanceAmountsNeedToBeModified = EnglishLang.advanceAmountsNeedToBeModified
            LanguageManager.PaidAmountIsRequired = EnglishLang.paidAmountIsRequired
            //PerOrderEditProductList
            LanguageManager.SelectProductsForPreOrder = EnglishLang.selectProductsForPreOrder
            
            //Module -> Expense
            //ExpenseViewController
            LanguageManager.TotalExpenseAmount = EnglishLang.totalExpenseAmount
            //AddNewExpenseVC
            LanguageManager.Employee = EnglishLang.employee
            LanguageManager.ExpenseAmount = EnglishLang.expenseAmount
            LanguageManager.UpdateInfo = EnglishLang.updateInfo
            LanguageManager.NewExpense = EnglishLang.newExpense
            LanguageManager.ExpenseTypeIsRequired = EnglishLang.expenseTypeIsRequired
            LanguageManager.ExpenseType = EnglishLang.expenseType
            LanguageManager.NewExpenseAdd = EnglishLang.newExpenseAdd
            LanguageManager.ExpenseDate = EnglishLang.expenseDate
            LanguageManager.SelectAnItemToSearch = EnglishLang.selectAnItemToSearch
            
            //Module -> Due
            //DueViewController
            LanguageManager.DueCustomer = EnglishLang.dueCustomer
            //AddNewDueVC
            LanguageManager.DueUpdate = EnglishLang.dueUpdate
            LanguageManager.WithdrawUpdate = EnglishLang.withdrawUpdate
            LanguageManager.WithdrawnUpdate = EnglishLang.withdrawnUpdate
            LanguageManager.CollectionAmount = EnglishLang.collectionAmount
            
            
            //Module -> Delivery
            //DeliveryViewController
            LanguageManager.DeliveryLog = EnglishLang.deliveryLog
            LanguageManager.DeliveryHistory = EnglishLang.deliveryHistory
            LanguageManager.Withdraw = EnglishLang.withdraw
            LanguageManager.DeliveryNameIsRequired = EnglishLang.deliveryNameIsRequired
            //AddNewDeliveryDueVC
            LanguageManager.DueAmount = EnglishLang.dueAmount
            LanguageManager.DueWithdraw = EnglishLang.dueWithdraw
            LanguageManager.DueWithdrawAmount = EnglishLang.dueWithdrawAmount
            LanguageManager.WithdrawAmountIsTooMuch = EnglishLang.withdrawAmountIsTooMuch
            //DeliveryLogSearchVC
            LanguageManager.From = EnglishLang.from
            LanguageManager.To = EnglishLang.to
            LanguageManager.Search = EnglishLang.search
            //AddNewDeliverySystemVC
            LanguageManager.NewDeliverySystemInfo = EnglishLang.newDeliverySystemInfo
            //DeliveryDuePaidVC
            LanguageManager.Commission = EnglishLang.commission
            LanguageManager.NewDueIsRequired = EnglishLang.newDueIsRequired
            LanguageManager.CommissionUnitIsRequired = EnglishLang.commissionUnitIsRequired
            LanguageManager.OneHundred = EnglishLang.oneHundred
            //DBaseViewController
            LanguageManager.DeliverySystemProfile = EnglishLang.deliverySystemProfile
            LanguageManager.TotalOrder = EnglishLang.totalOrder
            LanguageManager.CurrentOrder = EnglishLang.currentOrder
            LanguageManager.TotalDuePaid = EnglishLang.totalDuePaid
            //DDetailsViewController
            LanguageManager.By = EnglishLang.by
            LanguageManager.TotalPaid = EnglishLang.totalPaid
            LanguageManager.DueHistory = EnglishLang.dueHistory
            
            //Module -> Customer
            //CustomerSearchVC
            LanguageManager.CustomerSearch = EnglishLang.customerSearch
            //CustomerDBaseVC
            LanguageManager.CustomerDetails = EnglishLang.customerDetails
            LanguageManager.CustomerWallet = EnglishLang.customerWallet
            LanguageManager.DueAdjust = EnglishLang.dueAdjust
            LanguageManager.DueAdjustUpdate = EnglishLang.dueAdjustUpdate
            LanguageManager.NewDueAdjust = EnglishLang.newDueAdjust
            LanguageManager.DueAdjustAmount = EnglishLang.dueAdjustAmount
            LanguageManager.AdjustedBy = EnglishLang.adjustedBy
            LanguageManager.DueAdjustAmountIsRequired = EnglishLang.dueAdjustAmountIsRequired
            LanguageManager.YouCanNotAdjustMoreThan =  EnglishLang.youCanNotAdjustMoreThan
            //CustomerAddVC
            LanguageManager.NewCustomerAdd = EnglishLang.newCustomerAdd
            LanguageManager.UpdateCustomerInfo = EnglishLang.updateCustomerInfo
            LanguageManager.CustomerDueUpdate = EnglishLang.customerDueUpdate
            LanguageManager.CustomerAddressIsRequired = EnglishLang.customerAddressIsRequired
            //CustomerDuePaidVC
            LanguageManager.DuePaymentIsRequired = EnglishLang.duePaymentIsRequired
            
            //Module -> Vendors
            //VendorSearchViewController
            LanguageManager.VendorSearch = EnglishLang.vendorSearch
            //VendorDBaseVC
            LanguageManager.VendorProfile = EnglishLang.vendorProfile
            LanguageManager.TotalPurchase = EnglishLang.totalPurchase
            LanguageManager.CurrentPayable = EnglishLang.currentPayable
            LanguageManager.TotalPayable = EnglishLang.totalPayable
            LanguageManager.VendorWallet = EnglishLang.vendorWallet
            LanguageManager.PayableAdjust = EnglishLang.payableAdjust
            LanguageManager.PayableAdjustUpdate = EnglishLang.payableAdjustUpdate
            LanguageManager.NewPayableAdjust = EnglishLang.newPayableAdjust
            LanguageManager.PayableAdjustAmount = EnglishLang.payableAdjustAmount
            LanguageManager.PayableAdjustAmountIsRequired = EnglishLang.payableAdjustAmountIsRequired
            //VendorDViewController
            LanguageManager.PayableHistory = EnglishLang.payableHistory
            LanguageManager.PurchaseHistory = EnglishLang.purchaseHistory
            LanguageManager.ReturnHistory = EnglishLang.returnHistory
            LanguageManager.Payable = EnglishLang.payable
            LanguageManager.PurchaseBy = EnglishLang.purchaseBy
            LanguageManager.TotalAmount = EnglishLang.totalAmount
            LanguageManager.ReturnedBy = EnglishLang.returnedBy
            //AddNewVendorVC
            LanguageManager.VendorInfo = EnglishLang.vendorInfo
            LanguageManager.VendorInfoUpdate = EnglishLang.vendorInfoUpdate
            //VendorPaidVC
            LanguageManager.PayableWithdraw = EnglishLang.payableWithdraw
            LanguageManager.PaidAmountIsGreaterThanCurrentPayable = EnglishLang.paidAmountIsGreaterThanCurrentPayable
            
            //Module -> Payable Book
            //PayableViewController
            LanguageManager.Vendors = EnglishLang.vendors
            //AddNewPayableVC
            LanguageManager.PayableUpdate = EnglishLang.payableUpdate
            LanguageManager.PayableWithdrawUpdate = EnglishLang.payableWithdrawUpdate
            LanguageManager.PayableWithdrawAmount = EnglishLang.payableWithdrawAmount
            LanguageManager.PayableWithdrawAmountIsEmpty = EnglishLang.payableWithdrawAmountIsEmpty
            LanguageManager.PayableAmountIsRequired = EnglishLang.payableAmountIsRequired
            LanguageManager.PayableWithdrawUpdateAmountIsTooMuch = EnglishLang.payableWithdrawUpdateAmountIsTooMuch
            LanguageManager.PayableWithdrawnAmountIsTooMuch = EnglishLang.payableWithdrawnAmountIsTooMuch
            //EditPayableVC
            LanguageManager.AddressIsRequired = LanguageManager.AddressIsRequired
            
            //Module -> EmployeeBook
            //EmployeeDBaseVC
            LanguageManager.EmployeeDetails = EnglishLang.employeeDetails
            LanguageManager.EmployeeName = EnglishLang.employeeName
            LanguageManager.JoiningDate = EnglishLang.joiningDate
            LanguageManager.TotalInvoice = EnglishLang.totalInvoice
            LanguageManager.TotalsalaryPaid = EnglishLang.totalsalaryPaid
            LanguageManager.AccountStatus = EnglishLang.accountStatus
            LanguageManager.PaidSalary = EnglishLang.paidSalary
            LanguageManager.UnpaidSalary = EnglishLang.unpaidSalary
            LanguageManager.Active = EnglishLang.active
            LanguageManager.Inactive = EnglishLang.inactive
            LanguageManager.PaidSalaryIsTooMuch = EnglishLang.paidSalaryIsTooMuch
            //EmployeeDViewController
            LanguageManager.SaleHistory = EnglishLang.saleHistory
            LanguageManager.SalaryHistory = EnglishLang.salaryHistory
            //NewEmployeeAddVC
            LanguageManager.EmployeePassword = EnglishLang.employeePassword
            LanguageManager.EmployeeMobileNo = EnglishLang.employeeMobileNo
            LanguageManager.EmployeeInfoUpdate = EnglishLang.employeeInfoUpdate
            LanguageManager.EmployeeNameIsRequired = EnglishLang.employeeNameIsRequired
            LanguageManager.PermissionIsRequired = EnglishLang.permissionIsRequired
            LanguageManager.Permission = EnglishLang.permission
            //EmployeeSearchVC
            LanguageManager.EmployeeSearch = EnglishLang.employeeSearch
            //EmployeeDBasePopOverVC
            LanguageManager.Salary = EnglishLang.salary
            LanguageManager.Payment = EnglishLang.payment
            //EmployeeSalaryAdd
            LanguageManager.Status = EnglishLang.status
            LanguageManager.SalaryUpdate = EnglishLang.salaryUpdate
            LanguageManager.UnpaidSalaryAmount = EnglishLang.unpaidSalaryAmount
            
            //Module->Accounts
            //Asset
            //AssetPropertyListVC
            LanguageManager.Assets = EnglishLang.assets
            LanguageManager.SaleableStockAmount = EnglishLang.saleableStockAmount
            LanguageManager.Properties = EnglishLang.properties
            LanguageManager.Equipments = EnglishLang.equipments
            LanguageManager.GoodWill = EnglishLang.goodWill
            LanguageManager.AddNewProperty = EnglishLang.addNewProperty
            LanguageManager.AddNewEquipments = EnglishLang.addNewEquipments
            LanguageManager.GoodWillAmount = EnglishLang.goodWillAmount
            LanguageManager.GoodwillAmountIsRequired = EnglishLang.goodwillAmountIsRequired
            //PropertyNequipmentsAddVC
            LanguageManager.AmountIsRequired = EnglishLang.amountIsRequired
            LanguageManager.NewCategoryNameIsRequired = EnglishLang.newCategoryNameIsRequired
            //Gross&NetProfitVC
            LanguageManager.GrossProfit = EnglishLang.grossProfit
            LanguageManager.NetProfit = EnglishLang.netProfit
            //AccountsViewController
            LanguageManager.CashDrawer = EnglishLang.cashDrawer
            LanguageManager.Loan = EnglishLang.loan
            LanguageManager.Invest = EnglishLang.invest
            LanguageManager.GrossAndNetProfit = EnglishLang.grossAndNetProfit
            LanguageManager.CashFlowReport = EnglishLang.cashFlowReport
            LanguageManager.BalanceSheet = EnglishLang.balanceSheet
            LanguageManager.CommingSoon = EnglishLang.balanceSheet
            LanguageManager.WorkingOnProgress = EnglishLang.workingOnProgress
            LanguageManager.SorryForTheInterupt = EnglishLang.sorryForTheInterupt
            //CashFlowReportVC
            LanguageManager.Sales = EnglishLang.sales
            LanguageManager.Receivable = EnglishLang.receivable
            LanguageManager.CollectionOfLoan = EnglishLang.collectionOfLoan
            LanguageManager.CollectionOfInvest = EnglishLang.collectionOfInvest
            LanguageManager.TotalCashReceive = EnglishLang.totalCashReceive
            LanguageManager.CostOfSale = EnglishLang.costOfSale
            LanguageManager.DeliveryCommission = EnglishLang.deliveryCommission
            LanguageManager.LoanPaid = EnglishLang.loanPaid
            LanguageManager.PayBack = EnglishLang.payBack
            LanguageManager.TotalCashPaid = EnglishLang.totalCashPaid
            LanguageManager.CashInHand = EnglishLang.cashInHand
            //BalanceSheetVC
            LanguageManager.SearchDateIsRequired = EnglishLang.searchDateIsRequired
            LanguageManager.CurrentAssets = EnglishLang.currentAssets
            LanguageManager.FixedAssets = EnglishLang.fixedAssets
            LanguageManager.Liabilities = EnglishLang.liabilities
            LanguageManager.Investment = EnglishLang.investment
            LanguageManager.TotalAssets = EnglishLang.totalAssets
            LanguageManager.TotalLiabilities = EnglishLang.totalLiabilities
            LanguageManager.TotalInvestment = EnglishLang.totalInvestment
            //CashDrawer
            LanguageManager.EnterCashAmountOfYourDrawer = EnglishLang.enterCashAmountOfYourDrawer
            LanguageManager.CashIn = EnglishLang.cashIn
            LanguageManager.CashOut = EnglishLang.cashOut
            LanguageManager.YouCanNotCashOutMoreThan = EnglishLang.youCanNotCashOutMoreThan
            LanguageManager.CashInToVendorWallet = EnglishLang.cashInToVendorWallet
            LanguageManager.CashInToCustomerWallet = EnglishLang.cashInToCustomerWallet
            //CashTransactionVC
            LanguageManager.TransactionUpdate = EnglishLang.transactionUpdate
            LanguageManager.PreviousCashAmount = EnglishLang.previousCashAmount
            LanguageManager.NewCashAmount = EnglishLang.newCashAmount
            LanguageManager.EnterCashAmount = EnglishLang.enterCashAmount
            //LoanerListVC
            LanguageManager.InitialLoan = EnglishLang.initialLoan
            LanguageManager.InterestAmount = EnglishLang.interestAmount
            LanguageManager.Deadline = EnglishLang.deadline
            //LoanerDetailsVC
            LanguageManager.LoanerDetails = EnglishLang.loanerDetails
            LanguageManager.TotalLoan = EnglishLang.totalLoan
            //LoanerTransactionHistoryVC
            LanguageManager.LoanTransactions = EnglishLang.loanTransactions
            //NewLoanAddVC
            LanguageManager.NewLoan = EnglishLang.newLoan
            LanguageManager.LoanAmount = EnglishLang.loanAmount
            LanguageManager.LoanDeadline = EnglishLang.loanDeadline
            LanguageManager.LoanerSelection = EnglishLang.loanerSelection
            LanguageManager.NewLoanerAdd = EnglishLang.newLoanerAdd
            LanguageManager.LoanerName = EnglishLang.loanerName
            LanguageManager.LoanerPhone = EnglishLang.loanerPhone
            LanguageManager.LoanerAddress = EnglishLang.loanerAddress
            LanguageManager.LoanerSelectionIsRequired = EnglishLang.loanerSelectionIsRequired
            LanguageManager.LoanerAmountIsRequired = EnglishLang.loanerAmountIsRequired
            LanguageManager.InterestAmountIsRequired = EnglishLang.interestAmountIsRequired
            LanguageManager.LoanerNameIsRequired = EnglishLang.loanerNameIsRequired
            //LoanPaidVC
            LanguageManager.CurrentLoan = EnglishLang.currentLoan
            LanguageManager.LoanWithdrawAmount = EnglishLang.loanWithdrawAmount
            LanguageManager.LoanWithdrawAmountIsRequired = EnglishLang.loanWithdrawAmountIsRequired
            //LoanerUpdateVC
            LanguageManager.LoanerUpdate = EnglishLang.loanerUpdate
            //LoanerTransactionUpdateVC
            LanguageManager.WithdrawUpdatedAmountIsRequired = EnglishLang.withdrawUpdatedAmountIsRequired
            //LoanerTransactionLoanUpdateVC
            LanguageManager.EditedLoanAmount = EnglishLang.editedLoanAmount
            LanguageManager.EditedInterestAmount = EnglishLang.editedInterestAmount
            LanguageManager.EditedLoanAmountIsRequired = EnglishLang.editedLoanAmountIsRequired
            LanguageManager.EditedInterestAmountRequired = EnglishLang.editedInterestAmountRequired
            LanguageManager.LoanDeadlineIsRequired = EnglishLang.loanDeadlineIsRequired
            //InvestorsListVC
            LanguageManager.Investor = EnglishLang.investor
            //InvestorDetailsVC
            LanguageManager.InvestorDetails = EnglishLang.investorDetails
            //InvestmentTransactionHistoryVC
            LanguageManager.InvestmentTransactions = EnglishLang.investmentTransactions
            //NewInvestmentAddVC
            LanguageManager.NewInvestment = EnglishLang.newInvestment
            LanguageManager.InvestorSelection = EnglishLang.investorSelection
            LanguageManager.InvestAmount = EnglishLang.investAmount
            LanguageManager.NewInvestorAdd = EnglishLang.newInvestorAdd
            LanguageManager.InvestorName = EnglishLang.investorName
            LanguageManager.InvestorPhone = EnglishLang.investorPhone
            LanguageManager.InvestorAddress = EnglishLang.investorAddress
            LanguageManager.InvestorSelectionIsRequired = EnglishLang.investorSelectionIsRequired
            LanguageManager.InvestorAmountIsRequired = EnglishLang.investorAmountIsRequired
            LanguageManager.InvestorNameIsRequired = EnglishLang.investorNameIsRequired
            //InvestmentProfitWithdrawVC
            LanguageManager.InvestmentWithdrawAmountIsRequired = EnglishLang.investmentWithdrawAmountIsRequired
            LanguageManager.InvestmentWithdrawInfo = EnglishLang.investmentWithdrawInfo
            LanguageManager.CurrentInvestment = EnglishLang.currentInvestment
            LanguageManager.InvestmentWithdrawAmount = EnglishLang.investmentWithdrawAmount
            //InvestmentTransactionUpdateVC
            LanguageManager.EditedInvestedAmount = EnglishLang.editedInvestedAmount
            LanguageManager.EditedInvestmentAmountIsRequired = EnglishLang.editedInvestmentAmountIsRequired
            //InvestorSearchVC
            LanguageManager.InvestorSearch = EnglishLang.investorSearch
            //InvestorUpdateVC
            LanguageManager.InvestorUpdate = EnglishLang.investorUpdate
            //MobileBankingListVC
            LanguageManager.MobileBankIsRequired = EnglishLang.mobileBankIsRequired
            LanguageManager.MobileBankingName = EnglishLang.mobileBankingName
            LanguageManager.MobileBanking = EnglishLang.mobileBank
            
            //Contact
            LanguageManager.Call = EnglishLang.call
            LanguageManager.PonnoTitle = EnglishLang.ponnoTitle
            LanguageManager.PonnoAddress = EnglishLang.ponnoAddress
            LanguageManager.PonnoMobile = EnglishLang.ponnoMobile
            
            //Mark: Shop
            //SegmentedVC
            LanguageManager.ShopInformation = EnglishLang.shopInformation
            LanguageManager.ShopSettings = EnglishLang.shopSettings
            LanguageManager.PaymentAndReferrals = EnglishLang.paymentReferrals
            LanguageManager.Promotion = EnglishLang.promotion
            //FirstViewController
            LanguageManager.ShopName = EnglishLang.shopName
            LanguageManager.OwnerName = EnglishLang.ownerName
            LanguageManager.RegistrationNumber = EnglishLang.registrationNumber
            LanguageManager.ShopPhoneNumber = EnglishLang.shopPhoneNumber
            LanguageManager.ShopAddress = EnglishLang.shopAddress
            LanguageManager.ShopNameIsRequired = EnglishLang.shopNameIsRequired
            LanguageManager.OwnerNameIsRequired = EnglishLang.ownerNameIsRequired
            //SecondViewController
            LanguageManager.PurchaseBase = EnglishLang.purchaseBase
            LanguageManager.ProductionBase = EnglishLang.productionBase
            LanguageManager.LanguageLbl = EnglishLang.languageLbl
            LanguageManager.EnglishTitle = EnglishLang.englishTitle
            LanguageManager.BanglaTitle = EnglishLang.banglaTitle
            LanguageManager.InvoiceNote = EnglishLang.invoiceNote
            LanguageManager.ClearShopData = EnglishLang.clearShopData
            LanguageManager.PurchaseBaseOrProductionBaseIsRequired = EnglishLang.purchaseBaseOrProductionBaseIsRequired
            //LanguageManager.LanguageTitle = EnglishLang.languageTitle
            //ShopInfoUpdateVC
            LanguageManager.GeneralInfo = EnglishLang.generalInfo
            LanguageManager.LogInInfo = EnglishLang.logInInfo
            //OwnerLoginInfoUpdate
            LanguageManager.LoginInformation = EnglishLang.loginInformation
            LanguageManager.OldPassword = EnglishLang.oldPassword
            LanguageManager.NewPassword = EnglishLang.newPassword
            LanguageManager.ConfirmPassword = EnglishLang.confirmPassword
            LanguageManager.OldPasswordIsRequired = EnglishLang.oldPasswordIsRequired
            LanguageManager.NewPasswordIsRequired = EnglishLang.newPasswordIsRequired
            LanguageManager.ConfirmPasswordIsRequired = EnglishLang.confirmPasswordIsRequired
            //ThirdViewController
            LanguageManager.Referrals = EnglishLang.referrals
            LanguageManager.ReferralsHistory = EnglishLang.referralsHistory
            //ReferralInfoViewController
            LanguageManager.Referred = EnglishLang.referred
            LanguageManager.Pending = EnglishLang.pending
            LanguageManager.Expired = EnglishLang.expired
            LanguageManager.TotalIncome = EnglishLang.totalIncome
            LanguageManager.TotalReceived = EnglishLang.totalReceived
            LanguageManager.CurrentDue = EnglishLang.currentDue
            //ReferralDetailsViewController
            LanguageManager.NewReferral = EnglishLang.newReferral
            //ReferralAddViewController
            LanguageManager.ShopCategoryIsRequired = EnglishLang.shopCategoryIsRequired
        }
    }
    
    static func setLanguageAs(language : Language){
        LanguageManager.language = language
        LanguageManager.init()
    }
}










