//
//  Connectivity.swift
//  Ponno
//
//  Created by a k azad on 19/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import Alamofire

struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet:Bool {
        return self.sharedInstance.isReachable
    }
}
