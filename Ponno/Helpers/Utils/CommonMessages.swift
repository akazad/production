//
//  CommonMessages.swift
//  Ponno
//
//  Created by a k azad on 19/4/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

struct CommonMessages{
    static let NoInternet = "No Internet connection"
    static let ApiFailure = "Something Went Wrong, Please try again later"
    static let SomethingWrongPleaseTryAgainLater = "কিছু ভুল হয়েছে। আবার চেষ্টা করুন"
}
