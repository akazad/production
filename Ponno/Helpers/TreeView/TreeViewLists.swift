//
//  TreeViewLists.swift
//  TreeView1
//
//  Created by Cindy Oakes on 5/21/16.
//  Copyright © 2016 Cindy Oakes. All rights reserved.
//

import UIKit
import CoreData

class TreeViewLists
{
    //MARK:  Load Array With Initial Data
    
    
    
    static var productCategory : [ProductCategory] = []
    
    static func LoadInitialData() -> [TreeViewData]{
        var data: [TreeViewData] = []
        fetchData()
        for item in self.productCategory{
            let level = item.gen
            let id = "\(item.id)"
            let parentId = "\(item.parent)"
            if let name = item.name{
                data.append(TreeViewData(level: Int(level), name: name, id: id, parentId: parentId)!)
            }
        }
        return data
    }
    
//    static func LoadInitialData() -> [TreeViewData]
//    {
//        var data: [TreeViewData] = []
//
//        data.append(TreeViewData(level: 0, name: "cindy's family tree", id: "1", parentId: "-1")!)
//        data.append(TreeViewData(level: 0, name: "jack's family tree", id: "2", parentId: "-1")!)
//        data.append(TreeViewData(level: 1, name: "katherine", id: "3", parentId: "1")!)
//        data.append(TreeViewData(level: 1, name: "kyle", id: "4", parentId: "1")!)
//        data.append(TreeViewData(level: 2, name: "hayley", id: "5", parentId: "3")!)
//        data.append(TreeViewData(level: 2, name: "macey", id: "6", parentId: "3")!)
//        data.append(TreeViewData(level: 1, name: "katelyn", id: "7", parentId: "2")!)
//        data.append(TreeViewData(level: 1, name: "jared", id: "8", parentId: "2")!)
//        data.append(TreeViewData(level: 1, name: "denyee", id: "9", parentId: "2")!)
//        data.append(TreeViewData(level: 2, name: "cayleb", id: "10", parentId: "4")!)
//        data.append(TreeViewData(level: 2, name: "carter", id: "11", parentId: "4")!)
//        data.append(TreeViewData(level: 2, name: "braylon", id: "12", parentId: "4")!)
//        data.append(TreeViewData(level: 3, name: "samson", id: "13", parentId: "5")!)
//        data.append(TreeViewData(level: 3, name: "samson", id: "14", parentId: "6")!)
//
//
//        return data
//    }
    
    //MARK:  Load Nodes From Initial Data
    
    static func LoadInitialNodes(dataList: [TreeViewData]) -> [TreeViewNode]
    {
        var nodes: [TreeViewNode] = []
        
        for data in dataList where data.level == 1
        {
            let node: TreeViewNode = TreeViewNode()
            node.nodeLevel = data.level
            node.nodeObject = data.name as AnyObject
            node.isExpanded = GlobalVariables.FALSE
            let newLevel = data.level + 1
            
            let dataItem = dataList.filter{$0.level == 1}
            
            for i in 0..<dataItem.count
            {
                let d: TreeViewData = dataItem[i]
                print("\(d.name)")
            }
            
            node.nodeChildren = LoadChildrenNodes(dataList: dataList, level: Int(newLevel), parentId: data.id)
            
            if (node.nodeChildren?.count == 0)
            {
                node.nodeChildren = nil
            }
            
            nodes.append(node)
        }
        
        return nodes
    }
    
    //MARK:  Recursive Method to Create the Children/Grandchildren....  node arrays
    
    static func LoadChildrenNodes(dataList: [TreeViewData], level: Int, parentId: String) -> [TreeViewNode] 
    {
        print("\(level)" + "\(parentId)")
        var nodes: [TreeViewNode] = []
        
        let list = dataList.filter{$0.parentId == parentId}
        if list.count > 0 {
            for data in dataList where data.level == level && data.parentId == parentId
            {
                print("\(data.name)")
                
                let node: TreeViewNode = TreeViewNode()
                node.nodeLevel = data.level
                node.nodeObject = data.name as AnyObject
                node.isExpanded = GlobalVariables.FALSE
                let newLevel = level + 1
                node.nodeChildren = LoadChildrenNodes(dataList: dataList, level: newLevel, parentId: data.id)
                
                if (node.nodeChildren?.count == 0)
                {
                    node.nodeChildren = nil
                }
                
                nodes.append(node)
                
            }
        }
        return nodes
    }
    
    
}

extension TreeViewLists{
   static func fetchData(){

        TreeViewLists.productCategory.removeAll()
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductCategory")

        do {
            let results = try context.fetch(fetchRequest)
            let  productCategories = results as! [ProductCategory]
            
            for category in productCategories {
                TreeViewLists.productCategory.append(category)
                //print("Fetched")
            }
        }catch let err as NSError {
            print(err.debugDescription)
        }

    }
}
