//
//  FloatingActionButton.swift
//  Ponno
//
//  Created by a k azad on 9/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Floaty

class FloatingActionButton : NSObject{
    
    var floatyDelegate : FloatyDelegate!
    var floatyBtnList : [PermissionList] = []
    var parentValue : Int?
    var inventorySystem : Int?
    
    func getPermissions(){
        
        if let user = getUserData(){
            guard let parent = user.parent else{
                return
            }
            let parentInt = Int(parent)
            self.parentValue = parentInt
        }
        
        if let shop = getShopData(){
            if let inventorySystem = shop.inventorySystem{
                self.inventorySystem = inventorySystem
            }
        }
        
        if self.parentValue == 0 {
            if let list = getPermissionList(),list.count > 0, let assignedPermissions = getPermission(), let permissions = assignedPermissions.id, permissions.count > 0  {
                for item in permissions{
                    for permission in list{
                        if permission.id == item{
                            self.floatyBtnList.append(permission)
                        }
                    }
                }
                var filtered = self.floatyBtnList.filter{($0.id == 10) || ($0.id == 12) || ($0.id == 13)}
                //print(filtered.count)
                if self.inventorySystem == 1 {
                    filtered = filtered.filter{$0.id != 12}
                }
                self.floatyBtnList = filtered
            }
        }else if let list = getPermissionList(),list.count > 0{
            self.floatyBtnList += list
            var filtered = self.floatyBtnList.filter{($0.id == 10) || ($0.id == 12) || ($0.id == 13)}
            //print(filtered.count)
            if self.inventorySystem == 1 {
                filtered = filtered.filter{$0.id != 12} 
            }
            self.floatyBtnList = filtered
            
        }
    }
    
    func addFloaty(buttons : [FloatyItem]?)->UIView{
        getPermissions()
        
        let floaty = Floaty()
        floaty.buttonColor = UIColor.lightGreen
        
        if self.floatyBtnList.count > 0 {
            if let category = getBusinessCategory(){
                if category.businessCategory == 1{
                    //self.floatyBtnList.drop{($0.id == 12)}
                    let items = self.floatyBtnList.filter({($0.id != 12)})
                    self.floatyBtnList = items
                }
            }
            let purchase = self.floatyBtnList.filter{($0.id == 12)}
            let sale = self.floatyBtnList.filter{($0.id == 10)}
            let expense = self.floatyBtnList.filter{($0.id == 13)}
            if !expense.isEmpty{
                let expenseItem = FloatyItem()
                expenseItem.icon = UIImage(named: "next")
                expenseItem.buttonColor = UIColor.lightGreen
                expenseItem.title = LanguageManager.Expense
                expenseItem.handler = { item in
                    self.floatyDelegate?.navigateToAddExpenseVC()
                }
                floaty.addItem(item: expenseItem)
            }
            if !purchase.isEmpty{
                let purchaseItem = FloatyItem()
                purchaseItem.icon = UIImage(named: "next")
                purchaseItem.buttonColor = UIColor.lightGreen
                purchaseItem.title = LanguageManager.Purchase
                purchaseItem.handler = { item in
                    self.floatyDelegate.navigateToAddPurchaseVC()
                }
                floaty.addItem(item: purchaseItem)
            }
            if !sale.isEmpty {
                let saleItem = FloatyItem()
                saleItem.icon = UIImage(named: "next")
                saleItem.buttonColor = UIColor.lightGreen
                saleItem.title = LanguageManager.Sale
                saleItem.handler = { item in
                    self.floatyDelegate.navigateToAddSaleVC()
                }
                floaty.addItem(item: saleItem)
            }
        }
        
        floaty.sticky = false
        floaty.friendlyTap = true
        
        if floaty.items.count < 1 {
            floaty.isHidden = true
        }else {
            floaty.isHidden = false
        }

        guard let buttonList = buttons, buttonList.count > 0 else{
            return floaty
        }
        
        for button in buttonList{
            floaty.addItem(item: button)
        }
        return floaty
    }
    
    func setFloaty()->UIView{
        let floaty = Floaty()
        floaty.buttonColor = UIColor.lightGreen
        
        let saleItem = FloatyItem()
        saleItem.icon = UIImage(named: "next")
        saleItem.buttonColor = UIColor.lightGreen
        saleItem.title = "বিক্রয়"
        saleItem.handler = { item in
            print("bloop")
        }
        floaty.addItem(item: saleItem)
        
        
        let purchaseItem = FloatyItem()
        purchaseItem.icon = UIImage(named: "next")
        purchaseItem.buttonColor = UIColor.lightGreen
        purchaseItem.title = "ক্রয়"
        purchaseItem.handler = { item in
            print("bloop")
        }
        floaty.addItem(item: purchaseItem)
        
        
        let expenseItem = FloatyItem()
        expenseItem.icon = UIImage(named: "next")
        expenseItem.buttonColor = UIColor.lightGreen
        expenseItem.title = "খরচ"
        expenseItem.handler = { item in
            print("bloop")
//            self.navigateToAddNewExpenseViewController()
        }
        floaty.addItem(item: expenseItem)
        
        
        floaty.sticky = false
        floaty.friendlyTap = true
        
        return floaty
    }
}

protocol FloatyDelegate : NSObjectProtocol{
    func navigateToAddSaleVC()
    func navigateToAddPurchaseVC()
    func navigateToAddExpenseVC()
}
