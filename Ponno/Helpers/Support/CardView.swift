//
//  CardView.swift
//  Ponno
//
//  Created by a k azad on 26/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//
import UIKit

@IBDesignable
class CardView: UIView {
    
    @IBInspectable var viewCornerRadius: CGFloat = 2
    
    @IBInspectable var viewShadowOffsetWidth: Int = 0
    @IBInspectable var viewShadowOffsetHeight: Int = 3
    @IBInspectable var viewShadowColor: UIColor? = UIColor.black
    @IBInspectable var viewShadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = viewShadowColor?.cgColor
        layer.shadowOffset = CGSize(width: viewShadowOffsetWidth, height: viewShadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}

extension UIViewController{
    func setUpCardView(uiview : UIView){
        uiview.backgroundColor = UIColor.white
        uiview.layer.cornerRadius = 5.0
        uiview.layer.masksToBounds = false
        
        uiview.layer.shadowColor = (UIColor(red: 150/255, green: 150/255, blue: 150/255)).withAlphaComponent(0.5).cgColor
        uiview.layer.shadowOffset = CGSize(width: 0, height: 0)
        uiview.layer.shadowOpacity = 0.5
    }
}

//extension UITableViewCell{
//    func setUpCardView(uiview : UIView){
//        uiview.backgroundColor = UIColor.white
//        uiview.layer.cornerRadius = 5.0
//        uiview.layer.masksToBounds = false
//        
//        uiview.layer.shadowColor = (UIColor(red: 150/255, green: 150/255, blue: 150/255)).withAlphaComponent(0.5).cgColor
//        uiview.layer.shadowOffset = CGSize(width: 0, height: 0)
//        uiview.layer.shadowOpacity = 0.5
//    }
//}

extension UICollectionViewCell{
    func setUpCollectionCardView(uiview : UIView){
        uiview.backgroundColor = UIColor.white
        uiview.layer.cornerRadius = 5.0
        uiview.layer.masksToBounds = false
        
        uiview.layer.shadowColor = (UIColor(red: 150/255, green: 150/255, blue: 150/255)).withAlphaComponent(0.5).cgColor
        uiview.layer.shadowOffset = CGSize(width: 0, height: 0)
        uiview.layer.shadowOpacity = 0.5
    }
}

extension UITableViewCell{
    func setUpCardView(uiview : UIView){
        uiview.backgroundColor = UIColor.white
        uiview.layer.cornerRadius = 5.0
        uiview.layer.masksToBounds = false
        
        uiview.layer.shadowColor = (UIColor(red: 150/255, green: 150/255, blue: 150/255)).withAlphaComponent(0.5).cgColor
        uiview.layer.shadowOffset = CGSize(width: 0, height: 0)
        uiview.layer.shadowOpacity = 0.5
    }
    
    func setViewColor(amount: Double?, view: UIView){
        if let givenAmount = amount{
            if givenAmount <= 0.0 {
                view.backgroundColor = UIColor.white
            }else{
                view.backgroundColor = UIColor.lightRed
            }
        }
        
        view.roundCorners([.topLeft, .bottomLeft], radius: 2.0) 
    }
    
    func setLabelTextColor(amount: Double?, label: UILabel){
        if let givenAmount = amount{
            if givenAmount > 0.0 {
                label.textColor = .lightRed
            }else{
                label.textColor = .black
            }
        }
    }
    
    func quanityCheck(quantity: Double?, label: UILabel){
        if let quantityCheck = quantity{
            if quantityCheck <= 0.0{
                label.textColor = .lightRed
            }else{
                label.textColor = .black
            }
        }
    }
    
}

extension UIViewController {
    func setUpTableCardView(uiTableView : UITableView){
        uiTableView.backgroundColor = UIColor.white
        uiTableView.layer.cornerRadius = 5.0
        uiTableView.layer.masksToBounds = false
        
        uiTableView.layer.shadowColor = (UIColor(red: 150/255, green: 150/255, blue: 150/255)).withAlphaComponent(0.5).cgColor
        uiTableView.layer.shadowOffset = CGSize(width: 0, height: 0)
        uiTableView.layer.shadowOpacity = 0.5
    }
    
    func setUpCollectionCardView(uiCollectionView : UICollectionView){
        uiCollectionView.backgroundColor = UIColor.white
        uiCollectionView.layer.cornerRadius = 5.0
        uiCollectionView.layer.masksToBounds = false
        
        uiCollectionView.layer.shadowColor = (UIColor(red: 150/255, green: 150/255, blue: 150/255)).withAlphaComponent(0.5).cgColor
        uiCollectionView.layer.shadowOffset = CGSize(width: 0, height: 0)
        uiCollectionView.layer.shadowOpacity = 0.5
    }
}

extension UIView {

    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11.0, *) {
            clipsToBounds = true
            layer.cornerRadius = radius
            layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
        } else {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }
    }
}

extension UIViewController{
    func setUpView(uiview : UIView){
        uiview.backgroundColor = UIColor.white
        uiview.layer.cornerRadius = 5.0
        uiview.layer.masksToBounds = false
        
        uiview.layer.shadowColor = (UIColor(red: 150/255, green: 150/255, blue: 150/255)).withAlphaComponent(0.5).cgColor
        uiview.layer.shadowOffset = CGSize(width: 0, height: 0)
        uiview.layer.shadowOpacity = 0.5
    }
}

private var kAssociationKeyMaxLength: Int = 0

extension UITextField {
    
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        
        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)
        
        selectedTextRange = selection
    }
}

