//
//  AppColors.swift
//  Ponno
//
//  Created by a k azad on 10/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import UIKit

struct AppColors{
    static let greenColor = UIColor(red:0.20, green:0.59, blue:0.29, alpha:1.0)
}
