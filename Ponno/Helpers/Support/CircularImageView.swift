//
//  CircularImageView.swift
//  Ponno
//
//  Created by a k azad on 18/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import UIKit

class CircularImageView: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
}
