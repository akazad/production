//
//  PostImageService.swift
//  Ponno
//
//  Created by a k azad on 18/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

public class PostImageService : NSObject{
    func postImage(params : [String: String], image : UIImage?,imageUrl : String,  succeed: @escaping((AddDataMapper)->Void), failure : @escaping((String) -> Void)){
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
            return
        }
        guard let imageToUpload = image else{
            return
        }
        let header = RestURL.sharedInstance.postCommonHeader()
        let parameters = params
        let imagePostURL = imageUrl
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            if let imageData = imageToUpload.jpegData(compressionQuality: 0.1) {
                multipartFormData.append(imageData, withName: "image", fileName: "file.jpeg", mimeType: "image/jpeg")
            }
            
            for params in parameters {
                let value = params.value
                multipartFormData.append((value.data(using: .utf8))!, withName: params.key)
            }}, to: imagePostURL, method: .post, headers: header,
                encodingCompletion: { encodingResult  in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseObject { (response: DataResponse<AddDataMapper>) in
                            switch response.result {
                            case .success:
                                if let rest = response.result.value {
                                    if rest.success == true{
                                        succeed(rest)
                                    }else{
                                        let mes = rest.message
                                        failure(mes!)
                                    }
                                }
                            case .failure(let error):
                                print(error)
                                failure(CommonMessages.ApiFailure)
                            }
                            
                        }
                    case .failure(let encodingError):
                        print("error:\(encodingError)")
                        failure(CommonMessages.ApiFailure)
                    }
        })
    }
    
    func postShopImage(image : UIImage?,imageUrl : String,  succeed: @escaping((AddDataMapper)->Void), failure : @escaping((String) -> Void)){
        if !Connectivity.isConnectedToInternet {
            failure(CommonMessages.NoInternet)
            return
        }
        guard let imageToUpload = image else{
            return
        }
        let header = RestURL.sharedInstance.postCommonHeader()
        let imagePostURL = imageUrl
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            if let imageData = imageToUpload.jpegData(compressionQuality: 0.1) {
                multipartFormData.append(imageData, withName: "image", fileName: "file.jpeg", mimeType: "image/jpeg")
            }}, to: imagePostURL, method: .post, headers: header,
                encodingCompletion: { encodingResult  in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseObject { (response: DataResponse<AddDataMapper>) in
                            switch response.result {
                            case .success:
                                if let rest = response.result.value {
                                    if rest.success == true{
                                        succeed(rest)
                                    }else{
                                        let mes = rest.message
                                        failure(mes!)
                                    }
                                }
                            case .failure(let error):
                                print(error)
                                failure(CommonMessages.ApiFailure)
                            }
                            
                        }
                    case .failure(let encodingError):
                        print("error:\(encodingError)")
                        failure(CommonMessages.ApiFailure)
                    }
        })
    }
}


