//
//  Extensions.swift
//  Ponno
//
//  Created by a k azad on 22/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

extension UIViewController {
    func showAlert(title :String, message : String) -> Void {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                //self.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func clearCachedImages(){
        SDImageCache.shared.clearMemory()
        SDImageCache.shared.clearDisk()
    }
    
    func setUpNavigationBarWith(backgroundColor : UIColor, textColor : UIColor, title : String){
        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: textColor,.font : UIFont.systemFont(ofSize: 17, weight: .bold)]
        self.title = title
    }
    
    func popOverForIpad(action: UIAlertController){
        if let popoverController = action.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
    }
    
    
    
}

extension UITextField {
    func togglePasswordVisibility() {
        isSecureTextEntry = !isSecureTextEntry

        if let existingText = text, isSecureTextEntry {
            /* When toggling to secure text, all text will be purged if the user
             continues typing unless we intervene. This is prevented by first
             deleting the existing text and then recovering the original text. */
            deleteBackward()

            if let textRange = textRange(from: beginningOfDocument, to: endOfDocument) {
                replace(textRange, withText: existingText)
            }
        }

        /* Reset the selected text range since the cursor can end up in the wrong
         position after a toggle because the text might vary in width */
        if let existingSelectedTextRange = selectedTextRange {
            selectedTextRange = nil
            selectedTextRange = existingSelectedTextRange
        }
    }
}

//extension UITableViewCell{
//    func showAlert(title :String, message : String) -> Void {
//            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//            
//            let OKAction = UIAlertAction(title: "OK", style: .default){
//                (action:UIAlertAction!) in
//                //self.dismiss(animated: true, completion: nil)
//            }
//            alertController.addAction(OKAction)
//            self.present(alertController, animated: true, completion: nil)
//    }
//}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.00, Double(places))
        return (self * divisor).rounded() / divisor
    }
}


extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
    
    public func toInt() -> Int? {
        if let num = NumberFormatter().number(from: self) {
            return num.intValue
        } else {
            return nil
        }
    }

      
}

extension Double {
    func toInt() -> Int {
        if self >= Double(Int.min) && self < Double(Int.max) {
            return Int(self)
        } else {
            return 0
        }
    }
    
    func toString() -> String {
        return String(format: "%.1f",self)
    }
}

extension Int{
    func toString() -> String {
        return String(format: "%.1f",self)
    }
}

extension Optional {
    func asStringOrEmpty() -> String {
        switch self {
            case .some(let value):
                return String(describing: value)
            case _:
                return ""
        }
    }

    func asStringOrNilText() -> String {
        switch self {
            case .some(let value):
                return String(describing: value)
            case _:
                return "(nil)"
        }
    }
}

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }

}

extension UIViewController{
//    func isObjectNotNil(object:AnyObject!) -> Bool
//    {
//        if let _:AnyObject = object
//        {
//            return true
//        }
//
//        return false
//    }
}

extension UIViewController{
    @objc func navigateToSpecificVC(storyboard: String, with identifier: String){
        let storyBoard:UIStoryboard = UIStoryboard(name: storyboard, bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension UINavigationController {
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}

extension UIViewController{
    func setUpInitialLanguage(){
        if getLanguage(){
            LanguageManager.setLanguageAs(language: .Bangla)
        }
        else{
            LanguageManager.setLanguageAs(language: .English)
        }
    }
    
    func prepareSerialNumbers(serials : String)->[String]{
        guard serials != "" else {
            return []
        }
        
        let serialList = serials.components(separatedBy: ",").filter{$0 != ""}
        
        
        guard serialList.count > 0 else {
            return []
        }
        
        return serialList
    }
    
    func totalSelligPrice(amounts : [Double], sellingPrices: [Double]) -> Double{

        var temp : Int = 0
        var totalPrice = 0.0
        for price in sellingPrices {
            if amounts.count > 0{
                let tempTotal = price * amounts[temp]
                totalPrice += tempTotal
                temp += 1
            }
        }
        
        return totalPrice
    }
    
}


extension UITextField {
    func addDoneCancelToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onCancel = onCancel ?? (target: self, action: #selector(cancelButtonTapped))
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: onCancel.target, action: onCancel.action),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()
        
        self.inputAccessoryView = toolbar
    }
    
    // Default actions:
    @objc func doneButtonTapped() { self.resignFirstResponder() }
    @objc func cancelButtonTapped() { self.resignFirstResponder() }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}

extension UIViewController{
    func showLoader(){
        SVProgressHUD.show()
    }
    func hideLoader(){
        SVProgressHUD.dismiss()
    }
}

extension UIButton {
    func centerTextAndImage(spacing: CGFloat) {
        let insetAmount = spacing / 2
        let writingDirection = UIApplication.shared.userInterfaceLayoutDirection
        let factor: CGFloat = writingDirection == .leftToRight ? 1 : -1
        
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount*factor, bottom: 0, right: insetAmount*factor)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount*factor, bottom: 0, right: -insetAmount*factor)
        self.contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
    }
}

extension String {
    
    public func isPhone()->Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "^(?:\\+?88)?01[3-9]\\d{8}$"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    
    private func isAllDigits()->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
}

extension String {
    func matches(_ regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
}

//MARK: Localization support
extension String {
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "**\(self)**", comment: "")
    }
    
}

extension UIViewController{
    func validatePhoneNumber(value: String) -> Bool {
        let PHONE_REGEX = "^(?:\\+?88)?01[3-9]\\d{8}$"
        if value.matches(PHONE_REGEX){
            return true
        }
        return false
    }
    
    func validatedName(value: String) -> Bool {
    let name_regex = "/^[A-Za-z]+([\\ A-Za-z]+)*/"
        if value.matches(name_regex){
            return true
        }
        return false
    }
}

extension UIImageView {
    
    public func addTapGestureRecognizer(action: (() -> Void)?) {
        tapAction = action
        isUserInteractionEnabled = true
        let selector = #selector(handleTap)
        let recognizer = UITapGestureRecognizer(target: self, action: selector)
        addGestureRecognizer(recognizer)
    }
}

fileprivate extension UIImageView {
    
    typealias Action = (() -> Void)
    
    struct Key { static var id = "tapAction" }
    
    var tapAction: Action? {
        get {
            return objc_getAssociatedObject(self, &Key.id) as? Action
        }
        set {
            guard let value = newValue else { return }
            let policy = objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN
            objc_setAssociatedObject(self, &Key.id, value, policy)
        }
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        tapAction?()
    }
}

//        Password must contain at least one letter, at least one number, and be longer than six charaters
//passwordRegex = ^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$

//extension UIViewController{
//    func popToViewController(_ viewController : UIViewController?){
//        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
//        for aViewController in viewControllers {
//            if aViewController is viewController {
//                self.navigationController!.popToViewController(aViewController, animated: true)
//            }
//        }
//    }
//}


//    func addFloaty(){
//        let floaty = Floaty()
//        floaty.buttonColor = UIColor.lightGreen
//
//        let extraItem = FloatyItem()
//        extraItem.icon = UIImage(named: "next")
//        extraItem.buttonColor = UIColor.lightRed
//        extraItem.title = "বিক্রয়পণ্য ফেরত"
//        extraItem.handler = { item in
//            self.navigateToSaleProductsReturnVC()
//        }
//
//        floaty.addItem(item: extraItem)
//        self.view.addSubview(floaty)
//    }


//class PaddedTextField: UITextField {
//
//    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
//        let rightBounds = CGRect(x:bounds.size.width - 60, y:16, width:18, height:18)
//        return rightBounds
//    }
//
//}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension Date {
    
    func isEqualTo(_ date: Date) -> Bool {
        return self == date
    }
    
    func isGreaterThan(_ date: Date) -> Bool {
        return self > date
    }
    
    func isSmallerThan(_ date: Date) -> Bool {
        return self < date
    }
}

//private var KeyMaxLength: Int = 0
//
//extension UITextField {
//    @IBInspectable var maxLength: Int {
//        get {
//            if let length = objc_getAssociatedObject(self, &KeyMaxLength) as? Int {
//                return length
//            } else {
//                return Int.max
//            }
//        }
//        set {
//            objc_setAssociatedObject(self, &KeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
//            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
//        }
//    }
//    
//    @objc func checkMaxLength(textField: UITextField) {
//        guard let prospectiveText = self.text,
//            prospectiveText.count > maxLength
//            else {
//                return
//        }
//        
//        let selection = selectedTextRange
//        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
//        text = prospectiveText.substring(to: maxCharIndex)
//        selectedTextRange = selection
//    }
//}

extension Array{
    func uniqueElementsFrom(array: [String]) -> [String] {
        //Create an empty Set to track unique items
        var set = Set<String>()
        let result = array.filter {
            guard !set.contains($0) else {
                //If the set already contains this object, return false
                //so we skip it
                return false
            }
            //Add this item to the set since it will now be in the array
            set.insert($0)
            //Return true so that filtered array will contain this item.
            return true
        }
        return result
    }
}

//extension Double {
//    /// Rounds the double to decimal places value
//    func roundToPlaces(places:Int) -> Double {
//        let divisor = pow(10.0, Double(places))
//        return round(self * divisor) / divisor
//    }
//}

//import UIKit
//
//class NoCopyPasteUITextField: UITextField {
//
//    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        if action == #selector(UIResponderStandardEditActions.paste(_:)) || action == #selector(UIResponderStandardEditActions.copy(_:)) || action == #selector(UIResponderStandardEditActions.select(_:)) || action == #selector(UIResponderStandardEditActions.selectAll(_:)){
//            return false
//        }
//        
//        return super.canPerformAction(action, withSender: sender)
//    }
//}
extension UIViewController {
    /// Call this once to dismiss open keyboards by tapping anywhere in the view controller
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(self.endEditingRecognizer())
        //self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }

    /// Dismisses the keyboard from self.view
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }
}

extension UIViewController{
   @objc func makePhoneCall(phoneNumber: String) {
        if let phoneURL = NSURL(string: ("tel:" + phoneNumber)) {
            
            let alert = UIAlertController(title: ("Call " + phoneNumber + " ?"), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Call", style: .default, handler: { (action) in
                UIApplication.shared.open(phoneURL as URL, options: [:], completionHandler: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension UIViewController{
    func showPopup(_ controller: UIViewController, sourceView: UIView) {
           let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller) 
           presentationController.sourceView = sourceView
           presentationController.sourceRect = sourceView.bounds
           presentationController.permittedArrowDirections = [.down, .up]
           self.present(controller, animated: true)
       }

}

extension UIImage {
    func imageWithInsets(insetDimen: CGFloat) -> UIImage {
        return imageWithInset(insets: UIEdgeInsets(top: insetDimen, left: insetDimen, bottom: insetDimen, right: insetDimen))
    }
    
    func imageWithInset(insets: UIEdgeInsets) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: self.size.width + insets.left + insets.right,
                   height: self.size.height + insets.top + insets.bottom), false, self.scale)
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithInsets!
    }
    
}

extension UIViewController{
    func setSegmentTintColor(segment: UISegmentedControl){
        segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: UIControl.State.selected)
    }
}

extension UIView {
    func setRadiusWithShadow(_ radius: CGFloat? = nil) { // this method adds shadow to right and bottom side of button
        self.layer.cornerRadius = radius ?? self.frame.width / 2
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        self.layer.shadowRadius = 1.0
        self.layer.shadowOpacity = 0.7
        self.layer.masksToBounds = false
    }

    func setAllSideShadow(shadowShowSize: CGFloat = 1.0) { // this method adds shadow to allsides
        let shadowSize : CGFloat = shadowShowSize
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: self.frame.size.width + shadowSize,
                                                   height: self.frame.size.height + shadowSize))
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.8).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = shadowPath.cgPath
    }
}

extension UIButton {
    func leftImage(image: UIImage, renderMode: UIImage.RenderingMode) {
        self.setImage(image.withRenderingMode(renderMode), for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: image.size.width / 2)
        self.contentHorizontalAlignment = .left
        self.imageView?.contentMode = .scaleAspectFit
    }

    func rightImage(image: UIImage, renderMode: UIImage.RenderingMode){
        self.setImage(image.withRenderingMode(renderMode), for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left:image.size.width / 2, bottom: 0, right: 0)
        self.contentHorizontalAlignment = .right
        self.imageView?.contentMode = .scaleAspectFit
    }
    
    func leftImage(image: UIImage) {
      self.setImage(image, for: .normal)
      self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: image.size.width)
    }
}

extension UITableViewCell{
    func soldQuantityAvailable(soldQuantity: String) ->Bool{
        var checkSoldQuantity : Bool = false
        if !soldQuantity.isEmpty{
            checkSoldQuantity = true
        }
        return checkSoldQuantity
    }
    
    func soldQuantityAvailable(soldQuantity: Double) ->Bool{
        var checkSoldQuantity : Bool = false
        if soldQuantity != 0{
            checkSoldQuantity = true
        }
        return checkSoldQuantity
    }
    
}

extension Int {
    var stringValue:String {
        return "\(self)"
    }
}

 
