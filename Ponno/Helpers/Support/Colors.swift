//
//  Colors.swift
//  Ponno
//
//  Created by a k azad on 9/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static let lightGreen = UIColor(red:0.96, green:1.00, blue:0.89, alpha:1.0)
    static let darkGreen = UIColor(red:0.53, green:0.74, blue:0.43, alpha:1.0)
    static let lightRed = UIColor(red:0.86, green:0.21, blue:0.27, alpha:1.0)
    static let deepGreen = UIColor(red:0.14, green:0.47, blue:0.22, alpha:1.0)
}


