//
//  ResponseMessage.swift
//  Ponno
//
//  Created by a k azad on 7/3/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation
import UIKit

class ResponseMessage: UIAlertController {
    func displayMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: "Warning", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
    }
    
    func successMessage(userMessage:String) -> Void {
            let alertController = UIAlertController(title: "Successful", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default){
                (action:UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(OKAction)
           self.present(alertController, animated: true, completion: nil)
    }
} 
