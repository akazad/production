//
//  CollectionViewScrollManager.swift
//  Ponno
//
//  Created by a k azad on 15/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
class CollectionViewScrollManager : NSObject{
    
    private var listCount : Int?
    
    private var isTimerOn : Bool = false
    private var collectionView : UICollectionView?
    
    var x = 1
    var timer: Timer?
    var newOffsetX: CGFloat = 0.0
    
    override init() {
        
    }
    
    public func setUpManager(listCount : Int, collectionView : UICollectionView){
        self.listCount = listCount
        self.collectionView = collectionView
        if(!isTimerOn){
            isTimerOn = true
        }
        
        self.startTimer()
    }
    
    private func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
    func invalidateTimer(){
        if self.timer != nil{
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    
//    func startTimer() {
//
//        carousalTimer = Timer(fire: Date(), interval: 0.015, repeats: true) { (timer) in
//
//            let initailPoint = CGPoint(x: self.newOffsetX,y :0)
//
//            guard let collectionView = self.collectionView else{
//                return
//            }
//
//            if __CGPointEqualToPoint(initailPoint, collectionView.contentOffset) {
//
//                if self.newOffsetX < collectionView.contentSize.width {
//                    self.newOffsetX += 2.0
//                }
//                if self.newOffsetX > collectionView.contentSize.width - collectionView.frame.size.width {
//                    self.newOffsetX = 0
//                }
//
//                collectionView.contentOffset = CGPoint(x: self.newOffsetX,y :0)
//
//            } else {
//                self.newOffsetX = collectionView.contentOffset.x
//            }
//        }
//
//        RunLoop.current.add(carousalTimer!, forMode: .default)
//    }
//
//    private func startTimer() {
//        Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
//
//    }
    
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        guard let listCount = listCount , listCount > 0 else {
            return
        }
        if let coll  = collectionView {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)!  < listCount - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                    print("ScrollingRight")
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                    print("ScrollingLeft")
                }
            
            }
        }
    }
    
    @objc func autoScroll(_ timer1: Timer) {
        guard let listCount = listCount , listCount > 0 else {
            return
        }
        if self.x < listCount {
            let indexPath = IndexPath(item: x, section: 0)
            self.collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        } else {
            self.x = 0
            self.collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
}

