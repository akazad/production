//
//  CoreDataActions.swift
//  Ponno
//
//  Created by a k azad on 18/12/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit
import CoreData

extension UIViewController{
    // MARK: - Core Data actions
//    func insertProduct(inventoryId: Int, unit: String, sellingPrice: Double, productId: Int, name: String, company: String, varient: String, sku: String, image: String, categoryId: Int, categoryName: String) {
//
//        var FreemiumProducts = [NSManagedObject]()
//
//        // Get AppDelegate Object
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//
//        // Get Managed Context Object with help of AppDelegate.
//        let managedContext = appDelegate.managedObjectContext
//
//        // Create Entity Description with your entity name.
//        let entity =  NSEntityDescription.entity(forEntityName: "FreemiumProducts",
//                                                 in:managedContext)
//
//        // Create Managed Objec with help of Entity and Managed Context.
//        let productObj = NSManagedObject(entity: entity!,
//                                      insertInto: managedContext)
//
//        // Set the value for your represent attributes.
//        productObj.setValue(inventoryId, forKey: "inventoryId")
//        productObj.setValue(unit, forKey: "unit")
//        productObj.setValue(sellingPrice, forKey: "sellingPrice")
//        productObj.setValue(productId, forKey: "productId")
//        productObj.setValue(name, forKey: "name")
//        productObj.setValue(company, forKey: "company")
//        productObj.setValue(varient, forKey: "varient")
//        productObj.setValue(sku, forKey: "sku")
//        productObj.setValue(image, forKey: "image")
//        productObj.setValue(categoryId, forKey: "categoryId")
//        productObj.setValue(categoryName, forKey: "categoryName")
//
//        // Handle the Exception.
//        do {
//            try managedContext.save() // Save Mangaged Context
//            print("saved")
//            FreemiumProducts.append(productObj) // Append your managedObject to the Database.
//
//
//        } catch let error as NSError  {
//            print("Could not save \(error), \(error.userInfo)")
//        }
//    }
    
    func insertFreemiumProduct(inventoryId: Int, unit: String, sellingPrice: Double, productId: Int, name: String, company: String, varient: String, sku: String, image: String, categoryId: Int, categoryName: String){
        
        //var FreemiumProducts = [NSManagedObject]()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let freemiumProductEntity = NSEntityDescription.entity(forEntityName: "FreemiumProducts", in: managedContext)!
        
        let productObj = NSManagedObject(entity: freemiumProductEntity,
                                         insertInto: managedContext)
        
        // Set the value for your represent attributes.
        productObj.setValue(inventoryId, forKey: "inventoryId")
        productObj.setValue(unit, forKey: "unit")
        productObj.setValue(sellingPrice, forKey: "sellingPrice")
        productObj.setValue(productId, forKey: "productId")
        productObj.setValue(name, forKey: "name")
        productObj.setValue(company, forKey: "company")
        productObj.setValue(varient, forKey: "varient")
        productObj.setValue(sku, forKey: "sku")
        productObj.setValue(image, forKey: "image")
        productObj.setValue(categoryId, forKey: "categoryId")
        productObj.setValue(categoryName, forKey: "categoryName")
        
        // Handle the Exception.
        do {
            try managedContext.save() // Save Mangaged Context
            print("saved")
            //FreemiumProducts.append(productObj) // Append your managedObject to the Database.
            
            
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
    }
    
    func insertProductCategory(id: Int, name: String, gen: Int, parent: Int){
                
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let productCategoryEntity = NSEntityDescription.entity(forEntityName: "ProductCategory", in: managedContext)!
        
        let categoryObj = NSManagedObject(entity: productCategoryEntity,
                                         insertInto: managedContext)
        
        // Set the value for your represent attributes.
        categoryObj.setValue(id, forKey: "id")
        categoryObj.setValue(name, forKey: "name")
        categoryObj.setValue(gen, forKey: "gen")
        categoryObj.setValue(parent, forKey: "parent")
        
        // Handle the Exception.
        do {
            try managedContext.save() // Save Mangaged Context
            print("saved")
            
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
    }
    
    func deleteAllData(entity: String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false

        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                print("DeletedSuccessfully")
                managedContext.delete(managedObjectData)
            }
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    
    func saveFreemiumProductData(_ products: [FreemiumProducts]) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        for product in products {
            let newProduct = NSEntityDescription.insertNewObject(forEntityName: "FreemiumProducts", into: managedContext)
            newProduct.setValue(product.inventoryId, forKey: "inventoryId")
            newProduct.setValue(product.unit, forKey: "unit")
            newProduct.setValue(product.sellingPrice, forKey: "sellingPrice")
            newProduct.setValue(product.productId, forKey: "productId")
            newProduct.setValue(product.name, forKey: "name")
            newProduct.setValue(product.company, forKey: "company")
            newProduct.setValue(product.varient, forKey: "varient")
            newProduct.setValue(product.sku, forKey: "sku")
            newProduct.setValue(product.image, forKey: "image")
            newProduct.setValue(product.categoryId, forKey: "categoryId")
            newProduct.setValue(product.categoryName, forKey: "categoryName")
        }
        do {
            try managedContext.save()
            print("Success")
        } catch {
            print("Error saving: \(error)")
        }
    }
    
    /** for updating items */
    func Update(inventoryId: Int64, unit: String, sellingPrice: Double, productId: Int64, name: String, company: String, varient: String, sku: String, image: String, categoryId: Int64, categoryName: String) {

        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "FreemiumProducts")
        let predicate = NSPredicate(format: "inventoryId = '\(inventoryId)'")
        fetchRequest.predicate = predicate
        do
        {
            let object = try context.fetch(fetchRequest)
            if object.count == 1
            {
                let objectUpdate = object.first as! NSManagedObject
                objectUpdate.setValue(inventoryId, forKey: "inventoryId")
                objectUpdate.setValue(unit, forKey: "unit")
                objectUpdate.setValue(sellingPrice, forKey: "sellingPrice")
                objectUpdate.setValue(productId, forKey: "productId")
                objectUpdate.setValue(name, forKey: "name")
                objectUpdate.setValue(company, forKey: "company")
                objectUpdate.setValue(varient, forKey: "varient")
                objectUpdate.setValue(sku, forKey: "sku")
                objectUpdate.setValue(image, forKey: "image")
                objectUpdate.setValue(categoryId, forKey: "categoryId")
                objectUpdate.setValue(categoryName, forKey: "categoryName")
                do{
                    print("Update successfully")
                    try context.save()
                }
                catch
                {
                    print(error)
                }
            }
        }
        catch
        {
            print(error)
        }
    }
    
    func deleteData(id : Int)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "FreemiumProducts")
        let predicate = NSPredicate(format: "inventoryId = '\(id)'")
        fetchRequest.predicate = predicate
        fetchRequest.returnsObjectsAsFaults = false

        do
        {
            let results = try managedContext.fetch(fetchRequest)
            let objectToDelete = results[0] as! NSManagedObject
            managedContext.delete(objectToDelete)
            
            do{
                try managedContext.save()
            }catch{
                print(error)
            }
        } catch let error as NSError {
            print(error)
        }
    }
    
    func resetAllRecords(in entity : String)
    {

        let context = ( UIApplication.shared.delegate as! AppDelegate ).persistentContainer.viewContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do
        {
            try context.execute(deleteRequest)
            try context.save()
        }
        catch
        {
            print ("There was an error")
        }
    }
    
    func updateCoreData(productId: Int, productName: String){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext

        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "FreemiumProducts")
        fetchRequest.predicate = NSPredicate(format: "productId = '\(productId)'")
        do{
            let object = try managedContext.fetch(fetchRequest)
            if object.count > 1{
                let objectUpdate = object.first as! NSManagedObject
                objectUpdate.setValue(productName, forKey: "name")
                do{
                    print("Updated")
                    try managedContext.save()
                }catch{
                    print(error)
                }
            }
        }catch{
            print(error)
        }
        
    }
}
