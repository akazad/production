//
//  UserDefaults.swift
//  Ponno
//
//  Created by a k azad on 19/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

func setRememberToken(token : String){
    let userDefaults = UserDefaults.standard
    userDefaults.set(token, forKey: "token")
}
func getRememberToken()->String{
    let userDefaults = UserDefaults.standard
    if let token = userDefaults.string(forKey: "token"){
        return token
    }
    return ""
}
func setLoggedIn(status : Bool){
     let userDefaults = UserDefaults.standard
     userDefaults.set(status, forKey: "setLoggedIn")
}
func isUserLoggedIn()->Bool{
    let userDefaults = UserDefaults.standard
    if userDefaults.bool(forKey : "setLoggedIn"){
        return true
    }
    return false
}

//UserDefaults For Shop

struct ShopInfo: Codable { 
    let name, registrationNumber, address, image: String?, inventorySystem, deliverySystem : Int?, invoiceNote: String?, paymentStatus, accStatus, category : Int?
}

func setShopData(shopInfo : ShopInfo){
    let shopData = ShopInfo(name: shopInfo.name, registrationNumber: shopInfo.registrationNumber, address: shopInfo.address, image : shopInfo.image, inventorySystem: shopInfo.inventorySystem, deliverySystem: shopInfo.deliverySystem, invoiceNote: shopInfo.invoiceNote, paymentStatus : shopInfo.paymentStatus, accStatus: shopInfo.accStatus, category: shopInfo.category)
    UserDefaults.standard.set(try! PropertyListEncoder().encode(shopData), forKey: "shop_info")
}

func getShopData()->ShopInfo?{
    let storedObject: Data = UserDefaults.standard.object(forKey: "shop_info") as! Data
    let shop: ShopInfo = try! PropertyListDecoder().decode(ShopInfo.self, from: storedObject)
    return shop
}

//ShopCategory
struct BusinessCategory: Codable {
    let businessCategory : Int?
}

func setBusinessCategoryData(category: BusinessCategory){
    let businessCategory = BusinessCategory(businessCategory: category.businessCategory)
    UserDefaults.standard.set(try! PropertyListEncoder().encode(businessCategory), forKey: "business_category")
}

func getBusinessCategory() -> BusinessCategory?{
    let storedObject: Data = UserDefaults.standard.object(forKey: "business_category") as! Data
    let category: BusinessCategory = try! PropertyListDecoder().decode(BusinessCategory.self, from: storedObject)
    return category
}

//TimeStamp
struct TimeStamp: Codable{
    let timeStamp: String?
}

func setTimeStamp(timeStamp: TimeStamp){
    let lastUpdatedTime = TimeStamp(timeStamp: timeStamp.timeStamp)
    UserDefaults.standard.set(try! PropertyListEncoder().encode(lastUpdatedTime), forKey: "last_updated_time")
}

func getTimeStamp() -> TimeStamp?{
    let storedObject: Data = UserDefaults.standard.object(forKey: "last_updated_time") as! Data
    let timeStamp: TimeStamp = try! PropertyListDecoder().decode(TimeStamp.self, from: storedObject)
    return timeStamp
    
}

//AppViewSettings
struct ViewSettings: Codable {
    let navigationItem : String?
    let dueCustomer : String?
    let settings : String?
    let dashboard : String?
    let freemiumProduct : String?
    let salePanel : String?
    
}

func setViewSettings(settings: ViewSettings){
    let viewSettings = ViewSettings(navigationItem: settings.navigationItem, dueCustomer: settings.dueCustomer, settings: settings.settings, dashboard: settings.dashboard, freemiumProduct: settings.freemiumProduct, salePanel: settings.salePanel)
    UserDefaults.standard.set(try! PropertyListEncoder().encode(viewSettings), forKey: "appViewSettings")
}

func getViewSettings() ->ViewSettings?{
    let storedObject: Data = UserDefaults.standard.object(forKey: "appViewSettings") as! Data
    let viewSettings: ViewSettings = try! PropertyListDecoder().decode(ViewSettings.self, from: storedObject)
    return viewSettings
}


//UserDefaults For User
struct UserInfo: Codable {
    let id: Int?, name, phone: String?, parent,  shopId: Int?
}

func setUserData(userInfo : UserInfo){
    let userData = UserInfo(id : userInfo.id!,name: userInfo.name!, phone: userInfo.phone!, parent: userInfo.parent!, shopId : userInfo.shopId!)
    UserDefaults.standard.set(try! PropertyListEncoder().encode(userData), forKey: "user_info")
}
func getUserData() -> UserInfo?{
    let storedUserObject: Data = UserDefaults.standard.object(forKey: "user_info")  as! Data
    let user: UserInfo = try! PropertyListDecoder().decode(UserInfo.self, from: storedUserObject)
    return user
}

//UserDefaults for Assigned Permission
struct AssignedPermission: Codable {
    let id: [Int]?
}

func setPermission(permission : AssignedPermission){
    let permission = AssignedPermission(id: permission.id)
    UserDefaults.standard.set(try! PropertyListEncoder().encode(permission), forKey: "assigned_permission")
}

func getPermission() -> AssignedPermission?{
    let storedUserObject: Data = UserDefaults.standard.object(forKey: "assigned_permission")  as! Data
    let permission: AssignedPermission = try! PropertyListDecoder().decode(AssignedPermission.self, from: storedUserObject)
    return permission
}

//User defaults for permission list
//struct Permission : Codable {
//    let id : Int?, name : String?
//}
//
//struct Permission_List : Codable {
//    let list : [Permission]?
//}

func setPermissionList(list: [PermissionList]){
    //let permissionList = Permission_List(id: list.id, name: list.name)
    UserDefaults.standard.set(try! PropertyListEncoder().encode(list), forKey: "permission_list")
}

func getPermissionList () -> [PermissionList]?{
    let storedUserObject: Data = UserDefaults.standard.object(forKey: "permission_list")  as! Data
    let permissionList: [PermissionList] = try! PropertyListDecoder().decode([PermissionList].self, from: storedUserObject)
    return permissionList
}

//getBtnDummyDataValue
struct DummyData: Codable {
    let status: Int?
}

func setDummyData(dummyData : DummyData){
    let dummyData = DummyData(status: dummyData.status)
    UserDefaults.standard.set(try! PropertyListEncoder().encode(dummyData), forKey: "btn_dummy_data")
}
func getDummyData() -> DummyData?{
    let storedUserObject: Data = UserDefaults.standard.object(forKey: "btn_dummy_data")  as! Data
    let dummyData : DummyData = try! PropertyListDecoder().decode(DummyData.self, from: storedUserObject)
    return dummyData
}

//MARK: Language

func setLanguage(isBangla : Bool){
    let userDefaults = UserDefaults.standard
    userDefaults.set(isBangla, forKey: "isBangla")
    setUpLanguage(isBangla : isBangla)
}
func setUpLanguage(isBangla : Bool){
    if isBangla{
        LanguageManager.setLanguageAs(language: .Bangla)
    }
    else{
        LanguageManager.setLanguageAs(language: .English)
    }
}
func getLanguage()->Bool{
    let userDefaults = UserDefaults.standard
    return userDefaults.bool(forKey: "isBangla")
}

//Get Version
struct Version: Codable {
    let version : String?
}

func setApplicationVersion(versionNumber : String){
    let versionData = Version(version: versionNumber)
    UserDefaults.standard.set(try! PropertyListEncoder().encode(versionData), forKey: "version")
}

func getApplicationVersion() -> Version?{
    let storedVersion : Data = UserDefaults.standard.object(forKey: "version") as! Data
    let version : Version = try! PropertyListDecoder().decode(Version.self, from: storedVersion)
    return version
}

