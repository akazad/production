//
//  Storyboards.swift
//  Ponno
//
//  Created by a k azad on 16/2/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

enum AppStoryboard : String {
    case Main = "Main"
    case Vendors = "Vendors"
    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
}
