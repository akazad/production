//
//  Constants.swift
//  Ponno
//
//  Created by a k azad on 22/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import UIKit

class Constants{
    static let currencySymbol = " ৳"
    static let currencyPerUnitSymbol = " ৳/Unit"
    static let pcsSymbol = " Pcs"
    static let perPiece = " ৳/Pcs"
    static let times = " বার"
}
