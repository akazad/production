//
//  RestURL.swift
//  Ponno
//
//  Created by a k azad on 19/1/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

public class RestURL : NSObject{
    static let sharedInstance = RestURL()
//    public var baseUrl = "https://www.beta.ponno.co/api/"
    public var baseUrl = "https://www.ponno.co/api/"
    public var Login = "login"
    public var Registration = "register"
    public var VersionCheck = "versionCheck"
    public var ShopCategory = "categories"
    public var Dashboard = "dashboard"
    public var DashboardFreemium = "dashboard_freemium"
    public var Sales = "sales"
    public var Expense = "expenses"
    public var PerExpenseList = "expenses/"
    public var ExpenseAdd = "expenses/store"
    public var ExpenseCategoryList = "expenses/category_list"
    public var ExpenseUpdate = "expenses/update"
    public var ExpenseDelete = "expenses/delete"
    public var SalesPerDay = "sales/perday/"
    public var SaleDetails = "sales/view/"
    public var SalesAdd = "sales/store"
    public var SalesPaymentMethod = "payment_methods"
    public var SaleProductsReturn = "sales/return"
    public var SaleCancel = "sales/cancel"
    public var SaleEdit = "sales/edit_new/"
    public var SaleUpdate = "sales/update"
    public var SaleDraft = "sales_draft"
    public var SalePreorder = "sales_pre_order"
    public var SaleDraftStore = "sales_draft/store"
    public var SaleDraftUpdate = "sales_draft/update"
    public var SalePreOrderStore = "sales_pre_order/store"
    public var SaleDraftDelete = "sales_draft/delete"
    public var SalePreOrderDelete = "sales_pre_order/delete"
    public var SaleDraftEdit = "sales_draft/edit/"
    public var SalePreOrderEdit = "sales_pre_order/edit/"
    public var SalePreOrderUpdate = "sales_pre_order/update"
    public var DueCustomerList = "dues"
    public var DueList = "dues/log"
    public var DueAdd = "dues/store"
    public var DueDelete = "dues/delete"
    public var DueUpdate = "dues/update"
    public var Employee = "employees"
    public var EmployeeDetails = "employees/view/"
    public var EmployeeSaleHistory = "employees/sales_history/"
    public var EmployeeSalaryHistory = "employees/salary_history/"
    public var EmployeePermissions = "permissions"
    public var EmployeeAdd = "employees/store"
    public var EmployeeUpdate = "employees/update"
    public var EmployeeStatusUpdate = "employees/status/update"
    public var PermissionsOfEmployee = "employees/get_permission/"
    public var EmployeeImageUpload = "employees/upload_image"
    public var EmployeeSalaryStore = "employees/salary_store"
    public var EmployeeUnpaidSalary = "employees/unpaid_salaries/"
    public var EmployeePaymentStore = "employees/payment_store"
    public var EmployeeSalaryUpdate = "employees/salary_update"
    public var EmployeeSalaryDelete = "employees/salary_delete"
    public var Purchase = "purchases"
    public var PerDayPurchase = "purchases/perday/"
    public var PurchaseDetails = "purchases/view/"
    public var PurchaseReturnDetails = "purchases/return/view/"
    public var PurchaseAdd = "purchases/store"
    public var PurchaseReturn = "purchases/return"
    public var PurchaseCancel = "purchases/cancel"
    public var PreOrders = "preorders"
    public var PreOrdersAdd = "preorders/store"
    public var PreOrdersCancel = "preorders/cancel"
    public var PreOrderEditProducts = "preorders/edit/"
    public var PreOrderUpdate = "preorders/update"
    public var PreOrderReceive = "preorders/receive"
    public var Payable = "payables"
    public var PayableLog = "payables/log"
    public var PayableAdd = "payables/store"
    public var PayableDelete = "payables/delete"
    public var PayableUpdate = "payables/update"
    public var Customer = "customers"
    public var CustomerView = "customers/view/"
    public var CustomerDueHistory = "customers/due_history/"
    public var CustomerPurchaseHistory = "customers/purchase_history/"
    public var CustomerAdd = "customers/store"
    public var CustomerUpdate = "customers/update"
    public var CustomerImageUpload = "customers/upload_image"
    public var DueAdjust = "due_adjust"
    public var DueAdjustStore = "due_adjust/store"
    public var DueAdjustUpdate = "due_adjust/update"
    public var DueAdjustDelete = "due_adjust/delete"
    public var CustomerWallet = "customer_wallet"
    public var CustomerWalletStore = "customer_wallet/store"
    public var CustomerWalletCashInOut = "customer_wallet/cash_in_out"
    public var CustomerWalletUpdate = "customer_wallet/update"
    public var CustomerWalletDelete = "customer_wallet/delete"
    public var Vendors = "vendors"
    public var VendorDetails = "vendors/view/"
    public var VendorPayableHistory = "vendors/payable_history/"
    public var VendorRawMaterialPayableHistory = "vendors/material_payable_history/"
    public var VendorPurchaseHistory = "vendors/purchase_history/"
    public var VendorRawMaterialPurchaseHistory = "vendors/material_purchase_history/"
    public var VendorReturnHistory = "vendors/return_history/"
    public var VendorRawMaterialReturnHistory = "vendors/material_return_history/"
    public var VendorAdd = "vendors/store"
    public var VendorUpdate = "vendors/update"
    public var VendorImageUpload = "vendors/upload_image"
    public var PayableAdjust = "payable_adjust"
    public var PayableAdjustStore = "payable_adjust/store"
    public var PayableAdjustUpdate = "payable_adjust/update"
    public var PayableAdjustDelete = "payable_adjust/delete"
    public var VendorWallet = "vendor_wallet"
    public var VendorWalletStore = "vendor_wallet/store"
    public var VendorWalletCashInOut = "vendor_wallet/cash_in_out"
    public var VendorWalletUpdate = "vendor_wallet/update"
    public var VendorWalletDelete = "vendor_wallet/delete"
    public var Delivery = "deliveries"
    public var DeliveryLog = "deliveries/log"
    public var DeliveryDetails = "deliveries/view/"
    public var DeliveryDetailsCurrentOrder = "deliveries/current_order/"
    public var DeliveryDetailsSaleHistory = "deliveries/sale_history/"
    public var DeliveryDetailsDueHistory = "deliveries/due_history/"
    public var DeliveryDueAdd = "deliveries/due/store"
    public var DeliveryAdd = "deliveries/store"
    public var DeliveryUpdate = "deliveries/update"
    public var DeliveryInvoiceRecieve = "deliveries/invoice_receive"
    public var DeliveryOtherDueWithdraw = "deliveries/other_due_withdraw"
    
    public var Product = "products"
    public var ProductView = "products/view/"
    public var ProductRecentSale = "products/recent_sales/"
    public var ProductRecentPurchase = "products/recent_purchases/"
    public var ProductSerial = "products/serials/"
    public var ProductCategories = "product_categories"
    public var ProductCategoriesAdd = "product_categories/store"
    public var ProductUnit = "product_units"
    public var ProductAdd = "products/store"
    public var ProductUpdate = "products/update"
    public var ProductReturnStock = "products/return_stock"
    public var ProductRecentStockUpdate = "products/update_stock"
    public var ProductRecentStockDelete = "products/delete_stock"
    public var ProductSerialUpdate = "products/update_serial"
    public var ProductDelete = "products/empty_inventory"
    public var ProductImageUpload = "products/upload_image"
    public var ProductGellAllSerials = "products/get_all_serials/"
    public var GlobalProducts = "/get_global_products_only"
    public var CategoryUpdate = "product_categories/update"
    public var CategoryDelete = "product_categories/delete"
    
    public var DamagedProduct = "damaged_products"
    public var DamagedProducts = "damaged_products?damaged_product="
    public var DamagedPerProduct = "damaged_products/perproduct/"
    public var DamagedProductDelete = "damaged_products/delete"
    public var DamagedProductUpdate = "damaged_products/update"
    public var DamagedProductStore = "damaged_products/store"
    
    public var RawMaterial = "materials"
    public var RawMaterialView = "materials/view/"
    public var RawMaterialDelete = "materials/empty_inventory"
    public var RawMaterialRecentStock = "materials/recent_stocks/"
    public var RawMaterialRecentUse = "materials/recent_usages/"
    public var RawMaterialCategories = "material_categories"
    public var RawMaterialRecentStockDelete = "materials/delete_stock"
    public var RawMaterialUpdate = "materials/update"
    public var RawMaterialCategoryStore = "material_categories/store"
    public var RawMaterialUploadImage = "materials/upload_image"
    public var RawMaterialRecentStockReturn = "materials/return_stock"
    public var RawMaterialStore = "materials/store"
    public var ProductionStore = "productions/store"
    public var RawMaterialPurchase = "material_purchases"
    public var RawMaterialPerDayPurchase = "material_purchases/perday/"
    public var RawMaterialPurchaseDetails = "material_purchases/view/"
    public var RawMaterialPerDayPurchaseCancel = "material_purchases/cancel"
    public var RawMaterialPurchaseReturn = "material_purchases/return"
    public var RawMaterialPurchaseStore = "material_purchases/store"
    
    public var DamagedRawMaterial = "damaged_materials"
    public var DamagedPerRawMaterial = "damaged_materials/permaterial/"
    public var DamagedRawMaterialStore = "damaged_materials/store"
    public var DamagedRawMaterialUpdate = "damaged_materials/update"
    public var DamagedRawMaterialDelete = "damaged_materials/delete"
    
    public var Accounts = "accounts"
    public var AccountsTemp = "accounts"
    public var Shop = "shop/general_info"
    public var ShopUpdate = "shop/update"
    public var ShopImageUpload = "shop/upload_image"
    public var ShopGeneralInfoUpdate = "shop/update/general_info"
    public var ShopLoginInfoUpdate = "shop/update/login_info"
    public var ShopSettings = "shop/settings"
    public var ShopSettingsUpdate = "shop/update/settings"
    public var ShopPaymentInfo = "shop/payment_info"
    public var ShopRefferalInfo = "shop/referral_info"
    public var ShopRefferalList = "shop/referral_list"
    public var ShopCategories = "shop/categories"
    public var ShopReferralAdd = "shop/referral/add"
    public var RemoveReferral = "shop/referral/delete"
    public var ClearShopData = "shop/clear_data"
    public var ShopDummyData = "shop/dummy_data"
    
    public var CashDrawers = "cashdrawers"
    public var CashDrawersStore = "cashdrawers/store"
    public var CashInOut = "cashdrawers/cash_in_out"
    public var CashTransactionUpdate = "cashdrawers/update"
    public var CashTransactionDelete = "cashdrawers/delete"
    public var Loans = "loans"
    public var LoansView = "loans/view/"
    public var LoanTransactionsHistory = "loans/transaction_history/"
    public var LoanStore = "loans/store"
    public var LoanUpdate = "loans/update"
    public var LoanWithdraw = "loans/withdraw"
    public var LoanTransactionUpdate = "loans_transaction/update"
    public var LoanTransactionDelete = "loans_transaction/delete"
    public var LoanTransactionLoanUpdate = "loans_transaction/update_loan"
    public var LoanImageUpload = "loans/upload_image"
    public var LoanDelete = "loans/delete"
    public var Investors = "invests"
    public var InvestStore = "invests/store"
    public var InvestorUpdate = "invests/update"
    public var InvestmentTransactionHistory = "invests/transaction_history/"
    public var InvestorView = "invests/view/"
    public var InvestorDelete = "invests/delete"
    public var InvestmentTransactionUpdate = "invests_transaction/update"
    public var InvestmentTransactionDelete = "invests_transaction/delete"
    public var InvestorImageUpload = "invests/upload_image"
    public var AssetProperties = "properties"
    public var AssetPropertiesStore = "properties/store"
    public var goodWillStore = "properties/goodwill/store"
    public var AssetPropertiesUpdate = "properties/update"
    public var goodWillUpdate = "properties/goodwill/update"
    public var AssetPropertiesDelete = "properties/delete"
    public var goodWillDelete = "properties/goodwill/delete"
    public var PropertyEquipmentList = "properties/assets/"
    public var goodWillList = "properties/goodwills"
    public var CashFlowReport = "cashflow_report"
    public var BalanceSheet = "balance_sheet"
    public var MobileBankingList = "payment_methods/list"
    public var PerMobileBankingList = "payment_methods/permethod/"
    public var MobileBankStore = "payment_methods/store"
    public var MobileBankUpdate = "payment_methods/update"
    public var MobileBankDelete = "payment_methods/delete"
    
    override public init() {
        Login = baseUrl + Login
        Registration = baseUrl + Registration
        VersionCheck = baseUrl + VersionCheck
        ShopCategory = baseUrl + ShopCategory
        Dashboard = baseUrl + Dashboard
        DashboardFreemium = baseUrl + DashboardFreemium
        Sales = baseUrl + Sales
        Expense = baseUrl + Expense
        PerExpenseList = baseUrl + PerExpenseList
        ExpenseAdd = baseUrl + ExpenseAdd
        ExpenseCategoryList = baseUrl + ExpenseCategoryList
        ExpenseUpdate = baseUrl + ExpenseUpdate
        ExpenseDelete = baseUrl + ExpenseDelete
        SalesPerDay = baseUrl + SalesPerDay
        SaleDetails = baseUrl + SaleDetails
        SalesAdd = baseUrl + SalesAdd
        SalesPaymentMethod  = baseUrl + SalesPaymentMethod
        SaleProductsReturn = baseUrl + SaleProductsReturn
        SaleCancel = baseUrl + SaleCancel
        SaleEdit = baseUrl + SaleEdit
        SaleUpdate = baseUrl + SaleUpdate
        SaleDraft = baseUrl + SaleDraft
        SalePreorder = baseUrl + SalePreorder
        SaleDraftStore = baseUrl + SaleDraftStore
        SaleDraftUpdate = baseUrl + SaleDraftUpdate
        SalePreOrderStore = baseUrl + SalePreOrderStore
        SaleDraftDelete = baseUrl + SaleDraftDelete
        SalePreOrderDelete = baseUrl + SalePreOrderDelete
        SaleDraftEdit = baseUrl + SaleDraftEdit
        SalePreOrderEdit = baseUrl + SalePreOrderEdit
        DueCustomerList = baseUrl + DueCustomerList
        DueList = baseUrl + DueList
        DueAdd = baseUrl + DueAdd
        DueDelete = baseUrl + DueDelete
        DueUpdate = baseUrl + DueUpdate
        Employee = baseUrl + Employee
        EmployeeDetails = baseUrl + EmployeeDetails
        EmployeeSaleHistory = baseUrl + EmployeeSaleHistory
        EmployeeSalaryHistory = baseUrl + EmployeeSalaryHistory
        EmployeeAdd = baseUrl + EmployeeAdd
        EmployeePermissions = baseUrl + EmployeePermissions
        EmployeeUpdate = baseUrl + EmployeeUpdate
        EmployeeStatusUpdate = baseUrl + EmployeeStatusUpdate
        PermissionsOfEmployee = baseUrl + PermissionsOfEmployee
        EmployeeImageUpload = baseUrl + EmployeeImageUpload
        EmployeeSalaryStore = baseUrl + EmployeeSalaryStore
        EmployeeUnpaidSalary = baseUrl + EmployeeUnpaidSalary
        EmployeePaymentStore = baseUrl + EmployeePaymentStore
        EmployeeSalaryUpdate = baseUrl + EmployeeSalaryUpdate
        EmployeeSalaryDelete = baseUrl + EmployeeSalaryDelete
        Purchase = baseUrl + Purchase
        PerDayPurchase = baseUrl + PerDayPurchase
        PurchaseDetails = baseUrl + PurchaseDetails
        PurchaseReturnDetails = baseUrl + PurchaseReturnDetails
        PurchaseAdd = baseUrl + PurchaseAdd
        PurchaseReturn = baseUrl + PurchaseReturn
        PurchaseCancel = baseUrl + PurchaseCancel
        PreOrders = baseUrl + PreOrders
        PreOrdersAdd = baseUrl + PreOrdersAdd
        PreOrdersCancel = baseUrl + PreOrdersCancel
        PreOrderEditProducts = baseUrl + PreOrderEditProducts
        Payable = baseUrl + Payable
        PayableLog = baseUrl + PayableLog
        PayableAdd = baseUrl + PayableAdd
        PayableDelete = baseUrl + PayableDelete
        PayableUpdate = baseUrl + PayableUpdate
        ProductImageUpload = baseUrl + ProductImageUpload
        Customer = baseUrl + Customer
        CustomerView = baseUrl + CustomerView
        CustomerDueHistory = baseUrl + CustomerDueHistory
        CustomerPurchaseHistory = baseUrl + CustomerPurchaseHistory
        CustomerAdd = baseUrl + CustomerAdd
        CustomerUpdate = baseUrl + CustomerUpdate
        CustomerImageUpload = baseUrl + CustomerImageUpload
        DueAdjust = baseUrl + DueAdjust
        DueAdjustStore = baseUrl + DueAdjustStore
        DueAdjustUpdate = baseUrl + DueAdjustUpdate
        DueAdjustDelete = baseUrl + DueAdjustDelete
        CustomerWallet = baseUrl + CustomerWallet
        CustomerWalletStore = baseUrl + CustomerWalletStore
        CustomerWalletCashInOut = baseUrl + CustomerWalletCashInOut
        CustomerWalletUpdate = baseUrl + CustomerWalletUpdate
        CustomerWalletDelete = baseUrl + CustomerWalletDelete
        Vendors = baseUrl + Vendors
        VendorDetails = baseUrl + VendorDetails
        VendorPayableHistory = baseUrl + VendorPayableHistory
        VendorRawMaterialPayableHistory = baseUrl + VendorRawMaterialPayableHistory
        VendorPurchaseHistory = baseUrl + VendorPurchaseHistory
        VendorRawMaterialPurchaseHistory = baseUrl + VendorRawMaterialPurchaseHistory
        VendorReturnHistory = baseUrl + VendorReturnHistory
        VendorRawMaterialReturnHistory = baseUrl + VendorRawMaterialReturnHistory
        VendorAdd = baseUrl + VendorAdd
        VendorUpdate = baseUrl + VendorUpdate
        VendorImageUpload = baseUrl + VendorImageUpload
        PayableAdjust = baseUrl + PayableAdjust
        PayableAdjustStore = baseUrl + PayableAdjustStore
        PayableAdjustUpdate = baseUrl + PayableAdjustUpdate
        PayableAdjustDelete = baseUrl + PayableAdjustDelete
        VendorWallet = baseUrl + VendorWallet
        VendorWalletStore = baseUrl + VendorWalletStore
        VendorWalletCashInOut = baseUrl + VendorWalletCashInOut
        VendorWalletUpdate = baseUrl + VendorWalletUpdate
        VendorWalletDelete = baseUrl + VendorWalletDelete
        Delivery = baseUrl + Delivery
        DeliveryLog = baseUrl + DeliveryLog
        DeliveryDetails = baseUrl + DeliveryDetails
        DeliveryDetailsCurrentOrder = baseUrl + DeliveryDetailsCurrentOrder
        DeliveryDetailsSaleHistory  = baseUrl + DeliveryDetailsSaleHistory
        DeliveryDetailsDueHistory = baseUrl + DeliveryDetailsDueHistory
        DeliveryDueAdd = baseUrl + DeliveryDueAdd
        DeliveryAdd = baseUrl + DeliveryAdd
        DeliveryInvoiceRecieve = baseUrl + DeliveryInvoiceRecieve
        DeliveryUpdate = baseUrl + DeliveryUpdate
        DeliveryOtherDueWithdraw = baseUrl + DeliveryOtherDueWithdraw
        
        Product = baseUrl + Product
        ProductView = baseUrl + ProductView
        ProductRecentSale = baseUrl + ProductRecentSale
        ProductRecentPurchase = baseUrl + ProductRecentPurchase
        ProductSerial = baseUrl + ProductSerial
        ProductCategories = baseUrl + ProductCategories
        ProductCategoriesAdd = baseUrl + ProductCategoriesAdd
        ProductUnit = baseUrl + ProductUnit
        ProductAdd = baseUrl + ProductAdd
        ProductUpdate = baseUrl + ProductUpdate
        ProductReturnStock = baseUrl + ProductReturnStock
        ProductRecentStockUpdate = baseUrl + ProductRecentStockUpdate
        ProductRecentStockDelete = baseUrl + ProductRecentStockDelete
        ProductSerialUpdate = baseUrl + ProductSerialUpdate
        ProductDelete = baseUrl + ProductDelete
        ProductGellAllSerials = baseUrl + ProductGellAllSerials
        CategoryUpdate = baseUrl + CategoryUpdate
        CategoryDelete = baseUrl + CategoryDelete

        //GlobalProducts = baseUrl + GlobalProducts
        
        DamagedProduct = baseUrl + DamagedProduct
        DamagedProducts = baseUrl + DamagedProducts
        DamagedPerProduct = baseUrl + DamagedPerProduct
        DamagedProductDelete = baseUrl + DamagedProductDelete
        DamagedProductUpdate = baseUrl + DamagedProductUpdate
        DamagedProductStore = baseUrl + DamagedProductStore
        
        DamagedRawMaterial = baseUrl + DamagedRawMaterial
        DamagedPerRawMaterial = baseUrl + DamagedPerRawMaterial
        DamagedRawMaterialStore = baseUrl + DamagedRawMaterialStore
        DamagedRawMaterialUpdate = baseUrl + DamagedRawMaterialUpdate
        DamagedRawMaterialDelete = baseUrl + DamagedRawMaterialDelete
        
        RawMaterial = baseUrl + RawMaterial
        RawMaterialView = baseUrl + RawMaterialView
        RawMaterialDelete = baseUrl + RawMaterialDelete
        RawMaterialRecentStock = baseUrl + RawMaterialRecentStock
        RawMaterialRecentUse = baseUrl + RawMaterialRecentUse
        RawMaterialCategories = baseUrl + RawMaterialCategories
        RawMaterialRecentStockDelete = baseUrl + RawMaterialRecentStockDelete
        RawMaterialUpdate = baseUrl + RawMaterialUpdate
        RawMaterialCategoryStore = baseUrl + RawMaterialCategoryStore
        RawMaterialUploadImage = baseUrl + RawMaterialUploadImage
        RawMaterialRecentStockReturn = baseUrl + RawMaterialRecentStockReturn
        RawMaterialStore = baseUrl + RawMaterialStore
        ProductionStore = baseUrl + ProductionStore
        RawMaterialPurchase = baseUrl + RawMaterialPurchase
        RawMaterialPerDayPurchase = baseUrl + RawMaterialPerDayPurchase
        RawMaterialPerDayPurchaseCancel = baseUrl + RawMaterialPerDayPurchaseCancel
        RawMaterialPurchaseReturn = baseUrl + RawMaterialPurchaseReturn
        RawMaterialPurchaseStore = baseUrl + RawMaterialPurchaseStore
        RawMaterialPurchaseDetails = baseUrl + RawMaterialPurchaseDetails
        
        Accounts = baseUrl + Accounts
        AccountsTemp = baseUrl + AccountsTemp
        Shop = baseUrl + Shop
        ShopUpdate = baseUrl + ShopUpdate
        ShopImageUpload = baseUrl + ShopImageUpload
        ShopGeneralInfoUpdate = baseUrl + ShopGeneralInfoUpdate
        ShopLoginInfoUpdate = baseUrl + ShopLoginInfoUpdate
        ShopSettings = baseUrl + ShopSettings
        ShopSettingsUpdate = baseUrl + ShopSettingsUpdate
        ShopPaymentInfo = baseUrl + ShopPaymentInfo
        ShopRefferalInfo = baseUrl + ShopRefferalInfo
        ShopRefferalList = baseUrl + ShopRefferalList
        ShopCategories = baseUrl + ShopCategories
        ShopReferralAdd = baseUrl + ShopReferralAdd
        RemoveReferral = baseUrl + RemoveReferral
        ClearShopData = baseUrl + ClearShopData
        ShopDummyData = baseUrl + ShopDummyData
        CashDrawers = baseUrl + CashDrawers
        CashDrawersStore = baseUrl + CashDrawersStore
        CashInOut = baseUrl + CashInOut
        CashTransactionUpdate = baseUrl + CashTransactionUpdate
        CashTransactionDelete = baseUrl + CashTransactionDelete
        Loans = baseUrl + Loans
        LoansView = baseUrl + LoansView
        LoanTransactionsHistory = baseUrl + LoanTransactionsHistory
        LoanStore = baseUrl + LoanStore
        LoanUpdate = baseUrl + LoanUpdate
        LoanWithdraw = baseUrl + LoanWithdraw
        LoanImageUpload = baseUrl + LoanImageUpload
        LoanTransactionUpdate = baseUrl + LoanTransactionUpdate
        LoanTransactionDelete = baseUrl + LoanTransactionDelete
        LoanTransactionLoanUpdate = baseUrl + LoanTransactionLoanUpdate
        LoanDelete = baseUrl + LoanDelete
        
        Investors = baseUrl + Investors
        InvestStore = baseUrl + InvestStore
        InvestorUpdate = baseUrl + InvestorUpdate
        InvestorView = baseUrl + InvestorView
        InvestorDelete = baseUrl + InvestorDelete
        InvestmentTransactionHistory = baseUrl + InvestmentTransactionHistory
        InvestmentTransactionUpdate = baseUrl + InvestmentTransactionUpdate
        InvestmentTransactionDelete = baseUrl + InvestmentTransactionDelete
        InvestorImageUpload = baseUrl + InvestorImageUpload
        
        AssetProperties = baseUrl + AssetProperties
        AssetPropertiesStore = baseUrl + AssetPropertiesStore
        goodWillStore = baseUrl + goodWillStore
        AssetPropertiesUpdate = baseUrl + AssetPropertiesUpdate
        goodWillUpdate = baseUrl + goodWillUpdate
        AssetPropertiesDelete = baseUrl + AssetPropertiesDelete
        goodWillDelete = baseUrl + goodWillDelete
        PropertyEquipmentList = baseUrl + PropertyEquipmentList
        goodWillList = baseUrl + goodWillList
        CashFlowReport = baseUrl + CashFlowReport
        BalanceSheet = baseUrl + BalanceSheet
        MobileBankingList = baseUrl + MobileBankingList
        PerMobileBankingList = baseUrl + PerMobileBankingList
        MobileBankStore = baseUrl + MobileBankStore
        MobileBankUpdate = baseUrl + MobileBankUpdate
        MobileBankDelete = baseUrl + MobileBankDelete
    } 
    
    public func getLoginParams(userPhoneNumber : String, userPassword : String)-> [String: String]{
        let params = ["phone": userPhoneNumber ,"password": userPassword]
        return params
    }
    
    public func getCommonHeader()->[String:String]{
        
        if let appVersion = getApplicationVersion(), let versions = appVersion.version{
            let headers : [String:String] = [
                "Authorization": "Bearer " + getRememberToken(),
                "Content-type" : "application/json",
                "os" : "iOS",
                "version" : versions
            ]
            return headers
        }else{
            return [:]
        }
    }
    
    public func getSalesPerDayUrl(date: String)->String{
        return date
    }
    public func getSalesPerDayHeader()->[String:String]{
        return ["Authorization": "Bearer " + getRememberToken()]
    }
    
    public func getSalesParams(page : Int)->String{
        return "?page=\(page)"
    }
    
    public func getPageNumber(page : Int)->String{
        return "&page=\(page)"
    }
    
    public func getPaginationNumber(page: Int) -> String{
        return "?page=\(page)"
    }
    
    public func getBusinessCategory(businessCategory: Int) -> String{
        return "?business_category=\(businessCategory)"
    }
    
    public func getSearchText(text : String)->String{
        return "?search=\(text)"
    }
    
    public func getProductListOfflineWithTimeStamp(timeStamp: String) -> String{
        return "/get_product_list_offline?last_updated_time='\(timeStamp)'"
    }
    
    public func getSearchSerial(text : String)->String{
        return "\(text)"
    }
    
    public func getDateRange(startDate : String, endDate: String) -> String{
        return "?"+"startDt=\(startDate)" + "&"+"endDt=\(endDate)"
    }
    
    public func getSingleDate(date : String) -> String{
        return "?"+"date=\(date)"
    }
    
    public func getDateRangePage(text : Int) -> String{
        return "&page=\(text)"
    }
    
    public func getAllData(text : Bool) -> String{
        return "?get_all=\(text)"
    }
    
    public func getAllType(text : Int) -> String {
        return "?type=\(text)"
    }
    
    public func getSalesPerDayParams(page: Int)->String{
        return "?page=\(page)"
    }
    
    public func getSaleDetailsUrl(id : Int)->String{
        return "\(id)"
    }
    
    public func getCommonId(id: Int)-> String{
        return "\(id)"
    }
    
    public func getPerDayPurchaseUrl(date: String)->String{
        return date 
    }
    public func getPurchaseDetailsUrl(id: Int)->String{
        return "\(id)"
    }
    
    public func getPerExpenseListUrl(id: String)->String{
        return "\(id)"
    }
    
    public func getDueParams(page: Int)-> String{
        return "?page=\(page)"
    }
    public func getProductDetailsViewUrl(id: Int) -> String{
        return "\(id)"
    }
    public func getSaleId(id: Int) -> String{
        return "?sale_id=\(id)"
    }
    public func getProductRecentPurchaseParams(page: Int) -> String{
        return "?page=\(page)"
    }
    public func getCustomerDetailsViewUrl(id: Int) -> String{
        return String(describing: id)
    }
    public func getCustomerParams(page: Int)-> String{
        return "?page=\(page)"
    }
    public func getVendorDetailsUrl(id: Int) -> String{
        return String(describing: id)
    }
    public func getVendorsParam(page: Int)->String{
        return "?page=\(page)"
    }
    public func getEmployeeDetailsUrl(id: Int) -> String{
        return String(describing: id)
    }
    public func getDeliveryDetailsUrl(id: Int) -> String{
        return String(describing: id)
    }
    public func getPurchaseReturnDetailsUrl(id: Int) -> String{
        return String(describing: id)
    }
    public func getPreOrderEditProductListUrl(id : Int) -> String {
        return "\(id)"
    }
    public func getPayableParams(page: Int)->String{
        return "?page=\(page)"
    }
    
    public func getModuleList() -> String{
        return "/module_list"
    }
    
    public func addedBy(addedBy: String) -> String{
        return "&added_by=\(addedBy)"
    }
    
    public func getDueAdjustId(id: Int) -> String{
        return "/\(id)"
    }
    
    public func advanceProductSearch(name: String, company: String, variant: String, category: String, dealer: String) -> String{
        return "?name=\(name)" + "&company=\(company)" + "&variant=\(variant)" + "&category=\(category)" + "&dealer=\(dealer)"
    }
    
//    Post
    public func postCommonHeader()->[String:String]{
                
        if let appVersion = getApplicationVersion(), let versions = appVersion.version{
            let headers : [String:String] = [
                "Authorization": "Bearer " + getRememberToken(),
                "Content-type" : "application/json",
                "os" : "iOS",
                "version" : versions
            ]
            return headers
        }else{
            return [:]
        }
    }
}
