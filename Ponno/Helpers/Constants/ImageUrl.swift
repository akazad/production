//
//  ImageUrl.swift
//  Ponno
//
//  Created by a k azad on 18/5/19.
//  Copyright © 2019 Ponno. All rights reserved.
//

import Foundation

public class ImageUrl: NSObject{
    static let sharedImageInstance = ImageUrl()
    public var imageBaseUrl = "https://www.beta.ponno.co/uploads/images/"
    //public var imageBaseUrl = "https://www.ponno.co/uploads/images/"
    public var customerImage = "customer/"
    public var employeeImage = "employee/"
    public var vendorImage = "vendor/"
    public var productImage = "product/"
    public var shopImage = "shop/"
    public var loanerImage = "loans/"
    public var investorImage = "investor/"
    public var rawMaterialImage = "materials/"
    
    override public init(){
        customerImage = imageBaseUrl + customerImage
        employeeImage = imageBaseUrl + employeeImage
        vendorImage = imageBaseUrl + vendorImage
        productImage = imageBaseUrl + productImage
        shopImage = imageBaseUrl + shopImage
        loanerImage = imageBaseUrl + loanerImage
        investorImage = imageBaseUrl + investorImage
        rawMaterialImage = imageBaseUrl + rawMaterialImage
    } 
    
}
