//
//  ProductCategory+CoreDataProperties.swift
//  
//
//  Created by a k azad on 24/12/19.
//
//

import Foundation
import CoreData


extension ProductCategory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ProductCategory> {
        return NSFetchRequest<ProductCategory>(entityName: "ProductCategory")
    }

    @NSManaged public var parent: Int64
    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var gen: Int64

}
